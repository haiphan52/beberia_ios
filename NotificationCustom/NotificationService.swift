//
//  NotificationService.swift
//  NotificationCustom
//
//  Created by IMAC on 10/15/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UserNotifications
import UIKit
import Foundation

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    let KeyEntitlement = "group.com.gngbc.Beberia"

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        let test = UserDefaults.init(suiteName: KeyEntitlement)
        let a = test?.integer(forKey: "badge")
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
            
            guard let userInfo = bestAttemptContent.userInfo["data"] else {
                contentHandler(bestAttemptContent)
                return
            }
            
            guard let stringDict = userInfo as? String,  let dicUserInfo = convertToDictionary(text: stringDict) else {
                contentHandler(bestAttemptContent)
                return
            }
            
            bestAttemptContent.title = ""
            bestAttemptContent.sound = UNNotificationSound.default
            bestAttemptContent.badge = (a ?? 0) + 1 as NSNumber
            
            guard let dicUserInfoType = dicUserInfo["type"] as? Int, let listNoti = NotificationList(rawValue: dicUserInfoType)  else {
                contentHandler(bestAttemptContent)
                return
            }
            
          //  let listNoti = NotificationList(rawValue: dicUserInfo["type"] as! Int)
            
            
            switch listNoti {
            // Feed
            case .NewMessage:
                if let groupName = self.setupNewMessage(dicUserInfo: dicUserInfo).0 {
                    bestAttemptContent.title = groupName
                }
                if let body = self.setupNewMessage(dicUserInfo: dicUserInfo).1 {
                    bestAttemptContent.body = body
                }
            case .LikeFeed, .LikeDiary, .LikeInfo:
                bestAttemptContent.body = setuplikeFeed(likeFeed: dicUserInfo, typeNoti: listNoti)
            case .LikeComment, .LikeComtDiary, .LikeComInfo:
                bestAttemptContent.body = setuplikeComment(likeFeed: dicUserInfo, typeNoti: listNoti)
            case .LikeRepCom, .LikeRepComDiary, .LikeRepComInfo:
                bestAttemptContent.body = setuplikeRepCom(likeFeed: dicUserInfo, typeNoti: listNoti)
            case .ComFeed, .ComDiary, .ComInfo:
                bestAttemptContent.body = setupComFeed(likeFeed: dicUserInfo, typeNoti: listNoti)
            case .RepComt, .RepComDiary, .RepcomInfo:
                 bestAttemptContent.body = setupRepCom(likeFeed: dicUserInfo, typeNoti: listNoti)
                
            case .RemindWrite:
                bestAttemptContent.body = "Bạn đã lâu rồi chưa viết nhật ký!"
                
            case .LikeSecret, .ComSecret, .LikeRepComSecret, .RepcomSecret, .LikeComSecret:
                bestAttemptContent.body = "Bạn có một thông báo mới!"
                
            case .SENDREQUESTFRIEND:
                bestAttemptContent.body = setupSendRequestFriend(likeFeed: dicUserInfo, typeNoti: listNoti)
                
            case .NewFeed:
                bestAttemptContent.body = ""
                
            case .NotificationNomar :
                bestAttemptContent.body = (dicUserInfo["content"] as? String) ?? ""
                
            case .NOTIFICATIONGAME:
                bestAttemptContent.body = setupNotificationGame(likeFeed: dicUserInfo, typeNoti: listNoti)
            }
            
            

            contentHandler(bestAttemptContent)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    
    private func setupNewMessage(dicUserInfo: [String: Any]) -> (String?, String?) {
        var groupName: String?
        if let group = dicUserInfo["group"] as? [AnyHashable: Any], let name = group["group_name"] as? String {
            groupName = name
        }
        
        var lastMessage: String = ""
        if let message = dicUserInfo["message"] as? [AnyHashable: Any],
           let sender = message["sender"] as? [AnyHashable: Any],
            let userName = sender["display_name"] as? String,
           let messageType = message["message_type"] as? Int {
            lastMessage = userName + ": "
            switch messageType {
            case 1:
                if let messageStr = message["message"] as? String {
                    lastMessage += messageStr
                }
            case 2:
                lastMessage += "[Hình ảnh]"
            default: break
            }
        }
        
        return (groupName, lastMessage)
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func setuplikeFeed(likeFeed: [String: Any], typeNoti: NotificationList)-> String{
        var body = ""
        if (likeFeed["total_user"] as! Int) > 0 {
            body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") cùng \(likeFeed["total_user"] ?? 0) người khác \(typeNoti.title)"
        }else {
            body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") \(typeNoti.title)"
        }
        
        UNUserNotificationCenter.current()
            .getDeliveredNotifications { notifications in
                let matching = notifications.first(where: { notify in
                    let existingUserInfo = notify.request.content.userInfo
                    let id = 0
                    if let dicUserInfo = self.convertToDictionary(text: existingUserInfo["data"]! as! String) {
                        let id = (dicUserInfo["feed"] as! [AnyHashable: Any])["id"] as? Int
                        return id == (likeFeed["feed"] as! [AnyHashable: Any])["id"] as? Int && typeNoti.rawValue == dicUserInfo["type"] as? Int
                    }

                    return id == 0

                })

                if let matchExists = matching {
                    UNUserNotificationCenter.current().removeDeliveredNotifications(
                        withIdentifiers: [matchExists.request.identifier]
                    )
                }
        }
        
        
        
        return body
    }
    
    func setuplikeComment(likeFeed: [String: Any], typeNoti: NotificationList)-> String{
        var body = ""
        if (likeFeed["total_user"] as! Int) > 0 {
            body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") cùng \(likeFeed["total_user"] ?? 0) người khác \(typeNoti.title)"
        }else {
            body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") \(typeNoti.title)"
        }
        
        UNUserNotificationCenter.current()
            .getDeliveredNotifications { notifications in
                let matching = notifications.first(where: { notify in
                    let existingUserInfo = notify.request.content.userInfo
                    let id = 0
                    if let dicUserInfo = self.convertToDictionary(text: existingUserInfo["data"]! as! String) {
                        let id = (dicUserInfo["comment"] as! [AnyHashable: Any])["comment_id"] as? Int
                        return id == (likeFeed["comment"] as! [AnyHashable: Any])["comment_id"] as? Int && typeNoti.rawValue == dicUserInfo["type"] as? Int
                    }
                    
                    return id == 0
                    
                })
                
                if let matchExists = matching {
                    UNUserNotificationCenter.current().removeDeliveredNotifications(
                        withIdentifiers: [matchExists.request.identifier]
                    )
                }
        }
        
        
        
        return body
    }
    
    func setuplikeRepCom(likeFeed: [String: Any], typeNoti: NotificationList)-> String{
        var body = ""
        if (likeFeed["total_user"] as! Int) > 0 {
            body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") cùng \(likeFeed["total_user"] ?? 0) người khác \(typeNoti.title)"
        }else {
            body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") \(typeNoti.title)"
        }
        
        UNUserNotificationCenter.current()
            .getDeliveredNotifications { notifications in
                let matching = notifications.first(where: { notify in
                    let existingUserInfo = notify.request.content.userInfo
                    let id = 0
                    if let dicUserInfo = self.convertToDictionary(text: existingUserInfo["data"]! as! String) {
                        let id = (dicUserInfo["reply_comment"] as! [AnyHashable: Any])["comment_id"] as? Int
                        return id == (likeFeed["reply_comment"] as! [AnyHashable: Any])["comment_id"] as? Int && typeNoti.rawValue == dicUserInfo["type"] as? Int
                    }
                    
                    return id == 0
                    
                })
                
                if let matchExists = matching {
                    UNUserNotificationCenter.current().removeDeliveredNotifications(
                        withIdentifiers: [matchExists.request.identifier]
                    )
                }
        }
        
        
        
        return body
    }

    func setupComFeed(likeFeed: [String: Any], typeNoti: NotificationList)-> String{
        var body = ""
        let defaults = UserDefaults.init(suiteName: KeyEntitlement)
        if (likeFeed["total_user"] as! Int) > 0 {
            if ((likeFeed["feed"] as! [AnyHashable: Any])["user_create"] as! [AnyHashable: Any])["id"] as? Int == defaults?.integer(forKey: "userID") {
                body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") cùng \(likeFeed["total_user"] ?? 0) người khác \(typeNoti.title) bạn"

            }else {
                body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") cùng \(likeFeed["total_user"] ?? 0) người khác \(typeNoti.title) \(((likeFeed["feed"] as! [AnyHashable: Any])["user_create"] as! [AnyHashable: Any])["display_name"] ?? "")"
            }
        }else {
            if ((likeFeed["feed"] as! [AnyHashable: Any])["user_create"] as! [AnyHashable: Any])["id"] as? Int == defaults?.integer(forKey: "userID") {
                body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") \(typeNoti.title) bạn"
            }else {
                body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") \(typeNoti.title) \(((likeFeed["feed"] as! [AnyHashable: Any])["user_create"] as! [AnyHashable: Any])["display_name"] ?? "")"
            }
        }
        
        UNUserNotificationCenter.current()
            .getDeliveredNotifications { notifications in
                let matching = notifications.first(where: { notify in
                    let existingUserInfo = notify.request.content.userInfo
                    let id = 0
                    if let dicUserInfo = self.convertToDictionary(text: existingUserInfo["data"]! as! String) {
                        let id = (dicUserInfo["feed"] as! [AnyHashable: Any])["id"] as? Int
                        return id == (likeFeed["feed"] as! [AnyHashable: Any])["id"] as? Int && typeNoti.rawValue == dicUserInfo["type"] as? Int
                    }
                    
                    return id == 0
                    
                })
                
                if let matchExists = matching {
                    UNUserNotificationCenter.current().removeDeliveredNotifications(
                        withIdentifiers: [matchExists.request.identifier]
                    )
                }
        }
        
        return body
    }

    func setupRepCom(likeFeed: [String: Any], typeNoti: NotificationList) -> String{
        let defaults = UserDefaults.init(suiteName: KeyEntitlement)
        var body = ""
        if (likeFeed["total_user"] as! Int) > 0 {
            if ((likeFeed["comment"] as! [AnyHashable: Any])["user"] as! [AnyHashable: Any])["id"] as? Int == defaults?.integer(forKey: "userID") {
                body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") cùng \(likeFeed["total_user"] ?? 0) người khác \(typeNoti.title) bạn"

            }else {
          //      body = "\(likeFeed.userFrom?.displayName ?? "") cùng \(likeFeed.totalUser ?? 0) người khác \(typeNoti.title) \(likeFeed.comment?.user?.displayName ?? "")"
                
                body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") cùng \(likeFeed["total_user"] ?? 0) người khác \(typeNoti.title) \(((likeFeed["comment"] as! [AnyHashable: Any])["user"] as! [AnyHashable: Any])["display_name"] ?? "")"
            }
        }else {

            if ((likeFeed["comment"] as! [AnyHashable: Any])["user"] as! [AnyHashable: Any])["id"] as? Int == defaults?.integer(forKey: "userID") {
                 body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") \(typeNoti.title) bạn"
            }else {
                // body = "\(likeFeed.userFrom?.displayName ?? "") \(typeNoti.title) \(likeFeed.comment?.user?.displayName ?? "")"
                
                body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") \(typeNoti.title) \(((likeFeed["comment"] as! [AnyHashable: Any])["user"] as! [AnyHashable: Any])["display_name"] ?? "")"
            }
        }
        
        
        UNUserNotificationCenter.current()
            .getDeliveredNotifications { notifications in
                let matching = notifications.first(where: { notify in
                    let existingUserInfo = notify.request.content.userInfo
                    let id = 0
                    if let dicUserInfo = self.convertToDictionary(text: existingUserInfo["data"]! as! String) {
                        let id = (dicUserInfo["comment"] as! [AnyHashable: Any])["comment_id"] as? Int
                        return id == (likeFeed["comment"] as! [AnyHashable: Any])["comment_id"] as? Int && typeNoti.rawValue == dicUserInfo["type"] as? Int
                    }
                    return id == 0  
                })
                
                if let matchExists = matching {
                    UNUserNotificationCenter.current().removeDeliveredNotifications(
                        withIdentifiers: [matchExists.request.identifier]
                    )
                }
        }
        return body
    }
    
    func setupSendRequestFriend(likeFeed: [String: Any], typeNoti: NotificationList) -> String{
     var body = ""
        body = "\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") \(typeNoti.title)"
        
     return body
    }
    
    func setupNotificationGame(likeFeed: [String: Any], typeNoti: NotificationList) -> String{
     var body = ""
        body = "\(typeNoti.title)\((likeFeed["user_from"] as! [AnyHashable: Any])["display_name"] ?? "") "
        
     return body
    }
    
}


enum NotificationList: Int {
    case LikeFeed = 1
    case LikeComment = 2
    case LikeRepCom  = 3
    case ComFeed = 4
    case RepComt = 5
    case LikeDiary = 6
    case LikeComtDiary = 7
    case LikeRepComDiary = 8
    case ComDiary = 9
    case RepComDiary = 10
    case LikeInfo = 11
    case LikeComInfo = 12
    case LikeRepComInfo = 13
    case ComInfo = 14
    case RepcomInfo = 15
    
    case RemindWrite = 16
    
    case LikeSecret = 17
    case LikeComSecret = 18
    case LikeRepComSecret = 19
    case ComSecret = 20
    case RepcomSecret = 21
    
    case SENDREQUESTFRIEND = 22

    case NewFeed = 23
    
    case NotificationNomar = 24
    
    case NOTIFICATIONGAME = 25
    case NewMessage = 27
    
    var title: String{
        switch self {
            
        case .LikeFeed:
            return "đã thích bài viết của bạn"
        case .LikeComment, .LikeComtDiary, .LikeComInfo:
            return "đã thích bình luận của bạn"
        case .LikeRepCom, .LikeRepComDiary, .LikeRepComInfo:
            return "đã thích phản hồi bình luận của bạn trong một bài viết"
        case .ComFeed:
            return  "đã bình luận bài trong bài viết của"
        case .RepComt, .RepComDiary, .RepcomInfo:
            return "đã phản hồi bình luận của"
        case .LikeDiary:
            return "đã thích nhật ký của bạn"
        case .LikeInfo:
            return "đã thích bài thông tin"
        case .ComDiary:
            return "đã bình luận trong nhật ký của "
        case .ComInfo:
            return "đã bình luận trong thông tin "
            
        case .SENDREQUESTFRIEND:
        return " đã gửi lời mời kết bạn"
            
        case .NOTIFICATIONGAME:
        return "Ban đã nhận được 1 ticket từ "
            
        default:
            return ""
        }
    }
}
