//
//  UIFont+Extension.swift
//  Beberia
//
//  Created by haiphan on 25/05/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    enum NotoSansWeight {
        case regular, bold, medium, semiBold
    }
    static func notoSansFont(weight: NotoSansWeight, size: CGFloat) -> UIFont {
        switch weight {
        case .regular:
            return UIFont.init(name: "NotoSans-Regular", size: size) ?? UIFont.systemFont(ofSize: size)
        case .bold:
            return UIFont.init(name: "NotoSans-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
        case .medium:
            return UIFont.init(name: "NotoSans-Medium", size: size) ?? UIFont.systemFont(ofSize: size)
        case .semiBold:
            return UIFont.init(name: "NotoSans-SemiBold", size: size) ?? UIFont.systemFont(ofSize: size)
        }
        
    }
}
