//
//  NSNumberExtension.swift
//  Beberia
//
//  Created by haiphan on 20/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    
    var day: Double {
        return 60 * 60 * 24
    }
    
    var week: Double {
        return 60 * 60 * 24 * 7
    }
    
    func toWeek() -> String {
        var text: String = ""
        let week = Int(self / self.week)
        text += "\(week) Tuần "
        let modo = self.truncatingRemainder(dividingBy: self.week)
        let day = Int(modo / self.day)
        if day > 0 {
            text += "\(day) Ngày"
        }
        return text
    }
    
    //convert second to Hours
  func asString(style: DateComponentsFormatter.UnitsStyle) -> String {
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.minute, .second, .nanosecond]
    formatter.unitsStyle = style
    guard let formattedString = formatter.string(from: self) else { return "" }
    return formattedString
  }
    
    func getTextFromSecond() -> String {
        
        switch self {
        //10s
        case let x where x < 10:
            return "00:0\(x)"
            
        // less one minute
        case let x where x >= 10 && x < 60:
            return "00:\(x)"
            
        //less ten minute
        case let x where x >= 60 && x < 600:
            return "0\(Double(x).asString(style: .positional))"
            
        //less one hours
        case let x where x >= 600 && x < 3600:
            return "\(Double(x).asString(style: .positional))"
            
        //less ten hours
        case let x where x >= 3600 && x < 36000:
            return "0\(Double(x).asString(style: .positional))"

        //greater than ten hours
        case let x where x >= 36000:
            return "\(Double(x).asString(style: .positional))"
        default:
            return ""
        }
        
    }
    
}
