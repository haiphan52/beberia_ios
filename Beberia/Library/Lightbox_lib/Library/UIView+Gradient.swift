import UIKit

extension UIView {

  @discardableResult func addGradientLayer(_ colors: [UIColor]) -> CAGradientLayer {
    guard let gradientLayer = gradientLayer else {
    //    resizeGradientLayer()
     //   return gradientLayer
        let gradient = CAGradientLayer()

       // print(self.bounds)
       // print(self.frame)
        
        gradient.frame = bounds
        gradient.colors = colors.map { $0.cgColor }
        layer.insertSublayer(gradient, at: 0)
        print(gradient.frame)
        return gradient
        
        
    }
    resizeGradientLayer()
    return gradientLayer
    
  }

  func removeGradientLayer() -> CAGradientLayer? {
    gradientLayer?.removeFromSuperlayer()

    return gradientLayer
  }

  func resizeGradientLayer() {
    gradientLayer?.frame = bounds
  }

  fileprivate var gradientLayer: CAGradientLayer? {
    return layer.sublayers?.first as? CAGradientLayer
  }
}
