//
//  CALayerExtension.swift
//  AudioRecord
//
//  Created by haiphan on 13/11/2021.
//

import Foundation
import UIKit

//extension CA
extension CALayer {
    
    struct ShadowModel {
        let color: UIColor
        let aplha: Float
        let x: CGFloat
        let y: CGFloat
        let blur: CGFloat
        let spread: CGFloat
    }
    
    enum ShadowType {
        case normal
        
        func getShadow() -> CALayer.ShadowModel {
            switch self {
            case .normal:
                return CALayer.ShadowModel(color: R.color.shadow()!,
                                           aplha: 1,
                                           x: 0,
                                           y: 2,
                                           blur: 8,
                                           spread: 0)
            }
        }
        
    }
    
    func applySketchShadow(type: CALayer.ShadowType) {
        masksToBounds = false
        shadowColor = type.getShadow().color.cgColor
        shadowOpacity = type.getShadow().aplha
        shadowOffset = CGSize(width: type.getShadow().x, height: type.getShadow().y)
        shadowRadius = type.getShadow().blur / 2.0
        if type.getShadow().spread == 0 {
            shadowPath = nil
        } else {
            let dx = -type.getShadow().spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
    
  func applySketchShadow(
    color: UIColor = R.color.backOpacity30() ?? .black,
    alpha: Float = 0.5,
    x: CGFloat = 0,
    y: CGFloat = 2,
    blur: CGFloat = 4,
    spread: CGFloat = 0)
  {
    masksToBounds = false
    shadowColor = color.cgColor
    shadowOpacity = alpha
    shadowOffset = CGSize(width: x, height: y)
    shadowRadius = blur / 2.0
    if spread == 0 {
      shadowPath = nil
    } else {
      let dx = -spread
      let rect = bounds.insetBy(dx: dx, dy: dx)
      shadowPath = UIBezierPath(rect: rect).cgPath
    }
  }
}
