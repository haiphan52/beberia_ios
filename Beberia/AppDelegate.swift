//
//  AppDelegate.swift
//  Beberia
//
//  Created by IMAC on 8/13/19.
//  Copyright © 2019 IMAC. All rights reserved.
//ßß

import UIKit
import CoreData
import FBSDKLoginKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import FBSDKLoginKit
import RealmSwift
import GoogleSignIn
import Alamofire
import UserNotifications
import Firebase
import FirebaseMessaging
import SwiftKeychainWrapper
import SwiftyJSON
import FBSDKCoreKit
import Siren
import Flurry_iOS_SDK
import ZaloSDK
import RxSwift
import RxCocoa
import FirebaseDynamicLinks
import Kingfisher
import NVActivityIndicatorView
import FFPopup

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate {
    
    var badgeCount = 0
    {
        didSet{
            if badgeCount >= 0{
                UIApplication.shared.applicationIconBadgeNumber = badgeCount
                let defaultsAppGroup = UserDefaults.init(suiteName: UserDefault.KeyEntitlement)
                defaultsAppGroup?.set(badgeCount, forKey: UserDefault.badgeCount)
            }
        }
    }
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var deviceToken: Data?
    var tokennv = ""
    var username = ""
    var password = ""
    var provider = ""
    var task = ""
    var count = 0
    var fetchedListNoti = [Noti]()
    var isCheckConnectSendbird = false
    var listDataAges = [DataVaccineModel]()
    var listDataVacccines = [DataVaccineModel]()
    
    var babyTiemPhong = BabyInfo.init(json: "")
    var babyKhamThai = BabyInfo.init(json: "")
    var isCheckLogin = BehaviorRelay.init(value: false)
    var isCheckGetData = false
    var uuid = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.isCheckGetData = false
        ManageApp.shared.start()
        if let uuid = KeyChain.shared.getKeyChain() {
            self.uuid = uuid
        }
        
        self.moveToSplash()
        
//        self.checkLoginLaunchApp()
        
        LoaddingManager.initLoadding()
        
//        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
//            self.uuid = uuid
//        }
        
//        LightboxConfig.CloseButton.image = UIImage(named: ImageList.Lightbox.closeButton)
//        LightboxConfig.CloseButton.textAttributes = TextAttributes.Lightbox.closeButton
//        LightboxConfig.CloseButton.text = "Finish"
//
//        LightboxConfig.DeleteButton.image = UIImage(named: ImageList.Lightbox.deleteButton)
//        LightboxConfig.DeleteButton.textAttributes = TextAttributes.Lightbox.deleteButton
//        LightboxConfig.DeleteButton.text = "Delete"

//        LightboxConfig.InfoLabel.ellipsisText = "Show more"
        
        FirebaseApp.configure()
        ImageCache.default.memoryStorage.config.totalCostLimit = 1024 * 1024 * 100
        IQKeyboardManager.shared.enable = true
        self.configApplePush(application)
        // get location
        //self.readFileJson()
        
        // Flurry
        Flurry.startSession(LibKey.APIKeysYahoo, with: FlurrySessionBuilder
            .init()
            .withCrashReporting(true)
            .withLogLevel(FlurryLogLevelAll))
        
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = UIColor.black
        
        //  DispatchQueue.global(qos: .background).async {
      
        GIDSignIn.sharedInstance().clientID = LibKey.ClientIDGG
        GIDSignIn.sharedInstance().delegate = self
        
        Messaging.messaging().delegate = self
        
//        
//        SBDMain.initWithApplicationId(LibKey.APPIDSendbird)
        
        ZaloSDK.sharedInstance()?.initialize(withAppId: LibKey.APIKeysZalo)
        
        Siren.shared.wail() // Line 2
        
       //  }
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        print(JailBreakDetector.isJailBroken())
        
        
     //   getUpdateApp()
        
        return ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func moveToSplash() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let vc = SplashVC.createVC()
        let navi: UINavigationController = UINavigationController(rootViewController: vc)
        self.window?.rootViewController = navi
        self.window?.makeKeyAndVisible()
    }
    
    private func checkLoginLaunchApp() {
        // Check Frist Install App
        if !UserDefaults.standard.bool(forKey: UserDefault.KeyFristInstallApp){
            // Frist Install App
            let intro = IntroViewController.init(nib: R.nib.introViewController)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = intro
            self.window?.makeKeyAndVisible()
            
            // Remove Keychain items
            KeychainWrapper.standard.removeObject(forKey: KeychainKeys.isLoggedIn.rawValue)
            KeychainWrapper.standard.removeObject(forKey: KeychainKeys.userName.rawValue)
            KeychainWrapper.standard.removeObject(forKey: KeychainKeys.passWord.rawValue)
            
            UserDefaults.standard.set(true, forKey: UserDefault.KeyFristInstallApp)
        }else{
            if let tabBarVC = R.storyboard.main.tabBarVC() {
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = tabBarVC
                self.window?.makeKeyAndVisible()
                tabBarVC.selectedIndex = 1
            }
        }
    }
    
     func getUpdateApp(){
        
        RemoteConfigFB.share.fetchValueUpdateApp()
        
        RemoteConfigFB.share.getValueUpdateApp = { valueUpdateApp in
            if valueUpdateApp {
                Utils.appStoreVersion { (isVersion, version) in
                    print(version)
                    if !isVersion {
                        
                        print("version \(version)")
                        
                        let popupUpdateApp = PopupUpdateApp.instanceFromNib()
                        
                        popupUpdateApp.frame = CGRect.init(x: 0, y: 0, width: Screen.width - 52, height: 320)
                        let popup = FFPopup.init(contentView: popupUpdateApp)
                        popup.showType = .growIn
                        popup.shouldDismissOnBackgroundTouch = false
                        let layout = FFPopupLayout(horizontal: .center, vertical: .center)
                        popup.show(layout: layout)
                       
                        
                        popupUpdateApp.tapUpdate = {
                            if let url = URL(string: LibKey.linkURLAppStore) {
                                DispatchQueue.main.async {
                                    UIApplication.shared.open(url)
                                }
                            }
                            popup.dismiss(animated: true)
                        }
                        
                    } else {
                        print("chua co version moi")
                    }
                }
            }
        }
         
    }
    
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let handled = ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        if handled {
            return true
        }
        
        return handled
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            guard let urlString = dynamicLink.url?.absoluteString  else {
                return false
            }
            
            print("Dyanmic link url: \(urlString)")
            print("Dynamic link match type: \(dynamicLink.matchType.rawValue)")
            
            guard let bundleInfo = Bundle.main.infoDictionary,
                  let currentVersion = bundleInfo["CFBundleShortVersionString"] as? String
            else{
                print("some thing wrong")
                return false
            }
            
            if let _ = urlString.lastIndex(of: "=") {
                let subString = urlString.components(separatedBy: "=")
                if subString.count > 0{
                    if currentVersion != subString[1]{
                        if let url = URL(string: LibKey.linkURLAppStore) {
                            DispatchQueue.main.async {
                                UIApplication.shared.open(url)
                            }
                        }
                        else{
                        }
                    }
                }
            }
            
            return true
        }//

        
        if ApplicationDelegate.shared.application(app, open: url, options: options) {
            return true
        }
        
        if ZDKApplicationDelegate.sharedInstance()?.application(app, open: url, options: options) == true {
            return true
        }
        
        return true
    }
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        return ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func configApplePush(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
//        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let tokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        Messaging.messaging().token { (token, error) in
                if let error = error {
                    print("Error fetching remote instance ID: \(error.localizedDescription)")
                } else if let token = token {
                    print("=====Token is \(token)")
                }
            }
        print("=====tokenString\(tokenString)")
        
        self.deviceToken = deviceToken
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        
        // check Notification Sendbird Or Notification Serveer
        if let _ = userInfo["sendbird"] as? NSDictionary {
            
        } else {
            badgeCount += 1
            NotificationCenter.default.post(name: NSNotification.Name.updateBagde, object: nil, userInfo: nil)
            
            print(userInfo)
            
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
        }
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        let _ = user.userID
        
    }

    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let url = userActivity.webpageURL,
            let host = url.host else {
                return false
        }

        let isDynamicLinkHandled =
            DynamicLinks.dynamicLinks().handleUniversalLink(url) { dynamicLink, error in

                guard error == nil,
                    let dynamicLink = dynamicLink,
                    let urlString = dynamicLink.url?.absoluteString else {
                        return
                }
                print("Dynamic link minimumAppVersion: \(dynamicLink.minimumAppVersion)")
                print("Dynamic link host: \(host)")
                print("Dyanmic link url: \(urlString)")
                print("Dynamic link match type: \(dynamicLink.matchType.rawValue)")
                
                guard let bundleInfo = Bundle.main.infoDictionary,
                      let currentVersion = bundleInfo["CFBundleShortVersionString"] as? String
                else{
                    print("some thing wrong")
                    return 
                }
                
                if let _ = urlString.lastIndex(of: "=") {
                    let subString = urlString.components(separatedBy: "=")
                    if subString.count > 0{
                        if currentVersion != subString[1]{
                            if let url = URL(string: LibKey.linkURLAppStore) {
                                DispatchQueue.main.async {
                                    UIApplication.shared.open(url)
                                }
                            }
                            else{
                            }
                        }
                    }
                }
            }
        return isDynamicLinkHandled
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        AppEvents.activateApp()
        getUpdateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Beberia")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func handleClickNotification(channelUrl: String){
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
//            SendBirdManager.share.joinChannelbyChanelUrl(listChanels: [channelUrl], completion: { string in
//                print(string)
//            })
            
//            SendBirdManager.share.joinChanelSuccess  =  { (groupChannel) in
//                let chatVC = ListMessageViewController.init(nib: R.nib.listMessageViewController)
//                chatVC.groupChannel = groupChannel
//                chatVC.hidesBottomBarWhenPushed = true
//                Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
//            }
//        }
    }
    
    
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        badgeCount += 1
        NotificationCenter.default.post(name: NSNotification.Name.updateBagde, object: nil, userInfo: nil)
        let defaultsAppGroup = UserDefaults.init(suiteName: UserDefault.KeyEntitlement)
        defaultsAppGroup?.set(badgeCount, forKey: UserDefault.badgeCount)
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        // notification sendbird
        if let payload = userInfo["sendbird"] as? NSDictionary {
            
            let channel = payload["channel"] as! [String:Any]
            if let channelUrl = channel["channel_url"] as? String {
                
                Utils.saveStringToUserDefault(name: UserDefault.KeyNotificationSendBird, value: channelUrl)
                
                guard let viewVC = Utils.getTopMostViewController() , viewVC.isKind(of: BaseViewController.self) else {
                    return
                }
                
                Utils.saveStringToUserDefault(name: UserDefault.KeyNotificationSendBird, value: UserDefault.KeyNotificationEndSendBird)
                self.handleClickNotification(channelUrl: channelUrl)
                
                return
            }
            return
        }
        
        // notification server
        Utils.saveStringToUserDefault(name: UserDefault.KeyNotification, value: userInfo["data"] as! String)
        
        badgeCount -= 1
        
        let noti = Noti.init(json: JSON.init(parseJSON: userInfo["data"] as! String))
        fetchedListNoti = [noti]
        let listNoti = NotificationList(rawValue: fetchedListNoti[0].type ?? 0)
        
        guard let viewVC = Utils.getTopMostViewController() , viewVC.isKind(of: BaseViewController.self) else {
            return
        }
        
        Utils.saveStringToUserDefault(name: UserDefault.KeyNotification, value: UserDefault.KeyNotificationEnd)
        self.handelClickNotification(noti: listNoti, notification: fetchedListNoti[0], viewVC: viewVC as! BaseViewController)
        
    }
    
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        messaging.token { [weak self] token, error in
            guard let wSelf = self  else { return }
            print("=====Firebase registration token: \(token ?? "")")
            wSelf.tokennv = token ?? ""
            DispatchQueue.main.asyncAfter(deadline: .now() + ConstantApp.shared.timeSplashScreen) {
                if ManageApp.shared.hasInstall {
                    wSelf.checkLogin()
                }
            }
        }
       
        
        //       let loginVC = Utils.getTopMostViewController()
        //       if loginVC?.isKind(of: LoginSocialViewController.self) ?? false {
        //            (loginVC as! LoginSocialViewController).checkLogin()
        //        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveMessage remoteMessage: MessagingDelegate) {
        print("Received data message: \(remoteMessage.description)")
    }
}

//MARK: Handle Login

extension AppDelegate {
    
    func checkLogin(){
        
        let isLoggedIn = KeychainWrapper.standard.bool(forKey: KeychainKeys.isLoggedIn.rawValue) ?? false
        
        if isLoggedIn {
            
            if UserDefault.userIDSocial != ""{
                self.loginSocial(provider_id: UserDefault.userIDSocial)
            } else {
                if KeychainWrapper.standard.string(forKey: KeychainKeys.passWord.rawValue) ?? "" != "" {
                    
                    guard let userName = KeychainWrapper.standard.string(forKey: KeychainKeys.userName.rawValue) else { return }
                    guard let passWord = KeychainWrapper.standard.string(forKey: KeychainKeys.passWord.rawValue) else { return }
                    
                    self.loginWithAccount(userName: userName, pass: passWord)
                }else{
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
                        if let tabBarController = self.window?.rootViewController as? UITabBarController {
                            tabBarController.selectedIndex = 1
                        }
                    }
                }
            }
            
        }else{
            
            guard let tabBarVC = R.storyboard.main.tabBarVC() else {
                return
            }
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = tabBarVC
            self.window?.makeKeyAndVisible()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
                if let tabBarController = self.window?.rootViewController as? UITabBarController {
                    tabBarController.selectedIndex = 1
                }
            }
        }
    }
    
    func loginSocial(provider_id:String, isUpdateLogin: Bool = true) {
        
        APIManager.loginSocial(provider_id: provider_id,
                               fcm_token: Utils.getAppDelegate().tokennv,
                               providerType: AppSettings.loginType.rawValue,
                               callbackSuccess: { [weak self] (isSuccess, user) in
            guard let self = self else { return }
            
            UserDefault.userIDSocial = provider_id
            self.handelLoginSuccess(userID: user.id ?? 0, isUpdateLogin: isUpdateLogin)
            
            
        }) { (error,statusCode, user )  in
            print(error)
            print(statusCode)
            self.isCheckLogin.accept(false)
        }
    }
    
    func loginWithAccount(userName: String, pass: String){
        
        APIManager.login(user: userName, providerType: AppSettings.loginType.rawValue, pass: pass, fcmToken: "", callbackSuccess: { [weak self] (user) in
            guard let self = self else { return }
            self.handelLoginSuccess(userID: user.id ?? 0)
            KeychainWrapper.standard.set(userName, forKey: KeychainKeys.userName.rawValue)
            KeychainWrapper.standard.set(pass, forKey: KeychainKeys.passWord.rawValue)
            
        }) { (error, statusCode,display_name,password ) in
            print(error)
            print(statusCode)
            self.isCheckLogin.accept(false)
        }
    }
    
    func handelLoginSuccess(userID: Int, isUpdateLogin: Bool = true){
        
        //    SendBirdManager.share.connectUserToSendBird(completion: {})
//        SocketIOManager.connectSocket(userID: userID)
//        SendBirdManager.share.getListChannels { (groups) in
//            if Utils.getTopMostViewController()!.isKind(of: ChatViewController.self){
//                print(groups)
//                (Utils.getTopMostViewController() as! ChatViewController).litsChanel.accept(groups)
//            }
//
//        }
        
        let defaultsAppGroup = UserDefaults.init(suiteName: UserDefault.KeyEntitlement)
        defaultsAppGroup?.set(UserInfo.shareUserInfo.id, forKey: UserDefault.userID)
        
        KeychainWrapper.standard.set(true, forKey: KeychainKeys.isLoggedIn.rawValue)
        Utils.getAppDelegate().badgeCount = UserInfo.shareUserInfo.unWatch
        
        if isUpdateLogin {
            isCheckLogin.accept(true)
        }
        
    }
    
}


