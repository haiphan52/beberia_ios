//
//  ViewHeaderMenu.swift
//  Beberia
//
//  Created by IMAC on 6/16/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewHeaderMenu: UIView {
    @IBOutlet weak var nameSectionLable: PaddingLabel!
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewHeaderMenu.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewHeaderMenu {
        return R.nib.viewHeaderMenu.instantiate(withOwner: nil)[0] as! ViewHeaderMenu
    }
}
