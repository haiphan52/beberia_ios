//
//  ViewReplyComment.swift
//  Beberia
//
//  Created by IMAC on 9/11/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit

class ViewReplyComment: UIView {
    
    @IBOutlet weak var titleLikeLable: UILabel!
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var btnCopy: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var icHeart: UIImageView!
    @IBOutlet weak var viewDeleteMessage: UIView!
    
    var didTapReply:()->() = {}
    var didTapCopy:()->() = {}
    var didTapDelete:()->() = {}
    var didTapLike:()->() = {}
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewReplyComment.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewReplyComment {
        return R.nib.viewReplyComment.instantiate(withOwner: nil)[0] as! ViewReplyComment
    }
    
    
    @IBAction func didPressReply(_ sender: Any) {
        didTapReply()
    }
    
    @IBAction func didPressCopy(_ sender: Any) {
        didTapCopy()
    }
    
    @IBAction func didPressDelete(_ sender: Any) {
        didTapDelete()
    }
    
    @IBAction func didPressLike(_ sender: Any) {
        didTapLike()
    }
}
