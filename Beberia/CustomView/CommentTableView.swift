//
//  CommentTableView.swift
//  Beberia
//
//  Created by IMAC on 11/5/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Combine
import RxSwift

class CommentTableView: UITableView {

    var feed = HomeFeed.init(json: "")
    var commentNotification: Comment?
    var isCheckFromNotification = false
    var comments = [Comment]()
    var numberComment : Int = 0
    var pageReply = 1
    var parentId:Int? = nil
    var indexParent:Int = 0
    var nextPage: Int = 1
    var nextPageReply: Int = 1
    fileprivate var heightDictionary: [IndexPath : CGFloat] = [:]
    var isCheckEdit = false
    var indexEdit: Int = 0
    var isCheckCommentOrReply = false
    var idPost = 0
    {
        didSet{
            getComment(page: nextPage)
        }
    }
    var type: Int?
    var userIdTag:[Int]? = nil
    var fromToView = FromToview.Home
    {
        didSet{
            switch fromToView {
            case .Home:
                type = 4
            default:
                type = nil
            }
        }
    }
    let cell : CommentTableViewCell? = nil
    var tapReaply:(String)->() = {_ in}
    var showButtonViewMoreComment:(Bool)->() = {_ in}
    var scrollView : UIScrollView?
    var dismissLoadding = PublishSubject<Bool>()
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: CGRect.zero, style: .plain)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setupUI(){
        self.register(R.nib.commentTableViewCell)
        self.delegate = self
        self.dataSource = self
        self.separatorStyle = .singleLine
        self.allowsSelection = false
        self.rowHeight = UITableView.automaticDimension
        self.tableFooterView = UIView()
    }
    
    @IBAction func didPressCloseViewNotiReply(_ sender: Any) {
        self.parentId = nil
        self.indexParent = 0
        userIdTag = []
    }
    
    func didPressSendComment(content: String, completion:@escaping ()->()) {
        if self.fromToView == FromToview.Home {
            sendCommnetHomeFeed(cell: cell, content: content, completion: {
                completion()
            })
        }else{
            sendComment(content: content, completion: {
                 completion()
            })
        }
    }
    
    @IBAction func didPressLoadMoreComment(_ sender: Any) {
        getComment(page: nextPage)
    }

    
    func sendComment(content: String, completion:@escaping ()->()){

        SVProgressHUD.show()
        APIManager.sendComment(idPost, parentId: parentId, content: content, userTag: userIdTag?[0] ?? 0, callbackSuccess: { [weak self] (comment) in
            guard let self = self else { return }
            if self.parentId != nil{
                self.comments[self.indexParent].reply!.insert(comment, at: 0)
                self.comments[self.indexParent].replyCommentNumber! += 1
                self.comments[self.indexParent].isTapExpand = true
            }else{
                self.comments.insert(comment, at: 0)
                self.numberComment += 1
            }
            NotificationCenter.default.post(name: NSNotification.Name.updateNumberComment, object: [Key.KeyNotification.numberComment : self.numberComment], userInfo: nil)
            self.self.reloadData()
            self.parentId = nil
            self.indexParent = 0
            completion()
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func sendCommnetHomeFeed(cell: CommentTableViewCell?, content: String, completion:@escaping ()->()){

        SVProgressHUD.show()
        
        let type = (parentId == nil) ? Key.TypeComment : Key.TypeReplyComment
        
        APIManager.sendCommentHomeFeed(parentId ?? idPost, type: type, content: content, userTag: userIdTag ?? [], callbackSuccess: { [weak self] (comment) in
            guard let self = self else { return }
            if self.parentId != nil{
                self.comments[self.indexParent].reply!.insert(comment, at: 0)
                self.comments[self.indexParent].replyCommentNumber! += 1
                self.comments[self.indexParent].isTapExpand = true
            }else{
                self.comments.insert(comment, at: 0)
                self.numberComment += 1
            }
            
            NotificationCenter.default.post(name: NSNotification.Name.updateNumberComment, object: [Key.KeyNotification.numberComment : self.numberComment], userInfo: nil)
            
            self.reloadData()
            self.parentId = nil
            self.indexParent = 0
            completion()
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func getComment(page: Int){
    //    SVProgressHUD.show()
        APIManager.getComment(idPost, page: page, type: type, callbackSuccess: { [weak self] (comments, nextPage) in
            guard let self = self else { return }
            self.comments.append(contentsOf: comments)
            self.reloadData()
            if self.commentNotification != nil {
                if self.nextPage == 1{
                    let index = self.comments.firstIndex(where: { (comment) -> Bool in
                        return comment.commentId == self.commentNotification?.commentId
                    })

                    if index != nil{
                        self.nextPage = nextPage

                        let rectOfCellInTableView = self.rectForRow(at: IndexPath.init(row: index!, section: 0))
                        let rectOfCellInSuperview = self.convert(rectOfCellInTableView, to: self.superview)
                        UIView.animate(withDuration: 0.5, animations: {
                            self.scrollView?.contentOffset = CGPoint.init(x: rectOfCellInSuperview.origin.x , y: rectOfCellInSuperview.origin.y + 100)
                        })
                    }else{
                        //Fixbug show name nil
//                        self.comments.insert(self.commentNotification!, at: 0)
                        self.reloadData()
                        self.scroll(to: .top, animated: true)
                    }
                }else{

                }
            }
            
            self.nextPage = nextPage
            self.dismissLoadding.onNext(true)
       //     SVProgressHUD.dismiss()
        }) { (error) in
            self.dismissLoadding.onNext(true)
        //    SVProgressHUD.dismiss()
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func getCommentReplyHomeFeed(index: Int,page: Int, type: Int, completion:@escaping ()->()){
        SVProgressHUD.show()
        APIManager.getComment(self.comments[index].commentId ?? 0, page: page, type: type, callbackSuccess: { [weak self]  (comments, nextPage) in
            guard let self = self else { return }
            self.nextPageReply = nextPage
            if self.comments[index].reply?.count ?? [Comment]().count < 4 {
                self.comments[index].reply?.removeAll()
            }

            self.comments[index].reply?.insert(contentsOf: comments, at: 0)
            self.comments[index].nextPage = nextPage
            completion()
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func getCommentReply(index: Int, page: Int, completion:@escaping ()->()){
        SVProgressHUD.show()
        APIManager.getCommentReply(self.comments[index].commentId ?? 0, page: page, callbackSuccess: { [weak self]  (comments, nextPage) in
            guard let self = self else { return }
            self.nextPageReply = nextPage
            
            if self.comments[index].reply?.count ?? [Comment]().count < 4 {
                self.comments[index].reply?.removeAll()
            }
            self.comments[index].reply?.insert(contentsOf: comments, at: 0)
            self.comments[index].nextPage = nextPage
            completion()
            
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func deleteComment(index: Int, isCheckCommentOrReply: Bool, type: Int?, indexParentComment: Int?, cell : CommentTableViewCell){
        SVProgressHUD.show()
        let idDelete = isCheckCommentOrReply ? self.comments[index].commentId ?? 0 : self.comments[indexParentComment!].reply![index].commentId ?? 0
        APIManager.deleteComment(idDelete,type: type, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else { return }
            if isSuccess {
                if !isCheckCommentOrReply{
                    var numberRep = self.comments[indexParentComment!].replyCommentNumber ?? 0
                    numberRep -= 1
                    self.comments[indexParentComment!].reply?.remove(at: index)
                    self.comments[indexParentComment!].replyCommentNumber = numberRep
                    
                }else{
                    self.comments.remove(at: index)
                    self.numberComment -= 1
                 //   self.lblNumberComment.text = "\(self.numberComment) \(R.string.localizable.infomationComment())"
                }
                NotificationCenter.default.post(name: NSNotification.Name.updateNumberComment, object: [Key.KeyNotification.numberComment : self.numberComment], userInfo: nil)
                
                self.reloadData()
            }
            
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func editComment(index: Int, isCheckCommentOrReply: Bool, content: String, indexParentComment: Int? , type: Int?, cell : CommentTableViewCell, completion:@escaping ()->()){
        SVProgressHUD.show()
        let idEdit = isCheckCommentOrReply ? self.comments[index].commentId ?? 0 : self.comments[indexParentComment!].reply![index].commentId ?? 0
        APIManager.editComment(idEdit ,content, type: type ,callbackSuccess: { [weak self] (comment) in
            // add lại data mới khi edit thành công
            guard let self = self else { return }
            
            if !isCheckCommentOrReply{
                self.comments[indexParentComment!].reply![index].content = comment.content
            }else{
                self.comments[index].content = comment.content
            }
            // update UI
            self.isCheckEdit = false
            
            if type == Key.TypeReplyComment || !isCheckCommentOrReply {
                guard let cell1 = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                    return
                }
                cell1.tbvComment.reloadData()
                cell1.layoutIfNeeded()
                cell1.tbvComment.beginUpdates()
                cell1.tbvComment.endUpdates()
                completion()
            }else{
                self.reloadData()
            }
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func pushProfileVC(user: User){
            let ProfileVC = R.storyboard.main.profileViewController()
            ProfileVC?.hidesBottomBarWhenPushed = true
            ProfileVC?.check = 1
            ProfileVC?.user = user
            Utils.getTopMostViewController()?.navigationController?.pushViewController(ProfileVC!, animated: true)
    }
    
    func handelUpdateLayoutEditComment(cell: CommentTableViewCell, index: Int){
        cell.viewNumberLike.isHidden = self.isCheckEdit ? true : false
        cell.viewNumberReply.isHidden = self.isCheckEdit ? true : false
        cell.lblContent.isHidden = self.isCheckEdit ? true : false
        cell.btnSavedEdit.isHidden = self.isCheckEdit ? false : true
        cell.btnCancelEdit.isHidden = self.isCheckEdit ? false : true
        cell.txtContent.isHidden = self.isCheckEdit ? false : true
        cell.heightTextViewContent.isActive = self.isCheckEdit ? true : false
        cell.heightTextViewContent.constant = self.isCheckEdit ? max(cell.lblContent.frame.height, 66) : 0

        self.layoutIfNeeded()
        self.beginUpdates()
        self.endUpdates()
    }
    
    func showAtionDeleteEdit(index: Int,isCheckCommentOrReply: Bool, cell: CommentTableViewCell, type :Int?, indexParentComment: Int?){
        let bottomVC = BaseBottomViewController.init(nib: R.nib.baseBottomViewController)
        bottomVC.selectedOption = { indexSelect in
            
            switch indexSelect {
            case 0:
                 self.isCheckEdit = true
                self.indexEdit = index
                self.indexParent = indexParentComment ?? 0
                
                if type == Key.TypeReplyComment || !isCheckCommentOrReply{
                    
                    guard let cell1 = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                        return
                    }
             
                    self.handelUpdateLayoutEditComment(cell: cell1, index: self.indexParent)
                    
                    cell1.tbvComment.layoutIfNeeded()
                    cell1.tbvComment.beginUpdates()
                    cell1.tbvComment.endUpdates()
                    
                    cell.tbvComment.layoutIfNeeded()
                    cell.tbvComment.beginUpdates()
                    cell.tbvComment.endUpdates()
                    
                    self.layoutIfNeeded()
                    self.beginUpdates()
                    self.endUpdates()

                   
                }else {
                    self.handelUpdateLayoutEditComment(cell: cell, index: index)
                }
                
            case 1:
                Utils.getTopMostViewController()?.dismiss(animated: true, completion: {
                    Utils.showAlertOKWithAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: "Bạn có chắc chắn xoá bình luận ?", cancel: true) {
                        self.deleteComment(index: index, isCheckCommentOrReply: isCheckCommentOrReply, type: type, indexParentComment: indexParentComment, cell: cell)
                    }
                })
                
                
            default:
                print("123")
            }
            Utils.getTopMostViewController()?.dismiss(animated: true, completion: {
                self.parentId = nil
                self.indexParent = 0
                switch self.fromToView {
                case .Home:
                    self.type = 4
                default:
                    self.type = nil
                }
            })
        }
        bottomVC.datasource = [R.string.localizable.infomationEditComment(), R.string.localizable.homeDelete()]
        bottomVC.height = CGFloat((bottomVC.datasource.count + 1) * 44)
        bottomVC.topCornerRadius = 20
        bottomVC.presentDuration = 0.25
        bottomVC.dismissDuration = 0.25
        Utils.getTopMostViewController()?.present(bottomVC, animated: true, completion: nil)
    }
    
    func likeCommentInfomation(_ index :Int,_ type: Int, _ cell: CommentTableViewCell, idReply: Int, indexParent: Int){
        cell.btnLike.isEnabled = false
        let id = (idReply == Key.IndexException) ? self.comments[index].commentId ?? 0 : idReply
        APIManager.likePost(idPost: id, type: type, callbackSuccess: { [weak self] (numberLike, checkLike) in
            guard let self = self else { return }
            if idReply != Key.IndexException{
                guard let cell = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                    return
                }
                cell.isLike = checkLike
                cell.numberLike = numberLike
                cell.btnLike.isEnabled = true
                cell.lblNumberLike.text = "\(cell.numberLike)"
                self.comments[indexParent].reply?[index].isLiked = checkLike
                self.comments[indexParent].reply?[index].likeNumber = numberLike
            }else{
                cell.isLike = checkLike
                cell.numberLike = numberLike
                cell.btnLike.isEnabled = true
                cell.lblNumberLike.text = "\(cell.numberLike)"
                self.comments[index].isLiked = checkLike
                self.comments[index].likeNumber = numberLike
            }
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func likeCommentHomeFeed(_ index :Int,_ cell: CommentTableViewCell, type :Int, idReply: Int, indexParent: Int){
        cell.btnLike.isEnabled = false
        let id = (idReply == Key.IndexException) ? self.comments[index].commentId ?? 0 : idReply
        let checkLike = (idReply == Key.IndexException) ? self.comments[index].isLiked ?? 0 : self.comments[indexParent].reply?[index].isLiked
        APIManager.likeHomeFeed(id: id, type: type, checkLike: checkLike ?? 0, callbackSuccess: { [weak self] (numberLike, checkLike) in
            guard let self = self else { return }
            if type == Key.TypeLikeReplyComment{
                guard let cell = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                    return
                }
                cell.isLike = checkLike
                cell.numberLike = numberLike
                cell.btnLike.isEnabled = true
                cell.lblNumberLike.text = "\(cell.numberLike)"
                self.comments[indexParent].reply?[index].isLiked = checkLike
                self.comments[indexParent].reply?[index].likeNumber = numberLike
            }else{
                cell.isLike = checkLike
                cell.numberLike = numberLike
                cell.btnLike.isEnabled = true
                cell.lblNumberLike.text = "\(cell.numberLike)"
                self.comments[index].isLiked = checkLike
                self.comments[index].likeNumber = numberLike
            }
            
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }

    func findIndexReplyComment(_ id :Int)-> Int{
        if id != Key.IndexException {
            for (index ,comment) in self.comments.enumerated(){
                if comment.reply?.count ?? [Comment]().count > 0 {
                    for replyComment in comment.reply!{
                        if replyComment.commentId! == id{
                            return index
                        }
                    }
                }
            }
        }
        return Key.IndexException
    }
    
    @IBAction func didPressViewPost(_ sender: Any) {
        if self.fromToView == FromToview.Home {

        }else{

        }
    }
   
    func reloadTableView(cell: CommentTableViewCell, index: Int){
        self.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        cell.layoutIfNeeded()
        cell.tbvComment.beginUpdates()
        cell.tbvComment.endUpdates()
        cell.tbvCommenReply.beginUpdates()
        cell.tbvCommenReply.endUpdates()
        self.layoutIfNeeded()
        self.beginUpdates()
        self.endUpdates()
    }
}

extension CommentTableView: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.commentTableViewCell, for: indexPath)!
        cell.viewNumberLike.isHidden = false
        cell.viewNumberReply.isHidden = false
        cell.lblContent.isHidden = false
        cell.viewLine.isHidden = true
        cell.btnSavedEdit.isHidden = true
        cell.btnCancelEdit.isHidden = true
        cell.txtContent.isHidden = true
        cell.heightTextViewContent.isActive = false
        cell.contentView.backgroundColor = .white
        cell.viewBGReplyComment.backgroundColor  = UIColor.init(hexString: AppColor.colorReplyCommentBG, alpha: 0.14)
        cell.btnShowReplyComment.tag = indexPath.row
        cell.buttonLoadMore.tag = indexPath.row
        cell.setupData(comment: self.comments[indexPath.row], nextPage: self.comments[indexPath.row].nextPage ?? 100, isReply: false)
        cell.comments = self.comments
        cell.btnLike.tag = indexPath.row
        cell.buttonLoadMore.isHidden = true
        cell.heightButtonLoadMore.constant = 0
        
        if self.nextPage > 0 {
            if indexPath.row == self.comments.count - 1 {
                cell.buttonLoadMore.isHidden = false
                cell.heightButtonLoadMore.constant = 29
            }
        }
        
        self.showButtonViewMoreComment(false)
        if indexPath.row == self.comments.count - 1 {
            self.showButtonViewMoreComment(nextPage != 0)
        }
        
        cell.tapOption = { [weak self] index, idReply, type  in
            guard let self = self else { return }
            if self.fromToView == FromToview.Home {
                if type == Key.TypeComment{
                    self.showAtionDeleteEdit(index: index,isCheckCommentOrReply: true, cell: cell, type: Key.TypeComment, indexParentComment: nil)
                } else {
                    self.showAtionDeleteEdit(index: index,isCheckCommentOrReply: false, cell: cell, type: Key.TypeReplyComment, indexParentComment: self.findIndexReplyComment(idReply ?? Key.IndexException))
                }
            }else{
                // dựa vào có indexReplyComment ko để check nó là comment hay replr comment
                let indexReplyCM = self.findIndexReplyComment(idReply ?? Key.IndexException)
                self.showAtionDeleteEdit(index: index,isCheckCommentOrReply: indexReplyCM == Key.IndexException ? true : false, cell: cell, type: nil, indexParentComment: indexReplyCM)
            }
        }
        
        cell.tapShowReply = { [weak self] index in
            guard let self = self else { return }
            self.comments[index].isTapExpand = true
            self.reloadTableView(cell: cell, index: index)
        }
        
        cell.tapReply = { [weak self] index in
            guard let self = self else { return }
            self.didPressCloseViewNotiReply(UIButton.self)
            self.parentId = self.comments[index].commentId
            self.indexParent = index
            self.userIdTag = [self.comments[index].user?.id ?? 0]
            
            // check Secret
            if self.comments[index].secretNumber ?? 0 > 0{
                self.tapReaply(cell.lblName.text ?? "")
            }else{
                self.tapReaply(self.comments[index].user?.displayName ?? "")
            }

        }
        
        cell.tapReplyComment = { [weak self] userID, nameUser, idComment ,numberSecret in
            guard let self = self else { return }
            self.didPressCloseViewNotiReply(UIButton.self)
            var isCheckSecret = false
            var commentTemp: Comment? = nil
            for (index ,comment) in self.comments.enumerated(){
                if comment.reply?.count ?? [Comment]().count > 0 {
                    for replyComment in comment.reply!{
                        if replyComment.commentId! == idComment{
                            commentTemp = comment
                            self.indexParent = index
                        }
                    }
                }
                
                isCheckSecret = comment.secretNumber ?? 0 > 0
            }
            if commentTemp != nil{
                self.parentId = commentTemp!.commentId
            }
            self.userIdTag = [userID]
            
            if !isCheckSecret{
                self.tapReaply(nameUser)
            }else{
                self.tapReaply("Ẩn danh \(numberSecret)")
            }
        }
        
        cell.tapLike = { [weak self] index, idReply, type in
            guard let self = self else { return }
            if self.fromToView == FromToview.Home {
                
                self.likeCommentHomeFeed(index, cell, type: type, idReply: idReply ?? Key.IndexException, indexParent: self.findIndexReplyComment(idReply ?? Key.IndexException))
            }else{
                self.likeCommentInfomation(index, type, cell, idReply: idReply ?? Key.IndexException, indexParent: self.findIndexReplyComment(idReply ?? Key.IndexException))
            }
        }
        
        cell.tapShowMoreReplyComment = { [weak self] index,_ ,type in
            guard let self = self else { return }
            if type == Key.TypeComment {
                self.getComment(page: self.nextPage)
            }else {
                if self.fromToView == FromToview.Home {
                    self.getCommentReplyHomeFeed(index: indexPath.row, page: self.nextPageReply, type: type, completion: {
                        self.reloadTableView(cell: cell, index: indexPath.row)
                    })
                }else{

                    self.getCommentReply(index: indexPath.row, page: self.nextPageReply, completion: {
                        self.reloadTableView(cell: cell, index: indexPath.row)
                    })
                }
            }
            
        }
        
        cell.tapCancelEdit = { [weak self] index, idReply, type in
            guard let self = self else { return }
            self.isCheckEdit = false
            if type == Key.TypeReplyComment{
                guard let cell1 = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                    return
                }
                self.reloadRows(at: [IndexPath.init(row: self.findIndexReplyComment(idReply ?? Key.IndexException), section: 0)], with: .none)
                self.handelUpdateLayoutEditComment(cell: cell1, index: index)
            }else{
                cell.txtContent.text = cell.lblContent.text
                self.handelUpdateLayoutEditComment(cell: cell, index: index)
            }
        }
        
        cell.tapSavedEdit = { [weak self] index, idReply, type in
            guard let self = self else { return }
            if self.fromToView == FromToview.Home {
                if type == Key.TypeComment{
                    self.editComment(index: index, isCheckCommentOrReply: true, content: cell.txtContent.text, indexParentComment: self.findIndexReplyComment(idReply ?? Key.IndexException), type: type, cell: cell, completion: {
                        
                    })
                } else {
                    guard let cell1 = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                        return
                    }
                    self.editComment(index: index, isCheckCommentOrReply: false, content: cell1.txtContent.text, indexParentComment: self.findIndexReplyComment(idReply ?? Key.IndexException), type: type, cell: cell, completion: {
                        self.reloadTableView(cell: cell, index: self.findIndexReplyComment(idReply ?? Key.IndexException))
                    })
                }
            }else{
                
                let indexReplyCM = self.findIndexReplyComment(idReply ?? Key.IndexException)
                if indexReplyCM == Key.IndexException {
                    self.editComment(index: index, isCheckCommentOrReply: true, content: cell.txtContent.text, indexParentComment: indexReplyCM, type: nil, cell: cell, completion: {
                        
                    })
                }else{
                    guard let cell1 = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                        return
                    }
                    self.editComment(index: index, isCheckCommentOrReply: false, content: cell1.txtContent.text, indexParentComment: indexReplyCM, type: nil, cell: cell, completion: {
                        self.reloadTableView(cell: cell, index: self.findIndexReplyComment(idReply ?? Key.IndexException))
                    })
                }
            }
        }
        
        cell.tapUser = { [weak self] index, user, type in
            guard let self = self else { return }
            var user = user
            if type == Key.TypeComment{
                user = self.comments[index].user
            }
            print(user?.id as Any)
            self.pushProfileVC(user: user!)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return height ?? UITableView.automaticDimension
    }
}

