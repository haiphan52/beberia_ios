//
//  ExperienceView.swift
//  Beberia
//
//  Created by haiphan on 10/08/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class ExperienceView: UIView, SetUpFunctionBase {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var heightGiftImage: NSLayoutConstraint!
    @IBOutlet weak var widthGiftImage: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
    
}
extension ExperienceView {
    
    func setupUI() {
        self.img.image = self.img.image?.resizeImageSpace(width: self.img.frame.width)
        let width = self.img.image?.resizeImageSpace(width: self.img.frame.width)?.size.width
        let height = self.img.image?.resizeImageSpace(width: self.img.frame.width)?.size.height
        self.widthGiftImage.constant = width ?? 100
        self.heightGiftImage.constant = height ?? 100
        self.img.image = UIImage.gifImageWithName("gift_image_experience")
    }
    
    func setupRX() {
        
    }
    
}
