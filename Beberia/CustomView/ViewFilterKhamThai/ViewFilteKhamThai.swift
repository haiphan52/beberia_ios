//
//  ViewFilteKhamThai.swift
//  Beberia
//
//  Created by IMAC on 6/17/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewFilteKhamThai: UIView {
    
    @IBOutlet weak var imgDaKham: UIImageView!
    @IBOutlet weak var imgChuaKham: UIImageView!
    var filterKhamThai: (Int)->() = {_ in}
    var selectedStatus = 2

  override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewFilteKhamThai.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewFilteKhamThai {
        return R.nib.viewFilteKhamThai.instantiate(withOwner: nil)[0] as! ViewFilteKhamThai
    }

    @IBAction func didPressOK(_ sender: Any) {
        filterKhamThai(selectedStatus)
    }
    
    @IBAction func didPressFilter(_ sender: UIButton) {
        print(sender.tag)
        
        imgChuaKham.image = sender.tag == 25 ? R.image.ic_mark()! : R.image.ic_unMark()!
        imgDaKham.image = sender.tag == 26 ? R.image.ic_mark()! : R.image.ic_unMark()!
        selectedStatus = sender.tag == 25 ? 0 : 1
    }
}
