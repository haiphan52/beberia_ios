//
//  ViewInfoBaby.swift
//  Beberia
//
//  Created by IMAC on 10/8/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewInfoBaby: UIView {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var weightLable: UILabel!
    @IBOutlet weak var nameBaby: UILabel!
    @IBOutlet weak var heightBaby: UILabel!
    @IBOutlet weak var ageBaby: UILabel!
    @IBOutlet weak var dateLable: UILabel!
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
           guard subviews.isEmpty else { return self }
           return R.nib.viewInfoBaby.firstView(owner: nil)
       }
       
       class func instanceFromNib() -> ViewInfoBaby {
           return R.nib.viewInfoBaby.instantiate(withOwner: nil)[0] as! ViewInfoBaby
       }
    
    override func awakeFromNib() {
        weightLable.fitFontForSize()
        nameBaby.fitFontForSize()
        heightBaby.fitFontForSize()
        ageBaby.fitFontForSize()
        dateLable.fitFontForSize()
    }

}

extension UILabel {

    func fitFontForSize(minFontSize : CGFloat = 1.0, maxFontSize : CGFloat = 300.0, accuracy : CGFloat = 1.0) {
        var minFontSize = minFontSize
        var maxFontSize = maxFontSize
        assert(maxFontSize > minFontSize)
        layoutIfNeeded() // Can be removed at your own discretion
        let constrainedSize = bounds.size
        while maxFontSize - minFontSize > accuracy {
            let midFontSize : CGFloat = ((minFontSize + maxFontSize) / 2)
            font = font.withSize(midFontSize)
            sizeToFit()
            let checkSize : CGSize = bounds.size
            if  checkSize.height < constrainedSize.height && checkSize.width < constrainedSize.width {
                minFontSize = midFontSize
            } else {
                maxFontSize = midFontSize
            }
        }
        font = font.withSize(minFontSize)
        sizeToFit()
        layoutIfNeeded() // Can be removed at your own discretion
    }

}
