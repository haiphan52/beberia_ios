//
//  ViewChangeEASY.swift
//  Beberia
//
//  Created by IMAC on 7/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewChangeEASY: UIView {

    @IBOutlet weak var btnChangeEASY: UIButton!
    var changeEASY:()->() = {}
 
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
           guard subviews.isEmpty else { return self }
           return R.nib.viewChangeEASY.firstView(owner: nil)
       }
       
       class func instanceFromNib() -> ViewChangeEASY {
           return R.nib.viewChangeEASY.instantiate(withOwner: nil)[0] as! ViewChangeEASY
       }
    
    @IBAction func didPressChangeEASY(_ sender: Any) {
        changeEASY()
    }
}
