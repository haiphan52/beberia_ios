//
//  ViewSelectedBaby.swift
//  Beberia
//
//  Created by IMAC on 6/16/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewSelectedBaby: UIView {

    @IBOutlet weak var selectBabyLable: UILabel!
    @IBOutlet weak var tbvSelectedBaby: UITableView!
    @IBOutlet weak var viewTitle: UIView!
    
    var didSelectedBaby:(BabyInfo)->() = {_ in}
    var disposeBag = DisposeBag()
    var babys = UserInfo.shareUserInfo.baby_info
    var babies = BehaviorRelay.init(value: [BabyInfo]())
    var isCheckBaby = true {
        didSet {
            print(isCheckBaby)
            babys = babys.filter { (baby) -> Bool in
                return isCheckBaby ? baby.ageRange != 0 && baby.ageRange != 6 : baby.ageRange == 0
            }
            customData()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewSelectedBaby.firstView(owner: nil)
    }
    
    class func instanceFromNib(isCheckBaby: Bool) -> ViewSelectedBaby {
        guard let view = R.nib.viewSelectedBaby.instantiate(withOwner: nil)[0] as? ViewSelectedBaby else {
            fatalError("Attempted to create TestView, failed to find object")
        }
        view.isCheckBaby = isCheckBaby
        return view
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customize()
    }
    
    func customize(){

        
    }
    
    override func awakeFromNib() {
        initRx()
    }
    
    func customData(){
        let addBaby = BabyInfo.init(json: "")
        addBaby.nickname = "Thêm bé"
        addBaby.id = 0
        babys.append(addBaby)
        babies.accept(babys)
    }
    
    func initRx(){
        tbvSelectedBaby.register(R.nib.selectedTableViewCell)
        tbvSelectedBaby.estimatedRowHeight = 50
        
        babies.asObservable().observeOn(MainScheduler.asyncInstance)
        .bind(to: tbvSelectedBaby.rx.items(cellIdentifier: R.reuseIdentifier.selectedTableViewCell.identifier, cellType: SelectedTableViewCell.self)) { [weak self]  row, data, cell in
            guard let self = self else { return }
            cell.nameLable.text = data.nickname ?? ""
            cell.imgAvatar.kf.setImage(with: URL.init(string: data.avatar ?? ""), placeholder: R.image.placeholder())
            cell.imgAvatar.isHidden = false
            cell.imgAddBaby.isHidden = true
            if row == self.babies.value.count - 1 {
                cell.imgAvatar.isHidden = true
                cell.imgAddBaby.isHidden = false
            }
        }.disposed(by: disposeBag)
        
        
        Observable.zip(tbvSelectedBaby.rx.itemSelected, tbvSelectedBaby.rx.modelSelected(BabyInfo.self))
        .bind { [weak self] indexPath, item in
            guard let self = self else { return }
            
            self.didSelectedBaby(item)
        }
        .disposed(by: disposeBag)
    }
    
    
}
