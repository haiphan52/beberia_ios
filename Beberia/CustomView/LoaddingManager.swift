//
//  LoaddingManager.swift
//  Beberia
//
//  Created by Lap on 12/8/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class LoaddingManager {
   // static let shareLoadding = LoaddingManager.init()
  //  static var nVActivityIndicatorView: NVActivityIndicatorView!
    static var activityIndicator = UIActivityIndicatorView()
    
    static func initLoadding(view: UIView = UIView.init()){
//        nVActivityIndicatorView = NVActivityIndicatorView(frame: view.frame, type: .lineSpinFadeLoader, color: UIColor.init(hexString: "fff197"), padding: view.frame.size.width / 2.2)
//        view.addSubview(nVActivityIndicatorView)
        
        let window = UIApplication.shared.keyWindow!
        if #available(iOS 13.0, *) {
            activityIndicator = UIActivityIndicatorView.init(style: .large)
          //  activityIndicator.style = .large
        } else {
            activityIndicator = UIActivityIndicatorView.init(style: .gray)
        //    activityIndicator.style = .gray
            // Fallback on earlier versions
        }
        activityIndicator.center = window.center
        activityIndicator.hidesWhenStopped = true
        window.addSubview(activityIndicator)
        window.bringSubviewToFront(activityIndicator)
    }
    
    static func startLoadding(){
  //      nVActivityIndicatorView.startAnimating()
        activityIndicator.startAnimating()
    }
    
    static func stopLoadding(){
  //      nVActivityIndicatorView.stopAnimating()
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
}
