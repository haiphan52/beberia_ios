//
//  ViewToastPopup.swift
//  Beberia
//
//  Created by IMAC on 7/2/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewToastPopup: UIView {

    @IBOutlet weak var titleLable: UILabel!
   
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewToastPopup.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewToastPopup {
        return R.nib.viewToastPopup.instantiate(withOwner: nil)[0] as! ViewToastPopup
    }

}
