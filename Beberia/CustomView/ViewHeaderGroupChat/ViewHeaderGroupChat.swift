//
//  ViewHeaderGroupChat.swift
//  Beberia
//
//  Created by IMAC on 6/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewHeaderGroupChat: UIView {
    
    @IBOutlet weak var lastTimeLble: UILabel!
    @IBOutlet weak var nameGroupChatLable: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgAvatarUser: UIImageView!
    
    var changeImageCover:()->() = {}
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewHeaderGroupChat.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewHeaderGroupChat {
        return R.nib.viewHeaderGroupChat.instantiate(withOwner: nil)[0] as! ViewHeaderGroupChat
    }
    
    @IBAction func didPressImageCover(_ sender: Any) {
        changeImageCover()
    }
}
