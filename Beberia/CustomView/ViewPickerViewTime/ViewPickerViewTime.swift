//
//  ViewPickerViewTime.swift
//  Beberia
//
//  Created by IMAC on 6/18/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewPickerViewTime: UIView {
    
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var btnExit: UIButton!
    
    var tapExit:()->() = {}
    var tapOK:(String)->() = {_ in}
    var time: String?
    
   override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        
        datePickerView.rx.value.subscribe { (value) in
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            self.time = formatter.string(from: value.element!)
        }.disposed(by: disposeBag)
        
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewPickerViewTime.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewPickerViewTime {
        return R.nib.viewPickerViewTime.instantiate(withOwner: nil)[0] as! ViewPickerViewTime
    }
    
    @IBAction func didPressOK(_ sender: Any) {
        tapOK(time ?? "")
    }
    
    @IBAction func didPressExit(_ sender: Any) {
        tapExit()
    }
}
