//
//  ViewSelectedTopicHome.swift
//  Beberia
//
//  Created by IMAC on 7/16/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import TTGTagCollectionView

class ViewSelectedTopicHome: UIView {
    
    let kCONTENT_XIB_NAME = "ViewSelectedTopicHome"
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var stackViewTagViewBeME: UIStackView!
    @IBOutlet weak var imgIcAll: UIImageView!
    @IBOutlet weak var imgIcMe: UIImageView!
    @IBOutlet weak var imgIcBe: UIImageView!
    @IBOutlet weak var imgIcInfo: UIImageView!
    @IBOutlet weak var imgIcQA: UIImageView!
    @IBOutlet weak var imgIcSale: UIImageView!
//    @IBOutlet weak var imgBGAll: UIImageView!
//    @IBOutlet weak var imgBGMe: UIImageView!
//    @IBOutlet weak var imgBGBe: UIImageView!
//    @IBOutlet weak var imgBGInfo: UIImageView!
//    @IBOutlet weak var imgBGQA: UIImageView!
//    @IBOutlet weak var imgBGSale: UIImageView!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblQA: UILabel!
    @IBOutlet weak var lblSale: UILabel!
    @IBOutlet weak var lblAll: UILabel!
    @IBOutlet weak var tagViewBe: TTGTextTagCollectionView!
    @IBOutlet weak var tagViewMe: TTGTextTagCollectionView!
    @IBOutlet weak var viewAll: UIView!
    @IBOutlet weak var viewMe: UIView!
    @IBOutlet weak var viewBe: UIView!
    @IBOutlet weak var lblMe: UILabel!
    @IBOutlet weak var lblBe: UILabel!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var viewQA: UIView!
    @IBOutlet weak var viewSale: UIView!
    
    var about = 0
    var category = 0
    var ageRange = 7
    
    var selectedTopic:(Int,Int,Int)->() = {_,_,_ in}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
    }
    
    override func awakeFromNib() {
            setupAlphaBtnClick(viewAlpa: viewAll, label: lblAll, imgIC: imgIcAll)
            setupTagView()
    }
    
    func setupTagView(data: [String], tagView: TTGTextTagCollectionView){
        tagView.addTags(data)
        let config = tagView.defaultConfig!
        config.textColor = UIColor.init(hexString: AppColor.colorSelect,alpha: 0.3)
        config.selectedTextColor = UIColor.init(hexString: AppColor.colorYellow)
        config.backgroundColor = UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.4)
        config.selectedBackgroundColor = UIColor.init(hexString: AppColor.colorBGMenu)
        config.borderColor = UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.5)
        config.selectedBorderColor = UIColor.init(hexString: AppColor.colorYellow)
        config.borderWidth = 1
        config.selectedBorderWidth = 1
        config.cornerRadius = 15
        config.selectedCornerRadius = 15
        config.shadowOffset = CGSize.zero
        config.shadowRadius = 0
        config.shadowOpacity = 0
        config.exactHeight = 30
        tagView.enableTagSelection = true
        tagView.defaultConfig = config
        tagView.reload()
    }
    
    func setupTagView(){
        
        // setupTagView()
        
        setupTagView(data: ["Đang mang thai", "Đã có con"], tagView: tagViewMe)
        tagViewMe.alignment = .center
        tagViewMe.defaultConfig?.exactWidth = 143
        tagViewMe.delegate = self
        tagViewMe.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        tagViewMe.reload()
        
        setupTagView(data: ["~1 tuổi","~1-2 tuổi","~2-3 tuổi","~3-5 tuổi", "> 5 tuổi"], tagView: tagViewBe)
        tagViewBe.alignment = .left
        tagViewBe.defaultConfig?.exactWidth = tagViewBe.frame.width / 3 - 25
        tagViewBe.contentInset = UIEdgeInsets(top: 20, left: 15, bottom: 0, right: 0)
        tagViewBe.delegate = self
        tagViewBe.reload()
    }
    
    func setupAlphaBtnClick (viewAlpa:UIView, label: UILabel, imgIC:UIImageView){

        lblSale.textColor = UIColor.init(hexString: "cecece",alpha: 1)
        lblQA.textColor = UIColor.init(hexString: "cecece",alpha: 1)
        lblInfo.textColor = UIColor.init(hexString: "cecece",alpha: 1)
        lblAll.textColor = UIColor.init(hexString: "cecece",alpha: 1)
        lblBe.textColor = UIColor.init(hexString: "cecece",alpha: 1)
        lblMe.textColor = UIColor.init(hexString: "cecece",alpha: 1)
        
        imgIcAll.alpha = 0.4
        imgIcMe.alpha = 0.4
        imgIcBe.alpha = 0.4
        imgIcInfo.alpha = 0.4
        imgIcQA.alpha = 0.4
        imgIcSale.alpha = 0.4
        viewAll.backgroundColor = UIColor.init(hexString: "ffffff")
        viewMe.backgroundColor = UIColor.init(hexString: "ffffff")
        viewBe.backgroundColor = UIColor.init(hexString: "ffffff")
        viewInfo.backgroundColor = UIColor.init(hexString: "ffffff")
        viewQA.backgroundColor = UIColor.init(hexString: "ffffff")
        viewSale.backgroundColor = UIColor.init(hexString: "ffffff")
        viewAll.borderWidth = 1
        viewMe.borderWidth = 1
        viewBe.borderWidth = 1
        viewInfo.borderWidth = 1
        viewQA.borderWidth = 1
        viewSale.borderWidth = 1
     
        
        imgIC.alpha = 1
        viewAlpa.backgroundColor = UIColor.init(hexString: "fd799d")
        viewAlpa.borderWidth = 0.0
        label.textColor = .white
    }
    
    func setuphideTagView(tag: TTGTextTagCollectionView){
        
        self.tagViewBe.alpha = 0.0
        self.tagViewMe.alpha = 0.0
        self.tagViewBe.isHidden = true
        self.tagViewMe.isHidden = true
        
        if tag == self.tagViewMe || tag == self.tagViewBe {
            UIView.animate(
                withDuration: 0.3,
                delay: 0.0,
                options: [.curveEaseOut],
                animations: {
                    tag.isHidden = false
                    tag.alpha = 1.0
            })
        }
    }
    
    func setuptagBe(){
        lblBe.text = "Bé"
        tagViewBe.setTagAt(0, selected: false)
        tagViewBe.setTagAt(1, selected: false)
        tagViewBe.setTagAt(2, selected: false)
        tagViewBe.setTagAt(3, selected: false)
        tagViewBe.setTagAt(4, selected: false)
    }
    
    func setuptagMe(){
        lblMe.text = "Mẹ"
        tagViewMe.setTagAt(0, selected: false)
        tagViewMe.setTagAt(1, selected: false)
    }
    
    @IBAction func btnAll(_ sender: Any) {
        //   self.isLoadingMore = true
        about = 0
        category = 0
        ageRange = 7
        setuptagBe()
        setuptagMe()
        setuphideTagView(tag: TTGTextTagCollectionView())
        setupAlphaBtnClick(viewAlpa: viewAll, label: lblAll, imgIC: imgIcAll)
        self.selectedTopic(about,category,ageRange)
        //      self.getHomeFeed(page: 1, about: about,category: category, ageRange: ageRange, locationID: self.locationID)
    }
    
    @IBAction func btnMe(_ sender: Any) {
        about = 2
        category = 0
        setuptagBe()
        setuphideTagView(tag: tagViewMe)
        setupAlphaBtnClick(viewAlpa: viewMe, label: lblMe, imgIC: imgIcMe)
    }
    
    @IBAction func btnBe(_ sender: Any) {
        setuptagMe()
        about = 1
        category = 0
        setuphideTagView(tag: tagViewBe)
        setupAlphaBtnClick(viewAlpa: viewBe, label: lblBe, imgIC: imgIcBe)
    }
    
    @IBAction func btnInfo(_ sender: Any) {
        //     self.isLoadingMore = true
        setuptagBe()
        setuptagMe()
        category = 2
        ageRange = 7
        about = 0
        setuphideTagView(tag: TTGTextTagCollectionView())
        setupAlphaBtnClick(viewAlpa: viewInfo, label: lblInfo, imgIC: imgIcInfo)
        self.selectedTopic(about,category,ageRange)
        //     self.getHomeFeed(page: 1, about: about,category: category, ageRange: ageRange, locationID: self.locationID)
    }
    
    @IBAction func btnQA(_ sender: Any) {
        
        //      self.isLoadingMore = true
        setuptagBe()
        setuptagMe()
        category = 1
        ageRange = 7
        about = 0
        setuphideTagView(tag: TTGTextTagCollectionView())
        setupAlphaBtnClick(viewAlpa: viewQA, label: lblQA, imgIC: imgIcQA)
        self.selectedTopic(about,category,ageRange)
        //      self.getHomeFeed(page: 1, about: about,category: category, ageRange: ageRange, locationID: self.locationID)
        
    }
    
    @IBAction func btnSale(_ sender: Any) {
        //         self.isLoadingMore = true
        setuptagBe()
        setuptagMe()
        category = 3
        ageRange = 7
        about = 0
        setuphideTagView(tag: TTGTextTagCollectionView())
        setupAlphaBtnClick(viewAlpa: viewSale, label: lblSale, imgIC: imgIcSale)
        self.selectedTopic(about,category,ageRange)
        //    self.getHomeFeed(page: 1, about: about,category: category, ageRange: ageRange, locationID: self.locationID)
    }
    
}

// MARK: - TTGTextTagCollectionViewDelegate

extension ViewSelectedTopicHome: TTGTextTagCollectionViewDelegate{
    func textTagCollectionView(_ textTagCollectionView: TTGTextTagCollectionView!, didTapTag tagText: String!, at index: UInt, selected: Bool, tagConfig config: TTGTextTagConfig!) {
        
        //        self.isLoadingMore = true
        //        self.arrayHomeFeeds.removeAll()
        //        self.tbvHome.reloadData()
        
        if textTagCollectionView == tagViewMe{
            tagViewMe.setTagAt(0, selected: false)
            tagViewMe.setTagAt(1, selected: false)
            tagViewMe.setTagAt(index, selected: true)
            lblMe.text = tagText ?? ""
            lblBe.text = "Bé"
            ageRange = 6
            if index == 0 {
                lblMe.text = "Mang thai"
                ageRange = 0
            }
            self.tagViewMe.isHidden = true
            self.tagViewBe.isHidden = true
            self.selectedTopic(about,category,ageRange)
            //        self.getHomeFeed(page: 1, about: about, category: category, ageRange: ageRange, locationID: self.locationID)
        }
        if textTagCollectionView == tagViewBe {
            tagViewBe.setTagAt(0, selected: false)
            tagViewBe.setTagAt(1, selected: false)
            tagViewBe.setTagAt(2, selected: false)
            tagViewBe.setTagAt(3, selected: false)
            tagViewBe.setTagAt(4, selected: false)
            tagViewBe.setTagAt(index, selected: true)
            lblBe.text = tagText ?? ""
            lblMe.text = "Mẹ"
            self.tagViewMe.isHidden = true
            self.tagViewBe.isHidden = true
            switch (index) {
            case 0:
                ageRange = 1
            case 1:
                ageRange = 2
            case 2:
                ageRange = 3
            case 3:
                ageRange = 4
            case 4:
                ageRange = 5
            default:
                print(ageRange)
            }
            self.selectedTopic(about,category,ageRange)
            //        self.getHomeFeed(page: 1, about: about,category: category, ageRange: ageRange, locationID: self.locationID)
        }
    }
}
