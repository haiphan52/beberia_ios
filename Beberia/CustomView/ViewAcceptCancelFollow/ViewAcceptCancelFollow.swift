//
//  ViewAcceptCancelFollow.swift
//  Beberia
//
//  Created by IMAC on 7/8/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class ViewAcceptCancelFollow: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var btnSendRequest: UIButton!
    @IBOutlet weak var btnAddFriend: UIButton!
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var btnFriend: UIButton!
    
    let kCONTENT_XIB_NAME = "ViewAcceptCancelFollow"
    var userCreate = User.init(json: "")
    var disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
     required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           commonInit()
       }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        initRx()
    }
    
    func initRx(){
        btnSendRequest.rx.tap.subscribe { (_) in
            self.handleActionRequest()
        }.disposed(by: disposeBag)
        
        btnAddFriend.rx.tap.subscribe { (_) in
            self.handleActionRequest()
        }.disposed(by: disposeBag)
        
        btnReply.rx.tap.subscribe { (_) in
            self.showActionBottom(datasource: ["Chấp nhận", "Xoá lời mời"], isNoTextColorRed: false) { (index) in
                print(index)
                self.confirmCancelRequest(type: index == 0 ? 1 : 2)
            }
        }.disposed(by: disposeBag)
        
        btnFriend.rx.tap.subscribe { (_) in
            self.showActionBottom(datasource: ["Huỷ kết bạn"], isNoTextColorRed: false) { (index) in
                print(index)
                self.handleActionRequest()
            }
        }.disposed(by: disposeBag)
    }
    
    func checkShowButton(isFollow: Int){
        switch isFollow {
        case 0:
            btnAddFriend.isHidden = false
            btnReply.isHidden = true
            btnSendRequest.isHidden = true
            btnFriend.isHidden = true
            case 1:
            btnAddFriend.isHidden = true
            btnReply.isHidden = true
            btnSendRequest.isHidden = true
            btnFriend.isHidden = false
            case 2:
            btnAddFriend.isHidden = true
            btnReply.isHidden = true
            btnSendRequest.isHidden = false
            btnFriend.isHidden = true
            case 3:
            btnAddFriend.isHidden = true
            btnReply.isHidden = false
            btnSendRequest.isHidden = true
            btnFriend.isHidden = true
            
        default:
            btnAddFriend.isHidden = true
            btnReply.isHidden = true
            btnSendRequest.isHidden = true
            btnFriend.isHidden = true
        }
        
    }

    func showActionBottom(datasource:[String], isNoTextColorRed: Bool, completion: @escaping (_ index: Int)->()){
        let bottomVC = BaseBottomViewController.init(nib: R.nib.baseBottomViewController)
        //  print(bottomVC.w)
        bottomVC.selectedOption = { indexSelect in
            completion(indexSelect)
            Utils.getTopMostViewController()?.dismiss(animated: true, completion: {})
        }
        bottomVC.isNoTextColorRed = isNoTextColorRed
        bottomVC.datasource = datasource
        bottomVC.height = CGFloat((bottomVC.datasource.count + 1) * 44)
        bottomVC.topCornerRadius = 20
        bottomVC.presentDuration = 0.25
        bottomVC.dismissDuration = 0.25
        Utils.getTopMostViewController()?.navigationController?.present(bottomVC, animated: true)
    }
    
}

extension ViewAcceptCancelFollow {
    func handleActionRequest(){
        APIManager.handleActionRequest(follow_id: userCreate.id ?? 0, callbackSuccess: { (isFollow) in
            print(isFollow)
            self.checkShowButton(isFollow: isFollow)
            self.userCreate.isFollow = isFollow
        }) {(error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func confirmCancelRequest(type: Int){
        APIManager.confirmCancelRequest(user_id: userCreate.id ?? 0, type: type, callbackSuccess:  { (isFollow) in
            self.checkShowButton(isFollow: isFollow)
            self.userCreate.isFollow = isFollow
        }) {(error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
}

extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
