
//
//  ViewTitleEASY.swift
//  Beberia
//
//  Created by IMAC on 6/30/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewTitleEASY: UIView {

    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var title: UILabel!
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewTitleEASY.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewTitleEASY {
        return R.nib.viewTitleEASY.instantiate(withOwner: nil)[0] as! ViewTitleEASY
    }

}
