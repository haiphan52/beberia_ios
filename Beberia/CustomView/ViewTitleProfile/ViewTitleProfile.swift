//
//  ViewTitleProfile.swift
//  Beberia
//
//  Created by IMAC on 6/16/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewTitleProfile: UIView {
    
    @IBOutlet weak var nameSectionLable: PaddingLabel!
    @IBOutlet weak var nameUserLable: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    
    var tapImageAvatar : ()->() = {}
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewTitleProfile.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewTitleProfile {
        return R.nib.viewTitleProfile.instantiate(withOwner: nil)[0] as! ViewTitleProfile
    }
//
//    @IBAction func didPressBack(_ sender: Any) {
//        Utils.getTopMostViewController()?.navigationController?.popViewController(animated: true)
//    }
    
    @IBAction func didPressTapProfile(_ sender: Any) {
        tapImageAvatar()
    }
}
