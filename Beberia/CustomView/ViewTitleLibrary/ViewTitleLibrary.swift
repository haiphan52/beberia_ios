//
//  ViewTitleLibrary.swift
//  Beberia
//
//  Created by IMAC on 10/22/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit

class ViewTitleLibrary: UIView {
    
    @IBOutlet weak var titile: UILabel!
    var didPressLibrary:()->() = {}
    @IBOutlet weak var btnSeclectLibrary: UIButton!
    
    override var intrinsicContentSize: CGSize {
           return UIView.layoutFittingExpandedSize
       }
       
       override func awakeAfter(using aDecoder: NSCoder) -> Any? {
           guard subviews.isEmpty else { return self }
           return R.nib.viewTitleLibrary.firstView(owner: nil)
       }
       
       class func instanceFromNib() -> ViewTitleLibrary {
           return R.nib.viewTitleLibrary.instantiate(withOwner: nil)[0] as! ViewTitleLibrary
       }
    @IBAction func didPressLibrary(_ sender: Any) {
        didPressLibrary()
    }
}
