//
//  DesBabyFaceView.swift
//  Beberia
//
//  Created by haiphan on 07/08/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift

protocol DesBabyDelegate: AnyObject {
    func moveToUploadImage()
}

class DesBabyFaceView: UIView, SetUpFunctionBase {
    
    @IBOutlet weak var btSeeMore: UIButton!
    var delegate: DesBabyDelegate?
    
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
}
extension DesBabyFaceView {
    
    func setupUI() {
        self.btSeeMore.contentHorizontalAlignment = .left
    }
    
    func setupRX() {
        self.btSeeMore.rx.tap.bind { [weak self] _ in
            guard let self = self else { return }
            self.delegate?.moveToUploadImage()
        }.disposed(by: disposeBag)
    }
    
}
