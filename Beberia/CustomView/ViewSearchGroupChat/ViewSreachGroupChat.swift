//
//  ViewSreachGroupChat.swift
//  Beberia
//
//  Created by IMAC on 6/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewSreachGroupChat: UIView {
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var widthLableTitle: NSLayoutConstraint!
    @IBOutlet weak var leaddingViewSreach: NSLayoutConstraint!
    @IBOutlet weak var tfSreach: TextField!
    @IBOutlet weak var imgSearch: UIImageView!
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewSreachGroupChat.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewSreachGroupChat {
        return R.nib.viewSreachGroupChat.instantiate(withOwner: nil)[0] as! ViewSreachGroupChat
    }
    
    override func awakeFromNib() {
        let width = UIScreen.main.bounds.width
        leaddingViewSreach.constant = width - 10 - 60
        tfSreach.placeholder = ""
    }
    
    func customViewForChatGroup(){
        tfSreach.placeholder = "Tìm kiếm ..."
       // self.lblTitle.alpha = 1
        tfSreach.backgroundColor = UIColor.init(hexString: "ffffff")
      //  imgSearch.image = R.image.icBackSearch()
        imgSearch.image = UIImage.init()
        UIView.animate(withDuration: 0.3) {
            self.leaddingViewSreach.constant = 1
            self.lblTitle.alpha = 0
            self.lblTitle.text = ""
            self.layoutIfNeeded()
        }
    }
    
    @IBAction func didPressSreach(_ sender: Any) {
        let width = UIScreen.main.bounds.width
        
        if lblTitle.text == "Chat nhóm"{
            tfSreach.placeholder = "Tìm kiếm nhóm đã tham gia..."
            self.lblTitle.alpha = 1
            tfSreach.backgroundColor = UIColor.init(hexString: "ffffff")
            imgSearch.image = R.image.icBackSearch()
            UIView.animate(withDuration: 0.3) {
                self.leaddingViewSreach.constant = 1
                self.lblTitle.alpha = 0
                self.lblTitle.text = ""
                self.layoutIfNeeded()
            }
        }else{
            imgSearch.image = R.image.ic_search()
            tfSreach.placeholder = ""
            self.lblTitle.alpha = 0
            tfSreach.backgroundColor = UIColor.init(hexString: "f1f1f1")
            tfSreach.resignFirstResponder()
            UIView.animate(withDuration: 0.3) {
                self.leaddingViewSreach.constant = width - 10 - 60
                self.lblTitle.alpha = 1
                self.lblTitle.text = "Chat nhóm"
                
                self.layoutIfNeeded()
            }
        }
        
    }
    
}

class TextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 7)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
