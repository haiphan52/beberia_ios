//
//  ImageCollectionViewCell.swift
//  Canvasee
//
//  Created by VNPM1 on 7/5/19.
//  Copyright © 2019 VNPM1. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblNumberImageHide: UILabel!
    @IBOutlet weak var imgFeeds: UIImageView!
    @IBOutlet weak var btnTapAddImage: UIButton!
    
    var actionDelete:()->() = {}
    var actionTapImage:()->() = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgFeeds.setBorderImageView()
        self.clipsToBounds = true
        self.layer.cornerRadius = 4
    }

    @IBAction func didPressTapImage(_ sender: Any) {
        if imgFeeds.image == R.image.img_placeHolder_upload()!{
            actionTapImage()
        }
        
    }
    
    @IBAction func didPressDelete(_ sender: Any) {
        actionDelete()
    }
}
