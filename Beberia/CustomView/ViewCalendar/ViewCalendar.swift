//
//  ViewCalendar.swift
//  Beberia
//
//  Created by IMAC on 6/11/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit
import FSCalendar

class ViewCalendar: UIView, FSCalendarDelegate {
    
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var fsCalendar: FSCalendar!
    @IBOutlet weak var titlePopup: UILabel!
    
    var dateString = ""
    var tapOK:(String)->() = {_ in}
    var tapCancel:()->() = {}
    
    override func awakeFromNib() {
        fsCalendar.delegate = self
        customCalander()
        titlePopup.adjustsFontSizeToFitWidth = true
    }
    
    @IBAction func didPressPer(_ sender: Any) {
        let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: fsCalendar.currentPage)
        fsCalendar.setCurrentPage(previousMonth!, animated: true)
    }
    
    func customCalander(){
        self.fsCalendar.placeholderType = .none
        self.fsCalendar.appearance.headerDateFormat = "MMMM yyyy"
        self.fsCalendar.appearance.headerTitleFont =  UIFont.init(name: AppFont.HelveticaNeueMedium, size: 14)

        self.fsCalendar.calendarHeaderView.scrollEnabled = false
        self.fsCalendar.calendarWeekdayView.weekdayLabels[0].text = "T2"
               self.fsCalendar.calendarWeekdayView.weekdayLabels[1].text = "T3"
               self.fsCalendar.calendarWeekdayView.weekdayLabels[2].text = "T4"
               self.fsCalendar.calendarWeekdayView.weekdayLabels[3].text = "T5"
               self.fsCalendar.calendarWeekdayView.weekdayLabels[4].text = "T6"
               self.fsCalendar.calendarWeekdayView.weekdayLabels[5].text = "T7"
               self.fsCalendar.calendarWeekdayView.weekdayLabels[6].text = "CN"
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewCalendar.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewCalendar {
        return R.nib.viewCalendar.instantiate(withOwner: nil)[0] as! ViewCalendar
    }
    
    @IBAction func didPressCancel(_ sender: Any) {
        tapCancel()
    }
    
    @IBAction func didPressDelete(_ sender: Any) {
        tapOK(dateString)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        dateString = Utils.dateToString(date: date, format: "dd/MM/yyyy")
        print(dateString)
    }
}
