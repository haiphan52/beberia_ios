//
//  CanvaseeLayout.swift
//  CustomLayout
//
//  Created by VNPM1 on 7/5/19.
//  Copyright © 2019 VNPM1. All rights reserved.
//

import UIKit

class CanvaseeLayout: UICollectionViewLayout {
 //   weak var canvaseeDelegate: CanvaseeLayoutDelegate!
    
    struct Constant {
        static let distanceSides: CGFloat = 0
        static let cellPadding: CGFloat = 4
        static let totalCelTop: CGFloat = 2
        static let totalCellBot: CGFloat = 3
    }
    
    var heightViewImage: ((CGFloat) -> Void)?
    
    var numberImage: Int = 0 {
        didSet{
            prepare()
        }
    }
    let cellPadding:CGFloat = 4
    var cache = [UICollectionViewLayoutAttributes]()
    let height = UIScreen.main.bounds.width - Constant.distanceSides

    override func prepare() {
        cache = [UICollectionViewLayoutAttributes]()
        guard cache.isEmpty == true, let collectionView = collectionView else {
            return
        }
        
       // let height = UIScreen.main.bounds.width - 32
        
        switch numberImage {
        case 0:
            break
        case 1:
            var frame = CGRect.zero
            for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
                let indexPath = IndexPath(item: item, section: 0)
                frame = CGRect.init(x: 0, y: 0, width: collectionView.frame.width, height: height)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame
                cache.append(attributes)
            }
            break

        case 2:
            let widthCell = (collectionView.bounds.width - cellPadding) / 2
            var origiX: CGFloat = 0
        //    let height = collectionView.bounds.height
            for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
                var frame = CGRect.zero
                if item > 0{
                    origiX = CGFloat(item) * widthCell + cellPadding
                }
                frame = CGRect.init(x: origiX, y: 0, width: widthCell, height: height)

                let indexPath = IndexPath(item: item, section: 0)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame
                cache.append(attributes)
            }
            break
        case 3:
       //     let height = collectionView.bounds.height
            let widthCell = (collectionView.bounds.width - cellPadding) / 2
            let heightCell = (height - cellPadding) / 2
            var origiX: CGFloat = 0
            
            for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
                var frame = CGRect.zero

                switch item {
                case 0:
                    frame = CGRect.init(x: 0, y: 0, width: widthCell, height: height)
                    break
                    
                case 1:
                    origiX = widthCell + cellPadding
                    frame = CGRect.init(x: origiX, y: 0, width: widthCell, height: heightCell)
                    break
                    
                case 2:
                    frame = CGRect.init(x: origiX, y: heightCell + cellPadding, width: widthCell, height: heightCell)
                    break
                    
                default:
                    break
                }

                let indexPath = IndexPath(item: item, section: 0)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame
                cache.append(attributes)
            }
            break
        //
        case 4:
            let widthCell = (height - cellPadding * 2) / 3
            let heightCell = height
            let originY = heightCell - widthCell - cellPadding
            for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
                var frame = CGRect.zero
                
                switch item {
                case 0:
                    frame = CGRect.init(x: 0, y: 0, width: collectionView.bounds.width, height: originY)
                    break
                    
                case 1:
                    frame = CGRect.init(x: 0, y: originY + cellPadding, width: widthCell, height: widthCell)
                    break
                    
                case 2:
                    frame = CGRect.init(x: widthCell + cellPadding, y: originY + cellPadding, width: widthCell, height: widthCell)
                    break
                    
                case 3:
                    frame = CGRect.init(x: widthCell * 2 + cellPadding * 2, y: originY + cellPadding, width: widthCell, height: widthCell)
                    let hImages = frame.origin.y + frame.size.height
                    let hCell = UIScreen.main.bounds.width - 32
                    if hCell > hImages {
                        self.heightViewImage?(frame.origin.y + frame.size.height)
                    }
                    
                default:
                    break
                }
                
                let indexPath = IndexPath(item: item, section: 0)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame
                cache.append(attributes)
            }
            break
            
        default :
//            let widthCellHeader = (self.height - (Constant.cellPadding)) / Constant.totalCelTop
//            let widthCellSub = (self.height - (Constant.cellPadding * 2)) / Constant.totalCellBot
            
            let widthCellHeader = (height - cellPadding) / 2
            let widthCellSub = (height - cellPadding * 2) / 3
            let heightCell = height
//            let originY = heightCell - widthCell - cellPadding
           
            for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
                var frame = CGRect.zero
                
                switch item {
                case 0:
                    frame = CGRect.init(x: 0, y: 0, width: widthCellHeader, height: widthCellHeader)
                    break
                    
                case 1:
                    frame = CGRect.init(x: (collectionView.bounds.width / 2) + (cellPadding / 2), y: 0, width: widthCellHeader, height: widthCellHeader)
                    break
                    
                case 2:
                    frame = CGRect.init(x: 0, y: widthCellHeader + cellPadding, width: widthCellSub, height: widthCellSub)
                    break
                    
                case 3:
                    frame = CGRect.init(x: widthCellSub + cellPadding, y: widthCellHeader + cellPadding, width: widthCellSub, height: widthCellSub)
                    break
                    
                case 4:
                    frame = CGRect.init(x: widthCellSub * 2 + cellPadding * 2, y: widthCellHeader + cellPadding, width: widthCellSub, height: widthCellSub)
                    let hImages = frame.origin.y + frame.size.height
                    let hCell = UIScreen.main.bounds.width - 32
                    if hCell > hImages {
                        self.heightViewImage?(frame.origin.y + frame.size.height)
                    }
                    
                default:
                    break
                }
                
                let indexPath = IndexPath(item: item, section: 0)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame
                cache.append(attributes)
            }
            break
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        // Loop through the cache and look for items in the rect
        for attributes in cache {
         //   if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
         //   }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
}
