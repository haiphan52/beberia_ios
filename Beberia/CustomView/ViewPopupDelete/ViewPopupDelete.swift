//
//  ViewPopupDelete.swift
//  Beberia
//
//  Created by IMAC on 9/23/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import Foundation
import UIKit

class ViewPopupDelete: UIView {
    
    var tapDelete:()->() = {}
    var tapCancel:()->() = {}
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewPopupDelete.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewPopupDelete {
        return R.nib.viewPopupDelete.instantiate(withOwner: nil)[0] as! ViewPopupDelete
    }
    
    @IBAction func didPressCancel(_ sender: Any) {
        tapCancel()
    }
    
    @IBAction func didPressDelete(_ sender: Any) {
        tapDelete()
    }
}
