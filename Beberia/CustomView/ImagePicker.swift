//
//  ImagePicker.swift
//  quycogangnam
//
//  Created by OS on 4/29/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

public protocol ImagePickerDelegate: class {
    func didSelect(image: UIImage?, fileName: String?, fileUrl: String?)
}

open class ImagePicker: NSObject {

    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    private weak var delegate: ImagePickerDelegate?
    var fileName: String?
    var isFullImage = true
    var isScaleImage = true

    public init(presentationController: UIViewController, delegate: ImagePickerDelegate, isFullImage: Bool = true, isScaleImage:Bool = true) {
        self.pickerController = UIImagePickerController()

        super.init()

        self.presentationController = presentationController
        self.delegate = delegate
    
        self.pickerController.delegate = self
        self.pickerController.allowsEditing = isFullImage
        self.pickerController.mediaTypes = ["public.image"]
        self.isFullImage = isFullImage
        self.isScaleImage = isScaleImage
        
    }

    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }

        return UIAlertAction(title: title, style: .default) { [weak self] _ in
            guard let self = self else { return }
            self.pickerController.sourceType = type
            self.presentationController?.present(self.pickerController, animated: true)
        }
    }
    
    public func presentPickerView(type: Int){
        self.pickerController.sourceType = type == 0 ? .camera : .photoLibrary
        self.presentationController?.present(self.pickerController, animated: true)
    }

    public func present(from sourceView: UIView) {

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        if let action = self.action(for: .camera, title: R.string.localizable.commonCamera()) {
            alertController.addAction(action)
        }
        if let action = self.action(for: .savedPhotosAlbum, title: R.string.localizable.commonTakePhoto()) {
            alertController.addAction(action)
        }
//        if let action = self.action(for: .photoLibrary, title: "Photo library") {
//            alertController.addAction(action)
//        }

        alertController.addAction(UIAlertAction(title: R.string.localizable.commonCancel(), style: .cancel, handler: nil))

        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }

        self.presentationController?.present(alertController, animated: true)
    }

    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?,_ fileName: String?,_ fileUrl: String?) {
        controller.dismiss(animated: true) {
            guard let image = image else {
                return
            }


            if self.isScaleImage {
                let imgData = NSData(data: image.jpegData(compressionQuality: 1)!)
                let _: Int = imgData.count
                
                if let image = image.scaleImage(toSize: CGSize.init(width: 200, height: 200)) {
                    
                        let imgData = NSData(data: image.jpegData(compressionQuality: 1)!)
                        let _: Int = imgData.count
                //        print("actual size of image in KB: %f ", Double(imageSize) / 1000.0)
                        self.delegate?.didSelect(image: image, fileName: fileName ?? "file_name", fileUrl: fileUrl)
                }
            }else{
                self.delegate?.didSelect(image: image, fileName: fileName ?? "file_name", fileUrl: fileUrl)
            }
        }
    }
}

extension ImagePicker: UIImagePickerControllerDelegate {

    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        return self.pickerController(picker, didSelect: nil, nil ,nil )
    }

    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        var imageTemp = UIImage()
        if isFullImage {
            guard let image = info[.editedImage] as? UIImage else {
                return self.pickerController(picker, didSelect: nil, nil ,nil )
            }
            imageTemp = image
        }else{
            guard let image = info[.originalImage] as? UIImage else {
                return self.pickerController(picker, didSelect: nil, nil ,nil )
            }
            imageTemp = image
        }
        
        
        guard let fileUrl = info[.imageURL] as? URL else {
            self.pickerController(picker, didSelect: imageTemp.fixImageOrientation(), nil , nil )
            return
            
        }
        
        let imgName = fileUrl.lastPathComponent
        self.pickerController(picker, didSelect: imageTemp.fixImageOrientation(), imgName , fileUrl.absoluteString )
    }
}

extension ImagePicker: UINavigationControllerDelegate {

}
