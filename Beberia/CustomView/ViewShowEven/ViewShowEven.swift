//
//  ViewShowEven.swift
//  Beberia
//
//  Created by IMAC on 3/10/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit

class ViewShowEven: UIView {
    @IBOutlet weak var imageEven: UIImageView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnNoRemind: UIButton!
    
    var tapNotRemind: ()->() = {}
    var tapClose: ()->() = {}
    var tapEven: ()->() = {}
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewShowEven.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewShowEven {
        return R.nib.viewShowEven.instantiate(withOwner: nil)[0] as! ViewShowEven
    }
    
    @IBAction func didPressEven(_ sender: Any) {
        tapEven()
    }
    
    @IBAction func didPressClose(_ sender: Any) {
        tapClose()
    }
    
    @IBAction func didPressNotRemind(_ sender: Any) {
        tapNotRemind()
    }
}
