//
//  BottomTableViewViewController.swift
//  Beberia
//
//  Created by Lap on 12/21/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import IQKeyboardManagerSwift

class BottomTableViewViewController: BottomPopupViewController {
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightViewREply: NSLayoutConstraint!
    @IBOutlet weak var commentTbv: CommentTableView!
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet weak var tvInputComment: UITextView!
    @IBOutlet weak var heightConstraintViewInput: NSLayoutConstraint!
    @IBOutlet weak var heightViewInput: NSLayoutConstraint!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblNameUserReply: UILabel!
    @IBOutlet weak var viewNotiReplyComment: UIView!
    
    private var keyboardHelper: KeyboardHelper?
    var isFeed: Int? = nil
    var bag = DisposeBag()
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var activityIndicator = UIActivityIndicatorView()
    var isTyping = false
    var isFristTyping = false
    var updateComment: ()->() = {}
    let statusBar =  UIView()
    var isHidenViewInput = true{
        didSet{
            UIView.animate(withDuration: 0.5) {
                self.viewInput.isHidden = self.isHidenViewInput
                self.heightConstraintViewInput.constant = self.isHidenViewInput ? 0 : 50
            }
        }
    }
    
    var isCheckShowKBFirst = false
    var isHidenButtonViewMoreComment = true{
        didSet{
            //   btnViewMoreCommnet.isHidden = !isHidenButtonViewMoreComment
            //   heightbtnViewMoreCommnet.constant = !isHidenButtonViewMoreComment ? 0 : 40
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.view.frame.height)
        guard let isFeed = isFeed else {
            return
        }
        self.initLoadding()
        self.startLoadding()
        commentTbv.fromToView = FromToview.Home
        commentTbv.idPost = isFeed
        
        commentTbv.dismissLoadding.subscribe { (isDissmiss) in
            guard let dismiss = isDissmiss.element else { return }
            if dismiss == true
            {
                self.stopLoadding()
            }
        }.disposed(by: bag)
        
        //    initViewInput()
        initTextViewComment()
        
        commentTbv.tapReaply = { name in
            self.isHidenViewInput = false
            self.lblNameUserReply.text = ""
            self.viewNotiReplyComment.isHidden = true
            
            self.lblNameUserReply.text = name
            self.viewNotiReplyComment.isHidden = false
            self.showViewNotiReply()
        }
        
        commentTbv.showButtonViewMoreComment = { isShow in
            self.isHidenButtonViewMoreComment = isShow
        }
        
        let positionY = CGFloat(UIScreen.main.bounds.height) - CGFloat(UIScreen.main.bounds.height * 0.75)
        keyboardHelper = KeyboardHelper { [unowned self] animation, keyboardFrame, duration in
            switch animation {
            case .keyboardWillShow:
                
                //        bottomConstraint.constant = keyboardFrame.height
                statusBar.frame = UIApplication.shared.statusBarFrame
                statusBar.backgroundColor = .white
                UIApplication.shared.keyWindow?.addSubview(statusBar)
                if !isCheckShowKBFirst {
                    bottomConstraint.constant = 34
                    isCheckShowKBFirst = true
                }
                
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                self.view.frame = CGRect(x: 0, y: positionY - keyboardFrame.height - ConstantApp.shared.getHeightSafeArea(type: .bottom),
                                         width: UIScreen.main.bounds.width, height: self.view.frame.height)
            case .keyboardWillHide:
                bottomConstraint.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                statusBar.backgroundColor = .clear
                self.view.frame = CGRect(x: 0, y: positionY, width: UIScreen.main.bounds.width, height: self.view.frame.height)
            }
        }
        
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        IQKeyboardManager.shared.enable = false
    //    }
    //
    //    override func viewWillDisappear(_ animated: Bool) {
    //        IQKeyboardManager.shared.enable = true
    //    }
    
    func showViewNotiReply(){
        self.heightViewREply.constant = 0
        UIView.animate(withDuration: 0.25) {
            self.heightViewREply.constant = 35
            //  self.viewNotiReplyComment.center.y -= self.viewNotiReplyComment.bounds.height
            self.view.layoutIfNeeded()
        }
    }
    
    func initTextViewComment(){
        tvInputComment.cornerRadius = 15
        tvInputComment.delegate = self
        tvInputComment.text =  R.string.localizable.infomationWriteComment()
        tvInputComment.textColor = UIColor.lightGray
        btnSend.isEnabled = true
        if  tvInputComment.textColor == UIColor.lightGray{
            btnSend.isEnabled = false
        }
        tvInputComment.autocorrectionType = .no
        viewNotiReplyComment.isHidden = true
        heightViewREply.constant = 0
    }
    
    func initViewInput(){
        self.tvInputComment.text =  R.string.localizable.infomationWriteComment()
        self.tvInputComment.textColor = UIColor.lightGray
        self.heightViewInput.constant = 33
        self.heightConstraintViewInput.constant = 55
        Utils.getTopMostViewController()?.view.endEditing(true)
    }
    
    @IBAction func didPressSendComment(_ sender: UIButton) {
        
        guard let content = tvInputComment.text?.trimmingCharacters(in: .whitespacesAndNewlines), content.count > 0 else {
            return
        }
        sender.preventRepeatedPresses()
        
        commentTbv.didPressSendComment(content: content, completion: {
            self.initViewInput()
            self.lblNameUserReply.text = ""
            self.tvInputComment.text = ""
            self.viewNotiReplyComment.isHidden = true
            self.updateComment()
        })
    }
    
    @IBAction func didPressCloseViewNotiReply(_ sender: Any) {
        lblNameUserReply.text = ""
        viewNotiReplyComment.isHidden = true
        commentTbv.parentId = nil
        commentTbv.indexParent = 0
        commentTbv.userIdTag = []
        // scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        self.heightViewREply.constant = 35
        UIView.animate(withDuration: 0.25) {
            self.heightViewREply.constant = 0
            //  self.viewNotiReplyComment.center.y -= self.viewNotiReplyComment.bounds.height
        }
    }
    
    override func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(UIScreen.main.bounds.height * 0.75)
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 1.0
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 1.0
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
    
    func updateHeightTextView(_ textView: UITextView){
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
        heightConstraintViewInput.constant = newFrame.height + 22
        heightViewInput.constant = newFrame.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    func initLoadding(view: UIView = UIView.init()){
        if #available(iOS 13.0, *) {
            activityIndicator = UIActivityIndicatorView.init(style: .large)
        } else {
            activityIndicator = UIActivityIndicatorView.init(style: .gray)
            // Fallback on earlier versions
        }
        activityIndicator.center = CGPoint.init(x: UIScreen.main.bounds.width / 2, y: (UIScreen.main.bounds.height * 0.75) / 2 - 10)
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func startLoadding(){
        activityIndicator.startAnimating()
    }
    
    func stopLoadding(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
}

extension BottomTableViewViewController: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        
        updateHeightTextView(textView)
        btnSend.isEnabled = true
        isTyping = true
        
        if (tvInputComment.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! || tvInputComment.text.count < 1{
            btnSend.isEnabled = false
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        updateHeightTextView(textView)
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.isFristTyping = false
        isTyping = false
        if textView.text.isEmpty {
            tvInputComment.text =  R.string.localizable.infomationWriteComment()
            textView.textColor = UIColor.lightGray
        }
    }
    
}
