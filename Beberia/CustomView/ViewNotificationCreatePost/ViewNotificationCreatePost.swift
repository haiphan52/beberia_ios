//
//  ViewNotificationCreatePost.swift
//  Beberia
//
//  Created by IMAC on 9/25/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import Foundation
import UIKit

class ViewNotificationCreatePost: UIView {
    
    @IBOutlet weak var lblNoti: UILabel!
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewNotificationCreatePost.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewNotificationCreatePost {
        return R.nib.viewNotificationCreatePost.instantiate(withOwner: nil)[0] as! ViewNotificationCreatePost
    }

}
