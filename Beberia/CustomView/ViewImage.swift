//
//  ViewImage.swift
//  Canvasee
//
//  Created by VNPM1 on 7/5/19.
//  Copyright © 2019 VNPM1. All rights reserved.
//

import UIKit

enum ItemType{
    case photo
    case video
}

class ItemFeed{
    var photo: UIImage?
    var time: Float?
    var type: ItemType?
    var content: String! = ""
    var url: URL?
    
    init() {}
    
}

class ViewImage: UIView {
    
    @IBOutlet var clvImgFeeds: UICollectionView!
    
    var heightViewImage: ((CGFloat) -> Void)?
    
    var itemFeeds = [Media]()
    {
        didSet{
            //            itemFeeds.removeFirst()
            layout.numberImage = itemFeeds.count
            self.clv?.collectionViewLayout = layout
            self.clv.reloadData()
            
        }
    }
    var dismissLightBoxTrigger: (() -> Void)?
    var isFromViewBaby = false
    var tapImageBaby :(Int)->() = {_ in}
    var tapImage :()->() = {}
    var actionDelete :(_ index :Int)->() = {_ in}
    var layout = CanvaseeLayout()
    var photos = [UIImage]()
    var clv: UICollectionView! = nil
    var imagePreviews = [LightboxImage]()
    var images = [UIImage]() {
        didSet{
            layout.numberImage = images.count
            self.clv?.collectionViewLayout = layout
            self.clv.reloadData()
        }
    }
    
    override func awakeFromNib() {
        clv = UICollectionView(frame: self.frame, collectionViewLayout: layout)
        clv.dataSource = self
        clv.delegate = self
        clv?.backgroundColor = UIColor.white
        clv.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        clv.frame = bounds
        self.addSubview(clv)
        self.clv.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        self.layout.heightViewImage = { [weak self] value in
            guard let wSelf = self else { return }
            wSelf.heightViewImage?(value)
        }
    }
    
    func convertFeedToImage(){
        //   var iamgePreviewTemp  = [LightboxImage]()
        for itemFeed in itemFeeds {
            imagePreviews.append(LightboxImage.init(imageURL: URL(string: itemFeed.link ?? "")!, text: itemFeed.descriptionValue ?? "", videoURL: nil))
        }
    }
    
    func previewImage(index: Int){
        // Create an array of images.
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: imagePreviews)
        self.lightboxController(controller, didMoveToPage: 1)
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        controller.goTo(index)
        
        // Present your controller.
        Utils.getTopMostViewController()?.present(controller, animated: true, completion: nil)
        
        
    }
    
}
extension ViewImage: LightboxControllerPageDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension ViewImage: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        self.dismissLightBoxTrigger?()
    }
}

extension ViewImage: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count > 0 ? images.count : itemFeeds.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clv.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath)
        if let imageCollectionViewCell = cell as? ImageCollectionViewCell {
            
            imageCollectionViewCell.actionDelete = {
                self.actionDelete(indexPath.row)
            }
            
            if isFromViewBaby {
                imageCollectionViewCell.actionTapImage = {
                    self.tapImageBaby(self.tag)
                }
            } else {
                imageCollectionViewCell.btnTapAddImage.isHidden = true
            }
            
            
            if itemFeeds.count > 0 {
                imageCollectionViewCell.btnDelete.isHidden = true
                imageCollectionViewCell.lblNumberImageHide.text = ""
                //    imageCollectionViewCell.lblNumberImageHide.isHidden = true
                if itemFeeds.count > 5{
                    if indexPath.row == 4{
                        //       imageCollectionViewCell.lblNumberImageHide.isHidden = false
                        //       imageCollectionViewCell.lblNumberImageHide.text = "+\(itemFeeds.count - 5)"
                    }
                }
                
                imageCollectionViewCell.imgFeeds.sd_setImage(with: URL.init(string: itemFeeds[indexPath.row].link ?? ""), completed: nil)
            }else{
                imageCollectionViewCell.btnDelete.isHidden = false
                imageCollectionViewCell.lblNumberImageHide.text = ""
                //   imageCollectionViewCell.lblNumberImageHide.isHidden = true
                if images.count > 5{
                    if indexPath.row == 4{
                        //  imageCollectionViewCell.lblNumberImageHide.isHidden = false
                        //  imageCollectionViewCell.lblNumberImageHide.text = "+\(images.count - 5)"
                    }
                }
                
                imageCollectionViewCell.imgFeeds.image = images[indexPath.row]
            }
            
            if isFromViewBaby {
                imageCollectionViewCell.btnDelete.isHidden = imageCollectionViewCell.imgFeeds.image == R.image.img_placeHolder_upload()!
            }
            
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if itemFeeds.count > 0 {
            imagePreviews.removeAll()
            tapImage()
            convertFeedToImage()
            previewImage(index: indexPath.row)
        }
    }
}
