//
//  UpdateAvatarGroupChatViewController.swift
//  Beberia
//
//  Created by IMAC on 6/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class UpdateAvatarGroupChatViewController: BottomPopupViewController {
    
    @IBOutlet weak var titileLable: UILabel!
    @IBOutlet weak var imgAvatarGroupChat: UIImageView!
    @IBOutlet weak var btnSelectCamera: UIButton!
    @IBOutlet weak var btnTakePhoto: UIButton!
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    var tapCamera :(Int)->() = {_ in }
    var tapTakePhoto :(Int)->() = {_ in }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        titileLable.roundCorners([.topRight, .topLeft], radius: 20)
    }
    
    override func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(300)
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 1.0
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 1.0
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
    
    @IBAction func didPressCamera(_ sender: UIButton) {
        tapCamera(sender.tag)
    }
    
    @IBAction func didPressTakePhoto(_ sender: UIButton) {
        tapTakePhoto(sender.tag)
    }
}
