//
//  ViewTitleVaccin.swift
//  Beberia
//
//  Created by IMAC on 6/15/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewTitleVaccin: UIView {

    @IBOutlet weak var centerX: NSLayoutConstraint!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var imageTitle: UIImageView!
    
    override var intrinsicContentSize: CGSize {
           return UIView.layoutFittingExpandedSize
       }
       
       override func awakeAfter(using aDecoder: NSCoder) -> Any? {
           guard subviews.isEmpty else { return self }
           return R.nib.viewTitleVaccin.firstView(owner: nil)
       }
       
       class func instanceFromNib() -> ViewTitleVaccin {
           return R.nib.viewTitleVaccin.instantiate(withOwner: nil)[0] as! ViewTitleVaccin
       }

}
