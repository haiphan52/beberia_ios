//
//  SelectImageView.swift
//  Beberia
//
//  Created by haiphan on 08/08/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import AVFoundation

class SelectImageView: UIView,
                       SetUpFunctionBase,
                       SetupBaseCollection,
                       PlayVideoProtocel {
    
    enum ImageType {
        case gift, normal, video
        static func getStatus(index: Int) -> Self {
            if index == 0 {
                return .gift
            }
            
            if index == 4 {
                return .video
            }
            
            return .normal
        }
    }
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var contentVideoView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var giftImage: UIImageView!
    @IBOutlet weak var widthGiftImage: NSLayoutConstraint!
    @IBOutlet weak var heightGiftImage: NSLayoutConstraint!
    private let souces: BehaviorRelay<[UIImage?]> = BehaviorRelay.init(value: [])
    var playVideo: AVPlayer = AVPlayer()
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
        self.collectionView.selectItem(at: IndexPath(row: 0, section: 0),
                                       animated: true,
                                       scrollPosition: .bottom)
    }
}
extension SelectImageView {
    func setupUI() {
        self.setupCollectionView(collectionView: collectionView,
                                 delegate: self,
                                 name: SelectImageCell.self)
        self.layoutIfNeeded()
        self.img.image = self.img.image?.resizeImageSpace(width: self.img.frame.width)
        self.souces.accept(self.setupImages())
        guard let path = Bundle.main.path(forResource: "video_select_print", ofType:"mp4")else {
            debugPrint("video.m4v not found")
            return
        }
        let url = URL(fileURLWithPath: path)
        self.playVideo(url: url, videoView: self.videoView, playVideo: &playVideo)
        self.pauseVideo(playVideo: playVideo)
        self.contentVideoView.isHidden = true
        
        self.giftImage.image = self.giftImage.image?.resizeImageSpace(width: self.giftImage.frame.width)
        let width = self.giftImage.image?.resizeImageSpace(width: self.giftImage.frame.width)?.size.width
        let height = self.giftImage.image?.resizeImageSpace(width: self.giftImage.frame.width)?.size.height
        self.widthGiftImage.constant = width ?? 100
        self.heightGiftImage.constant = height ?? 100
        self.loadGift()
        
        let tapVideo: UITapGestureRecognizer = UITapGestureRecognizer()
        self.videoView.addGestureRecognizer(tapVideo)
        
        tapVideo.rx.event.asObservable().bind { [weak self] _ in
            guard let wSelf = self else { return }
            if wSelf.playVideo.rate != 0 {
                wSelf.pauseVideoSelect()
            } else {
                wSelf.playVideoSelect()
            }
        }.disposed(by: self.disposeBag)
        
        self.layer.applySketchShadow()
        
    }
    
    func setupRX() {
        self.souces
            .bind(to: self.collectionView.rx.items(cellIdentifier: SelectImageCell.identifier, cellType: SelectImageCell.self)) { row, data, cell in
                cell.setImage(img: data)
            }.disposed(by: disposeBag)
        
        self.collectionView
            .rx
            .itemSelected
            .withUnretained(self)
            .bind { owner, idx in
                switch ImageType.getStatus(index: idx.row) {
                case .gift:
                    owner.pauseVideo(playVideo: owner.playVideo)
                    owner.loadGift()
                    owner.contentVideoView.isHidden = true
                    owner.img.isHidden = true
                    owner.giftImage.isHidden = false
                case .normal:
                    owner.pauseVideo(playVideo: owner.playVideo)
                    let item = owner.souces.value[idx.row]
                    owner.img.image = item?.resizeImageSpace(width: owner.img.frame.width)
                    owner.contentVideoView.isHidden = true
                    owner.img.isHidden = false
                    owner.giftImage.isHidden = true
                case .video:
                    owner.play(playVideo: owner.playVideo)
                    owner.contentVideoView.isHidden = false
                    owner.img.isHidden = true
                    owner.giftImage.isHidden = true
                }
            }.disposed(by: disposeBag)
    }
    
    private func loadGift() {
        self.giftImage.image = UIImage.gifImageWithName("gift_images_baby")
    }
    
    
    func pauseVideoSelect() {
        self.pauseVideo(playVideo: playVideo)
    }
    
    func playVideoSelect() {
        guard self.contentVideoView.isHidden == false else {
            return
        }
        self.play(playVideo: self.playVideo)
    }
    
    private func setupImages() -> [UIImage?] {
        return [R.image.img_select_1(),
                R.image.img_select_2(),
                R.image.img_select_3(),
                R.image.img_select_4(),
                R.image.img_select_5()]
    }
}
extension SelectImageView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
}
