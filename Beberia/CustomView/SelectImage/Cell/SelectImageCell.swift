//
//  SelectImageCell.swift
//  Beberia
//
//  Created by haiphan on 08/08/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class SelectImageCell: UICollectionViewCell, SetUpFunctionBase {

    @IBOutlet weak var img: UIImageView!
    
    override var isSelected: Bool {
       didSet{
           if self.isSelected {
               self.selectedView()
           }
           else {
               self.deSelectedView()
           }
       }
   }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupUI()
        self.setupRX()
    }

}
extension SelectImageCell {
    
    func setupUI() {
        self.clipsToBounds = true
        self.layer.cornerRadius = 4
    }
    
    func setupRX() {
        
    }
    
    private func deSelectedView() {
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 1
    }
    
    private func selectedView() {
        self.layer.borderColor = R.color.ff0046()?.cgColor
        self.layer.borderWidth = 1
    }
    
    func setImage(img: UIImage?) {
        self.img.image = img
    }
    
}
