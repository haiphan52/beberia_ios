//
//  JailBreakDetector.swift
//  Beberia
//
//  Created by Lap on 1/13/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import UIKit


final class JailBreakDetector {
  
  static func isJailBroken() -> Bool {
    #if targetEnvironment(simulator)
    return false
    #else
    return detectFiles()
   // return detectFiles() || detectSchemes() || canSpoofing() || detectFork()
    #endif
  }
  
}


fileprivate struct System {
  
  static let files = [
//    "/bin/sh",
//    "/etc/apt",
//    "/bin/bash",
//    "/usr/bin/ssh",
//    "/usr/libexec/ssh-keysign",
//    "/usr/libexec/sftp-server",
//    "/private/var/stash",
//    "/private/var/lib/apt/",
//    "/private/var/cache/apt/",
//    "/private/var/log/syslog",
//    "/Applications/Icy.app",
//    "/Applications/MxTube.app",
//    "/Applications/RockApp.app",
//    "/Applications/blackra1n.app",
//    "/Applications/IntelliScreen.app",
//    "/Library/MobileSubstrate/MobileSubstrate.dylib",
//    "/Library/MobileSubstrate/CydiaSubstrate.dylib",
//    "/System/Library/LaunchDaemons/com.ikey.bbot.plist",
//    "/Library/MobileSubstrate/DynamicLibraries/Veency.plist",
//    "/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",
    
    "/etc/fstab",
    "/etc/master.passwd",
    "/var/log/apt/term.log",
    "/bin/ps",
    "/usr/bin/gunzip",
    "/usr/bin/gzip",
    "/usr/contrib/bin/gzip",
    "/usr/contrib/bin/gunzip",
    "/usr/bin/sshd",
    "/usr/sbin/sshd",
    "/etc/ssh/sshd_config",
    "/private/var/lib/cydia",
    "/private/var/tmp/cydia.log",
    "/Applications/Cydia.app",
    "/Applications/SBSettings.app",
    "/Applications/FakeCarrier.app",
    "/Applications/WinterBoard.app",
    "/private/var/mobile/Library/SBSettings/Themes",
    "/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist"
  ]
  
  static let schemes = ["cydia://package/com.example.package"]
  
  static let jailbreakTextFile = "/private/jailbreak.txt"
  
}


fileprivate extension JailBreakDetector {
  
  //-----------------------------------------------------------------------------
  static func detectFiles() -> Bool {
    var existsPath = false
    for path: String in System.files {
      
      if FileManager.default.fileExists(atPath: path) {
        existsPath = true
        break
      }
      
    }
    return existsPath
  }
  
  //-----------------------------------------------------------------------------
  static func detectSchemes() -> Bool {
    var canOpenScheme = false
    for scheme: String in System.schemes {
      if let url = URL(string: scheme) {
        if UIApplication.shared.canOpenURL(url) {
          canOpenScheme = true
          break
        }
      }
    }
    return canOpenScheme
  }
  
  //-----------------------------------------------------------------------------
  static func canSpoofing() -> Bool {
    let spoofString = "Anti-spoofing detection."
    var grantsToWrite = false
    do {
      try spoofString.write(toFile: System.jailbreakTextFile, atomically: true, encoding: .utf8)
      grantsToWrite = true
    } catch {
      print("Device is not jailbroken")
    }
    try? FileManager.default.removeItem(atPath: System.jailbreakTextFile)
    return grantsToWrite
  }
  
  //-----------------------------------------------------------------------------
  static func detectFork() -> Bool {
    // The symbol lookup happens in the normal global scope
    // Special pseudo-handle for finding the first occurrence of the desired symbol
    // using the default library search order
    var canCreateFork = false
    let RTLD_DEFAULT = UnsafeMutableRawPointer(bitPattern: -2)
    let forkPtr = dlsym(RTLD_DEFAULT, "fork")
    typealias ForkType = @convention(c) () -> Int32
    let fork = unsafeBitCast(forkPtr, to: ForkType.self)
    let child: __darwin_pid_t = fork()
    let pid = Int(child)
    
    /*
     After initializing pid to -1 and status to 0.
     For jailbroken device pid value is greater than 0 whereas for non jailbroken device pid value remains unchanged.
     */
    
    precondition(pid != 0,"\n[🔥 ERROR]: ----- Incorrect Child Fork Behavior For iOS : \(self)" )
    if pid > 0  { canCreateFork = true }
    return canCreateFork
  }
  
}
