//
//  CustomSegmentView.swift
//  Beberia
//
//  Created by IMAC on 2/14/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit

protocol CustomSegmentedControlDelegate:class {
    func changeToIndex(index:Int)
}

class CustomSegmentView: UIView {
    
    private var buttonTitles:[String]!
    private var buttons: [UIButton]!
    private var selectorView: UIView!
    var selectedIndex : Int = 0
    weak var delegate:CustomSegmentedControlDelegate?
    
    var textColor:UIColor = UIColor.init(hexString: "000000", alpha: 0.5)!
    var selectorViewColor: UIColor = UIColor.init(hexString: "fcad24", alpha: 1)!
    var selectorTextColor: UIColor = .white
    var cornerRadiusView: CGFloat = 4.0
    
    convenience init(frame:CGRect,buttonTitle:[String]) {
        self.init(frame: frame)
        self.buttonTitles = buttonTitle
        updateView()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.backgroundColor = UIColor.white
        //   updateView()
    }
    
    func setButtonTitles(buttonTitles:[String]) {
        self.buttonTitles = buttonTitles
        self.updateView()
    }
    
    func setIndex(index:Int) {
        buttons.forEach({
            $0.setTitleColor(textColor, for: .normal)
            $0.backgroundColor = UIColor.init(hexString: "f8f8f8")
        })
        let button = buttons[index]
        selectedIndex = index
        button.setTitleColor(selectorTextColor, for: .normal)
        button.backgroundColor = UIColor.init(hexString: "ffc440")
        let selectorPosition = (Screen.width - 48)/CGFloat(buttonTitles.count) * CGFloat(index)
        UIView.animate(withDuration: 0.3) {
            self.selectorView.frame.origin.x = selectorPosition
        }
    }
    
    @objc func buttonAction(sender:UIButton) {
        for (buttonIndex, btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            btn.backgroundColor = UIColor.init(hexString: "f8f8f8")
            if btn == sender {
                let selectorPosition = (Screen.width - 48)/CGFloat(buttonTitles.count) * CGFloat(buttonIndex)
                selectedIndex = buttonIndex
                delegate?.changeToIndex(index: selectedIndex)
                UIView.animate(withDuration: 0.3) {
                    self.selectorView.frame.origin.x = selectorPosition
                }
                btn.setTitleColor(selectorTextColor, for: .normal)
                btn.backgroundColor = UIColor.init(hexString: "ffc440")
                //btn.layer.cornerRadius = 4
            }
        }
    }
}

//Configuration View
extension CustomSegmentView {
    private func updateView() {
        configView()
        createButton()
        configSelectorView()
        configStackView()
    }
    
    private func configView(){
        self.layer.cornerRadius = cornerRadiusView
        //       self.layer.borderColor = UIColor.init(hexString: "aeaeae")?.cgColor
        //       self.layer.borderWidth = 1.0
    }
    
    private func configStackView() {
        let stack = UIStackView(arrangedSubviews: buttons)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stack.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stack.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        stack.backgroundColor = UIColor.init(hex: "f8f8f8")
        stack.layer.cornerRadius = 4.0
    }
    
    private func configSelectorView() {
        let selectorWidth = (Screen.width - 48) / CGFloat(self.buttonTitles.count)
        selectorView = UIView(frame: CGRect(x: 0, y: 0, width: selectorWidth, height: self.frame.height))
        selectorView.backgroundColor = UIColor.init(hexString: "ffc440")
        selectorView.layer.cornerRadius = cornerRadiusView
        addSubview(selectorView)
    }
    
    private func createButton() {
        buttons = [UIButton]()
        buttons.removeAll()
        subviews.forEach({$0.removeFromSuperview()})
        for buttonTitle in buttonTitles {
            let button = UIButton(type: .system)
            button.setTitle(buttonTitle, for: .normal)
            button.titleLabel?.font = UIFont(name: AppFont.NotoSansMedium, size: 16)
            button.addTarget(self, action:#selector(CustomSegmentView.buttonAction(sender:)), for: .touchUpInside)
            button.setTitleColor(textColor, for: .normal)
            buttons.append(button)
        }
        buttons[0].backgroundColor = UIColor.init(hexString: "ffc440")
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
        buttons[0].titleLabel?.font = UIFont(name: AppFont.NotoSansMedium, size: 16)
    }
}

