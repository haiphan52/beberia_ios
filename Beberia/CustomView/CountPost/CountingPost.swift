//
//  CountingPost.swift
//  Beberia
//
//  Created by haiphan on 20/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CountingPost: UIViewController {
    
    var countPost: CheckTimeModel?
    var textTitle: String = "Oops !"

    @IBOutlet weak var lbSub: UILabel!
    @IBOutlet weak var btClose: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    
    private var remain: Double = 0
    private var detectTime: Disposable?
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
}
extension CountingPost {
    
    private func setupUI() {
        if let model = self.countPost {
            self.remain =  model.remain ?? 0
            self.lbTitle.text = textTitle
        }
        self.setupLabel()
        self.autoRunTimeMilieReplace()
    }
    
    private func setupRX() {
        self.btClose.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.dismiss(animated: true, completion: nil)
        }.disposed(by: self.disposeBag)
    }
    
    private func clearActionMilieReplace() {
        detectTime?.dispose()
    }
    private func autoRunTimeMilieReplace() {
        detectTime?.dispose()

        detectTime = Observable<Int>.interval(.seconds(1), scheduler: MainScheduler.asyncInstance)
            .bind(onNext: { [weak self] (time) in
                guard let wSelf = self else { return }
                
                if wSelf.remain <= 0 {
                    wSelf.dismiss(animated: true, completion: nil)
                } else {
                    wSelf.remain -= 1
                    wSelf.setupLabel()
                }
            })
    }
    
    private func setupLabel() {
        let attrs1 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 14),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "5D5B5B")]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Bold", size: 14),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "141F3C")]
        
        let attrs3 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 14),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "5D5B5B")]
        
        let attributedString1 = NSMutableAttributedString(string:"Mẹ quay lại đăng bài sau ", attributes:attrs1 as [NSAttributedString.Key : Any])
        
        let attributedString2 = NSMutableAttributedString(string:"\(self.remain.getTextFromSecond()) ", attributes:attrs2 as [NSAttributedString.Key : Any])
        let attributedString3 = NSMutableAttributedString(string:"nữa nhé !\nChúc mẹ có trải nghiệm vui vẻ cùng Beberia", attributes:attrs3 as [NSAttributedString.Key : Any])
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        self.lbSub.attributedText = attributedString1
    }
}
 
