//
//  HeaderFilterTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 10/2/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class HeaderFilterTableViewCell: UITableViewHeaderFooterView {

    @IBOutlet weak var viewLocation: UIView!

    @IBOutlet weak var viewType: UIView!
    
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var imgArrowType: UIImageView!
    @IBOutlet weak var viewDistric: UIView!
    @IBOutlet weak var btnDistric: UIButton!
    @IBOutlet weak var imgArrowDistric: UIImageView!
    @IBOutlet weak var lblDistric: UILabel!
    
    var tapLocation: (_ index :Int) -> () = {_ in}
    var tapType: (_ index :Int) -> () = {_ in}
    var tapDistric: (_ index :Int) -> () = {_ in}
    override func awakeFromNib() {
        super.awakeFromNib()
        viewType.cornerRadius = 5
//        viewType.borderWidth = 1
//        viewType.borderColor = UIColor.init(hexString: AppColor.BDColor)
        viewLocation.cornerRadius = 5
//        viewLocation.borderWidth = 1
//        viewLocation.borderColor = UIColor.init(hexString: AppColor.BDColor)
        viewDistric.cornerRadius = 5
//        viewDistric.borderWidth = 1
//        viewDistric.borderColor = UIColor.init(hexString: AppColor.BDColor)
        
        // Initialization code
    }

    @IBAction func btnLocation(_ sender: Any) {
        tapLocation(btnLocation.tag)
    }
    
    @IBAction func btnType(_ sender: Any) {
        tapType(btnType.tag)
    }
    @IBAction func btnDistric(_ sender: Any) {
        tapDistric(btnType.tag)
    }
    
}
