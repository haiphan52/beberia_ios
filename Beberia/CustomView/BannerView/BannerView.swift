//
//  BannerView.swift
//  Beberia
//
//  Created by IMAC on 7/23/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit
import FSPagerView
import SDWebImage
import SnapKit

final class BannerView: UIView {
    
    enum BannerTypeRedirect: Int {
        case none, rankPoint, giftGame, info, momTok, news, listDiary, diaary, home
    }
    
    @IBOutlet weak var fsPageView: FSPagerView!
    @IBOutlet weak var fsPageControl: FSPageControl!
    @IBOutlet var containerView: UIView!
    
    final var arrayImage = [String]()
        {
        didSet{
            self.fsPageControl.numberOfPages = self.arrayImage.count
            self.fsPageView.reloadData()
        }
    }
    
    private final let kCONTENT_XIB_NAME = "BannerView"
    
    var tapBanner: (Int)->() = {_ in}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
     required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           commonInit()
       }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        containerView.fixInView(self)
    }
    
    override func awakeFromNib() {
        setupSlideShow()
        
    }
    
    func setupSlideShow(){
          fsPageView.dataSource = self
          fsPageView.delegate = self
          fsPageView.automaticSlidingInterval = 3.0
          fsPageView.interitemSpacing = 10
          fsPageView.transformer = FSPagerViewTransformer(type: .depth)
          fsPageView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
          fsPageControl.setFillColor(.gray, for: .normal)
          fsPageControl.setFillColor(UIColor.init(hexString: "ff99bb"), for: .selected)
      }
}

//MARK: FSPagerViewDelegate
extension BannerView: FSPagerViewDataSource,FSPagerViewDelegate{
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return arrayImage.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
//        cell.imageView?.contentMode = .scaleToFill
       // cell.imageView?.kf.setImage(with: URL.init(string: arrayImage[index]))
        cell.imageView?.setImage(imageString: arrayImage[index])
        if let image = cell.imageView?.image {
            cell.imageView?.image = image.resizeImageSpace(width: self.fsPageView.frame.width)
        }
        cell.contentView.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.black , opacity: 0.05, radius: 20)
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        
        tapBanner(index)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.fsPageControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.fsPageControl.currentPage = pagerView.currentIndex
    }
    
}
