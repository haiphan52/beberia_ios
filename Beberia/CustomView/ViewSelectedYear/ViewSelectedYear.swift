//
//  ViewSelectedYear.swift
//  Beberia
//
//  Created by IMAC on 7/1/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CustomPickerView: UIPickerView {

    override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.borderWidth = 0 // Main view rounded border

        // Component borders
        self.subviews.forEach {
            $0.layer.borderWidth = 0
            $0.isHidden = $0.frame.height <= 1.0
        }
    }

}

class ViewSelectedYear: UIView, UIPickerViewDelegate, UIPickerViewDataSource  {
    
    @IBOutlet weak var titileView: UILabel!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var pickerView: CustomPickerView!
    
    var disposeBag = DisposeBag()
    var data = ["1998","19909","2000"]
    var tapOK:(String)->() = {_ in}
    var valueSelected = ""
    
   override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewSelectedYear.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewSelectedYear {
        return R.nib.viewSelectedYear.instantiate(withOwner: nil)[0] as! ViewSelectedYear
    }
    
    @IBAction func didPressOK(_ sender: Any) {
        tapOK(valueSelected)
    }
    
    override func awakeFromNib() {
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
               
    }
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont (name: AppFont.NotoSans, size: 19)
        label.text =  data[row]
        label.textAlignment = .center
        label.textColor = UIColor.init(hex: "0d0d0d")
        return label
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        valueSelected = data[row]
    }
}

