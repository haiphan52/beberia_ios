//
//  ViewInput.swift
//  Beberia
//
//  Created by IMAC on 9/27/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import Foundation
import UIKit

class ViewInput: UIView, UITextViewDelegate {
    
    @IBOutlet weak var tvInputComment: UITextView!
    @IBOutlet weak var heightConstraintViewInput: NSLayoutConstraint!
    @IBOutlet weak var heightViewInput: NSLayoutConstraint!
    
    var sendComment:(_ comment: String)->() = {_ in}
    var updatHeightViewInput:(_ height: CGFloat)->() = {_ in}
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewInput.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewInput {
        return R.nib.viewInput.instantiate(withOwner: nil)[0] as! ViewInput
    }
    
    override func awakeFromNib() {
        tvInputComment.cornerRadius = 15
        tvInputComment.delegate = self
        tvInputComment.text = R.string.localizable.infomationWriteComment()
        tvInputComment.textColor = UIColor.lightGray
        tvInputComment.autocorrectionType = .no
    }
    
    @IBAction func didPressSendComment(_ sender: Any) {
        guard tvInputComment.text.count > 0 else {
            return
        }
        sendComment(tvInputComment.text)
    }
    
    func initViewInput(){
        self.tvInputComment.text = R.string.localizable.infomationWriteComment()
        self.tvInputComment.textColor = UIColor.lightGray
        self.heightViewInput.constant = 33
        self.heightConstraintViewInput.constant = 55
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
        heightConstraintViewInput.constant = newFrame.height + 22
        heightViewInput.constant = newFrame.height
        updatHeightViewInput(heightConstraintViewInput.constant)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            tvInputComment.text = R.string.localizable.infomationWriteComment()
            textView.textColor = UIColor.lightGray
        }
    }

}
