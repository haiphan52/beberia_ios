//
//  ViewSelectedBabyRightBar.swift
//  Beberia
//
//  Created by IMAC on 6/29/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class ViewSelectedBabyRightBar: UIView {
    
    @IBOutlet weak var imageAvatarBaby: UIImageView!
    @IBOutlet weak var rightButton: UIButton!
    
    var performEventSegue: Observable<Void> {
        return rightButton.rx.tap.asObservable()
    }
    
    var imageUrl = "" {
        didSet{
        //    imageAvatarBaby.loadImageUsingCache(withUrl: imageUrl)
//            let url = URL(string: imageUrl)
//            let data = try? Data(contentsOf: url ?? URL.init(string: "")!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//            imageAvatarBaby.image = UIImage(data: data!)
            
          //  imageAvatarBaby.image = R.image.appleButton()
//            imageAvatarBaby.kf.setImage(with: URL.init(string: imageUrl), placeholder: nil, options: .none, progressBlock: { (a, b) in
//                print(a)
//                print(b)
//            }) { (bc, dd) in
//                print(bc)
//                print(dd)
//            }
        }
    }
    
    var actionRightBar:()->() = {}
   
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
          guard subviews.isEmpty else { return self }
          return R.nib.viewSelectedBabyRightBar.firstView(owner: nil)
      }
      
    class func instanceFromNib(imageUrl: String = "") -> ViewSelectedBabyRightBar {
          guard let view =  R.nib.viewSelectedBabyRightBar.instantiate(withOwner: nil)[0] as? ViewSelectedBabyRightBar
            else {
            fatalError("Attempted to create TestView, failed to find object")
        }
      //  view.imageAvatarBaby.kf.setImage(with: URL.init(string: imageUrl))
     //   view.imageUrl = imageUrl
        return view
      }
    
    @IBAction func didPressTap(_ sender: Any) {
        actionRightBar()
        
    }
}

let imageCache = NSCache<NSString, UIImage>()
extension UIImageView {
    func loadImageUsingCache(withUrl urlString : String) {
        let url = URL(string: urlString)
        if url == nil {return}
        self.image = nil

        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString)  {
            self.image = cachedImage
            return
        }

        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView.init(style: .gray)
        addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityIndicator.center = self.center

        // if not, download image from url
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }

            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                    activityIndicator.removeFromSuperview()
                }
            }

        }).resume()
    }
}
