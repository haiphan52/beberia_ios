//
//  ParentBIMSegmentView.swift
//  Beberia
//
//  Created by haiphan on 13/02/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

protocol ParentBIMSegmentViewDelegate {
    func changeToIndex(index:Int)
}

class ParentBIMSegmentView: UIView {
    private var buttonTitles:[String]!
    private var buttonImages: [UIImage?]!
    private var buttons: [UIButton]!
//    private var selectorView: UIView!
    var selectedIndex : Int = 0
    var delegate: ParentBIMSegmentViewDelegate?
    
    var textColor:UIColor = UIColor.init(hexString: "000000", alpha: 0.5)!
    var selectorViewColor: UIColor = UIColor.init(hexString: "fcad24", alpha: 1)!
    var selectorTextColor: UIColor = .white
    var cornerRadiusView: CGFloat = 4.0
    
    
    convenience init(frame:CGRect,buttonTitle:[String], buttonImages: [UIImage?]) {
        self.init(frame: frame)
        self.buttonTitles = buttonTitle
        self.buttonImages = buttonImages
        updateView()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.backgroundColor = UIColor.white
        //   updateView()
    }
    
    func setupOnlyPregnant() {
        self.buttonTitles = ["Mang Thai"]
        self.buttonImages = [R.image.img_pregnant()]
        updateView()
    }
    
    func setButtonTitles(buttonTitles:[String], buttonImages: [UIImage?]) {
        self.buttonTitles = buttonTitles
        self.buttonImages = buttonImages
        self.updateView()
    }
    
    func setIndex(index:Int) {
        buttons.forEach({
            $0.setTitleColor(textColor, for: .normal)
            $0.backgroundColor = UIColor.clear
        })
        let button = buttons[index]
        selectedIndex = index
        button.setTitleColor(selectorTextColor, for: .normal)
        button.backgroundColor = UIColor.init(hexString: "ffc440")
//        let selectorPosition = (Screen.width - 48)/CGFloat(buttonTitles.count) * CGFloat(index)
//        UIView.animate(withDuration: 0.3) {
//            self.selectorView.frame.origin.x = selectorPosition
//        }
    }
    
    @objc func buttonAction(sender:UIButton) {
        for (buttonIndex, btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            btn.backgroundColor = UIColor.clear
            if btn == sender {
//                let selectorPosition = (Screen.width - 48)/CGFloat(buttonTitles.count) * CGFloat(buttonIndex)
                selectedIndex = buttonIndex
                delegate?.changeToIndex(index: selectedIndex)
                btn.setTitleColor(selectorTextColor, for: .normal)
                btn.backgroundColor = UIColor.init(hexString: "ffc440")
                //btn.layer.cornerRadius = 4
            }
        }
    }
}
//Configuration View
extension ParentBIMSegmentView {
    private func updateView() {
        configView()
        createButton()
        configStackView()
    }
    
    private func configView(){
        self.layer.cornerRadius = 24
        self.layer.borderColor = R.color.neutralgray4()?.cgColor
        self.layer.borderWidth = 1.0
        self.clipsToBounds = true
    }
    
    private func configStackView() {
        let stack = UIStackView(arrangedSubviews: buttons)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(4)
        }
//        stack.backgroundColor = UIColor.init(hex: "f8f8f8")
        stack.layer.cornerRadius = 20
    }
    
//    private func configSelectorView() {
//        let selectorWidth = (Screen.width - 48) / CGFloat(self.buttonTitles.count)
//        selectorView = UIView(frame: CGRect(x: 0, y: 0, width: selectorWidth, height: self.frame.height))
//        selectorView.backgroundColor = UIColor.init(hexString: "ffc440")
//        selectorView.layer.cornerRadius = cornerRadiusView
//        addSubview(selectorView)
//    }
    
    private func createButton() {
        buttons = [UIButton]()
        buttons.removeAll()
        subviews.forEach({$0.removeFromSuperview()})
        for (index, buttonTitle) in buttonTitles.enumerated() {
            let button = UIButton(type: .custom)
            button.setTitle("  \(buttonTitle)", for: .normal)
            button.titleLabel?.font = UIFont(name: AppFont.NotoSansMedium, size: 16)
            button.addTarget(self, action:#selector(self.buttonAction(sender:)), for: .touchUpInside)
            button.setTitleColor(textColor, for: .normal)
            button.clipsToBounds = true
            button.layer.cornerRadius = 20
            if self.buttonImages.count > 0 {
                button.setImage(self.buttonImages[index], for: .normal)
            }
            buttons.append(button)
        }
        buttons[0].backgroundColor = UIColor.init(hexString: "ffc440")
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
        buttons[0].titleLabel?.font = UIFont(name: AppFont.NotoSansMedium, size: 16)
    }
}

