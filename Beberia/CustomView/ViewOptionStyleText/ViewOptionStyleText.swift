//
//  ViewOptionStyleText.swift
//  Beberia
//
//  Created by IMAC on 9/3/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import DKImagePickerController
import Photos
import MobileCoreServices

class ViewOptionStyleText: UIView {
    
    @IBOutlet weak var btnBold: UIButton!
    @IBOutlet weak var btnUtralic: UIButton!
    @IBOutlet weak var btnItalic: UIButton!
    
    var isDiaryVC = false
    var tapTextI: ()->() = {}
    var tapTextU: ()->() = {}
    var tapTextB: ()->() = {}
    var tapSelectImage: (_ tapSelectImage : [(UIImage,Data?)])->() = {_ in}
    var isSticker: ((Bool) -> Void)?
    var selectImageDiary = 0
    var select = 0
    var isSelectI = false{
        didSet{
            btnItalic.setImage(isSelectI ? R.image.icIActive()! : R.image.icIUnActive()!, for: .normal)
        }
    }
    
    var isSelectB = false{
        didSet{
            btnBold.setImage(isSelectB ? R.image.icBActive()! : R.image.icBUnActive()!, for: .normal)
        }
    }
    
    var isSelectU = false{
        didSet{
            btnUtralic.setImage(isSelectU ? R.image.icUActive()! : R.image.icUUnActive()!, for: .normal)
        }
    }
    
    override func awakeFromNib() {
    
    }

    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewOptionStyleText.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewOptionStyleText {
        return R.nib.viewOptionStyleText.instantiate(withOwner: nil)[0] as! ViewOptionStyleText
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage? {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail:UIImage? = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result
        })
        return thumbnail
    }
    
    @IBAction func didPresdPhoto(_ sender: Any) {
        if isDiaryVC {
            if self.selectImageDiary < 5 {
              //  var images = [UIImage]()
                let pickerController = DKImagePickerController()
                pickerController.maxSelectableCount = 1
                pickerController.didSelectAssets = { [weak self] (assets: [DKAsset])  in
                    print("didSelectAssets")
                    if assets.count > 0{
                      //  print(assets)
                        let requestOptions = PHImageRequestOptions()
                        requestOptions.deliveryMode = .highQualityFormat
                        requestOptions.isSynchronous = true
                        self?.selectImageDiary += 1
                    //    print(self?.select)
                    //    for asset in assets {
                        assets[0].fetchOriginalImage(options: requestOptions, completeBlock: { [weak self] image, info in
                                if let img = image {
                                  
                                        let editPhotoVC = EditPhotoHomeViewController.init(nib: R.nib.editPhotoHomeViewController)
                                        let naviEditPhotoVC = UINavigationController.init(rootViewController: editPhotoVC)
                                        naviEditPhotoVC.modalPresentationStyle = .overFullScreen
                                        editPhotoVC.photoSelect = img
                                        editPhotoVC.isDiary = true
                                        guard let asset = assets[0].originalAsset else {
                                            return
                                        }
                                        editPhotoVC.thumbnailImage = CIImage.init(image: (self?.getAssetThumbnail(asset: asset))!)!
                                        editPhotoVC.editPhotoDone = { image in
                                            self?.tapSelectImage([(image.0, nil)])
                                            self?.isSticker?(image.1)
                                        }
                                        Utils.getTopMostViewController()?.present(naviEditPhotoVC, animated: true) {}
                                }
                            })
                    }
                }
                Utils.getTopMostViewController()?.present(pickerController, animated: true) {}
            }
            else {
                ViewOptionStyleText.showAlertView( title: "Max photo limit reached" , message: "You can select 5 item")
            }
        }else{
            if self.select < 5 {
                var images = [(UIImage, Data?)]()
                let pickerController = DKImagePickerController()
                pickerController.maxSelectableCount = 5 - self.select
                pickerController.didSelectAssets = { [weak self] (assets: [DKAsset])  in
                    print("didSelectAssets")
                    if assets.count > 0{
                        //  print(assets)
                        let requestOptions = PHImageRequestOptions()
                        requestOptions.deliveryMode = .highQualityFormat
                        requestOptions.isSynchronous = true
                        self?.select = (self?.select ?? 0) + assets.count
                        //  print(self?.select)
                        for asset in assets {
                            asset.fetchOriginalImage(options: requestOptions, completeBlock: { [weak self] image, info in
                                if let img = image {
                                    var data1: Data? = nil
                                    var isGif = false
                                    if let data = info!["PHImageFileDataKey"] as? Data {
                                        isGif = Utils.isAnimatedImage(data)
                                        print("isAnimated: \(isGif)")
                                    }

                                    if isGif
                                    {
                                        asset.fetchImageData { (data, info) in
                                            data1 = data
                                            images.append((img,data1))
                                            if images.count == assets.count{
                                                self?.tapSelectImage(images)
                                            }
                                        }
                                    }else
                                    {
                                        data1 = nil
                                        images.append((img,data1))
                                        if images.count == assets.count{
                                            self?.tapSelectImage(images)
                                        }
                                    }
                                    
                                    
                                    
                                }
                            })
                        }
                    }
                }
                Utils.getTopMostViewController()?.present(pickerController, animated: true) {}
            }
            else {
                ViewOptionStyleText.showAlertView( title: "Max photo limit reached" , message: "You can select 5 item")
            }
        }
      

    }
    @IBAction func didPressTextI(_ sender: UIButton) {
        isSelectI = !isSelectI
        self.tapTextI()
    }
    @IBAction func didPressU(_ sender: UIButton) {
        isSelectU = !isSelectU
        self.tapTextU()
    }
    @IBAction func didPressTextB(_ sender: UIButton) {
        isSelectB = !isSelectB
        self.tapTextB()
    }
    class func showAlertView( title : String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
        Utils.getTopMostViewController()?.present(alert, animated: true, completion: nil)
    }
}
