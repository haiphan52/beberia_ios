//
//  SelectActivityViewController.swift
//  Beberia
//
//  Created by IMAC on 6/18/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class SelectActivityViewController: BottomPopupViewController {

      var height: CGFloat?
        var topCornerRadius: CGFloat?
        var presentDuration: Double?
        var dismissDuration: Double?
        var shouldDismissInteractivelty: Bool?
    
    var selectedActivity:(String)->() = {_ in}
     
     override func viewDidLoad() {
         super.viewDidLoad()

         // Do any additional setup after loading the view.
     }
     
     override func getPopupHeight() -> CGFloat {
         return height ?? CGFloat(300)
     }
     
     override func getPopupTopCornerRadius() -> CGFloat {
         return topCornerRadius ?? CGFloat(10)
     }
     
     override func getPopupPresentDuration() -> Double {
         return presentDuration ?? 1.0
     }
     
     override func getPopupDismissDuration() -> Double {
         return dismissDuration ?? 1.0
     }
     
     override func shouldPopupDismissInteractivelty() -> Bool {
         return shouldDismissInteractivelty ?? true
     }
    
    @IBAction func didPressSelect(_ sender: UIButton) {
        selectedActivity(sender.titleLabel?.text ?? "")
    }
}
