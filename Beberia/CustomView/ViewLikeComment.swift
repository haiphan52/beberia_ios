//
//  ViewLikeComment.swift
//  Beberia
//
//  Created by IMAC on 8/30/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import Foundation
import UIKit
//import DTOverlayController

enum FromToview {
    case Home
    case Info
    case Diary
    case Secret
}

class ViewLikeComment: UIView {
    
    @IBOutlet weak var lblSave: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var lblNumberLike: UILabel!
    @IBOutlet weak var lblNumberComment: UILabel!
    @IBOutlet weak var imgSave: UIImageView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var viewSave: UIView!
    
    var tapButtonLike: ()->() = {}
    var tapButtonComment: ()->() = {}
    var tapButtonShare: ()->() = {}
    
    var postDetailModel = PostDetailModel.init(json: "")
    var homeFeed = HomeFeed.init(json: "")
    var numberLike: Int = 0
    var numberComment: Int = 0
    var id : Int = 0
    var pressLike = 0
    var fromToView = FromToview.Home
    {
        didSet{
            if fromToView == .Secret{
                viewSave.isHidden = true
            }
        }
    }
    
        var isFavourite = 0
        {
            didSet{
                imgSave.image = (isFavourite == 1) ? R.image.ic_CheckMark() : R.image.ic_Mark()
                lblSave.text = (isFavourite == 1) ? R.string.localizable.commonSaved() : R.string.localizable.commonSave()
            }
        }
    
        var isLike = 0
        {
            didSet{
                imgLike.image = (isLike == 1) ? R.image.icLikeActive() : R.image.ic_like()
            }
        }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewLikeComment.firstView(owner: nil) //Bundle.main.loadNibNamed("ViewLikeComment", owner: nil, options: nil)?.first
    }
    
    class func instanceFromNib() -> ViewLikeComment {
        return R.nib.viewLikeComment.instantiate(withOwner: nil)[0] as! ViewLikeComment // UINib(nibName: "ViewLikeComment", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ViewLikeComment
    }
    
    func loadData(){
        lblNumberLike.text = R.string.localizable.homeLike()
        lblNumberComment.text = R.string.localizable.homeComment()
        
        if self.homeFeed.id == nil{
          //  lblNumberLike.text = "\(self.postDetailModel.likeNumber ?? 0)"
          //  lblNumberComment.text = "\(self.postDetailModel.commentNumber ?? 0)"
            numberLike = self.postDetailModel.likeNumber ?? 0
            numberComment = self.postDetailModel.commentNumber ?? 0
            isFavourite = self.postDetailModel.isFavourite ?? 0
            isLike = self.postDetailModel.isLiked ?? 0
          
            id = self.postDetailModel.id ?? 0
        }else{
          //  lblNumberLike.text = "\(self.homeFeed.numberLike ?? 0)"
          //  lblNumberComment.text = "\(self.homeFeed.numberComment ?? 0)"
            numberLike = self.homeFeed.numberLike ?? 0
            numberComment = self.homeFeed.numberComment ?? 0
            isFavourite = self.homeFeed.checkSave ?? 0
            isLike = self.homeFeed.checkLike ?? 0
        
            id = self.homeFeed.id ?? 0
        }
        self.btnLike.isEnabled = (self.id != 0) ? true : false
    }
    
    func saveFeed(){
        APIManager.saveHomeFeed(id: id, checkSave: isFavourite , callbackSuccess: { [weak self] (checkSave) in
            guard let self = self else { return }
            self.isFavourite = checkSave
            self.homeFeed.checkSave = checkSave
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func likeFeed(){
        btnLike.isEnabled = false
  
        APIManager.likeHomeFeed(id: id, type: 1, checkLike:isLike , callbackSuccess: { [weak self] (numberLike, checkLike) in
            guard let self = self else { return }
            self.isLike = checkLike
            self.numberLike = numberLike
            self.btnLike.isEnabled = true
            self.homeFeed.checkLike = checkLike
            self.homeFeed.numberLike = numberLike
            NotificationCenter.default.post(name: NSNotification.Name.updateNumberLike, object: [Key.KeyNotification.numberLike : self.numberLike], userInfo: nil)
            
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func likePost(){
        btnLike.isEnabled = false
        APIManager.likePost(idPost: id, type: 1, callbackSuccess: { [weak self] (numberLike, checkLike) in
            
            guard let self = self else { return }
            
                self.isLike = checkLike
                self.numberLike = numberLike
         //       self.lblNumberLike.text = "\(self.numberLike)"
                self.btnLike.isEnabled = true
            
            switch self.fromToView {
            case .Diary:
                print("Like Diary")
                self.homeFeed.checkLike = checkLike
                self.homeFeed.numberLike = numberLike
            case .Info:
                print("Like Home")
               self.postDetailModel.likeNumber = numberLike
                self.postDetailModel.isLiked = checkLike
            case .Home:
                 print("Like Home")
            case .Secret:
                print("Like Secret")
            }
            NotificationCenter.default.post(name: NSNotification.Name.updateNumberLike, object: [Key.KeyNotification.numberLike : self.numberLike], userInfo: nil)
            
          //  }
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func favouritePost(){
        APIManager.favouritePost(idPost: id, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else { return }
            if isSuccess{
                self.isFavourite = (self.isFavourite == 0) ? 1 : 0
            }
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    @IBAction func didPressSave(_ sender: Any) {
        if Utils.checkLogin(selfView: Utils.getTopMostViewController()!){
            switch fromToView {
                case .Info, .Diary:
                    favouritePost()
                case .Home:
                    saveFeed()
            case .Secret:
                print("Save Secret")
            }
        }
    }
    
    @IBAction func didPessLike(_ sender: Any) {
        if Utils.checkLogin(selfView: Utils.getTopMostViewController()!){
            switch fromToView {
            case .Info, .Diary:
                likePost()
            case .Home, .Secret:
              
//                if isLike == 1 {
//                      var numberLike = Int(lblNumberLike.text!)
//                      numberLike! -= 1
//                      lblNumberLike.text = "\(numberLike ?? 0)"
//                     imgLike.image = R.image.ic_like()
//                     NotificationCenter.default.post(name: NSNotification.Name.updateNumberLike, object: [Key.KeyNotification.numberLike : numberLike], userInfo: nil)
//                  }else {
//                      var numberLike = Int(lblNumberLike.text!)
//                      numberLike! += 1
//                      lblNumberLike.text = "\(numberLike ?? 0)"
//                      imgLike.image = R.image.ic_CheckLike()
//                      NotificationCenter.default.post(name: NSNotification.Name.updateNumberLike, object: [Key.KeyNotification.numberLike : numberLike], userInfo: nil)
//                      isLike = 1
//                  }
                likeFeed()
            }
        }
    }
    
    @IBAction func didPressComment(_ sender: Any) {
        if Utils.checkLogin(selfView: Utils.getTopMostViewController()!){
            tapButtonComment()
        }
//        let commentVC = CommentViewController.init(nib: R.nib.commentViewController)
//
//        switch fromToView {
//        case .Info, .Diary:
//            commentVC.fromToView = .Info
//        case .Home:
//            commentVC.fromToView = .Home
//        }
//
//        commentVC.numberComment = numberComment
//        commentVC.idPost = id
//        let overlayController = DTOverlayController(viewController: commentVC)
//        overlayController.isPanGestureEnabled = false
//        overlayController.overlayHeight = .dynamic(0.9)
//        Utils.getTopMostViewController()!.present(overlayController, animated: true, completion: nil)
    }
    
}
