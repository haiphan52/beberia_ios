//
//  ViewLikeCommentFeed.swift
//  Beberia
//
//  Created by Lap on 1/8/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit

class ViewLikeCommentFeed: UIView {
    @IBOutlet weak var lblNumberComment: UILabel!
    @IBOutlet weak var lblNumberLike: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var icComment: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    
    var tapComment:()->() = {}
    var tapLike:(_ homeFeed: HomeFeed)->() = {_ in}
    var tapSave:(_ homeFeed: HomeFeed)->() = {_ in}
    var homeFeed = HomeFeed.init(json: "")
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewLikeCommentFeed.firstView(owner: nil) //Bundle.main.loadNibNamed("ViewLikeComment", owner: nil, options: nil)?.first
    }
    
    class func instanceFromNib() -> ViewLikeCommentFeed {
        return R.nib.viewLikeCommentFeed.instantiate(withOwner: nil)[0] as! ViewLikeCommentFeed // UINib(nibName: "ViewLikeComment", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ViewLikeComment
    }
    
    func setupData(homeFeed: HomeFeed){
        self.homeFeed = homeFeed
        lblNumberComment.text = "\(homeFeed.numberComment ?? 0)"
        lblNumberLike.text = "\(homeFeed.numberLike ?? 0)"
        
        imgLike.image = R.image.icLikeNew()
        if homeFeed.checkLike == 1 {
            imgLike.image = R.image.icLikeActive()
        }
        
        btnSave.setImage(R.image.icSaveNew(), for: .normal)
        if homeFeed.checkSave == 1 {
            btnSave.setImage(R.image.icSaveActive(), for: .normal)
        }
        
        icComment.image = R.image.icCommentNew()
        if homeFeed.numberComment ?? 0 > 0 {
            icComment.image = R.image.icCommentActive()
        }
        
        var location = ""
        location = "\(homeFeed.district?.districtName ?? "") - \(homeFeed.cityName?.locationNameVi ?? "")"
        if homeFeed.district?.districtName ?? "" == "" {
            location = homeFeed.cityName?.locationNameVi ?? ""
        }
        lblLocation.text = location
    }
    
    @IBAction func didPressLike(_ sender: Any) {
        likeFeed()
    }
    
    @IBAction func didPressComment(_ sender: Any) {
        tapComment()
    }
    
    @IBAction func didPressBtnSave(_ sender: Any) {
        saveFeed()
    }
    
    func saveFeed(){
        guard let id = homeFeed.id, let isFavourite = homeFeed.checkSave else {
            return
        }
        
        self.homeFeed.checkSave = isFavourite == 0 ? 1 : 0
        self.setupData(homeFeed: self.homeFeed)
        self.tapSave(self.homeFeed)
        
        APIManager.saveHomeFeed(id: id, checkSave: isFavourite , callbackSuccess: { [weak self] (checkSave) in
            guard let self = self else { return }
            if self.homeFeed.checkSave != checkSave {
                self.homeFeed.checkSave = checkSave
                self.setupData(homeFeed: self.homeFeed)
                self.tapSave(self.homeFeed)
            }
            
            
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func likeFeed(){
        guard let id = homeFeed.id, let isLike = homeFeed.checkLike else {
            return
        }
        
        self.homeFeed.checkLike = isLike == 0 ? 1 : 0
        self.homeFeed.numberLike = self.homeFeed.checkLike == 0 ? (self.homeFeed.numberLike ?? 0) - 1 : (self.homeFeed.numberLike ?? 0) + 1
        self.setupData(homeFeed: self.homeFeed)
        self.tapLike(self.homeFeed)
  
        APIManager.likeHomeFeed(id: id, type: 1, checkLike:isLike , callbackSuccess: { [weak self] (numberLike, checkLike) in
            guard let self = self else { return }
            if self.homeFeed.checkLike != self.homeFeed.checkLike {
                self.homeFeed.checkLike = checkLike
                self.homeFeed.numberLike = numberLike
                self.setupData(homeFeed: self.homeFeed)
                self.tapLike(self.homeFeed)
            }
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
}
