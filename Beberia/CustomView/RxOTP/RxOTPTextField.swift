//
//  RxOTPTextField.swift
//
//  Created by OS on 4/16/20.
//

import UIKit

public protocol RxOTPTextFieldDelegate: class {
	func deleteBackward(sender: RxOTPTextField, prevValue: String?)
}

open class RxOTPTextField: UITextField {
    
    weak open var deleteDelegate: RxOTPTextFieldDelegate?

    override open func deleteBackward() {
		let prevValue = text
		super.deleteBackward()
        deleteDelegate?.deleteBackward(sender: self, prevValue: prevValue)
    }
}
