//
//  RxOTPView.swift
//
//  Created by OS on 4/16/20.
//

import UIKit

@objc
public protocol RxOTPViewDelegate: class {
	func codeView(sender: RxOTPView, didFinishInput code: String) -> Bool
}

@IBDesignable
open class RxOTPView: UIControl {
	@IBInspectable open var length: Int = 8 {
		didSet {
			setupUI()
		}
	}

	@IBOutlet open weak var delegate: RxOTPViewDelegate?

	var stackView: UIStackView = {
		let stackView = UIStackView()
		stackView.axis = .horizontal
        stackView.distribution = .fillEqually
		stackView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        stackView.spacing = 3
		return stackView
	}()

	fileprivate var items: [RxOTPItemView] = []
	open var code: String {
		get {
			let items = stackView.arrangedSubviews.map({$0 as! RxOTPItemView})
			let values = items.map({$0.textField.text ?? ""})
			return values.joined()
		}
		set {
			let array = newValue.map(String.init)
			for i in 0..<length {
				let item = stackView.arrangedSubviews[i] as! RxOTPItemView
				item.textField.text = i < array.count ? array[i] : ""
			}
			if !stackView.arrangedSubviews.compactMap({$0 as? UITextField}).filter({$0.isFirstResponder}).isEmpty {
				self.becomeFirstResponder()
			}
		}
	}
    
//    open var errored: Bool {
//        get { return false }
//        set {
//            stackView.arrangedSubviews
//            .map({$0 as! RxOTPItemView})
//            .forEach { codeView in
//                codeView.layer.borderWidth = newValue ? 1 : 0
//                codeView.layer.borderColor = newValue ? R.color.tomato60()!.cgColor : UIColor.clear.cgColor
//            }
//        }
//    }

	override open func awakeFromNib() {
		super.awakeFromNib()
		setupUI()

		let tap = UITapGestureRecognizer(target: self, action: #selector(becomeFirstResponder))
		addGestureRecognizer(tap)
	}
    
//    func toggleError(_ error: Bool) {
//        stackView.arrangedSubviews
//            .map({$0 as! RxOTPItemView})
//            .forEach { codeView in
//                codeView.layer.borderWidth = error ? 1 : 0
//                codeView.layer.borderColor = error ? R.color.tomato60()!.cgColor : UIColor.clear.cgColor
//            }
//    }

	fileprivate func setupUI() {
		stackView.frame = self.bounds
        stackView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(stackView)
		if stackView.superview == nil {
			addSubview(stackView)
		}
		stackView.arrangedSubviews.forEach{($0.removeFromSuperview())}

		for i in 0..<length {
			let itemView = generateItem()
            
			itemView.textField.deleteDelegate = self
			itemView.textField.delegate = self
			itemView.tag = i
			itemView.textField.tag = i
            itemView.textField.tintColor = .black
			stackView.addArrangedSubview(itemView)
		}
	}

	open func generateItem() -> RxOTPItemView {
		let type = RxOTPItemView.self
		let typeStr = type.description().components(separatedBy: ".").last ?? ""
		let bundle = Bundle(for: type)
		let view =  bundle
			.loadNibNamed(typeStr,
						  owner: nil,
						  options: nil)?
			.last as! RxOTPItemView
        
        view.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        
        return view
	}

	@discardableResult
	override open func becomeFirstResponder() -> Bool {
		let items = stackView.arrangedSubviews
			.map({$0 as! RxOTPItemView})
//         let index = items.filter { (tf) -> Bool in
//            return (tf.textField.text ?? "").isEmpty && (tf.textField.text ?? "").count > 1
//        }
//        return (index.first ?? items.last)!.becomeFirstResponder()
         
		return (items.filter({($0.textField.text ?? "").isEmpty}).first ?? items.last)!.becomeFirstResponder()
	}

	@discardableResult
	override open func resignFirstResponder() -> Bool {
		stackView.arrangedSubviews.forEach({$0.resignFirstResponder()})
		return true
	}

	override open func prepareForInterfaceBuilder() {
		super.prepareForInterfaceBuilder()
		setupUI()
	}
}

extension RxOTPView: UITextFieldDelegate, RxOTPTextFieldDelegate {

	public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

		if string == "" { //is backspace
			return true
		}

		if !textField.hasText {
			let index = textField.tag
			let item = stackView.arrangedSubviews[index] as! RxOTPItemView
			item.textField.text = string
			sendActions(for: .valueChanged)
			if index == length - 1 { //is last textfield
				if (delegate?.codeView(sender: self, didFinishInput: self.code) ?? false) {
					textField.resignFirstResponder()
				}
				return false
			}

			_ = stackView.arrangedSubviews[index + 1].becomeFirstResponder()
		}

		return false
	}

	public func deleteBackward(sender: RxOTPTextField, prevValue: String?) {
		for i in 1..<length {
			let itemView = stackView.arrangedSubviews[i] as! RxOTPItemView

			guard itemView.textField.isFirstResponder, (prevValue?.isEmpty ?? true) else {
				continue
			}

			let prevItem = stackView.arrangedSubviews[i-1] as! RxOTPItemView
			if itemView.textField.text?.isEmpty ?? true {
				prevItem.textField.text = ""
				_ = prevItem.becomeFirstResponder()
			}
		}
		sendActions(for: .valueChanged)
	}
}
