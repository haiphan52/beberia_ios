//
//  RxOTPItemView.swift
//
//  Created by OS on 4/16/20.
//

import UIKit

open class RxOTPItemView: UIView {

    @IBOutlet open weak var textField: RxOTPTextField!
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        textField.text = ""
        layer.cornerRadius = 10
        isUserInteractionEnabled = false
    }
    
    override open func becomeFirstResponder() -> Bool {
        return textField.becomeFirstResponder()
    }
    
    override open func resignFirstResponder() -> Bool {
        return textField.resignFirstResponder()
    }
}
