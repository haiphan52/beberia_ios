//
//  RxOTPView+Rx.swift
//
//  Created by OS on 4/16/20.
//

import UIKit

#if canImport(RxCocoa)
import RxSwift
import RxCocoa

public extension Reactive where Base: RxOTPView {
    
    /// Reactive wrapper for `code` property.
    var code: ControlProperty<String> {
        return controlProperty(editingEvents: [.allEditingEvents, .valueChanged],
                               getter: { codeView in
                                codeView.code
        }, setter: { codeView, value in
            codeView.code = value
        })
    }
    
//    var errored: Binder<Bool> {
//        return Binder(base) { (codeView, value) in
//            codeView.errored = value
//        }
//    }
}
#endif
