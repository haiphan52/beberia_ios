//
//  IntroduceBabeFace.swift
//  Beberia
//
//  Created by haiphan on 07/08/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import CloudKit

protocol IntroduceBabeFaceDelegate: AnyObject {
    func selectAction(action: IntroduceBabeFace.Action)
}

class IntroduceBabeFace: UIView, SetUpFunctionBase {
    
    enum Action: Int, CaseIterable {
        case introduce, rate, question
    }
    
    
    @IBOutlet var bts: [UIButton]!
    @IBOutlet var views: [UIView]!
    @IBOutlet var lbs: [UILabel]!
    var delegate: IntroduceBabeFaceDelegate?
    @IBOutlet weak var lineView: UIView!
    
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
    
}
extension IntroduceBabeFace {
    
    func setupUI() {
        
    }
    
    func setupRX() {
        Action.allCases.forEach { type in
            let bt = self.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let self = self else { return }
                var f = self.lineView.frame
                UIView.animate(withDuration: 0.2) {
                    f.origin.x = self.views[type.rawValue].frame.origin.x
                    self.lineView.frame = f
                }
                self.lbs.forEach { lb in
                    lb.textColor = R.color.d4D4D()
                }
                self.lbs[type.rawValue].textColor = R.color.fd799D()
                self.delegate?.selectAction(action: type)
            }.disposed(by: disposeBag)
        }
    }
    
}
