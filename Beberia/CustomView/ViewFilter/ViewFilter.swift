//
//  ViewFilter.swift
//  Beberia
//
//  Created by IMAC on 6/15/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewFilter: UIView {
    
    @IBOutlet weak var btnTheoVAccin: UIButton!
    @IBOutlet weak var btnTheoThangTuoi: UIButton!
    @IBOutlet weak var btnDaTiem: UIButton!
    @IBOutlet weak var btnChuaTiem: UIButton!
    @IBOutlet weak var imgTheoVaccin: UIImageView!
    @IBOutlet weak var imgTheoThangTuoi: UIImageView!
    @IBOutlet weak var imgDaTiem: UIImageView!
    @IBOutlet weak var imgChuaTiem: UIImageView!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var clvItem: UICollectionView!
    @IBOutlet weak var viewFilterAge: UIView!
    @IBOutlet weak var viewFilterVaccin: UIView!
    @IBOutlet weak var heightClv: NSLayoutConstraint!
    @IBOutlet weak var heightLable: NSLayoutConstraint!
    @IBOutlet weak var lable: UILabel!
    
    @IBAction func didPressFilter(_ sender: UIButton) {
        imgChuaTiem.image = sender.tag == 21 ? R.image.ic_mark()! : R.image.ic_unMark()!
        imgDaTiem.image = sender.tag == 22 ? R.image.ic_mark()! : R.image.ic_unMark()!
        
        setHideView(bool: sender.tag == 21)
       
        if sender.tag == 21 {
             setHideClv(bool: true)
            imgTheoThangTuoi.image = R.image.ic_unMark()!
            imgTheoVaccin.image = R.image.ic_unMark()!
        }
        indexSelected = sender.tag
    }
    
    @IBAction func didPressFilterTheo(_ sender: UIButton) {
        
        imgTheoThangTuoi.image = sender.tag == 23 ? R.image.ic_mark()! : R.image.ic_unMark()!
        imgTheoVaccin.image = sender.tag == 24 ? R.image.ic_mark()! : R.image.ic_unMark()!
        lable.text = sender.tag == 23 ? "Theo tháng tuổi" : "Theo Vaccine"
        
//        if sender.tag == 23 {
//            sender.isSelected = !sender.isSelected
//            imgTheoThangTuoi.image = sender.isSelected ? R.image.ic_tickBox()! : R.image.ic_untickBox()!
//        }
//
//        if sender.tag == 24 {
//            sender.isSelected = !sender.isSelected
//            imgTheoVaccin.image = sender.isSelected ? R.image.ic_tickBox()! : R.image.ic_untickBox()!
//        }
        
        setHideClv(bool: false)
        let listTemp = sender.tag == 23 ? Utils.getAppDelegate().listDataAges : Utils.getAppDelegate().listDataVacccines
        listItemVaccine.accept(listTemp)
        indexSelected = sender.tag
    }
    
    var idSelected = 0
    var indexSelected = 0
    var disposeBag = DisposeBag()
    var listItemVaccine = BehaviorRelay.init(value: [DataVaccineModel]())
    var selectedFilter :(Int, Int, Int)->() = {_,_,_ in}
    
  //  is_vaccinated: 2, age_id: 0, vaccine_id: 0
    
    override func awakeFromNib() {
        initRx()
        
        setHideView(bool: true)
        setHideClv(bool: true)
    }
    
    func setHideClv(bool : Bool){
       heightClv.constant = bool ? 0 : 142
        heightLable.constant = bool ? 0 : 40
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func setHideView(bool : Bool){
        viewFilterAge.isHidden = bool
        viewFilterVaccin.isHidden = bool
    }
    
    func initRx(){
        
        let flowLayoutAfterBefore = UICollectionViewFlowLayout()
        flowLayoutAfterBefore.itemSize = CGSize(width: (UIScreen.main.bounds.width - 40 - 52 - 12) / 3, height: 35)
        flowLayoutAfterBefore.scrollDirection = .vertical
        flowLayoutAfterBefore.minimumInteritemSpacing = 0
        flowLayoutAfterBefore.minimumLineSpacing = 6
        flowLayoutAfterBefore.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvItem.setCollectionViewLayout(flowLayoutAfterBefore, animated: true)
        clvItem.register(R.nib.itemVaccineCollectionViewCell)
        
        listItemVaccine.asObservable().observeOn(MainScheduler.asyncInstance).bind(to: self.clvItem.rx.items(cellIdentifier: R.reuseIdentifier.itemVaccineCollectionViewCell.identifier, cellType: ItemVaccineCollectionViewCell.self)) { [weak self]  row, data, cell in
            guard let self = self else { return }
            cell.nameVaccineLable.text = data.name ?? ""
            
            cell.nameVaccineLable.backgroundColor = UIColor.init(hexString: "f6f6f6", alpha: 1.0)
            cell.nameVaccineLable.textColor = UIColor.init(hexString: "000000", alpha: 0.4)
            if data.isSeclected {
                self.idSelected = data.id ?? 0
                cell.nameVaccineLable.backgroundColor = UIColor.init(hexString: "ffc440", alpha: 1.0)
                cell.nameVaccineLable.textColor = UIColor.init(hexString: "ffffff", alpha: 1.0)
            }
            
        }.disposed(by: disposeBag)
        
        Observable
            .zip(clvItem.rx.itemSelected, clvItem.rx.modelSelected(DataVaccineModel.self))
            .bind { [weak self] indexPath, data in
                
                guard let self = self else { return }

                var items = self.listItemVaccine.value
                for (i,_) in items.enumerated(){
                    items[i].isSeclected = false
                }
                
                let index = items.firstIndex { (re) -> Bool in
                    return re.id == data.id
                }
                
                if index != nil{
                    items[index!].isSeclected = true
                    self.listItemVaccine.accept(items)
                }
                
        }.disposed(by: disposeBag)
        
        
    }
    
    @IBAction func didPressOK(_ sender: Any) {
        switch indexSelected {
        case 21:
            selectedFilter(0,0,0)
        case 22:
            selectedFilter(1,0,0)
        case 23:
            selectedFilter(1,idSelected,0)
        case 24:
            selectedFilter(1,0,idSelected)
            
        default:
            selectedFilter(2,0,0)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewFilter.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewFilter {
        return R.nib.viewFilter.instantiate(withOwner: nil)[0] as! ViewFilter
    }
}
