//
//  ViewTitleHome.swift
//  Beberia
//
//  Created by IMAC on 1/9/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit

class ViewTitleHome: UIView {
    
    @IBOutlet weak var widthImage: NSLayoutConstraint!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var btBack: UIButton!
    
    var tapAvatar: ()->() = {}
    var tapBack: ()->() = {}
    var tapSearch: ()->() = {}
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
          guard subviews.isEmpty else { return self }
          return R.nib.viewTitleHome.firstView(owner: nil) //Bundle.main.loadNibNamed("ViewLikeComment", owner: nil, options: nil)?.first
      }
    
      override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
      }
    
      class func instanceFromNib() -> ViewTitleHome {
          return R.nib.viewTitleHome.instantiate(withOwner: nil)[0] as! ViewTitleHome // UINib(nibName: "ViewLikeComment", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ViewLikeComment
      }
    
    @IBAction func tapSearch(_ sender: Any) {
        tapSearch()
    }
    @IBAction func tapBtBack(_ sender: UIButton) {
        tapBack()
    }
    
    @IBAction func didPressAvatar(_ sender: UIButton) {
        tapAvatar()
    }
}
