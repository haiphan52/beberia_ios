//
//  VaccineService.swift
//  Beberia
//
//  Created by IMAC on 6/25/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Moya
import SwiftyJSON

extension APIManager {
    
    // MARK: Vaccine
    
    static func getVaccineSchedule (page: Int,
                                    is_vaccinated: Int,
                                    age_id: Int,
                                    vaccine_id: Int,
                                    baby_id: Int,
                                    type: Int,
                                    callbackSuccess: @escaping (_ typeCar: [VaccineModel], _ page: Int) -> Void,
                                    failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameters: Parameters = [
            "page": page,
            "is_vaccinated" : is_vaccinated,
            "age_id" : age_id,
            "vaccine_id" : vaccine_id,
            "baby_id" : baby_id,
            "type": type
        ]
        self.requestJson(target: .getVaccineSchedule(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonhSearchHome = json["data"]["datas"].arrayValue
                    var searchHome = [VaccineModel]()
                    for jsonHomeFeed in jsonhSearchHome{
                        searchHome.append(VaccineModel.init(json: jsonHomeFeed))
                    }
                    callbackSuccess(searchHome, json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getPregnancySchedule (is_cared: Int, baby_id: Int , callbackSuccess: @escaping (_ typeCar: [PregnancyModel]) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameters: Parameters = [
            "is_cared" : is_cared,
            "baby_id" : baby_id
        ]
        self.requestJson(target: .getPregnancySchedule(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonhSearchHome = json["data"]["datas"].arrayValue
                    var searchHome = [PregnancyModel]()
                    for jsonHomeFeed in jsonhSearchHome{
                        searchHome.append(PregnancyModel.init(json: jsonHomeFeed))
                    }
                    callbackSuccess(searchHome)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getAgesVaccine (callbackSuccess: @escaping (_ typeCar: [DataVaccineModel]) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameters: Parameters = [:]
        self.requestJson(target: .getAgesVaccine(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonhSearchHome = json["data"].arrayValue
                    var searchHome = [DataVaccineModel]()
                    for jsonHomeFeed in jsonhSearchHome{
                        searchHome.append(DataVaccineModel.init(json: jsonHomeFeed))
                    }
                    callbackSuccess(searchHome)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getDataVaccine (callbackSuccess: @escaping (_ typeCar: [DataVaccineModel]) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameters: Parameters = [:]
        self.requestJson(target: .getDataVaccine(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonhSearchHome = json["data"].arrayValue
                    var searchHome = [DataVaccineModel]()
                    for jsonHomeFeed in jsonhSearchHome{
                        searchHome.append(DataVaccineModel.init(json: jsonHomeFeed))
                    }
                    callbackSuccess(searchHome)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func markVaccine(typeCreateOrUpdate: Int,
                             schedule_id: Int,
                             baby_id: Int,
                             time: Int,
                             type: Int,
                             callbackSuccess: @escaping (_ typeCar: VaccineModel?, _ pregnancyModel: PregnancyModel?) -> Void,
                             failed callbackFailure: @escaping (_ mess: String ) -> Void) {
           
           let parameters: Parameters = [
            "schedule_id": schedule_id,
            "baby_id": baby_id,
            "time": time,
            "type": type,
        ]
        
        let request = typeCreateOrUpdate == 0 ? BeberiaService.markVaccine(param: parameters) : BeberiaService.updateTimeVaccine(param: parameters)
        
           self.requestJson(target: request, success: { (response) in
               do {
                   let json = try JSON.init(data: response.data)
                   if json["status"].intValue == 200{
                    if  json["data"]["sick"].string != nil{
                        callbackSuccess(VaccineModel.init(json: json["data"]) , nil)
                    }else{
                        callbackSuccess(nil, PregnancyModel.init(json: json["data"]))
                    }
                       
                   }else{
                       callbackFailure(json["message"].stringValue)
                   }
               } catch {
                   callbackFailure("Có lỗi xảy ra")
               }
           }, error: { (error) in
               callbackFailure(error.localizedDescription)
           }) { (error) in
               callbackFailure(error.localizedDescription)
           }
       }
}
