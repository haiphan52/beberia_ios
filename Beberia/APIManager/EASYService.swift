//
//  EASYService.swift
//  Beberia
//
//  Created by IMAC on 6/30/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Moya
import SwiftyJSON

extension APIManager {
    
    static func getEasyActive ( callbackSuccess: @escaping (_ typeCar: ESAYModel) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameters: Parameters = [:]
        
        self.requestJson(target: .getEasyActive(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(ESAYModel.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getEasyDetail (idEASY: Int, callbackSuccess: @escaping (_ typeCar: ESAYModel) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameters: Parameters = [ "id" : idEASY ]
        
        self.requestJson(target: .getEasyDetail(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(ESAYModel.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getListEasy (userID: Int,page: Int, callbackSuccess: @escaping (_ typeCar: [ListEASYModel]) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
           
        let parameters: Parameters = [
            "user_id": userID,
            "page": page
        ]
           
           self.requestJson(target: .getListEasy(param: parameters), success: { (response) in
               do {
                   let json = try JSON.init(data: response.data)
                   if json["status"].intValue == 200{
                     
                    
                    let jsonhSearchHome = json["data"]["datas"].arrayValue
                    var searchHome = [ListEASYModel]()
                    for jsonHomeFeed in jsonhSearchHome{
                        searchHome.append(ListEASYModel.init(json: jsonHomeFeed))
                    }
                    callbackSuccess(searchHome)
                    
                   }else{
                       callbackFailure(json["message"].stringValue)
                   }
               } catch {
                   callbackFailure("Có lỗi xảy ra")
               }
           }, error: { (error) in
               callbackFailure(error.localizedDescription)
           }) { (error) in
               callbackFailure(error.localizedDescription)
           }
       }
    
    static func addTimeLine (start_time: Int,end_time: Int,content: String ,code: Int, callbackSuccess: @escaping (_ typeCar: Timelines) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
     let parameters: Parameters = [
         "start_time": start_time,
         "end_time": end_time,
         "content": content,
         "code": code
     ]
        
        self.requestJson(target: .addTimeline(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                 callbackSuccess(Timelines.init(json: json["data"]))
                 
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func updateTimeline (id_timeline: Int, start_time: Int,end_time: Int,content: String ,code: Int, callbackSuccess: @escaping (_ typeCar: Timelines) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
           
        let parameters: Parameters = [
            "id_timeline": id_timeline,
            "start_time": start_time,
            "end_time": end_time,
            "content": content,
            "code": code
        ]
           
           self.requestJson(target: .updateTimeline(param: parameters), success: { (response) in
               do {
                   let json = try JSON.init(data: response.data)
                   if json["status"].intValue == 200{
                    callbackSuccess(Timelines.init(json: json["data"]))
                    
                   }else{
                       callbackFailure(json["message"].stringValue)
                   }
               } catch {
                   callbackFailure("Có lỗi xảy ra")
               }
           }, error: { (error) in
               callbackFailure(error.localizedDescription)
           }) { (error) in
               callbackFailure(error.localizedDescription)
           }
       }
    
    static func deleleTimeline (id_timeline: Int, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
     let parameters: Parameters = [
         "id_timeline": id_timeline
     ]
        
        self.requestJson(target: .deleteTimeline(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                 callbackSuccess(true)
                 
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func editScheduleEasy (easy_id: Int, group: [Group],name: String,from_week: Int ,to_week: Int,note: String, is_public: Int, callbackSuccess: @escaping (_ typeCar: [Group], _ easyID: Int) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
              
           let parameters: Parameters = [
               "easy_id": easy_id,
               "group": group,
               "name": name,
               "from_week": from_week,
               "to_week": to_week,
               "note": note,
               "is_public": is_public
           ]
              
              self.requestJson(target: .editScheduleEasy(param: parameters), success: { (response) in
                  do {
                      let json = try JSON.init(data: response.data)
                      if json["status"].intValue == 200{
                        
                       let jsonhSearchHome = json["data"]["group"].arrayValue
                       var searchHome = [Group]()
                       for jsonHomeFeed in jsonhSearchHome{
                           searchHome.append(Group.init(json: jsonHomeFeed))
                       }
                        callbackSuccess(searchHome,  json["data"]["easy_id"].intValue)
                        
                      }else{
                          callbackFailure(json["message"].stringValue)
                      }
                  } catch {
                      callbackFailure("Có lỗi xảy ra")
                  }
              }, error: { (error) in
                  callbackFailure(error.localizedDescription)
              }) { (error) in
                  callbackFailure(error.localizedDescription)
              }
          }
    
    static func easyAction (easy_id: Int, wake_up: Double, callbackSuccess: @escaping (_ typeCar: ESAYModel) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
               
            let parameters: Parameters = [
                "easy_id": easy_id,
                "wake_up": wake_up
            ]
               
               self.requestJson(target: .easyAction(param: parameters), success: { (response) in
                   do {
                       let json = try JSON.init(data: response.data)
                       if json["status"].intValue == 200{

                        callbackSuccess(ESAYModel.init(json: json["data"]))
                         
                       }else{
                           callbackFailure(json["message"].stringValue)
                       }
                   } catch {
                       callbackFailure("Có lỗi xảy ra")
                   }
               }, error: { (error) in
                   callbackFailure(error.localizedDescription)
               }) { (error) in
                   callbackFailure(error.localizedDescription)
               }
           }
    
}
