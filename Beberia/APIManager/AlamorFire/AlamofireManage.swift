//
//  AlamofireManage.swift
//  Beberia
//
//  Created by haiphan on 15/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa
import SwiftyJSON

protocol APIProtocol {
    func getImage(url: String, completion: @escaping ((GetImageResult) -> Void) )
    func dowloadImageURL(url: String,
                         folderName: String,
                         id: Double,
                         destinationURL: PublishSubject<URL>,
                         msgError: PublishSubject<String>,
                         processing: ((Double) -> Void)?)
    
}

class AlamofireManage: APIProtocol {
    static var shared = AlamofireManage()
    
    private func requestJSON(url: String,
                             method: Alamofire.HTTPMethod = .get,
                             parameters: [String: Any]? = nil) -> Observable<Data> {
        return Observable.create({ (observe) -> Disposable in
            let headers: HTTPHeaders = ["Content-Type": "application/json",
                                        "Authorization": "Bearer \(UserInfo.shareUserInfo.token)"]
            
            
            let request =  Alamofire.request(ConstantApp.shared.environment.text + url,
                                             method: method,
                                             parameters: parameters,
                                             encoding: (method == .get) ? URLEncoding.default : JSONEncoding.default,
                                             headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let swiftJsonVar = JSON(value)
                    print("------------ API RESPONSE ---------")
                    print("//Method: \(method)")
                    print("//Status Code: \(String(describing: response.response?.statusCode))")
                    print("//url")
                    print("//\(ConstantApp.shared.environment.text + url)")
                    print("//Parameters")
                    print("//\(String(describing: parameters))")
                    print("// Token: \(UserInfo.shareUserInfo.token)")
                    print("//Result")
                    print("\(swiftJsonVar)")
                    print("--------------***END***------------------")
                    do {
                        guard let dic = swiftJsonVar.dictionaryObject,
                              let status = swiftJsonVar["status"].int else {
                            return
                        }
                        if status == 200 {
                            let data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
                            observe.onNext(data)
                        } else {
                            let errorMsg = ErrorMsg(dic: dic)
                            observe.onError(errorMsg)
                        }
                        
                        observe.onCompleted()
                    } catch {
                        print("========= Error ========")
                        let p: [String: Any] = ["message": error.localizedDescription ]
                        let errorMsg = ErrorMsg(dic: p)
                        observe.onError(errorMsg)
                    }
                case .failure(let error):
                    let p: [String: Any] = ["message": error.localizedDescription ]
                    let errorMsg = ErrorMsg(dic: p)
                    observe.onError(errorMsg)
                    observe.onCompleted()
                    print("------------ API ERROR ---------")
                    print("//Method: \(method)")
                    print("//Status Code: \(String(describing: response.response?.statusCode))")
                    print("//url")
                    print("//\(ConstantApp.shared.environment.text + url)")
                    print("//Parameters")
                    print("//\(String(describing: parameters))")
                    print("// Token: \(UserInfo.shareUserInfo.token)")
                    print("//Result")
                    print("\(error.localizedDescription)")
                    print("--------------***END***------------------")
                }
            }
            return Disposables.create {
                request.cancel()
            }
        })
        
    }
    
    func reponse<T: Codable>(offType: T.Type, url: String,
                             method: Alamofire.HTTPMethod = .get,
                             parameters: [String: Any]? = nil) -> Observable<T> {
        return requestJSON(url: url,
                           method: method,
                           parameters: parameters)
        .decode(type: T.self, decoder: JSONDecoder())
    }
    
    func dowloadImageURL(url: String,
                         folderName: String,
                         id: Double,
                         destinationURL: PublishSubject<URL>,
                         msgError: PublishSubject<String>,
                         processing: ((Double) -> Void)?) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = ManageApp.shared.createURL(folder: folderName,
                                                          name: "1111111",
                                                          type: .jpeg)
            return (documentsURL, [.removePreviousFile])
        }
        Alamofire.download(
            url,
            method: .get,
            parameters: [:],
            encoding: JSONEncoding.default,
            headers: nil,
            to: destination).downloadProgress(closure: { (progress) in
                //progress closure
                processing?(progress.fractionCompleted)
            }).response(completionHandler: { (DefaultDownloadResponse) in
                //here you able to access the DefaultDownloadResponse
                //result closure
                if let destinationUrl = DefaultDownloadResponse.destinationURL {
                    destinationURL.onNext(destinationUrl)
                }
                
                if let msg = DefaultDownloadResponse.error {
                    msgError.onNext(msg.localizedDescription)
                }
                
            })
    }
    
    func getImage(url: String, completion: @escaping ((GetImageResult) -> Void) ) {
        Alamofire.request(url, method: .get).validate().responseData(completionHandler: { [weak self] (responseData) in
            guard let self = self else { return }
            switch responseData.result {
            case .success(let data):
                completion(.success(data))
                self.getImageTracking(url: url, data: data, error: nil)
            case .failure(let err):
                completion(.error(err))
                self.getImageTracking(url: url, data: nil, error: err.localizedDescription)
            }
        })
    }
    
    private func getImageTracking(url: String, data: Data?, error: String?) {
        print("=========== API Response ============")
        print("URL: \(url)")
        print("Http: \(HTTPMethod.get.rawValue)")
        print("Data: \(data ?? Data())")
        print("Error: \(error ?? "")")
        print("=====================================")
    }
}

enum GetImageResult {
    case success(Data)
    case error(Error)
}
struct ErrorMsg: Error {
    let dic: [String: Any]
}
