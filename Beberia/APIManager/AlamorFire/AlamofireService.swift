//
//  AlamofireService.swift
//  Beberia
//
//  Created by haiphan on 15/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import RxSwift

protocol AlamofireSeriveType {
    func getHomeShop() -> Observable<ShopHomeModel>
    func getRecommend(page: Int)  -> Observable<ShopRecommenDatadMode>
    func getDetailProduct(productID: Int) -> Observable<DetailProductDateModel>
    func searchShop(text: String, page: Int, searchType: SearchShopType) -> Observable<ShopRecommenDatadMode>
    func addToCart() -> Observable<BaseModelReponseSuccess>
    func dealShop(pageToken: String) -> Observable<ShopRecommenDatadMode>
    func deleteAcount() -> Observable<BaseModelReponseSuccess>
    func getProductTradeMark(shopID: Int, productTradeMarkType: ProductTradeMarkType, page: Int) -> Observable<ShopRecommenDatadMode>
}

final class AlamorfireService {
    static var shared = AlamorfireService()
    private var alamofireManager = AlamofireManage.shared
}

extension AlamorfireService: AlamofireSeriveType {
    
    func getProductTradeMark(shopID: Int, productTradeMarkType: ProductTradeMarkType, page: Int) -> Observable<ShopRecommenDatadMode> {
        let apiStr = String(format: APILink.productTradeMark.rawValue, shopID, productTradeMarkType.rawValue, page)
        return alamofireManager.reponse(offType: ShopRecommenDatadMode.self, url: apiStr)
    }
    
    func deleteAcount() -> Observable<BaseModelReponseSuccess> {
        return alamofireManager.reponse(offType: BaseModelReponseSuccess.self,
                                        url: APILink.deleteAccount.rawValue,
                                        method: .post)
    }
    
    func dealShop(pageToken: String) -> Observable<ShopRecommenDatadMode> {
        let api = String(format: APILink.dealShop.rawValue, pageToken)
        return alamofireManager.reponse(offType: ShopRecommenDatadMode.self, url: api)
    }
    
    func addToCart() -> Observable<BaseModelReponseSuccess> {
        let p: [String: Any] = [
            "product_id": 1,
            "product_option_ids": "1_4",
            "product_buycnt": 1,
            "product_price": 496000
        ]
        return alamofireManager.reponse(offType: BaseModelReponseSuccess.self,
                                        url: APILink.addToCart.rawValue,
                                        method: .post,
                                        parameters: p)
    }
    
    func searchShop(text: String, page: Int, searchType: SearchShopType) -> Observable<ShopRecommenDatadMode> {
        let p: [String: Any] =  [
            "keyword": text,
            "page": page,
            "sort": searchType.rawValue
        ]
        return alamofireManager.reponse(offType: ShopRecommenDatadMode.self,
                                        url: APILink.searchShop.rawValue,
                                        method: .post,
                                        parameters: p)
    }
    func getDetailProduct(productID: Int) -> Observable<DetailProductDateModel> {
        let urlStr = String(format: APILink.detailProduct.rawValue, productID)
        return alamofireManager.reponse(offType: DetailProductDateModel.self, url: urlStr)
    }
    
    func getRecommend(page: Int)  -> Observable<ShopRecommenDatadMode> {
        let urlStr = String(format: APILink.shopRecommend.rawValue, page)
        return alamofireManager.reponse(offType: ShopRecommenDatadMode.self, url: urlStr)
    }
    
    func getHomeShop() -> Observable<ShopHomeModel> {
        return alamofireManager.reponse(offType: ShopHomeModel.self, url: APILink.homeShop.rawValue)
    }
}
