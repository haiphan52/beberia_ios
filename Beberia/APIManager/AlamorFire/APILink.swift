//
//  APILink.swift
//  Beberia
//
//  Created by haiphan on 15/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import UIKit

enum APILink: String {
    
    case banner = "api/v1/assistant/ultrasound-last/"
    case homeShop = "api/v1/shop/home"
    case shopRecommend = "api/v1/shop/recomend?page=%d"
    case detailProduct = "api/v1/shop/detail?product_id=%d"
    case searchShop = "api/v1/shop/search"
    case addToCart = "api/v1/shop/addToCart"
    case dealShop = "api/v1/shop/deal?token_page=%@"
    case deleteAccount = "api/v1/user/delete"
    case productTradeMark = "/api/v1/shop/products?shop_id=%d&sort=%d&page=%d"
    
}
