//
//  APISendBirdManager.swift
//  Beberia
//
//  Created by IMAC on 7/22/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import SwiftyJSON

struct APISendBirdManager {
    
    static func getListChannel(nameSearch: String, completion: @escaping ([ChannelSBModel])->()){
        let urlString = "https://api-\(LibKey.APPIDSendbird).sendbird.com/v3/group_channels"
        let headers = [
            "Content-Type": "application/json, charset=utf8",
            "Api-Token": LibKey.APITokenSB
        ]
        
        let parameters: Parameters = [
            "name_contains" : nameSearch,
            "limit" : 30
        ]
        
        Alamofire.request(urlString, method: .get, parameters: parameters,encoding: URLEncoding.queryString, headers: headers).responseJSON {
            response in
            switch response.result {
            case .success:
             //   print(response)
                do {
                    let json = try JSON.init(data: response.data!)
                  //  if json["status"].intValue == 200{
                        let jsonCityNames = json["channels"].arrayValue
                        var cityNames = [ChannelSBModel]()
                        for jsonCityName in jsonCityNames{
                            cityNames.append(ChannelSBModel.init(json: jsonCityName))
                        }
                        completion(cityNames)
                 //   }else{
                 //       completion([])
                //    }
                } catch {
                    completion([])
                }
                
                break
            case .failure(let error):
                completion([])
                print(error)
            }
        }
    }
    
    static func getListUser(token: String,channel_url: String,  completion: @escaping ([UserSendbirdModel],_ token: String)->()){
        let urlString = "https://api-\(LibKey.APPIDSendbird).sendbird.com/v3/group_channels/\(channel_url)/members?limit=30&token=\(token)&order=operator_then_member_alphabetical"
        let headers = [
            "Content-Type": "application/json, charset=utf8",
            "Api-Token": LibKey.APITokenSB
        ]
        
        Alamofire.request(urlString, method: .get, parameters: [:],encoding: URLEncoding.queryString, headers: headers).responseJSON {
            response in
            switch response.result {
            case .success:
             //   print(response)
                do {
                    let json = try JSON.init(data: response.data!)
                  //  if json["status"].intValue == 200{
                        let jsonCityNames = json["members"].arrayValue
                        var cityNames = [UserSendbirdModel]()
                        for jsonCityName in jsonCityNames{
                            cityNames.append(UserSendbirdModel.init(json: jsonCityName))
                        }
                    completion(cityNames, json["next"].stringValue)
                 //   }else{
                 //       completion([])
                //    }
                } catch {
                    completion([], "")
                }
                
                break
            case .failure(let error):
                completion([], "")
                print(error)
            }
        }
    }
    
    static func getListChannelCustomType(customType: String, completion: @escaping ([ChannelSBModel])->()){
           let urlString = "https://api-\(LibKey.APPIDSendbird).sendbird.com/v3/group_channels?custom_types=\(customType)&limit=100"
           let headers = [
               "Content-Type": "application/json, charset=utf8",
               "Api-Token": LibKey.APITokenSB
           ]
           
           Alamofire.request(urlString, method: .get, parameters: [:],encoding: URLEncoding.queryString, headers: headers).responseJSON {
               response in
               switch response.result {
               case .success:
                //   print(response)
                   do {
                       let json = try JSON.init(data: response.data!)
                     //  if json["status"].intValue == 200{
                           let jsonCityNames = json["channels"].arrayValue
                           var cityNames = [ChannelSBModel]()
                           for jsonCityName in jsonCityNames{
                               cityNames.append(ChannelSBModel.init(json: jsonCityName))
                           }
                           completion(cityNames)
                    //   }else{
                    //       completion([])
                   //    }
                   } catch {
                       completion([])
                   }
                   
                   break
               case .failure(let error):
                   completion([])
                   print(error)
               }
           }
       }
    
    static func updateChannel(coverURL: [String: Any] ,channel_url: String, completion: @escaping (ChannelSBModel)->()){

//        let jsonData =  try JSONSerialization.data(withJSONObject: coverURL, options: .prettyPrinted)// first of all convert json to the data
//        let convertedString = String(data: jsonData, encoding: .utf8) // the data will be converted to the string

        let urlString = "https://api-\(LibKey.APPIDSendbird).sendbird.com/v3/group_channels/\(channel_url)"
        let headers = [
            "Content-Type": "multipart/form-data, charset=utf8",
            "Api-Token": LibKey.APITokenSB
        ]


        Alamofire.request(urlString, method: .put, parameters: ["cover_file": coverURL ],encoding: JSONEncoding.default, headers: headers).responseJSON {
                       response in
                       switch response.result {
                       case .success:
                        //   print(response)
                           do {
                               let json = try JSON.init(data: response.data!)

                               completion(ChannelSBModel.init(json: json))

                           } catch {
                              // completion([])
                               print("dâdadadadad")
                           }

                           break
                       case .failure(let error):
                          // completion([])
                           print(error)
                       }
                   }

    }
    
    static func updateLoadFile(fileData: Data, channel_url: String, completion: @escaping (ChannelSBModel)->()){
        let headers = [
                   "Content-Type": "multipart/form-data; boundary=\(UUID().uuidString)",
                   "Api-Token": LibKey.APITokenSB
               ]
        Alamofire.upload(multipartFormData: { multipart in
            multipart.append(fileData, withName: "cover_file", fileName: "imageCover.jpg", mimeType: "image/jpeg")
           

        }, to: "https://api-\(LibKey.APPIDSendbird).sendbird.com/v3/group_channels/\(channel_url)", method: .put, headers: headers) { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (answer) in
                    do {
                        let json = try JSON.init(data: (answer.data)!)
                        completion(ChannelSBModel.init(json: json))
                    } catch {
                        print("dâdadadadad")
                    }
                })
                upload.uploadProgress { progress in
                    //call progress callback here if you need it
                }
            case .failure(let encodingError):
                print("multipart upload encodingError: \(encodingError)")
            }
        }
    }
    
    static func uploadImage(paramName: String, fileName: String, image: UIImage,channel_url: String) {
        let url = URL(string: "https://api-\(LibKey.APPIDSendbird).sendbird.com/v3/group_channels/\(channel_url)")

        // generate boundary string using a unique per-app string
        let boundary = UUID().uuidString

        let session = URLSession.shared

        // Set the URLRequest to POST and to the specified URL
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = "PUT"

        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("\(LibKey.APITokenSB)", forHTTPHeaderField: "Api-Token")

        var data = Data()

        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(paramName)\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(image.pngData()!)

        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)

        // Send a POST request to the URL, with the data we created earlier
        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: responseData!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                    
                    self.updateChannel(coverURL: json, channel_url: channel_url) { (channel) in
                        print(channel)
                    }
                }
            }
        }).resume()
    }
}

