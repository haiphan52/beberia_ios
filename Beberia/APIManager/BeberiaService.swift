//
//  CavanseeService.swift
//
//
//  Created by Mac on 1/16/19.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Alamofire
import Moya

enum BeberiaService{
    // user
    case login(param: Parameters)
    case loginSocial(param:Parameters)
    case registerSocial(param:Parameters)
    case updateInfo(param:Parameters)
    case checkName(param:Parameters)
    case checkPhone(param:Parameters)
    case checkEmail(param:Parameters)
    case register(param:Parameters)
    case sendOtp(param:Parameters)
    case verifyOtp(param:Parameters)
    case forgotPassword(param:Parameters)
    case changePassForgot(param:Parameters)
    case verifyOtpPassForgot(param:Parameters)
    // profile
    case getFeedbyUser(param:Parameters)
    case updateInfoProfile(param:Parameters)
    case editBabyProfile(param:Parameters)
    case addBabyProfile(param:Parameters)
    case changePassWord(param:Parameters)
    case changeAvatar(param:Parameters)
    case listFavorite(param:Parameters)
    case logout(param:Parameters)
    case deleteBaby(param:Parameters)
    // home
    case getHomeFeed(param:Parameters)
    case createFeed(param:Parameters)
    case getSearchHome(param:Parameters)
    case likeHomeFeed(param:Parameters)
    case saveHomeFeed(param:Parameters)
    case checkCompletedFeed(param:Parameters)
    case getCommentFeed(param:Parameters)
    case editCommentFeed(param:Parameters)
    case detailFeed(param:Parameters)
    case sendCommentHomeFeed(param:Parameters)
    case deleteCommentHomeFeed(param:Parameters)
    case getNoti(param:Parameters)
    case deleteAndCancelFeed(param:Parameters)
    case editFeed(param:Parameters)
    case readNotification(param:Parameters)
    case turnOnOffNotification(param:Parameters)
    case checkTime(param:Parameters)
    // imfomation
    case getCategoryShow(param: Parameters)
    case getCategory(param: Parameters)
    case getPostDetail(param: Parameters)
    case likePost(param: Parameters)
    case favouritePost(param: Parameters)
    case getComment(param: Parameters)
    case getCommentReply(param: Parameters)
    case sendComment(param: Parameters)
    case deleteComment(param: Parameters)
    case editComment(param: Parameters)
    case getDetailMore(param:Parameters)
    
    // getLocation
    case getLocation(param:Parameters)
    case getDistrict(param:Parameters)
    // Diary
    case getDate(param:Parameters)
    case getBanner(param:Parameters)
    case gethomeDiary(param:Parameters)
    case getlistDiary(param:Parameters)
    case createDiary(param:Parameters)
    case checkCreateDiary(param:Parameters)
    case detailDiary(param:Parameters)
    case getSearchDiary(param:Parameters)
    case deleteDiary(param:Parameters)
    case editDiary(param:Parameters)
    case settingNoti(param:Parameters)
    case listSuggest(param:Parameters)
    
    
    //MARK: Secret
    case getSecretHome(param:Parameters)
    case createPassSecret(param:Parameters)
    case getNotiSecret(param:Parameters)
    
    //MARK: Vaccine
    case getVaccineSchedule(param:Parameters)
    case getAgesVaccine(param:Parameters)
    case getDataVaccine(param:Parameters)
    case getPregnancySchedule(param:Parameters)
    case markVaccine(param:Parameters)
    case updateTimeVaccine(param:Parameters)
    
    //MARK: EASY
    case getEasyActive(param:Parameters)
    case getEasyDetail(param:Parameters)
    case getListEasy(param:Parameters)
    
    case addTimeline(param:Parameters)
    case updateTimeline(param:Parameters)
    case deleteTimeline(param:Parameters)
    case editScheduleEasy(param:Parameters)
    case easyAction(param:Parameters)
    
    //MARK: Menstrual
    case createCycleMenstrual(param:Parameters)
    case getCycleMenstrual(param:Parameters)
    
    //MARK: Add Friend
    case handleActionRequest(param:Parameters)
    case confirmCancelRequest(param:Parameters)
    case searchNameFriend(param:Parameters)
    
    //MARK: BMI
    case createBMI(param:Parameters)
    case getListBMI(param:Parameters)
    case getDataStandard(param:Parameters)
    
    //MARK: Active Card
    case verifedNumberCard(param:Parameters)
    
    //MARK: Policy Sale
    case getPolicySale(param:Parameters)
    
    //MARK: Get Profile
    case getProfile(param:Parameters)
    
    //MARK: BabyFace
    case getListPrice(param:Parameters)
    case getListFrame(param:Parameters)
    case createOrderBaby(param:Parameters)
    
    
    //MARK: Game
    case listGift(param:Parameters)
    case myListReceived(param:Parameters)
    case inviteCode(param:Parameters)
    case ringGift(param:Parameters)
    case listReceived(param:Parameters)
    case listInvite(param:Parameters)
    
    // Ranking List
    case rankingList(param:Parameters)
    case rankingListByTime(param:Parameters)
    case listHistory(param:Parameters)
    case rankingListPoint(param:Parameters)
    case review(param:Parameters)
    case rankingDetail(param:Parameters)
    
    //orders
    case listOrder(param:Parameters)
    case refundOrder(param:Parameters)
    
    //policy
    case policy(param:Parameters)
    
    //Chat Group
    case createGroup(param:Parameters)
    case listGroup(param:Parameters)
    case listMessage(param:Parameters)
    case sendMessage(param:Parameters)
    case leaveGroup(param:Parameters)
    case chatMembers(param:Parameters)
    case readMessage(param:Parameters)
    case addMember(param:Parameters)
    case likeMessage(param:Parameters)
    case deleteMessage(param:Parameters)
    case searchGroup(param:Parameters)
    
    //SplashScreen
    case splashScreen(param:Parameters)
    
    //setting NotifiGroup
    case notification(param:Parameters)
    
    //AssitancePregnant
    case lastBMI(param:Parameters)
    case pregDate(param:Parameters)
    case addImageUltraSound(param:Parameters)
    case updateImageUltraSound(param:Parameters)
    case listImagesSound(param:Parameters)
    case listPregnantDetail(param:Parameters)
    case lastUltraSound(param: Parameters)
    case listPregnant(param: Parameters)
}

extension BeberiaService: TargetType{
    
    var baseURL: URL {
        switch self {
     
       // case .getHomeFeed:
       //     return URL(string: "http://dev.beberia.me/")!
        
        default:
            //Live https://api.beberia.me/
            // Dev http://apidev.beberia.me/
            return URL(string: ConstantApp.shared.environment.text)!
        }
    
    }
    
    //MARK: HAVE TO UPDATE API IF NOT, APP DON'T GET API
    //MARK: HAVE TO UPDATE MOYA & METHOD
    var path: String {
        switch self {
        case .listPregnant:
            return "/api/v1/assistant/pregnancy-list"
        case .lastUltraSound:
            return "/api/v1/assistant/ultrasound-last"
        case .listPregnantDetail:
            return "/api/v1/assistant/pregnancy"
        case .updateImageUltraSound:
            return "/api/v1/assistant/ultrasound-update"
        case .listImagesSound:
            return "/api/v1/assistant/ultrasound-list"
        case .addImageUltraSound:
            return "/api/v1/assistant/ultrasound"
        case .pregDate:
            return "/api/v1/assistant/preg-date"
        case .lastBMI:
            return "api/v1/bmi/getLast"
        case .notification:
            return "/api/v2/chat/notification"
        case .splashScreen:
            return "/api/v1/splash-list"
        case .rankingDetail:
            return "/api/v1/ranking/detail"
        case .searchGroup:
            return "/api/v2/chat/search-group"
        case .deleteMessage:
            return "/api/v2/chat/delete-message"
        case .likeMessage:
            return "/api/v2/chat/like-message"
        case .addMember:
            return "/api/v2/chat/add-member"
        case .readMessage:
            return "/api/v2/chat/read"
        case .chatMembers:
            return "/api/v2/chat/members"
        case .leaveGroup:
            return "/api/v2/chat/remove-member"
        case .sendMessage:
            return "/api/v2/chat/create-message"
        case .listMessage:
            return "/api/v2/chat/list-message"
        case .listGroup:
            return "/api/v2/chat/list"
        case .createGroup:
            return "/api/v2/chat/create-group"
        case .checkTime:
            return "/api/v1/feed/check-time"
        case .listSuggest:
            return "/api/v2/chat/suggest"
        case .policy:
            return "/api/v1/ranking/policy"
        case .refundOrder:
            return "/api/v1/babyface/refund-order"
        case .listOrder:
            return "/api/v1/babyface/list-order"
        case .review:
            return "/api/v1/babyface/review"
        // Rankking
        case .listHistory:
            return "/api/v1/ranking/list-history"
        case .rankingListByTime:
            return "/api/v1/ranking/list-by-time"
        case .rankingList:
            return "/api/v1/ranking/list"
        
        //MARK: Game
        case .listGift:
            return "/api/v1/game/listGift"
        case .myListReceived:
            return "/api/v1/game/myListReceived"
        case .inviteCode:
            return "/api/v1/game/inviteCode"
        case .ringGift:
            return "/api/v1/game/ringGift"
        case .listReceived:
            return "/api/v1/game/listReceived"
        case .listInvite:
            return "/api/v1/game/listInvite"
        
        // user
        case .login:
            return "/api/v1/auth/login"
        case .logout:
            return "/api/v1/logout"
        case .loginSocial:
            return "/api/v1/auth/loginSocial"
        case.registerSocial:
            return "/api/v1/auth/registerSocial"
        case.register:
            return "/api/v1/auth/register"
        case.checkName:
            return "/api/v1/auth/checkName"
        case.checkPhone:
            return "/api/v1/auth/checkPhone"
        case.checkEmail:
            return "/api/v1/auth/checkEmail"

          
            case.sendOtp:
                return "/api/v1/user/sendOtp"
            case.verifyOtp:
                return "/api/v1/user/verifyOtp"
            case.forgotPassword:
                return "/api/v1/auth/forgotPassword"
            case.changePassForgot:
                return "/api/v1/auth/changePassForgot"
            case.verifyOtpPassForgot:
            return "/api/v1/auth/verifyReset"
        // imfomation
        case .getCategoryShow:
            return "/api/v1/categories/show"
        case .getCategory:
            return "/api/v1/categories/list"
        case .getPostDetail:
            return "/api/v1/posts/detail"
        case .likePost:
            return "/api/v1/likes"
        case .favouritePost:
            return "/api/v1/posts/favourite"
        case .getComment:
            return "/api/v1/comments/list"
        case .getCommentReply:
            return "/api/v1/comments/reply"
        case .sendComment:
            return "/api/v1/comments"
        case .deleteComment:
            return "/api/v1/comments/delete"
        case .editComment:
            return "/api/v1/comments/update"
        case .getDetailMore:
            return "/api/v1/posts/list"
        case .updateInfo:
            return "/api/v1/auth/updateInfo"
            
        // Home
        case .getHomeFeed:
            return "/api/v1/feed/home"
        case .createFeed:
            return "/api/v1/feed/create"
        case .getSearchHome:
            return "/api/v1/search/feed"
        case .likeHomeFeed:
            return "/api/v1/feed/like"
        case .saveHomeFeed:
            return "/api/v1/feed/save"
        case .checkCompletedFeed:
            return "/api/v1/feed/checkCompleted"
        case .getCommentFeed:
            return "/api/v1/comment/get"
        case .editCommentFeed:
            return "/api/v1/comment/edit"
        case .detailFeed:
            return "/api/v1/feed/detailFeed"
        case .sendCommentHomeFeed:
            return "api/v1/feed/comment"
        case .deleteCommentHomeFeed:
            return "api/v1/comment/delete"
        case .getNoti:
            return "/api/v1/notification/get"
        case .deleteAndCancelFeed:
            return "/api/v1/feed/cancel"
        case .editFeed:
            return "/api/v1/feed/edit"
        case .readNotification:
            return "/api/v1/notification/read"
        case .turnOnOffNotification:
            return "/api/v1/feed/settingNoti"
            
            
        // getLocation
        case .getLocation:
            return "/api/v1/location/get"
        case .getDistrict:
            return "/api/v1/location/district"
            
        // Profile
        case .getFeedbyUser:
            return "/api/v1/user/listFeed"
        case .updateInfoProfile:
            return "/api/v1/user/editProfile"
        case .editBabyProfile:
            return "/api/v1/user/editBaby"
        case .addBabyProfile:
            return "/api/v1/user/addBaby"
        case .changePassWord:
            return "/api/v1/user/changePass"
        case .changeAvatar:
            return "/api/v1/user/changeAvatar"
        case .listFavorite:
            return "/api/v1/posts/favourite/list"
        case .deleteBaby:
            return "/api/v1/user/delBaby"
            
        // Diary
        case .getDate :
            return "/api/v1/diaries/date"
        case .getBanner :
            return "/api/v1/banners"
        case.gethomeDiary :
            return "/api/v1/diaries/home"
        case.getlistDiary :
            return "/api/v1/diaries/list"
        case .createDiary :
            return "/api/v1/diaries/create"
        case .checkCreateDiary :
            return "/api/v1/diaries/check"
        case .detailDiary :
            return "/api/v1/diaries/detail"
        case .getSearchDiary:
            return "/api/v1/search/diary"
        case .deleteDiary:
            return "/api/v1/diaries/delete"
        case .editDiary:
            return "/api/v1/diaries/update"
        case .settingNoti:
            return "/api/v1/posts/settingNotify"
            
        //MARK: Secret
        case .getSecretHome:
            return "/api/v1/search/feed"
        case .createPassSecret:
            return "/api/v1/user/createPassSecret"
        case .getNotiSecret:
            return "api/v1/notification/getNotiSecret"
            
        //MARK: Vaccine
        case .getVaccineSchedule:
            return "api/v1/schedules/get"
        case .getAgesVaccine:
            return "api/v1/ages/get/0"
        case .getDataVaccine:
            return "api/v1/vaccines/get"
        case .getPregnancySchedule:
            return "api/v1/schedules/preg"
        case .markVaccine:
            return "/api/v1/schedules/action"
        case .updateTimeVaccine:
            return "api/v1/schedules/updateTime"
            
        //MARK: EASY
        case .getEasyActive:
            return "/api/v1/easy/getEasyActive"
        case .getEasyDetail:
            return "/api/v1/easy/detail"
        case .getListEasy:
            return "/api/v1/easy/getList"
        case .addTimeline:
            return "/api/v1/easy/addTimeLine"
        case .updateTimeline:
            return "/api/v1/easy/updateTimeline"
        case .deleteTimeline:
            return "/api/v1/easy/delTimeline"
        case .editScheduleEasy:
            return "/api/v1/easy/edit"
        case .easyAction:
            return "/api/v1/easy/action"
            
        //MARK: Menstrual
        case .createCycleMenstrual:
            return "/api/v1/cycle/store"
        case .getCycleMenstrual:
            return "/api/v1/cycle/getMonth"
            
            
        //MARK: Add Friend
        case .handleActionRequest(param:_):
            return "/api/v1/user/follow"
        case .confirmCancelRequest(param:_):
            return "/api/v1/user/action"
        case .searchNameFriend(param:_):
            return "/api/v1/user/searchFollow"
            
        //MARK: BMI
        case .createBMI(param:_):
            return "/api/v1/bmi/store"
        case .getListBMI(param:_):
            return "/api/v1/bmi/list"
        case .getDataStandard(param:_):
            return "/api/v1/standard/getList"
            
            //MARK: Active Card
            case .verifedNumberCard(param: _):
                return "/api/v1/card/verified"
            
        //MARK: getPolicySale
        case .getPolicySale(param: _):
            return "/api/v1/sale/get"
            
        //MARK: getProfile
        case .getProfile(param: _):
            return "/api/v1/user/getInfo"
            
        //MARK: BabyFace
        case .getListPrice(param:_):
            return "api/v1/babyface/getItems"
        case .getListFrame(param:_):
            return "api/v1/babyface/getFrame"
        case .createOrderBaby(param:_):
            return "api/v1/babyface/store"
        case .rankingListPoint:
            return "/api/v1/ranking/point"
        }
    }
    
    //MARK: HAVE TO UPDATE API IF NOT, APP DON'T GET API
    var method: Moya.Method {
        switch self {
        case .getAgesVaccine(param: _),
                .getDataVaccine(param: _),
                .getEasyDetail(param: _),
                .getEasyActive,
                .getListFrame(param: _),
                .listInvite(param: _),
                .rankingList(param: _),
                .rankingListPoint(param: _),
                .review(param: _),
                .listOrder(param: _),
                .rankingListByTime(param: _),
                .listHistory(param: _),
                .policy(param: _),
                .listSuggest(param: _),
                .checkTime(param: _),
                .listGroup(param: _),
                .listMessage(param: _),
                .chatMembers(param: _),
                .rankingDetail(param: _),
                .splashScreen(param: _),
                .listPregnantDetail(param: _),
                .listImagesSound(param: _),
                .lastUltraSound(param: _),
                .listPregnant(param: _):
            return .get
        default :
            return .post
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case .login(param: let parameters):
            return parameters
        case .logout(param: let parameters):
            return parameters
        case .loginSocial(param: let parameters):
            return parameters
        case .register(param: let parameters):
            return parameters
        case .registerSocial(param: let parameters):
            return parameters
        case .checkName(param: let parameters):
            return parameters
        case .checkPhone(param: let parameters):
            return parameters
        case .checkEmail(param: let parameters):
            return parameters
        case .checkPhone(param: let parameters):
            return parameters
        case .getCategory(param: let parameters):
            return parameters
        case .getPostDetail(param: let parameters):
            return parameters
        case .likePost(param: let parameters):
            return parameters
        case .favouritePost(param: let parameters):
            return parameters
        case .getComment(param: let parameters):
            return parameters
        case .getCommentReply(param: let parameters):
            return parameters
        case .sendComment(param: let parameters):
            return parameters
        case .deleteComment(param: let parameters):
            return parameters
        case .editComment(param: let parameters):
            return parameters
        case .getDetailMore(param: let parameters):
            return parameters
        case .getCategoryShow(param: let parameters):
            return parameters
        case .updateInfo(param: let parameters):
            return parameters
        case .getHomeFeed(param: let parameters):
            return parameters
        case .getLocation(param: let parameters):
            return parameters
        case .createFeed(param: let parameters):
            return parameters
        case .getDate(param: let parameters):
            return parameters
        case .getBanner(param: let parameters):
            return parameters
        case .getSearchHome(param: let parameters):
            return parameters
        case .likeHomeFeed(param: let parameters):
            return parameters
        case .saveHomeFeed(param: let parameters):
            return parameters
        case .gethomeDiary(param: let parameters):
            return parameters
        case .getlistDiary(param: let parameters):
            return parameters
        case .checkCompletedFeed(param: let parameters):
            return parameters
        case .getCommentFeed(param: let parameters):
            return parameters
        case .editCommentFeed(param: let parameters):
            return parameters
        case .detailFeed(param: let parameters):
            return parameters
        case .sendCommentHomeFeed(param: let parameters):
            return parameters
        case .deleteCommentHomeFeed(param: let parameters):
            return parameters
        case .createDiary(param: let parameters):
            return parameters
        case .checkCreateDiary(param: let parameters):
            return parameters
        case .getFeedbyUser(param: let parameters):
            return parameters
        case .updateInfoProfile(param: let parameters):
            return parameters
        case .editBabyProfile(param: let parameters):
            return parameters
        case .addBabyProfile(param: let parameters):
            return parameters
        case .changePassWord(param: let parameters):
            return parameters
        case .changeAvatar(param: let parameters):
            return parameters
        case .getNoti(param: let parameters):
            return parameters
        case .detailDiary(param: let parameters):
            return parameters
        case .deleteAndCancelFeed(param: let parameters):
            return parameters
        case .editFeed(param: let parameters):
            return parameters
        case .listFavorite(param: let parameters):
            return parameters
        case .getSearchDiary(param: let parameters):
            return parameters
        case .deleteDiary(param: let parameters):
            return parameters
        case .editDiary(param: let parameters):
            return parameters
        case .readNotification(param: let parameters):
            return parameters
        case .turnOnOffNotification(param: let parameters):
            return parameters
        case .settingNoti(param: let parameters):
            return parameters
        case .getDistrict(param: let parameters):
            return parameters
        case .getSecretHome(param: let parameters):
            return parameters
        case .createPassSecret(param: let parameters):
            return parameters
        case .getNotiSecret(param: let parameters):
            return parameters
        case .deleteBaby(param: let parameters):
            return parameters
        case .sendOtp(param: let parameters):
            return parameters
        case .verifyOtp(param: let parameters):
            return parameters
        case .forgotPassword(param: let parameters):
            return parameters
        case .changePassForgot(param: let parameters):
            return parameters
        case.verifyOtpPassForgot(param: let parameters):
            return parameters
        case .getVaccineSchedule(param: let parameters):
            return parameters
        case .getAgesVaccine(param: let parameters):
            return parameters
        case .getDataVaccine(param: let parameters):
            return parameters
        case .getPregnancySchedule(param: let parameters):
            return parameters
        case .markVaccine(param: let parameters):
            return parameters
        case .updateTimeVaccine(param: let parameters):
            return parameters
            
        case .getEasyActive(param: let parameters):
            return parameters
        case .getEasyDetail(param: let parameters):
            return parameters
        case .getListEasy(param: let parameters):
            return parameters
            
        case .addTimeline(param: let parameters):
            return parameters
        case .updateTimeline(param: let parameters):
            return parameters
        case .deleteTimeline(param: let parameters):
            return parameters
        case .editScheduleEasy(param: let parameters):
            return parameters
        case .easyAction(param: let parameters):
            return parameters
            
        case .createCycleMenstrual(param: let parameters):
            return parameters
        case .getCycleMenstrual(param: let parameters):
            return parameters
            
        case .handleActionRequest(param: let parameters):
            return parameters
        case .confirmCancelRequest(param: let parameters):
            return parameters
        case .searchNameFriend(param: let parameters):
            return parameters
            
        //MARK: BMI
        case .createBMI(param: let parameters):
            return parameters
        case .getListBMI(param: let parameters):
            return parameters
        case .verifedNumberCard(param: let parameters):
            return parameters
        case .getDataStandard(param: let parameters):
            return parameters
            
            
        case .getPolicySale(param: let parameters):
            return parameters
        case .getProfile(param: let parameters):
            return parameters
            
            
        //MARK: BabyFace
        case .getListPrice(param: let parameters):
            return parameters
        case .getListFrame(param: let parameters):
            return parameters
        case .createOrderBaby(param: let parameters):
            return parameters
            
            
        //MARK: Game
        case .listGift(param: let parameters):
            return parameters
        case .myListReceived(param: let parameters):
            return parameters
        case .inviteCode(param: let parameters):
            return parameters
        case .ringGift(param: let parameters):
            return parameters
        case .listReceived(param: let parameters):
            return parameters
        case .listInvite(param: let parameters):
            return parameters
            
        case .rankingList(param: let parameters):
            return parameters
               
        case .rankingListPoint(param: let parameters):
            return parameters
        case .review(param: let parameters):
            return parameters
        case .listOrder(param: let parameters):
            return parameters
        case .refundOrder(param: let parameters):
            return parameters
        case .rankingListByTime(param: let parameters):
            return parameters
        case .listHistory(param: let parameters):
            return parameters
        case .policy(param: let parameters):
            return parameters
        case .listSuggest(param: let parameters):
            return parameters
        case .checkTime(param: let parameters):
            return parameters
        case .createGroup(param: let parameters):
            return parameters
        case .listGroup(param: let parameters):
            return parameters
        case .listMessage(param: let parameters):
            return parameters
        case .sendMessage(param: let parameters):
            return parameters
        case .leaveGroup(param: let parameters):
            return parameters
        case .chatMembers(param: let parameters):
            return parameters
        case .readMessage(param: let parameters):
            return parameters
        case .addMember(param: let parameters):
            return parameters
        case .likeMessage(param: let parameters):
            return parameters
        case .deleteMessage(param: let parameters):
            return parameters
        case .searchGroup(param: let parameters):
            return parameters
        case .rankingDetail(param: let parameters):
            return parameters
        case .splashScreen(param: let parameters):
            return parameters
        case .notification(param: let parameters):
            return parameters
        case .lastBMI(param: let parameters):
            return parameters
        case .pregDate(param: let parameters):
            return parameters
        case .addImageUltraSound(param: let parameters):
            return parameters
        case .listImagesSound(param: let parameters):
            return parameters
        case .updateImageUltraSound(param: let parameters):
            return parameters
        case .listPregnantDetail(param: let parameters):
            return parameters
        case .lastUltraSound(param: let parameters):
            return parameters
        case .listPregnant(param: let parameters):
            return parameters
        }
        
        
    }
    
    var headers: [String: String]? {
        switch self {
        case  .login(param: _):
            return ["Content-Type": "application/json"]
            
        default :
            print(UserInfo.shareUserInfo.token)
            return ["Content-Type": "application/json", "Authorization": "Bearer \(UserInfo.shareUserInfo.token)"]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .login(param: _):
            return JSONEncoding.default
        default :
            return JSONEncoding.default
        }
        
    }
    
    var sampleData: Data {
        return Data()
    }
    
    //MARK: HAVE TO UPDATE API IF NOT, APP DON'T GET API
    var task: Task {
        switch self {
        case .getAgesVaccine(param: _),
                .getDataVaccine(param: _),
                .getEasyActive,
                .getEasyDetail,
                .getListFrame,
                .listInvite,
                .rankingList(param: _),
                .rankingListPoint(param: _),
                .review(param: _),
                .listOrder(param: _),
                .rankingListByTime(param: _),
                .listHistory(param: _),
                .policy(param: _),
                .listSuggest(param: _),
                .checkTime(param: _),
                .listGroup(param: _),
                .listMessage(param: _),
                .chatMembers(param: _),
                .rankingDetail(param: _),
                .splashScreen(param: _),
                .listPregnantDetail(param: _),
                .listImagesSound(param: _),
                .lastUltraSound(param: _),
                .listPregnant(param: _):
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        default :
            return .requestParameters(parameters: parameters, encoding: parameterEncoding)
        }
    }
}
