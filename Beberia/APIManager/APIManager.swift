//
//  APIManager.swift
//
//
//  Created by Mac on 1/16/19.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Alamofire
import Moya
import SwiftyJSON
import SwiftKeychainWrapper
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import SwiftKeychainWrapper
import SVProgressHUD

struct APIManager {
    
    class DefaultAlamofireManager: Alamofire.SessionManager {
        static let sharedManager: DefaultAlamofireManager = {
            let configuration = URLSessionConfiguration.default
            configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
            configuration.timeoutIntervalForRequest = 60 // as seconds, you can set your request timeout
            configuration.timeoutIntervalForResource = 60 // as seconds, you can set your resource timeout
            configuration.requestCachePolicy = .useProtocolCachePolicy
            return DefaultAlamofireManager(configuration: configuration)
        }()
    }
    
    static let provider = MoyaProvider<BeberiaService>(manager: DefaultAlamofireManager.sharedManager)

    static func requestJson(target: BeberiaService, success successCallback: @escaping (Response) -> Void, error errorCallback: @escaping (Swift.Error) -> Void, failure failureCallback: @escaping (MoyaError) -> Void) {

        print(target.baseURL)
        print(target.method)
        print(target.parameters)
        print(target.path)
        print(target.headers)
       
        provider.request(target) { (result) in
            switch result {
            case .success(let response):
                APIManager.trackReponse(response: response, target: target)
                if response.statusCode >= 200 && response.statusCode <= 300 {
                    
                    // xử lý khi status = 406 (token hết hạn)
                    let json = try? JSON.init(data: response.data)
                    if json?["status"].intValue == 406{
                        print(json?["status"].intValue as Any)
                        APIManager.logout406()
                    }else if json?["status"].intValue == 401{
                        print(json)
                        Utils.showAlertOKWithAction(controller: Utils.getTopMostViewController()!, title: "Thông báo", message: "Bạn không có quyền truy cập!") {
                            print("OK")
                        }
                    }
                    else{
                        // thành công
                        successCallback(response)
                    }
                }
                else {
                    let error = NSError(domain:"com.vsemenchenko.networkLayer", code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
                    errorCallback(error)
                }
            case .failure(let error):
                failureCallback(error)
            }
        }
    }
    
    static func trackReponse(response: Response, target: BeberiaService) {
        let json = JSON(response.data)
        print("------------ API RESPONSE ---------")
        print("//Method: \(target.method)")
        print("//Status Code: \(String(describing: response.statusCode))")
        print("//url")
        print("//\(target.baseURL)\(target.path)")
        print("//Parameters")
        print("//\(String(describing: target.parameters))")
        print("// Header: \(String(describing: target.headers))")
        print("//Result")
        print("\(json)")
        print("--------------***END***------------------")
        if json["status"].intValue == 200 {
            
        }else{
            print(json["message"].stringValue)
        }
    }
    
    static func logout406(){
      //  APIManager.logout(callbackSuccess: { (isSuccess) in
            
            let defaults = UserDefaults.standard
            KeychainWrapper.standard.set(false, forKey: KeychainKeys.isLoggedIn.rawValue)
            UIApplication.shared.applicationIconBadgeNumber = 0
            UserInfo.shareUserInfo = UserInfo()
            let loginManager = LoginManager()
            loginManager.logOut()
            defaults.set("" , forKey: "userIDGG")
            defaults.set("" , forKey: "userIDApple")
            defaults.set(false, forKey: "tunrOnOff")
            
            KeychainWrapper.standard.set("", forKey: KeychainKeys.userName.rawValue)
            KeychainWrapper.standard.set("", forKey: KeychainKeys.passWord.rawValue)
            
            // unRegisterDevicePushToken
//            SendBirdManager.share.unRegisterDevicePushToken()
            
            // Disconnect SEndBird
//            SBDMain.disconnect(completionHandler: {
//                // A current user is disconnected from Sendbird server.
//                Utils.getAppDelegate().isCheckConnectSendbird = false
//            })
//            
            
            Utils.showAlertOKWithAction(controller: Utils.getTopMostViewController()!, title: "Thông báo", message: "Phiên đăng nhập đã hết hạn. Vui lòng đăng nhập lại!") {
                print("OK")
                
                Utils.dismissPopAllViewViewControllers()
                
            }
            
//        }) { (error) in
//            print(error)
//        }
    }
    
    // MARK: - Location
    static func getLocation(page: Int, paginate: Int,key: String, callbackSuccess: @escaping (_ typeCar: [CityName],_ nextPage: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let parameters: Parameters = [
            "page" : page,
            "paginate" : paginate,
            "key" : key
        ]
        self.requestJson(target: .getLocation(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonCityNames = json["data"]["datas"].arrayValue
                    var cityNames = [CityName]()
                    for jsonCityName in jsonCityNames{
                        cityNames.append(CityName.init(json: jsonCityName))
                    }
                    callbackSuccess(cityNames, json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getDistrict(page:Int, location: Int ,key: String, callbackSuccess: @escaping (_ typeCar: [DistrictModel],_ nextPage: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let parameters: Parameters = [
            "page":page,
            "location_code" : location,
            "key" : key
        ]

        self.requestJson(target: .getDistrict(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonCityNames = json["data"]["datas"].arrayValue
                    var cityNames = [DistrictModel]()
                    for jsonCityName in jsonCityNames{
                       
                        cityNames.append(DistrictModel.init(json: jsonCityName))
//                        cityNames.insert(Key.DistrictDefault, at: 0)
                    }
                    callbackSuccess(cityNames, json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    // MARK: - User
    static func login(user: String,
                      providerType: Int, pass: String,fcmToken: String, callbackSuccess: @escaping (_ typeCar: User) -> Void, failed callbackFailure: @escaping (_ message: String,_ statusCode: Int,_ display_name: String,_ password:String ) -> Void) {
        let parameters: Parameters = [
            "display_name" : user,
            "password" : pass,
            "fcm_token" : fcmToken,
            "device": 1,
            "provider_type": providerType,
           "device_id": Utils.getAppDelegate().uuid
        ]
        print(parameters)
        UserInfo.shareUserInfo.baby_info = [BabyInfo]()
        self.requestJson(target: .login(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let user = User.init(json: json["data"]["user"])
                    UserInfo.shareUserInfo.initDataUser(json: json["data"])
                    callbackSuccess(user)
                } else if json["status"].intValue == 5113 {
                    SVProgressHUD.dismiss()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        guard let topvc = ManageApp.shared.topViewController() else {
                            return
                        }
                        let mess = json["message"].string
                        topvc.showAlert(title: "Thông báo", message: mess)
                    }
                } else{
                    UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"])
                    callbackFailure(json["message"].stringValue, json["status"].intValue, json["message"]["display_name"][0].stringValue, json["message"]["password"][0].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra", 0, "", "")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription, 0, "", "")
        }) { (error) in
            callbackFailure(error.localizedDescription, 0, "", "")
        }
    }

    // Register
    static func register (email: String, city_code: Int, display_name: String,password:String,fcmToken:String, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ statusCode: Int, _ message: String,_ phone: String,_ password:String ) -> Void) {
        let parameters: Parameters = [
            "email" : email,
            "display_name" : display_name,
            "password": password,
            "city_code":city_code,
            "fcm_token" : fcmToken,
            "device": 1
        ]
        
        self.requestJson(target: .register(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                   UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"])
               //    UserInfo.shareUserInfo.initDataUser(json: json["data"])
                    callbackSuccess(true)
                }else{
                    callbackFailure(json["status"].intValue ,json["message"]["display_name"][0].stringValue,json["message"]["email"][0].stringValue,json["message"]["password"][0].stringValue)
                }
            } catch {
                callbackFailure(0,"Có lỗi xảy ra","","")
            }
        }, error: { (error) in
            callbackFailure(0,error.localizedDescription,"","" )
        }) { (error) in
            callbackFailure(0,error.localizedDescription,"","")
        }
    }
    
    // register Social
    static func registerSocial (phone: String,
                                ageRange: Int,
                                birthdayBaby: Int,
                                birthdayPreg: Int?,
                                display_name: String,
                                provider_id: String,
                                city_code: Int,
                                fcmToken: String,
                                callbackSuccess: @escaping (_ typeCar: Bool) -> Void,
                                failed callbackFailure: @escaping (_ message: String ,_ provider_id:String,_ phone:String,_ display_name: String, _ statusCode: Int ) -> Void) {
        let parameters: Parameters = [
            "display_name" : display_name,
            "provider_id": provider_id,
            "city_code" : city_code,
            "fcm_token" : fcmToken,
            "age_range" : ageRange,
            "device_id": Utils.getAppDelegate().uuid,
            "phone": phone,
            "birthday_baby": birthdayBaby,
            "birthday_preg": birthdayPreg
        ]
        self.requestJson(target: .registerSocial(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                  print(json["data"])
                  UserInfo.shareUserInfo.initDataUser(json: json["data"])
//                    UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"])
                 //   print(UserInfo.shareUserInfo.id)
                    callbackSuccess(true)
                }else{
                    
                    callbackFailure(json["message"].stringValue,
                                    json["message"]["provider_id"][0].stringValue,
                                    json["message"]["phone"][0].stringValue,
                                    json["message"]["display_name"][0].stringValue,
                                    json["status"].intValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra", "", "", "", 0)
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription, "", "", "", 0)
        }) { (error) in
            callbackFailure(error.localizedDescription, "", "", "", 0)
        }
    }
    
    static func updateInfo(ageRange: Int, userId: Int, birthday: Double, phone:String , cityCode: Int, baby_info : String, typeMom: Int,district_code:String, callbackSuccess: @escaping (_ typeCar: User) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        let parameters: Parameters = [
            "user_id" : userId,
            "city_code" : cityCode,
            "baby_info": baby_info,
            "birthday": birthday,
            "phone": phone,
            "type_mom": typeMom,
            "district_code":district_code,
            "fcm_token" : UserInfo.shareUserInfo.fcm_token,
            "age_range" : ageRange
        ]
        
        print(parameters)

        self.requestJson(target: .updateInfo(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    print(json["data"])
                    let user = User.init(json: json["data"])
                    UserInfo.shareUserInfo.initDataUser(json: json["data"])
                    callbackSuccess(user)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }

    // Login social
    static func loginSocial (provider_id:String,
                             fcm_token:String,
                             providerType: Int,
                             callbackSuccess: @escaping (_ typeCar: Bool, _ user: User) -> Void,
                             failed callbackFailure: @escaping (_ message: String , _ statusCode: Int, _ userId: Int) -> Void) {
        let parameters: Parameters = [
            "provider_id": provider_id,
            "fcm_token" : fcm_token,
             "device": 1,
            "provider_type": providerType,
            "device_id": Utils.getAppDelegate().uuid
        ]
    
        UserInfo.shareUserInfo.baby_info = [BabyInfo]()
        self.requestJson(target: .loginSocial(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let user = User.init(json: json["data"])
                    UserInfo.shareUserInfo.initDataUser(json: json["data"])
                    callbackSuccess(true, user)
                } else if json["status"].intValue == 5113 {
                    SVProgressHUD.dismiss()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        guard let topvc = ManageApp.shared.topViewController() else {
                            return
                        }
                        let mess = json["message"].string
                        topvc.showAlert(title: "Thông báo", message: mess)
                    }
                } else {
                    UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"])
                    callbackFailure(json["message"].stringValue, json["status"].intValue, json["data"]["id"].intValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra", 0, 0)
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription, 0, 0)
        }) { (error) in
            callbackFailure(error.localizedDescription, 0,0)
        }
    }
    
    // Check Name
    static func checkName (display_name:String, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ message: String , _ statusCode: Int) -> Void) {
        let parameters: Parameters = [
            "display_name": display_name
        ]
        self.requestJson(target: .checkName(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(true)
                }else{
                    callbackFailure(json["message"]["display_name"][0].stringValue, json["status"].intValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra", 0)
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription, 0)
        }) { (error) in
            callbackFailure(error.localizedDescription, 0)
        }
    }
    
//    // Check Phone
//       static func checkPhone (phone:String, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ message: String , _ statusCode: Int) -> Void) {
//           let parameters: Parameters = [
//               "phone": phone
//           ]
//           self.requestJson(target: .checkPhone(param: parameters), success: { (response) in
//               do {
//                   let json = try JSON.init(data: response.data)
//                   if json["status"].intValue == 200{
//                       callbackSuccess(true)
//                   }else{
//                       callbackFailure(json["message"]["display_name"][0].stringValue, json["status"].intValue)
//                   }
//               } catch {
//                   callbackFailure("Có lỗi xảy ra", 0)
//               }
//           }, error: { (error) in
//               callbackFailure(error.localizedDescription, 0)
//           }) { (error) in
//               callbackFailure(error.localizedDescription, 0)
//           }
//       }
    
    //Check Email
    static func checkEmail(email:String, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ message: String , _ statusCode: Int) -> Void) {
        
        let parameters: Parameters = [
            "email": email
        ]
        
        self.requestJson(target: .checkEmail(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    callbackSuccess(true)
                }else{
                    
                    callbackFailure(json["message"]["email"][0].stringValue, json["status"].intValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra", 0)
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription, 0)
        }) { (error) in
            callbackFailure(error.localizedDescription, 0)
        }
    }
    
    static func checkPhone(phone:String, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ message: String , _ statusCode: Int) -> Void) {
           
           let parameters: Parameters = [
               "phone": phone
           ]
           
           self.requestJson(target: .checkPhone(param: parameters), success: { (response) in
               do {
                   let json = try JSON.init(data: response.data)
                   if json["status"].intValue == 200{
                       
                       callbackSuccess(true)
                   }else{
                       
                       callbackFailure(json["message"]["email"][0].stringValue, json["status"].intValue)
                   }
               } catch {
                   callbackFailure("Có lỗi xảy ra", 0)
               }
           }, error: { (error) in
               callbackFailure(error.localizedDescription, 0)
           }) { (error) in
               callbackFailure(error.localizedDescription, 0)
           }
       }
    
    
    static func sendOtp(email:String,type: TypeView, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ message: String , _ statusCode: Int) -> Void) {
        
        let parameters: Parameters = [
            "email": email
        ]
        
        let target = type == .register ? BeberiaService.sendOtp(param: parameters) : BeberiaService.forgotPassword(param: parameters)
        
        self.requestJson(target:  target, success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    if type == .forgetPass { UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"]) }
                    callbackSuccess(true)
                }else{
                    
                    callbackFailure(json["message"].stringValue, json["status"].intValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra", 0)
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription, 0)
        }) { (error) in
            callbackFailure(error.localizedDescription, 0)
        }
    }
    
    static func verifyOtp(otp:String, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ message: String , _ statusCode: Int) -> Void) {
          
          let parameters: Parameters = [
              "otp": otp
          ]
          
          self.requestJson(target: .verifyOtp(param: parameters), success: { (response) in
              do {
                  let json = try JSON.init(data: response.data)
                  if json["status"].intValue == 200{
                      UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"])
                      callbackSuccess(true)
                  }else{
                      
                      callbackFailure("", json["status"].intValue)
                  }
              } catch {
                  callbackFailure("Có lỗi xảy ra", 0)
              }
          }, error: { (error) in
              callbackFailure(error.localizedDescription, 0)
          }) { (error) in
              callbackFailure(error.localizedDescription, 0)
          }
      }
    
    static func verifyReset(otp :String, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ message: String , _ statusCode: Int) -> Void) {
         
         let parameters: Parameters = [
             "otp": otp,
             "user_id" : UserInfo.shareUserInfo.id
         ]
         
         self.requestJson(target: .verifyOtpPassForgot(param: parameters), success: { (response) in
             do {
                 let json = try JSON.init(data: response.data)
                 if json["status"].intValue == 200{
                     
                     callbackSuccess(true)
                 }else{
                     
                     callbackFailure(json["message"].stringValue, json["status"].intValue)
                 }
             } catch {
                 callbackFailure("Có lỗi xảy ra", 0)
             }
         }, error: { (error) in
             callbackFailure(error.localizedDescription, 0)
         }) { (error) in
             callbackFailure(error.localizedDescription, 0)
         }
     }
     
    static func changePassForgot(password: String, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ message: String , _ statusCode: Int) -> Void) {
           
           let parameters: Parameters = [
            "user_id" : UserInfo.shareUserInfo.id as Any,
               "password" : password
           ]
           
           self.requestJson(target: .changePassForgot(param: parameters), success: { (response) in
               do {
                   let json = try JSON.init(data: response.data)
                   if json["status"].intValue == 200{
                 //      UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"])
                       callbackSuccess(true)
                   }else{
                       
                       callbackFailure("", json["status"].intValue)
                   }
               } catch {
                   callbackFailure("Có lỗi xảy ra", 0)
               }
           }, error: { (error) in
               callbackFailure(error.localizedDescription, 0)
           }) { (error) in
               callbackFailure(error.localizedDescription, 0)
           }
       }
    
      // MARK: - Diary
    
    // Get Date
    static func getDate(callbackSuccess: @escaping (_ today:Int , _ day_number: String, _ is_writed: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [:]
        
        self.requestJson(target: .getDate(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(json["data"]["today"].intValue, json["data"]["day_number"].stringValue, json["data"]["is_writed"].intValue)
                }else{
                    
                    callbackFailure(json["message"]["phone"][0].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    // Get banner
    static func getBanner(type: Int, callbackSuccess: @escaping (_ typeCar: [Baner]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "type" : type,
        ]
        
        self.requestJson(target: .getBanner(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonBanners = json["data"].arrayValue
                    var banners = [Baner]()
                    for jsonBanner in jsonBanners{
                        banners.append(Baner.init(json: jsonBanner))
                    }
                    callbackSuccess(banners)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    // Get banner
    static func getListSuggest( callbackSuccess: @escaping (_ typeCar: SuggestsModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        self.requestJson(target: .listSuggest(param: [:]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(SuggestsModel.self , from:json["data"].rawData())
                    callbackSuccess(data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    
    // get Diary Home
    
    static func getDiaryHome(callbackSuccess: @escaping (_ typeCar: [DiaryHome]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            :]
        
        self.requestJson(target: .gethomeDiary(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonDiaryHome = json["data"].arrayValue
                    var diaryHome = [DiaryHome]()
                    for jsonDiary in jsonDiaryHome{
                        diaryHome.append(DiaryHome.init(json: jsonDiary))
                    }
                    callbackSuccess(diaryHome)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    // get ListDiary more
    static func getListDiaryMore(type: String, page: Int,id: Int? = nil, callbackSuccess: @escaping (_ typeCar: [Diaries],_ next_page:Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "type" : type,
            "page": page
        ]
        
        let parametersDiaryOther: Parameters = [
            "type" : type,
            "page": page,
            "id": id as Any
        ]
        self.requestJson(target: .getlistDiary(param: id == nil ? parameters : parametersDiaryOther), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonListDiaryMore = json["data"]["diaries"].arrayValue
                    var listDiaryMore = [Diaries]()
                    for jsonDiaryMore in jsonListDiaryMore{
                        listDiaryMore.append(Diaries.init(json: jsonDiaryMore))
                    }
                    callbackSuccess(listDiaryMore, json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    // get search Diary
    static func getSearchDiary(key: String, type:Int,page: Int, callbackSuccess: @escaping (_ typeCar: [Diaries],_ next_page:Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "key" : key,
            "type" : type,
            "page" : page
        ]
        print(parameters)
        
        self.requestJson(target: .getSearchDiary(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonListsearchDiary = json["data"]["diaries"].arrayValue
                    var searchDiary = [Diaries]()
                    for jsonsearchDiary in jsonListsearchDiary{
                        searchDiary.append(Diaries.init(json: jsonsearchDiary))
                    }
                    callbackSuccess(searchDiary,json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
  
    static func editDiary(id: Int, newMedia: Int, title: String,content: String,images: String, isPrivate: Int, contentSearch: String, callbackSuccess: @escaping (_ typeCar: HomeFeed) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let parameters: Parameters = [
            "id": id,
            "title" : title,
            "content" : content,
            "images" : images,
            "is_private" : isPrivate,
            "new_media": newMedia,
            "content_search": contentSearch
        ]
        self.requestJson(target: .editDiary(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(HomeFeed.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    
    static func createDiary(title: String,content: String,images: String, isPrivate: Int,contentSearch: String, isSticker: Bool, callbackSuccess: @escaping (_ typeCar: HomeFeed) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let valueSticker = (isSticker) ? 1 : 0
        let parameters: Parameters = [
            "title" : title,
            "content" : content,
            "images" : images,
            "is_private" : isPrivate,
            "content_search": contentSearch,
            "is_sticker": valueSticker
        ]
        self.requestJson(target: .createDiary(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(HomeFeed.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func checkCompletedDiary(id: Int, callbackSuccess: @escaping (
        _ typeCar: Diaries, _ status: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "id" : id
        ]
        self.requestJson(target: .checkCreateDiary(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonFeeds = Diaries.init(json: json["data"]["diary"])
                    let status = json["data"]["status"].intValue
                    callbackSuccess(jsonFeeds, status)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getDetailDiary(id: Int,  callbackSuccess: @escaping (_ typeCar: HomeFeed) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let parameters: Parameters = [
            "diary_id" : id
        ]

        self.requestJson(target: .detailDiary(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(HomeFeed.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func deleteDiary(id: Int, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameterDiary: Parameters = [
            "id" : id
        ]

        self.requestJson(target: .deleteDiary(param: parameterDiary), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(true)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func settingNotification(id: Int, turnOn:Int, callbackSuccess: @escaping (_ typeCar: Int) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameters: Parameters = [
            "id" : id,
            "turnOn": turnOn
        ]
        self.requestJson(target: .settingNoti(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    print(json["message"].stringValue)
                    print(json["data"]["turnOn"].intValue)
                    callbackSuccess(json["data"]["turnOn"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
        // MARK: - Infomation
    static func getCategoryShow(parentId: Int,  callbackSuccess: @escaping (_ typeCar: [Category]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "id" : parentId
        ]
        self.requestJson(target: .getCategoryShow(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonCategories = json["data"].arrayValue
                    var categories = [Category]()
                    for jsonCategory in jsonCategories{
                        categories.append(Category.init(json: jsonCategory))
                    }
                    callbackSuccess(categories)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getCategory(category_id: Int,type:Int,location:String,  callbackSuccess: @escaping (_ typeCar: [Category]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
            let parameters: Parameters = [
                "category_id" : category_id,
                "type":type
//                "location":location
            ]
    
            self.requestJson(target: .getCategory(param: parameters), success: { (response) in
                do {
                    let json = try JSON.init(data: response.data)
                    if json["status"].intValue == 200{
                        let jsonCategories = json["data"].arrayValue
                        var categories = [Category]()
                        for jsonCategory in jsonCategories{
                            categories.append(Category.init(json: jsonCategory))
                        }
                        callbackSuccess(categories)
                    }else{
                       callbackFailure(json["message"].stringValue)
                    }
                } catch {
                     callbackFailure("Có lỗi xảy ra")
                }
            }, error: { (error) in
                callbackFailure(error.localizedDescription)
            }) { (error) in
               callbackFailure(error.localizedDescription)
            }
        }
    // get detail load more
    static func getDetailLoadMore(category_id: Int,page: Int,  callbackSuccess: @escaping (_ typeCar: [PostDetailModel] ,_ next_page: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let parameters: Parameters = [
            "category_id" : category_id,
            "page": page
        ]
        self.requestJson(target: .getDetailMore(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonDetailMore = json["data"]["posts"].arrayValue
                    var detailMore = [PostDetailModel]()
                    for jsonDetail in jsonDetailMore{
                        detailMore.append(PostDetailModel.init(json: jsonDetail))
                    }
                    callbackSuccess(detailMore, json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    //get search infomation

    static func getSearchInfo(key: String, type:Int, page: Int, callbackSuccess: @escaping (_ typeCar: [PostDetailModel],_ next_page:Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "key" : key,
            "type" : type,
            "page" : page
        ]

        self.requestJson(target: .getSearchDiary(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonListsearchInfo = json["data"]["posts"].arrayValue

                    var searchInfo = [PostDetailModel]()
                    for jsonsearchInfo in jsonListsearchInfo{
                        searchInfo.append(PostDetailModel.init(json: jsonsearchInfo))
                    }
                    callbackSuccess(searchInfo,json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getPostDetail(idPost: Int,  callbackSuccess: @escaping (_ typeCar: PostDetailModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let parameters: Parameters = [
            "id" : idPost
        ]
        self.requestJson(target: .getPostDetail(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(PostDetailModel.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    // type: 0 là bài post , 1 là comment
    static func likePost(idPost: Int,type: Int,  callbackSuccess: @escaping (_ numberLike: Int,
        _ checkLike: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let parameters: Parameters = [
            "id" : idPost,
            "type" : type
        ]
        self.requestJson(target: .likePost(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(json["data"]["like_number"].intValue, json["data"]["is_liked"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func favouritePost(idPost: Int, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "post_id" : idPost
        ]
        self.requestJson(target: .favouritePost(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(true)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getComment(_ postID: Int,page: Int,type: Int?, callbackSuccess: @escaping (_ comments: [Comment],_ nextPage :Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
            let parametersInfomation: Parameters = [
                "post_id"       : postID,
                "page"          : page
            ]
        
            let parametersHome: Parameters = [
                "id"       : postID,
                "page"          : page,
                "type"          : type as Any
            ]

        let taget = (type == nil) ? BeberiaService.getComment(param: parametersInfomation) : BeberiaService.getCommentFeed(param: parametersHome)

            self.requestJson(target: taget, success: { (response) in
                do {
                    let json = try JSON.init(data: response.data)
                    if json["status"].intValue == 200{
                        var jsonComments = [JSON]()
                        jsonComments = json["data"]["datas"].arrayValue
                        var comments = [Comment]()
                        for jsonComment in jsonComments{
                            comments.append(Comment.init(json: jsonComment))
                        }
                        callbackSuccess(comments, json["data"]["next_page"].intValue)
                    }else{
                        callbackFailure(json["message"].stringValue)
                    }
                } catch {
                    callbackFailure("Có lỗi xảy ra")
                }
            }, error: { (error) in
                callbackFailure(error.localizedDescription)
            }) { (error) in
                callbackFailure(error.localizedDescription)
            }
        }
    
    static func getCommentReply(_ postID: Int,page: Int, callbackSuccess: @escaping (_ comments: [Comment],_ nextPage :Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parametersFeed: Parameters = [
            "parent_comment"       : postID,
            "page"          : page
        ]
        
        print(parametersFeed)
        
        self.requestJson(target: .getCommentReply(param: parametersFeed), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonComments = json["data"]["list_reply_comment"].arrayValue
                    var comments = [Comment]()
                    for jsonComment in jsonComments{
                        comments.append(Comment.init(json: jsonComment))
                    }
                    callbackSuccess(comments, json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func sendComment(_ postID: Int, parentId: Int?,content: String, userTag : Int, callbackSuccess: @escaping (_ comment: Comment) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        
        let parameterssCommentReply: Parameters = [
            "post_id"       : postID,
            "parent_id"     : parentId as Any,
            "content"       : content,
            "user_tag"      : userTag
        ]
        
        let parametersCommentPost: Parameters = [
            "post_id"       : postID,
            "content": content
        ]

        self.requestJson(target: .sendComment(param: parentId == nil ? parametersCommentPost : parameterssCommentReply), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(Comment.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func deleteComment(_ postID: Int,type: Int?, callbackSuccess: @escaping (_ comment: Bool) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        var parameters: Parameters = [String:Any]()
        switch type {
        case nil:
            parameters = [
                "id"       : postID
            ]
        default:
            parameters = [
                "id"       : postID,
                "type": type as Any
            ]
        }
        
        let tagret:BeberiaService = (type == nil) ? .deleteComment(param: parameters) : .deleteCommentHomeFeed(param: parameters)
        self.requestJson(target: tagret, success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(true)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func editComment(_ postID: Int,_ content: String,type: Int?, callbackSuccess: @escaping (_ comment: Comment) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        
        let parametersInfomation: Parameters = [
            "id"       : postID,
            "content"  : content
        ]
        
        let parametersHomeComment: Parameters = [
            "id"       : postID,
            "content"  : content,
            "type"      : type as Any
        ]
        
        let parametersHomeCommentReply: Parameters = [
            "id"       : postID,
            "content"  : content,
            "type"     : type as Any,
            "user_tag" : Utils.json(from: []) as Any
        ]
        
        var taget = BeberiaService.editComment(param: parametersInfomation)
        
        switch type {
        case nil:
            taget = BeberiaService.editComment(param: parametersInfomation)
        case 4:
            taget = BeberiaService.editCommentFeed(param: parametersHomeComment)
        case 5:
            taget = BeberiaService.editCommentFeed(param: parametersHomeCommentReply)
        default:
            print("sqwe")
        }
        
        self.requestJson(target: taget, success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(Comment.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    // MARK: - Home
    static func getHomeFeed(tokenPage: String,locationCode: Int, about:Int, category:Int, ageRange:Int, district_code:String, callbackSuccess: @escaping (_ typeCar: [HomeFeed], _ nextPage: String) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "tokenPage" : tokenPage,
            "location_code" : locationCode,
            "about":about,
            "category":category,
            "ageRange":ageRange,
            "district_code": district_code
        ]
        print(parameters)
        
        self.requestJson(target: .getHomeFeed(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonhHomeFeeds = json["data"]["datas"].arrayValue
                    var homeFeeds = [HomeFeed]()
                    for jsonHomeFeed in jsonhHomeFeeds{
                        homeFeeds.append(HomeFeed.init(json: jsonHomeFeed))
                    }
                    callbackSuccess(homeFeeds, json["data"]["tokenPage"].stringValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getDetailFeed(id: Int,  callbackSuccess: @escaping (_ typeCar: HomeFeed) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "id" : id
        ]
        print(parameters)
        
        self.requestJson(target: .detailFeed(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(HomeFeed.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func createHomeFeed(title: String,content: String,images: String,videos: String,city_code: Int,about: Int,category: Int,content_search: String,ageRange: Int,district_code:String ,subTitle: Int, callbackSuccess: @escaping (_ typeCar: HomeFeed) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "title" : title,
            "content" : content,
            "images" : images,
            "videos" : videos,
            "city_code" : city_code,
            "about" : about,
            "category" : category,
            "content_search" : content_search,
            "age_range" : ageRange,
            "ads": 0,
            "district_code": district_code,
            "subTitle": subTitle
        ]
        
        self.requestJson(target: .createFeed(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(HomeFeed.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }

    // get search Home
    static func getSearchHome(key: String,
                              page: Int,
                              cityCode: Int,
                              category: Int,
                              districtCode:String ,
                              tokenPage: String,
                              callbackSuccess: @escaping (_ typeCar: [HomeFeed],_ tokenPage:String) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "key" : key,
//            "page": page,
            "city_code": cityCode,
            "category": category,
            "district_code": districtCode,
            "tokenPage": tokenPage
        ]
 
        self.requestJson(target: .getSearchHome(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonhSearchHome = json["data"]["datas"].arrayValue
                    var searchHome = [HomeFeed]()
                    for jsonHomeFeed in jsonhSearchHome{
                        searchHome.append(HomeFeed.init(json: jsonHomeFeed))
                    }
                    callbackSuccess(searchHome, json["data"]["tokenPage"].stringValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func likeHomeFeed(id: Int, type: Int, checkLike: Int,callbackSuccess: @escaping (_ numberLike: Int,
        _ checkLike: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "id" : id,
            "type" : type,
            "check_like" : checkLike
        ]
        
        self.requestJson(target: .likeHomeFeed(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(json["data"]["number_like"].intValue, json["data"]["check_like"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func checkTime(callbackSuccess: @escaping ((CheckTimeModel, String)) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        self.requestJson(target: .checkTime(param: [:]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200 {
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(CheckTimeModel.self , from:json["data"].rawData())
                    callbackSuccess((data, ""))
                } else if json["status"].intValue == 40010 {
                    let decoder = JSONDecoder()
                    let message = json["message"].stringValue
                    let data = try decoder.decode(CheckTimeModel.self , from:json["data"].rawData())
                    callbackSuccess((data, message))
                } else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func saveHomeFeed(id: Int, checkSave: Int,callbackSuccess: @escaping (
        _ checkSave: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "id" : id,
            "check_save" : checkSave
        ]
        
        self.requestJson(target: .saveHomeFeed(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(json["data"]["check_save"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func checkCompletedFeed(id: Int, callbackSuccess: @escaping (
        _ typeCar: HomeFeed, _ status: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "id" : id
        ]
        self.requestJson(target: .checkCompletedFeed(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonFeeds = HomeFeed.init(json: json["data"]["feed"])
                    let status = json["data"]["status"].intValue
                    callbackSuccess(jsonFeeds, status)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func sendCommentHomeFeed(_ idFeed: Int,
                                    type: Int?,
                                    content: String,
                                    userTag: [Int],
                                    callbackSuccess: @escaping (_ comment: Comment) -> Void,
                                    failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        
        let parametersComment: Parameters = [
            "id": idFeed,
            "type": 4,
            "content": content,
            "images": "[]",
            "videos": "[]",
            "user_tag": Utils.json(from: userTag) as Any
        ]
        
        let parametersReply: Parameters = [
            "id": idFeed,
            "type": 5,
            "content": content,
            "images": "[]",
            "videos": "[]",
            "user_tag": Utils.json(from: userTag) as Any
        ]
        
        let parameters = (type == 4) ? parametersComment : parametersReply
        
        self.requestJson(target: .sendCommentHomeFeed(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(Comment.init(json: json["data"]))
                } else if json["status"].intValue == 40013 {
                    callbackFailure(json["message"].stringValue)
                }  else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func cancelAndDelete(id: Int,type: Int?, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameterCancel: Parameters = [
            "id" : id
        ]
        
        let parameterDelete: Parameters = [
            "id" : id,
            "type": type as Any
        ]
        
        self.requestJson(target: .deleteAndCancelFeed(param: (type == nil ) ? parameterCancel : parameterDelete), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
      
                //    UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"]["user"])
                     callbackSuccess(true)
                }else{
                    
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func editFeed(id: Int, title: String, city_code: Int, content: String,images: String ,about: Int,category: Int,newMedia:Int, contentSearch:String,ageRange:Int,district_code: String, callbackSuccess: @escaping (_ typeCar: HomeFeed) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameterEditFeed: Parameters = [
            "id" : id,
            "title" : title,
            "content" : content,
            "images" : images,
            "about" : about,
            "category" : category,
            "new_media" : newMedia,
            "city_code" : city_code,
            "content_search" : contentSearch,
            "age_range" : ageRange,
            "ads": 0,
            "district_code":district_code
        ]
        
        self.requestJson(target: .editFeed(param: parameterEditFeed), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(HomeFeed.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func readNotification(id: Int, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameters: Parameters = [
            "id" : id
        ]
        
        self.requestJson(target: .readNotification(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(true)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func turnOnOffNotification(id: Int, turnOn:Int, callbackSuccess: @escaping (_ typeCar: Int) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
        
        let parameters: Parameters = [
            "id" : id,
            "turnOn": turnOn
        ]
        self.requestJson(target: .turnOnOffNotification(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(json["data"]["turnOn"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }

    // MARK: Profile
    // get list favorite
    static func getListDiaryFavorite(type: String,page: Int, callbackSuccess: @escaping (_ typeCar: [Diaries],_ next_page:Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let parameters: Parameters = [
            "type" : type,
            "page": page
        ]
        self.requestJson(target: .listFavorite(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let jsonListDiaryMore = json["data"]["diaries"].arrayValue
                    var listDiaryMore = [Diaries]()
                    for jsonDiaryMore in jsonListDiaryMore{
                        listDiaryMore.append(Diaries.init(json: jsonDiaryMore))
                    }
                    callbackSuccess(listDiaryMore,json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    //get list infomation Favorite
    static func getListInfoFavorite(type: String,page: Int,  callbackSuccess: @escaping (_ typeCar: [PostDetailModel],_ next_page:Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let parameters: Parameters = [
            "type" : type,
            "page" : page
        ]
        self.requestJson(target: .listFavorite(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonInfoMark = json["data"]["posts"].arrayValue
                    var infoMark = [PostDetailModel]()
                    for jsonInfo in jsonInfoMark{
                        infoMark.append(PostDetailModel.init(json: jsonInfo))
                    }
                    callbackSuccess(infoMark, json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }

    // get profile feed
    static func getProfileFeed(id: Int, type: Int,page: Int,callbackSuccess: @escaping (_ typeCar: [HomeFeed],_ next_page: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "id" : id,
            "type":type,
            "page":page
        ]

        self.requestJson(target: .getFeedbyUser(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonhHomeFeeds = json["data"]["datas"].arrayValue
                    var homeFeeds = [HomeFeed]()

                    for jsonHomeFeed in jsonhHomeFeeds{
                        homeFeeds.append(HomeFeed.init(json: jsonHomeFeed))
                    }
                    callbackSuccess(homeFeeds,json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
                    

    // get notification
    static func getListNoti(page: Int,  callbackSuccess: @escaping (_ typeCar: [Noti],_ next_page: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "page" : page
        ]
      
        
        self.requestJson(target: .getNoti(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let jsonNotis = json["data"]["datas"].arrayValue
                    var listNotis = [Noti]()
                    for jsonNoti in jsonNotis{
                        listNotis.append(Noti.init(json: jsonNoti))
                    }
                    
                    callbackSuccess(listNotis,json["data"]["next_page"].intValue )
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }

    
    //update info profile
    
    static func editProfile (image: [String], birthday: Double, birthday_baby: Double, id_baby:Int, city_code: Int, district_code:String,address:String, phone:String, callbackSuccess: @escaping (_ typeCar: User) -> Void, failed callbackFailure: @escaping (_ message: String ) -> Void) {
       
        let parameters: Parameters = [
            "birthday" : birthday,
            "city_code": city_code,
            "birthday_baby":birthday_baby,
            "id_baby":id_baby,
            "address":address,
            "phone":phone,
            "district_code": district_code,
            "image":  Utils.json(from: image) as Any
        ]

        self.requestJson(target: .updateInfoProfile(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let user = User.init(json: json["data"]["user"])
                    UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"]["user"])
                    callbackSuccess(user)
                }else{
                    
                    if !json["message"]["display_name"][0].stringValue.isEmpty {
                        callbackFailure(json["message"]["display_name"][0].stringValue)
                    
                    }
                    
                    if !json["message"]["phone"][0].stringValue.isEmpty {
                        callbackFailure(json["message"]["phone"][0].stringValue)
                    }
                    
                    
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription )
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    // edit baby profile
    static func editBabyProfile (id: Int,
                                 gender: Int,
                                 nickname: String,
                                 birthday:Double,
                                 image: String? = nil,
                                 callbackSuccess: @escaping (_ typeCar: BabyInfo) -> Void,
                                 failed callbackFailure: @escaping (_ message: String ) -> Void) {
        
        var parameters: Parameters = [
            "id" : id,
            "gender": gender,
            "nickname": nickname,
            "birthday": birthday
        ]
        if let image = image {
            parameters["image"] = image
        }
        print()
        
        self.requestJson(target: .editBabyProfile(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let baby = BabyInfo.init(json: json["data"])
                    callbackSuccess(baby)
                }else{
                    
                    callbackFailure(json["message"]["display_name"][0].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription )
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    // add bayby profile
    static func addBabyProfile ( gender: Int,nickname: String,birthday:Double,image: String = "[]", callbackSuccess: @escaping (_ typeCar: BabyInfo) -> Void, failed callbackFailure: @escaping (_ message: String ) -> Void) {
        
        let parameters: Parameters = [
            "gender": gender,
            "nickname": nickname,
            "birthday": birthday,
            "image": image,
        ]

        self.requestJson(target: .addBabyProfile(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let baby = BabyInfo.init(json: json["data"])
                    callbackSuccess(baby)
                }else{
                    callbackFailure(json["message"]["display_name"][0].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription )
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    // delete baby profile
       static func deleteBabyProfile ( idBaby: Int, callbackSuccess: @escaping (_ typeCar: BabyInfo) -> Void, failed callbackFailure: @escaping (_ message: String ) -> Void) {
           
           let parameters: Parameters = [
               "id_baby": idBaby
           ]

           self.requestJson(target: .deleteBaby(param: parameters), success: { (response) in
               do {
                   let json = try JSON.init(data: response.data)
                   if json["status"].intValue == 200{
                    UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"]["user"])
                       let baby = BabyInfo.init(json: json["data"])
                       callbackSuccess(baby)
                   }else{
                       
                       callbackFailure(json["message"]["display_name"][0].stringValue)
                   }
               } catch {
                   callbackFailure("Có lỗi xảy ra")
               }
           }, error: { (error) in
               callbackFailure(error.localizedDescription )
           }) { (error) in
               callbackFailure(error.localizedDescription)
           }
       }

    // change password
    
    static func changePassword (old_password: String,new_password:String, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ old_password: String,_ new_password:String,_ statusCode: Int,_ mess: String ) -> Void) {
        
        let parameters: Parameters = [
            "old_password" : old_password,
            "new_password": new_password
        ]
        self.requestJson(target: .changePassWord(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
//                    UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"])
                    callbackSuccess(true)
                }else{
                    callbackFailure(json["message"]["old_password"][0].stringValue,json["message"]["new_password"][0].stringValue,json["status"].intValue, json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra","",0,"")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription,"",0, "" )
        }) { (error) in
            callbackFailure(error.localizedDescription,"",0,"")
        }
    }
    // Logout
        static func logout(callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
    
            let parameters: Parameters = [:]
            self.requestJson(target: .logout(param: parameters), success: { (response) in
                do {
                    let json = try JSON.init(data: response.data)
                    if json["status"].intValue == 200{
                        callbackSuccess(true)
                    }else{
                        callbackFailure(json["message"].stringValue)
                    }
                } catch {
                    callbackFailure("")
                }
            }, error: { (error) in
                callbackFailure(error.localizedDescription)
            }) { (error) in
                callbackFailure(error.localizedDescription)
            }
        }
    
    // MARK: Secret
    
     static func createPassSecret (password_secrect: String, callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ mess: String ) -> Void) {
            
            let parameters: Parameters = [
                "password_secret" : password_secrect
            ]
            self.requestJson(target: .createPassSecret(param: parameters), success: { (response) in
                do {
                    let json = try JSON.init(data: response.data)
                    if json["status"].intValue == 200{
                        callbackSuccess(true)
                    }else{
                        callbackFailure(json["message"].stringValue)
                    }
                } catch {
                    callbackFailure("Có lỗi xảy ra")
                }
            }, error: { (error) in
                callbackFailure(error.localizedDescription)
            }) { (error) in
                callbackFailure(error.localizedDescription)
            }
        }
    
    
        static func getListNotiSecret(page: Int,  callbackSuccess: @escaping (_ typeCar: [Noti],_ next_page: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
               
               let parameters: Parameters = [
                   "page" : page
               ]
             
               
               self.requestJson(target: .getNotiSecret(param: parameters), success: { (response) in
                   do {
                       let json = try JSON.init(data: response.data)
                       if json["status"].intValue == 200{
                           let jsonNotis = json["data"]["datas"].arrayValue
                           var listNotis = [Noti]()
                           for jsonNoti in jsonNotis{
                               listNotis.append(Noti.init(json: jsonNoti))
                           }
                           
                           callbackSuccess(listNotis,json["data"]["next_page"].intValue )
                       }else{
                           callbackFailure(json["message"].stringValue)
                       }
                   } catch {
                       callbackFailure("Có lỗi xảy ra")
                   }
               }, error: { (error) in
                   callbackFailure(error.localizedDescription)
               }) { (error) in
                   callbackFailure(error.localizedDescription)
               }
           }
    
    
    
//    static func changeAvatar (image: [String], callbackSuccess: @escaping (_ typeCar: Bool) -> Void, failed callbackFailure: @escaping (_ statusCode: Int,_ mess: String ) -> Void) {
//
//        let parameters: Parameters = [
//            "image" : Utils.json(from: image) as Any
//        ]
//
//        self.requestJson(target: .changeAvatar(param: parameters), success: { (response) in
//            do {
//                let json = try JSON.init(data: response.data)
//                if json["status"].intValue == 200{
//                    UserInfo.shareUserInfo.initDataUserWithoutToken(json: json["data"]["user"])
//                    callbackSuccess(true)
//                }else{
//
//                    callbackFailure(json["status"].intValue, json["message"].stringValue)
//                }
//            } catch {
//                callbackFailure(0,"")
//            }
//        }, error: { (error) in
//            callbackFailure(0, error.localizedDescription)
//        }) { (error) in
//            callbackFailure(0,error.localizedDescription)
//        }
//    }
    
    static func getSecretHome (key: String, page: Int, secret: Int, subTitle: Int, filter_date: Int  , callbackSuccess: @escaping (_ typeCar: [HomeFeed],_ next_page:Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
           
           let parameters: Parameters = [
               "key" : key,
               "page": page,
               "secret": 1,
               "subTitle": subTitle,
               "filter_date":filter_date
           ]

           self.requestJson(target: .getSecretHome(param: parameters), success: { (response) in
               do {
                   let json = try JSON.init(data: response.data)
                   if json["status"].intValue == 200{
                       let jsonhSearchHome = json["data"]["datas"].arrayValue
                       var searchHome = [HomeFeed]()
                       for jsonHomeFeed in jsonhSearchHome{
                           searchHome.append(HomeFeed.init(json: jsonHomeFeed))
                       }
                       callbackSuccess(searchHome, json["data"]["next_page"].intValue)
                   }else{
                       callbackFailure(json["message"].stringValue)
                   }
               } catch {
                   callbackFailure("Có lỗi xảy ra")
               }
           }, error: { (error) in
               callbackFailure(error.localizedDescription)
           }) { (error) in
               callbackFailure(error.localizedDescription)
           }
       }
    
    // MARK: Menstrual
    
    static func createCycleMenstrual (distance: Int, cycle_time: Int, date_cycle: Double, callbackSuccess: @escaping (_ menstrual: [Int], _ ovulation: [Int], _ conception: [Int]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
              
              let parameters: Parameters = [
                  "distance" : distance,
                  "cycle_time": cycle_time,
                  "date_cycle": date_cycle
              ]

              self.requestJson(target: .createCycleMenstrual(param: parameters), success: { (response) in
                  do {
                      let json = try JSON.init(data: response.data)
                      if json["status"].intValue == 200{
 
                        callbackSuccess(json["data"]["cycle"]["menstrual"].arrayObject as! [Int], json["data"]["cycle"]["ovulation"].arrayObject as! [Int], json["data"]["cycle"]["conception"].arrayObject as! [Int])
                      }else{
                          callbackFailure(json["message"].stringValue)
                      }
                  } catch {
                      callbackFailure("Có lỗi xảy ra")
                  }
              }, error: { (error) in
                  callbackFailure(error.localizedDescription)
              }) { (error) in
                  callbackFailure(error.localizedDescription)
              }
          }
    
     static func getCycleMenstrual (month: Double, callbackSuccess: @escaping (_ menstrual: [Int], _ ovulation: [Int], _ conception: [Int]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
                 
                 let parameters: Parameters = [
                     "month" : month
                 ]

                 self.requestJson(target: .getCycleMenstrual(param: parameters), success: { (response) in
                     do {
                         let json = try JSON.init(data: response.data)
                         if json["status"].intValue == 200{
    
                           callbackSuccess(json["data"]["menstrual"].arrayObject as! [Int], json["data"]["ovulation"].arrayObject as! [Int], json["data"]["conception"].arrayObject as! [Int])
                         }else{
                             callbackFailure(json["message"].stringValue)
                         }
                     } catch {
                         callbackFailure("Có lỗi xảy ra")
                     }
                 }, error: { (error) in
                     callbackFailure(error.localizedDescription)
                 }) { (error) in
                     callbackFailure(error.localizedDescription)
                 }
             }
    
    // MARK: Add Friend
       
    static func handleActionRequest (follow_id: Int, callbackSuccess: @escaping (_ is_follow: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
                 
                 let parameters: Parameters = [
                     "follow_id" : follow_id
                 ]

                 self.requestJson(target: .handleActionRequest(param: parameters), success: { (response) in
                     do {
                         let json = try JSON.init(data: response.data)
                         if json["status"].intValue == 200{
    
                            callbackSuccess(json["data"]["is_follow"].intValue)
                         }else{
                             callbackFailure(json["message"].stringValue)
                         }
                     } catch {
                         callbackFailure("Có lỗi xảy ra")
                     }
                 }, error: { (error) in
                     callbackFailure(error.localizedDescription)
                 }) { (error) in
                     callbackFailure(error.localizedDescription)
                 }
             }
       
    static func confirmCancelRequest (user_id: Int,type: Int, callbackSuccess: @escaping (_ is_follow: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
                     
                     let parameters: Parameters = [
                         "user_id" : user_id,
                         "type" : type
                     ]

                     self.requestJson(target: .confirmCancelRequest(param: parameters), success: { (response) in
                         do {
                             let json = try JSON.init(data: response.data)
                             if json["status"].intValue == 200{
        
                                callbackSuccess(json["data"]["is_follow"].intValue)
                             }else{
                                 callbackFailure(json["message"].stringValue)
                             }
                         } catch {
                             callbackFailure("Có lỗi xảy ra")
                         }
                     }, error: { (error) in
                         callbackFailure(error.localizedDescription)
                     }) { (error) in
                         callbackFailure(error.localizedDescription)
                     }
                 }
    
    static func searchNameFriend (key: String,page: Int, callbackSuccess: @escaping (_ user: [User],_ nextPage: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
                 
                 let parameters: Parameters = [
                     "key" : key,
                     "page" : page
                 ]

                 self.requestJson(target: .searchNameFriend(param: parameters), success: { (response) in
                     do {
                         let json = try JSON.init(data: response.data)
                         if json["status"].intValue == 200{
                            let jsonhSearchHome = json["data"]["datas"].arrayValue
                            var searchHome = [User]()
                            for jsonHomeFeed in jsonhSearchHome{
                                searchHome.append(User.init(json: jsonHomeFeed))
                            }
    
                            callbackSuccess(searchHome, json["data"]["next_page"].intValue)
                         }else{
                             callbackFailure(json["message"].stringValue)
                         }
                     } catch {
                         callbackFailure("Có lỗi xảy ra")
                     }
                 }, error: { (error) in
                     callbackFailure(error.localizedDescription)
                 }) { (error) in
                     callbackFailure(error.localizedDescription)
                 }
             }
    
    // MARK: BMI Index
    
    static func createBMI (height: Double,weight: Double, circumference: Int, baby_id: Int,date: Double, callbackSuccess: @escaping (_ user: BMIModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
                    
                    let parameters: Parameters = [
                        "height" : height,
                        "weight" : weight,
                        "circumference" : circumference,
                        "baby_id" : baby_id,
                        "date" : date
                    ]

                    self.requestJson(target: .createBMI(param: parameters), success: { (response) in
                        do {
                            let json = try JSON.init(data: response.data)
                            if json["status"].intValue == 200{
                               
                                callbackSuccess( BMIModel.init(json: json["data"]))
                            }else{
                                callbackFailure(json["message"].stringValue)
                            }
                        } catch {
                            callbackFailure("Có lỗi xảy ra")
                        }
                    }, error: { (error) in
                        callbackFailure(error.localizedDescription)
                    }) { (error) in
                        callbackFailure(error.localizedDescription)
                    }
                }
    
    static func getListBMI ( baby_id: Int, callbackSuccess: @escaping (_ user: [BMIModel]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "baby_id" : baby_id
        ]
        
        self.requestJson(target: .getListBMI(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let jsonhSearchHome = json["data"].arrayValue
                    var searchHome = [BMIModel]()
                    for jsonHomeFeed in jsonhSearchHome{
                        searchHome.append(BMIModel.init(json: jsonHomeFeed))
                    }
                    
                    callbackSuccess(searchHome)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getDataStandard ( gender: Int,page: Int, callbackSuccess: @escaping (_ user: [DataStandardModel], _ totalPage: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
           
           let parameters: Parameters = [
               "gender" : gender,
               "page" : page
           ]
           
           self.requestJson(target: .getDataStandard(param: parameters), success: { (response) in
               do {
                   let json = try JSON.init(data: response.data)
                   if json["status"].intValue == 200{
                       
                       let jsonhSearchHome = json["data"]["datas"].arrayValue
                       var searchHome = [DataStandardModel]()
                       for jsonHomeFeed in jsonhSearchHome{
                           searchHome.append(DataStandardModel.init(json: jsonHomeFeed))
                       }
                       
                       callbackSuccess(searchHome, json["data"]["total_page"].intValue)
                   }else{
                       callbackFailure(json["message"].stringValue)
                   }
               } catch {
                   callbackFailure("Có lỗi xảy ra")
               }
           }, error: { (error) in
               callbackFailure(error.localizedDescription)
           }) { (error) in
               callbackFailure(error.localizedDescription)
           }
       }
    
    // MARK: Active Card
    
    static func verifedNumberCard (number_card: String, callbackSuccess: @escaping (_ verified_card: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "number_card" : number_card
        ]
        
        self.requestJson(target: .verifedNumberCard(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess( json["data"]["verified_card"].intValue)
                }else{
                    if json["status"].intValue == 416{
                        callbackFailure("Số thẻ đã có người sử dụng!")
                    }else{
                        // callbackFailure(json["message"].stringValue)
                        callbackFailure("Số thẻ xác thực không đúng!")
                    }
                    
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    
    static func getPolicySale ( page: Int, callbackSuccess: @escaping (_ user: [PolicySaleModel], _ page: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "page" : page
        ]
        
        self.requestJson(target: .getPolicySale(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let jsonhSearchHome = json["data"]["datas"].arrayValue
                    var searchHome = [PolicySaleModel]()
                    for jsonHomeFeed in jsonhSearchHome{
                        searchHome.append(PolicySaleModel.init(json: jsonHomeFeed))
                    }
                    
                    callbackSuccess(searchHome, json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getProfile (userID: Int, callbackSuccess: @escaping (_ user: User) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "user_id" : userID
        ]

        self.requestJson(target: .getProfile(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                   
                    callbackSuccess( User.init(json: json["data"]))
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getPrice (page: Int, callbackSuccess: @escaping (_ user: [PricePrintModel]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "page" : page
        ]

        self.requestJson(target: .getListPrice(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([PricePrintModel].self , from:json["data"]["datas"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func review (callbackSuccess: @escaping (_ user: [Renew]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [:]

        self.requestJson(target: .review(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([Renew].self , from:json["data"]["datas"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getListFrame (callbackSuccess: @escaping (_ user: [FrameBabyface]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .getListFrame(param: [:]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([FrameBabyface].self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func createReOrder(orderId: Int,
                             contactEmail: String,
                             contactName: String,
                             phone: String,
                             message: String,
                             filesChild: [UIImage],
                             imagelink: [String],
                             pay_type:Int,
                             frame: String,
                             gender: String,
                             callbackSuccess: @escaping (_ user: OrderModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let headers = [
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer \(UserInfo.shareUserInfo.token)"
        ]
        
        let parameters: Parameters = [
            "gender": gender,
            "order_id": orderId,
            "contactEmail": contactEmail,
            "contactName": contactName,
            "phone": phone,
            "pay_type": pay_type,
            "message": message,
            "frame": frame,
//            "image_link[]": imagelink
        ]
        
        let url = "\(ConstantApp.shared.environment.text)/api/v1/babyface/update-order"
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            filesChild.indices.forEach {
                if filesChild[$0] != R.image.img_placeHolder_upload()!{
                    multipartFormData.append(filesChild[$0].jpegData(compressionQuality:1)!, withName: "filesChild[]", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                }
                
            }
            
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, method: .post, headers: headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print("uploading \(progress)")
                })
                upload.responseJSON { response in
                    //print response.result
                    guard let json = response.result.value as? [String:Any] else {return}
                    print("------------ API RESPONSE ---------")
                    print("//url")
                    print("//\(url)")
                    print("//Parameters")
                    print("//\(String(describing: parameters))")
                    print("//token \(UserInfo.shareUserInfo.token)")
                    print("//Result")
                    print("\(json)")
                    print("--------------***END***------------------")
                
                    
                    do {
                        let json = try JSON.init(data: response.data!)
                        if json["status"].intValue == 200{
                            
                            let decoder = JSONDecoder()
                            let data = try decoder.decode(OrderModel.self , from:json["data"].rawData())
                            callbackSuccess( data)
                        }else{
                            callbackFailure(json["message"].stringValue)
                        }
                    } catch {
                        callbackFailure("Có lỗi xảy ra")
                    }
                    
                 
                    
                }
            case .failure( _): break
                print ("ádasdasd")
            }
        }
    }
    
    static func createOrder (item_id: Int,
                             contactEmail: String,
                             contactName: String,
                             phone: String,
                             address: String?,
                             message: String,
                             filesChild: [UIImage],
                             filesParent: [UIImage],
                             frame: String,
                             city_code: Int?,
                             district_code: String?,
                             pay_type:Int,
                             gender: String,
                             callbackSuccess: @escaping (_ user: OrderModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let headers = [
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer \(UserInfo.shareUserInfo.token)"
        ]
        
        let parameters: Parameters = [
            "gender": gender,
            "item_id": item_id,
            "contactEmail": contactEmail,
            "contactName": contactName,
            "phone": phone,
//            "address": address,
            "message": message,
            "frame": frame,
//            "city_code": city_code,
//            "district_code" : district_code,
            "pay_type": pay_type
        ]
        
        let url = "\(ConstantApp.shared.environment.text)/api/v1/babyface/store"
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            filesChild.indices.forEach {
                if filesChild[$0] != R.image.img_placeHolder_upload()!{
                    multipartFormData.append(filesChild[$0].jpegData(compressionQuality:1)!, withName: "filesChild[]", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                }
                
            }
            filesParent.indices.forEach {
                if filesParent[$0] != R.image.img_placeHolder_upload()!{
                    multipartFormData.append(filesParent[$0].jpegData(compressionQuality:1)!, withName: "filesParent[]", fileName: "swift_file\($0).jpeg", mimeType: "image/jpeg")
                }
               
            }
            
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, method: .post, headers: headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    print("uploading \(progress)")
                })
                upload.responseJSON { response in
                    //print response.result
                    guard let json = response.result.value as? [String:Any] else {return}
                    print(json)
                
                    
                    do {
                        let json = try JSON.init(data: response.data!)
                        if json["status"].intValue == 200{
                            
                            let decoder = JSONDecoder()
                            let data = try decoder.decode(OrderModel.self , from:json["data"].rawData())
                            callbackSuccess( data)
                        }else{
                            callbackFailure(json["message"].stringValue)
                        }
                    } catch {
                        callbackFailure("Có lỗi xảy ra")
                    }
                    
                 
                    
                }
            case .failure( _): break
                print ("ádasdasd")
            }
        }
    }
    
    // MARK: Mini Game
    
    static func inviteCode (code: String, callbackSuccess: @escaping (_ user: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        var p: Parameters = [
            "code": code
        ]
        if let id = UserInfo.shareUserInfo.game.id {
            p["game_id"] = id
        }

        self.requestJson(target: .inviteCode(param: p), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200 {
                    callbackSuccess(json["status"].intValue)
                } else {
                    callbackFailure(json["message"].stringValue)
                }
                
            } catch {
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func listGift (game_id: String, callbackSuccess: @escaping (_ user: [GiftModel]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .listGift(param: ["game_id": game_id]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([GiftModel].self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    
    static func myListReceived (game_id: Int,page: Int, callbackSuccess: @escaping (_ user: [MyGiftModel],_ total: Int, _ nextPage: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .myListReceived(param: ["game_id": game_id, "page": page]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([MyGiftModel].self , from:json["data"]["datas"].rawData())
                    callbackSuccess( data, json["data"]["total"].intValue, json["data"]["next_page"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func listReceived (game_id: Int, callbackSuccess: @escaping (_ user: [MyGiftModel]) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .listReceived(param: ["game_id": game_id]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([MyGiftModel].self , from:json["data"]["datas"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func ringGift (game_id: Int, callbackSuccess: @escaping (_ user: RingGiftModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .ringGift (param: ["game_id": game_id]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(RingGiftModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func listInvite (page: Int, callbackSuccess: @escaping (_ user: [GameUser],_ nextPage: Int,_ totalPage: Int) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .listInvite(param: ["page": page]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([GameUser].self , from:json["data"]["datas"].rawData())
                    callbackSuccess( data, json["data"]["next_page"].intValue, json["data"]["total"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func listOrders (page: Int,
                            status: Int,
                            callbackSuccess: @escaping (_ user: [ManageOrderModel],_ nextPage: Int,_ totalPage: Int) -> Void,
                            failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "page": page,
            "status": status
        ]
        self.requestJson(target: .listOrder(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([ManageOrderModel].self , from:json["data"]["datas"].rawData())
                    callbackSuccess( data, json["data"]["next_page"].intValue, json["data"]["total"].intValue)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func refundOrder(orderId: Int,
                            cancelReason: String,
                            callbackSuccess: @escaping (_ user: ()) -> Void,
                            failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "order_id": orderId,
            "cancel_reason": cancelReason
        ]
        self.requestJson(target: .refundOrder(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    callbackSuccess(())
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    
    static func rankingList (callbackSuccess: @escaping (_ user: RankingModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .rankingList(param: [:]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(RankingModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func rankingListByTime (callbackSuccess: @escaping (_ user: RankingByTimeModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .rankingListByTime(param: [:]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(RankingByTimeModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func rankingDetail (timeId: Int, userId: Int, callbackSuccess: @escaping (_ user: AchievementsModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "time_id": timeId,
            "user_id": userId
        ]
        self.requestJson(target: .rankingDetail(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(AchievementsModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func listHistory(callbackSuccess: @escaping (_ user: RankingByTimeModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .listHistory(param: [:]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(RankingByTimeModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func rankingListPoint (callbackSuccess: @escaping (_ user: GuidePointData) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .rankingListPoint(param: [:]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(GuidePointData.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func policy (callbackSuccess: @escaping (_ user: PolicyRankingModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {

        self.requestJson(target: .policy(param: [:]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(PolicyRankingModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func createGroup (param:  Parameters, callbackSuccess: @escaping (_ user: NewGroupModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        self.requestJson(target: .createGroup(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(NewGroupModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func listGroup (param:  Parameters, callbackSuccess: @escaping (_ user: NewGroupsModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        self.requestJson(target: .listGroup(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(NewGroupsModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func listMesaage (nextPage: Int, groupId: Int , callbackSuccess: @escaping (_ user: ChatGroupsModel) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "page": nextPage,
            "group_id": groupId
        ]
        self.requestJson(target: .listMessage(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(ChatGroupsModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func sendMessage(message: String, replyID: Int, messageType: CellTypeChatNew, groupId: Int, media: String = "", callbackSuccess: @escaping (_ user: ChatGroup) -> Void, failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "group_id": groupId,
            "message_type": messageType.rawValue,
            "reply_id": replyID,
            "message": message,
            "media": media
        ]
        self.requestJson(target: .sendMessage(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200 {
                    
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(ChatGroup.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func leaveGroup(usersID: String,
                           groupId: Int,
                           callbackSuccess: @escaping (_ user: ()) -> Void,
                           failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "user_ids": usersID,
            "group_id": groupId
        ]
        self.requestJson(target: .leaveGroup(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(())
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func chatMembers(nextPage: Int,
                           groupId: Int,
                           callbackSuccess: @escaping (_ user: ChatMembersModel) -> Void,
                           failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "nextPage": nextPage,
            "group_id": groupId
        ]
        self.requestJson(target: .chatMembers(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(ChatMembersModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func readMessage(groupId: Int,
                            callbackSuccess: @escaping () -> Void,
                           failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "group_id": groupId
        ]
        self.requestJson(target: .readMessage(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess()
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func addMembers(groupId: Int,
                           userId: String,
                           callbackSuccess: @escaping () -> Void,
                           failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "user_ids": userId,
            "group_id": groupId,
        ]
        self.requestJson(target: .addMember(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess()
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func likeMessage(groupId: Int,
                            messageId: Int,
                            isLike: Int,
                           callbackSuccess: @escaping (LikeMessageModel) -> Void,
                           failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "message_id": messageId,
            "group_id": groupId,
            "is_liked": isLike
        ]
        self.requestJson(target: .likeMessage(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(LikeMessageModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func deleteMessage(messageId: Int,
                           callbackSuccess: @escaping () -> Void,
                           failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "message_id": messageId
        ]
        self.requestJson(target: .deleteMessage(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess()
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func searchGroup(page: Int,
                            searchText: String,
                           callbackSuccess: @escaping (SearchGroupModel) -> Void,
                           failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "key": searchText,
            "page": page
        ]
        self.requestJson(target: .searchGroup(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(SearchGroupModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func splashScreen(callbackSuccess: @escaping (SplashScreensModel) -> Void,
                           failed callbackFailure: @escaping (_ message: String) -> Void) {
        self.requestJson(target: .splashScreen(param: [:]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(SplashScreensModel.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func settingNotificationV2(groupId: Int, status: Int, callbackSuccess: @escaping (()) -> Void,
                           failed callbackFailure: @escaping (_ message: String) -> Void) {
        let param: Parameters = [
            "group_id": groupId,
            "status": status
        ]
        self.requestJson(target: .notification(param: param), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess(())
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
            
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getLastBMI (baby_id: Int,
                            callbackSuccess: @escaping (_ user: BMIModel) -> Void,
                            failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "baby_id" : baby_id
        ]
        
        self.requestJson(target: .lastBMI(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let baby = BMIModel.init(json: json["data"])
                    callbackSuccess(baby)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getPreDate(date: Double,
                            callbackSuccess: @escaping (_ user: PreDatePregnant) -> Void,
                            failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "date_preg": date
        ]
        
        self.requestJson(target: .pregDate(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(PreDatePregnant.self , from:json["data"].rawData())
                    callbackSuccess( data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    
    static func addUltraSound(babyId: Int,
                              imageBase64: String,
                              week: Int,
                              day: Int,
                              datePreg: Double,
                              length: String,
                              weight: String,
                              callbackSuccess: @escaping () -> Void,
                              failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "baby_id": babyId,
            "image": imageBase64,
            "week": week,
            "day_ultra": datePreg,
            "day_number": day,
            "length": length,
            "weight": weight
        ]
        
        self.requestJson(target: .addImageUltraSound(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess()
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func updateUltraSound(babyId: Int,
                                 imageBase64: String,
                                 week: Int,
                                 day: Int,
                                 datePreg: Double,
                                 length: String,
                                 weight: String,
                                 imageSound: Int,
                                 callbackSuccess: @escaping () -> Void,
                                 failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "baby_id": babyId,
            "image": imageBase64,
            "week": week,
            "day_ultra": datePreg,
            "day_number": day,
            "length": length,
            "weight": weight,
            "ultrasound_id": imageSound
        ]
        
        self.requestJson(target: .updateImageUltraSound(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    callbackSuccess()
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func listImageSound(babyId: Int,
                              callbackSuccess: @escaping ([UltraSoundModel]) -> Void,
                              failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "baby_id": babyId
        ]
        
        self.requestJson(target: .listImagesSound(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([UltraSoundModel].self , from:json["data"]["list"].rawData())
                    callbackSuccess(data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func lastUltraSound(babyId: Int,
                              callbackSuccess: @escaping (UltraSoundModel) -> Void,
                              failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "baby_id": babyId
        ]
        
        self.requestJson(target: .lastUltraSound(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(UltraSoundModel.self , from:json["data"].rawData())
                    callbackSuccess(data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func listPregnantDetail(babyId: Int,
                                   callbackSuccess: @escaping (PregnantDetailModels) -> Void,
                                   failed callbackFailure: @escaping (_ message: String) -> Void) {
        
        let parameters: Parameters = [
            "baby_id": babyId
        ]
        
        self.requestJson(target: .listPregnantDetail(param: parameters), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    let decoder = JSONDecoder()
                    let data = try decoder.decode(PregnantDetailModels.self , from:json["data"].rawData())
                    callbackSuccess(data)
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
    static func getListPregnant(callbackSuccess: @escaping (_ user: PregnantVacciNewModel?) -> Void,
                                failed callbackFailure: @escaping (_ message: String) -> Void) {
        self.requestJson(target: .listPregnant(param: [:]), success: { (response) in
            do {
                let json = try JSON.init(data: response.data)
                if json["status"].intValue == 200{
                    do {
                        let mode = try json["data"].rawData().toModel(offType: PregnantVacciNewModel.self)
                        callbackSuccess(mode)
                    } catch {
                        
                    }
                }else{
                    callbackFailure(json["message"].stringValue)
                }
            } catch {
                callbackFailure("Có lỗi xảy ra")
            }
        }, error: { (error) in
            callbackFailure(error.localizedDescription)
        }) { (error) in
            callbackFailure(error.localizedDescription)
        }
    }
    
}
