//
//  LoginSocialManager.swift
//  Beberia
//
//  Created by IMAC on 9/7/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import SwiftKeychainWrapper
import AuthenticationServices
import ZaloSDK

class LoginSocialManager: NSObject {
    
    static let share = LoginSocialManager.init()
    
    var loginSocialSuccess:(_ mail: String, _ idSocial: String, _ name: String?)->() = {_,_,_  in}
    
    func loginZalo(){
        guard let viewControllerTop = Utils.getTopMostViewController() else  { return }
        ZaloSDK.sharedInstance()?.authenticateZalo(with: ZAZaloSDKAuthenTypeViaZaloAppOnly, parentController: viewControllerTop, handler: { response in

            if let response = response {
                if response.isSucess {
                    self.loginSocialSuccess(response.displayName, response.userId, nil)
                }
            }
        })
    }
    
    func signOut() {
        ZaloSDK.sharedInstance()?.unauthenticate()
    }
    
    func checkIsAuthenticated() -> Bool {
//        ZaloSDK.sharedInstance()!.isAuthenticatedZalo(completionHandler: { response in
//            if let response = response {
//                if response.isSucess {
//                    return true
//                }else {
//                    return false
//                }
//            } else {
//                return false
//            }
//        })
        
        return true
    }
    
    func loginApple(){
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
            guard let viewControllerTop = Utils.getTopMostViewController() else  { return }
            Utils.showAlertView(controller: viewControllerTop, title: R.string.localizable.commonNoti(), message: "Bạn cần iOS 13 để sử dụng tính năng này!")
        }
    }
    
    func loginGoogle(){
        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.presentingViewController = Utils.getTopMostViewController()!
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func loginFaceBook(){
        let fbLoginManager : LoginManager = LoginManager()
        
        fbLoginManager.logOut()
        fbLoginManager.loginBehavior = .browser
        let cookies = HTTPCookieStorage.shared
        let facebookCookies = cookies.cookies(for: URL(string: "https://facebook.com/")!)
        for cookie in facebookCookies! {
            cookies.deleteCookie(cookie )
        }
        
        fbLoginManager.logIn(permissions: ["email"], from: Utils.getTopMostViewController()!) { [weak self] (result, error) -> Void in
            guard let self = self else { return }
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
//                if(fbloginresult.grantedPermissions.contains("email"))
//                {

                    self.getFBUserData()
                    
             //   }
            }
        }
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { [weak self] (connection, result, error) -> Void in
                
                if (error == nil){
                    print("Result111:\(String(describing: result)) "as Any)
                }
                let dict = result as! NSDictionary
                
                self?.loginSocialSuccess(dict["email"] as? String ?? "", AccessToken.current?.userID ?? "0", nil)
                
            })
        }
        
    }
}

//MARK: APPLe ID Delegate
extension LoginSocialManager: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding{
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
        let fullName = appleIDCredential.fullName
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Create an account in your system.
            
            print(authorization.provider)
            let giveName = fullName?.givenName
            let familyName = fullName?.familyName
            let middleName = fullName?.middleName
            let name = "\(familyName ?? "")\(middleName ?? "")\(giveName ?? "")"
            self.loginSocialSuccess(appleIDCredential.email ?? "", appleIDCredential.user, name)
            
            //Navigate to other view controller
        } else if authorization.credential is ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            
        }
    }
    }
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return Utils.getTopMostViewController()!.view.window!
    }
    
}

//MARK: Google Delegate
extension LoginSocialManager: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if (error == nil) {
            // Perform any operations on signed in user here.
            
            self.loginSocialSuccess(user.profile.email, user.userID, nil)
            GIDSignIn.sharedInstance()?.signOut()
        } else {
            print("ERROR ::\(error.localizedDescription)")
        }
    }
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
        guard error == nil else {
            print("Error while trying to redirect : \(String(describing: error))")
            return
        }
        
        print("Successful Redirection")
    }
}

