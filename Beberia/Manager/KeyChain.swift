//
//  KeychainManage.swift
//  Beberia
//
//  Created by haiphan on 12/03/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import Security
import UIKit
import SwiftKeychainWrapper

class KeyChain {
    static var shared = KeyChain()
    
    func getKeyChain() -> String? {
        var uuIdStr: String?
        if let valueInfoDevice: String = KeychainWrapper.standard.string(forKey: ConstantApp.shared.keyUUIDBeberia) {
            uuIdStr = valueInfoDevice
        } else if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            uuIdStr = uuid
            KeychainWrapper.standard.set(uuid, forKey: ConstantApp.shared.keyUUIDBeberia)
        }
        return uuIdStr
    }
}
