//
//  ConstantApp.swift
//  Beberia
//
//  Created by haiphan on 15/12/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import UIKit

class ConstantApp {
    
    enum FolderName: String, CaseIterable {
        case folderDowloadImage
    }
    
    enum Environment {
        case dev, live
        
        var text: String {
            switch self {
            case .dev: return "http://apidev.beberia.me/"
            case .live: return "https://api.beberia.me/"
            }
        }
    }
    static var shared = ConstantApp()
    let environment: Environment = .live
    
    let bannerType: Int = 11
    let textVersion: String = "iOS App "
    let distanceToLoad: Int = 5
    let linkPreferPregnantGeneral: String = "www.vinmec.com"
    let maxMess: Int = 20
    let linkGame = "https://beberia.page.link/game"
    let linkForum = "https://www.facebook.com/beberia.mebe"
    let linkShareFB = "https://beberia.page.link/app"
    let linkMomHelp = "https://www.facebook.com/beberia.mebe"
    let linkRate = "https://www.facebook.com/groups/babyfacevn"
    let linkFeedback = "https://www.facebook.com/beberia.mebe"
    let linkPregnant = "https://www.vinmec.com/vi/tin-tuc/thong-tin-suc-khoe/san-phu-khoa-va-ho-tro-sinh-san/loi-ich-khi-thai-giao-trong-3-thang-giua-thai-ky/?link_type=related_posts"
    let imagePlaceHodler = "placeholder1"
    let linkPreferPregnant: String = "https://hongngochospital.vn/cach-du-tinh-ngay-sinh/"
    let contentFBShare = """
    "𝑴𝒆̣ 𝒕𝒖̛𝒐̛𝒏𝒈 𝒕𝒂́𝒄 𝒏𝒈𝒂𝒚 - 𝑵𝒉𝒂̣̂𝒏 𝒒𝒖𝒂̀ 𝒍𝒊𝒆̂̀𝒏 𝒕𝒂𝒚"
    👉Tải ngay ứng dụng Beberia để nhận nhiều thông tin hữu ích mẹ nhé !
    """
    let onGroupCreateEvent: String = "onGroupCreate"
    let onMessageReceivedEvent: String = "onMessageReceived"
    let onMessageUpdateEvent: String = "onMessageUpdate"
    let onChannelChangedEvent: String = "onChannelChanged"
    let onMessageDeleteEvent: String = "onMessageDelete"
    let onMessageLikeEvent: String = "onMessageLike"
    let keyUUIDBeberia: String = "keyUUIDBeberia"
    let timeSplashScreen: Double = 3
    
    let contentFeelPregnant = """
Thực hành thai giáo cảm xúc bằng những hoạt động
Thực hành thai giáo cảm xúc bằng Âm nhạc
Mẹ bầu duy trì việc nghe các bản nhạc nhẹ nhàng, êm ái cho để tinh thần được thư giãn, thoải mái nhất.
Trong đó, nhạc thai giáo 3 tháng đầu đóng vai trò quan trọng giúp mẹ bầu giảm các triệu chứng buồn nôn, mệt mỏi của những cơn ốm nghén gây ra.

Đồng thời, nhạc thai giáo 3 tháng giữa cũng vô cùng quan trọng, giúp duy trì cảm xúc tích cực, vui vẻ, thư giãn cho mẹ bầu. Ngoài ra còn giúp con yêu phát triển các giác quan và não bộ một cách tốt nhất.

Thực hành thai giáo cảm xúc bằng cách trò chuyện cùng con
Ba mẹ trò chuyện, thủ thỉ với con, chia sẻ với con về những niềm vui trong cuộc sống, kể cho con nghe về những viễn cảnh tốt đẹp của gia đình mình trong tương lai,
 giúp tăng sợi dây kết nối với con và giúp tình cảm gia đình thêm gắn kết.

Con yêu sẽ cảm nhận được tình yêu thương của ba mẹ. Từ đó, con sẽ cảm thấy an toàn và phát triển tốt nhất trong bụng mẹ.

Thực hành thai giáo cảm xúc bằng cách Thai giáo mỹ thuật giúp mẹ thỏa trí sáng tạo và tưởng tượng.
Mẹ bầu cùng ba thực hành thai giáo mỹ thuật như ngắm nhìn những bức tranh đẹp, vẽ nên những bức hình ngộ nghĩnh, đáng yêu và tô những màu sắc tươi sáng.

Việc này không chỉ giúp mẹ bầu cảm thấy thư giãn mà còn phát triển khả năng sáng tạo. Con yêu sinh ra cũng sớm phát triển năng khiếu hội họa và có cái nhìn đẹp đẽ về cuộc sống hơn.

Thực hành thai giáo cảm xúc bằng cách Thai giáo vận động giúp cơ thể khỏe mạnh.
Mẹ bầu có thể tham khảo và thực hành các phương pháp thai giáo vận động như bơi lội, đi bộ, tập yoga hay tập luyện những động tác dành riêng cho bà bầu.

Vận động không chỉ giúp mẹ được khỏe mạnh mà còn giúp cơ thể tiết ra hormone hạnh phúc edorphins giúp cơ thể thoải mái, dễ chịu…

Mẹ bầu có thể rủ chồng cùng đi dạo với mình vào buổi sáng hay chiều tối ở những nơi thoáng mát, yên tĩnh để hít thở không khí trong lành, đồng thời tình cảm gia đình thêm gắn kết.

Người chồng chính là nguồn cảm xúc tích cực tốt nhất với mẹ bầu. Vậy nên hãy dành nhiều thời gian quan tâm vợ nhé.

Thực hành thai giáo cảm xúc bằng cách Viết nhật ký giúp mẹ cân bằng cảm xúc, lưu lại những kỷ niệm.
Mỗi khi gặp chuyện vui hay buồn mẹ bầu đều có thể viết nhật ký để lưu lại những kỷ niệm tươi đẹp, giúp mẹ trân trọng những khoảnh khắc đáng nhớ đó.
Viết nhật ký khi buồn giúp mẹ có thể bình tĩnh trở lại và cân bằng, điều chỉnh cảm xúc của mình một cách dễ dàng hơn.
"""
    
    func linkSocketChat() -> String {
        switch self.environment {
        case .dev: return "http://chatdev.beberia.me/"
        case .live: return "http://chat.beberia.me/"
        }
    }
    
    func getHeightSafeArea(type: GetHeightSafeArea.SafeAreaType) -> CGFloat {
        return GetHeightSafeArea.shared.getHeight(type: type)
    }
    
    func contentShare(text: String) -> String {
       return  "Bạn ơi, Mình muốn gửi bạn\n\nMÃ GIỚI THIỆU: \(text)\n\nBạn nhớ tải ứng dụng và nhập mã giới thiệu của mình để cùng nhận được điểm tương tác nhận quà hữu ích cho mẹ và bé từ ứng dụng Miễn phí Beberia nhé !\nLink tải ứng dụng : https://beberia.page.link/app"
    }
    
    func contentShareGame(text: String) -> String {
       return  "Bạn ơi, mình muốn tặng bạn 1 ticket lắc quà may mắn của Beberia.\nMÃ GIỚI THIỆU của bạn là \(text).\nBạn hãy tải ứng dụng và nhập mã giới thiệu này để có cơ hội nhận những phần quà hữu ích cho bé yêu nhé!\nnhttps://beberia.page.link/game"
    }
    
    func getStatusBarHeight() -> CGFloat {
        var statusBarHeight: CGFloat = 0
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            statusBarHeight = window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        } else {
            statusBarHeight = UIApplication.shared.statusBarFrame.height
        }
        return statusBarHeight
    }
}

enum StatusOrderGlobal: Int {
    case notYetPay = 0
    case preparing = 2
    case completed = 3
    case sent = 4
    case cancel = 5
    case all = 6
    
    var text: String {
        switch self {
        case .all: return ""
        case .notYetPay: return "Chờ thanh toán"
        case .preparing: return "Đang phân tích"
        case .completed: return "Xem kết quả"
        case .sent: return "Đã gửi"
        case .cancel: return "Đã huỷ"
        }
    }
}
extension ConstantApp {
    enum UIStoryBoardName: String {
        case pregnantUltraSound = "UltraSoundPregnancy"
    }
}
