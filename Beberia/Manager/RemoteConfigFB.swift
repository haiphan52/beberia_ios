//
//  RemoteConfigFB.swift
//  Beberia
//
//  Created by IMAC on 9/28/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import FirebaseRemoteConfig
import UIKit

class RemoteConfigFB: NSObject {
    
    static let share = RemoteConfigFB()
    
    fileprivate var remoteConfig = RemoteConfig.remoteConfig()
    var getValueUpdateApp: (Bool)->() = {_ in}
    
        func fetchValueUpdateApp(){
            let defaults: [String: NSObject] = ["require_update_app" : false as NSObject]
    
            remoteConfig.setDefaults(defaults)
    
            let settings = RemoteConfigSettings()
            settings.minimumFetchInterval = 0
            remoteConfig.configSettings = settings
    
            self.remoteConfig.fetch(withExpirationDuration: 0) { (status, errror) in
                if status == .success, errror == nil {
//                    self.remoteConfig.activate { (error) in
//                        guard error == nil else {
//                            print(error as Any)
//                            return
//
//                        }
                    self.remoteConfig.activate(completion: nil)
//                    self.remoteConfig.activate(completionHandler: nil)
                        let value = self.remoteConfig.configValue(forKey: "require_update_app").boolValue
                        self.getValueUpdateApp(value)
                 //   }
                }else {
                    print("lỗi get remote config")
                    self.getValueUpdateApp(false)
                }
            }
        }
}
