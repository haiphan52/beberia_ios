//
//  AppSettings.swift
//  GooDic
//
//  Created by ttvu on 6/2/20.
//  Copyright © 2020 paxcreation. All rights reserved.
//

import Foundation

enum AppSettings {
    @Storage(key: "isSelectBaby", defaultValue: false)
    static var isSelectBaby: Bool
    
    @Storage(key: "loginType", defaultValue: LoginSocialViewController.LoginType.gooogle)
    static var loginType: LoginSocialViewController.LoginType
    
}
