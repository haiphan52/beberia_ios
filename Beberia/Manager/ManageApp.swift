//
//  ManageApp.swift
//  Beberia
//
//  Created by haiphan on 14/12/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxRelay

final class ManageApp {
    
    struct Constant {
        static let calculateDayBorn: Double = 280
        static let limitPregnant: Double = 294
        static let limitWeekPregnant: Int = 42
    }
    
    enum PregnantStatus {
        case pregnanting(Double), bornLate(Double), wasBorn
    }
    
    enum FileDowload: String, CaseIterable {
        case jpeg
    }
 
    static var shared = ManageApp()
    let hideImageEvent: PublishSubject<Void> = PublishSubject.init()
    let typeBanner: BehaviorRelay<BannerView.BannerTypeRedirect?> = BehaviorRelay.init(value: nil)
    var hasInstall: Bool = true
    
    private init() {}
    
    func start() {
        ConstantApp.FolderName.allCases.forEach { folder in
            self.createFolder(path: folder.rawValue, success: nil, failure: nil)
        }
    }
    
    func detectLoadPaging(indexPaths: [IndexPath], value: Int, totalItem: Int) -> Bool {
        guard let max = indexPaths.max()?.row,
              value < totalItem,
              value - max <= ConstantApp.shared.distanceToLoad else {
                  return false
              }
        return true
    }
    
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    func saveImageToPhoto(inputImage: UIImage) {
        UIImageWriteToSavedPhotosAlbum(inputImage, nil, nil, nil)
    }
    
    //MARK: Save to Camera Roll
    func saveToCameraRoll(savePathUrl: URL) {
        //let assetsLib = ALAssetsLibrary()
        //assetsLib.writeVideoAtPathToSavedPhotosAlbum(savePathUrl, completionBlock: nil)
        UISaveVideoAtPathToSavedPhotosAlbum(savePathUrl.path,nil, nil, nil)
    }
    
    public func createURL(folder: String, name: String, type: FileDowload) -> URL {
        let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let outputURL = documentURL.appendingPathComponent("\(folder)/\(name)").appendingPathExtension(type.rawValue)
        return outputURL
    }
    
    func createFolder(path: String, success: ((URL) -> Void)?, failure: ((String) -> Void)?) {
        // path to documents directory
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        if let documentDirectoryPath = documentDirectoryPath {
            // create the custom folder path
            let imagesDirectoryPath = documentDirectoryPath.appending("/\(path)")
            let fileManager = FileManager.default
            if !fileManager.fileExists(atPath: imagesDirectoryPath) {
                do {
                    try fileManager.createDirectory(atPath: imagesDirectoryPath,
                                                    withIntermediateDirectories: false,
                                                    attributes: nil)
                    let appURL = URL(fileURLWithPath: imagesDirectoryPath)
                    success?(appURL)
                } catch {
                    print("Error creating images folder in documents dir: \(error.localizedDescription)")
                    failure?(error.localizedDescription)
                }
            } else {
                failure?("Folder is exist")
            }
        }
    }
    
    func changeStatusBar(statusBarStyle: UIStatusBarStyle) {
        UIApplication.shared.statusBarStyle = statusBarStyle
    }
    
    func toNumberWeek(dayExpectedBorn: Double) -> PregnantInfoBabyVC.DayBornUpdate {
        let current = Date().timeIntervalSince1970
        let restTime = dayExpectedBorn - current
        
        if restTime < 0 {
            return .invalid
        }
        
        let wasPregnant = (Constant.limitPregnant * Double().day) - restTime
        if  Int(wasPregnant) / Int(Double().week) < 0 {
            let day = current + (Double().day * Constant.limitPregnant)
            let strDay = day.toDate().toString(format: .ddMMyyyy)
            return .greaterThan42Week(strDay)
        }
        
        return .pregnanting
    }
    
    func calcaulateDayBorn(dayExpectedBorn: Double, ageRange: Int) -> PregnantStatus {
        if ageRange >= 1 {
            return .wasBorn
        }
        let current = Date().timeIntervalSince1970
        let restTime = dayExpectedBorn - current
        if restTime > 0 {
            let wasPregnant = (Constant.calculateDayBorn * Double().day) - restTime
            return .pregnanting(wasPregnant)
        }
        return .bornLate(0)
    }
    
    func sizeOfImageAt(url: URL) -> CGSize? {
        // with CGImageSource we avoid loading the whole image into memory
        guard let source = CGImageSourceCreateWithURL(url as CFURL, nil) else {
            return nil
        }
        
        let propertiesOptions = [kCGImageSourceShouldCache: false] as CFDictionary
        guard let properties = CGImageSourceCopyPropertiesAtIndex(source, 0, propertiesOptions) as? [CFString: Any] else {
            return nil
        }
        
        if let width = properties[kCGImagePropertyPixelWidth] as? CGFloat,
           let height = properties[kCGImagePropertyPixelHeight] as? CGFloat {
            return CGSize(width: width, height: height)
        } else {
            return nil
        }
    }
    
    func encodeString(usersIds: [String]) -> String {
        let jsonEncode = Utils.json(from: usersIds )
        return jsonEncode ?? ""
    }
    
    func shareCode(viewcontroller: UIViewController) {
        let objectsToShare: [String] = ["\(ConstantApp.shared.contentShare(text: UserInfo.shareUserInfo.name))"]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.excludedActivityTypes = [.airDrop, .addToReadingList, .assignToContact,
                                            .mail, .message, .postToFacebook, .postToWhatsApp]
        activityVC.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            activityVC.dismiss(animated: true) {
            }
        }
        viewcontroller.present(activityVC, animated: true, completion: nil)
    }
    
    func openLink(link: String) {
        guard let url = URL(string: link) else { return }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}
