//
//  Notification+AppDelegate.swift
//  Beberia
//
//  Created by IMAC on 10/14/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import SwiftyJSON

extension AppDelegate {
    func handelClickNotification(noti: NotificationList?, notification: Noti, viewVC : BaseViewController){
        switch noti {
        // done
        case .NewMessage:
            if let obj = notification.group {
                let item: NewGroupModel = NewGroupModel(groupID: obj.groupID,
                                                        groupName: obj.groupName,
                                                        numberMember: obj.numberMember,
                                                        coverURL: obj.coverURL,
                                                        members: [], data: [], lastMessage: nil,
                                                        numberUnread: obj.numberUnread,
                                                        notificationOff: obj.notificationOff)
                let chatVC = ListMessageViewController.init(nib: R.nib.listMessageViewController)
                chatVC.groupdChat = item
                chatVC.hidesBottomBarWhenPushed = true
                Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
            }
        case .LikeFeed?:
            let id = notification.feed?.id ?? 0
            viewVC.getDetailFeed(array: [HomeFeed](), idFeed: id, 0, editFromView: .Notification) { (homefeed) in
            }
            
        case .LikeComment?, .ComFeed?, .LikeComtDiary?, .ComDiary?, .LikeComInfo?:
            // done
            //            let commentVC = CommentViewController.init(nib: R.nib.commentViewController)
            //            commentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            //            commentVC.isCheckFromNotification = true
            //            if noti == .LikeComment || noti == .ComFeed{
            //                commentVC.fromToView = .Home
            //            }else{
            //                commentVC.fromToView = .Diary
            //            }
            //
            //            commentVC.commentNotification = notification.comment
            //            commentVC.idPost = notification.feed?.id ?? 0
            //            viewVC.navigationController?.pushViewController(commentVC, animated: true)
            
            if noti == .LikeComment || noti == .ComFeed{
                viewVC.getDetailFeed(array: [HomeFeed](), idFeed: notification.feed?.id ?? 0, 0, editFromView: .Notification, commentCommentNotification: notification.comment) { (_) in
                    print("123")
                }
            }
            else if noti == .LikeComInfo || noti == .ComInfo{
                viewVC.pushPostDetail(array: [PostDetailModel](), idPost: notification.feed?.id ?? 0, 0, commentCommentNotification: notification.comment,completion: { (_) in
                    print("123")
                })
            }
            else{
                viewVC.getDetailDiary(array: [Diaries](), idDiary: notification.feed?.id ?? 0, 0, editFromView: .Notification, commentCommentNotification: notification.comment, tableView: UITableView.init()) { (_) in
                    print("123")
                }
            }
            
        case .LikeRepCom?, .LikeRepComDiary?, .LikeRepComInfo?:
            // done
            let notiCommentVC = NotificationCommentViewController.init(nib: R.nib.notificationCommentViewController)
            //   if (notification.comment?.reply ?? [Comment]()).count > 0{
            let comment = notification.replyComment
            notiCommentVC.commentNotification = comment
            //   }
            notiCommentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            notiCommentVC.fromToView = noti == .LikeRepCom ? .Home : .Diary
            notiCommentVC.userFrom = notification.userFrom ?? User.init(json: "")
            notiCommentVC.commentRoot = notification.comment
            notiCommentVC.typeNotification = noti!
            notiCommentVC.idCommentParent = notification.comment?.commentId ?? 0
            viewVC.navigationController?.pushViewController(notiCommentVC, animated: true)
            
        case .RepComt?, .RepComDiary?, .RepcomInfo?:
            
            let notiCommentVC = NotificationCommentViewController.init(nib: R.nib.notificationCommentViewController)
            //      if (notification.comment?.reply ?? [Comment]()).count > 0{
            let comment = notification.replyComment
            notiCommentVC.commentNotification = comment
            //     }
            
            notiCommentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            notiCommentVC.fromToView = noti == .RepComt ? .Home : .Diary
            notiCommentVC.idNotification = notification.id ?? 0
            notiCommentVC.userFrom = notification.userFrom ?? User.init(json: "")
            notiCommentVC.commentRoot = notification.comment
            notiCommentVC.idCommentParent = notification.comment?.commentId ?? 0
            notiCommentVC.typeNotification = noti!
            viewVC.navigationController?.pushViewController(notiCommentVC, animated: true)
            
        case .LikeDiary?:
            let id = notification.feed?.id ?? 0
            viewVC.getDetailDiary(array: [Diaries](), idDiary: id, 0, editFromView: .Notification, tableView: UITableView.init()) { (_) in
                print("123")
            }
            
        case .RemindWrite:
            print("Noti")
            if let tabBarController = viewVC.tabBarController{
                tabBarController.selectedIndex = 1
                
                // print(Utils.getTopMostViewController())
                if let view = Utils.getTopMostViewController() as? DiaryViewController{
                    let writeHome = WriteHomeViewController.init(nib: R.nib.writeHomeViewController)
                    writeHome.hidesBottomBarWhenPushed = true
                    writeHome.fromView = .Diary
                    writeHome.checkComplete = { id in
                        view.getCheckCompleteDiary(id: id ,isEdit: false, completion: { diary in
//                            guard let diaryNews = view.getObjectDiaryNews() else { return }
//                            diaryNews.diaries?.insert(diary, at: 0)
//                            view.tbvDiary.reloadData()
                            
                            var listDiary = view.listDiaryNew.value
                            listDiary.insert(diary, at: 0)
                            view.listDiaryNew.accept(listDiary)
                            
                            NotificationCenter.default.post(name: NSNotification.Name.updateDiaryNew, object: ["DiaryNew": diary], userInfo: nil)
                            
                            view.dayWrite += 1
                            view.setupProgressBar(day: view.dayWrite)
                        })
                    }
                    view.navigationController?.pushViewController(writeHome, animated: true)
                }
            }
            
        case .LikeComSecret, .LikeSecret, .LikeRepComSecret, .ComSecret, .RepcomSecret:
            
            let setPassVC = SetPasswordSecretViewController.init(nib: R.nib.setPasswordSecretViewController)
            setPassVC.typeSetPass = .Permiss
            let naviSetPass = UINavigationController.init(rootViewController: setPassVC)
            naviSetPass.modalPresentationStyle = .overCurrentContext
            viewVC.present(naviSetPass, animated: true, completion: nil)
            
            setPassVC.actionConfirm =  { (isResult) in
                if isResult{
                    viewVC.tabBarController?.selectedIndex = 2
                }
            }
        
        case .SENDREQUESTFRIEND:
            let ProfileVC = R.storyboard.main.profileViewController()
            ProfileVC?.user = notification.userFrom ?? User.init(json: "")
            let naviProfile = UINavigationController.init(rootViewController: ProfileVC!)
            naviProfile.modalPresentationStyle = .fullScreen
           viewVC.present(naviProfile, animated: true, completion: nil)
        
        case .NotificationNomar:
            print("Open Notification Default")
            guard let bannerType = BannerView.BannerTypeRedirect(rawValue: notification.typeRedirect ?? 0) else {
                return
            }
            ManageApp.shared.typeBanner.accept(bannerType)
        default:
            print("Open Notification Default")
            
        }
        
        
        if noti != .NotificationNomar && noti != .NOTIFICATIONGAME{
            APIManager.readNotification(id: notification.id ?? 0, callbackSuccess: { [weak self] (isSuccess) in
                guard let _ = self else { return }
                if isSuccess{
                    print("Read Notication Success")
                }else{
                    print("Read Notication Fail")
                }
            }) { (error) in
                print(error)
            }
        }
        
       
    }
    
    func writeDiary (){
        
    }
    
    //    func setuplikeFeed(likeFeed: Noti, typeNoti: NotificationList, user: [AnyHashable: Any]){
    //        var body = ""
    //        if likeFeed.totalUser ?? 0 > 0 {
    //            body = "\(likeFeed.userFrom?.displayName ?? "") cùng \(likeFeed.totalUser ?? 0) người khác \(typeNoti.title)"
    //        }else {
    //            body = "\(likeFeed.userFrom?.displayName ?? "") \(typeNoti.title)"
    //        }
    //        let content = UNMutableNotificationContent()
    //        //        content.title = "Beberia"
    //        content.body = body
    //        content.userInfo = user
    //        //  content.sound = UNNotificationSound.default
    //        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    //
    //        let request = UNNotificationRequest(identifier: "\(String(describing: likeFeed.feed?.id))", content: content, trigger: trigger)
    //        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    //    }
    //
    //    func setuplikeComment (likeFeed: Noti ,typeNoti: NotificationList, user: [AnyHashable: Any]){
    //        var body = ""
    //
    //        if likeFeed.totalUser ?? 0 > 0 {
    //            body = "\(likeFeed.userFrom?.displayName ?? "") cùng \(likeFeed.totalUser ?? 0) người khác \(typeNoti.title)"
    //
    //        }else {
    //            body = "\(likeFeed.userFrom?.displayName ?? "") \(typeNoti.title)"
    //
    //        }
    //        let content = UNMutableNotificationContent()
    //        //        content.title = "Beberia"
    //        content.body = body
    //        content.userInfo = user
    //        //  content.sound = UNNotificationSound.default
    //        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    //        let request = UNNotificationRequest(identifier: "\(String(describing: likeFeed.comment?.commentId))", content: content, trigger: trigger)
    //        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    //
    //    }
    //
    //    func setuplikeRepCom(likeFeed: Noti, typeNoti: NotificationList, user: [AnyHashable: Any]){
    //        var body = ""
    //
    //        if likeFeed.totalUser ?? 0 > 0 {
    //            body = "\(likeFeed.userFrom?.displayName ?? "") cùng \(likeFeed.totalUser ?? 0) người khác \(typeNoti.title)"
    //
    //        }else {
    //            body = "\(likeFeed.userFrom?.displayName ?? "") \(typeNoti.title)"
    //
    //        }
    //        let content = UNMutableNotificationContent()
    //        //        content.title = "Beberia"
    //        content.body = body
    //        content.userInfo = user
    //        //  content.sound = UNNotificationSound.default
    //        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    //
    //        let request = UNNotificationRequest(identifier: "\(likeFeed.replyComment?.commentId)", content: content, trigger: trigger)
    //        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    //
    //    }
    //
    //    func setupComFeed(likeFeed: Noti, typeNoti: NotificationList, user: [AnyHashable: Any]){
    //        var body = ""
    //        if likeFeed.totalUser ?? 0 > 0 {
    //
    //            if likeFeed.feed?.userCreate?.id == UserInfo.shareUserInfo.id {
    //                body = "\(likeFeed.userFrom?.displayName ?? "") cùng \(likeFeed.totalUser ?? 0) người khác \(typeNoti.title) bạn"
    //
    //            }else {
    //                body = "\(likeFeed.userFrom?.displayName ?? "") cùng \(likeFeed.totalUser ?? 0) người khác \(typeNoti.title) \(likeFeed.feed?.userCreate?.displayName ?? "")"
    //            }
    //        }else {
    //            if likeFeed.feed?.userCreate?.id == UserInfo.shareUserInfo.id {
    //                body = "\(likeFeed.userFrom?.displayName ?? "") \(typeNoti.title) bạn "
    //            }else {
    //                body = "\(likeFeed.userFrom?.displayName ?? "") \(typeNoti.title) \(likeFeed.feed?.userCreate?.displayName ?? "")"
    //            }
    //        }
    //        let content = UNMutableNotificationContent()
    //        //        content.title = "Beberia"
    //        content.body = body
    //        content.userInfo = user
    //        //  content.sound = UNNotificationSound.default
    //        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    //        if likeFeed.type == 1 {
    //            let request = UNNotificationRequest(identifier: "-\(String(describing: likeFeed.feed?.id))", content: content, trigger: trigger)
    //            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    //        }else {
    //            let request = UNNotificationRequest(identifier: "\(String(describing: likeFeed.feed?.id))", content: content, trigger: trigger)
    //            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    //        }
    //
    //    }
    //
    //    func setupRepCom(likeFeed: Noti, typeNoti: NotificationList, user: [AnyHashable: Any]){
    //        var body = ""
    //        if likeFeed.totalUser ?? 0 > 0 {
    //            if likeFeed.comment?.user?.id == UserInfo.shareUserInfo.id {
    //                body = "\(likeFeed.userFrom?.displayName ?? "") cùng \(likeFeed.totalUser ?? 0) người khác \(typeNoti.title) bạn"
    //
    //            }else {
    //                body = "\(likeFeed.userFrom?.displayName ?? "") cùng \(likeFeed.totalUser ?? 0) người khác \(typeNoti.title) \(likeFeed.comment?.user?.displayName ?? "")"
    //            }
    //        }else {
    //
    //            if likeFeed.comment?.user?.id == UserInfo.shareUserInfo.id {
    //                body = "\(likeFeed.userFrom?.displayName ?? "") \(typeNoti.title) bạn"
    //            }else {
    //                body = "\(likeFeed.userFrom?.displayName ?? "") \(typeNoti.title) \(likeFeed.comment?.user?.displayName ?? "")"
    //
    //            }
    //        }
    //        let content = UNMutableNotificationContent()
    //        //        content.title = "Beberia"
    //        content.body = body
    //        content.userInfo = user
    //        //  content.sound = UNNotificationSound.default
    //        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    //        if likeFeed.type == 1 {
    //            let request = UNNotificationRequest(identifier: "-\(likeFeed.comment?.commentId)", content: content, trigger: trigger)
    //            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    //        }else {
    //            let request = UNNotificationRequest(identifier: "\(likeFeed.comment?.commentId)", content: content, trigger: trigger)
    //            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    //        }
    //    }
    
}
