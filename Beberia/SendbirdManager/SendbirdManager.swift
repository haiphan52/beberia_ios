//
//  SendbirdManager.swift
//  Beberia
//
//  Created by IMAC on 6/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

//class SendBirdManager {
//    
//    static let share = SendBirdManager()
//    
//    var joinChanelSuccess: (_ message: SBDGroupChannel)->() = {_ in}
//    var createChaneSuccess: (_ message: SBDGroupChannel)->() = {_ in}
//    
// //   var listMessages = BehaviorRelay<[SBDBaseMessage]>.init(value: [])
//    var sendMessageSuccess = PublishSubject<Void>()
//    var pushNotificationMessage = PublishSubject<Bool>()
//    var sBDGroupChannelListQuery: SBDGroupChannelListQuery?
//    var sBDPreviousMessageListQuery : SBDPreviousMessageListQuery?
//    
//    func createChannel(nameGroup: String, userIds: [Int], dataImage: Data){
//            guard let userId = UserInfo.shareUserInfo.id, userId != 0 else {
//                return
//            }
//                        // Create a channel
//                        let params = SBDGroupChannelParams()
//                        params.isPublic = true
//                        params.isSuper = true
//                     //   params.isDistinct = true
//                        params.name = nameGroup
//        print(userIds.map { String($0) })
//        
//                        params.addUserIds(userIds.map { String($0) })
//                        params.coverImage = dataImage
//                        SBDGroupChannel.createChannel(with: params) { (groupChannel, error) in
//                            guard error == nil else {   // Error.
//                                print(error.debugDescription)
//                                return
//                            }
//
//                            self.createChaneSuccess(groupChannel!)
//    
//                        }
//        }
//    
//    func addMessageMetaArrayValues(groupChannel: SBDGroupChannel, message: SBDBaseMessage, metaArrayValue:[String:[String]], completion:@escaping (SBDBaseMessage)->()){
//
//        groupChannel.addMessageMetaArrayValues(with: message, keyValues: metaArrayValue, completionHandler: { (message, error) in
//            guard error == nil else {   // Error.
//                return
//            }
//
//            completion(message!)
//        })
//    }
//    
//    func removeMessageMetaArrayValues(groupChannel: SBDGroupChannel, message: SBDBaseMessage, metaArrayValue:[String:[String]], completion:@escaping (SBDBaseMessage)->()){
//
//       groupChannel.removeMessageMetaArrayValues(with: message, keyValues: metaArrayValue, completionHandler: { (message, error) in
//            guard error == nil else {   // Error.
//                return
//            }
//        })
//        
//        completion(message)
//    }
//    
//    func leaveChannel(channel: SBDGroupChannel, completion:@escaping ()->()){
//        channel.leave { (error) in
//            guard error == nil else {   // Error.
//                return
//            }
//            completion()
//        }
//    }
//    
//    func connectUserToSendBird(completion: @escaping ()->()){
//        
//        if !Utils.getAppDelegate().isCheckConnectSendbird {
//            
//         //   UserInfo.shareUserInfo.id = 329
//            
//            guard let userId = UserInfo.shareUserInfo.id, userId != 0 else {
//                return
//            }
//            
//            SBDMain.connect(withUserId: "\(userId)", completionHandler: { (user, error) in
//                guard error == nil else {   // Error.
//                    print(error.debugDescription)
//                    return
//                }
//                completion()
//                Utils.getAppDelegate().isCheckConnectSendbird = true
//                
//                self.registerDevicePushToken()
//                
////                SBDMain.updateCurrentUserInfo(withNickname: UserInfo.shareUserInfo.name, profileUrl: UserInfo.shareUserInfo.avartar, completionHandler: { (error) in
////                    guard error == nil else {   // Error.
////                        return
////                    }
////                })
//            })
//        }else{
//            completion()
//            Utils.getAppDelegate().isCheckConnectSendbird = true
//        }
//    }
//    
//    func registerDevicePushToken(){
//        guard let deviceToken = Utils.getAppDelegate().deviceToken else {
//                    return
//                }
//        
//                // Register a device token to Sendbird server.
//                 SBDMain.registerDevicePushToken(deviceToken, unique: true, completionHandler: { (status, error) in
//                     if error == nil {
//                         if status == SBDPushTokenRegistrationStatus.pending {
//                             // A token registration is pending.
//                             // If this status is returned, invoke `+ registerDevicePushToken:unique:completionHandler:` with `[SBDMain getPendingPushToken]` after connection.
//                            print("A token registration is pending.")
//                         }
//                         else {
//                             // A device token is successfully registered.
//                            print("A device token is successfully registered.")
//                         }
//                     }
//                     else {
//                         // Registration failure.
//                        print("Registration failure.")
//                     }
//                 })
//    }
//    
//    func unRegisterDevicePushToken(){
//        guard let _ = Utils.getAppDelegate().deviceToken else {
//            return
//        }
//        
//        // If you want to unregister the all devices of the user, invoke this method.
//        SBDMain.unregisterAllPushToken(completionHandler: { (response, error) in
//            guard error == nil else {   // Error.
//                return
//            }
//            print("unregisterAllPushToken")
//        })
//    }
//    
//    func loadMessage(groupChannel: SBDGroupChannel, completion: @escaping ([SBDBaseMessage])->()){
//        
//        // There should be only one single instance per channel.
//        sBDPreviousMessageListQuery = groupChannel.createPreviousMessageListQuery()
//        sBDPreviousMessageListQuery?.includeMetaArray = true   // Retrieve a list of messages along with their metaarrays.
//        sBDPreviousMessageListQuery?.includeReactions = true   // Retrieve a list of messages along with their reactions.
//        
//        
//        // Retrieving previous messages.
//        sBDPreviousMessageListQuery?.loadPreviousMessages(withLimit: 30, reverse: false, completionHandler: { (messages, error) in
//            guard error == nil else {   // Error.
//                print(error.debugDescription)
//                return
//            }
//       //     self.listMessages.accept(messages!)
//            completion(messages!)
//        })
//        
//    }
//    
//    func loadPreviousMessages(completion: @escaping ([SBDBaseMessage])->() ){
//        // Retrieving previous messages.
//        sBDPreviousMessageListQuery?.loadPreviousMessages(withLimit: 30, reverse: false, completionHandler: { (messages, error) in
//            guard error == nil else {   // Error.
//                print(error.debugDescription)
//                return
//            }
//            completion(messages!)
//        })
//    }
//    
//    func sendMessage(groupChannel: SBDGroupChannel, message: String, metaArrayValue:[String:[String]], completion:@escaping (SBDBaseMessage)->()){
//        let params = SBDUserMessageParams.init(message: message)!
//        params.message = message
//        params.pushNotificationDeliveryOption = .default
//        
//        groupChannel.sendUserMessage(with: params, completionHandler: {(userMessage, error) in
//            guard error == nil else {   // Error.
//                print(error.debugDescription)
//                
//                if error?.code == 900021 {
//                    Utils.showAlertOKWithAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: R.string.localizable.loginAccountBan(),completion: {
//                        Utils.dismissPopAllViewViewControllers()
//                    })
//                }
//                
//                return
//            }
//            
//            guard let message = userMessage else { return }
//            
//            if let _ = metaArrayValue["content"]{
//                self.addMessageMetaArrayValues(groupChannel: groupChannel, message: message, metaArrayValue: metaArrayValue, completion: { mess in
//                    completion(mess)
//                })
//            }else{
//                completion(message)
//
//            }
//        })
//    }
//    
//    func disconnectSendBird(){
//        SBDMain.disconnect(completionHandler: {
//            print("A current user is disconnected from Sendbird server.")
//        })
//    }
//    
//    func deleteMessage(groupChannel: SBDGroupChannel, message: SBDBaseMessage){
//        // The BASE_MESSAGE below indicates a BaseMessage object to delete.
//        groupChannel.delete(message, completionHandler: { (error) in
//            guard error == nil else {       // Error.
//                return
//            }
//        })
//    }
//    
//    func joinChannelbyChanelUrl(listChanels: [String], offNoti: Bool = false, completion: @escaping (String)->()){
//        guard let userId = UserInfo.shareUserInfo.id, userId != 0 else {
//            
//            return
//        }
//            var count = 0
//            for chanelUrl in listChanels{
//                SBDGroupChannel.getWithUrl(chanelUrl, completionHandler: { (groupChannel, error) in
//                    guard error == nil else {   // Error.
//                        completion("Có lỗi xảy ra")
//                        return
//                    }
//                    guard let groupChannel = groupChannel else { return }
//                    if groupChannel.isPublic {
//                        groupChannel.join { (error) in
//                            guard error == nil else {   // Error.
//                                print(error.debugDescription)
//                                
//                                // error
//                                if error?.code == 400203{
//                                    completion("Nhóm đã đủ thành viên!")
//                                }
//                                
//                                return
//                            }
//                            
//                            // off noti các kênh defaul
//                            if offNoti {
//                                self.setPushNotification(groupChannel: groupChannel, enable: false)
//                            }
//                            
//                            // count == số kênh thì join vào tất cả các kênh thành công
//                            count += 1
//                            if count == listChanels.count{
//                                self.joinChanelSuccess(groupChannel)
//                            }
//                            
//                        }
//                    }
//                })
//            }
//    }
//    
//    func sendFile(groupChannel: SBDGroupChannel, data: Data, fileName: String?, metaArrayValue: [String:[String]], completion:@escaping (SBDBaseMessage)->()){
//        // Sending a file message with a raw file
//        var thumbnailSizes = [SBDThumbnailSize]()
//        
////        thumbnailSizes.append(SBDThumbnailSize.make(withMaxCGSize: CGSize(width: 100.0, height: 100.0))!)       // Allowed number of thumbnail images: 3
////        thumbnailSizes.append(SBDThumbnailSize.make(withMaxWidth: 200.0, maxHeight: 200.0)!)
//        
//        let params = SBDFileMessageParams.init(file: data)
//        params!.fileName = "1 photo send"
//        params!.thumbnailSizes = thumbnailSizes
//        params!.pushNotificationDeliveryOption = .default
//        
//        groupChannel.sendFileMessage(with: params!, completionHandler: { (fileMessage, error) in
//            guard error == nil else {   // Error.
//                
//                if error?.code == 900021 {
//                    Utils.showAlertOKWithAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: R.string.localizable.loginAccountBan(),completion: {
//                        Utils.dismissPopAllViewViewControllers()
//                    })
//                }
//                
//                return
//            }
//            
//            guard let message = fileMessage else { return }
//                       
//                       if let _ = metaArrayValue["content"]{
//                           self.addMessageMetaArrayValues(groupChannel: groupChannel, message: message, metaArrayValue: metaArrayValue, completion: { mess in
//                            completion(mess)
//                            
//                           })
//                       }else{
//                        completion(message)
//                       }
//        })
//    }
////
////    func setPushNotification(groupChannel: SBDGroupChannel, enable: Bool) {
////
////
////        if !enable{
////            // If you want to turn off notifications for this channel, pass `false` as a parameter. By default, they are turned on for a channel (true).
////            groupChannel.setPushPreferenceWithPushOn(false, completionHandler: { (error) in
////                guard error == nil else {   // Error.
////                    return
////                }
////                self.pushNotificationMessage.onNext(false)
////                // print(<#T##items: Any...##Any#>)
////            })
////        }else{
////            // If you want to automatically apply the user's push notification setting to the channel.
////            groupChannel.setMyPushTriggerOption(.default, completionHandler: { (error) in
////                guard error == nil else {   // Error.
////                    return
////                }
////                self.pushNotificationMessage.onNext(true)
////            })
////        }
////    }
//
//    
////    func getListChannels(completion:@escaping (_ : [SBDGroupChannel])->()){
////
////        SendBirdManager.share.connectUserToSendBird(completion: {
////            self.sBDGroupChannelListQuery = SBDGroupChannel.createMyGroupChannelListQuery()
////
////            self.sBDGroupChannelListQuery?.includeEmptyChannel = true
////            self.sBDGroupChannelListQuery?.order = .latestLastMessage
////            self.sBDGroupChannelListQuery?.limit = 40
////
////            self.sBDGroupChannelListQuery?.loadNextPage(completionHandler: { (groupChannels, error) in
////                guard error == nil else {   // Error.
////                    completion([])
////                    return
////                }
////                guard let groupChannels = groupChannels else { return }
////
////                completion(groupChannels)
////            })
////        })
////    }
//    
////    func addMembers(userIds:[Int], groupChannel: SBDGroupChannel, completion: ()->()){
////        let userIDs = userIds.map { String($0) }
////        groupChannel.inviteUserIds(userIDs, completionHandler: { (error) in
////            guard error == nil else {   // Error.
////                return
////            }
////        })
////        completion()
////    }
//    
////    func loadNextPage(completion:@escaping (_ : [SBDGroupChannel])->()){
////        self.sBDGroupChannelListQuery?.loadNextPage(completionHandler: { (groupChannels, error) in
////            guard error == nil else {   // Error.
////                completion([])
////                return
////            }
////            guard let groupChannels = groupChannels else { return }
////
////            completion(groupChannels)
////        })
////    }
//    
////    func searchChannel(textSearch: String, completion:@escaping (_ : [SBDGroupChannel])->()){
////
////        guard let channelListQuery = SBDGroupChannel.createMyGroupChannelListQuery() else { return }
////        channelListQuery.includeEmptyChannel = true
////       // channelListQuery.setSearchFilter(textSearch, fields: .channelName)
////        channelListQuery.channelNameContainsFilter = textSearch
////        channelListQuery.loadNextPage(completionHandler: { (groupChannels, error) in
////            guard error == nil else {   // Error.
////                completion([])
////                return
////            }
////
////            guard let groupChannels = groupChannels else { return }
////            completion(groupChannels)
////        })
////    }
//    
////    func getListUserAdmin(groupChannel: SBDGroupChannel){
////        let query = groupChannel.createOperatorListQuery()
////        query?.loadNextPage(completionHandler: { (users, error) in
////            guard error == nil else {   // Error
////                return
////            }
////            
////           // print(users)
////
////            // Retrieving operators.
////            for usser in users! {
////               print(usser)
////            }
////        })
////    }
//}

