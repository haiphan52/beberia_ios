//
//  SetPasswordSecretViewController.swift
//  Beberia
//
//  Created by IMAC on 1/7/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import SVProgressHUD

enum TypeSetPass {
    case First
    case RePass
    case Permiss
}

class SetPasswordSecretViewController: BaseViewController {
    
    @IBOutlet weak var imageDot4: UIImageView!
    @IBOutlet weak var imageDot3: UIImageView!
    @IBOutlet weak var imageDot2: UIImageView!
    @IBOutlet weak var imageDot1: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tfNumberVerOne: UITextField!
    @IBOutlet weak var tfNumberVerTwo: UITextField!
    @IBOutlet weak var tfNumberVerThree: UITextField!
    @IBOutlet weak var tfNumberVerFour: UITextField!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblError: UILabel!
    
    var otpTemp = ""
    var typeSetPass = TypeSetPass.First
    var actionConfirm:(_ result: Bool)->() = {_ in }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }
    
    override func backVC() {
        
        switch typeSetPass {
        case .First:
            self.dismiss(animated: true, completion: nil)
        case .RePass:
            self.navigationController?.popViewController(animated: true) 
        case .Permiss:
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.resetTexrView()
    }
    
    func resetTexrView(){
        tfNumberVerOne.text = ""
        tfNumberVerTwo.text = ""
        tfNumberVerThree.text = ""
        tfNumberVerFour.text = ""
        tfNumberVerOne.becomeFirstResponder()
    }
    
    func setupUI(){
        imageDot1.alpha = 0.0
        imageDot2.alpha = 0.0
        imageDot3.alpha = 0.0
        imageDot4.alpha = 0.0
        
        tfNumberVerOne.becomeFirstResponder()
        tfNumberVerOne.delegate   = self
        tfNumberVerTwo.delegate   = self
        tfNumberVerThree.delegate = self
        tfNumberVerFour.delegate  = self
        
        btnConfirm.isHidden = true
        
        lblError.text = ""
        
        switch typeSetPass {
        case .First:
            title = R.string.localizable.secretSetPass()
            lblTitle.text = R.string.localizable.secretTile_Frist()
            btnConfirm.setImage(R.image.ic_continue(), for: .normal)
            customLeftBarButton()
        case .RePass:
            title = R.string.localizable.secretSetPass()
            lblTitle.text = R.string.localizable.secretTile_RePass()
            btnConfirm.setImage(R.image.ic_confirm(), for: .normal)
            customLeftBarButton()
        case .Permiss:
            title = R.string.localizable.secretRequirePass()
            lblTitle.text = R.string.localizable.secretTile_PleasePass()
            btnConfirm.setImage(R.image.ic_confirm(), for: .normal)
            customLeftBarButton()
        }
        
    }
    
    func confirmACtion()-> () {
        guard let otpOne = tfNumberVerOne.text, let otpTwo = tfNumberVerTwo.text,let otpThree = tfNumberVerThree.text,let otpFour = tfNumberVerFour.text, otpOne.count > 0, otpTwo.count > 0,otpThree.count > 0,otpFour.count > 0 else {
            Utils.showAlertOK(controller: self, title: R.string.localizable.commonError(), message: R.string.localizable.secretError_OTP())
            actionConfirm(false)
            return
        }
        let otp = "\(otpOne)\(otpTwo)\(otpThree)\(otpFour)"
        print(otp)
        
        switch typeSetPass {
        case .First:
            let setPassVC = SetPasswordSecretViewController.init(nib: R.nib.setPasswordSecretViewController)
            setPassVC.typeSetPass = .RePass
            setPassVC.otpTemp = otp
            self.navigationController?.pushViewController(setPassVC, animated: true)
        // actionConfirm(true)
        case .RePass:
            if otp == otpTemp{
                
                self.createPassSecret(password_secrect: otp) { (isSuccess) in
                    if isSuccess{
                        UserInfo.shareUserInfo.passwordSecrect = self.otpTemp
                        //   self.navigationController?.popViewController(animated: true)
                        Utils.getTopMostViewController()?.dismiss(animated: true, completion: {
                            Utils.getTopMostViewController()?.tabBarController?.selectedIndex = 2
                        })
                        self.actionConfirm(true)
                        
                    }else{
                        self.actionConfirm(false)
                    }
                }
            }else{
                resetTexrView()
                lblError.text = R.string.localizable.secretError_Pass_Not()
                actionConfirm(false)
            }
            
        case .Permiss:
            if UserInfo.shareUserInfo.passwordSecrect == otp{
                self.navigationController?.popViewController(animated: true)
                Utils.getTopMostViewController()?.dismiss(animated: true, completion: {
                    Utils.getTopMostViewController()?.tabBarController?.selectedIndex = 2
                })
                actionConfirm(true)
            }else{
                resetTexrView()
                lblError.text = R.string.localizable.secretError_Pass()
                actionConfirm(false)
            }
        }
    }
    
    func createPassSecret(password_secrect: String, completion: @escaping (_ result: Bool)->()){
        SVProgressHUD.show()
        APIManager.createPassSecret(password_secrect: password_secrect, callbackSuccess: { [weak self] (isSuccess) in
            guard let _ = self else { return }
            completion(isSuccess)
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    @IBAction func didPressConfirm(_ sender: Any) {
        let _ = confirmACtion()
    }
}

extension SetPasswordSecretViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text?.count)! < 1  && string.count > 0){
            if(textField == tfNumberVerOne)
            {
                tfNumberVerTwo.becomeFirstResponder()
                textField.text = string
            }
            if(textField == tfNumberVerTwo)
            {
                tfNumberVerThree.becomeFirstResponder()
                textField.text = string
            }
            if(textField == tfNumberVerThree)
            {
                tfNumberVerFour.becomeFirstResponder()
                textField.text = string
            }
            if(textField == tfNumberVerFour)
            {
                textField.resignFirstResponder()
                textField.text = string
                confirmACtion()
            }
            
            
            return false
        }
        else if ((textField.text?.count)! >= 1  && string.count == 0){
            // on deleting value from Textfield
            if(textField == tfNumberVerTwo)
            {
                tfNumberVerOne.becomeFirstResponder()
            }
            if(textField == tfNumberVerThree)
            {
                tfNumberVerTwo.becomeFirstResponder()
            }
            if(textField == tfNumberVerFour)
            { 
                tfNumberVerThree.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }
        else if ((textField.text?.count)! >= 1  )
        {
            textField.text = string
            textField.becomeFirstResponder()
            return false
        }
     
        
        return true
    }
}

