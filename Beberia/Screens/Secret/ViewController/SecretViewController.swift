//
//  SecretViewController.swift
//  Beberia
//
//  Created by IMAC on 12/30/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import PullToRefresh
import RxCocoa
import RxSwift

class SecretViewController: BaseViewController {
    
    
    @IBOutlet weak var viewTopic: UIView!
    @IBOutlet weak var tbvSecret: UITableView!
    @IBOutlet weak var bannerView: BannerView!
    
    var rightBarButtonItemNoti = UIBarButtonItem()
    var isLoadingMore = false
    var arraySecret =  BehaviorRelay.init(value: [HomeFeed]())
    var nextPage = 0
    var page = 1
    let refresher = PullToRefresh()
    var filter_date = 0
    var subTitle = 0
    var checkedToday = true
    var checkedWeek = true
    var checkedMonth = true
    
    @IBOutlet weak var viewTokToday: UIView!
    @IBOutlet weak var imgTokToday: UIImageView!
    @IBOutlet weak var lblTokToday: UILabel!
    @IBOutlet weak var viewTokWeek: UIView!
    @IBOutlet weak var imgTokWeek: UIImageView!
    @IBOutlet weak var lblTokWeek: UILabel!
    @IBOutlet weak var viewTokMonth: UIView!
    @IBOutlet weak var lblTokMonth: UILabel!
    @IBOutlet weak var imgTokMonth: UIImageView!
    
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupViewTopic()
        NotificationCenter.default.addObserver(self, selector: #selector(checkCreateSecret), name: NSNotification.Name.createSecret, object: nil)
        SVProgressHUD.show()
        getSecretHome(page: page, key: "", secret: 1, subTitle: subTitle, filter_date: filter_date)
        self.tabBarController?.delegate = self
        self.getBanner()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.createSecret, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.setupNavigationiOS15(font: UIFont.init(name: "NotoSans-Bold", size: 18) ?? UIFont.systemFont(ofSize: 18), isBackground: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        tbvSecret.isUserInteractionEnabled = true
    }
    
    //MARK: Setup UI
    func setupViewTopic(){
        let viewTopic1 = ViewTopic.instanceFromNib(data: SUBTITLE_SECRET.arrayList , isUnselect: true)
        viewTopic1.didSelect = { index in
         //   self.arraySecret.removeAll()
            self.subTitle = index
            self.arraySecret.accept([])
            self.page = 1
            self.getSecretHome(page: self.page, key: "", secret: 1, subTitle: self.subTitle, filter_date: self.filter_date)
        }
        viewTopic1.frame = viewTopic.bounds
        viewTopic.addSubview(viewTopic1)
    }
    
    func setupUI(){
        let label = UILabel()
        label.text = R.string.localizable.secretTile()
        label.font = UIFont(name:"\(AppFont.HelveticaNeueBold)", size: 30)
        label.textColor = UIColor.init(hex: "0299fe")
        
        let leftBarButtonItem = UIBarButtonItem.menuButton(self, action: #selector(BaseViewController.backVC), imageName: R.image.ic_Back() ?? UIImage.init(), height: 30, width: 40, view: self.view)
        self.navigationItem.leftBarButtonItems = [leftBarButtonItem, UIBarButtonItem.init(customView: label)]
        self.navigationController?.navigationBar.isTranslucent = false
        self.extendedLayoutIncludesOpaqueBars = true
        setupRightBarButton()
        
        tbvSecret.register(R.nib.secretCell)
        tbvSecret.separatorStyle = .none
        tbvSecret.showsVerticalScrollIndicator = false
        tbvSecret.isScrollEnabled = true
        tbvSecret.rowHeight = 135
      //  tbvSecret.infiniteScrollTriggerOffset = 700
        tbvSecret.showsVerticalScrollIndicator = false
        tbvSecret.addInfiniteScroll { (tableView) in
            tableView.performBatchUpdates({ () -> Void in
                // update collection view
                if self.nextPage != 0{
                    self.isLoadingMore = true
                    self.page += 1
                    self.getSecretHome(page: self.page, key: "", secret: 1, subTitle: self.subTitle, filter_date: self.filter_date)
                }else{
                    self.tbvSecret.endRefreshing(at: .top)
                    self.tbvSecret.finishInfiniteScroll(completion: { (collection) in
                    })
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                tableView.finishInfiniteScroll()
            });
            
        }
        
        tbvSecret.addPullToRefresh(refresher) {
            self.isLoadingMore = true
            self.page = 1
            self.arraySecret.accept([])
            
            self.getSecretHome(page: self.page, key: "", secret: 1, subTitle: self.subTitle, filter_date: self.filter_date)
            self.tbvSecret.startRefreshing(at: .top)
            
        }
        
        // bind to data
        self.arraySecret.asObservable().observeOn(MainScheduler.asyncInstance).bind(to: tbvSecret.rx.items){(tv, row, item) -> UITableViewCell in
            let cell = tv.dequeueReusableCell(withIdentifier: R.reuseIdentifier.secretCell.identifier) as! SecretCell
            
            cell.setupDataSecret(secret: item)
            return cell
        }.disposed(by: disposeBag)
        
        // itemSelected taablview
              tbvSecret.rx.itemSelected.observeOn(MainScheduler.asyncInstance).subscribe { (row) in
                Router.share.pushToViewHomeDetail(tableView: self.tbvSecret, array: self.arraySecret.value,indexPath: row.element!, viewSelf: self, idFeed: self.arraySecret.value[row.element!.row].id ?? 0, editFromView: .Secret, completion: { homeFeeds in
                    self.arraySecret.accept(homeFeeds)
                })
              }.disposed(by: disposeBag)
    }
    
    private func getBanner(){
        var arrayImage = [String]()
        APIManager.getBanner(type: 7, callbackSuccess: { [weak self]  (banners) in
            guard let self = self else { return }
            for banner in banners {
                arrayImage.append(banner.link ?? "")
            }
            
            self.showBannerView(isShow: arrayImage.count > 0)
            self.bannerView.arrayImage = arrayImage
            self.bannerView.tapBanner = { index in
                guard let bannerType = BannerView.BannerTypeRedirect(rawValue: banners[index].typeRedirect ?? 0) else {
                    return
                }
                switch bannerType {
                case .none:
                    if let url = URL.init(string: banners[index].linkAds ?? "")  {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url , options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                case .rankPoint:
                    let rankPointVC = RankPointVC.init(nib: R.nib.rankPointVC)
                    rankPointVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(rankPointVC, animated: true)
                case .giftGame:
                    let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
                    gameVC.modalPresentationStyle = .overFullScreen
                    self.present(gameVC, animated: true, completion: nil)
                case .info:
                    guard let lichKTVC = R.storyboard.main.infoViewController() else{ return }
                    lichKTVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(lichKTVC, animated: true)
                case .momTok: break
                case .news:
                    let listNewHotVC = ListNewHotViewController.init(nib: R.nib.listNewHotViewController)
                    listNewHotVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(listNewHotVC, animated: true)
                case .listDiary:
                    let postVC = ListDiaryViewController.init(nib: R.nib.listDiaryViewController)
                    postVC.hidesBottomBarWhenPushed = true
                    postVC.gettype = "3"
                    postVC.title = "Mẹo cho bạn"
                    self.navigationController?.pushViewController(postVC, animated: true)
                case .home:
                    self.navigationController?.popViewController(animated: true, {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            guard let vc = Utils.getTopMostViewController(), let tabbar = vc.tabBarController else { return }
                            tabbar.selectedIndex = 1
                            vc.tabBarController?.tabBar.isHidden = false
                        }
                    })
                case .diaary:
                    self.navigationController?.popViewController(animated: true, {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            guard let vc = Utils.getTopMostViewController(), let tabbar = vc.tabBarController else { return }
                            tabbar.selectedIndex = 2
                            vc.tabBarController?.tabBar.isHidden = false
                        }
                    })
                }
            }
            self.bannerView.backgroundColor = .clear
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        })
    }
    
    private func showBannerView(isShow: Bool){
//        let height = (Screen.width * 780 ) / 2050
//        heightBannerView.constant = isShow ? height : 0
        bannerView.isHidden = isShow ? false : true

        UIView.animate(withDuration: 0.3) {
            self.updateViewConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    func setupRightBarButton(){
        rightBarButtonItemNoti = UIBarButtonItem.menuButton(self, action: #selector(SecretViewController.tapSreach), imageName: R.image.ic_searchSecret() ?? UIImage.init(), height: 30, width: 30)
        var rightBarButtonNotiSec = UIBarButtonItem.menuButton(self, action: #selector(SecretViewController.tapNoti), imageName: R.image.ic_notiSecret() ?? UIImage.init(), height: 30, width: 30)
        if UserInfo.shareUserInfo.unWatch_secret > 0 {
            rightBarButtonNotiSec = UIBarButtonItem.menuButton(self, action: #selector(SecretViewController.tapNoti), imageName: R.image.ic_notiSecretRead() ?? UIImage.init(), height: 30, width: 30)
            rightBarButtonNotiSec.addBadge(text: "\(UserInfo.shareUserInfo.unWatch_secret)")
        }
        
        let rightBarButtonItemSreach = UIBarButtonItem.menuButton(self, action: #selector(SecretViewController.tapSreach), imageName: R.image.ic_searchSecret() ?? UIImage.init(), height: 30, width: 30)
        rightBarButtonItemSreach.tintColor = .white
        self.navigationItem.rightBarButtonItems = [rightBarButtonNotiSec,rightBarButtonItemSreach]
        
    }
    
    @objc func checkCreateSecret(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        
        let idSecret = object[Key.KeyNotification.idSecret] as? Int
        let isEdit = object[Key.KeyNotification.EditFeed] as? Bool
        self.getCheckCompleteFeed(id: idSecret ?? 0, isEdit: isEdit ?? true, completion: { homeFeed in
            var arrayTemp = self.arraySecret.value
            arrayTemp.insert(homeFeed, at: 0)
            self.arraySecret.accept(arrayTemp)
        })
    }
    
    
    //MARK: @objc
    @objc func tapSreach(){
        let homeVC = SearchSecretVC.init(nib: R.nib.searchSecretVC)
        homeVC.hidesBottomBarWhenPushed = true
        Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @objc func tapNoti(){
        rightBarButtonItemNoti.setBadge(text: "")
        UserInfo.shareUserInfo.unWatch_secret = 0
        
        let homeVC = NotificationSecretViewController.init(nib: R.nib.notificationSecretViewController)
        homeVC.hidesBottomBarWhenPushed = true
        Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    func getSecretHome(page: Int,key:String,secret: Int,subTitle: Int,filter_date:Int){
        if !isLoadingMore{
            SVProgressHUD.show()
        }
        APIManager.getSecretHome(key: key, page: page, secret: secret, subTitle: subTitle, filter_date: filter_date, callbackSuccess: { [weak self] (homeFeeds, nextPage) in
            guard let self = self else { return }
            self.nextPage = nextPage
            self.isLoadingMore = false
            
            if self.arraySecret.value.count == 0 {
                self.arraySecret.accept(homeFeeds)
            }else{
                self.arraySecret.accept(self.arraySecret.value + homeFeeds)
            }
            
            self.tbvSecret.endRefreshing(at: .top)
            self.tbvSecret.finishInfiniteScroll(completion: { (collection) in
            })
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    @IBAction func didPressCreateSecret(_ sender: Any) {
        let writeHome = WriteHomeViewController.init(nib: R.nib.writeHomeViewController)
        writeHome.hidesBottomBarWhenPushed = true
        writeHome.fromView = .Secret
        self.navigationController?.pushViewController(writeHome, animated: true)
    }
    
    @IBAction func didPressMonth(_ sender: Any) {
        self.arraySecret.accept([])
        filter_date = 3
        checkedWeek = true
        checkedToday =  true
        if checkedMonth {
            setupTok(viewColor: viewTokMonth, label: lblTokMonth, imgBest: imgTokMonth)
            checkedMonth = false
        } else {
            setupTok(viewColor: UIView(), label: UILabel(), imgBest: UIImageView())
            filter_date = 0
            checkedMonth = true
        }
        
        self.getSecretHome(page: self.page, key: "", secret: 1, subTitle: subTitle, filter_date: filter_date)
    }
    
    @IBAction func didPressToday(_ sender: Any) {
        self.arraySecret.accept([])
        filter_date = 1
        checkedWeek = true
        checkedMonth = true
        if checkedToday {
            setupTok(viewColor: viewTokToday, label: lblTokToday, imgBest: imgTokToday)
            checkedToday = false
        } else {
            setupTok(viewColor: UIView(), label: UILabel(), imgBest: UIImageView())
            filter_date = 0
            checkedToday = true
        }
        self.getSecretHome(page: self.page, key: "", secret: 1, subTitle: subTitle, filter_date: filter_date)
    }
    
    @IBAction func didPressTokWeek(_ sender: Any) {
        self.arraySecret.accept([])
        filter_date = 2
        checkedToday =  true
        checkedMonth = true
        if checkedWeek {
            setupTok(viewColor: viewTokWeek, label: lblTokWeek, imgBest: imgTokWeek)
            checkedWeek = false
        } else {
            setupTok(viewColor: UIView(), label: UILabel(), imgBest: UIImageView())
            filter_date = 0
            checkedWeek = true
        }
        self.getSecretHome(page: self.page, key: "", secret: 1, subTitle: subTitle, filter_date: filter_date)
    }
    
    func setupTok (viewColor:UIView, label: UILabel, imgBest:UIImageView){
        viewTokToday.backgroundColor = .white
        imgTokToday.image = R.image.ic_best()
        lblTokToday.textColor = UIColor.init(hexString: "e64747",alpha: 1)
        
        viewTokWeek.backgroundColor = .white
        imgTokWeek.image = R.image.ic_best()
        lblTokWeek.textColor = UIColor.init(hexString: "e64747",alpha: 1)
        
        viewTokMonth.backgroundColor = .white
        imgTokMonth.image = R.image.ic_best()
        lblTokMonth.textColor = UIColor.init(hexString: "e64747",alpha: 1)
        viewColor.backgroundColor = UIColor.init(hexString: "e64747",alpha: 1)
        imgBest.image = R.image.ic_bestActive()
        label.textColor = .white
        
    }
}
