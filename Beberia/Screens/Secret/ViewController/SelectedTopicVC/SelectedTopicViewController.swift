//
//  SelectedTopicViewController.swift
//  Beberia
//
//  Created by IMAC on 1/8/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

class SelectedTopicViewController: BaseViewController {

    @IBOutlet weak var viewTopic: UIView!
  
    var contentSreach = ""
    var stringImage = ""
    var titlePost = ""
    var comtentHTML = ""
    var indexSubTitle = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewTopic()
        
        title = R.string.localizable.secretWrite_Tile()
        customLeftBarButton()
        let rightBarButtonItem = UIBarButtonItem.init(title: R.string.localizable.homePost(), style: .plain, target: self, action: #selector(SelectedTopicViewController.createSecret))
        rightBarButtonItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "eebf3b") as Any], for: .normal)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBottomBorderColor(color: .gray)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBottomBorderColor(color: .white)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    func createPostSecret(){
        SVProgressHUD.show()
              APIManager.createHomeFeed(title: titlePost, content: comtentHTML , images: stringImage, videos: "[]", city_code: 0, about: 0, category: 4, content_search: contentSreach, ageRange: 7, district_code: "0", subTitle: self.indexSubTitle, callbackSuccess: { [weak self] (homeFeed) in
                guard let self = self else { return }
                  SVProgressHUD.dismiss()
                  NotificationCenter.default.post(name: NSNotification.Name.createSecret, object: [Key.KeyNotification.idSecret: homeFeed.id ?? 0, Key.KeyNotification.EditFeed : false], userInfo: nil)
                //  self.navigationController?.popToRootViewController(animated: true)
                
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: SecretViewController.self) {
                        _ =  self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                
              }) { (error) in
                  SVProgressHUD.dismiss()
                  Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
              }
    }
    
    @objc func createSecret(){
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        createPostSecret()
        print("createSecret")
    }

    //MARK: - Setup UI
    
    func setupViewTopic(){
        let arrayList = SUBTITLE_SECRET.arrayList
        let viewTopic1 = ViewTopic.instanceFromNib(data: arrayList.filter { $0 != "Tất cả" }, isUnselect: true)
        viewTopic1.didSelect = { index in
            self.indexSubTitle = index + 1
            print(self.indexSubTitle)
        }
        viewTopic1.frame = viewTopic.bounds
        viewTopic.addSubview(viewTopic1)
    }

}
