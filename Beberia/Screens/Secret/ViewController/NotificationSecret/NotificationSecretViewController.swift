//
//  NotificationSecretViewController.swift
//  Beberia
//
//  Created by OS on 1/7/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import Kingfisher
import PullToRefresh

class NotificationSecretViewController: BaseViewController {

    @IBOutlet weak var tbvNoti: UITableView!
    
    var fetchedListNoti = [Noti]()
    var fetchedListNoti2 = [Noti]()
    var listNoti = [SectionData]()
    var page = 1
    var nextpage = 0
    let refresher = PullToRefresh()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getListNotiSecret(page: 1)
        // Do any additional setup after loading the view.
    }

   
    func setupUI(){
          tbvNoti.register(R.nib.notificationTableViewCell)
          tbvNoti.registerHeaderFooterView(R.nib.headerSectionTableViewCell)
          tbvNoti.delegate = self
          tbvNoti.dataSource = self
          tbvNoti.separatorStyle = .none
          tbvNoti.rowHeight = UITableView.automaticDimension
        tbvNoti.estimatedRowHeight = UITableView.automaticDimension
        self.title = R.string.localizable.homeNotification()
        customLeftBarButton()
        
        tbvNoti.infiniteScrollTriggerOffset = 700
        tbvNoti.showsVerticalScrollIndicator = false
        tbvNoti.addInfiniteScroll { (tableView) in
            tableView.performBatchUpdates({ () -> Void in
                // update collection view
                if self.nextpage > 0 {
                    self.page = self.page + 1
                    self.getListNotiSecret(page: self.page)
                }else {
                    self.tbvNoti.finishInfiniteScroll(completion: { (collection) in
                    })
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                tableView.finishInfiniteScroll()
            });
            
        }
          
          tbvNoti.addPullToRefresh(refresher) {
              self.page = 1
              self.listNoti.removeAll()
              self.tbvNoti.reloadData()
              self.getListNotiSecret(page: self.page)
              self.tbvNoti.startRefreshing(at: .top)
          }
      }
        
    func getListNotiSecret(page:Int){
        SVProgressHUD.show()
        APIManager.getListNotiSecret(page: page, callbackSuccess: { [weak self] (notifications, next_page) in
            guard let self = self else { return }
            self.nextpage = next_page
            if page == 1 {
                if notifications.count > 0 {
                    if notifications.count - 1 > 4 {
                        self.fetchedListNoti = Array(notifications[0...4])
                        self.fetchedListNoti2 = Array(notifications[5...notifications.count - 1])
                        self.listNoti.append(SectionData(title: R.string.localizable.homeNotificationLatest(), data: self.fetchedListNoti))
                        self.listNoti.append(SectionData(title: R.string.localizable.homeNotificationRecently(), data: self.fetchedListNoti2))
                    }else {
                        self.listNoti.append(SectionData(title:  R.string.localizable.homeNotificationLatest(), data: notifications))
                    }
                }
            }else {
                self.listNoti[1].data?.append(contentsOf: notifications)
            }
            
            self.tbvNoti.reloadData()
            
            self.tbvNoti.endRefreshing(at: .top)
            self.tbvNoti.finishInfiniteScroll(completion: { (collection) in
            })
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    
}



extension NotificationSecretViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return listNoti[section].data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.notificationTableViewCell, for: indexPath)!
        let noti = NotificationList(rawValue: listNoti[indexPath.section].data?[indexPath.row].type ?? 0)
        switch noti {
        case .LikeSecret?:
            cell.setuplikeSecret(likeSecret : self.listNoti[indexPath.section], row: indexPath.row, typeNoti: noti!)
        case.LikeComSecret?:
            cell.setuplikeSecret(likeSecret : self.listNoti[indexPath.section], row: indexPath.row, typeNoti: noti!)
        case .ComSecret?:
            cell.setupComSecret(likeSecret: self.listNoti[indexPath.section], row: indexPath.row, typeNoti: noti!)
        case .LikeRepComSecret:
            cell.setuplikeRepComSecret(likeSecret: self.listNoti[indexPath.section], row: indexPath.row, typeNoti: noti!)
        case .RepcomSecret?:
            cell.setupRepComSecret(likeSecret: self.listNoti[indexPath.section], row: indexPath.row, typeNoti: noti!)
                 
             default:
                return UITableViewCell()
        }
          return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return UITableView.automaticDimension
      }
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return listNoti.count
      }
      
      func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
              return 40
      }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.reuseIdentifier.headerSectionTableviewCell)!
           headerView.lblTitle.text = listNoti[section].title
           return headerView
       }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let notification = listNoti[indexPath.section].data?[indexPath.row] ?? Noti.init(json: "")
        let noti = NotificationList(rawValue: notification.type ?? 0)
        switch noti {
        // done
        case .LikeSecret?:
            guard let selfStrong = Utils.getTopMostViewController() else {
                return
            }
            let id = notification.feed?.id ?? 0
            Router.share.pushToViewHomeDetail(tableView: tableView, array: [HomeFeed](),indexPath: indexPath, viewSelf: selfStrong, idFeed: id, editFromView: .Secret, completion: { homeFeeds in
                
            })
            
        case .LikeComSecret?,.ComSecret?:
            guard let selfStrong = Utils.getTopMostViewController() else {
                return
            }
             let id = notification.feed?.id ?? 0
            Router.share.pushToViewHomeDetail(tableView: tableView, array: [HomeFeed](),indexPath: indexPath, viewSelf: selfStrong, idFeed: id, editFromView: .Secret, completion: { homeFeeds in
                
            })

        case .LikeRepComSecret?:
          
            let notiCommentVC = NotificationCommentViewController.init(nib: R.nib.notificationCommentViewController)
            //    if (notification.comment?.reply ?? [Comment]()).count > 0{
            let comment = notification.replyComment
            notiCommentVC.commentNotification = comment
            //     }
            notiCommentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            notiCommentVC.fromToView = noti == .LikeRepCom ? .Home : .Diary
            notiCommentVC.userFrom = notification.userFrom ?? User.init(json: "")
            notiCommentVC.commentRoot = notification.comment
            notiCommentVC.typeNotification = noti!
            notiCommentVC.idCommentParent = notification.comment?.commentId ?? 0
            self.navigationController?.pushViewController(notiCommentVC, animated: true)
            
        case .RepcomSecret?:
             print("")
            let notiCommentVC = NotificationCommentViewController.init(nib: R.nib.notificationCommentViewController)
            //   if (notification.comment?.reply ?? [Comment]()).count > 0{
            let comment = notification.replyComment
            notiCommentVC.commentNotification = comment
            //   }
            notiCommentVC.updateDataWhenDeleteComment = { idNoti in
                if self.listNoti.count > 0{
                    for (index , value) in self.listNoti.enumerated(){
                        var notiTemp = value
                        let indexDelete = notiTemp.data?.firstIndex(where: { (notiResult) -> Bool in
                            return notiResult.id == idNoti
                        })

                        if indexDelete != nil{
                            notiTemp.data?.remove(at: indexDelete!)
                            self.listNoti[index] = notiTemp
                            self.tbvNoti.reloadData()
                            break
                        }
                    }

                }
            }
            notiCommentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            notiCommentVC.fromToView = noti == .RepComt ? .Home : .Diary
            notiCommentVC.idNotification = notification.id ?? 0
            notiCommentVC.userFrom = notification.userFrom ?? User.init(json: "")
            notiCommentVC.commentRoot = notification.comment
            notiCommentVC.idCommentParent = notification.comment?.commentId ?? 0
            notiCommentVC.typeNotification = noti!
            self.navigationController?.pushViewController(notiCommentVC, animated: true)
            
        case .LikeDiary?:
//            let id = notification.feed?.id ?? 0
           print("")
            
        default:
            print("Noti")
        }
        
        
        APIManager.readNotification(id: notification.id ?? 0, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else { return }
            if isSuccess{
                print("Read Notication Success")
                notification.read = 1
                self.tbvNoti.reloadData()
            }else{
                print("Read Notication Fail")
            }
        }) { (error) in
            print(error)
        }
    }

}
