//
//  SearchSecretVC.swift
//  Beberia
//
//  Created by OS on 12/31/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import PullToRefresh

class SearchSecretVC: BaseViewController {
    
    @IBOutlet weak var tbvSearch: UITableView!
    
    var dataCategory = ["Tất cả","Gia đình","Cuộc sống","Con cái","Bóc phốt","Khác"]
    var isLoadingMore = false
    var arraySecret = [HomeFeed]()
    var key = ""
    var subTitle = 0
    var nextPage = 0
    var page = 1
    let refresher = PullToRefresh()
    var filter_date = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        customLeftBarButton()
        let searchBar = UISearchBar()
        searchBar.placeholder = R.string.localizable.homeSearch()
        searchBar.delegate = self
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        navigationItem.titleView = searchBar
        
        tbvSearch.delegate = self
        tbvSearch.dataSource = self
        tbvSearch.tableFooterView = UIView()
        tbvSearch.register(R.nib.secretCell)
        tbvSearch.registerHeaderFooterView(R.nib.searchSecretHeaderCell)
        tbvSearch.separatorStyle = .none
        tbvSearch.showsVerticalScrollIndicator = false
        tbvSearch.addInfiniteScroll { (tableView) in
            tableView.performBatchUpdates({ () -> Void in
                // update collection view
                if self.nextPage != 0{
                    self.isLoadingMore = true
                    self.page += 1
                    self.getSecretHome(page: self.page, key: "", secret: 1, subTitle: 0)
                }else{
                    self.tbvSearch.endRefreshing(at: .top)
                    self.tbvSearch.finishInfiniteScroll(completion: { (collection) in
                    })
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                tableView.finishInfiniteScroll()
            });
            
        }
        
        tbvSearch.addPullToRefresh(refresher) {
            self.isLoadingMore = true
            self.page = 1
            self.arraySecret = []
            self.getSecretHome(page: self.page, key: "", secret: 1, subTitle: 0)
            self.tbvSearch.startRefreshing(at: .top)
        }
        //        getSecretHome(page: 1, key: key, secret: 1, subTitle: subTitle)
    }
    
    func getSecretHome(page: Int,key:String,secret: Int,subTitle: Int){
        
        
        if !isLoadingMore{
            SVProgressHUD.show()
        }
        APIManager.getSecretHome(key: key, page: page, secret: secret, subTitle: subTitle, filter_date: filter_date, callbackSuccess: { [weak self] (homeFeeds, nextPage) in
            guard let self = self else { return }
            self.nextPage = nextPage
            self.isLoadingMore = false
            
            if self.arraySecret.count == 0 {
                self.arraySecret = homeFeeds
            }else{
                self.arraySecret.append(contentsOf: homeFeeds)
            }
            self.tbvSearch.reloadData()
            self.tbvSearch.endRefreshing(at: .top)
            self.tbvSearch.finishInfiniteScroll(completion: { (collection) in
            })
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
}

extension SearchSecretVC: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.view.endEditing(true)
        
        key = searchBar.text ?? ""
        self.arraySecret.removeAll()
        getSecretHome(page: 1, key: key, secret: 1, subTitle: 0)
        
        
    }
}

extension SearchSecretVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySecret.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arraySecret.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.secretCell, for: indexPath)!
            let homefeed = self.arraySecret[indexPath.row]
            cell.setupDataSecret(secret: homefeed)
            return cell
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.reuseIdentifier.searchSecretHeaderCell)
        
        headerView?.tapCategory = { index in
            let districVC = DistrictViewController.init(nib: R.nib.districtViewController)
            districVC.hidesBottomBarWhenPushed = true
            
            districVC.type = 2
            districVC.title = "Chủ đề"
            districVC.tapType = { index in
                self.arraySecret.removeAll()
                self.subTitle = index
                headerView?.viewCategory.backgroundColor = UIColor.init(hexString: AppColor.colorBGMenu)
                headerView?.lblCategory.textColor = UIColor.init(hexString: AppColor.colorText)
                //                     headerView.viewType.borderColor = UIColor.init(hexString: AppColor.colorText)
                headerView?.imgArrow.image = R.image.polygon_23()
                headerView?.lblCategory.text = self.dataCategory[index]
                //
                //                     self.category = index + 1
                //                     self.page = 1
                self.getSecretHome(page: 1, key: self.key, secret: 1, subTitle: self.subTitle)
            }
            Utils.getTopMostViewController()?.navigationController?.pushViewController(districVC, animated: false)
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selfStrong = Utils.getTopMostViewController() else {
            return
        }
        Router.share.pushToViewHomeDetail(tableView: tableView, array: self.arraySecret,indexPath: indexPath, viewSelf: selfStrong, idFeed: self.arraySecret[indexPath.row].id ?? 0, editFromView: .Secret, completion: { homeFeeds in
            self.arraySecret = homeFeeds
            self.tbvSearch.reloadData()
        })
    }
}
