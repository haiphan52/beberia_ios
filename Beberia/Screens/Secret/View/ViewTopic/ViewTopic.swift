//
//  ViewTopic.swift
//  Beberia
//
//  Created by IMAC on 12/30/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import Foundation
import TTGTagCollectionView

class ViewTopic: UIView, TTGTextTagCollectionViewDelegate {
    @IBOutlet weak var viewTopic: TTGTextTagCollectionView!
    
    var data = [String]()
    var didSelect: (_ index: Int)->() = { _ in}
    var isUnselect = false
    var indexSelect = -1
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewTopic.firstView(owner: nil) //Bundle.main.loadNibNamed("ViewLikeComment", owner: nil, options: nil)?.first
    }
    
    class func instanceFromNib(data: [String], isUnselect:Bool) -> ViewTopic {
        let view = R.nib.viewTopic.instantiate(withOwner: nil)[0] as! ViewTopic
        view.data = data
        view.isUnselect = isUnselect
        view.setupView()
        return view
    }
    
    override func awakeFromNib() {
      //  setupView()
    }
    
    func setupView(){
        setupTagView(data: data, tagView: viewTopic)
        viewTopic.alignment = .center
        viewTopic.defaultConfig?.exactWidth = (UIScreen.main.bounds.width / 3) - 10
        viewTopic!.delegate = self
        viewTopic.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        viewTopic.reload()
    }

    func setupTagView(data: [String], tagView: TTGTextTagCollectionView){
        //        tagView.addTags([locations![0].locationNameVi, locations![1].locationNameVi])
        tagView.addTags(data)
        let config = tagView.defaultConfig!
        config.textColor = UIColor.init(hexString: "e83636", alpha: 1)
        config.backgroundColor = UIColor.init(hexString: "#ffffff",alpha: 1)
        config.textFont = UIFont.init(name: AppFont.NotoSansMedium, size: 14)
     //   config.selectedTextColor = UIFont.init(name: AppFont.NotoSansMedium, size: 12)
        config.selectedTextColor = UIColor.init(hexString: "#ffffff",alpha: 0.87)
        config.selectedBackgroundColor = UIColor.init(hexString: "e64747")

        config.borderColor = UIColor.init(hexString: "e83636" ,alpha: 0.5)
        config.selectedBorderColor = UIColor.init(hexString: "#ffffff")
        
        config.borderWidth = 1
        config.selectedBorderWidth = 1
        config.cornerRadius = 5
        config.selectedCornerRadius = 5
        config.shadowOffset = CGSize.zero
        config.shadowRadius = 0
        config.shadowOpacity = 0
        config.exactHeight = 30
        tagView.enableTagSelection = true
        tagView.defaultConfig = config
        tagView.setTagAt(0, selected:true)
        //        tagView.delegate = self
        tagView.reload()
    }

    func textTagCollectionView(_ textTagCollectionView: TTGTextTagCollectionView!, didTapTag tagText: String!, at index: UInt, selected: Bool, tagConfig config: TTGTextTagConfig!) {

//        if isUnselect{
//            viewTopic.setTagAt(0, selected: false)
//            viewTopic.setTagAt(1, selected: false)
//            viewTopic.setTagAt(2, selected: false)
//            viewTopic.setTagAt(3, selected: false)
//            viewTopic.setTagAt(4, selected: false)
//            viewTopic.setTagAt(5, selected: false)
//
//            if indexSelect == Int(index) && selected == false{
//                viewTopic.setTagAt(index, selected: false)
//                 didSelect(-1)
//            }else{
//                viewTopic.setTagAt(index, selected: true)
//               didSelect(Int(index))
//            }
//
//            indexSelect = Int(index)
//
//        }else{
            didSelect(Int(index))
            viewTopic.setTagAt(0, selected: false)
            viewTopic.setTagAt(1, selected: false)
            viewTopic.setTagAt(2, selected: false)
            viewTopic.setTagAt(3, selected: false)
            viewTopic.setTagAt(4, selected: false)
            viewTopic.setTagAt(5, selected: false)
            viewTopic.setTagAt(index, selected: true)
//        }
    }
    
}
