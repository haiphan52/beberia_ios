//
//  SecretCell.swift
//  Beberia
//
//  Created by OS on 12/30/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class SecretCell: UITableViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNumberComment: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewContainer.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupDataSecret(secret:HomeFeed){
        if secret.subTitle == 0 { secret.subTitle = 5 }
        lblCategory.text = "[\(SUBTITLE_SECRET.init(rawValue: secret.subTitle ?? 0)?.title ?? "")]"
        lblTitle.text = "#BB\(secret.id ?? 0)"
        lblSubTitle.text = secret.title
//        lblContent.text = secret.content
        lblDate.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(secret.created ?? 0)), format: Key.DateFormat.DateFormatddMMyy)
        lblNumberComment.text = "\(secret.numberComment ?? 0)"
        imgIcon.isHidden = true
        if secret.media?.count ?? 0 > 0 {
           imgIcon.isHidden = false
        }
        
        
        var content1 = secret.content!
        if !content1.contains("<span"){
            content1 = "<span" + Key.FontSizeAttribuite.FontSize + content1 + "</span>"
        }
        
        
        let data = content1.data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSMutableAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        if let attrStr = attrStr {
            attrStr.addAttribute(.foregroundColor, value: UIColor.init(hexString: "000000", alpha: 0.6)!, range: NSRange(location: 0, length: attrStr.length))


            self.lblContent.attributedText = attrStr
            self.lblContent.lineBreakMode = .byTruncatingTail
        }
    }
    
}
