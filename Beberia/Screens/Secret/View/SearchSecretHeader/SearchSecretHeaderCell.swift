//
//  SearchSecretHeaderCell.swift
//  Beberia
//
//  Created by OS on 12/31/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class SearchSecretHeaderCell: UITableViewHeaderFooterView {


    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnCategory: UIButton!
    
    var tapCategory: (_ index:Int) -> () = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    @IBAction func btnCategory(_ sender: Any) {
        tapCategory(btnCategory.tag)
    }
    
    
}
