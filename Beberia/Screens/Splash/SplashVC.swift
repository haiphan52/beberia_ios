
//
//  
//  SplashVC.swift
//  Beberia
//
//  Created by haiphan on 29/03/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift
import SwiftKeychainWrapper
import Kingfisher

class SplashVC: UIViewController {
    
    enum ImageStatus: Int, CaseIterable {
        case logo, bg
    }
    
    // Add here outlets
    @IBOutlet var imgs: [UIImageView]!
    
    // Add here your view model
    private var viewModel: SplashVM = SplashVM()
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + ConstantApp.shared.timeSplashScreen) {
            self.checkLoginLaunchApp()
        }
    }
    
}
extension SplashVC {
    
    private func setupUI() {
        // Add here the setup for the UI
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        self.viewModel.splashScreens.asObservable().bind { [weak self] list in
            guard let wSelf = self else { return }
            list.enumerated().forEach { item in
                let element = item.element
                let offset = item.offset
                if let type = ImageStatus.init(rawValue: offset) {
                    switch type {
                    case .logo:
                        if let url = URL.init(string: element.link ?? "") {
                            KingfisherManager.shared.retrieveImage(with: url) { result in
                                do {
                                    let image = try result.get().image
                                    wSelf.imgs[offset].image = image.resizeImageSpace(width: UIScreen.main.bounds.size.width - 30)
                                } catch {
                                    print(error)
                                }
                            }
                        }
                        
                    case .bg: wSelf.imgs[offset].kf.setImage(with: URL.init(string: element.link ?? ""))
                    }
                }
            }
        }.disposed(by: self.disposeBag)
    }
    
    private func checkLoginLaunchApp() {
        // Check Frist Install App
        if !UserDefaults.standard.bool(forKey: UserDefault.KeyFristInstallApp){
            // Frist Install App
            let intro = IntroViewController.init(nib: R.nib.introViewController)
//            Utils.getAppDelegate().window = UIWindow(frame: UIScreen.main.bounds)
//            Utils.getAppDelegate().window?.rootViewController = intro
//            Utils.getAppDelegate().window?.makeKeyAndVisible()
            self.navigationController?.pushViewController(intro, completion: nil)
            // Remove Keychain items
            KeychainWrapper.standard.removeObject(forKey: KeychainKeys.isLoggedIn.rawValue)
            KeychainWrapper.standard.removeObject(forKey: KeychainKeys.userName.rawValue)
            KeychainWrapper.standard.removeObject(forKey: KeychainKeys.passWord.rawValue)
            
            UserDefaults.standard.set(true, forKey: UserDefault.KeyFristInstallApp)
            ManageApp.shared.hasInstall = false
        }else{
            ManageApp.shared.hasInstall = true
            if let tabBarVC = R.storyboard.main.tabBarVC() {
//                Utils.getAppDelegate().window = UIWindow(frame: UIScreen.main.bounds)
//                Utils.getAppDelegate().window?.rootViewController = tabBarVC
//                Utils.getAppDelegate().window?.makeKeyAndVisible()
//                tabBarVC.selectedIndex = 1
                self.navigationController?.pushViewController(tabBarVC, completion: nil)
            }
        }
    }
}
