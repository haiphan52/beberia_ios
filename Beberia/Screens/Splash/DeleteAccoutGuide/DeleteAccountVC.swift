
//
//  
//  DeleteAccountVC.swift
//  Beberia
//
//  Created by haiphan on 09/07/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift
import WebKit
import MessageUI

class DeleteAccountVC: UIViewController {
    
    // Add here outlets
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var btPolicy: UIButton!
    @IBOutlet weak var btnGotIt: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    // Add here your view model
    private var viewModel: DeleteAccountVM = DeleteAccountVM()
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
}
extension DeleteAccountVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        self.webView.navigationDelegate = self
        self.btPolicy.contentHorizontalAlignment = .left
        self.btPolicy.isHidden = false
        self.btnGotIt.isEnabled = false
        self.btnGotIt.backgroundColor = R.color.neutralgray4()
        if let url = URL(string: "https://api.beberia.me/privacy") {
          let urlRequest = URLRequest(url: url)
          self.webView.load(urlRequest)
        }
        let tapGes: UITapGestureRecognizer = UITapGestureRecognizer()
        self.contentView.addGestureRecognizer(tapGes)
        tapGes.rx.event.bind { [weak self] _ in
            guard let self = self else { return }
            self.dismiss(animated: true)
        }.disposed(by: self.disposeBag)
    }
    
    private func setupRX() {
      self.btPolicy.rx.tap.bind { [weak self] in
        guard let self = self else { return }
        if self.btPolicy.isSelected {
          self.btPolicy.isSelected = false
          self.btnGotIt.isEnabled = false
          self.btPolicy.setImage(R.image.img_unselect_button_pregnant(), for: .normal)
          self.btnGotIt.backgroundColor = R.color.neutralgray4()
        } else {
          self.btPolicy.isSelected = true
          self.btnGotIt.isEnabled = true
          self.btPolicy.setImage(R.image.img_select_button_pregnant(), for: .normal)
          self.btnGotIt.backgroundColor = R.color.feColor()
        }
      }.disposed(by: self.disposeBag)
        
        self.btnGotIt.rx.tap.bind { [weak self] in
            guard let self = self else { return }
            self.sendEmail()
        }.disposed(by: self.disposeBag)
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["Beberia01@gmail.com"])
            mail.setSubject("Yêu cầu chấm dứt tài khoản")
            mail.setMessageBody("<p>Nếu bạn muốn xoá account vui lòng gửi Họ và tến (nick name) cho chúng tôi, chúng tôi sẽ xác nhận và liên hệ cho bạn</p>", isHTML: true)

            present(mail, animated: true)
        } else {
            self.showAlert(title: nil, message: "Email chưa được thiết lập")
        }
    }
}
extension DeleteAccountVC: WKNavigationDelegate {
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
      let js = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='300%'"//dual size
      webView.evaluateJavaScript(js, completionHandler: nil)
  }
}
extension DeleteAccountVC: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
