
//
//  ___HEADERFILE___
//
import Foundation
import RxCocoa
import RxSwift

class SplashVM {
    
    let splashScreens: PublishSubject<[SplashScreen]> = PublishSubject.init()
    
    private let disposeBag = DisposeBag()
    init() {
        self.start()
    }
    
    private func start() {
        APIManager.splashScreen { [weak self] item in
            guard let wSelf = self, let list = item.list else { return }
            wSelf.splashScreens.onNext(list)
        } failed: { message in
            print(message)
        }
    }
}
