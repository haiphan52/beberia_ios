//
//  SolashScreenModel.swift
//  Beberia
//
//  Created by haiphan on 29/03/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

// MARK: - DataClass
struct SplashScreensModel: Codable {
    let list: [SplashScreen]?
    enum CodingKeys: String, CodingKey {
        case list
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([SplashScreen].self, forKey: .list)
    }
}

// MARK: - List
struct SplashScreen: Codable {
    let id: Int?
    let link: String?
    let type, status: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, link, type, status
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        link = try values.decodeIfPresent(String.self, forKey: .link)
        type = try values.decodeIfPresent(Int.self, forKey: .type)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
    }
}
