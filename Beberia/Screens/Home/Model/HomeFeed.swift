//
//  HomeFeed.swift
//
//  Created by IMAC on 8/28/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class HomeFeed: NSCoding, Equatable{
    
   public static func ==(lhs: HomeFeed, rhs: HomeFeed) -> Bool{
        return
            lhs.numberLike == rhs.numberLike &&
            lhs.numberComment == rhs.numberComment &&
            lhs.checkSave == rhs.checkSave //&&
          //  lhs.checkLike == rhs.checkLike &&
          //  lhs.checkSave == rhs.checkSave
           
    }
    

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let media = "media"
    static let score = "score"
    static let numberLike = "like_number"
    static let cityName = "city_name"
    static let numberComment = "comment_number"
    static let type = "type"
    static let mediaFirst = "media_first"
    static let category = "category"
    static let shareItem = "share_item"
    static let content = "content"
    static let userCreate = "user_create"
    static let id = "id"
    static let created = "created"
    static let about = "about"
    static let checkYourFeed = "check_your_feed"
    static let title = "title"
    static let checkLike = "is_liked"
    static let checkSave = "check_save"
    static let updated = "updated"
    static let userUpdate = "user_update"
    static let isPrivate = "is_private"
    static let turnOn = "turnOn"
    static let owner_post_is_watching = "owner_post_is_watching"
    static let ads = "ads"
    static let age_range = "age_range"
    static let cover = "cover"
    static let district = "district"
    static let subTitle = "subTitle"
  }

  // MARK: Properties
    public var media: [Media]?
    public var cover: [Media]?
    public var score: Float?
    public var numberLike: Int?
    public var cityName: CityName?
    public var district: DistrictModel?
    public var numberComment: Int?
    public var type: Int?
    public var mediaFirst: Media?
    public var category: Int?
    public var shareItem: [Any]?
    public var content: String?
    public var userCreate: User?
    public var id: Int?
    public var created: Int?
    public var about: Int?
    public var checkYourFeed: Int?
    public var title: String?
    public var checkLike: Int?
    public var checkSave: Int?
    public var updated: Int?
    public var userUpdate: User?
    public var isPrivate: Int?
    public var turnOn: Int?
    public var owner_post_is_watching: Int?
    public var ads: Int?
    public var age_range: Int?
    public var subTitle: Int?
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.media].array { media = items.map { Media(json: $0) } }
    if let items = json[SerializationKeys.cover].array { cover = items.map { Media(json: $0) } }
    score = json[SerializationKeys.score].float
    numberLike = json[SerializationKeys.numberLike].int
    subTitle = json[SerializationKeys.subTitle].int
    cityName = CityName(json: json[SerializationKeys.cityName])
    district = DistrictModel(json: json[SerializationKeys.district])
    numberComment = json[SerializationKeys.numberComment].int
    type = json[SerializationKeys.type].int
    turnOn = json[SerializationKeys.turnOn].int
    owner_post_is_watching = json[SerializationKeys.owner_post_is_watching].int
    mediaFirst = Media(json: json[SerializationKeys.mediaFirst])
    category = json[SerializationKeys.category].int
    if let items = json[SerializationKeys.shareItem].array { shareItem = items.map { $0.object} }
    content = json[SerializationKeys.content].string
    userCreate = User(json: json[SerializationKeys.userCreate])
    userUpdate = User(json: json[SerializationKeys.userUpdate])
    id = json[SerializationKeys.id].int
    created = json[SerializationKeys.created].int
    updated = json[SerializationKeys.updated].int
    about = json[SerializationKeys.about].int
    checkYourFeed = json[SerializationKeys.checkYourFeed].int
    title = json[SerializationKeys.title].string
    checkLike = json[SerializationKeys.checkLike].int
    checkSave = json[SerializationKeys.checkSave].int
    isPrivate = json[SerializationKeys.isPrivate].int
    ads = json[SerializationKeys.ads].int
    age_range = json[SerializationKeys.age_range].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = media { dictionary[SerializationKeys.media] = value.map { $0.dictionaryRepresentation() } }
    if let value = cover { dictionary[SerializationKeys.cover] = value.map { $0.dictionaryRepresentation() } }
    if let value = score { dictionary[SerializationKeys.score] = value }
    if let value = numberLike { dictionary[SerializationKeys.numberLike] = value }
    if let value = subTitle { dictionary[SerializationKeys.subTitle] = value }
    if let value = cityName { dictionary[SerializationKeys.cityName] = value.dictionaryRepresentation() }
    if let value = district { dictionary[SerializationKeys.district] = value.dictionaryRepresentation() }
    if let value = numberComment { dictionary[SerializationKeys.numberComment] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = turnOn { dictionary[SerializationKeys.turnOn] = value }
    if let value = owner_post_is_watching { dictionary[SerializationKeys.owner_post_is_watching] = value }
    if let value = mediaFirst { dictionary[SerializationKeys.mediaFirst] = value.dictionaryRepresentation() }
    if let value = category { dictionary[SerializationKeys.category] = value }
    if let value = shareItem { dictionary[SerializationKeys.shareItem] = value }
    if let value = content { dictionary[SerializationKeys.content] = value }
    if let value = userCreate { dictionary[SerializationKeys.userCreate] = value.dictionaryRepresentation() }
    if let value = userUpdate { dictionary[SerializationKeys.userUpdate] = value.dictionaryRepresentation() }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = updated { dictionary[SerializationKeys.updated] = value }
    if let value = about { dictionary[SerializationKeys.about] = value }
    if let value = checkYourFeed { dictionary[SerializationKeys.checkYourFeed] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = checkLike { dictionary[SerializationKeys.checkLike] = value }
    if let value = checkSave{ dictionary[SerializationKeys.checkSave] = value }
    if let value = isPrivate{ dictionary[SerializationKeys.isPrivate] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.media = aDecoder.decodeObject(forKey: SerializationKeys.media) as? [Media]
    self.score = aDecoder.decodeObject(forKey: SerializationKeys.score) as? Float
    self.numberLike = aDecoder.decodeObject(forKey: SerializationKeys.numberLike) as? Int
    self.cityName = aDecoder.decodeObject(forKey: SerializationKeys.cityName) as? CityName
     self.district = aDecoder.decodeObject(forKey: SerializationKeys.district) as? DistrictModel
    self.numberComment = aDecoder.decodeObject(forKey: SerializationKeys.numberComment) as? Int
    self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? Int
    self.turnOn = aDecoder.decodeObject(forKey: SerializationKeys.turnOn) as? Int
    self.owner_post_is_watching = aDecoder.decodeObject(forKey: SerializationKeys.owner_post_is_watching) as? Int
    self.mediaFirst = aDecoder.decodeObject(forKey: SerializationKeys.mediaFirst) as? Media
    self.category = aDecoder.decodeObject(forKey: SerializationKeys.category) as? Int
    self.shareItem = aDecoder.decodeObject(forKey: SerializationKeys.shareItem) as? [Any]
    self.content = aDecoder.decodeObject(forKey: SerializationKeys.content) as? String
    self.userCreate = aDecoder.decodeObject(forKey: SerializationKeys.userCreate) as? User
    self.userUpdate = aDecoder.decodeObject(forKey: SerializationKeys.userUpdate) as? User
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.created = aDecoder.decodeObject(forKey: SerializationKeys.created) as? Int
    self.updated = aDecoder.decodeObject(forKey: SerializationKeys.updated) as? Int
    self.about = aDecoder.decodeObject(forKey: SerializationKeys.about) as? Int
    self.checkYourFeed = aDecoder.decodeObject(forKey: SerializationKeys.checkYourFeed) as? Int
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.checkLike = aDecoder.decodeObject(forKey: SerializationKeys.checkLike) as? Int
    self.checkSave = aDecoder.decodeObject(forKey: SerializationKeys.checkSave) as? Int
    self.isPrivate = aDecoder.decodeObject(forKey: SerializationKeys.isPrivate) as? Int
    self.subTitle = aDecoder.decodeObject(forKey: SerializationKeys.subTitle) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(media, forKey: SerializationKeys.media)
    aCoder.encode(score, forKey: SerializationKeys.score)
    aCoder.encode(numberLike, forKey: SerializationKeys.numberLike)
    aCoder.encode(cityName, forKey: SerializationKeys.cityName)
    aCoder.encode(district, forKey: SerializationKeys.district)
    aCoder.encode(numberComment, forKey: SerializationKeys.numberComment)
    aCoder.encode(type, forKey: SerializationKeys.type)
    aCoder.encode(turnOn, forKey: SerializationKeys.turnOn)
    aCoder.encode(owner_post_is_watching, forKey: SerializationKeys.owner_post_is_watching)
    aCoder.encode(mediaFirst, forKey: SerializationKeys.mediaFirst)
    aCoder.encode(category, forKey: SerializationKeys.category)
    aCoder.encode(shareItem, forKey: SerializationKeys.shareItem)
    aCoder.encode(content, forKey: SerializationKeys.content)
    aCoder.encode(userCreate, forKey: SerializationKeys.userCreate)
    aCoder.encode(userUpdate, forKey: SerializationKeys.userUpdate)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(created, forKey: SerializationKeys.created)
    aCoder.encode(updated, forKey: SerializationKeys.updated)
    aCoder.encode(about, forKey: SerializationKeys.about)
    aCoder.encode(checkYourFeed, forKey: SerializationKeys.checkYourFeed)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(checkLike, forKey: SerializationKeys.checkLike)
    aCoder.encode(checkSave, forKey: SerializationKeys.checkSave)
    aCoder.encode(isPrivate, forKey: SerializationKeys.isPrivate)
    aCoder.encode(subTitle, forKey: SerializationKeys.subTitle)
  }

}
