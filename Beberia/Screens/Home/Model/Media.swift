//
//  Media.swift
//
//  Created by IMAC on 8/28/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Media: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let descriptionValue = "description"
    static let link = "link"
    static let id = "id"
    static let type = "type"
    static let thumbnail = "thumbnail"
    static let createdAt = "created_at"
  }

  // MARK: Properties
  public var descriptionValue: String?
  public var link: String?
  public var id: Int?
  public var type: Int?
  public var thumbnail: String?
  public var createdAt: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    descriptionValue = json[SerializationKeys.descriptionValue].string
    link = json[SerializationKeys.link].string
    id = json[SerializationKeys.id].int
    type = json[SerializationKeys.type].int
    thumbnail = json[SerializationKeys.thumbnail].string
    createdAt = json[SerializationKeys.createdAt].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = link { dictionary[SerializationKeys.link] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = thumbnail { dictionary[SerializationKeys.thumbnail] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
    self.link = aDecoder.decodeObject(forKey: SerializationKeys.link) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? Int
    self.thumbnail = aDecoder.decodeObject(forKey: SerializationKeys.thumbnail) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
    aCoder.encode(link, forKey: SerializationKeys.link)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(type, forKey: SerializationKeys.type)
    aCoder.encode(thumbnail, forKey: SerializationKeys.thumbnail)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
  }

}
