//
//  CheckTimeModel.swift
//  Beberia
//
//  Created by haiphan on 20/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

struct CheckTimeModel: Codable {
    let remain: Double?
    
    enum CodingKeys: String, CodingKey {
        case remain
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        remain = try values.decodeIfPresent(Double.self, forKey: .remain)
    }
    
}
