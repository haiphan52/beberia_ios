//
//  SelectCategoryVC.swift
//  Beberia
//
//  Created by IMAC on 10/30/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import TTGTagCollectionView
import SVProgressHUD

class SelectCategoryVC: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var viewButtonThanhLy: UIView!
    @IBOutlet weak var viewButtonHoiDap: UIView!
    @IBOutlet weak var viewButtonThongTin: UIView!
    @IBOutlet weak var viewButtonMe: UIView!
    @IBOutlet weak var viewButtonBe: UIView!
    @IBOutlet weak var viewBe: TTGTextTagCollectionView!
    @IBOutlet weak var viewMe: TTGTextTagCollectionView!
    @IBOutlet weak var imgThanhLy: UIImageView!
    @IBOutlet weak var imgHoiDap: UIImageView!
    @IBOutlet weak var imgThongTin: UIImageView!
    @IBOutlet weak var imgBe: UIImageView!
    @IBOutlet weak var imgMe: UIImageView!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var imgIconLocation: UIImageView!
    @IBOutlet weak var viewDistric: UIView!
    @IBOutlet weak var btnDistric: UIButton!
    @IBOutlet weak var imgDistric: UIImageView!
    @IBOutlet weak var lblErrorTopic: UILabel!
    @IBOutlet weak var lblErrorType: UILabel!
    @IBOutlet weak var lblMe: UILabel!
    @IBOutlet weak var lblThanhLy: UILabel!
    @IBOutlet weak var lblHoiDap: UILabel!
    @IBOutlet weak var lblThongTin: UILabel!
    @IBOutlet weak var lblBe: UILabel!
    @IBOutlet weak var lblErrorDistric: UILabel!
    
    // MARK: - Properties
    
    var contentSreach = ""
    var stringImage = ""
    var titlePost = ""
    var comtentHTML = ""
    var locationFeed = CityName.init(json: "")
    var districtModel = DistrictModel.init(json: "")
    var typeCategory = -1
    var typeAbout = -1
    var idFeed = 0
    var newMedia = 0
    var ageRange = -1
    var tagButtonCategoryInput = -1
    var tagButtonAboutInput = -1
    var editFromView = EditFromView.Home
    var fromView = FromView.Home
    var locationOther = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupTagView()
        setupData()
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        title = R.string.localizable.homeWritePost()
        customLeftBarButton()
        let rightBarButtonItem = UIBarButtonItem.init(title: R.string.localizable.homePost(), style: .plain, target: self, action: #selector(SelectCategoryVC.createHomeFeed))
        rightBarButtonItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "fd799d") as Any,
                                                   NSAttributedString.Key.font: UIFont.init(name: AppFont.NotoSansBold, size: 18) as Any], for: .normal)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        setColorBtnLocation()
        
        if locationFeed.locationNameVi == nil{
            print("1212")
        }
        
        btnLocation.setTitle(locationFeed.locationNameVi == nil ? UserInfo.shareUserInfo.city_name.locationNameVi : locationFeed.locationNameVi , for: .normal)
        self.setupColorButton(districtName: districtModel.districtName == nil ? UserInfo.shareUserInfo.district.districtName ?? "" : districtModel.districtName ?? "")
        
        lblErrorTopic.isHidden = true
        lblErrorType.isHidden = true
        lblErrorDistric.isHidden = true
        viewBe.isHidden = true
        viewMe.isHidden = true
        
        
    }
    
    override func setupTagView(data: [String], tagView: TTGTextTagCollectionView){
        //        tagView.addTags([locations![0].locationNameVi, locations![1].locationNameVi])
        tagView.addTags(data)
        let config = tagView.defaultConfig!
        config.textColor = UIColor.init(hexString: "ffffff")
        config.selectedTextColor = UIColor.init(hexString: "ffffff")
        config.backgroundColor = UIColor.init(hexString: "81a1ff",alpha: 0.35)
        config.selectedBackgroundColor = UIColor.init(hexString: "81a1ff",alpha: 0.79)
        config.cornerRadius = 4
        config.selectedCornerRadius = 4
        config.shadowOffset = CGSize.zero
        config.shadowRadius = 0
        config.shadowOpacity = 0
        config.exactHeight = 30
        tagView.enableTagSelection = true
        tagView.defaultConfig = config
        //        tagView.delegate = self
        tagView.reload()
    }
    
    func setupTagView(){
        // TagView Loation
        //        tagView.delegate = self
        //        tagView.alignment = .center
        //        setupTagViewLocation()
        // TagView Mother
        setupTagView(data: ["Đang mang thai", "Đã có con"], tagView: viewMe)
        viewMe.alignment = .center
        viewMe.defaultConfig?.exactWidth = 143
        viewMe.delegate = self
        viewMe.contentInset = UIEdgeInsets(top: 18, left: 0, bottom: 0, right: 0)
        viewMe.reload()
        viewMe.isHidden = true
        // TagView Baby
        setupTagView(data: ["~1 tuổi","~1-2 tuổi","~2-3 tuổi","~3-5 tuổi","> 5 tuổi"], tagView: viewBe)
        viewBe.alignment = .left
        let width = viewBe.frame.width / 3 - 25
        viewBe.defaultConfig?.exactWidth = width
        viewBe.contentInset = UIEdgeInsets(top: 22, left: 10, bottom: 0, right: 0)
        viewBe.delegate = self
        
    }
    
    func setColorBtnLocation(){
        btnLocation.setTitleColor(UIColor.white, for: .normal)
        btnDistric.setTitleColor(UIColor.white, for: .normal)
        imgIconLocation.image = R.image.ic_ArrowDownYL()
        viewLocation.backgroundColor =  UIColor.init(hexString: "0299fe",alpha: 1)
     //   viewLocation.borderColor = UIColor.init(hexString: AppColor.colorYellow,alpha: 0.5)
     //   viewLocation.borderWidth = 1
        viewLocation.cornerRadius = 4

        viewDistric.backgroundColor =  UIColor.init(hexString: "fd799d",alpha: 1)
        viewDistric.cornerRadius = 4
    }
    
    func setAlphaCategoryButton(tag: Int){
        setupUIButton(lable: lblThongTin, img: imgThongTin, active: false, viewSelected: viewButtonThongTin)
        setupUIButton(lable: lblHoiDap, img: imgHoiDap, active: false, viewSelected: viewButtonHoiDap)
        setupUIButton(lable: lblThanhLy, img: imgThanhLy, active: false, viewSelected: viewButtonThanhLy)
        if tag == 301{
            viewButtonThongTin.alpha = 1
            setupUIButton(lable: lblThongTin, img: imgThongTin, active: true, viewSelected: viewButtonThongTin)
            typeCategory = 2
        }
        if tag == 302{
            viewButtonHoiDap.alpha = 1
            setupUIButton(lable: lblHoiDap, img: imgHoiDap, active: true, viewSelected: viewButtonHoiDap)
            typeCategory = 1
        }
        if tag == 303{
            viewButtonThanhLy.alpha = 1
            setupUIButton(lable: lblThanhLy, img: imgThanhLy, active: true , viewSelected: viewButtonThanhLy)
            typeCategory = 3
        }
        
        if typeCategory == -1 {
            lblErrorType.isHidden = false
        }else{
            lblErrorType.isHidden = true
        }
    }
    
    func setAlphaAboutButton(tag: Int){
        
        ageRange = -1
        
        viewMe.setTagAt(0, selected: false)
        viewMe.setTagAt(1, selected: false)
        
        viewBe.setTagAt(0, selected: false)
        viewBe.setTagAt(1, selected: false)
        viewBe.setTagAt(2, selected: false)
        viewBe.setTagAt(3, selected: false)
        viewBe.setTagAt(4, selected: false)
        viewBe.setTagAt(5, selected: false)
        
        setupUIButton(lable: lblBe, img: imgBe, active: false, viewSelected: viewButtonBe)
        setupUIButton(lable: lblMe, img: imgMe, active: false, viewSelected: viewButtonMe)
        
        if tag == 304{
            setupUIButton(lable: lblMe, img: imgMe, active: true, viewSelected: viewButtonMe)
            
        }
        if tag == 305{
            setupUIButton(lable: lblBe, img: imgBe, active: true, viewSelected: viewButtonBe)
        }
        
    }
    
    func setupUIButton(lable: UILabel, img : UIImageView, active: Bool, viewSelected: UIView){
        lable.textColor = active ? UIColor.white : UIColor.init(hexString: "cecece")
        lable.alpha = active ? 1 : 0.5
        img.alpha = active ? 1 : 0.5
        viewSelected.layer.borderWidth = 1
        viewSelected.backgroundColor = active ? UIColor.init(hexString: "fd799d") : UIColor.init(hexString: "ffffff")
        viewSelected.layer.borderColor = active ? UIColor.init(hexString: "fd799d")?.cgColor : UIColor.init(hexString: "cecece")?.cgColor
    }
    
    func setupColorButton(districtName:String){
        self.btnDistric.setTitle(districtName, for: .normal)
        if districtName == "" {
            Utils.districtColorDefault(btn: self.btnDistric, img: self.imgDistric, view: self.viewDistric)
        }
    }
    
    // MARK: - SetupData
    
    func setupData(){
        // setup Info
        switch typeCategory {
        case 1:
            tagButtonCategoryInput = 302
        case 2:
            tagButtonCategoryInput = 301
        case 3:
            tagButtonCategoryInput = 303
        default:
            print("typeInfo")
        }
        setAlphaCategoryButton(tag: tagButtonCategoryInput)
        
        // setup Topic
        if typeAbout == 1{
            viewBe.isHidden = false
            viewMe.isHidden = true
            tagButtonAboutInput = 305
        }
        if typeAbout == 2{
            viewBe.isHidden = true
            viewMe.isHidden = false
            tagButtonAboutInput = 304
        }
        setAlphaAboutButton(tag: tagButtonAboutInput)
        
        switch ageRange {
        case 0:
            viewMe.setTagAt(0, selected: true)
        case 1:
            viewBe.setTagAt(0, selected: true)
        case 2:
            viewBe.setTagAt(1, selected: true)
        case 3:
            viewBe.setTagAt(2, selected: true)
        case 4:
            viewBe.setTagAt(3, selected: true)
        case 5:
            viewBe.setTagAt(4, selected: true)
        case 6:
            viewMe.setTagAt(1, selected: true)
        default:
            print(ageRange)
        }
    }
    
    //MARK: - Helper methods
    
    @objc func createHomeFeed(){

        
        switch fromView {
        case .Home:
            createPost()
        case .EditHome:
            editFeed()
        case .Diary:
            print("Diary")
        case .EditDiary:
            print("EditDiary")
        case .Secret:
            print("Secret")
        }
    }
    
    func pushViewLocation(){
        let locationVC = LocationViewController.init(nib: R.nib.locationViewController)
        locationVC.didSelectedLocation = { city in
            self.locationFeed = city
            self.btnLocation.setTitle(city.locationNameVi ?? "", for: .normal)
            self.btnDistric.setTitle(R.string.localizable.commonDistrict(), for: .normal)
//            Utils.districtColorDefault(btn: self.btnDistric, img: self.imgDistric, view: self.viewDistric)
//            self.viewDistric.borderWidth = 1
//            self.viewDistric.cornerRadius = 15
            self.districtModel = DistrictModel.init(json: "")
        }
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    func checkConditionInput() -> Bool{
        // disnable rightBarButtonItem (CLick 1 lần)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        if typeAbout == -1 ||  ageRange == -1{
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            lblErrorTopic.isHidden = false
            return false
        }
        lblErrorTopic.isHidden = true
        
        if typeCategory == -1 {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            lblErrorType.isHidden = false
            return false
        }
        lblErrorType.isHidden = true
        
        if districtModel.districtName == nil {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            lblErrorDistric.isHidden = false
            return false
        }
        lblErrorDistric.isHidden = true
        
        return true
    }
    
    // MARK: - Request API
    
    func createPost(){
        
        if !checkConditionInput() { return }
        
        SVProgressHUD.show()
        APIManager.createHomeFeed(title: titlePost, content: comtentHTML , images: stringImage, videos: "[]", city_code: locationFeed.locationCode ?? 0, about: typeAbout, category: typeCategory, content_search: contentSreach, ageRange: ageRange, district_code: districtModel.districtCode ?? "", subTitle: 0, callbackSuccess: { [weak self] (homeFeed) in
            guard let self = self else { return }
            SVProgressHUD.dismiss()
            NotificationCenter.default.post(name: NSNotification.Name.checkEditHome, object: [Key.KeyNotification.idFeed: homeFeed.id ?? 0, Key.KeyNotification.EditFeed : false], userInfo: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func editFeed(){
        
        if !checkConditionInput() { return }
        
        APIManager.editFeed(id: idFeed, title: titlePost, city_code: locationFeed.locationCode ?? 0, content: comtentHTML, images: stringImage, about: typeAbout, category: typeCategory, newMedia: newMedia, contentSearch: contentSreach, ageRange: ageRange, district_code: districtModel.districtCode ?? "", callbackSuccess: { [weak self] (homeFeed) in
            guard let self = self else { return }
            let viewVCs =  self.navigationController?.viewControllers
            switch self.editFromView{
            case .Home :
                NotificationCenter.default.post(name: NSNotification.Name.checkEditHome, object: [Key.KeyNotification.idFeed: homeFeed.id ?? 0], userInfo: nil)
                self.navigationController?.popToRootViewController(animated: true)
            case .Profile:
                NotificationCenter.default.post(name: NSNotification.Name.checkEditHomeProfile, object: [Key.KeyNotification.idFeed: homeFeed.id ?? 0], userInfo: nil)
                self.navigationController?.popToRootViewController(animated: true)
            case .Save:
                for viewVC in viewVCs!{
                    if viewVC.isKind(of: InfoMarkProfileViewController.self){
                        NotificationCenter.default.post(name: NSNotification.Name.checkEditHomeSave, object: [Key.KeyNotification.idFeed: homeFeed.id ?? 0], userInfo: nil)
                        self.navigationController?.popToViewController(viewVC, animated: true)
                    }
                }
            case .Sreach:
                for viewVC in viewVCs!{
                    if viewVC.isKind(of: HomeInfo.self){
                        NotificationCenter.default.post(name: NSNotification.Name.checkEditHomeSreach, object: [Key.KeyNotification.idFeed: homeFeed.id ?? 0], userInfo: nil)
                        self.navigationController?.popToViewController(viewVC, animated: true)
                    }
                }
                
            case .Notification:
                print("Notification")
            case .Secret:
                print("Secret")
            }
            
            SVProgressHUD.dismiss()
            
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    // MARK: - Action
    
    @IBAction func btnLocation(_ sender: Any) {
        pushViewLocation()
    }
    
    @IBAction func btnDistric(_ sender: Any) {
        let districVC = DistrictViewController.init(nib: R.nib.districtViewController)
        districVC.locationID = locationFeed.locationCode ?? 0
        districVC.isHaveDistric = false
        districVC.hidesBottomBarWhenPushed = true
        districVC.didSelectedLocation = { city in
            self.districtModel = city
            self.setupColorButton(districtName: city.districtName ?? "")
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(districVC, animated: true)
    }
    
    @IBAction func didPressThanhLy(_ sender: UIButton) {
        setAlphaCategoryButton(tag: sender.tag)
        
    }
    
    @IBAction func didPressHoiDap(_ sender: UIButton) {
        setAlphaCategoryButton(tag: sender.tag)
        
    }
    
    @IBAction func didPressThongTin(_ sender: UIButton) {
        setAlphaCategoryButton(tag: sender.tag)
        
    }
    
    @IBAction func didPressBe(_ sender: UIButton) {
        setAlphaAboutButton(tag: sender.tag)
        viewBe.isHidden = false
        viewMe.isHidden = true
        typeAbout = -1
        ageRange = -1
    }
    
    @IBAction func didPressMe(_ sender: UIButton) {
        setAlphaAboutButton(tag: sender.tag)
        viewBe.isHidden = true
        viewMe.isHidden = false
        typeAbout = -1
        ageRange = -1
    }
}

// MARK: - TTGTextTagCollectionViewDelegate

extension SelectCategoryVC: TTGTextTagCollectionViewDelegate{
    func textTagCollectionView(_ textTagCollectionView: TTGTextTagCollectionView!, didTapTag tagText: String!, at index: UInt, selected: Bool, tagConfig config: TTGTextTagConfig!) {
        
        if textTagCollectionView == viewMe{
            viewMe.setTagAt(0, selected: false)
            viewMe.setTagAt(1, selected: false)
            viewMe.setTagAt(index, selected: true)
            ageRange = index == 0 ? 0 : 6
            typeAbout = 2
        }
        if textTagCollectionView == viewBe {
            viewBe.setTagAt(0, selected: false)
            viewBe.setTagAt(1, selected: false)
            viewBe.setTagAt(2, selected: false)
            viewBe.setTagAt(3, selected: false)
            viewBe.setTagAt(4, selected: false)
            viewBe.setTagAt(5, selected: false)
            viewBe.setTagAt(index, selected: true)
            ageRange = Int(index + 1)
            typeAbout = 1
        }
        
        if typeAbout == -1 ||  ageRange == -1{
            lblErrorTopic.isHidden = false
        }else{
            lblErrorTopic.isHidden = true
        }
        
    }
}
