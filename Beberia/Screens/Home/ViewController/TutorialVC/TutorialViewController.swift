//
//  TutorialViewController.swift
//  Beberia
//
//  Created by IMAC on 2/25/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

    @IBOutlet weak var imageTutorial: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                imageTutorial.image = R.image.tutorial()
            default:
                print("Unknown")
                imageTutorial.image = R.image.tutorialX()
            }
        }
    }

    @IBAction func didPressTapImage(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
