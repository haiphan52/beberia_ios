//
//  LocationViewController.swift
//  Beberia
//
//  Created by OS on 11/2/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll
import RxCocoa
import RxSwift

class LocationViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tbvLocation: UITableView!
    
    // MARK: - Properties
    
    var arrayLocations = [CityName]()
    var page = 1
    var paginate = 20
    var key = ""
    var didSelectedLocation:(_ location: CityName)->() = {_ in}
    var isCheckHome = false
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customLeftBarButton()
        title = "Danh sách các tỉnh thành phố"
        setupUI()
        getLocation(page: 1, key: "")
        
        txtSearch.rx.text.orEmpty.asObservable().skip(1).distinctUntilChanged().debounce(.milliseconds(500), scheduler: MainScheduler.asyncInstance).subscribe { (text) in
            self.page = 1
            self.arrayLocations = [CityName]()
            self.key = text.element ?? ""
            self.getLocation(page: self.page, key: self.key)
        }.disposed(by: disposeBag)
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        self.extendedLayoutIncludesOpaqueBars = true
        tbvLocation.register(R.nib.locationTableViewCell)
        tbvLocation.delegate = self
        tbvLocation.dataSource = self
        tbvLocation.separatorStyle = .none
        tbvLocation.infiniteScrollTriggerOffset = 700
        
        tbvLocation.showsVerticalScrollIndicator = false
        tbvLocation.addInfiniteScroll { (tableView) in
            tableView.performBatchUpdates({ () -> Void in
                // update collection view
                if self.page != 0{
                    //  self.page = self.page + 1
                    self.getLocation(page: self.page, key: self.txtSearch.text ?? "")
                }else{
                    self.tbvLocation.endRefreshing(at: .top)
                    self.tbvLocation.finishInfiniteScroll(completion: { (collection) in
                    })
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                tableView.finishInfiniteScroll()
            });
        }
        
        txtSearch.delegate = self
    }
    
    // MARK: - Request API
    
    func getLocation(page:Int, key: String){
        APIManager.getLocation(page: page, paginate: paginate, key: key, callbackSuccess: { [weak self] (arrayLocations, page) in
            guard let self = self else { return }
            if self.arrayLocations.count == 0 {
                self.arrayLocations = arrayLocations
                
                if self.isCheckHome {
                    let allProvide = CityName.init()
                    allProvide.id = 1000
                    allProvide.locationCode = 0
                    allProvide.locationNameVi = "Toàn quốc"
                    
                    self.arrayLocations.insert(allProvide, at: 0)
                }
                
            }else{
                
                self.arrayLocations.append(contentsOf: arrayLocations)
            }
            self.tbvLocation.endRefreshing(at: .top)
            self.tbvLocation.finishInfiniteScroll(completion: { (collection) in
            })
            self.page = page
            self.tbvLocation.reloadData()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
}

// MARK: - UITableViewDelegate

extension LocationViewController: UITableViewDelegate,  UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayLocations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell", for: indexPath) as! LocationTableViewCell
        cell.lblName.text = self.arrayLocations[indexPath.row].locationNameVi ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectedLocation(self.arrayLocations[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITextViewDelegate

extension LocationViewController: UITextFieldDelegate{
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        page =  1
//        paginate =  20
//        arrayLocations = [CityName]()
//        getLocation(page: page, key: textField.text ?? "")
//        return true
//    }
//
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     //h   key = textField.text ?? ""
        return true
    }
}
