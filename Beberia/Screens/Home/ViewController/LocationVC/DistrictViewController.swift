//
//  LocationViewController.swift
//  Beberia
//
//  Created by OS on 11/2/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll

class DistrictViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tbvDistrict: UITableView!
    
    // MARK: - Properties
    
    var arrayLocations = [DistrictModel]()
    var page = 1
    var locationID = 0
    var key = ""
    var type = 0
    var didSelectedLocation:(_ location: DistrictModel)->() = {_ in}
    var tapType: (_ index :Int) -> () = {_ in}
    var dataInfo = CategoryFeed.arrayList
    var dataCategory = ["Tất cả","Gia đình","Cuộc sống","Con cái","Bóc phốt","Khác"]
    let jsonObject: [String: Any] = [
        "district_code": "",
        "id": 0,
        "district_name": "Quận"
    ]
    var isHaveDistric = true
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customLeftBarButton()
//        title = "Danh sách các quận"
        setupUI()
        getDictrict(locationID: locationID, key: "")
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        self.extendedLayoutIncludesOpaqueBars = true
        tbvDistrict.register(R.nib.locationTableViewCell)
        tbvDistrict.delegate = self
        tbvDistrict.dataSource = self
        tbvDistrict.separatorStyle = .none
        tbvDistrict.infiniteScrollTriggerOffset = 700
        tbvDistrict.showsVerticalScrollIndicator = false
        tbvDistrict.addInfiniteScroll { (tableView) in
            tableView.performBatchUpdates({ () -> Void in
                // update collection view
                if self.page != 0{
                    self.page = self.page + 1
                    self.getDictrict(locationID: self.locationID, key: self.txtSearch.text ?? "")
                }else{
                    self.tbvDistrict.endRefreshing(at: .top)
                    self.tbvDistrict.finishInfiniteScroll(completion: { (collection) in
                    })
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                tableView.finishInfiniteScroll()
            });
        }
        
        txtSearch.delegate = self
    }
    
    // MARK: - Request API
    
    func getDictrict(locationID:Int, key: String){
        APIManager.getDistrict(page: page,location: locationID, key: key, callbackSuccess: { [weak self] (arrayLocations, page) in
            guard let self = self else { return }
            if self.arrayLocations.count == 0 {
                self.arrayLocations = arrayLocations
                if self.isHaveDistric { self.arrayLocations.insert(DistrictModel(object: self.jsonObject), at: 0) }
                
            }else{
                
                self.arrayLocations.append(contentsOf: arrayLocations)
            }
            self.tbvDistrict.endRefreshing(at: .top)
            self.tbvDistrict.finishInfiniteScroll(completion: { (collection) in
            })
            
            self.page = page
            self.tbvDistrict.reloadData()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
}

// MARK: - UITableViewDelegate

extension DistrictViewController: UITableViewDelegate,  UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch type {
        case 0:
             return self.arrayLocations.count
        case 1 :
             return self.dataInfo.count
        default:
             return self.dataCategory.count
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell", for: indexPath) as! LocationTableViewCell
        switch type {
        case 0:
             cell.lblName.text = self.arrayLocations[indexPath.row].districtName ?? ""
        case 1:
             cell.lblName.text = self.dataInfo[indexPath.row]
        default:
             cell.lblName.text = self.dataCategory[indexPath.row]
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.popViewController(animated: true)
        switch type {
        case 0:
             didSelectedLocation(self.arrayLocations[indexPath.row])
        case 1:
             tapType(indexPath.row)
        default:
            tapType(indexPath.row)
        }
    }
}

// MARK: - UITextViewDelegate

extension DistrictViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        page =  1
        //   paginate =  20
        arrayLocations = [DistrictModel]()
        getDictrict(locationID: locationID, key: textField.text ?? "")
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        key = textField.text ?? ""
        return true
    }
}
