//
//  PagingListBaseView.swift
//  Beberia
//
//  Created by iMAC on 9/3/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftEntryKit
import PullToRefresh

@objc public class PagingListBaseView: UIView {

  //  @IBOutlet var sadasd: UITableView!
    //MARK: - Properties
    @objc public var tableview: UITableView!
    @objc public var dataSource: [String]?
    var arraySearchHome = [HomeFeed]()
    var arraySearchDiary = [Diaries]()
    var arraySearchInfo = [PostDetailModel]()
    var key = ""
    var gettype = ""
    var page = 1
    var nextpage = 0
    let refresher = PullToRefresh()
    var dataInfo = CategoryFeed.arrayList
    var tokenPage: String = ""
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    private var isHeaderRefreshed: Bool = false
    deinit {
        listViewDidScrollCallback = nil
    }
    var lastIndex: Int = -1
    var cityCode = 0
    var category = 0
    var districtCode = ""
    var cellHeightsDictionary: [IndexPath: CGFloat] = [:]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        tableview = UITableView(frame: self.frame, style: .plain)
        tableview.backgroundColor = UIColor.white
        tableview.tableFooterView = UIView()
      
        tableview.dataSource = self
        tableview.delegate = self
        tableview.separatorStyle = .none
        tableview.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 80,right: 0)
        tableview.register(R.nib.homeNTableViewCell)
        tableview.register(R.nib.diaryNewTableViewCell)
        tableview.register(R.nib.detailInfoTableViewCell)
        tableview.register(R.nib.homeTableViewCell)
        tableview.register(R.nib.homeQCTableViewCell)
        tableview.registerHeaderFooterView(R.nib.headerFilterTableViewCell)
        tableview.showsVerticalScrollIndicator = false

        addSubview(tableview)
        
     
        NotificationCenter.default.addObserver(self, selector: #selector(checkEditFeedSreach), name: NSNotification.Name.checkEditHomeSreach, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkEditFeedDiarySreach), name: NSNotification.Name.checkEditDiarySreach, object: nil)
    }
    
    func beginFirstRefresh() {
        if !isHeaderRefreshed {
            self.isHeaderRefreshed = true
            self.tableview.reloadData()
        }
    }
    
    //MARK: - @objc
    @objc func checkEditFeedSreach(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        let idFeed = object[Key.KeyNotification.idFeed] as? Int
        self.getCheckCompleteFeed(id: idFeed ?? 0)
    }
    
    @objc func checkEditFeedDiarySreach(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        let idFeed = object[Key.KeyNotification.idDiary] as? Int
        self.getCheckCompleteDiary(id: idFeed ?? 0)
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        tableview.frame = self.bounds
    }
    
    //MARK: - Request API
    func getSearchHomeList(key:String,page:Int){
        //    SVProgressHUD.show()
        APIManager.getSearchHome(key:key, page: page, cityCode: cityCode, category: category, districtCode: districtCode, tokenPage: self.tokenPage,  callbackSuccess: { [weak self] (Searchhome, tokenPage) in
               guard let self = self else { return }
            self.tokenPage = tokenPage
//                self.nextpage = next_page
                if self.page == 1{
                    self.arraySearchHome.removeAll()
                }
                if self.arraySearchHome.count == 0 {
                    self.arraySearchHome = Searchhome
                }else{
                    self.arraySearchHome.append(contentsOf: Searchhome)
                }
            
                self.tableview.reloadData()
                self.tableview.endRefreshing(at: .top)
                self.tableview.finishInfiniteScroll(completion: { (collection) in
                })
                SVProgressHUD.dismiss()
            }) { (error) in
                SVProgressHUD.dismiss()
            }
        }
    
    func getSearchDiaryList(key:String,gettype:Int,page:Int){
      //  SVProgressHUD.show()
        APIManager.getSearchDiary(key: key, type: gettype, page: page, callbackSuccess: { [weak self] (searchDiary,next_page) in
            guard let self = self else { return }
            self.nextpage = next_page
            if self.page == 1{
                self.arraySearchDiary.removeAll()
            }
            if self.arraySearchDiary.count == 0 {
                self.arraySearchDiary = searchDiary
            }else{
                self.arraySearchDiary.append(contentsOf: searchDiary)
            }
            self.tableview.reloadData()
            self.tableview.endRefreshing(at: .top)
            self.tableview.finishInfiniteScroll(completion: { (collection) in
            })
          //  SVProgressHUD.dismiss()
        }, failed: { (error) in
             SVProgressHUD.dismiss()
        })
    }
    func getSearchInfoList(key:String,gettype:Int,page:Int){
      //  SVProgressHUD.show()
        APIManager.getSearchInfo(key: key, type: gettype, page: page, callbackSuccess: { [weak self] (searchInfo,next_page) in
            guard let self = self else { return }
            self.nextpage = next_page
            if self.page == 1{
                self.arraySearchInfo.removeAll()
            }
            if self.arraySearchInfo.count == 0 {
                self.arraySearchInfo = searchInfo
            }else{
                self.arraySearchInfo.append(contentsOf: searchInfo)
            }
            self.tableview.reloadData()
            self.tableview.endRefreshing(at: .top)
            self.tableview.finishInfiniteScroll(completion: { (collection) in
            })
            SVProgressHUD.dismiss()
        }, failed: { (error) in
          
            SVProgressHUD.dismiss()
        })
    }
    
    func getDetailDiary(array: [Diaries] ,idDiary: Int,_ indexRow: Int, editFromView: EditFromView, completion: @escaping (_ homeFeeds: [Diaries])->()){
        var arrayTemp = array
        SVProgressHUD.show()
        APIManager.getDetailDiary(id: idDiary, callbackSuccess: { [weak self] (homeFeed) in
            guard let self = self else { return }
            let homeDetail = HomeDetailViewController.init(nib: R.nib.homeDetailViewController)
            homeDetail.hidesBottomBarWhenPushed = true
            homeDetail.homeFeed = homeFeed
            homeDetail.isHomeVC = false
            homeDetail.titleVC = "Nhật ký"
            homeDetail.editFromView = editFromView
            homeDetail.updateObject = { homefeedUpdate in
                arrayTemp[indexRow].likeNumber = homefeedUpdate.numberLike
                arrayTemp[indexRow].isLiked = homefeedUpdate.checkLike
                arrayTemp[indexRow].commentNumber = homefeedUpdate.numberComment
                completion(arrayTemp)
            }
            
            homeDetail.deleteObject = { id in
                arrayTemp.remove(at: indexRow)
                completion(arrayTemp)
            }
            Utils.getTopMostViewController()?.navigationController?.pushViewController(homeDetail, animated: true)
            SVProgressHUD.dismiss()
            self.tableview.isUserInteractionEnabled = true
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
            self.tableview.isUserInteractionEnabled = true
        }
    }
    
    func getDetailFeed(array: [HomeFeed] ,idFeed: Int,_ indexRow: Int, editFromView: EditFromView, completion: @escaping (_ homeFeeds: [HomeFeed])->()){
        var arrayTemp = array
        SVProgressHUD.show()
        APIManager.getDetailFeed(id: idFeed, callbackSuccess: { [weak self] (homeFeed) in
            guard let self = self else { return }
            let homeDetail = HomeDetailViewController.init(nib: R.nib.homeDetailViewController)
            homeDetail.hidesBottomBarWhenPushed = true
            homeDetail.homeFeed = homeFeed
            homeDetail.isHomeVC = true
            homeDetail.titleVC = "Bài viết"
            homeDetail.editFromView = editFromView
            homeDetail.updateObject = { homefeedUpdate in
                let homeFeedTemp = arrayTemp[indexRow]
                arrayTemp[indexRow] = homefeedUpdate
                arrayTemp[indexRow].content = homeFeedTemp.content
                completion(arrayTemp)
            }
            
            homeDetail.deleteObject = { id in
                arrayTemp.remove(at: indexRow)
                completion(arrayTemp)
            }
            
            Utils.getTopMostViewController()?.navigationController?.pushViewController(homeDetail, animated: true)
            SVProgressHUD.dismiss()
            self.tableview.isUserInteractionEnabled = true
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
            self.tableview.isUserInteractionEnabled = true
        }
    }
    
    func getCheckCompleteFeed(id: Int){
        self.showNotiCreatePost()
        let _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { (timer) in
            APIManager.checkCompletedFeed(id: id, callbackSuccess: { [weak self] (homeFeed, status) in
                guard let self = self else { return }
                if status == 1{
                    switch self.gettype {
                    case "1","2":
                        print("1234")
                    default:
                        let index = self.arraySearchHome.firstIndex(where: { (home) -> Bool in
                            return home.id == homeFeed.id
                        })
                        if index != nil{
                            self.arraySearchHome[index!] = homeFeed
                        }
                    }
                    self.tableview.reloadData()
                    timer.invalidate()
                    self.dismissPopup()
                }
            }) { (error) in
                timer.invalidate()
                self.dismissPopup()
             //   Utils.showAlertView(controller: self, title: "Error", message: error)
            }
        }
    }
    
    func getCheckCompleteDiary(id: Int){
        self.showNotiCreatePost()
        let _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { (timer) in
            APIManager.checkCompletedDiary(id: id, callbackSuccess: { [weak self] (diary, status) in
                guard let self = self else { return }
                if status == 1{
                    switch self.gettype {
                    case "1":
                        let index = self.arraySearchDiary.firstIndex(where: { (diaryEdit) -> Bool in
                            return diary.id == diaryEdit.id
                        })
                        if index != nil{
                            self.arraySearchDiary[index!] = diary
                        }
                    default:
                        print("1234")
                    }
                    self.tableview.reloadData()
                    timer.invalidate()
                    self.dismissPopup()
                }
            }) { (error) in
                self.dismissPopup()
             //   Utils.showAlertView(controller: self, title: "Error", message: error)
            }
        }
    }
    
    //MARK: - Action Push View
    func pushPostDetail(array: [PostDetailModel] ,idPost: Int,_ indexRow: Int, completion: @escaping (_ homeFeeds: [PostDetailModel])->()){
        var arrayTemp = array
        let postDetail = PostDetailViewController.init(nib: R.nib.postDetailViewController)
        postDetail.idPost = idPost
        postDetail.hidesBottomBarWhenPushed = true
        postDetail.titleVC = "Thông tin"
        postDetail.updateObject = { post in
            arrayTemp[indexRow] = post
            completion(arrayTemp)
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(postDetail, animated: true)
    }
    
    func showNotiCreatePost(){
        let viewPopupDelete =  ViewNotificationCreatePost.instanceFromNib()
        var attributes = EKAttributes()
        attributes.displayDuration = 5
        attributes.position = .top
        SwiftEntryKit.display(entry: viewPopupDelete, using: attributes)
    }
    
    func likePost(idFeed: Int, index: Int, isLike: Int){
        let homeFeed = self.arraySearchHome[index]
        guard let numberLike = homeFeed.numberLike, numberLike > -1 else {
            return
        }
        
        homeFeed.checkLike = isLike == 1 ? 0 : 1
        homeFeed.numberLike = homeFeed.checkLike == 0 ? numberLike - 1 :numberLike + 1
        self.arraySearchHome[index] = homeFeed
        self.tableview.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        APIManager.likeHomeFeed(id: idFeed, type: 1, checkLike:isLike , callbackSuccess: { [weak self] (numberLike, checkLike) in
            guard let self = self else { return }
            if numberLike != homeFeed.numberLike {
                let homeFeed = self.arraySearchHome[index]
                homeFeed.checkLike = checkLike
                homeFeed.numberLike = numberLike
                self.arraySearchHome[index] = homeFeed
                self.tableview.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
            
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func favouritePost(idFeed: Int, index: Int, isFavourite: Int){
        let homeFeed = self.arraySearchHome[index]
        homeFeed.checkSave = isFavourite == 1 ? 0 : 1
        self.arraySearchHome[index] = homeFeed
        self.tableview.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        
        APIManager.saveHomeFeed(id: idFeed, checkSave: isFavourite , callbackSuccess: { [weak self] (checkSave) in
            guard let self = self else { return }
            if checkSave != homeFeed.checkSave{
                let homeFeed = self.arraySearchHome[index]
                homeFeed.checkSave = checkSave
                self.arraySearchHome[index] = homeFeed
                self.tableview.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
            
            
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func handelLike(indexRow: Int, homefeed: HomeFeed){
        guard let viewSelf = Utils.getTopMostViewController() else {
            return
        }
        if Utils.checkLogin(selfView: viewSelf) {
            guard let id = homefeed.id else { return }
            guard let checkLike = homefeed.checkLike else { return }
            self.likePost(idFeed: id, index: indexRow, isLike: checkLike)
        }
    }
    
    func handelSave(indexRow: Int, homefeed: HomeFeed){
        guard let viewSelf = Utils.getTopMostViewController() else {
            return
        }
        if Utils.checkLogin(selfView: viewSelf) {
            guard let id = homefeed.id else { return }
            guard let checkSave = homefeed.checkSave else { return }
            self.favouritePost(idFeed: id, index: indexRow, isFavourite: checkSave)
        }
    }
    
    func handelComment(indexRow: Int, homefeed: HomeFeed){
        guard let viewSelf = Utils.getTopMostViewController() else {
            return
        }
        if Utils.checkLogin(selfView: viewSelf) {
            let bottomCommentTableView = BottomTableViewViewController.init(nib: R.nib.bottomTableViewViewController)
            bottomCommentTableView.topCornerRadius = 20
            bottomCommentTableView.presentDuration = 0.25
            bottomCommentTableView.dismissDuration = 0.25
            bottomCommentTableView.isFeed = homefeed.id ?? 0
            bottomCommentTableView.updateComment = {
                homefeed.numberComment = homefeed.numberComment ?? 0 + 1
                self.tableview.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .none)
            }
           // bottomCommentTableView.modalPresentationStyle = .custom
            Utils.getTopMostViewController()?.present(bottomCommentTableView, animated: true, completion: nil)
        }
    }
    
    //MARK: - Acction Dissmis Popup
    func dismissPopup(){
        SwiftEntryKit.dismiss()
    }
    
}

//MARK: - UITableviewDelegate
extension PagingListBaseView: UITableViewDataSource, UITableViewDelegate {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch gettype {
        case "1":
            return arraySearchDiary.count
            
        case "2":
             return arraySearchInfo.count
            
        default:
            return arraySearchHome.count
            
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch gettype {
        case "1":
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.diaryNewTableViewCell, for: indexPath)!
            cell.setupDataMore(listDiaryMore: self.arraySearchDiary[indexPath.row])
            return cell
        case "2":
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.detailInfoTableViewCell, for: indexPath)!
            cell.setupDataMore(listDetailMore: self.arraySearchInfo[indexPath.row])
            return cell
            
        default:
            let homefeed = self.arraySearchHome[indexPath.row]
            if homefeed.ads == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeTableViewCell, for: indexPath)!
                cell.setupDataHomeFeed(homeFeed: homefeed)
                cell.tapComment = { self.handelComment(indexRow: indexPath.row, homefeed: homefeed) }
                cell.tapLike = { self.handelLike(indexRow: indexPath.row, homefeed: homefeed)}
                cell.tapSave = { self.handelSave(indexRow: indexPath.row, homefeed: homefeed) }
                
                return cell
            }else{
                let cellQC = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeQCTableViewCell, for: indexPath)!
                cellQC.setupDataHomeFeedQC(homeFeed: homefeed)
                cellQC.tapComment = { self.handelComment(indexRow: indexPath.row, homefeed: homefeed) }
                cellQC.tapLike = { self.handelLike(indexRow: indexPath.row, homefeed: homefeed)}
                cellQC.tapSave = { self.handelSave(indexRow: indexPath.row, homefeed: homefeed) }
                
                return cellQC
            }
//            let homefeed = self.arraySearchHome[indexPath.row]
//            if homefeed.ads == 0{
//                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeNTableViewCell, for: indexPath)!
//                cell.setupDataHomeFeed(homeFeed: homefeed)
//                return cell
//            }else{
//                let cellQC = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeQCTableViewCell, for: indexPath)!
//                cellQC.setupDataHomeFeedQC(homeFeed: homefeed)
//                return cellQC
//            }
           
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch gettype {
        case "1":
            return 143
        case "2":
            return DetailInfoTableViewCell.heightRow
        default:
//            let homefeed = self.arraySearchHome[indexPath.row]
//            if homefeed.ads == 0{
//                return 175
//            }
//            return 250
            return UITableView.automaticDimension
        }

    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch gettype {
        case "1":
            return 0
        case "2":
            return 0
        default:
            return 80
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.reuseIdentifier.headerFilterTableViewCell)!
        headerView.tapLocation = { index in
            if self.gettype != "1" && self.gettype != "2"{
                let locationVC = LocationViewController.init(nib: R.nib.locationViewController)
                          locationVC.hidesBottomBarWhenPushed = true
                          locationVC.didSelectedLocation = { city in
                            
                            // reset distric
                            self.districtCode = ""
                            headerView.lblDistric.text = R.string.localizable.commonDistrict()
                            headerView.lblDistric.textColor = UIColor.init(hexString: "#979797",alpha: 0.87)
                            Utils.districtColorDefault(btn: UIButton(), img: headerView.imgArrowDistric, view: headerView.viewDistric)

                            
                             self.cityCode = city.locationCode ?? 0
                            headerView.viewLocation.backgroundColor = UIColor.init(hexString: "fff5d4")
                            headerView.lblLocation.textColor = UIColor.init(hexString: "000000")
                            headerView.viewLocation.borderColor = UIColor.init(hexString: AppColor.colorText)
                            headerView.imgArrow.image = R.image.polygon_23()
                              switch city.locationCode {
                              case 1:
                                  headerView.lblLocation.text = "Hà Nội"
                              case 79:
                                  headerView.lblLocation.text = R.string.localizable.commonTPHCMShort()
                              default:
                                headerView.lblLocation.text = city.locationNameVi
                              }
                              self.getSearchHomeList(key: self.key, page: self.page)
                          }
                          Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: false)
            }
        }
        headerView.tapType = { index in
            let districVC = DistrictViewController.init(nib: R.nib.districtViewController)
            districVC.hidesBottomBarWhenPushed = true
           districVC.title = "Thể loại bài viết"
            districVC.type = 1
            districVC.tapType = { index in
                headerView.viewType.backgroundColor = UIColor.init(hexString: "fff5d4")
                headerView.lblType.textColor = UIColor.init(hexString: "000000")
                headerView.viewType.borderColor = UIColor.init(hexString: AppColor.colorText)
                headerView.imgArrowType.image = R.image.polygon_23()
                headerView.lblType.text = self.dataInfo[index]

                self.category = index + 1
                self.page = 1
                self.getSearchHomeList(key: self.key, page: self.page)
            }
            Utils.getTopMostViewController()?.navigationController?.pushViewController(districVC, animated: false)
        }
        headerView.tapDistric = { index in
            let districVC = DistrictViewController.init(nib: R.nib.districtViewController)
            districVC.hidesBottomBarWhenPushed = true
            districVC.locationID = self.cityCode
            districVC.title = "Danh sách các quận"
            districVC.didSelectedLocation = { city in
                self.districtCode = city.districtCode ?? ""
                headerView.viewDistric.backgroundColor = UIColor.init(hexString: "fff5d4")
                headerView.lblDistric.textColor = UIColor.init(hexString: "000000")
                headerView.viewDistric.borderColor = UIColor.init(hexString: AppColor.colorText)
                headerView.imgArrowDistric.image = R.image.polygon_23()
                headerView.lblDistric.text = city.districtName
                if city.districtCode == "" {
                     headerView.lblDistric.textColor = UIColor.init(hexString: "#979797",alpha: 0.87)
                    Utils.districtColorDefault(btn: UIButton(), img: headerView.imgArrowDistric, view: headerView.viewDistric)
                }
                self.getSearchHomeList(key: self.key, page: self.page)
            }
            Utils.getTopMostViewController()?.navigationController?.pushViewController(districVC, animated: false)
        }
        return headerView
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utils.checkLogin(selfView: Utils.getTopMostViewController()!) {
            lastIndex = indexPath.row
            
            switch gettype {
            case "1":
                guard let selfStrong = Utils.getTopMostViewController() else {
                    return
                }
                Router.share.pushToViewDiaryDetail(tableView: tableView, array: self.arraySearchDiary, indexPath: indexPath,row: indexPath.row, viewSelf: selfStrong, idFeed: self.arraySearchDiary[indexPath.row].id ?? 0, editFromView: .Sreach) { (diary) in
                    self.arraySearchDiary = diary
                    self.tableview.reloadData()
                }
            case "2":
                let idPost = self.arraySearchInfo[indexPath.row].id ?? 0
                self.pushPostDetail(array: self.arraySearchInfo, idPost: idPost, indexPath.row) { (posts) in
                    self.arraySearchInfo = posts
                    self.tableview.reloadData()
                    tableView.isUserInteractionEnabled = true
                }
            default:
                guard let selfStrong = Utils.getTopMostViewController() else {
                    return
                }
                Router.share.pushToViewHomeDetail(tableView: tableView, array: self.arraySearchHome,indexPath: indexPath, viewSelf: selfStrong, idFeed: self.arraySearchHome[indexPath.row].id ?? 0, editFromView: .Sreach, completion: { homeFeeds in
                    self.arraySearchHome = homeFeeds
                    self.tableview.reloadData()
                })
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.cellHeightsDictionary[indexPath] = cell.frame.size.height
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height =  self.cellHeightsDictionary[indexPath] {
            return height
        }
        return UITableView.automaticDimension
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
    
    
}

//MARK: - JXPagingViewListViewDelegate
extension PagingListBaseView: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }
    
    public func listScrollView() -> UIScrollView {
        return self.tableview
    }
}

