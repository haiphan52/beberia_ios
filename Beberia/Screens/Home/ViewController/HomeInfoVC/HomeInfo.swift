//
//  HomeInfo.swift
//  Beberia
//
//  Created by iMAC on 9/3/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import JXSegmentedView
import PullToRefresh

enum TypeSearch {
    case News
    case Diary
    case Infomation
}

class HomeInfo: BaseViewController {
    //MARK: -Properties
    var pagingView: JXPagingView!
    var userHeaderView: PagingViewTableHeaderView!
    var userHeaderContainerView: UIView!
    var segmentedViewDataSource: JXSegmentedTitleDataSource!
    var segmentedView: JXSegmentedView!
    let titles = [R.string.localizable.homeNews(), R.string.localizable.diaryDiary(),R.string.localizable.infomationInfomation()]
    var txtsearch = ""
    var indext = 0
    var JXTableHeaderViewHeight: Int = 0
    var JXheightForHeaderInSection: Int = 50
    let refresher = PullToRefresh()
    
    //MARK: - SetupUI
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                     bgColor: .white,
                                     textColor: R.color.f3C()!)
    }
    
    //MARK: - SetupUI
    func setupUI(){
        self.title = "Tìm kiếm" 
        self.navigationController?.navigationBar.isTranslucent = false
        self.definesPresentationContext = true
        userHeaderContainerView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: CGFloat(JXTableHeaderViewHeight)))
        userHeaderContainerView.backgroundColor = UIColor.init(hex: "000000")
        
        userHeaderView = PagingViewTableHeaderView(frame: userHeaderContainerView.bounds)
        userHeaderContainerView.addSubview(userHeaderView)
        segmentedViewDataSource = JXSegmentedTitleDataSource()
        segmentedViewDataSource.titles = titles
        segmentedViewDataSource.titleSelectedColor = .black
    
        segmentedViewDataSource.titleNormalColor = UIColor.init(hexString: AppColor.colorBlack, alpha: 0.5)!
        segmentedViewDataSource.isTitleColorGradientEnabled = true
        segmentedViewDataSource.isTitleZoomEnabled = true

        segmentedViewDataSource.reloadData(selectedIndex: indext)
        segmentedViewDataSource.isItemSpacingAverageEnabled = false
        segmentedView = JXSegmentedView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: CGFloat(JXheightForHeaderInSection)))
       
    //    segmentedView.backgroundColor = UIColor.init(hex: "ffc440")
        segmentedView.defaultSelectedIndex = indext
        segmentedView.dataSource = segmentedViewDataSource
        segmentedView.delegate = self
        segmentedView.contentEdgeInsetLeft = 16
        segmentedView.isContentScrollViewClickTransitionAnimationEnabled = true
        
//        let lineView = JXSegmentedIndicatorLineView()
//        lineView.indicatorColor = UIColor.init(hexString: AppColor.colorYellow)!
//        lineView.indicatorWidth = 30
//        lineView.borderWidth = 0
//        lineView.verticalOffset = 8
//        
//        lineView.borderColor = UIColor.clear
//        segmentedView.indicators = [lineView]
//        let lineWidth = 1/UIScreen.main.scale
//
//        let lineLayer = CALayer()
//        lineLayer.backgroundColor = UIColor.clear.cgColor
//
//        lineLayer.frame = CGRect(x: 0, y: segmentedView.bounds.height - lineWidth, width: segmentedView.bounds.width, height: lineWidth)
//        segmentedView.layer.addSublayer(lineLayer)
        pagingView = JXPagingView(delegate: self )
        self.view.addSubview(pagingView)
        segmentedView.contentScrollView = pagingView.listContainerView.collectionView
        
        customLeftBarButton()
        pagingView.indexCurrent = { index  in
            self.indext = index
        }
        
//        pagingView.mainTableView?.addPullToRefresh(refresher) {
//            self.pagingView.mainTableView?.startRefreshing(at: .top)
//        }
    }
    
    //MARK: - override
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let y = GetHeightSafeArea.shared.getHeight(type: .top) + ConstantApp.shared.getStatusBarHeight() + 10
        pagingView.frame = CGRect(x: 0, y: y, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        let searchBar = UISearchBar()
        searchBar.placeholder = R.string.localizable.homeSearch()
        searchBar.text = txtsearch
        searchBar.delegate = self
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        self.navigationItem.titleView = searchBar

    }
    
}
//MARK: - JXPagingViewDelegate
extension HomeInfo: JXPagingViewDelegate {
    
    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
        return JXTableHeaderViewHeight
    }
    
    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
        return userHeaderContainerView
    }
    
    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
        return JXheightForHeaderInSection
    }
    
    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
        return segmentedView
    }
    
    func numberOfLists(in pagingView: JXPagingView) -> Int {
        return titles.count
    }
    
    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        let list = PagingListBaseView()
        guard list.tableview != nil else {
            return list
        }
        
      //  list.tableview.showsVerticalScrollIndicator = false
        list.tableview.infiniteScrollTriggerOffset = 700
        
        switch index {
        case 0:
            
            
            list.tableview.addInfiniteScroll { (tableView) in
                tableView.performBatchUpdates({ () -> Void in
                    // update collection view
                    if !list.tokenPage.isEmpty {
                        list.page = list.page + 1
                        list.getSearchHomeList(key: self.txtsearch , page:list.page)
                    }else {
                        list.tableview.finishInfiniteScroll(completion: { (collection) in
                        })
                    }
                }, completion: { (finished) -> Void in
                    // finish infinite scroll animations
                    tableView.finishInfiniteScroll()
                });
            }
            
//            pagingView.mainTableView?.addPullToRefresh(refresher) {
//                list.page = 1
//                list.getSearchHomeList(key: self.txtsearch, page: list.page)
//                self.pagingView.mainTableView?.endRefreshing(at: .top)
//            }
            
            list.getSearchHomeList(key: txtsearch, page: 1)
            indext = index
            list.key = txtsearch
        case 1:
            list.gettype = "1"
            
            list.tableview.addInfiniteScroll { (tableView) in
                tableView.performBatchUpdates({ () -> Void in
                    // update collection view
                    if list.nextpage > 0 {
                        list.page = list.page + 1
                        list.getSearchDiaryList(key: self.txtsearch,gettype:1, page: list.page)
                    }else {
                        list.tableview.finishInfiniteScroll(completion: { (collection) in
                        })
                    }
                }, completion: { (finished) -> Void in
                    // finish infinite scroll animations
                    tableView.finishInfiniteScroll()
                });
            }
            
            
//            pagingView.mainTableView?.addPullToRefresh(refresher) {
//                list.page = 1
//                list.getSearchDiaryList(key: self.txtsearch,gettype:1, page: list.page)
//                self.pagingView.mainTableView?.endRefreshing(at: .top)
//            }
            list.getSearchDiaryList(key: txtsearch,gettype:1, page: 1)
            indext = index
        case 2:
            
            list.tableview.addInfiniteScroll { (tableView) in
                tableView.performBatchUpdates({ () -> Void in
                    // update collection view
                    if list.nextpage > 0 {
                        list.page = list.page + 1
                        list.getSearchInfoList(key: self.txtsearch,gettype:2, page: list.page)
                    }else {
                        list.tableview.finishInfiniteScroll(completion: { (collection) in
                        })
                    }
                }, completion: { (finished) -> Void in
                    // finish infinite scroll animations
                    tableView.finishInfiniteScroll()
                });
            }
            
//            pagingView.mainTableView?.addPullToRefresh(refresher) {
//                list.page = 1
//                list.getSearchInfoList(key: self.txtsearch,gettype:2, page: list.page)
//                self.pagingView.mainTableView?.endRefreshing(at: .top)
//                list.tableview.finishInfiniteScroll(completion: { (collection) in
//                })
//            }
            
            list.gettype = "2"
            list.getSearchInfoList(key: txtsearch,gettype:2, page: 1)
            indext = index
        default:
             print(index)
        }

        list.beginFirstRefresh()
        return list
    }
   
    func mainTableViewDidScroll(_ scrollView: UIScrollView) {
        userHeaderView?.scrollViewDidScroll(contentOffsetY: scrollView.contentOffset.y)
    }
}

extension HomeInfo: JXSegmentedViewDelegate {
    
}

//MARK: UISearchBarDelegate
extension HomeInfo: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
          txtsearch = searchBar.text ?? ""
          setupUI()
    }
}
