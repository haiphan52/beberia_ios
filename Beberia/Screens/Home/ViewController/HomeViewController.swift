//
//  HomeViewController.swift
//  Beberia
//
//  Created by IMAC on 8/28/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import FSPagerView
import Kingfisher
import SVProgressHUD
import TTGTagCollectionView
import UIScrollView_InfiniteScroll
import PullToRefresh
import SwiftyJSON
import FFPopup
import NVActivityIndicatorView
import RxSwift

class HomeViewController: BaseViewController {
    
    // MARK: - Outlets
    var openFrom: GuideGetPointViewController.openfrom = .other
    
    @IBOutlet weak var btnNewFeedNews: UIButton!
    @IBOutlet weak var viewSelectedTopicHome: ViewSelectedTopicHome!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintHeightTbv: NSLayoutConstraint!
    @IBOutlet weak var viewNoti: UIView!
    @IBOutlet weak var constrainHeightViewNoti: NSLayoutConstraint!
    @IBOutlet weak var btnButtonAdd: UIButton!
    @IBOutlet weak var pageControl: FSPageControl!
    @IBOutlet weak var tbvHome: ContentSizedTableView!
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var viewDistric: UIView!
    @IBOutlet weak var btnDistric: UIButton!
    @IBOutlet weak var imgDistric: UIImageView!
    
    // MARK: - Properties
    
    var heightDictionary: [IndexPath : CGFloat] = [:]
    var isLoadingMore = false // flag
    var rightBarButtonItemNoti = UIBarButtonItem()
    var arrayImage = [String]()
    var arrayHomeFeeds = [HomeFeed]()
    var about = 0
    var category = 0
    var ageRange = 7
    var page = 1
    var tokenPage = ""
    var nextPage = 0
    var indexLocation:Int? = nil
    let refresher = PullToRefresh()
    var indexObjectUpdate = 0
    var isLoading = false
  //  var locationID = 1
    var idDistrict = ""
    var listBanner = [Baner]()
    var allProvide = CityName.init()
    private let checkTimeEvent: PublishSubject<(CheckTimeModel?, String)> = PublishSubject.init()
    
    // MARK: View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
           
        allProvide.id = 1000
        allProvide.locationCode = 0
        allProvide.locationNameVi = "Toàn quốc"
        
        self.view.backgroundColor = UIColor.white
        setupUI()
        
        tabBarController?.delegate = self
        
        LoaddingManager.initLoadding(view: self.view)
        // Call API
        getHomeFeed(tokenPage: "", about: about, category: category, ageRange: ageRange, locationID: allProvide.locationCode ?? 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setBadgeCount), name: NSNotification.Name.updateBagde, object: nil)
        
        // selected from ViewSelectedTopicHome
        viewSelectedTopicHome.selectedTopic = { about, category, ageRange in
            self.about = about
            self.category = category
            self.ageRange = ageRange
            self.isLoadingMore = true
            self.arrayHomeFeeds.removeAll()
            self.tbvHome.reloadData()
            self.getHomeFeed(tokenPage: "", about: about,category: category, ageRange: ageRange, locationID: self.allProvide.locationCode ?? 0)
        }
        
        if tabBarController?.tabBar.items?[1].badgeValue == "" {
            tabBarController?.tabBar.items?[1].badgeValue = nil
        }
        
//        self.checkShowTutorial()
        //        self.getBannerEven()
        
        //        btnNewFeedNews.rx.tap.subscribe { (_) in
        //            self.tabBarController?.tabBar.items?[1].badgeValue = nil
        //            self.getHomeFeed(page: 1, about: 0, category: 0, ageRange: 7, locationID: 1)
        //            self.setUpButtonLocation()
        //        }.disposed(by: disposeBag)
        
        self.setupRX()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tbvHome.isUserInteractionEnabled = true
        setupRightBarButton()
        setBadgeCount()
        self.tabBarController?.delegate = self
        
        if tabBarController?.tabBar.items?[1].badgeValue == "" {
            tabBarController?.tabBar.items?[1].badgeValue = nil
            self.getHomeFeed(tokenPage: "", about: 0, category: 0, ageRange: 7, locationID: 1)
            self.setUpButtonLocation()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(),for: .default)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let image = R.image.ic_Elipse()
        self.navigationController?.navigationBar.setBackgroundImage(image,for: .default)
        self.navigationController?.hideHairline()
    }
    
    private func checkShowTutorial(){
        if !UserDefaults.standard.bool(forKey: "FristTutorial"){
            let tutorialViewController = TutorialViewController.init(nib: R.nib.tutorialViewController)
            tutorialViewController.modalPresentationStyle = .fullScreen
            self.present(tutorialViewController, animated: true, completion: nil)
            UserDefaults.standard.set(true, forKey: "FristTutorial")
        }
    }
    
    
    // MARK: Overrides
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tbvHome.layer.removeAllAnimations()
        self.constraintHeightTbv.constant = tbvHome.contentSize.height
        //        UIView.animate(withDuration: 0.5) {
        //            self.updateViewConstraints()
        //            self.view.layoutIfNeeded()
        //        }
    }
    
    //MARK: Helper methods
    
    private func setupRX() {
        self.checkTimeEvent.asObservable().bind { [weak self] model in
            guard let wSelf = self else { return }
            if model.0?.remain == nil {
                let writeHome = WriteHomeNewViewController.init(nib: R.nib.writeHomeNewViewController)
                writeHome.hidesBottomBarWhenPushed = true
                writeHome.fromView = .Home
                //        writeHome.checkComplete = { id in
                //            self.getCheckCompleteFeed(id: id, isEdit: false, completion: { homeFeed in
                //                self.arrayHomeFeeds.insert(homeFeed, at: 0)
                //                self.tbvHome.beginUpdates()
                //                self.tbvHome.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                //                self.tbvHome.endUpdates()
                //                NotificationCenter.default.post(name: NSNotification.Name.updateFeedNew, object: ["FeedNew": homeFeed], userInfo: nil)
                //            })
                //        }
                wSelf.navigationController?.pushViewController(writeHome, animated: true)
            } else {
                let vc = CountingPost.createVC()
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overFullScreen
                vc.countPost = model.0
                vc.textTitle = model.1
                wSelf.present(vc, animated: true, completion: nil)
            }
            
        }.disposed(by: self.disposeBag)
        
        Utils.getAppDelegate().isCheckLogin.asObservable().bind { [weak self] isLogin in
            guard let wSelf = self, isLogin else { return }
            wSelf.setupRightBarButton()
        }.disposed(by: self.disposeBag)
    }
    
    @objc func checkEditFeed(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        
        let idFeed = object[Key.KeyNotification.idFeed] as? Int
        let isEdit = object[Key.KeyNotification.EditFeed] as? Bool
        self.getCheckCompleteFeed(id: idFeed ?? 0, isEdit: isEdit ?? true, completion: { homeFeed in
            if isEdit ?? true{
                NotificationCenter.default.post(name: NSNotification.Name.updateFeedNew, object: ["FeedNew": homeFeed], userInfo: nil)
                let index = self.arrayHomeFeeds.firstIndex(where: { (home) -> Bool in
                    return home.id == homeFeed.id
                })
                if index != nil{
                    self.arrayHomeFeeds[index!] = homeFeed
                    self.tbvHome.reloadRows(at: [IndexPath.init(row: index!, section: 0)], with: .automatic)
                }
                
            }else{
                self.arrayHomeFeeds.insert(homeFeed, at: 0)
                //                self.tbvHome.reloadData()
                self.tbvHome.beginUpdates()
                self.tbvHome.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                self.tbvHome.endUpdates()
                self.tbvHome.reloadData()
                
                NotificationCenter.default.post(name: NSNotification.Name.updateFeedNew, object: ["FeedNew": homeFeed], userInfo: nil)
            }
        })
    }
    
    private func tapSearchNew() {
        if Utils.checkLogin(selfView: self) {
            let homeVC = HomeInfo.init(nib: R.nib.homeInfo)
            homeVC.indext = 0
            homeVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
    @objc func tapNotification(){
        rightBarButtonItemNoti.setBadge(text: "")
        Utils.getAppDelegate().badgeCount = 0
        
        let homeVC = NotificationViewController.init(nib: R.nib.notificationViewController)
        homeVC.hidesBottomBarWhenPushed = true
        Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @objc func tapSreach(){
        //        let homeVC = HomeSearchViewController.init(nib: R.nib.homeSearchViewController)
        //        homeVC.indexSearch = 0
        //        homeVC.hidesBottomBarWhenPushed = true
        //        Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: true)
        if Utils.checkLogin(selfView: self) {
            let homeVC = HomeInfo.init(nib: R.nib.homeInfo)
            homeVC.indext = 0
            homeVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: false)
        } 
    }
    
    @objc func tapMenu(){
       
        if Utils.checkLogin(selfView: self) {
            let menuVC = R.storyboard.main.menuViewController()!
            menuVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(menuVC, animated: false)
        }
    }
    
    // MARK: SetupUI
    
    func setupUI(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.extendedLayoutIncludesOpaqueBars = true
        btnNewFeedNews.isHidden = true
        
        setupTableView()
        setUpButtonLocation()
        
        
        // hede view distric
        viewDistric.isHidden = true
        
        // custom view Logo Beberia
        let viewTitle = ViewTitleHome.instanceFromNib()
        viewTitle.viewImage.isHidden = true
        viewTitle.widthImage.constant = 0
        customLeftBarView(view: viewTitle)
        
        viewTitle.tapSearch = { [weak self] in
            guard let self = self else { return }
            self.tapSearchNew()
        }
        
        viewTitle.tapBack = { [weak self] in
            guard let wSelf = self else { return }
            wSelf.dismiss(animated: true, completion: nil)
        }
        switch self.openFrom {
        case .guide:
            viewTitle.btBack.isHidden = false
        case .other:
            viewTitle.btBack.isHidden = true
        }
    }
    
    func setUpButtonLocation(){
        setColorBtnLocation()
//        btnLocation.setTitle(UserInfo.shareUserInfo.city_name.locationNameVi ?? "", for: .normal)
//        tabBarController?.delegate = self
//      //  btnLocation.setTitle("Hà Nội", for: .normal)
//
//        if Utils.checkLogin(selfView: Utils.getTopMostViewController()!, showAlert: false){
//            locationID = UserInfo.shareUserInfo.city_name.locationCode ?? 1
//            btnLocation.setTitle(UserInfo.shareUserInfo.city_name.locationNameVi, for: .normal)
//        }
        
      //  locationID = UserInfo.shareUserInfo.city_name.locationCode ?? 1
        btnLocation.setTitle(allProvide.locationNameVi, for: .normal)
    }
    
    func setColorBtnLocation(){
        btnLocation.setTitleColor(UIColor.init(hexString: "ffffff"), for: .normal)
        imgLocation.image = R.image.icDownWhile()
        viewLocation.backgroundColor =  UIColor.init(hexString: "0299fe" ,alpha: 1)
        viewLocation.cornerRadius = 4
        if UserInfo.shareUserInfo.district.districtName ?? "" == "" {
            Utils.districtColorDefault(btn: self.btnDistric, img: self.imgDistric, view: self.viewDistric)
        }
    }
    
    func setupTableView(){
        tbvHome.register(R.nib.homeTableViewCell)
        tbvHome.register(R.nib.homeQCTableViewCell)
        //      tbvHome.rowHeight = 387
        tbvHome.delegate = self
        tbvHome.dataSource = self
        tbvHome.separatorStyle = .none
        tbvHome.isScrollEnabled = false
        tbvHome.estimatedRowHeight = UITableView.automaticDimension
        scrollView.isScrollEnabled = true
        scrollView.delegate = self
        scrollView.infiniteScrollTriggerOffset = 700
        scrollView.addInfiniteScroll { (tableView) in
            if self.tokenPage != ""{
                self.isLoadingMore = true
                self.getHomeFeed(tokenPage: self.tokenPage, about: self.about,category: self.category, ageRange: self.ageRange, locationID: self.allProvide.locationCode ?? 0)
            }else{
                self.scrollView.endRefreshing(at: .top)
                self.scrollView.finishInfiniteScroll(completion: { (collection) in
                })
            }
        }
        
        scrollView.addPullToRefresh(refresher) {
            self.isLoadingMore = true
            self.arrayHomeFeeds = []
            self.getHomeFeed(tokenPage: "", about: self.about, category: self.category, ageRange: self.ageRange, locationID: self.allProvide.locationCode ?? 0)
            self.scrollView.startRefreshing(at: .top)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(checkEditFeed), name: NSNotification.Name.checkEditHome, object: nil)
        
        self.tbvHome.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    @objc func setBadgeCount(){
        if Utils.checkLogin(selfView: self, showAlert: false){
            if Utils.getAppDelegate().badgeCount > 0 {
                // if  UIApplication.shared.applicationIconBadgeNumber > 0 {
                rightBarButtonItemNoti.addBadge(text: "\(Utils.getAppDelegate().badgeCount)")
            }else{
                rightBarButtonItemNoti.setBadge(text: "")
            }
        }
    }
    
    func getBannerEven(){
        
        APIManager.getBanner(type: 6, callbackSuccess: { (banners) in
            if banners.count > 0{
                Utils.checkShowViewEven(baner: banners[0])
            }
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        })
    }
    
    func setupRightBarButton(){
        
        if Utils.checkLogin(selfView: self, showAlert: false){
            rightBarButtonItemNoti = UIBarButtonItem.menuButton(self, action: #selector(HomeViewController.tapNotification), imageName: R.image.group2923() ?? UIImage.init(), height: 25, width: 25)
            
         //   let rightBarButtonItemMenu = UIBarButtonItem.menuButton(self, action: #selector(HomeViewController.tapMenu), imageName: R.image.group15303() ?? UIImage.init(), height: 30, width: 30)
            
//            let rightBarButtonItemSreach = UIBarButtonItem.menuButton(self, action: #selector(HomeViewController.tapSreach), imageName: R.image.group2929() ?? UIImage.init(), height: 30, width: 30)
            
            self.navigationItem.rightBarButtonItems = [rightBarButtonItemNoti]
            
        }else{
//            rightBarButtonItemNoti = UIBarButtonItem.init()
//            
//            let rightBarButtonItemSreach = UIBarButtonItem.menuButton(self, action: #selector(HomeViewController.tapSreach), imageName: R.image.group2929() ?? UIImage.init(), height: 30, width: 30)
//            
//            self.navigationItem.rightBarButtonItems = [rightBarButtonItemSreach]
        }

    }
    
    // MARK: - Request API
    
    func getHomeFeed(tokenPage: String,about:Int,category: Int,ageRange: Int,locationID:Int, isLoadFeedNews: Bool = false){
        if !isLoadingMore{
            // SVProgressHUD.show()
            LoaddingManager.startLoadding()
        }
        APIManager.getHomeFeed(tokenPage: tokenPage, locationCode: locationID, about: about, category: category, ageRange: ageRange, district_code: idDistrict , callbackSuccess: { [weak self] (homeFeeds, tokenPage) in
            guard let self = self else { return }
            self.tokenPage = tokenPage
            self.isLoadingMore = false
            
            if self.arrayHomeFeeds.count == 0 || isLoadFeedNews{
                self.arrayHomeFeeds = homeFeeds
            }else{
                self.arrayHomeFeeds.append(contentsOf: homeFeeds)
            }
            DispatchQueue.main.async {
                // Update UI
                //do task what you want.
                // run on the main queue, after the previous code in outer block
                self.tbvHome.reloadData()
                self.scrollView.endRefreshing(at: .top)
                self.scrollView.finishInfiniteScroll(completion: { (collection) in
                })
            }
            LoaddingManager.stopLoadding()
            //   SVProgressHUD.dismiss()
        }) { (error) in
            //     SVProgressHUD.dismiss()
            LoaddingManager.stopLoadding()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func getDetailFeed(idFeed: Int, completion: @escaping (_ homeFeed: HomeFeed)->()){
        SVProgressHUD.show()
        APIManager.getDetailFeed(id: idFeed, callbackSuccess: { [weak self] (homeFeed) in
            guard let _ = self else { return }
            completion(homeFeed)
            SVProgressHUD.dismiss()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        }
    }
    
    
    // MARK: - Action
    
    @IBAction func btnLocation(_ sender: Any) {
        let locationVC = LocationViewController.init(nib: R.nib.locationViewController)
        locationVC.hidesBottomBarWhenPushed = true
        locationVC.isCheckHome = true
        self.arrayHomeFeeds.removeAll()
        locationVC.didSelectedLocation = { city in
            
            self.allProvide = city
            
            switch city.locationCode {
            case 79:
              //  self.locationID = city.locationCode ?? 0
                self.btnLocation.setTitle(R.string.localizable.commonTPHCMShort(), for: .normal)
                self.getHomeFeed(tokenPage: "", about: self.about, category: self.category, ageRange: self.ageRange, locationID: self.allProvide.locationCode ?? 0)
            default:
               // self.locationID = city.locationCode ?? 0
                self.btnLocation.setTitle(city.locationNameVi ?? "", for: .normal)
                self.getHomeFeed(tokenPage: "", about: self.about, category: self.category, ageRange: self.ageRange, locationID: self.allProvide.locationCode ?? 0)
                break
            }
            
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: true)
        
    }
    
    @IBAction func btnDistrict(_ sender: Any) {
        let districVC = DistrictViewController.init(nib: R.nib.districtViewController)
        districVC.hidesBottomBarWhenPushed = true
        districVC.locationID = self.allProvide.locationCode ?? 0
        self.arrayHomeFeeds.removeAll()
        districVC.didSelectedLocation = { city in
            self.btnDistric.setTitleColor(UIColor.init(hexString: AppColor.colorYellow), for: .normal)
            self.imgDistric.image = R.image.ic_ArrowDownYL()
            self.viewDistric.backgroundColor =  UIColor.init(hexString: AppColor.colorBGMenu,alpha: 0.4)
            self.viewDistric.borderColor = UIColor.init(hexString: AppColor.colorYellow,alpha: 0.5)
            self.viewDistric.borderWidth = 1
            self.viewDistric.cornerRadius = 15
            self.idDistrict = city.districtCode ?? ""
            if city.districtCode == "" {
                Utils.districtColorDefault(btn: self.btnDistric, img: self.imgDistric, view: self.viewDistric)
            }
            self.btnDistric.setTitle(city.districtName ?? "", for: .normal)
            self.getHomeFeed(tokenPage: "", about: self.about, category: self.category, ageRange: self.ageRange, locationID: self.allProvide.locationCode ?? 0)
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(districVC, animated: true)
    }
    
    @IBAction func didPressAdd(_ sender: Any) {
        if Utils.checkLogin(selfView: self) {
            self.checkTime()
        }
    }
    
    private func checkTime() {
        APIManager.checkTime { [weak self] model in
            guard let wSelf = self else { return }
            wSelf.checkTimeEvent.onNext(model)
        } failed: { error in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }

    }
    
    func likePost(idFeed: Int, index: Int, isLike: Int){
        let homeFeed = self.arrayHomeFeeds[index]
        guard let numberLike = homeFeed.numberLike, numberLike > -1 else {
            return
        }
        
        homeFeed.checkLike = isLike == 1 ? 0 : 1
        homeFeed.numberLike = homeFeed.checkLike == 0 ? numberLike - 1 :numberLike + 1
        self.arrayHomeFeeds[index] = homeFeed
        self.tbvHome.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        APIManager.likeHomeFeed(id: idFeed, type: 1, checkLike:isLike , callbackSuccess: { [weak self] (numberLike, checkLike) in
//            guard let self = self else { return }
//            if numberLike != homeFeed.numberLike {
//                let homeFeed = self.arrayHomeFeeds[index]
//                homeFeed.checkLike = checkLike
//                homeFeed.numberLike = numberLike
//                self.arrayHomeFeeds[index] = homeFeed
//                self.tbvHome.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
//            }
            
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func favouritePost(idFeed: Int, index: Int, isFavourite: Int){
        let homeFeed = self.arrayHomeFeeds[index]
        homeFeed.checkSave = isFavourite == 1 ? 0 : 1
        self.arrayHomeFeeds[index] = homeFeed
        self.tbvHome.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        
        APIManager.saveHomeFeed(id: idFeed, checkSave: isFavourite , callbackSuccess: { [weak self] (checkSave) in
            guard let self = self else { return }
            if checkSave != homeFeed.checkSave{
                let homeFeed = self.arrayHomeFeeds[index]
                homeFeed.checkSave = checkSave
                self.arrayHomeFeeds[index] = homeFeed
                self.tbvHome.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
            
            
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func handelLike(indexRow: Int, homefeed: HomeFeed){
        if Utils.checkLogin(selfView: self) {
            guard let id = homefeed.id else { return }
            guard let checkLike = homefeed.checkLike else { return }
            self.likePost(idFeed: id, index: indexRow, isLike: checkLike)
        }
    }
    
    func handelSave(indexRow: Int, homefeed: HomeFeed){
        if Utils.checkLogin(selfView: self) {
            guard let id = homefeed.id else { return }
            guard let checkSave = homefeed.checkSave else { return }
            self.favouritePost(idFeed: id, index: indexRow, isFavourite: checkSave)
        }
    }
    
    func handelComment(indexRow: Int, homefeed: HomeFeed){
        if Utils.checkLogin(selfView: self) {
            let bottomCommentTableView = BottomTableViewViewController.init(nib: R.nib.bottomTableViewViewController)
            bottomCommentTableView.topCornerRadius = 20
            bottomCommentTableView.presentDuration = 0.25
            bottomCommentTableView.dismissDuration = 0.25
            bottomCommentTableView.isFeed = homefeed.id ?? 0
            bottomCommentTableView.updateComment = {
                homefeed.numberComment = homefeed.numberComment ?? 0 + 1
                self.tbvHome.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .none)
            }
           // bottomCommentTableView.modalPresentationStyle = .custom
            Utils.getTopMostViewController()?.present(bottomCommentTableView, animated: true, completion: nil)
        }
    }
    
    // MARK: - Deinit
    
    deinit {
        print("Remove NotificationCenter Deinit")
        self.tbvHome?.removeObserver(self, forKeyPath: "contentSize")
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.updateBagde, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.checkEditHome, object: nil)
        
    }
}


// MARK: - UITableViewDelegate

extension HomeViewController: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayHomeFeeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.arrayHomeFeeds.count > 0{
            let homefeed = self.arrayHomeFeeds[indexPath.row]
            if homefeed.ads == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeTableViewCell, for: indexPath)!
                cell.setupDataHomeFeed(homeFeed: homefeed)
                cell.refreshhCell = {
                    tableView.beginUpdates()
                    tableView.endUpdates()
                }
                cell.tapComment = { self.handelComment(indexRow: indexPath.row, homefeed: homefeed) }
                cell.tapLike = { self.handelLike(indexRow: indexPath.row, homefeed: homefeed)}
                cell.tapSave = { self.handelSave(indexRow: indexPath.row, homefeed: homefeed) }
                
                return cell
            }else{
                let cellQC = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeQCTableViewCell, for: indexPath)!
                cellQC.setupDataHomeFeedQC(homeFeed: homefeed)
                cellQC.tapComment = { self.handelComment(indexRow: indexPath.row, homefeed: homefeed) }
                cellQC.tapLike = { self.handelLike(indexRow: indexPath.row, homefeed: homefeed)}
                cellQC.tapSave = { self.handelSave(indexRow: indexPath.row, homefeed: homefeed) }
                
                return cellQC
            }
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utils.checkLogin(selfView: self) {
            Router.share.pushToViewHomeDetail(tableView: tableView, array: self.arrayHomeFeeds,indexPath: indexPath, viewSelf: self, idFeed: arrayHomeFeeds[indexPath.row].id ?? 0, editFromView: .Home, completion: { homeFeeds in
                self.arrayHomeFeeds = homeFeeds
                self.tbvHome.reloadData()
            })
        } 
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        heightDictionary[indexPath] = cell.frame.size.height
    //
    //        if nextPage == 2 || (nextPage == 0 && self.arrayHomeFeeds.count < 10){
    //            cell.alpha = 0
    //            UIView.animate(
    //                withDuration: 0.3,
    //                delay: 0.05 * Double(indexPath.row),
    //                animations: {
    //                    cell.alpha = 1
    //            })
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath] = cell.frame.size.height
    }
}
