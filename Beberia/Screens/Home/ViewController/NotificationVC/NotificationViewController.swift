//
//  NotificationViewController.swift
//  Beberia
//
//  Created by iMAC on 9/5/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import Kingfisher
import PullToRefresh

struct SectionData {
    let title: String
    var data : [Noti]?
}

class NotificationViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tbvListNoti: UITableView!
    
    // MARK: - Properties
    
    var fetchedListNoti = [Noti]()
    var fetchedListNoti2 = [Noti]()
    var listNoti = [SectionData]()
    var page = 1
    var nextpage = 0
    let refresher = PullToRefresh()
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoaddingManager.initLoadding()
        setupUI()
        getListNoti(page:page)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("NotificationViewController111")
        tbvListNoti.isUserInteractionEnabled = true
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        tbvListNoti.register(R.nib.notificationTableViewCell)
        tbvListNoti.registerHeaderFooterView(R.nib.headerSectionTableViewCell)
        tbvListNoti.delegate = self
        tbvListNoti.dataSource = self
        tbvListNoti.separatorStyle = .none
        tbvListNoti.rowHeight = UITableView.automaticDimension
        tbvListNoti.estimatedRowHeight = UITableView.automaticDimension
        self.title = R.string.localizable.homeNotification()
        customLeftBarButton()
        
        tbvListNoti.infiniteScrollTriggerOffset = 700
        tbvListNoti.showsVerticalScrollIndicator = false
           tbvListNoti.addInfiniteScroll { (tableView) in
               tableView.performBatchUpdates({ () -> Void in
                   // update collection view
                   if self.nextpage > 0 {
                       self.page = self.page + 1
                       self.getListNoti(page: self.page)
                   }else {
                       self.tbvListNoti.finishInfiniteScroll(completion: { (collection) in
                       })
                   }
               }, completion: { (finished) -> Void in
                   // finish infinite scroll animations
                   tableView.finishInfiniteScroll()
               });
           }
        
        tbvListNoti.addPullToRefresh(refresher) {
            self.page = 1
            self.listNoti.removeAll()
            self.tbvListNoti.reloadData()
            self.getListNoti(page: self.page)
            self.tbvListNoti.startRefreshing(at: .top)
        }
    }
    
    // MARK: - Request API
    
    func getListNoti(page:Int){
        LoaddingManager.startLoadding()
        APIManager.getListNoti(page: page, callbackSuccess: { [weak self] (notifications, next_page) in
            guard let self = self else { return }
            self.nextpage = next_page
            if page == 1 {
                if notifications.count > 0 {
                    if notifications.count - 1 > 4 {
                        self.fetchedListNoti = Array(notifications[0...4])
                        self.fetchedListNoti2 = Array(notifications[5...notifications.count - 1])
                        self.listNoti.append(SectionData(title: R.string.localizable.homeNotificationLatest(), data: self.fetchedListNoti))
                        self.listNoti.append(SectionData(title: R.string.localizable.homeNotificationRecently(), data: self.fetchedListNoti2))
                    }else {
                        self.listNoti.append(SectionData(title:  R.string.localizable.homeNotificationLatest(), data: notifications))
                    }
                }
            }else {
                self.listNoti[1].data?.append(contentsOf: notifications)
            }
            
            self.tbvListNoti.reloadData()
            
            self.tbvListNoti.endRefreshing(at: .top)
            self.tbvListNoti.finishInfiniteScroll(completion: { (collection) in
            })
            LoaddingManager.stopLoadding()
        }) { (error) in
            LoaddingManager.stopLoadding()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func confirmAction(type: Int, section: Int, row: Int, listNotification: [SectionData]){
        APIManager.confirmCancelRequest(user_id: listNotification[section].data![row].userFrom?.id ?? 0, type: type, callbackSuccess: { (isFollow) in
            print(isFollow)
            
            // isFollow == 0 -> Chưa kết bạn -> Remove cell
            if isFollow == 0 {
                guard var data = listNotification[section].data else {
                    return
                }
                data.remove(at: row)
                
                self.listNoti[section].data = data
                self.tbvListNoti.reloadData()
            }
            
            // isFollow == 1 -> Đã kết bạn
            if isFollow == 1 {
                
                guard let data = listNotification[section].data else {
                    return
                }
                data[row].userFrom?.isFollow = isFollow
                
                self.listNoti[section].data = data
                self.tbvListNoti.reloadData()
            }
            
        }) { (error) in
            print(error)
        }
    }
}

// MARK: - UITableViewDelegate

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listNoti[section].data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.notificationTableViewCell, for: indexPath)!
        
        
        cell.viewAcceptCancel.isHidden = true
        
        let noti = NotificationList(rawValue: listNoti[indexPath.section].data?[indexPath.row].type ?? 0)
        switch noti {
        // Feed
        case .LikeFeed?, .LikeDiary?, .LikeInfo?:
            cell.setuplikeFeed(likeFeed : self.listNoti[indexPath.section], row: indexPath.row, typeNoti: noti!)
        case .LikeComment?, .LikeComtDiary?, .LikeComInfo?:
            cell.setuplikeComment(likeFeed : self.listNoti[indexPath.section], row: indexPath.row,typeNoti: noti!)
        case .LikeRepCom?, .LikeRepComDiary?, .LikeRepComInfo?:
            cell.setuplikeRepCom(likeFeed : self.listNoti[indexPath.section], row: indexPath.row, typeNoti: noti!)
        case .ComFeed?, .ComDiary?, .ComInfo?:
            cell.setupComFeed(likeFeed : self.listNoti[indexPath.section], row: indexPath.row, typeNoti: noti!)
        case .RepComt?, .RepComDiary?, .RepcomInfo?:
            cell.setupRepCom(likeFeed : self.listNoti[indexPath.section], row: indexPath.row, typeNoti: noti!)
            
        case .SENDREQUESTFRIEND:
            cell.setSenndRequestFriend(likeFeed : self.listNoti[indexPath.section], row: indexPath.row, typeNoti: noti!)
            cell.confirmActionFriend = { type in
                self.confirmAction(type: type, section: indexPath.section, row: indexPath.row, listNotification: self.listNoti)
            }
            
        default:
           return UITableViewCell()
        }
        cell.contentView.backgroundColor = listNoti[indexPath.section].data?[indexPath.row].read == 0 ?  UIColor.init(hexString: "eebf3b", alpha: 0.09) : .white
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listNoti.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.reuseIdentifier.headerSectionTableviewCell)!
        headerView.lblTitle.text = listNoti[section].title
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let notification = listNoti[indexPath.section].data?[indexPath.row] ?? Noti.init(json: "")
        let noti = NotificationList(rawValue: notification.type ?? 0)
        //  tbvListNoti.isUserInteractionEnabled = false
        switch noti {
        // done
        case .LikeFeed?:

            guard let selfStrong = Utils.getTopMostViewController() else {
                return
            }
            
            Router.share.pushToViewHomeDetail(tableView: UITableView.init(), array: [HomeFeed](),indexPath: IndexPath.init(), viewSelf: selfStrong, idFeed: notification.feed?.id ?? 0, editFromView: .Notification, completion: { homeFeeds in
            })
            
        case .LikeComment?, .ComFeed?, .LikeComtDiary?, .ComDiary?, .LikeComInfo?, .ComInfo?:
            //            let commentVC = CommentViewController.init(nib: R.nib.commentViewController)
            //            commentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            //            commentVC.isCheckFromNotification = true
            //            if noti == .LikeComment || noti == .ComFeed{
            //                commentVC.fromToView = .Home
            //            }else{
            //                commentVC.fromToView = .Diary
            //            }
            //
            //            commentVC.commentNotification = notification.comment
            //            commentVC.idPost = notification.feed?.id ?? 0
            //            self.navigationController?.pushViewController(commentVC, animated: true)
            
            if noti == .LikeComment || noti == .ComFeed{

                guard let selfStrong = Utils.getTopMostViewController() else {
                    return
                }
                Router.share.pushToViewHomeDetail(tableView: UITableView.init(), array: [HomeFeed](),indexPath: IndexPath.init(), viewSelf: selfStrong, idFeed: notification.feed?.id ?? 0, editFromView: .Notification,commentCommentNotification: notification.comment, completion: { homeFeeds in
                    
                })
                
            } else if noti == .LikeComInfo || noti == .ComInfo{
                self.pushPostDetail(array: [PostDetailModel](), idPost: notification.feed?.id ?? 0, 0,commentCommentNotification: notification.comment, completion: { (_) in
                    print("123")
                })
            } else {
                guard let selfStrong = Utils.getTopMostViewController() else {
                    return
                }
                Router.share.pushToViewDiaryDetail(tableView: UITableView.init(), array: [Diaries](), indexPath: IndexPath.init(),row: 0, viewSelf: selfStrong, idFeed: notification.feed?.id ?? 0, editFromView: .Notification,commentCommentNotification: notification.comment ) { (diary) in
                }
            }
            
            
            
        case .LikeRepCom?, .LikeRepComDiary?, .LikeRepComInfo?:
            // done
            let notiCommentVC = NotificationCommentViewController.init(nib: R.nib.notificationCommentViewController)
            //    if (notification.comment?.reply ?? [Comment]()).count > 0{
            let comment = notification.replyComment
            notiCommentVC.commentNotification = comment
            //     }
            notiCommentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            notiCommentVC.fromToView = noti == .LikeRepCom ? .Home : .Diary
            notiCommentVC.userFrom = notification.userFrom ?? User.init(json: "")
            notiCommentVC.commentRoot = notification.comment
            notiCommentVC.typeNotification = noti!
            notiCommentVC.idCommentParent = notification.comment?.commentId ?? 0
            self.navigationController?.pushViewController(notiCommentVC, animated: true)
            
        case .RepComt?, .RepComDiary?, .RepcomInfo?:
            
            let notiCommentVC = NotificationCommentViewController.init(nib: R.nib.notificationCommentViewController)
            //   if (notification.comment?.reply ?? [Comment]()).count > 0{
            let comment = notification.replyComment
            notiCommentVC.commentNotification = comment
            //   }
            notiCommentVC.updateDataWhenDeleteComment = { idNoti in
                if self.listNoti.count > 0{
                    for (index , value) in self.listNoti.enumerated(){
                        var notiTemp = value
                        let indexDelete = notiTemp.data?.firstIndex(where: { (notiResult) -> Bool in
                            return notiResult.id == idNoti
                        })
                        
                        if indexDelete != nil{
                            notiTemp.data?.remove(at: indexDelete!)
                            self.listNoti[index] = notiTemp
                            self.tbvListNoti.reloadData()
                            break
                        }
                    }
                    
                }
            }
            notiCommentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            notiCommentVC.fromToView = noti == .RepComt ? .Home : .Diary
            notiCommentVC.idNotification = notification.id ?? 0
            notiCommentVC.userFrom = notification.userFrom ?? User.init(json: "")
            notiCommentVC.commentRoot = notification.comment
            notiCommentVC.idCommentParent = notification.comment?.commentId ?? 0
            notiCommentVC.typeNotification = noti!
            self.navigationController?.pushViewController(notiCommentVC, animated: true)
            
        case .LikeDiary?:
            guard let selfStrong = Utils.getTopMostViewController() else {
                return
            }
            let id = notification.feed?.id ?? 0

            Router.share.pushToViewDiaryDetail(tableView: tbvListNoti, array: [Diaries](), indexPath: indexPath,row: indexPath.row, viewSelf: selfStrong, idFeed: id, editFromView: .Notification) { (diary) in
            }
            
        default:
            print("Noti")
        }
        
        
        APIManager.readNotification(id: notification.id ?? 0, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else { return }
            if isSuccess{
                print("Read Notication Success")
                notification.read = 1
                self.tbvListNoti.reloadData()
            }else{
                print("Read Notication Fail")
            }
        }) { (error) in
            print(error)
        }
    }
    
}
