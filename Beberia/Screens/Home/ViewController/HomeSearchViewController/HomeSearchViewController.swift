//
//  HomeSearchViewController.swift
//  Beberia
//
//  Created by iMAC on 8/30/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import TTGTagCollectionView
class HomeSearchViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var viewKeyHot: UIView!
    @IBOutlet weak var tagView: TTGTextTagCollectionView!
    @IBOutlet weak var tbvSearch: UITableView!
    
    // MARK: - Properties
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
    var arrSearch = [String]()
    var searchs = DBManager.sharedInstance.getDataFromDB().toArray()
    var indexSearch:Int? = 0
    
    // MARK: View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getHomeSearch()
        self.tabBarController?.delegate = self
    }
    
    //MARK: Helper methods
    
    func getHomeSearch(){
        arrSearch = [String]()
        for search in searchs {
            arrSearch.append(search.textSearch)
            DispatchQueue.main.async {
                self.tbvSearch.reloadData()
            }
        }
    }
    
    // MARK: SetupUI
    
    func setupUI(){
        viewKeyHot.isHidden = true
        self.title = "Tìm kiếm" 
        customLeftBarButton()
        tbvSearch.register(R.nib.searchHomeTableViewCell)
        tbvSearch.registerHeaderFooterView(R.nib.headerSectionTableViewCell)
        tagView.addTags(["iphone","toys","sport","kitchen","kitchen","entertaiment","beauty"])
        let config = tagView.defaultConfig!
        config.textColor = UIColor.init(hexString: AppColor.colorSelect,alpha: 0.6)
        config.selectedTextColor = UIColor.init(hexString: AppColor.colorSelect,alpha: 0.6)
        config.backgroundColor = UIColor.init(hexString: AppColor.colorBGMenu)
        config.selectedBackgroundColor = UIColor.init(hexString: AppColor.colorBGMenu)
        config.cornerRadius = 15
        config.selectedCornerRadius = 15
        config.shadowOffset = CGSize.zero
        config.shadowRadius = 0
        config.shadowOpacity = 0
        config.exactHeight = 30
        tagView.enableTagSelection = true
        tagView.defaultConfig = config
        tagView.setTagAt(0, selected: true)
        tagView.delegate = self
        tagView.reload()
        tbvSearch.delegate = self
        tbvSearch.dataSource = self
        tbvSearch.tableFooterView = UIView()
        let searchBar = UISearchBar()
        searchBar.placeholder = R.string.localizable.homeSearch()
        searchBar.delegate = self
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        navigationItem.titleView = searchBar
    }
    
}

// MARK: - TTGTextTagCollectionViewDelegate

extension HomeSearchViewController: TTGTextTagCollectionViewDelegate{
    func textTagCollectionView(_ textTagCollectionView: TTGTextTagCollectionView!, didTapTag tagText: String!, at index: UInt, selected: Bool, tagConfig config: TTGTextTagConfig!) {
        tagView.setTagAt(0, selected: false)
        tagView.setTagAt(1, selected: false)
        tagView.setTagAt(index, selected: true)
        let homeVC = HomeInfo.init(nib: R.nib.homeInfo)
        homeVC.txtsearch = tagText ?? ""
        homeVC.indext = indexSearch ?? 0
        homeVC.hidesBottomBarWhenPushed = true
        Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: true)
    }
}

// MARK: - UITableViewDelegate

extension HomeSearchViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.searchHomeTableViewCell, for: indexPath)!
        cell.lblName.text = arrSearch[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.reuseIdentifier.headerSectionTableviewCell)!
        headerView.lblTitle.text = R.string.localizable.homeHistorySearch()
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let homeVC = HomeInfo.init(nib: R.nib.homeInfo)
        homeVC.txtsearch = arrSearch[indexPath.row]
        homeVC.indext = indexSearch ?? 0
        homeVC.hidesBottomBarWhenPushed = true
        Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: false)
    }
}

// MARK: - UISearchBarDelegate

extension HomeSearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let itemSearchs = ItemSearch()
        let item = DBManager.sharedInstance.getDataFromDB().filter(NSPredicate(format: "textSearch = %@", searchBar.text ?? ""))
        if item.count < 1 {
            for i in item {
                DBManager.sharedInstance.deleteFromDb(object: i)
            }
            itemSearchs.textSearch = searchBar.text ?? ""
            itemSearchs.timeStamp = Date().timeIntervalSince1970
            DBManager.sharedInstance.addData(object: itemSearchs)
            getHomeSearch()
            let homeVC = HomeInfo.init(nib: R.nib.homeInfo)
            homeVC.txtsearch = searchBar.text ?? ""
            homeVC.indext = indexSearch ?? 0
            homeVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: false)
        }else {
            itemSearchs.textSearch = searchBar.text ?? ""
            itemSearchs.timeStamp = Date().timeIntervalSince1970
            DBManager.sharedInstance.addData(object: itemSearchs)
            getHomeSearch()
            let homeVC = HomeInfo.init(nib: R.nib.homeInfo)
            homeVC.txtsearch = searchBar.text ?? ""
            homeVC.indext = indexSearch ?? 0
            homeVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
}
