//
//  HomeDetailViewController.swift
//  Beberia
//
//  Created by IMAC on 8/30/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import Kingfisher
import TTGTagCollectionView
import SVProgressHUD
import FFPopup
import SDWebImage
import RxCocoa
import RxSwift

class HomeDetailViewController: BaseViewController, UIScrollViewDelegate {
    
    struct Constant {
        static let widthBE: String = "width:100%"
        static let withReplace: String = "width:"
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var lblDateSecret: UILabel!
    @IBOutlet weak var lblBBSecret: UILabel!
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet weak var tvInputComment: UITextView!
    @IBOutlet weak var heightConstraintViewInput: NSLayoutConstraint!
    @IBOutlet weak var heightViewInput: NSLayoutConstraint!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblNameUserReply: UILabel!
    @IBOutlet weak var viewNotiReplyComment: UIView!
    @IBOutlet weak var btnViewMoreCommnet: UIButton!
    @IBOutlet weak var heightbtnViewMoreCommnet: NSLayoutConstraint!
    @IBOutlet weak var heightTbvComment: NSLayoutConstraint!
    @IBOutlet weak var tbvComment: CommentTableView!
    @IBOutlet weak var heightViewInfoUser: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightTbv: NSLayoutConstraint!
    @IBOutlet weak var tbvContent: UITableView!
    @IBOutlet weak var lblDateCreate: UILabel!
    @IBOutlet weak var lblNameUser: UILabel!
    @IBOutlet weak var imgUserCreate: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewLikeComment: UIView!
    @IBOutlet weak var imgAvaUser: UIImageView!
    @IBOutlet weak var lblDisplayName: UILabel!
    @IBOutlet weak var lblNumberLike: UILabel!
    @IBOutlet weak var lblNumberCom: UILabel!
    @IBOutlet weak var widthAvatar: NSLayoutConstraint!
    @IBOutlet weak var heightTagView: NSLayoutConstraint!
    @IBOutlet weak var viewAcceptCancel: UIView!
    @IBOutlet weak var lblInfoPost: UILabel!
    @IBOutlet weak var viewAcceptCancelFollow: ViewAcceptCancelFollow!
    @IBOutlet weak var heightViewInfo: NSLayoutConstraint!
    @IBOutlet weak var bottomView: NSLayoutConstraint!
    
    // MARK: - Properties
    
    var isHidenButtonViewMoreComment = true{
        didSet{
            //   btnViewMoreCommnet.isHidden = !isHidenButtonViewMoreComment
            //   heightbtnViewMoreCommnet.constant = !isHidenButtonViewMoreComment ? 0 : 40
        }
    }
    
    var isHidenViewInput = true{
        didSet{
            UIView.animate(withDuration: 0.5) {
                self.viewInput.isHidden = self.isHidenViewInput
                self.heightConstraintViewInput.constant = self.isHidenViewInput ? 0 : 50
            }
        }
    }
    
    
    var imageHeights = [CGFloat]()
    var letheStretchyHeader: LetheStretchyHeader?
    var viewLikeAndComment: ViewLikeComment = ViewLikeComment.instanceFromNib()
    var viewLikeCommentFeed: ViewLikeCommentFeed = ViewLikeCommentFeed.instanceFromNib()
    var homeFeed = HomeFeed.init(json: "")
    var idFeed = 0
    var updateObject: (_ homeFeedUpdate : HomeFeed) -> () = {_ in}
    var deleteObject: (_ idFeed : Int) -> () = {_ in}
    var isHomeVC = false
    var heightImage = [CGFloat]()
    fileprivate var heightDictionary: [Int : CGFloat] = [:]
    var editFromView = EditFromView.Home
    var titleVC = ""
    var fromToView = FromToview.Home
    var isTyping = false
    var isFristTyping = false
    var commentNotification: Comment? = nil
    
    // MARK: View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isHidenViewInput = true
        setupUI()
        LoaddingManager.initLoadding(view: self.view)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            switch self.fromToView {
            case .Home:
                self.getDetailHomeFeed()
            case .Diary:
                self.getDetailDiary()
            default:
                print("!21212")
            }
        }
        self.setupRX()
        
    }
    
    override func backVC() {
        updateObject(homeFeed)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBottomBorderColor(color: .gray)
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBottomBorderColor(color: .white)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    

    
    // MARK: SetupUI
    
    func setupUI(){
        self.extendedLayoutIncludesOpaqueBars = true
        self.title = titleVC
        customLeftBarButton()
        tbvContent.register(R.nib.homeDetailTableViewCell)
        tbvContent.delegate = self
        tbvContent.dataSource = self
        tbvContent.separatorStyle = .none
        tbvContent.allowsSelection = false
        tbvContent.rowHeight = UITableView.automaticDimension
        tbvContent.isScrollEnabled = false
        scrollView.delegate = self
        
        tbvContent.rx.observe(CGSize.self, "contentSize").asObservable().observeOn(MainScheduler.asyncInstance).subscribe { [weak self] (size) in
            guard let self = self else { return }
            self.heightTbv.constant = size.element!!.height
//                        UIView.animate(withDuration: 0.3) {
//                            self.updateViewConstraints()
//                            self.view.layoutIfNeeded()
//                        }
        }.disposed(by: disposeBag)
        
        tbvComment.rx.observe(CGSize.self, "contentSize").asObservable().observeOn(MainScheduler.asyncInstance).subscribe { [weak self] (size) in
            guard let self = self else { return }
            self.heightTbvComment.constant = size.element!!.height
//                        UIView.animate(withDuration: 0.2) {
//                            self.updateViewConstraints()
//                            self.view.layoutIfNeeded()
//                        }
        }.disposed(by: disposeBag)

        if isHomeVC && homeFeed.ads == 1{
            initialHeader()
            title = "Bài quảng cáo"
            lblInfoPost.isHidden = true
            viewAcceptCancelFollow.isHidden = true
            heightViewInfoUser.constant = 70
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        }

        lblInfoPost.isHidden = isHomeVC ? false : true
        if isHomeVC && homeFeed.ads == 0{
            lblInfoPost.text = "\(homeFeed.cityName?.locationNameVi ?? "") - \(CategoryFeed.init(rawValue: homeFeed.category ?? 1 )?.title ?? "") - \(AboutFeed.init(rawValue: homeFeed.about ?? 1 )?.title ?? "")"
        }

        imgUserCreate.cornerRadius = imgUserCreate.frame.height / 2
        heightViewInfoUser.constant = isHomeVC ? 100 : 75


        lblBBSecret.isHidden = editFromView != .Secret
        lblDateSecret.isHidden = editFromView != .Secret

        lblNameUser.isHidden = editFromView == .Secret
        lblDisplayName.isHidden = editFromView == .Secret
        lblDateCreate.isHidden = editFromView == .Secret
      //  imgAvaUser.isHidden = editFromView == .Secret
        imgUserCreate.isHidden = editFromView == .Secret

        if editFromView == .Secret {
            title = R.string.localizable.secretTile_Detail()
            lblInfoPost.isHidden = true
            viewAcceptCancelFollow.isHidden = true
            heightViewInfoUser.constant = 70
          //  heightTagView.constant = 0
        }

        if Utils.checkLogin(selfView: Utils.getTopMostViewController()!, showAlert: false){
            if editFromView != .Secret{ customrightBarButton() }

        }

        if fromToView == .Diary {
            lblInfoPost.isHidden = true
            viewAcceptCancelFollow.isHidden = true
        }

        NotificationCenter.default.addObserver(self, selector: #selector(updateNumberComment), name: NSNotification.Name.updateNumberComment, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateNumberLike), name: NSNotification.Name.updateNumberLike, object: nil)



        tbvComment.tapReaply = { name in
            self.isHidenViewInput = false
            self.lblNameUserReply.text = ""
            self.viewNotiReplyComment.isHidden = true

            self.lblNameUserReply.text = name
            self.viewNotiReplyComment.isHidden = false
            self.showViewNotiReply()
        }

        tbvComment.showButtonViewMoreComment = { isShow in
            self.isHidenButtonViewMoreComment = isShow
        }

        initTextViewComment()
        
        switch fromToView {
        case .Home:
            viewLikeCommentFeed.frame = viewLikeComment.bounds
            viewLikeComment.addSubview(viewLikeCommentFeed)
         //   viewLikeCommentFeed.tapComment = {
         //       self.scrollView.scrollToBottom(animated: true)
                self.isHidenViewInput = false
        //        self.tvInputComment.becomeFirstResponder()
                self.isFristTyping = true
        //    }
            heightViewInfo.constant = 0
            viewInfo.isHidden = true
            viewLine.isHidden = false
        default:
            viewLine.isHidden = true
            viewInfo.isHidden = false
            heightViewInfo.constant = 110
            viewLikeAndComment.frame = viewLikeComment.bounds
            viewLikeAndComment.btnLike.isEnabled = false
            viewLikeComment.addSubview(viewLikeAndComment)
            viewLikeAndComment.tapButtonComment = {
                self.scrollView.scrollToBottom(animated: true)
                self.isHidenViewInput = false
                self.tvInputComment.becomeFirstResponder()
                self.isFristTyping = true
            }
        }
        
    }
    
    private func setupRX() {
        let show = NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification).map { KeyboardInfo($0) }
        let hide = NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification).map { KeyboardInfo($0) }
        
        Observable.merge(show, hide).bind { [weak self] keyboard in
            guard let wSelf = self else { return }
            wSelf.runAnimate(by: keyboard)
        }.disposed(by: disposeBag)
    }
    
    private func initialHeader() {
        if let url = URL(string: homeFeed.cover?[0].link ?? ""){
            let data = NSData(contentsOf: url)
            LetheStretchyHeader().initial(viewController: self, parentView: scrollView, customHeader: nil, image: UIImage(data: data! as Data), height: 170, type: . afterShowNavigationBar,completionMore: {index in
                self.showAtionTunrOnOff(turnOnOff: self.homeFeed.turnOn ?? 0, id: self.homeFeed.id ?? 0)
            }, completion :{ index in
                self.backVC()
                self.navigationController?.setNavigationBarHidden(true, animated: false)
            })
        }
    }
    
    @objc func pushProfileVC(tapGestureRecognizer: UITapGestureRecognizer){
        let user = homeFeed.userCreate
        let ProfileVC = R.storyboard.main.profileViewController()
        ProfileVC?.hidesBottomBarWhenPushed = true
        ProfileVC?.check = 1
        ProfileVC?.user = user!
        
        ProfileVC?.updateStatusUser = { [weak self] user in
            // refesh status view accept cancel
//                   guard let isFollow = user.isFollow else {
//                       return
//                   }
            var isFollow = 5
            if (self?.homeFeed.userCreate ?? User.init(json: "")).id ?? 0 != UserInfo.shareUserInfo.id {
                isFollow = (self?.homeFeed.userCreate ?? User.init(json: "")).isFollow ?? 0
            }
            self?.viewAcceptCancelFollow.checkShowButton(isFollow: isFollow)
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(ProfileVC!, animated: true)
    }
    
    
    @objc func updateNumberComment(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        let numberComment = object[Key.KeyNotification.numberComment] as? Int
        viewLikeAndComment.homeFeed.numberComment = numberComment
        viewLikeAndComment.numberComment = numberComment ?? 0
        lblNumberCom.text = "\(numberComment ?? 0) bình luận"
    }
    
    @objc func updateNumberLike(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        let numberComment = object[Key.KeyNotification.numberLike] as? Int
        lblNumberLike.text = "\(numberComment ?? 0) thích"
        
    }
    
    func customrightBarButton(){
        let rightBarButtonItemOption = UIBarButtonItem.menuButton(self, action: #selector(HomeDetailViewController.tapOption), imageName: R.image.ic_option1() ?? UIImage.init(), height: 40, width: 40)
        self.navigationItem.rightBarButtonItem = rightBarButtonItemOption
    }
    
    func initTextViewComment(){
        tvInputComment.cornerRadius = 15
        tvInputComment.delegate = self
        tvInputComment.text =  R.string.localizable.infomationWriteComment()
        tvInputComment.textColor = UIColor.lightGray
        btnSend.isEnabled = true
        if  tvInputComment.textColor == UIColor.lightGray{
            btnSend.isEnabled = false
        }
        tvInputComment.autocorrectionType = .no
        viewNotiReplyComment.isHidden = true
    }
    
    func initViewInput(){
        self.tvInputComment.text =  R.string.localizable.infomationWriteComment()
        self.tvInputComment.textColor = UIColor.lightGray
        self.heightViewInput.constant = 33
        self.heightConstraintViewInput.constant = 55
        Utils.getTopMostViewController()?.view.endEditing(true)
    }
    
    @IBAction func didPressInfoUser(_ sender: Any) {
        
               let bottomVC = BaseBottomViewController.init(nib: R.nib.baseBottomViewController)
               //  print(bottomVC.w)
               bottomVC.selectedOption = { indexSelect in
                   print(indexSelect)
                   Utils.getTopMostViewController()?.dismiss(animated: true, completion: {})
               }
               bottomVC.isNoTextColorRed = false
               bottomVC.datasource = ["Chặn", "Bỏ theo dõi", "Huỷ kết bạn"]
               bottomVC.height = CGFloat((bottomVC.datasource.count + 1) * 44)
               bottomVC.topCornerRadius = 20
               bottomVC.presentDuration = 0.25
               bottomVC.dismissDuration = 0.25
               Utils.getTopMostViewController()?.navigationController?.present(bottomVC, animated: true)
           
    }
    
    @IBAction func didPressSendComment(_ sender: UIButton) {
        
        guard let content = tvInputComment.text?.trimmingCharacters(in: .whitespacesAndNewlines), content.count > 0 else {
            return
        }
        sender.preventRepeatedPresses()
         
        tbvComment.didPressSendComment(content: content, completion: {
            self.initViewInput()
            self.lblNameUserReply.text = ""
            self.tvInputComment.text = ""
            self.viewNotiReplyComment.isHidden = true
        })
    }
    
    @IBAction func didPressCloseViewNotiReply(_ sender: Any) {
        lblNameUserReply.text = ""
        viewNotiReplyComment.isHidden = true
        tbvComment.parentId = nil
        tbvComment.indexParent = 0
        tbvComment.userIdTag = []
        scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func showViewNotiReply(){
        scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 40, right: 0)
        UIView.animate(withDuration: 0.25) {
            self.viewNotiReplyComment.center.y -= self.viewNotiReplyComment.bounds.height
        }
    }
    
    @objc func tapOption(){
        if UserInfo.shareUserInfo.id == homeFeed.userCreate?.id ?? 0 {
            if self.isHomeVC == true {
                showAtionDeleteEditFeed(turnOnOff: homeFeed.turnOn ?? 0, id: homeFeed.id ?? 0)
            }else {
                showAtionDeleteEditFeed(turnOnOff: homeFeed.turnOn ?? 0, id: homeFeed.id ?? 0)
            }
            
        }else{
            if self.isHomeVC == true {
                showAtionTunrOnOff(turnOnOff: homeFeed.turnOn ?? 0, id: homeFeed.id ?? 0)
            }else {
                showAtionTunrOnOff(turnOnOff: homeFeed.turnOn ?? 0, id: homeFeed.id ?? 0)
            }
        }
    }
    
    func loadData(){
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(pushProfileVC(tapGestureRecognizer:)))
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(pushProfileVC(tapGestureRecognizer:)))
        imgUserCreate.isUserInteractionEnabled = true
        imgUserCreate.addGestureRecognizer(tapGestureRecognizer)
        
        imgAvaUser.isUserInteractionEnabled = editFromView != .Secret
        imgAvaUser.addGestureRecognizer(tapGestureRecognizer1)
        lblNameUser.text = homeFeed.userCreate?.displayName
        lblDisplayName.text = homeFeed.userCreate?.displayName
        lblBBSecret.text = "#BB\(homeFeed.id!)"
        lblDateSecret.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(homeFeed.created ?? 0)), format: "dd/MM/yy - HH:mm")
        lblNumberLike.text = "\(homeFeed.numberLike ?? 0) thích"
        lblNumberCom.text = "\(homeFeed.numberComment ?? 0) bình luận"
        
        if editFromView == .Secret{
            imgUserCreate.image = R.image.avaDefautSecret()
            imgAvaUser.image = R.image.avaDefautSecret()
        }else{
//            imgUserCreate.kf.setImage(with: URL.init(string: homeFeed.userCreate?.avatar ?? "") ,placeholder: UIImage.init(named: Key.ImagePlaceholder))
//            imgAvaUser.kf.setImage(with: URL.init(string: homeFeed.userCreate?.avatar ?? "") ,placeholder: UIImage.init(named: Key.ImagePlaceholder))
            imgUserCreate.setImage(imageString: homeFeed.userCreate?.avatar ?? "")
            imgAvaUser.setImage(imageString: homeFeed.userCreate?.avatar ?? "")
        }

        lblTitle.text = (homeFeed.title ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        
        if isHomeVC && homeFeed.ads == 1{
            lblDateCreate.text = "Được tài trợ"
            lblDateCreate.textColor = .lightGray
        }else{
          //  lblDateCreate.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(homeFeed.created ?? 0)), format: "dd/MM/yy - HH:mm")
            
            lblDateCreate.text = Date.init(timeIntervalSince1970: TimeInterval(homeFeed.created ?? 0)).timeAgoDisplayHomeFeed()
        }
        
        if editFromView == .Secret{
            viewLikeAndComment.fromToView = .Secret
        }else{
            viewLikeAndComment.fromToView = isHomeVC ? .Home : .Diary
        }
        
        fromToView = isHomeVC ? .Home : .Diary
        
        
        
        if fromToView == .Home {
            viewLikeCommentFeed.setupData(homeFeed: homeFeed)
            viewLikeCommentFeed.tapLike = { feed in
                self.homeFeed = feed
            }
            viewLikeCommentFeed.tapSave = { feed in
                self.homeFeed = feed
            }
          //  viewLikeCommentFeed.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 0)
        }else{
            viewLikeAndComment.homeFeed = homeFeed
            viewLikeAndComment.loadData()
            viewLikeAndComment.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 0)
        }
        
        
        viewInput.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 5)
        
        switch fromToView {
        case .Info, .Diary:
            tbvComment.fromToView = .Info
        case .Home,.Secret:
            tbvComment.fromToView = .Home
        }
        tbvComment.idPost = homeFeed.id ?? 0
        tbvComment.commentNotification = commentNotification
        tbvComment.scrollView = scrollView
        tbvComment.numberComment = homeFeed.numberComment ?? 0
      //  tbvComment.reloadData()
        
        viewAcceptCancelFollow.userCreate = homeFeed.userCreate ?? User.init(json: "")

        var isFollow = 5
        if (homeFeed.userCreate ?? User.init(json: "")).id ?? 0 != UserInfo.shareUserInfo.id {
            isFollow = (homeFeed.userCreate ?? User.init(json: "")).isFollow ?? 0
        }
        viewAcceptCancelFollow.checkShowButton(isFollow: isFollow)
    }
    
    func getImage(){
        for media in homeFeed.media ?? [Media]() {
            KingfisherManager.shared.retrieveImage(with: URL.init(string: media.link ?? "")! , options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                guard let img = image else { return }
                
          //       let heihgtIamge = self.tbvContent.frame.width / (img.getCropRatio() <= 1 ? img.getCropRatio() : img.getCropRatio() - 0.2 )
                let heihgtIamge = self.tbvContent.frame.width / img.getCropRatio()
                print("heihgtIamge\(heihgtIamge)")
                self.imageHeights.append(heihgtIamge)
                if self.homeFeed.media?.count == self.imageHeights.count {
                    self.tbvContent.reloadData()
                }
            })
        }
        
    }
    
    func deletePost(){
        SVProgressHUD.show()
        APIManager.cancelAndDelete(id: homeFeed.id ?? 0, type: 1, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else { return }
            if isSuccess {
                self.navigationController?.popViewController(animated: true)
                self.deleteObject(self.homeFeed.id ?? 0)
                
            }
            SVProgressHUD.dismiss()
        }) {(error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        }
    }
    
    func deleteDiary(){
        SVProgressHUD.show()
        APIManager.deleteDiary(id: homeFeed.id ?? 0, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else { return }
            if isSuccess {
                self.navigationController?.popViewController(animated: true)
                self.deleteObject(self.homeFeed.id ?? 0)
                //                NotificationCenter.default.post(name: NSNotification.Name.deleteDiary, object:nil )
                //                 NotificationCenter.default.post(name: NSNotification.Name.updateDiaryNew, object: ["DiaryNew": diary], userInfo: nil)
            }
            SVProgressHUD.dismiss()
        }) {(error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        }
    }
    
    func pushToEditVC(){
        if self.isHomeVC{
            let writeVC = WriteHomeNewViewController.init(nib: R.nib.writeHomeNewViewController)
            writeVC.homeFeed = homeFeed
            writeVC.editFromView = editFromView
            writeVC.fromView = self.isHomeVC ? .EditHome : .EditDiary
            self.navigationController?.pushViewController(writeVC, animated: true)
        }else{
            let writeVC = WriteHomeViewController.init(nib: R.nib.writeHomeViewController)
            writeVC.homeFeed = homeFeed
            writeVC.editFromView = editFromView
            writeVC.fromView = self.isHomeVC ? .EditHome : .EditDiary
            self.navigationController?.pushViewController(writeVC, animated: true)
        }
    }
    
    func showAtionDeleteEditFeed(turnOnOff:Int, id:Int){
        var onOff = R.string.localizable.homeTunrOnNoti()
        if turnOnOff == 1 {
            onOff = R.string.localizable.homeTunrOffNoti()
        }
        self.showActionBottom(datasource: [R.string.localizable.homeEditPost(),onOff, R.string.localizable.homeDelete()], isNoTextColorRed: false) { (index) in
            switch index {
            case 0:
                self.pushToEditVC()
            case 1:
                if self.isHomeVC == true {
                    self.turnOnOff(turnOn: turnOnOff, id: id)
                }else {
                    self.settingNoti(turnOn: turnOnOff, id: id)
                }
            case 2:
                let viewPopupDelete =  ViewPopupDelete.instanceFromNib()
                viewPopupDelete.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width - 40, height: 126)
                let popup = FFPopup.init(contentView: viewPopupDelete)
                
                viewPopupDelete.tapDelete = {
                    
                    self.isHomeVC ? self.deletePost() : self.deleteDiary()
                    popup.dismiss(animated: true)
                }
                
                viewPopupDelete.tapCancel = {
                    popup.dismiss(animated: true)
                }
                
                popup.showType = .growIn
                popup.dismissType = .shrinkOut
                let layout = FFPopupLayout(horizontal: .center, vertical: .center)
                popup.show(layout: layout)
                
            default:
                print("123")
            }
        }
    }
    
    func turnOnOff(turnOn:Int, id:Int ){
        SVProgressHUD.show()
        APIManager.turnOnOffNotification(id: id, turnOn: turnOn, callbackSuccess: { [weak self] (isSucces) in
            guard let self = self else { return }
            self.homeFeed.turnOn = isSucces
            SVProgressHUD.dismiss()
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        })
    }
    
    func settingNoti(turnOn:Int, id:Int ){
        SVProgressHUD.show()
        APIManager.settingNotification(id: id, turnOn: turnOn, callbackSuccess: { [weak self] (isSucces) in
            guard let self = self else { return }
            self.homeFeed.turnOn = isSucces
            SVProgressHUD.dismiss()
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        })
    }
    
    func showAtionTunrOnOff(turnOnOff:Int, id:Int){
        var onOff = R.string.localizable.homeTunrOnNoti()
        if turnOnOff == 1 {
            onOff = R.string.localizable.homeTunrOffNoti()
        }
        self.showActionBottom(datasource: [onOff], isNoTextColorRed: false) { (index) in
            switch index {
            case 0:
                if self.isHomeVC == true {
                    
                    self.turnOnOff(turnOn: turnOnOff, id: id)
                }else {
                    self.settingNoti(turnOn: turnOnOff, id: id)
                }
                
            default:
                print("123")
            }
        }
    }
    
    func stringAddFontSizeAttributedString(content: String)->NSAttributedString{
        var content1 = content
        content1 = "<span" + Key.FontSizeAttribuite.FontSize + content1 + "</span>"
        return content1.html2AttributedString ?? NSAttributedString.init(string: "")
    }
    
    func updateHeightTextView(_ textView: UITextView){
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
        if ConstantApp.shared.getHeightSafeArea(type: .bottom) > 0 {
            heightConstraintViewInput.constant = newFrame.height + ConstantApp.shared.getHeightSafeArea(type: .bottom)
        } else {
            heightConstraintViewInput.constant = newFrame.height + 22
        }
        
        heightViewInput.constant = newFrame.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    deinit {
        self.tbvContent?.removeObserver(self, forKeyPath: "contentSize")
        self.tbvComment?.removeObserver(self, forKeyPath: "contentSize")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.updateNumberComment, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.updateNumberLike, object: nil)
    }
    
    //I don't know, why I set -300, then the UI shows correctly
    private func runAnimate(by keyboarInfor: KeyboardInfo?) {
        guard let i = keyboarInfor else {
            return
        }
        let h = i.height
        let d = i.duration
        let heightViewLikeComment: CGFloat = 45
        UIView.animate(withDuration: d) {
            if ConstantApp.shared.getHeightSafeArea(type: .bottom) > 0 {
                self.bottomView.constant = (h > 0) ? -300 : 0
            } else {
                self.bottomView.constant = (h > 0) ? (-300 + heightViewLikeComment) : 0
            }
            
        }
    }
    
}

// MARK: - UITableViewDelegate

extension HomeDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (homeFeed.media?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbvContent.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeDetailTableViewCell, for: indexPath)!
        cell.btnTapImage.tag = indexPath.row
        
        cell.tapImage = { index in
//            let viewImageOfGif = ViewImageOrGifViewController.init(nib: R.nib.viewImageOrGifViewController)
//            viewImageOfGif.homeFeed = self.homeFeed.media![index]
//            viewImageOfGif.modalPresentationStyle = .overCurrentContext
//            self.present(viewImageOfGif, animated: true, completion: nil)
        }
        
        if indexPath.row != 0{
            let media = homeFeed.media![indexPath.row - 1]
            
            var content1 = media.descriptionValue ?? ""
            if !(media.descriptionValue ?? "").contains("<span"){
                content1 = "<span" + Key.FontSizeAttribuite.FontSize + (media.descriptionValue ?? "") + "</span>"
            }
            
            let data = content1.data(using: String.Encoding.unicode)! // mind "!"
            let attrStr = try? NSMutableAttributedString( // do catch
                data: data,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
                documentAttributes: nil)
            cell.heightTextView.constant = 0
            if let attrStr = attrStr , attrStr.string != ""{
                cell.textView.attributedText = attrStr
            }else{
                cell.heightTextView.constant = 0
            }

         //   cell.textView.attributedText = self.stringAddFontSizeAttributedString(content: media.descriptionValue ?? "")
            
            //cell.textView.isEditable = false
            // set nếu là admin cho click link
            if isHomeVC && homeFeed.ads == 1{
                cell.textView.isClickLink = true
            }
            
         //   cell.imgPost.kf.setImage(with: URL.init(string: media.link ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
          cell.imgPost.setImage(imageString: media.link ?? "")
            
            print(media.link ?? "")

            if self.imageHeights.count == self.homeFeed.media?.count {
                cell.heightImg.constant = self.imageHeights[indexPath.row - 1]
                UIView.animate(withDuration: 0.2) {
                    cell.contentView.layoutIfNeeded()
                    self.tbvContent.beginUpdates()
                    self.tbvContent.endUpdates()
                }
            }
        }else{
            cell.heightImg.constant = 0
            
//            let content = (homeFeed.content ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
//            print(content)
//            cell.textView.attributedText = self.stringAddFontSizeAttributedString(content: content)
            
            var content = homeFeed.content ?? ""
            if content.suffix(3) == "<p>"{
                content = String(content.prefix(content.count - 3))
            }
            
            content = content.trimmingCharacters(in: .whitespacesAndNewlines)
            let width = cell.textView.frame.width
            let text = content.replace(string: Constant.widthBE, replacement: "\(Constant.withReplace)\(width)")
            cell.textView.attributedText =  self.stringAddFontSizeAttributedString(content: text)
            cell.textView.textColor = R.color.f3C()

            // set nếu là admin cho click link
            if isHomeVC && homeFeed.ads == 1{
                cell.textView.isClickLink = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath.row] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath.row]
        return height ?? UITableView.automaticDimension
    }
}

// MARK: - UITextViewDelegate

extension HomeDetailViewController: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        
        updateHeightTextView(textView)
        btnSend.isEnabled = true
        isTyping = true
        
        if (tvInputComment.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! || tvInputComment.text.count < 1{
            btnSend.isEnabled = false
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        updateHeightTextView(textView)
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.isFristTyping = false
        isTyping = false
        if textView.text.isEmpty {
            tvInputComment.text =  R.string.localizable.infomationWriteComment()
            textView.textColor = UIColor.lightGray
        }
    }
    
}

extension UIScrollView {
    
    // Scroll to a specific view so that it's top is at the top our scrollview
    func scrollToView(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x:0, y:childStartPoint.y,width: 1,height: self.frame.height), animated: animated)
        }
    }
}

extension HomeDetailViewController{
    func getDetailHomeFeed(){
    //    SVProgressHUD.show()
        LoaddingManager.startLoadding()
        APIManager.getDetailFeed(id: idFeed) { (homeFeeds) in
            self.homeFeed = homeFeeds
            self.tbvContent.reloadData()
            self.getImage()
            self.loadData()
         //   SVProgressHUD.dismiss()
            LoaddingManager.stopLoadding()
        } failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
         //   SVProgressHUD.dismiss()
            LoaddingManager.stopLoadding()
        }
    }
    
    func getDetailDiary(){
  //      SVProgressHUD.show()
        LoaddingManager.startLoadding()
        APIManager.getDetailDiary(id: idFeed) { (homeFeeds) in
            self.homeFeed = homeFeeds
            self.tbvContent.reloadData()
            self.getImage()
            self.loadData()
        //    SVProgressHUD.dismiss()
            LoaddingManager.stopLoadding()
        } failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
          //  SVProgressHUD.dismiss()
            LoaddingManager.stopLoadding()
        }
    }
}
//extension StringProtocol {
//    func index<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
//        range(of: string, options: options)?.lowerBound
//    }
//    func endIndex<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
//        range(of: string, options: options)?.upperBound
//    }
//    func indices<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Index] {
//        ranges(of: string, options: options).map(\.lowerBound)
//    }
//    func ranges<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Range<Index>] {
//        var result: [Range<Index>] = []
//        var startIndex = self.startIndex
//        while startIndex < endIndex,
//            let range = self[startIndex...]
//                .range(of: string, options: options) {
//                result.append(range)
//                startIndex = range.lowerBound < range.upperBound ? range.upperBound :
//                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
//        }
//        return result
//    }
//}
