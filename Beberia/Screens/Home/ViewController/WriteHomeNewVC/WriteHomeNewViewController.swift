//
//  WriteHomeNewViewController.swift
//  Beberia
//
//  Created by Lap on 1/5/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Alamofire
import Kingfisher

class WriteHomeNewViewController: BaseViewController {

    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var heightViewImage: NSLayoutConstraint!
    @IBOutlet weak var heightTextViewTitle: NSLayoutConstraint!
    @IBOutlet weak var heightTextViewContent: NSLayoutConstraint!
    @IBOutlet weak var viewImage: ViewImage!
    @IBOutlet weak var textViewContent: RichTextEditor!
    @IBOutlet weak var textViewTitle: UITextView!
    @IBOutlet weak var viewStyleText: UIView!
    @IBOutlet weak var constraintBottomViewStyleText: NSLayoutConstraint!
    
    @IBOutlet weak var tfPlacehoder: UITextField!
    var viewSelectStyleText: ViewOptionStyleText = ViewOptionStyleText.instanceFromNib()
    var typeAbout = 2
    var typeCategory = 1
    var fromView = FromView.Home
    var itemPosts = [ItemPost]()
    var homeFeed = HomeFeed.init(json: "")
    var locationFeed = CityName.init(json: "")
    var districtModel = DistrictModel.init(json: "")
    var tapEndEdit: UITapGestureRecognizer!
    var isCheckTitle = true {
        didSet{
            print(isCheckTitle)
            viewStyleText.isHidden = isCheckTitle
        }
    }
    var contentSearch = ""
    var imagesSelected = [UIImage]()
    // 1 là có media mới ,2 là không co media mới
    var newMedia = 2
    var editFromView = EditFromView.Home
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   self.tfPlacehoder.isHidden = true
        textViewTitle.isHidden = true
        self.heightViewImage.constant = 0
        setupUI()
        customLeftBarButton()
        
     //   textViewContent.delegate = self
        textViewContent.autocorrectionType = .no
       // textViewContent.placeHolderText = "Nhập nội dung bài viết"
        textViewContent.isScrollEnabled = false
        textViewContent.rx.text.subscribe { (text) in
            guard let textString = text.element, (textString?.attributedString.string ?? "").count > 0 else {return}
            self.tfPlacehoder.isHidden = (textString?.attributedString.string ?? "").count > 0
            self.itemPosts[0].content = NSMutableAttributedString.init(attributedString: self.textViewContent.attributedText)
            self.getHeightAttributedString(text: self.textViewContent.attributedText ?? NSAttributedString.init(string: ""))
        }.disposed(by: disposeBag)
        
        tfTitle.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBottomBorderColor(color: .gray)
    }
    
    override func backVC() {
        if itemPosts.count > 1 || itemPosts[0].content != NSMutableAttributedString.init(string: "") || self.tfTitle.text != ""{
            Utils.showAlertOK(controller: self, title: R.string.localizable.commonNoti(), message: "Bạn có chắc chắn muốn thoát khi đang tạo bài viết ?", cancel: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }

    func setupUI(){
        
        switch fromView {
        case .EditHome:
            // seti title
            self.title = R.string.localizable.homeTitleEditPost()
            self.handelDataFromEditVC()
            locationFeed = homeFeed.cityName ?? CityName.init(json: "")
            typeAbout = homeFeed.about ?? 2
            typeCategory = homeFeed.category ?? 1
            districtModel = homeFeed.district ?? DistrictModel.init(json: "")
            viewSelectStyleText.select = homeFeed.media?.count ?? 0
            /* custom Right Bar Button */
            customRightBarButtonCreate(title: "Tiếp")
            tfPlacehoder.isHidden = true
            textViewContent.isHidden = false

        case .Home:
            self.title = R.string.localizable.homeWritePost()
            locationFeed =  UserInfo.shareUserInfo.city_name
            districtModel = UserInfo.shareUserInfo.district
            typeAbout = 1
            typeCategory = 1
            
            // custom Right Bar Button
            customRightBarButtonCreate(title: "Tiếp")
            
            // init ItemPost
            let itemPost = ItemPost()
            itemPost.image = nil
            itemPost.content = NSMutableAttributedString.init(string: "")
            self.itemPosts.append(itemPost)

        default:
            print("default")
        }
        
        
        viewSelectStyleText.frame = viewStyleText.bounds
        viewStyleText.addSubview(viewSelectStyleText)
        viewStyleText.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 5)
        
        viewSelectStyleText.tapTextI = { [weak self] in
            guard let self = self  else { return }
            
            self.textViewContent.formatItalicSeletedText()
        }
        
        viewSelectStyleText.tapTextU = { [weak self] in
            guard let self = self  else { return }
            self.textViewContent.formatUnderlinedSeletedText()
        }
        
        viewSelectStyleText.tapTextB = { [weak self] in
            guard let self = self  else { return }
            self.textViewContent.formatBoldSeletedText()
        }
        
        viewSelectStyleText.tapSelectImage = { [weak self] infoImages in
            guard let self = self  else { return }
            let image = infoImages.map({ $0.0 })
//            if self.fromView != .EditHome {
//                self.imagesSelected.removeAll()
//            }
            
            self.imagesSelected.append(contentsOf: image)
            
          
            self.viewImage.images = self.imagesSelected
            self.showViewImage()
            
            for image in infoImages{
                let item = ItemPost()
                item.image = image.0
                self.itemPosts.append(item)
            }
        }
        
        viewImage.actionDelete = { index in
 
            guard let indexImage = self.itemPosts.firstIndex(where: { $0.image == self.imagesSelected[index] }) else {
                return
            }
            self.itemPosts.remove(at: indexImage)
            self.imagesSelected.remove(at: index)
            self.viewSelectStyleText.select -= 1
           
            self.viewImage.images = self.imagesSelected
            self.showViewImage()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)

        tapEndEdit = UITapGestureRecognizer.init(target: self, action: #selector(WriteHomeNewViewController.endEditting(_:)))
    }
    
    func showViewImage()
    {
        if self.imagesSelected.count > 0 {
            self.heightViewImage.constant = UIScreen.main.bounds.width - 24
            if self.imagesSelected.count > 3{
                self.heightViewImage.constant = UIScreen.main.bounds.width - 32
            }
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func endEditting(_ sender: UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    @objc func keyboardWasShown(notification: NSNotification) {
        
     //   self.isCheckTitle = false
        
        self.view.addGestureRecognizer(tapEndEdit)
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        UIView.animate(withDuration: 0.25) {
      
            self.constraintBottomViewStyleText.constant = keyboardRect!.height
            self.view.layoutIfNeeded()
        }
        
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.isCheckTitle = false
        self.view.removeGestureRecognizer(tapEndEdit)
      
        UIView.animate(withDuration: 0.25) {
        //    self.viewStyleText.frame.origin.y =  self.frameOrigin.origin.y
            self.constraintBottomViewStyleText.constant = 0
        }
    }
    
    func customRightBarButtonCreate(title: String){
        let rightBarButtonItem = UIBarButtonItem.init(title: title, style: .plain, target: self, action: #selector(WriteHomeNewViewController.createPost))
        rightBarButtonItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "fd799d") as Any, NSAttributedString.Key.font: UIFont.init(name: AppFont.NotoSansBold, size: 18) as Any], for: .normal)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    func checkInputFeed() -> Bool {
        guard let title = tfTitle.text?.trimmingCharacters(in: .whitespacesAndNewlines), title.count > 0 else {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertNoTileFeed())
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            return false
        }
        
        guard title.count < 100 else {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertMaxTileFeed())
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            return false
        }
        
        if imagesSelected.count == 0 && textViewContent.attributedText.string.count  == 0{
                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertNoContentFeed())
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                return false
        }
        
        return true
    }
    
    @objc func createPost(){
        createHomeFeed()
    }
    
    // Handle Image to String
    
//    func convertImageToItemPost(){
//        for image in imagesSelected {
//            let item = ItemPost()
//            item.image = image
//            itemPosts.append(item)
//        }
//    }
    
    func createJsonImage()->String{
//        if fromView == .Home{
//            convertImageToItemPost()
//        }
        
        if itemPosts.count > 0{
            var params = [Parameters]()
            for (index,item) in itemPosts.enumerated(){
                if index > 0 && index < 7{
                    if item.image != nil{
                        
                        let imageResize = Toucan.Resize.resizeImage(item.image!, size: CGSize(width: 700, height: 600))
                        
                        /*
                         let imgData = NSData(data: (imageResize as! UIImage).jpegData(compressionQuality: 1)!)
                         var imageSize: Int = imgData.count
                         print("actual size of image in KB: %f ", Double(imageSize) / 1000.0)
                         */
                        
                        var stringBase64: String = ""
                        if item.dataImageGIF == nil{
                            if let base64 = imageResize!.convertImageTobase64(format: .jpeg(1), image: imageResize!) {
                                stringBase64 = Key.HeaderJsonImage.HeaderJsonImage + base64
                            }
                        }else{
                            if let base64 = item.dataImageGIF?.base64EncodedString() {
                                stringBase64 = Key.HeaderJsonImage.HeaderJsonImage + base64
                            }
                        }

                        let param = self.createDic(content: item.contentHtml ?? "", base64: stringBase64, id: item.id ?? 0)
                        params.append(param)
                        
                    }else{
                        let param = self.createDic(content: item.contentHtml ?? "", base64: item.urlImage ?? "", id: item.id ?? 0)
                        params.append(param)
                    }
                }
                contentSearch += item.content?.string ?? ""
            }
            let jsonEncode = Utils.json(from: params)
            return jsonEncode ?? ""
        }
        return ""
    }
    
    func convertAttribuiteStringToHtml(){
        for item in itemPosts{
            item.contentHtml = item.content?.generateHtmlString()
        }
    }
    
    func createDic(content: String, base64: String, id: Int) -> Parameters{
        var param = Parameters()
        param["content"] = content
        param["base64"] = base64
        if fromView == .EditHome || fromView == .EditDiary { param["id"] = id }
        return param
    }
    
    func createHomeFeed(){
        if !checkInputFeed() { return }
        
        convertAttribuiteStringToHtml()
        contentSearch = ""
        contentSearch += "\(tfTitle.text ?? "") "
        
        let selectCategoryVC =  SelectCategoryVC.init(nib: R.nib.selectCategoryVC)
        selectCategoryVC.stringImage = createJsonImage()
        selectCategoryVC.contentSreach = contentSearch
        selectCategoryVC.titlePost = tfTitle.text!
        selectCategoryVC.comtentHTML = itemPosts[0].contentHtml ?? ""
        selectCategoryVC.locationFeed = locationFeed
        selectCategoryVC.districtModel = districtModel
        
        selectCategoryVC.idFeed = homeFeed.id ?? 0
        selectCategoryVC.newMedia = newMedia
        selectCategoryVC.editFromView = editFromView
        selectCategoryVC.fromView = fromView
        selectCategoryVC.ageRange = homeFeed.age_range ?? -1
        
        if fromView == .EditHome{
            selectCategoryVC.typeAbout = typeAbout
            selectCategoryVC.typeCategory = typeCategory
            selectCategoryVC.districtModel = homeFeed.district ?? DistrictModel.init(json: "")
            selectCategoryVC.locationFeed = homeFeed.cityName ?? CityName.init(json: "")
        }
        
        
        self.navigationController?.pushViewController(selectCategoryVC, animated: true)
        
    }
    
    func stringAddFontSizeAttributedString(content: String)->NSAttributedString{
        var content1 = content
        content1 = "<span" + Key.FontSizeAttribuite.FontSize + content1 + "</span>"
        return content1.html2AttributedString ?? NSAttributedString.init(string: "")
    }
    
    func handelDataFromEditVC(){
        // add title
        tfTitle.text = homeFeed.title ?? ""
        textViewContent.attributedText = self.stringAddFontSizeAttributedString(content: homeFeed.content ?? "")
        // add homefeed to itempost
        let item = ItemPost()
        item.image = nil
        //  item.id = -2
     //   item.content = NSMutableAttributedString.init(attributedString: self.stringAddFontSizeAttributedString(content: homeFeed.content ?? ""))
        item.content = NSMutableAttributedString.init(attributedString: (homeFeed.content?.html2AttributedString!)!)
        itemPosts.append(item)
        
        if (homeFeed.media ?? [Media]()).count > 0{
            
            for homeFeedEdit in homeFeed.media! {
                
                let item = ItemPost()
                item.id = homeFeedEdit.id ?? 0
             //   item.urlImage = homeFeedEdit.link ?? ""
             //   item.content = NSMutableAttributedString.init(attributedString: (homeFeedEdit.descriptionValue?.html2AttributedString!)!)
                                KingfisherManager.shared.retrieveImage(with: URL.init(string: homeFeedEdit.link ?? "")! , options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                                    guard let img = image else { return }
                                    item.image = img
                                    self.imagesSelected.append(img)
                                    
                                    if self.imagesSelected.count == self.homeFeed.media!.count{
                                        self.showViewImage()
                                        self.viewImage.images = self.imagesSelected
                                        
                                        self.viewSelectStyleText.select = self.imagesSelected.count
                                    }
                                })
                
                itemPosts.append(item)
            }
        }
        
    }
    
    func getHeightAttributedString(text: NSAttributedString){
        
        let height1 = text.height(containerWidth: UIScreen.main.bounds.width - 32)
        heightTextViewContent.constant = height1 + 20
        self.view.layoutIfNeeded()
    }
     
}

extension WriteHomeNewViewController : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.isCheckTitle = true
    }
}
