//
//  ViewImageOrGifViewController.swift
//  Beberia
//
//  Created by Lap on 11/25/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SDWebImage

class ViewImageOrGifViewController: UIViewController {
    
    @IBOutlet weak var imageGIF: SDAnimatedImageView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgImage: UIImageView!
    
    var image: UIImage?
    var itemPost: ItemPost?
    var homeFeed: Media?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        imageGIF.isHidden = true
        imgImage.isHidden = true
        
        if itemPost?.image != nil {
            if itemPost?.dataImageGIF == nil{
                imgImage.isHidden = false
                imgImage.image = itemPost?.image
            }else{
                imageGIF.isHidden = false
                let iamgeData = SDAnimatedImage.init(data: itemPost?.dataImageGIF ?? Data.init())
                imageGIF.image = iamgeData
            }
        }else{
            
            if let theProfileImageUrl = URL.init(string: homeFeed?.link ?? "")  {
                do {
                    let imageData = try Data(contentsOf: theProfileImageUrl as URL)
                    
                    let isGIF = Utils.isAnimatedImage(imageData)
                    if isGIF {
                        imageGIF.isHidden = false
                        let iamgeData = SDAnimatedImage.init(data: itemPost?.dataImageGIF ?? Data.init())
                        imageGIF.image = iamgeData
                    }else{
                        imgImage.isHidden = false
                        imgImage.image = UIImage(data: imageData)
                    }
                } catch {
                    print("Unable to load data: \(error)")
                }
            }
        }
        
    }
    
    @IBAction func didPressClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
