//
//  WriteHomeViewController.swift
//  Beberia
//
//  Created by IMAC on 9/3/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import IQKeyboardManagerSwift
import Kingfisher
import SDWebImage

class WriteHomeViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var heightTbv: NSLayoutConstraint!
    @IBOutlet weak var heightViewPrivate: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomViewStyleText: NSLayoutConstraint!
    @IBOutlet weak var viewStyleText: UIView!
    @IBOutlet weak var tbvDesc: UITableView!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var viewPrivate: UIView!
    @IBOutlet weak var btnPrivate: UIButton!
    
    // MARK: - Properties
    
    var isCheckTitle = false
    var heightDictionary: [IndexPath : CGFloat] = [:]
    var viewSelectStyleText: ViewOptionStyleText = ViewOptionStyleText.instanceFromNib()
    var itemPosts = [ItemPost]()
    var frameOrigin = CGRect()
    var tapEndEdit: UITapGestureRecognizer!
    var arrayImage = [UIImage]()
    var contentSearch = ""
    var checkComplete: (_ id:Int) -> () = {_ in}
    var idFeedCreate = 0
    var dataLocation = [String]()
    let dataChildren = CategoryFeed.arrayList
    let dataInfo = AboutFeed.arrayList
    var typeAbout = 2
    var typeCategory = 1
    var locationFeed = CityName.init(json: "")
    var districtModel = DistrictModel.init(json: "")
    var cell: WriteHomeTableViewCell?
    var isEnableI = false
    var isEnableU = false
    var isEnableB = false
    var homeFeed = HomeFeed.init(json: "")
    var fromView = FromView.Home
    var editFromView = EditFromView.Home
    var privateInt = 0
    var idDiary = 0
    var isPrivate = false
    {
        didSet{
            btnPrivate.setImage(UIImage.init(named: !isPrivate ? "ic_untickBox" : "ic_tickBox"), for: .normal)
            privateInt = isPrivate ? 1 : 0
        }
    }
    // 1 là có media mới ,2 là không co media mới
    var newMedia = 2
    private var isSticker: Bool = false
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        tapEndEdit = UITapGestureRecognizer.init(target: self, action: #selector(WriteDiaryViewController.endEditting(_:)))
        customLeftBarButton()
        
        setupUI()
        
//        let tapTableView = UITapGestureRecognizer.init(target: self, action: #selector(tapOnTextView(_:)))
//        tapTableView.numberOfTapsRequired = 1
//        self.tbvDesc.addGestureRecognizer(tapTableView)
    }
    
//    @objc private final func tapOnTextView(_ tapGesture: UITapGestureRecognizer){
//
//        let point = tapGesture.location(in: tbvDesc)
//        let indexPath = tbvDesc.indexPathForRow(at: point)
//
//        if let cell = tbvDesc.cellForRow(at: indexPath!) as?  WriteHomeTableViewCell{
//            let pointCell = tapGesture.location(in: cell.tvContent)
//            cell.tvContent.beginFloatingCursor(at: pointCell)
//        }
//    }
//

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBottomBorderColor(color: .gray)
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBottomBorderColor(color: .white)
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    //    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    //       //    tbvHome.layer.removeAllAnimations()
    //           self.heightTbv.constant = tbvDesc.contentSize.height
    //           UIView.animate(withDuration: 0.5) {
    //               self.updateViewConstraints()
    //               self.view.layoutIfNeeded()
    //           }
    //       }
    
    // MARK: - Overrides
    
    override func backVC() {
        if itemPosts.count > 1 || itemPosts[0].content != NSMutableAttributedString.init(string: "") || self.tfTitle.text != ""{
            Utils.showAlertOK(controller: self, title: R.string.localizable.commonNoti(), message: "Bạn có chắc chắn muốn thoát khi đang tạo bài viết ?", cancel: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        
        switch fromView {
        case .EditHome:
            // seti title
            self.title = R.string.localizable.homeTitleEditPost()
            self.handelDataFromEditVC()
            locationFeed = homeFeed.cityName ?? CityName.init(json: "")
            typeAbout = homeFeed.about ?? 2
            typeCategory = homeFeed.category ?? 1
            districtModel = homeFeed.district ?? DistrictModel.init(json: "")
            viewSelectStyleText.select = homeFeed.media?.count ?? 0
            /* custom Right Bar Button */
            customRightBarButtonCreate(title: R.string.localizable.homeContinue())
            
            /* setup UI */
            heightViewPrivate.constant = 0
            viewPrivate.isHidden = true
        case .Home:
            self.title = R.string.localizable.homeWritePost()
            locationFeed =  UserInfo.shareUserInfo.city_name
            districtModel = UserInfo.shareUserInfo.district
            typeAbout = 1
            typeCategory = 1
            
            // custom Right Bar Button
            customRightBarButtonCreate(title: R.string.localizable.homeContinue())
            
            // init ItemPost
            let itemPost = ItemPost()
            itemPost.image = nil
            itemPost.content = NSMutableAttributedString.init(string: "")
            self.itemPosts.append(itemPost)
            
            // setup UI
            heightViewPrivate.constant = 0
            viewPrivate.isHidden = true
            
        case .Diary:
            self.title = R.string.localizable.diaryTilteWrite()
            
            // setup UI
            heightViewPrivate.constant = 39
            viewPrivate.isHidden = false
            viewSelectStyleText.isDiaryVC = true
            
            // init ItemPost
            let itemPost = ItemPost()
            itemPost.image = nil
            itemPost.content = NSMutableAttributedString.init(string: "")
            self.itemPosts.append(itemPost)
            
            // custom Right Bar Button
            customRightBarButtonCreate(title: R.string.localizable.homePost())
            
        case .EditDiary:
            self.title = R.string.localizable.diaryTilteEdit()
            self.handelDataFromEditVC()
            // setup UI
            heightViewPrivate.constant = 39
            viewPrivate.isHidden = false
            viewSelectStyleText.isDiaryVC = true
        
            viewSelectStyleText.selectImageDiary = homeFeed.media?.count ?? 0
            // custom Right Bar Button
            customRightBarButtonSave()
            isPrivate = homeFeed.isPrivate == 1 ? true : false
            
        case .Secret:
            self.title = R.string.localizable.secretWrite_Tile()
            
            // custom Right Bar Button
            customRightBarButtonCreate(title: R.string.localizable.homeContinue())
            
            // init ItemPost
            let itemPost = ItemPost()
            itemPost.image = nil
            itemPost.content = NSMutableAttributedString.init(string: "")
            self.itemPosts.append(itemPost)
            
            // setup UI
            heightViewPrivate.constant = 0
            viewPrivate.isHidden = true
        }
        
        tbvDesc.register(R.nib.writeHomeTableViewCell)
        
        tbvDesc.delegate = self
        tbvDesc.dataSource = self
        tbvDesc.separatorStyle = .none
        
        viewSelectStyleText.frame = viewStyleText.bounds
        viewStyleText.addSubview(viewSelectStyleText)
        viewStyleText.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 5)
        
//        viewSelectStyleText.tapTextI = { [weak self] in
//            guard let self = self  else { return }
//            self.isEnableI = !self.isEnableI
//            self.cell?.tvContent.formatItalicSeletedText()
//        }
//
//        viewSelectStyleText.tapTextU = { [weak self] in
//            guard let self = self  else { return }
//            self.isEnableU = !self.isEnableU
//            self.cell?.tvContent.formatUnderlinedSeletedText()
//        }
//
//        viewSelectStyleText.tapTextB = { [weak self] in
//            guard let self = self  else { return }
//            self.isEnableB = !self.isEnableB
//            self.cell?.tvContent.formatBoldSeletedText()
//        }
        
        viewSelectStyleText.isSticker = { [weak self] isSticker in
            guard let wSelf = self else { return }
            wSelf.isSticker = isSticker
        }
        
        viewSelectStyleText.tapSelectImage = { [weak self] infoImages in
            guard let self = self  else { return }
            self.viewSelectStyleText.isSelectI = false
            self.viewSelectStyleText.isSelectU = false
            self.viewSelectStyleText.isSelectB = false
            
            // đổi lại newMedia = 1 (có media mới)
            self.newMedia = 1
            
            for image in infoImages{
                // adđ object new
                let itemPost = ItemPost()
                itemPost.image = image.0
                itemPost.dataImageGIF = image.1
                itemPost.isImageGIF = image.1 != nil
               // itemPost.isImageGIF = Utils.isAnimatedImage(<#T##imageUrl: URL##URL#>)
                itemPost.content = NSMutableAttributedString.init(string: "")
                if self.fromView == .EditHome || self.fromView == .EditDiary { itemPost.id = 0 }
                self.itemPosts.append(itemPost)
            }
           

            self.tbvDesc.reloadData()
            //fix bug crash app
            if !self.itemPosts.isEmpty {
                self.tbvDesc.scrollToRow(at: IndexPath.init(row: self.itemPosts.count - 1, section: 0), at: .none, animated: false)
            }
            // kepp status style text
            self.viewSelectStyleText.isSelectI = self.isEnableI
            self.viewSelectStyleText.isSelectU = self.isEnableU
            self.viewSelectStyleText.isSelectB = self.isEnableB
            
            if self.isEnableI{
                self.cell?.tvContent.isItalicEnabled = false
                self.cell?.tvContent.formatItalicSeletedText()
            }
            
            if self.isEnableU{
                self.cell?.tvContent.isUnderlineEnabled = false
                self.cell?.tvContent.formatUnderlinedSeletedText()
            }
            
            if self.isEnableB{
                self.cell?.tvContent.isBoldEnabled = false
                self.cell?.tvContent.formatBoldSeletedText()
            }
        }
        
        frameOrigin = viewStyleText.frame
        tfTitle.delegate = self
    }
    
    func customRightBarButtonCreate(title: String){
        let rightBarButtonItem = UIBarButtonItem.init(title: title, style: .plain, target: self, action: #selector(WriteHomeViewController.createPost))
        rightBarButtonItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "eebf3b") as Any], for: .normal)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    func customRightBarButtonSave(){
        let rightBarButtonItem = UIBarButtonItem.init(title: R.string.localizable.homeLuu(), style: .plain, target: self, action: #selector(WriteHomeViewController.editFeedHome))
        rightBarButtonItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "eebf3b") as Any], for: .normal)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    //MARK: - @objc
    
    @objc func editFeedHome(){
        // disnable rightBarButtonItem (CLick 1 lần)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        switch fromView {
        case .EditHome:
            //  editFeed()
            print("Ádaadasd")
        case .EditDiary:
            editDiary()
        default:
            print("editFeedHome")
        }
        
    }
    
    @objc func createPost(){
        // disnable rightBarButtonItem (CLick 1 lần)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        switch fromView {
        case .Home, .EditHome:
            createHomeFeed()
        case .Diary:
            createDiary()
        case .Secret:
            createSecret()
        default:
            print("createPost")
        }
        
    }
    
    @objc func endEditting(_ sender: UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    @objc func keyboardWasShown(notification: NSNotification) {
        
        viewStyleText.isHidden = self.isCheckTitle
        
        self.tbvDesc.addGestureRecognizer(tapEndEdit)
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        UIView.animate(withDuration: 0.25) {
            self.constraintBottomViewStyleText.constant = -keyboardRect!.height //self.isCheckTitle ? 0 : (-keyboardRect!.height)
            self.view.layoutIfNeeded()
        }
        
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.isCheckTitle = false
        self.tbvDesc.removeGestureRecognizer(tapEndEdit)
        print(notification)
        UIView.animate(withDuration: 0.25) {
            self.viewStyleText.frame.origin.y =  self.frameOrigin.origin.y
            self.constraintBottomViewStyleText.constant = 0
        }
    }
    
    //MARK: - Helper methods
    
    func stringAddFontSizeAttributedString(content: String)->NSAttributedString{
          var content1 = content
          content1 = "<span" + Key.FontSizeAttribuite.FontSize + content1 + "</span>"
          return content1.html2AttributedString ?? NSAttributedString.init(string: "")
      }
    
    func handelDataFromEditVC(){
        // add title
        tfTitle.text = homeFeed.title ?? ""
        
        // add homefeed to itempost
        let item = ItemPost()
        item.image = nil
        //  item.id = -2
     //   item.content = NSMutableAttributedString.init(attributedString: self.stringAddFontSizeAttributedString(content: homeFeed.content ?? ""))
        item.content = NSMutableAttributedString.init(attributedString: (homeFeed.content?.html2AttributedString!)!)
        itemPosts.append(item)
        
        if (homeFeed.media ?? [Media]()).count > 0{
            for homeFeedEdit in homeFeed.media! {
                let item = ItemPost()
                item.id = homeFeedEdit.id ?? 0
                item.urlImage = homeFeedEdit.link ?? ""
                item.content = NSMutableAttributedString.init(attributedString: (homeFeedEdit.descriptionValue?.html2AttributedString!)!)
                //                KingfisherManager.shared.retrieveImage(with: URL.init(string: homeFeedEdit.link ?? "")! , options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                //                    guard let img = image else { return }
                //                    item.image = img
                //                })
                viewSelectStyleText.select += 1
                itemPosts.append(item)
            }
        }
    }
    
    func checkInputFeed() -> Bool {
        guard let title = tfTitle.text?.trimmingCharacters(in: .whitespacesAndNewlines), title.count > 0 else {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertNoTileFeed())
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            return false
        }
        
        guard title.count < 100 else {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertMaxTileFeed())
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            return false
        }
        
        if itemPosts.count > 1{
            
            if  itemPosts[1].image != nil || itemPosts[1].urlImage != nil || (itemPosts[1].content?.string.trimmingCharacters(in: .whitespacesAndNewlines) ?? "").count != 0{
                
                return true
            }else{
                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertNoContentFeed())
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                return false
            }
        }
        
        if itemPosts.count > 0{
            if (itemPosts[0].content?.string.trimmingCharacters(in: .whitespacesAndNewlines) ?? "").count != 0{
                return true
            }
        }
        Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertNoContentFeed())
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        return false
    }
    
    
    func checkInputDiary() -> Bool {
        guard let title = tfTitle.text?.trimmingCharacters(in: .whitespacesAndNewlines), title.count > 0 else {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertNoTileDiary())
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            return false
        }
        
        guard title.count < 100 else {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertMaxTileDiary())
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            return false
        }
        
        
        if itemPosts.count > 1{
            
            if  itemPosts[1].image != nil || itemPosts[1].urlImage != nil || (itemPosts[1].content?.string.trimmingCharacters(in: .whitespacesAndNewlines) ?? "").count != 0{
                
                return true
            }else{
                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertNoContentDiary())
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                return false
            }
        }
        
        if itemPosts.count > 0{
            if (itemPosts[0].content?.string.trimmingCharacters(in: .whitespacesAndNewlines) ?? "").count != 0{
                return true
            }
        }
        
        Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.commonAlertNoContentDiary())
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        return false
    }
    
    func createSecret(){
        if !checkInputFeed() { return }
        
        convertAttribuiteStringToHtml()
        contentSearch = ""
        contentSearch += "\(tfTitle.text ?? "") "
        
        let selectedTopic = SelectedTopicViewController.init(nib: R.nib.selectedTopicViewController)
        selectedTopic.stringImage = createJsonImage()
        selectedTopic.contentSreach = contentSearch
        selectedTopic.titlePost = tfTitle.text!
        selectedTopic.comtentHTML = itemPosts[0].contentHtml ?? ""
        
        self.navigationController?.pushViewController(selectedTopic, animated: true)
    }
    
    func createHomeFeed(){
        if !checkInputFeed() { return }
        
        convertAttribuiteStringToHtml()
        contentSearch = ""
        contentSearch += "\(tfTitle.text ?? "") "
        
        let selectCategoryVC =  SelectCategoryVC.init(nib: R.nib.selectCategoryVC)
        selectCategoryVC.stringImage = createJsonImage()
        selectCategoryVC.contentSreach = contentSearch
        selectCategoryVC.titlePost = tfTitle.text!
        selectCategoryVC.comtentHTML = itemPosts[0].contentHtml ?? ""
        selectCategoryVC.locationFeed = locationFeed
        selectCategoryVC.districtModel = districtModel
        
        selectCategoryVC.idFeed = homeFeed.id ?? 0
        selectCategoryVC.newMedia = newMedia
        selectCategoryVC.editFromView = editFromView
        selectCategoryVC.fromView = fromView
        selectCategoryVC.ageRange = homeFeed.age_range ?? -1
        
        if fromView == .EditHome{
            selectCategoryVC.typeAbout = typeAbout
            selectCategoryVC.typeCategory = typeCategory
            selectCategoryVC.districtModel = homeFeed.district ?? DistrictModel.init(json: "")
            selectCategoryVC.locationFeed = homeFeed.cityName ?? CityName.init(json: "")
        }
        
        
        self.navigationController?.pushViewController(selectCategoryVC, animated: true)
        
    }
    
    
    // Handle Image to String
    
    func createJsonImage()->String{
        if itemPosts.count > 0{
            var params = [Parameters]()
            for (index,item) in itemPosts.enumerated(){
                if index > 0 && index < 7{
                    if item.image != nil{
                        
                        let imageResize = Toucan.Resize.resizeImage(item.image!, size: CGSize(width: 700, height: 600))
                        
                        /*
                         let imgData = NSData(data: (imageResize as! UIImage).jpegData(compressionQuality: 1)!)
                         var imageSize: Int = imgData.count
                         print("actual size of image in KB: %f ", Double(imageSize) / 1000.0)
                         */
                        
                        var stringBase64: String = ""
                        if item.dataImageGIF == nil{
                            if let base64 = imageResize!.convertImageTobase64(format: .jpeg(1), image: imageResize!) {
                                stringBase64 = Key.HeaderJsonImage.HeaderJsonImage + base64
                            }
                        }else{
                            if let base64 = item.dataImageGIF?.base64EncodedString() {
                                stringBase64 = Key.HeaderJsonImage.HeaderJsonImage + base64
                            }
                        }

                        let param = self.createDic(content: item.contentHtml ?? "", base64: stringBase64, id: item.id ?? 0)
                        params.append(param)
                        
                    }else{
                        let param = self.createDic(content: item.contentHtml ?? "", base64: item.urlImage ?? "", id: item.id ?? 0)
                        params.append(param)
                    }
                }
                contentSearch += item.content?.string ?? ""
            }
            let jsonEncode = Utils.json(from: params)
            return jsonEncode ?? ""
        }
        return ""
    }
    
    func createDic(content: String, base64: String, id: Int) -> Parameters{
        var param = Parameters()
        param["content"] = content
        param["base64"] = base64
        if fromView == .EditHome || fromView == .EditDiary { param["id"] = id }
        return param
    }
    
    func convertAttribuiteStringToHtml(){
        for item in itemPosts{
            item.contentHtml = item.content?.generateHtmlString()
        }
    }
    
    func getHeightTextView(text: String, cell: WriteHomeTableViewCell){
        let height = text.height(withConstrainedWidth: cell.tvContent.frame.width - 10, font: UIFont.systemFont(ofSize: 14))
        print(height)
        cell.heightTextView.constant = height + 20
        cell.layoutIfNeeded()
        tbvDesc.layoutIfNeeded()
    }
    
    func getHeightAttributedString(text: NSAttributedString, cell: WriteHomeTableViewCell){
        
        let height1 = text.height(containerWidth: self.tbvDesc.frame.width - 15)
        cell.heightTextView.constant = height1 + 20
        cell.layoutIfNeeded()
    }
    
    // MARK: - Request API
    
    func editDiary(){
        if !checkInputDiary() { return }
        convertAttribuiteStringToHtml()
        contentSearch = ""
        SVProgressHUD.show()
        contentSearch += "\(tfTitle.text ?? "") "
        APIManager.editDiary(id: homeFeed.id ?? 0, newMedia: newMedia, title: tfTitle.text!, content: itemPosts[0].contentHtml ?? "", images: createJsonImage(), isPrivate: privateInt,contentSearch: contentSearch, callbackSuccess: { [weak self] (homeFeed) in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            let viewVCs =  self.navigationController?.viewControllers
            switch self.editFromView{
            case .Home :
                NotificationCenter.default.post(name: NSNotification.Name.checkEditDiary, object: [Key.KeyNotification.idDiary: homeFeed.id ?? 0], userInfo: nil)
                self.navigationController?.popToRootViewController(animated: true)
            case .Profile:
                NotificationCenter.default.post(name: NSNotification.Name.checkEditDiaryProfile, object: [Key.KeyNotification.idDiary: homeFeed.id ?? 0], userInfo: nil)
                self.navigationController?.popToRootViewController(animated: true)
            case .Save:
                for viewVC in viewVCs!{
                    if viewVC.isKind(of: InfoMarkProfileViewController.self){
                        NotificationCenter.default.post(name: NSNotification.Name.checkEditDiarySave, object: [Key.KeyNotification.idDiary: homeFeed.id ?? 0], userInfo: nil)
                        self.navigationController?.popToViewController(viewVC, animated: true)
                    }
                }
            case .Sreach:
                for viewVC in viewVCs!{
                    if viewVC.isKind(of: HomeInfo.self){
                        NotificationCenter.default.post(name: NSNotification.Name.checkEditDiarySreach, object: [Key.KeyNotification.idDiary: homeFeed.id ?? 0], userInfo: nil)
                        self.navigationController?.popToViewController(viewVC, animated: true)
                    }
                }
                
            case .Notification:
                print("Notification")
            case .Secret:
                print("Secret")
            }
            
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func createDiary(){
        if !checkInputDiary() { return }
        convertAttribuiteStringToHtml()
        contentSearch = ""
        SVProgressHUD.show()
        contentSearch += "\(tfTitle.text ?? "") "
        APIManager.createDiary(title: tfTitle.text!,
                               content: itemPosts[0].contentHtml ?? "" ,
                               images: createJsonImage(),
                               isPrivate: privateInt,
                               contentSearch: contentSearch,
                               isSticker: self.isSticker,
                               callbackSuccess: { [weak self] (homeFeed) in
            guard let self = self else { return }
            SVProgressHUD.dismiss()
            self.idDiary = homeFeed.id ?? 0
            self.navigationController?.popViewController(animated: true)
            self.checkComplete(self.idDiary)
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    
    
    @IBAction func didPressPrivate(_ sender: Any) {
        isPrivate = !isPrivate
    }
    
    deinit {
      //    NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
      }
}

// MARK: - UITextViewDelegate

extension WriteHomeViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        isCheckTitle = true
    }
}

// MARK: - UITableViewDelegate

extension WriteHomeViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.itemPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.writeHomeTableViewCell, for: indexPath)!
        self.cell = cell
        cell.btnDelete.tag = indexPath.row
        cell.tfPlachoder.isHidden = true
        
      //  cell.btnPlayGIF.isHidden = !(self.itemPosts[indexPath.row].isImageGIF ?? false)
        cell.btnPlayGIF.isHidden = true
        
        if indexPath.row == 0{
            cell.heightImage.constant = 0
            cell.tvContent.attributedText = self.itemPosts[0].content ?? NSAttributedString.init(string: "")
            cell.tfPlachoder.isHidden = self.itemPosts[0].content?.string.count ?? 1 > 0
            cell.btnDelete.isHidden = true
            self.getHeightAttributedString(text: self.itemPosts[0].content ?? NSMutableAttributedString.init(string: ""), cell: cell)
            
        }else{
            cell.imgPost?.image = UIImage.init()
            cell.imgPost.isHidden = true
            cell.tfPlachoder.isHidden = true

            if let img = self.itemPosts[indexPath.row].image {
                cell.btnDelete.isHidden = false
                cell.imgPost.isHidden = false
                cell.heightImage.constant =  cell.frame.width / img.getCropRatio()
                
                if self.itemPosts[indexPath.row].isImageGIF ?? false{
//                    let sdImage = SDAnimatedImageView.init(frame: cell.imgPost.frame)
//                    sdImage.image = SDAnimatedImage.init(data: self.itemPosts[indexPath.row].dataImageGIF!)
//                    cell.imgPost.addSubview(sdImage)
                    cell.imgPost?.image = img
                }
                else{
                  //  SDAnimatedImage.i
                    cell.imgPost?.image = img
                }
            }else{
                cell.btnDelete.isHidden = false
                if cell.imgPost != nil {
                    cell.imgPost.isHidden = false
                }
               
                cell.imgPost.kf.setImage(with: URL.init(string: self.itemPosts[indexPath.row].urlImage ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
                KingfisherManager.shared.retrieveImage(with: URL.init(string: self.itemPosts[indexPath.row].urlImage ?? "")! , options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                    guard let img = image else { return }
                    cell.heightImage.constant = cell.frame.width / (img.getCropRatio() < 1 ? img.getCropRatio() : img.getCropRatio() - 0.2 )
                })
            }
            cell.tvContent.attributedText = self.itemPosts[indexPath.row].content ?? NSAttributedString.init(string: "")
            self.getHeightAttributedString(text: self.itemPosts[indexPath.row].content ?? NSMutableAttributedString.init(string: ""), cell: cell)
        }
        
        cell.tapPlayGIF = { index in
//            let viewImageOfGif = ViewImageOrGifViewController.init(nib: R.nib.viewImageOrGifViewController)
//            viewImageOfGif.itemPost = self.itemPosts[indexPath.row]
//            viewImageOfGif.modalPresentationStyle = .overCurrentContext
//            self.present(viewImageOfGif, animated: true, completion: nil)
        }
        
        self.viewSelectStyleText.tapTextI = { [weak self] in
            guard let self = self else {  return  }
            self.isEnableI = !self.isEnableI
            for cell in self.tbvDesc.visibleCells {
                if cell.isKind(of: WriteHomeTableViewCell.self){
                    (cell as! WriteHomeTableViewCell).tvContent.formatItalicSeletedText()
                }
            }
            self.itemPosts[indexPath.row].content = NSMutableAttributedString.init(attributedString: cell.tvContent.attributedText)
        }

        self.viewSelectStyleText.tapTextU = { [weak self] in
            guard let self = self else {  return  }
            self.isEnableU = !self.isEnableU
            for cell in self.tbvDesc.visibleCells {
                if cell.isKind(of: WriteHomeTableViewCell.self){
                    (cell as! WriteHomeTableViewCell).tvContent.formatUnderlinedSeletedText()
                }
            }
            self.itemPosts[indexPath.row].content = NSMutableAttributedString.init(attributedString: cell.tvContent.attributedText)
        }

        self.viewSelectStyleText.tapTextB = { [weak self] in
            guard let self = self else {  return  }
            self.isEnableB = !self.isEnableB
            for cell in self.tbvDesc.visibleCells {
                if cell.isKind(of: WriteHomeTableViewCell.self){
                    (cell as! WriteHomeTableViewCell).tvContent.formatBoldSeletedText()
                }
            }
            self.itemPosts[indexPath.row].content = NSMutableAttributedString.init(attributedString: cell.tvContent.attributedText)
        }
        
        if indexPath.row == 0 && itemPosts.count == 1 && itemPosts[0].content == NSMutableAttributedString.init(string: ""){
            cell.heightTextView.constant = self.tbvDesc.frame.size.height
        }
        
        cell.tapDeleteImage = { [weak self] index, sender in
            guard let self = self else {  return  }
            self.viewSelectStyleText.select -= 1
            self.viewSelectStyleText.selectImageDiary -= 1
            if let cell = sender.superview?.superview as? WriteHomeTableViewCell{
                let indexPath = self.tbvDesc.indexPath(for: cell)
                // Now delete the table cell
                let index = indexPath?.row ?? 0
                if index > 0 {
                    self.itemPosts[index - 1].content?.append(self.itemPosts[index].content ?? NSMutableAttributedString.init(string: ""))  
                    self.itemPosts.remove(at: index)
                    
                    self.tbvDesc.performBatchUpdates({
                         //do stuff then delete the row
                        
                        self.tbvDesc.deleteRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                    }, completion: { (done) in
                         //perform table refresh
                        // self.tbvDesc.reloadRows(at: [IndexPath.init(row: index - 1, section: 0)], with: .none)
                         self.tbvDesc.reloadData()
                         self.tbvDesc.layoutIfNeeded()

                    })
                }
            }
        }
        
        cell.tvContent.checkAttribuite = { [weak self] isEnableBold, isEnableItalic, isEnableUtralic in
            guard let self = self else {  return  }
            self.viewSelectStyleText.isSelectI = isEnableItalic
            self.viewSelectStyleText.isSelectU = isEnableUtralic
            self.viewSelectStyleText.isSelectB = isEnableBold
        }
        
        cell.tvContent.beginEditTextView = { [weak self] textView in
            self?.isCheckTitle = false
            self!.viewStyleText.isHidden = self!.isCheckTitle
        }
        
        cell.updateHeightOfRow = { [weak self] textView in
            guard let self = self else {  return  }

            cell.tfPlachoder.isHidden = textView.text.count > 0

            self.itemPosts[cell.btnDelete.tag].content = NSMutableAttributedString.init(attributedString: textView.attributedText)
          //  cell.tvContent.attributedText = self.itemPosts[cell.btnDelete.tag].content
            cell.tvContent.textColor = .black
            
            // update height textview
            self.getHeightAttributedString(text: cell.tvContent.attributedText, cell: cell)
            UIView.setAnimationsEnabled(false)
            self.tbvDesc.beginUpdates()
            self.tbvDesc.endUpdates()
            UIView.setAnimationsEnabled(true)
            
            // Scoll up your textview if required
            if let thisIndexPath = self.tbvDesc.indexPath(for: cell) {
                self.tbvDesc.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return height ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
