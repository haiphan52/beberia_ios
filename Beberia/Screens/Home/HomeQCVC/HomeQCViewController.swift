//
//  HomeQCViewController.swift
//  Beberia
//
//  Created by OS on 11/5/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import Kingfisher
import SVProgressHUD

class HomeQCViewController: BaseViewController{
    
    // MARK: - Outlets
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tbvContent: UITableView!
    @IBOutlet weak var imgUserCreate: UIImageView!
    @IBOutlet weak var lblUserCreate: UILabel!
    @IBOutlet weak var viewLikeComment: UIView!
    @IBOutlet weak var heightTbv: NSLayoutConstraint!
    @IBOutlet weak var imgAvaUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    
    // MARK: - Properties
    
    var viewLikeAndComment: ViewLikeComment = ViewLikeComment.instanceFromNib()
    fileprivate var heightDictionary: [IndexPath : CGFloat] = [:]
    var homeFeed = HomeFeed.init(json: "")
    var letheStretchyHeader: LetheStretchyHeader?
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        customLeftBarButton()
        customrightBarButton()
        tbvContent.register(R.nib.homeDetailTableViewCell)
        tbvContent.delegate = self
        tbvContent.dataSource = self
        tbvContent.separatorStyle = .none
        tbvContent.allowsSelection = false
        tbvContent.rowHeight = UITableView.automaticDimension
        tbvContent.isScrollEnabled = false
        viewLikeAndComment.frame = viewLikeComment.bounds
        viewLikeComment.addSubview(viewLikeAndComment)
        self.tbvContent.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateNumberComment), name: NSNotification.Name.updateNumberComment, object: nil)
        //        self.tbvContent.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    private func initialHeader() {
        if let url = URL(string: homeFeed.cover?[0].link ?? ""){
            let data = NSData(contentsOf: url)
            LetheStretchyHeader().initial(viewController: self, parentView: scrollview, customHeader: nil, image: UIImage(data: data! as Data), height: 140, type: . afterShowNavigationBar,completionMore: {index in
                self.showAtionTunrOnOff(turnOnOff: self.homeFeed.turnOn ?? 0, id: self.homeFeed.id ?? 0)
            }, completion :{ index in
                self.navigationController?.popViewController(animated: true)
                self.navigationController?.setNavigationBarHidden(true, animated: false)
            })
        }
    }
    
    func customrightBarButton(){
        let rightBarButtonItemOption = UIBarButtonItem.menuButton(self, action: #selector(HomeQCViewController.tapOption), imageName: R.image.ic_option1() ?? UIImage.init(), height: 40, width: 40)
        self.navigationItem.rightBarButtonItem = rightBarButtonItemOption
    }
    
    //MARK: - Helper methods
    
    func showAtionTunrOnOff(turnOnOff:Int, id:Int){
        var onOff = R.string.localizable.homeTunrOnNoti()
        if turnOnOff == 1 {
            onOff = R.string.localizable.homeTunrOffNoti()
        }
        self.showActionBottom(datasource: [onOff], isNoTextColorRed: false) { (index) in
            switch index {
            case 0:
                self.turnOnOff(turnOn: turnOnOff, id: id)
            default:
                print("123")
            }
        }
    }
    
    @objc func tapOption(){
        showAtionTunrOnOff(turnOnOff: homeFeed.turnOn ?? 0, id: homeFeed.id ?? 0)
    }
    
    @objc func updateNumberComment(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        let numberComment = object[Key.KeyNotification.numberComment] as? Int
        viewLikeAndComment.lblNumberComment.text = "\(numberComment ?? 0)"
        viewLikeAndComment.homeFeed.numberComment = numberComment
        viewLikeAndComment.numberComment = numberComment ?? 0
    }
    
    // MARK: - Request API
    
    func turnOnOff(turnOn:Int, id:Int ){
        SVProgressHUD.show()
        APIManager.turnOnOffNotification(id: id, turnOn: turnOn, callbackSuccess: { [weak self] (isSucces) in
            guard let self = self else { return }
            self.homeFeed.turnOn = isSucces
            SVProgressHUD.dismiss()
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        })
    }
    
    // MARK: - SetupData
    
    func loadData(){
        lblTitle.text = homeFeed.title ?? ""
        lblUserCreate.text = homeFeed.userCreate?.displayName
        lblUserName.text = homeFeed.userCreate?.displayName
        lblLike.text = "\(homeFeed.numberLike ?? 0) Thích"
        lblComment.text = "\(homeFeed.numberComment ?? 0) Bình luận"
        imgUserCreate.kf.setImage(with: URL.init(string: homeFeed.userCreate?.avatar ?? "") ,placeholder: UIImage.init(named: Key.ImagePlaceholder))
        imgAvaUser.kf.setImage(with: URL.init(string: homeFeed.userCreate?.avatar ?? "") ,placeholder: UIImage.init(named: Key.ImagePlaceholder))
        viewLikeAndComment.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 0)
        //        viewLikeAndComment.fromToView = isHomeVC ? .Home : .Diary
        viewLikeAndComment.homeFeed = homeFeed
        viewLikeAndComment.loadData()
        viewLikeAndComment.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 0)
    }
    
    // MARK: - Overrides
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tbvContent.layer.removeAllAnimations()
        
        self.heightTbv.constant = tbvContent.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Get Hieght AttributedString
    
    func getHeightAttributedString(text: NSAttributedString, cell: HomeDetailTableViewCell){
        let height = text.height(containerWidth: cell.textView.frame.width)
        cell.heightTextView.constant = height + 20
        cell.layoutIfNeeded()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.updateNumberComment, object: nil)
    }
}

// MARK: - UITableViewDelegate

extension HomeQCViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (homeFeed.media?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbvContent.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeDetailTableViewCell, for: indexPath)!
        if indexPath.row != 0{
            let media = homeFeed.media![indexPath.row - 1]
            cell.textView.attributedText = media.descriptionValue?.html2AttributedString ?? NSAttributedString.init(string: "")
            cell.imgPost.kf.setImage(with: URL.init(string: media.link ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
            self.getHeightAttributedString(text: cell.textView.attributedText, cell: cell)
            KingfisherManager.shared.retrieveImage(with: URL.init(string: media.link ?? "")! , options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                guard let img = image else { return }
                print(img.getCropRatio())
                cell.heightImg.constant = cell.imgPost.frame.width / (img.getCropRatio() < 1 ? img.getCropRatio() : img.getCropRatio() - 0.2 )
                cell.contentView.layoutIfNeeded()
                cell.layoutIfNeeded()
                self.tbvContent.reloadRows(at: [indexPath], with: .none)
            })
        }else{
            cell.heightImg.constant = 0
            cell.textView.attributedText = homeFeed.content?.html2AttributedString ?? NSAttributedString.init(string: "")
            self.getHeightAttributedString(text: cell.textView.attributedText, cell: cell)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return height ?? UITableView.automaticDimension
    }
}
