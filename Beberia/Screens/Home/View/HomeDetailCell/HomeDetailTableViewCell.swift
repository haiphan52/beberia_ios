//
//  HomeDetailTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 8/30/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SDWebImage

class HomeDetailTableViewCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var heightTextView: NSLayoutConstraint!
    @IBOutlet weak var textView: RichTextEditor!
    @IBOutlet weak var heightImg: NSLayoutConstraint!
    @IBOutlet weak var imgPost: SDAnimatedImageView!
    @IBOutlet weak var btnTapImage: UIButton!
    
    var tapImage:(_ index: Int)->() = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()

        btnTapImage.isHidden = true
        textView.isSelectable = true
        textView.dataDetectorTypes = UIDataDetectorTypes.link
        textView.isUserInteractionEnabled = true
        textView.isEditable = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func didPressTapImage(_ sender: Any) {
        tapImage(btnTapImage.tag)
    }
    
    @IBAction func didPressTapPlay(_ sender: Any) {
        tapImage(btnTapImage.tag)
    }
    
}
