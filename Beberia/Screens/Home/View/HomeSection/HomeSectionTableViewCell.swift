//
//  HomeSectionTableViewCell.swift
//  Beberia
//
//  Created by OS on 10/25/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class HomeSectionTableViewCell: UITableViewCell {

    @IBOutlet weak var btn0: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var imgBG1: UIImageView!
    @IBOutlet weak var imgBG2: UIImageView!
    @IBOutlet weak var imgBG3: UIImageView!
    @IBOutlet weak var imgIC1: UIImageView!
    @IBOutlet weak var imgIC2: UIImageView!
    @IBOutlet weak var imgIC3: UIImageView!
    @IBOutlet weak var lblText1: UILabel!
    @IBOutlet weak var lblText2: UILabel!
    @IBOutlet weak var lblText3: UILabel!
    @IBOutlet weak var view1: UIView!
    
    var tapViewClick: (_ index :Int) -> () = {_ in}
    var tapViewClick2: (_ index :Int) -> () = {_ in}
    override func awakeFromNib() {
        super.awakeFromNib()
        imgBG1.alpha = 0.4
        imgIC1.alpha = 0.4
        imgBG2.alpha = 0.4
        imgIC2.alpha = 0.4
        imgBG3.alpha = 0.4
        imgIC3.alpha = 0.4
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func action1(_ sender: Any) {
          tapViewClick(btn1.tag)
        imgBG1.alpha = 0.4
        imgIC1.alpha = 0.4
        imgBG2.alpha = 1
        imgIC2.alpha = 1
        imgBG3.alpha = 0.4
        imgIC3.alpha = 0.4
    }
    @IBAction func action2(_ sender: Any) {
            tapViewClick2(btn2.tag)
        imgBG1.alpha = 0.4
        imgIC1.alpha = 0.4
        imgBG2.alpha = 0.4
        imgIC2.alpha = 0.4
        imgBG3.alpha = 1
        imgIC3.alpha = 1
    }
    
    @IBAction func action0(_ sender: Any) {
        imgBG1.alpha = 1
        imgIC1.alpha = 1
        imgBG2.alpha = 0.4
        imgIC2.alpha = 0.4
        imgBG3.alpha = 0.4
        imgIC3.alpha = 0.4
    }
    
}
