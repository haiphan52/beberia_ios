//
//  ExpandTableViewCell.swift
//  Beberia
//
//  Created by OS on 10/25/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import TTGTagCollectionView
class ExpandTableViewCell: UITableViewCell {

    @IBOutlet weak var tagView: TTGTextTagCollectionView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var heightContraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        tagView.layer.cornerRadius = 10
        // Configure the view for the selected state
    }
    func setupTagView(test:[String], tag:Int){
        //        tagView.addTags([locations![0].locationNameVi, locations![1].locationNameVi])
        tagView.addTags(test)
        tagView.alignment = .center
        heightContraint.constant = 65
        let config = tagView.defaultConfig!
        config.textColor = UIColor.init(hexString: AppColor.colorSelect,alpha: 0.3)
        config.selectedTextColor = UIColor.init(hexString: AppColor.colorYellow)
        config.backgroundColor = UIColor.init(hexString: "f6f6f7",alpha: 1)
        config.selectedBackgroundColor = UIColor.init(hexString: AppColor.colorBGMenu)
        config.borderColor = UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.5)
        config.selectedBorderColor = UIColor.init(hexString: AppColor.colorYellow)
        config.borderWidth = 1
        config.selectedBorderWidth = 1
        config.cornerRadius = 15
        config.selectedCornerRadius = 15
        config.shadowOffset = CGSize.zero
        config.shadowRadius = 0
        config.shadowOpacity = 0
        config.exactHeight = 30
        config.exactWidth = 143
        tagView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: 0, right: 0)
        if tag == 1 {
            heightContraint.constant = 100
            tagView.alignment = .left
            config.exactWidth = 120
            tagView.contentInset = UIEdgeInsets(top: 12, left: 8, bottom: 0, right: 0)
        }
        tagView.enableTagSelection = true
        tagView.defaultConfig = config
        tagView.delegate = self
        tagView.reload()
    }
    func removeTagview() {
        tagView.removeAllTags()
        tagView.reload()
    }
}
extension ExpandTableViewCell: TTGTextTagCollectionViewDelegate{
    func textTagCollectionView(_ textTagCollectionView: TTGTextTagCollectionView!, didTapTag tagText: String!, at index: UInt, selected: Bool, tagConfig config: TTGTextTagConfig!) {
        tagView.setTagAt(0, selected: false)
        tagView.setTagAt(1, selected: false)
        tagView.setTagAt(2, selected: false)
        tagView.setTagAt(3, selected: false)
        tagView.setTagAt(index, selected: true)
//        indexLocation = Int(index)
//        if self.indexLocation == 0 {
//            if self.arrayHomeFeedsHN.count == 0 {
//                self.getHomeFeed(page: self.pageHN)
//            }
//        } else {
//            if self.arrayHomeFeedsHCM.count == 0 {
//                self.getHomeFeed(page: self.pageHCM)
//            }
//        }
//        tbvHome.reloadData()
    }
    
    
}
