//
//  PopupUpdateApp.swift
//  Beberia
//
//  Created by Lap on 11/16/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import UIKit


class PopupUpdateApp: UIView {
    
    
    @IBOutlet weak var btnUpdateApp: UIButton!
    @IBOutlet weak var lblDEsc: UILabel!
    
    var tapUpdate:()->() = {}
    
    override func awakeFromNib() {
        btnUpdateApp.layer.cornerRadius = 8
        self.layer.cornerRadius = 12
        
        lblDEsc.text = "Cập nhật app ngay \n nhận nhiều điều hay mẹ nhé!"
    }
    
    @IBAction func didPressUpdate(_ sender: Any) {
        tapUpdate()
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupUpdateApp.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> PopupUpdateApp {
        return R.nib.popupUpdateApp.instantiate(withOwner: nil)[0] as! PopupUpdateApp
    }
    
    
}
