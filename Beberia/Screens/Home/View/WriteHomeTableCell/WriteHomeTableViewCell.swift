//
//  WriteHomeTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 9/3/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SDWebImage

class WriteHomeTableViewCell: UITableViewCell, UITextViewDelegate {
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var heightTextView: NSLayoutConstraint!
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    @IBOutlet weak var tvContent: RichTextEditor!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var tfPlachoder: UITextView!
    //    @IBOutlet weak var heightImageGIF: NSLayoutConstraint!
//    @IBOutlet weak var imageGIF: SDAnimatedImageView!
    @IBOutlet weak var btnPlayGIF: UIButton!
    
    var tapDeleteImage: (_ index :Int, _ button: UIButton)->() = {_,_ in}
    var updateHeightOfRow :(_ textView: UITextView) -> () = {_ in}
    var tapPlayGIF: (_ index :Int)->() = {_ in}
    var beginEditTextView :(_ textView: UITextView) -> () = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tfPlachoder.text = "Nội dung (KHÔNG đăng bài hỏi đáp, bác sĩ chỉ tư vấn sức khoẻ trên Diễn đàn)"
        tfPlachoder.textColor = UIColor.lightGray
        
        tvContent.isScrollEnabled = false
        tvContent.autocorrectionType = .no
        tvContent.updateTextChange = { textView in
            self.updateHeightOfRow(textView)
        }
        tvContent.beginEditTextView = { textView in
            self.beginEditTextView(textView)
        }
        
        tvContent.delegate = self
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("ádsdad")
    }
    
    @IBAction func didPressTapImage(_ sender: Any) {
        tapPlayGIF(btnDelete.tag)
    }
    
    @IBAction func didPressPlayGif(_ sender: Any) {
        tapPlayGIF(btnDelete.tag)
    }
    
    @IBAction func didPressDeleteImage(_ sender: UIButton) {
        
        tapDeleteImage(btnDelete.tag, sender)
    }
}



