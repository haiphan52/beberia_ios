//
//  NotificationTableViewCell.swift
//  Beberia
//
//  Created by iMAC on 9/5/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var viewAcceptCancel: UIView!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    var secret = "Ẩn danh"
    var confirmActionFriend: (Int)->() = {_ in }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgAva.cornerRadius = imgAva.frame.height / 2
        // Initialization code
//        btnReject.isHidden = true
//        btnAccept.isHidden = true
        viewAcceptCancel.isHidden = true
    }
  
    func setuplikeFeed(likeFeed: SectionData,row:Int,typeNoti: NotificationList){
        let formattedString = NSMutableAttributedString()
            if likeFeed.data?[row].totalUser ?? 0 > 0 {
                formattedString
                    .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                    .normal(" cùng \(likeFeed.data?[row].totalUser ?? 0) người khác \(typeNoti.title)")
                
            }else {
                formattedString
                    .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                    .normal(" \(typeNoti.title)")
            }
            lblContent.attributedText = formattedString
            let date = Date(timeIntervalSince1970: TimeInterval(likeFeed.data?[row].createdAt ?? 0))
            imgAva.kf.setImage(with: URL(string: likeFeed.data?[row].userFrom?.avatar  ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
            imgIcon.image = typeNoti.image//R.image.ic_NotiHome() // UIImage(named: "group_3923")
            lblTitle.text = "\"\(likeFeed.data?[row].feed?.title ?? "")\""
            lblTime.text = date.timeAgoDisplayHomeFeed()
    }
    
    func setuplikeComment (likeFeed: SectionData,row:Int,typeNoti: NotificationList){
        let formattedString = NSMutableAttributedString()
        
        if likeFeed.data?[row].totalUser ?? 0 > 0 {
            formattedString
                .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                .normal(" cùng \(likeFeed.data?[row].totalUser ?? 0) người khác \(typeNoti.title)")
        }else {
            formattedString
                .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                .normal(" \(typeNoti.title)")
        }
        imgIcon.image = typeNoti.image//UIImage(named: "group_3923")
        let date = Date(timeIntervalSince1970: TimeInterval(likeFeed.data?[row].createdAt ?? 0))
        imgAva.kf.setImage(with: URL(string: likeFeed.data?[row].userFrom?.avatar  ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        lblContent.attributedText = formattedString
        lblTitle.text = "\"\(likeFeed.data?[row].comment?.content ?? "")\""
        lblTime.text = date.timeAgoDisplayHomeFeed()
    }
    
    func setupComFeed(likeFeed: SectionData,row:Int, typeNoti: NotificationList){
        let formattedString = NSMutableAttributedString()
        if likeFeed.data?[row].totalUser ?? 0 > 0 {
            if likeFeed.data?[row].feed?.userCreate?.id == UserInfo.shareUserInfo.id {
                formattedString
                    .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                    .normal(" cùng \(likeFeed.data?[row].totalUser ?? 0) người khác \(typeNoti.title) bạn ")
            }else {
                if typeNoti == .ComInfo || typeNoti == .RepcomInfo{
                    formattedString
                        .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                        .normal(" cùng \(likeFeed.data?[row].totalUser ?? 0) người khác \(typeNoti.title) ")
                }else{
                    formattedString
                        .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                        .normal(" cùng \(likeFeed.data?[row].totalUser ?? 0) người khác \(typeNoti.title) ")
                        .bold("\(likeFeed.data?[row].feed?.userCreate?.displayName ?? "")")
                }
                
            }
        }else {
            if likeFeed.data?[row].feed?.userCreate?.id == UserInfo.shareUserInfo.id {
                formattedString
                    .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                    .normal(" \(typeNoti.title) bạn ")
            }else {
                if typeNoti == .ComInfo || typeNoti == .RepcomInfo{
                    formattedString
                        .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                        .normal(" \(typeNoti.title) ")
                }else{
                    formattedString
                        .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                        .normal(" \(typeNoti.title) ")
                        .bold("\(likeFeed.data?[row].feed?.userCreate?.displayName ?? "")")
                }
                
            }
        }
        imgIcon.image = typeNoti.image //R.image.ic_NotiHome()
        let date = Date(timeIntervalSince1970: TimeInterval(likeFeed.data?[row].createdAt ?? 0))
        imgAva.kf.setImage(with: URL(string: likeFeed.data?[row].userFrom?.avatar  ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        lblContent.attributedText = formattedString
        lblTitle.text = "\"\(likeFeed.data?[row].feed?.title ?? "")\""
        lblTime.text = date.timeAgoDisplayHomeFeed()
    }
    
    func setuplikeRepCom(likeFeed: SectionData, row:Int, typeNoti: NotificationList){
        let formattedString = NSMutableAttributedString()
      
        if likeFeed.data?[row].totalUser ?? 0 > 0 {
            formattedString
                .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                .normal(" cùng \(likeFeed.data?[row].totalUser ?? 0) người khác \(typeNoti.title)")
        }else {
            formattedString
                .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                .normal(" \(typeNoti.title)")
        }
        imgIcon.image = typeNoti.image
        let date = Date(timeIntervalSince1970: TimeInterval(likeFeed.data?[row].createdAt ?? 0))
        imgAva.kf.setImage(with: URL(string: likeFeed.data?[row].userFrom?.avatar  ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        lblContent.attributedText = formattedString
        lblTitle.text = "\"\(likeFeed.data?[row].replyComment?.content ?? "")\""
        lblTime.text = date.timeAgoDisplayHomeFeed()
    }
    
    func setupRepCom(likeFeed: SectionData,row:Int, typeNoti: NotificationList){
        let formattedString = NSMutableAttributedString()
        if likeFeed.data?[row].totalUser ?? 0 > 0 {
            if likeFeed.data?[row].comment?.user?.id == UserInfo.shareUserInfo.id {
                formattedString
                    .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                    .normal(" cùng \(likeFeed.data?[row].totalUser ?? 0) người khác \(typeNoti.title) bạn ")
            }else {
                formattedString
                    .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                    .normal(" cùng \(likeFeed.data?[row].totalUser ?? 0) người khác \(typeNoti.title) ")
                    .bold("\(likeFeed.data?[row].comment?.user?.displayName ?? "") ")
            }
        }else {
            if likeFeed.data?[row].comment?.user?.id == UserInfo.shareUserInfo.id {
                formattedString
                    .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                    .normal(" \(typeNoti.title) bạn")
            }else {
                formattedString
                    .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
                    .normal(" \(typeNoti.title) ")
                    .bold("\(likeFeed.data?[row].comment?.user?.displayName ?? "") ")
            }
        }
        imgIcon.image = typeNoti.image  //R.image.ic_NotiHome()
        let date = Date(timeIntervalSince1970: TimeInterval(likeFeed.data?[row].createdAt ?? 0))
        imgAva.kf.setImage(with: URL(string: likeFeed.data?[row].userFrom?.avatar  ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        lblContent.attributedText = formattedString
        lblTitle.text = "\"\(likeFeed.data?[row].comment?.content ?? "")\""
        lblTime.text = date.timeAgoDisplayHomeFeed()
    }
    
    func setSenndRequestFriend(likeFeed: SectionData,row:Int, typeNoti: NotificationList){
        
        
        
        let formattedString = NSMutableAttributedString()
        
        formattedString
        .bold(likeFeed.data?[row].userFrom?.displayName ?? "")
        .normal("\(typeNoti.title)")
        
        imgIcon.image = typeNoti.image  //R.image.ic_NotiHome()
        
        imgAva.kf.setImage(with: URL(string: likeFeed.data?[row].userFrom?.avatar  ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        lblContent.attributedText = formattedString
        
        
        viewAcceptCancel.isHidden = false
        lblTime.text = ""
        lblTitle.text = ""
        if likeFeed.data?[row].userFrom?.isFollow == 1 {
           viewAcceptCancel.isHidden = true
            lblTitle.text = "Đã kết bạn"
        }
        
    }
    
    
    //MARK: Setup Secret
    func setuplikeSecret(likeSecret: SectionData,row:Int,typeNoti: NotificationList){
        let formattedString = NSMutableAttributedString()
        let numberSecret = likeSecret.data?[row].secret_number ?? 0
        if numberSecret > 0 {
            secret = "Ẩn danh \(likeSecret.data?[row].secret_number ?? 0)"
        }
        
        if likeSecret.data?[row].totalUser ?? 0 > 0 {
            formattedString
                .bold("\(secret)")
                .normal(" cùng \(likeSecret.data?[row].totalUser ?? 0) người khác \(typeNoti.title)")
            
        }else {
            
            formattedString
                .bold("\(secret)")
                .normal(" \(typeNoti.title)")
            
        }
        lblContent.attributedText = formattedString
        let date = Date(timeIntervalSince1970: TimeInterval(likeSecret.data?[row].createdAt ?? 0))
        imgAva.image = UIImage(named: "avaDefautSecret")
        imgIcon.image = typeNoti.image//R.image.ic_NotiHome() // UIImage(named: "group_3923")
        lblTitle.text = "\"\(likeSecret.data?[row].feed?.title ?? "")\""
        lblTime.text = date.timeAgoDisplayHomeFeed()
    }
    
    func setuplikeCommentSecret (likeSecret: SectionData,row:Int,typeNoti: NotificationList){
        let formattedString = NSMutableAttributedString()
        let numberSecret = likeSecret.data?[row].secret_number ?? 0
        if numberSecret > 0 {
            secret = "Ẩn danh \(likeSecret.data?[row].secret_number ?? 0)"
        }
        if likeSecret.data?[row].totalUser ?? 0 > 0 {
            formattedString
                .bold("\(secret)")
                .normal(" cùng \(likeSecret.data?[row].totalUser ?? 0) người khác \(typeNoti.title)")
        }else {
            formattedString
                .bold("\(secret)")
                .normal(" \(typeNoti.title)")
            
        }
        imgIcon.image = typeNoti.image//UIImage(named: "group_3923")
        let date = Date(timeIntervalSince1970: TimeInterval(likeSecret.data?[row].createdAt ?? 0))
        imgAva.image = UIImage(named: "avaDefautSecret")
        lblContent.attributedText = formattedString
        lblTitle.text = "\"\(likeSecret.data?[row].comment?.content ?? "")\""
        lblTime.text = date.timeAgoDisplayHomeFeed()
    }
    
    func setupComSecret(likeSecret: SectionData,row:Int, typeNoti: NotificationList){
        let formattedString = NSMutableAttributedString()
        
        let numberSecret = likeSecret.data?[row].secret_number ?? 0
        if numberSecret > 0 {
            secret = "Ẩn danh \(likeSecret.data?[row].secret_number ?? 0)"
        }
        
        if likeSecret.data?[row].totalUser ?? 0 > 0 {
            if likeSecret.data?[row].feed?.userCreate?.id == UserInfo.shareUserInfo.id {
                formattedString
                    .bold("\(secret)")
                    .normal(" cùng \(likeSecret.data?[row].totalUser ?? 0) người khác \(typeNoti.title) bạn ")
            }else {
                formattedString
                    .bold("\(secret)")
                    .normal(" cùng \(likeSecret.data?[row].totalUser ?? 0) người khác \(typeNoti.title) ")
                    .bold("#BB\(likeSecret.data?[row].feed?.id ?? 0)")
            }
        }else {
            if likeSecret.data?[row].feed?.userCreate?.id == UserInfo.shareUserInfo.id {
                formattedString
                    .bold("\(secret)")
                    .normal(" \(typeNoti.title) bạn ")
            }else {
                formattedString
                    .bold("\(secret)")
                    .normal(" \(typeNoti.title) ")
                    .bold("#BB\(likeSecret.data?[row].feed?.id ?? 0)")
            }
        }
        imgIcon.image = typeNoti.image //R.image.ic_NotiHome()
        let date = Date(timeIntervalSince1970: TimeInterval(likeSecret.data?[row].createdAt ?? 0))
        imgAva.image = UIImage(named: "avaDefautSecret")
        lblContent.attributedText = formattedString
        lblTitle.text = "\"\(likeSecret.data?[row].feed?.title ?? "")\""
        lblTime.text = date.timeAgoDisplayHomeFeed()
    }
    
    func setuplikeRepComSecret(likeSecret: SectionData, row:Int, typeNoti: NotificationList){
        let formattedString = NSMutableAttributedString()
        let numberSecret = likeSecret.data?[row].secret_number ?? 0
        if numberSecret > 0 {
            secret = "Ẩn danh \(likeSecret.data?[row].secret_number ?? 0)"
        }
           if likeSecret.data?[row].totalUser ?? 0 > 0 {
               formattedString
                   .bold("\(secret)")
                   .normal(" cùng \(likeSecret.data?[row].totalUser ?? 0) người khác \(typeNoti.title)")
           }else {
               formattedString
                .bold("\(secret)")
                   .normal(" \(typeNoti.title)")
           }
           imgIcon.image = typeNoti.image
           let date = Date(timeIntervalSince1970: TimeInterval(likeSecret.data?[row].createdAt ?? 0))
           imgAva.image = UIImage(named: "avaDefautSecret")
           lblContent.attributedText = formattedString
           lblTitle.text = "\"\(likeSecret.data?[row].replyComment?.content ?? "")\""
           lblTime.text = date.timeAgoDisplayHomeFeed()
       }
    
    func setupRepComSecret(likeSecret: SectionData,row:Int, typeNoti: NotificationList){
           let formattedString = NSMutableAttributedString()
        let numberSecret = likeSecret.data?[row].secret_number ?? 0
             if numberSecret > 0 {
                 secret = "Ẩn danh \(likeSecret.data?[row].secret_number ?? 0)"
             }
           if likeSecret.data?[row].totalUser ?? 0 > 0 {
               if likeSecret.data?[row].comment?.user?.id == UserInfo.shareUserInfo.id {
                   formattedString
                       .bold("\(secret)")
                       .normal(" cùng \(likeSecret.data?[row].totalUser ?? 0) người khác \(typeNoti.title) bạn ")
               }else {
                   formattedString
                       .bold("\(secret)")
                       .normal(" cùng \(likeSecret.data?[row].totalUser ?? 0) người khác \(typeNoti.title) ")
                       .bold("Ẩn danh \(likeSecret.data?[row].comment?.secretNumber ?? 0) ")
               }
           }else {
               if likeSecret.data?[row].comment?.user?.id == UserInfo.shareUserInfo.id {
                   formattedString
                       .bold("\(secret)")
                       .normal(" \(typeNoti.title) bạn")
               }else {
                   formattedString
                       .bold("\(secret)")
                       .normal(" \(typeNoti.title) ")
                       .bold("Ẩn danh \(likeSecret.data?[row].comment?.secretNumber ?? 0) ")
               }
           }
           imgIcon.image = typeNoti.image  //R.image.ic_NotiHome()
           let date = Date(timeIntervalSince1970: TimeInterval(likeSecret.data?[row].createdAt ?? 0))
           imgAva.image = UIImage(named: "avaDefautSecret")
           lblContent.attributedText = formattedString
           lblTitle.text = "\"\(likeSecret.data?[row].comment?.content ?? "")\""
           lblTime.text = date.timeAgoDisplayHomeFeed()
    }
    
    @IBAction func didPressConfirm(_ sender: UIButton) {
        let type = sender.tag == 60 ? 1 : 2
        
        confirmActionFriend(type)
       
    }
    
    
}
