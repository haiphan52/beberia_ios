//
//  HomeNTableViewCell.swift
//  Beberia
//
//  Created by OS on 10/22/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SDWebImage

class HomeNTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnTvContent: UIButton!
    @IBOutlet weak var viewColor: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgPost: SDAnimatedImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDecs: UILabel!
    @IBOutlet weak var imgUserCreate: UIImageView!
    @IBOutlet weak var lblNameCreate: UILabel!
    @IBOutlet weak var lblDateCreate: UILabel!
    @IBOutlet weak var lblNumberComment: UILabel!
    @IBOutlet weak var lblNumberLike: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var widthbtnDelete: NSLayoutConstraint!
    @IBOutlet var ratioWidthImage: NSLayoutConstraint!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var tvContent: RichTextEditor!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var imgMB: UIImageView!
    @IBOutlet weak var lblMB: UILabel!
    
    var tapShowAlert:(_ index :Int) -> () = {_ in}
    var tapContent:(_ index :Int) -> () = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        imgUserCreate.cornerRadius = imgUserCreate.frame.height / 2
        imgPost.cornerRadius = 8
        viewContainer.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 5)
        // Configure the view for the selected state
    }
    
    func stringAddFontSizeAttributedString(content: String)->NSAttributedString{
        var content1 = content
        content1 = "<span" + Key.FontSizeAttribuite.FontSize + content1 + "</span>"
        return content1.html2AttributedString ?? NSAttributedString.init(string: "")
    }
    
    func setupDataHomeFeed(homeFeed: HomeFeed){
        lblNumberComment.text = "\(homeFeed.numberComment ?? 0)"
        lblNumberLike.text = "\(homeFeed.numberLike ?? 0)"
        lblNameCreate.text = homeFeed.userCreate?.displayName
        
        ratioWidthImage.constant = 0
        if (homeFeed.media ?? [Media]()).count > 0{
            imgPost.setImage(imageString: homeFeed.media?[0].link ?? "")
            ratioWidthImage.constant = 80
            if homeFeed.media?[0].link ?? "" == "" {
                ratioWidthImage.constant = 0
            }
        }

        imgLike.image = R.image.ic_like()
        if homeFeed.checkLike == 1 {
            imgLike.image = R.image.icLikeActive()
        }

        imgUserCreate.setImage(imageString: homeFeed.userCreate?.avatar ?? "")
       // imgUserCreate.kf.setImage(with: URL.init(string: homeFeed.userCreate?.avatar ?? "") ,placeholder: UIImage.init(named: Key.ImagePlaceholder))
        lblDecs.text = homeFeed.title ?? ""

        var content1 = homeFeed.content!
        if !content1.contains("<span"){
            content1 = "<span" + Key.FontSizeAttribuite.FontSize + content1 + "</span>"
        }
        
        
        let data = content1.data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSMutableAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        if let attrStr = attrStr {
            attrStr.addAttribute(.foregroundColor, value: UIColor.init(hexString: "000000", alpha: 0.6)!, range: NSRange(location: 0, length: attrStr.length))


            self.lblContent.attributedText = attrStr
            self.lblContent.lineBreakMode = .byTruncatingTail
        }

       // var baby = [BabyInfo]()
        lblInfo.text = CategoryFeed.init(rawValue: homeFeed.category ?? 1)?.title ?? ""
        lblInfo.textColor = UIColor.init(hexString: "\(CategoryFeed.init(rawValue: homeFeed.category ?? 1)?.color ?? "")",alpha: 0.6)
        viewColor.backgroundColor = UIColor.init(hexString: "\(CategoryFeed.init(rawValue: homeFeed.category ?? 1)?.colorLine ?? "")")

        var location = ""
        location = "\(homeFeed.district?.districtName ?? "") - \(homeFeed.cityName?.locationNameVi ?? "")"
        if homeFeed.district?.districtName ?? "" == "" {
            location = homeFeed.cityName?.locationNameVi ?? ""
        }

        if homeFeed.about ?? 1 == 2 {
            lblTitle.text = location
            viewInfo.backgroundColor = UIColor.init(hexString: "f7b9c4")
            imgMB.image = R.image.icMomNew()

            lblMB.text = "Mẹ"
            if homeFeed.age_range == 0{
                lblMB.text = "Mang thai"
            }
            if homeFeed.age_range == 6{
                lblMB.text = AgeRange.init(rawValue: homeFeed.age_range ?? 0)?.title ?? ""
            }

        }else{
                var ageRange = "Bé"
                if homeFeed.age_range != 0 && homeFeed.age_range != 6{
                    ageRange = AgeRange.init(rawValue: homeFeed.age_range ?? 0)?.title ?? "Bé"
                }

                lblTitle.text = location
                viewInfo.backgroundColor = UIColor.init(hexString: "6fdae8")
                imgMB.image = R.image.icBabyNew()
                lblMB.text = ageRange
        }

        lblDateCreate.text =  Date(timeIntervalSince1970: TimeInterval(homeFeed.created ?? 0)).timeAgoDisplayHomeFeed()
        
        
    }
    @IBAction func didPressContent(_ sender: Any) {
        tapContent(btnTvContent.tag)
    }
    
    @IBAction func didPressShow(_ sender: Any) {
        tapShowAlert(btnDelete.tag)
    }
}
