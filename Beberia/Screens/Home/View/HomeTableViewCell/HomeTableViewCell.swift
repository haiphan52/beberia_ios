//
//  HomeTableViewCell.swift
//  Beberia
//
//  Created by Lap on 12/14/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgUserCreate: UIImageView!
    @IBOutlet weak var viewImage: ViewImage!
    @IBOutlet weak var lblDecs: UILabel!
    @IBOutlet weak var lblNameCreate: UILabel!
    @IBOutlet weak var lblDateCreate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNumberComment: UILabel!
    @IBOutlet weak var lblNumberLike: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var imgMB: UIImageView!
    @IBOutlet weak var lblMB: UILabel!
    @IBOutlet weak var heightViewImage: NSLayoutConstraint!
    @IBOutlet weak var icComment: UIImageView!
    
    var tapComment:()->() = {}
    var tapLike:()->() = {}
    var tapSave:()->() = {}
    var refreshhCell: (() -> Void )?
    var dismissLightBoxTrigger: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func stringAddFontSizeAttributedString(content: String)->NSAttributedString{
        var content1 = content
        content1 = "<span" + Key.FontSizeAttribuite.FontSize + content1 + "</span>"
        return content1.html2AttributedString ?? NSAttributedString.init(string: "")
    }

    func setupDataHomeFeed(homeFeed: HomeFeed){
        
        viewImage.itemFeeds = homeFeed.media ?? [Media]()
        
        lblNumberComment.text = "\(homeFeed.numberComment ?? 0)"
        lblNumberLike.text = "\(homeFeed.numberLike ?? 0)"
        lblNameCreate.text = homeFeed.userCreate?.displayName

        heightViewImage.constant = 0
        if (homeFeed.media ?? [Media]()).count > 0{
            heightViewImage.constant = UIScreen.main.bounds.width - 24
            if (homeFeed.media ?? [Media]()).count > 3{
                heightViewImage.constant = UIScreen.main.bounds.width - 32
            }
            
            if homeFeed.media?[0].link ?? "" == "" {
                heightViewImage.constant = 0
            }
        }
        viewImage.dismissLightBoxTrigger = { [weak self] in
            guard let self = self else { return }
            self.dismissLightBoxTrigger?()
        }
        viewImage.heightViewImage = { [weak self] value in
            guard let wSelf = self else { return }
            wSelf.heightViewImage.constant = value
        }

        imgLike.image = R.image.icLikeNew()
        if homeFeed.checkLike == 1 {
            imgLike.image = R.image.icLikeActive()
        }
        
        btnSave.setImage(R.image.icSaveNew(), for: .normal)
        if homeFeed.checkSave == 1 {
            btnSave.setImage(R.image.icSaveActive(), for: .normal)
        }
        
        icComment.image = R.image.icCommentNew()
        if homeFeed.numberComment ?? 0 > 0 {
            icComment.image = R.image.icCommentActive()
        }
        
        if let linkAvatar = homeFeed.userCreate?.avatar {
            imgUserCreate.setImage(imageString: linkAvatar)
        } else {
            self.imgUserCreate.image = R.image.placeholder()!
        }

        lblTitle.text = homeFeed.title ?? ""

        var content1 = homeFeed.content ?? ""
//        self.lblDecs.text = content1
//
        if !content1.contains("<span"){
            content1 = "<span" + Key.FontSizeAttribuite.FontSize + (homeFeed.content ?? "") + "</span>"
        }
        
        
        let data = content1.data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSMutableAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        if let attrStr = attrStr {
            attrStr.addAttribute(.foregroundColor, value: UIColor.init(hexString: "000000", alpha: 0.6)!, range: NSRange(location: 0, length: attrStr.length))


            self.lblDecs.attributedText = attrStr
            self.lblDecs.lineBreakMode = .byTruncatingTail
        }

        
//
//       // var baby = [BabyInfo]()
//        lblInfo.text = CategoryFeed.init(rawValue: homeFeed.category ?? 1)?.title ?? ""
//        lblInfo.textColor = UIColor.init(hexString: "\(CategoryFeed.init(rawValue: homeFeed.category ?? 1)?.color ?? "")",alpha: 0.6)
//        viewColor.backgroundColor = UIColor.init(hexString: "\(CategoryFeed.init(rawValue: homeFeed.category ?? 1)?.colorLine ?? "")")

        var location = ""
        location = "\(homeFeed.district?.districtName ?? "") - \(homeFeed.cityName?.locationNameVi ?? "")"
        if homeFeed.district?.districtName ?? "" == "" {
            location = homeFeed.cityName?.locationNameVi ?? ""
        }
        lblLocation.text = location

        if homeFeed.about ?? 1 == 2 {

            imgMB.image = R.image.icMomNew()
            lblMB.textColor = UIColor.init(hexString: "0299fe")
            lblMB.text = "Mẹ"
            if homeFeed.age_range == 0{
                lblMB.text = "Mang thai"
            }
            if homeFeed.age_range == 6{
                lblMB.text = AgeRange.init(rawValue: homeFeed.age_range ?? 0)?.title ?? ""
            }

        }else{
                var ageRange = "Bé"
                if homeFeed.age_range != 0 && homeFeed.age_range != 6{
                    ageRange = AgeRange.init(rawValue: homeFeed.age_range ?? 0)?.title ?? "Bé"
                }
            lblMB.textColor = UIColor.init(hexString: "fd799d")
                imgMB.image = R.image.icBabyNew()
                lblMB.text = ageRange
        }

       lblDateCreate.text =  Date(timeIntervalSince1970: TimeInterval(homeFeed.created ?? 0)).timeAgoDisplayHomeFeed()
        
        
    }
    
    @IBAction func didPressLike(_ sender: Any) {
        tapLike()
    }
    
    @IBAction func didPressComment(_ sender: Any) {
        tapComment()
    }
    
    @IBAction func didPressBtnSave(_ sender: Any) {
        tapSave()
    }
}
