//
//  SearchHomeTableViewCell.swift
//  Beberia
//
//  Created by iMAC on 9/3/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class SearchHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
