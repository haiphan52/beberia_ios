//
//  HomeQCTableViewCell.swift
//  Beberia
//
//  Created by OS on 11/1/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class HomeQCTableViewCell: UITableViewCell {

    @IBOutlet weak var viewDesc: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgQC: UIImageView!
    @IBOutlet weak var lblNumberComment: UILabel!
    @IBOutlet weak var lblNumberLike: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var lblDecs: UILabel!
    @IBOutlet weak var icComment: UIImageView!
    @IBOutlet weak var btnSave: UIButton!

    var tapComment:()->() = {}
    var tapLike:()->() = {}
    var tapSave:()->() = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewDesc.cornerRadius = 5
        viewDesc.clipsToBounds = true
        viewContainer.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 5)
     
    }

    func setupDataHomeFeedQC(homeFeed: HomeFeed){
        lblNumberComment.text = "\(homeFeed.numberComment ?? 0)"
        lblNumberLike.text = "\(homeFeed.numberLike ?? 0)"

        if (homeFeed.media ?? [Media]()).count > 0{
            imgQC.kf.setImage(with: URL.init(string: (homeFeed.cover ?? [Media]()).count > 0 ? homeFeed.cover?[0].link ?? "" : homeFeed.media?[0].link ?? "") ,placeholder: UIImage.init(named: Key.ImagePlaceholder))

        }
        imgLike.image = R.image.ic_like()
        if homeFeed.checkLike == 1 {
            imgLike.image = R.image.icLikeActive()
        }
        lblDecs.text = homeFeed.title ?? ""
        
        btnSave.setImage(R.image.icSaveNew(), for: .normal)
        if homeFeed.checkSave == 1 {
            btnSave.setImage(R.image.icSaveActive(), for: .normal)
        }
        
        icComment.image = R.image.icCommentNew()
        if homeFeed.numberComment ?? 0 > 0 {
            icComment.image = R.image.icCommentActive()
        }

//        lblDateCreate.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(homeFeed.created ?? 0)), format: Key.DateFormat.DateFormatddMMyy)

    }
    
    @IBAction func didPressLike(_ sender: Any) {
        tapLike()
    }
    
    @IBAction func didPressComment(_ sender: Any) {
        tapComment()
    }
    
    @IBAction func didPressBtnSave(_ sender: Any) {
        tapSave()
    }
    
}
