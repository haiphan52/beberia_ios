//
//  ShopVC.swift
//  Beberia
//
//  Created by haiphan on 06/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ShopVC: BaseViewController, SetUpFunctionBase, ShopActionProtocol {
    
    @IBOutlet weak var contentSearchView: UIView!
    @IBOutlet weak var contentTradeMakeView: UIView!
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var contentTradeMarkElement: UIView!
    @IBOutlet weak var contentJobHot: UIView!
    @IBOutlet weak var contentGoodDeal: UIView!
    @IBOutlet weak var contentSectionProduct: UIView!
    @IBOutlet weak var contenGuessMomVuew: UIView!
    private let searchView: MomSearch = .loadXib()
    private let tradeMarkView: TradeMark = .loadXib()
    private let tradeMarkElementView: BigTradeMark = .loadXib()
    private let jobHotView: TradeMarkElement = .loadXib()
    private let goodDealView: GoodDealView = .loadXib()
    private let sectionProduct: ProductPortfolioView = .loadXib()
    private let guessMomView: GuessMom = .loadXib()
    private var viewModel: ShopViewModel = ShopViewModel()
    private let alertComingSoon: PublishSubject<Void> = PublishSubject.init()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.viewModel.getShopHome()
    }
    
}
extension ShopVC {
    
    func setupUI() {
        self.contentSearchView.addSubview(searchView)
        searchView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        searchView.delegate = self
        searchView.hideBtClose()
        
        self.contentTradeMakeView.addSubview(tradeMarkView)
        tradeMarkView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        tradeMarkView.delegate = self
        
        self.contentTradeMarkElement.addSubview(tradeMarkElementView)
        tradeMarkElementView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        tradeMarkElementView.actionSeeAll = { [weak self] in
            guard let self = self else { return }
            self.alertComingSoon.onNext(())
//            self.moveToTradeMark()
        }
        
        self.contentJobHot.addSubview(jobHotView)
        jobHotView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        jobHotView.setStatus(status: .jobHot)
        jobHotView.setTitle(title: "Mách mẹ ngành hàng HOT")
        
        self.contentGoodDeal.addSubview(goodDealView)
        goodDealView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.contentGoodDeal.isHidden = true
        
        self.contentSectionProduct.addSubview(sectionProduct)
        sectionProduct.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        self.contenGuessMomVuew.addSubview(guessMomView)
        guessMomView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        guessMomView.setTtile(text: "Gợi ý dành cho Mẹ")
        guessMomView.selectIndex = { [weak self] productID in
            guard let self = self else {
                return
            }
            self.alertComingSoon.onNext(())
//            self.moveToProductDetail(productID: productID, hidesBottomBarWhenPushed: true)
        }
        
        getBanner()
        self.viewModel.dealHot(pageToken: "")
    }
    
    func setupRX() {
        self.viewModel.shopHomeModel
            .withUnretained(self)
            .bind { owner, model in
                guard let model = model else { return }
                if let malls = model.data.mall {
                    owner.tradeMarkElementView.setValue(values: malls)
                    ShopStreamManage.shared.updateMalls(malls: malls)
                }
                if let hot = model.data.categoryHot {
                    owner.jobHotView.setValue(values: hot)
                }
                if let cats = model.data.category {
                    owner.sectionProduct.setValue(values: cats)
                }
            }.disposed(by: disposeBag)
        
        self.viewModel
            .shopRecommend
            .withUnretained(self)
            .bind { owner, item in
                if let list = item?.data?.products {
                    owner.guessMomView.setValue(values: list, totalItem: 1)
                }
            }.disposed(by: disposeBag)
        
        self.alertComingSoon
            .withUnretained(self)
            .bind { owner, _ in
                owner.showAlert(title: nil, message: "Sắp ra mắt!")
        }.disposed(by: disposeBag)
        
        self.viewModel.dealHot.withUnretained(self)
            .bind { owner, item in
                if let list = item?.data?.products {
                    owner.contentGoodDeal.isHidden = list.isEmpty
                    owner.goodDealView.setValues(values: list)
                }
            }.disposed(by: disposeBag)
    }
    
    private func getBanner(){
        var arrayImage = [String]()
        APIManager.getBanner(type: 11, callbackSuccess: { [weak self]  (banners) in
            guard let self = self else { return }
            for banner in banners {
                arrayImage.append(banner.link ?? "")
            }
            
            self.showBannerView(isShow: arrayImage.count > 0)
            self.bannerView.arrayImage = arrayImage
            self.bannerView.tapBanner = { index in
                guard let bannerType = BannerView.BannerTypeRedirect(rawValue: banners[index].typeRedirect ?? 0 ) else {
                    return
                }
                switch bannerType {
                case .none:
                    if let url = URL.init(string: banners[index].linkAds ?? "")  {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url , options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                case .rankPoint:
                    let rankPointVC = RankPointVC.init(nib: R.nib.rankPointVC)
                    rankPointVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(rankPointVC, animated: true)
                case .giftGame:
                    let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
                    gameVC.modalPresentationStyle = .overFullScreen
                    self.present(gameVC, animated: true, completion: nil)
                case .info:
                    guard let lichKTVC = R.storyboard.main.infoViewController() else{ return }
                    lichKTVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(lichKTVC, animated: true)
                case .momTok:
                    guard let momtokVC = R.storyboard.main.secretViewController() else {
                        return
                    }
                    momtokVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(momtokVC, animated: true)
                case .news:
                    let listNewHotVC = ListNewHotViewController.init(nib: R.nib.listNewHotViewController)
                    listNewHotVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(listNewHotVC, animated: true)
                case .listDiary:
                    let postVC = ListDiaryViewController.init(nib: R.nib.listDiaryViewController)
                    postVC.hidesBottomBarWhenPushed = true
                    postVC.gettype = "3"
                    postVC.title = "Mẹo cho bạn"
                    self.navigationController?.pushViewController(postVC, animated: true)
                case .home:
                    guard let tabbar  = self.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 1
                case .diaary:
                    guard let tabbar  = self.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 2
                }
            }
            self.bannerView.backgroundColor = .clear
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        })
    }
    
    private func showBannerView(isShow: Bool) {
        self.bannerView.isHidden = !isShow
    }
    
}
extension ShopVC: MomSearchDelegate {
    func action(action: MomSearch.ActionBt) {
        switch action {
        case .search:
            self.alertComingSoon.onNext(())
//            self.moveToSearch(text: "", item: self.viewModel.shopHomeModel.value)
        case .cart:
            self.alertComingSoon.onNext(())
//            
//            let vc = PaymentCartVC.createVC()
//            vc.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(vc, animated: true)
        case .close, .noti: break
        }
    }
}
extension ShopVC: TradeMarkDelegate {
    func selecteđInex(index: Int) {
        self.alertComingSoon.onNext(())
//        self.moveToSectiondetail(type: .homeShop)
//        self.moveToTradeMark()
    }
}
