//
//  ShopStreamManage.swift
//  Beberia
//
//  Created by haiphan on 03/10/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

final class ShopStreamManage {
    static var shared = ShopStreamManage()
    
    var malls: [MallShopModel] = []
    
    private init() { }
    
    func updateMalls(malls: [MallShopModel]) {
        self.malls = malls
    }
}
