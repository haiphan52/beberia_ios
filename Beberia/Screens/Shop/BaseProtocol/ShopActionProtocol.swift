//
//  ShopActionProtocol.swift
//  Beberia
//
//  Created by haiphan on 08/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import UIKit

protocol ShopActionProtocol {}
extension ShopActionProtocol {
    
    func moveToTradeMark() {
        guard let topVC = ManageApp.shared.topViewController() else {
            return
        }
        let vc = TradeMarkVC.createVC()
        vc.hidesBottomBarWhenPushed = true
        topVC.navigationController?.pushViewController(vc, animated: true)
    }
    
    func moveToSectiondetail(type: SectionDetailVC.Openfrom , textSearch: String? = nil, mallModel: MallShopModel? = nil) {
        guard let topVC = ManageApp.shared.topViewController() else {
            return
        }
        let vc = SectionDetailVC.createVC()
        vc.hidesBottomBarWhenPushed = true
        vc.statusOpen = type
        vc.textSearch = textSearch
        vc.mallModel = mallModel
        topVC.navigationController?.pushViewController(vc, animated: true)
    }
    
    func moveToProductDetail(productID: Int, hidesBottomBarWhenPushed: Bool) {
        guard let topVC = ManageApp.shared.topViewController() else {
            return
        }
        let vc = ProductDetailVC.createVC()
        vc.productID = productID
        vc.hidesBottomBarWhenPushed = hidesBottomBarWhenPushed
        topVC.navigationController?.pushViewController(vc, animated: true)
    }
    
    func moveToSearch(text: String, item: ShopHomeModel?) {
        guard let topVC = ManageApp.shared.topViewController() else {
            return
        }
        let vc = SearchShopVC.createVC()
        let navi = UINavigationController(rootViewController: vc)
        navi.modalPresentationStyle = .fullScreen
        vc.item = item
        topVC.present(navi, animated: true, completion: nil)
    }
    
}
