
//
//  
//  SearchShopVC.swift
//  Beberia
//
//  Created by haiphan on 07/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

class SearchShopVC: BaseVC, SetupTableView, ShopActionProtocol {
    
    
    var item: ShopHomeModel?
    
    // Add here outlets
    @IBOutlet weak var contentSearchView: UIView!
    @IBOutlet weak var contentTradeMakeView: UIView!
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var contentTradeMarkElement: UIView!
    private let searchView: MomSearch = .loadXib()
    private let tradeMarkView: TradeMark = .loadXib()
    private let tradeMarkElementView: BigTradeMark = .loadXib()
    private let sources: BehaviorRelay<[String]> = BehaviorRelay.init(value: [])
    private var totalItem: Int = 1
    @IBOutlet weak var tableView: UITableView!
    // Add here your view model
    private var viewModel: SearchShopVM = SearchShopVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
}
extension SearchShopVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        self.contentSearchView.addSubview(searchView)
        searchView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        searchView.delegate = self
        searchView.hideCartButton()
        searchView.hideNotiButton()
        searchView.hideSearchButton()
        searchView.searchText = { [weak self] text in
            guard let self = self else { return }
            self.tableView.isHidden = text.isEmpty
            self.sources.accept([])
            self.viewModel.searchProduct(text: text, nextPage: 1)
        }
        tableView.isHidden = searchView.isHideSearch()
        if let text = searchView.getText() {
            viewModel.searchProduct(text: text, nextPage: 1)
        }
        
        
        self.contentTradeMakeView.addSubview(tradeMarkView)
        tradeMarkView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.contentTradeMarkElement.addSubview(tradeMarkElementView)
        tradeMarkElementView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        if let item = item, let mall = item.data.mall {
            tradeMarkElementView.setValue(values: mall)
        }
        
        getBanner()
        
        setupTableView(tableView: tableView, delegate: self, name: SearchCell.self)
        tableView.separatorStyle = .singleLine
        tableView.prefetchDataSource = self
        
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        viewModel.searchProduct.withUnretained(self).bind { owner, item in
            guard let item = item,
                  let products = item.data?.products,
                  let total = item.data?.total else {
                return
            }
            
            let list = products.map { $0.productName }.compactMap { $0 } + owner.sources.value
            owner.totalItem = total
            owner.sources.accept(list)
        }.disposed(by: disposeBag)
        
        sources
            .bind(to: tableView.rx.items(cellIdentifier: SearchCell.identifier, cellType: SearchCell.self)) {(row, element, cell) in
                cell.setTitle(text: element)
                cell.hideCloseButton()
                cell.hideiConHistory()
            }.disposed(by: disposeBag)
        
        self.tableView.rx
            .itemSelected
            .withUnretained(self)
            .bind { owner, idx in
                let item = owner.sources.value[idx.row]
                owner.moveToSectiondetail(type: .search, textSearch: item)
        }.disposed(by: disposeBag)
    }
    
    private func getBanner(){
        var arrayImage = [String]()
        APIManager.getBanner(type: ConstantApp.shared.bannerType, callbackSuccess: { [weak self]  (banners) in
            guard let self = self else { return }
            for banner in banners {
                arrayImage.append(banner.link ?? "")
            }
            
            self.showBannerView(isShow: arrayImage.count > 0)
            self.bannerView.arrayImage = arrayImage
            self.bannerView.tapBanner = { index in
                guard let bannerType = BannerView.BannerTypeRedirect(rawValue: banners[index].typeRedirect ?? 0 ) else {
                    return
                }
                switch bannerType {
                case .none:
                    if let url = URL.init(string: banners[index].linkAds ?? "")  {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url , options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                case .rankPoint:
                    let rankPointVC = RankPointVC.init(nib: R.nib.rankPointVC)
                    rankPointVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(rankPointVC, animated: true)
                case .giftGame:
                    let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
                    gameVC.modalPresentationStyle = .overFullScreen
                    self.present(gameVC, animated: true, completion: nil)
                case .info:
                    guard let lichKTVC = R.storyboard.main.infoViewController() else{ return }
                    lichKTVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(lichKTVC, animated: true)
                case .momTok:
                    guard let momtokVC = R.storyboard.main.secretViewController() else {
                        return
                    }
                    momtokVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(momtokVC, animated: true)
                case .news:
                    let listNewHotVC = ListNewHotViewController.init(nib: R.nib.listNewHotViewController)
                    listNewHotVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(listNewHotVC, animated: true)
                case .listDiary:
                    let postVC = ListDiaryViewController.init(nib: R.nib.listDiaryViewController)
                    postVC.hidesBottomBarWhenPushed = true
                    postVC.gettype = "3"
                    postVC.title = "Mẹo cho bạn"
                    self.navigationController?.pushViewController(postVC, animated: true)
                case .home:
                    guard let tabbar  = self.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 1
                case .diaary:
                    guard let tabbar  = self.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 2
                }
            }
            self.bannerView.backgroundColor = .clear
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        })
    }
    private func showBannerView(isShow: Bool) {
        self.bannerView.isHidden = !isShow
    }
}
extension SearchShopVC: MomSearchDelegate {
    func action(action: MomSearch.ActionBt) {
        switch action {
        case .search:
            let vc = CheckVC.createVC()
            self.navigationController?.pushViewController(vc)
        case .close:
            self.dismiss(animated: true)
        case .cart, .noti: break
        }
    }
}
extension SearchShopVC: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let value = sources.value.count
        guard ManageApp.shared.detectLoadPaging(indexPaths: indexPaths,
                                                value: value,
                                                totalItem: totalItem) else {
            return
        }
        let nexPage = self.viewModel.searchProduct.value?.data?.nextPage ?? 1
        viewModel.searchProduct(text: searchView.getText() ?? "", nextPage: nexPage)
    }
    
}
extension SearchShopVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
}
