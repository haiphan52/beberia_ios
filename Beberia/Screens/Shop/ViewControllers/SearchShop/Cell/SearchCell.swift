//
//  SearchCell.swift
//  Beberia
//
//  Created by haiphan on 02/10/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift

class SearchCell: UITableViewCell {

    @IBOutlet weak var iconHistory: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btClose: UIButton!
    
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        setupRX()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension SearchCell {
    
    func setupRX() {
        btClose.rx.tap
            .withUnretained(self)
            .bind { owner, _ in
            }.disposed(by: disposeBag)
    }
    
    func setTitle(text: String) {
        lbTitle.text = text
    }
    
    func hideiConHistory() {
        iconHistory.isHidden = true
    }
    
    func hideCloseButton() {
        btClose.isHidden = true
    }
    
}
