
//
//  ___HEADERFILE___
//
import Foundation
import RxCocoa
import RxSwift

class SearchShopVM: ErrorTrackerProtocol {
    
    var errorTracker: ErrorTracker = ErrorTracker()
    let searchProduct: BehaviorRelay<ShopRecommenDatadMode?> = BehaviorRelay.init(value: nil)
    
    private let disposeBag = DisposeBag()
    init() {
        
    }
    
    func searchProduct(text: String, nextPage: Int) {
        AlamorfireService.shared.searchShop(text: text, page: nextPage, searchType: .relateProduct)
            .trackError(errorTracker)
            .bind(to: searchProduct).disposed(by: disposeBag)
    }
}
