//
//  ProductDetailVM.swift
//  Beberia
//
//  Created by Hai Phan Thanh on 09/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import RxRelay
import RxSwift

final class ProductDetailVM {
    
    let detailProduct: BehaviorRelay<DetailProductDateModel?> = BehaviorRelay.init(value: nil)
    let searchProduct: BehaviorRelay<ShopRecommenDatadMode?> = BehaviorRelay.init(value: nil)
    private let disposeBag = DisposeBag()
    init() {
        getSearchProduct()
    }
    
    func getDetailProduct(productID: Int) {
        AlamorfireService.shared.getDetailProduct(productID: productID).bind(to: detailProduct).disposed(by: disposeBag)
    }
    
    func getSearchProduct() {
        AlamorfireService.shared.searchShop(text: "men vi sinh", page: 1, searchType: .relateProduct).bind(to: searchProduct).disposed(by: disposeBag)
    }
    
    func addToCart() {
        AlamorfireService.shared.addToCart().bind { item in
            print(item)
        }.disposed(by: disposeBag)
    }
    
}
