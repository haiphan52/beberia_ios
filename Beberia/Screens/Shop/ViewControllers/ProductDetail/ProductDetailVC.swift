//
//  ProductDetailVC.swift
//  Beberia
//
//  Created by Hai Phan Thanh on 09/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductDetailVC: BaseVC,
                       SetUpFunctionBase,
                       ShopActionProtocol {

    var productID: Int?
    
    @IBOutlet weak var imgBgContentView: UIView!
    @IBOutlet weak var containerIntroduceView: UIView!
    @IBOutlet weak var contentTheSameProduct: UIView!
    @IBOutlet weak var contentSelectPaymentView: UIView!
    @IBOutlet weak var contentOptions: UIView!
    @IBOutlet weak var contentShopInfo: UIView!
    private let imgBgView: ProductCell = .loadXib()
    private let introduceView: IntroduceProduct = .loadXib()
    private let sameProductView: GoodDealView = .loadXib()
    private let selectPaymentView: SelectPayment = .loadXib()
    private let optionsView: OptionsView = .loadXib()
    private let viewModel = ProductDetailVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

}
extension ProductDetailVC {
    
    func setupUI() {
        imgBgContentView.addSubview(imgBgView)
        imgBgView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.left.right.equalToSuperview().inset(16)
        }
        imgBgView.enableHeightLessThan(isActive: false)
        self.view.layoutIfNeeded()
        imgBgView.setHeightImage(value: imgBgView.frame.width - 32)
        imgBgView.hideDiscountView()
        imgBgView.hideOnlyRateView()
        imgBgView.spacingValueStackViewContent(spacing: 12)
        imgBgView.spacingValueStackViewImage(spacing: 17)
        imgBgView.spacingValueStackViewPrice(spacing: 8)
        imgBgView.enableRateView()
        imgBgView.enableDiscountPriceView()
        imgBgView.delegate = self
        imgBgView.showHeaderAction()
        
        containerIntroduceView.addSubview(introduceView)
        introduceView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        introduceView.delegate = self
        
        self.contentTheSameProduct.addSubview(sameProductView)
        sameProductView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        sameProductView.setStatus(status: .productDetail)
        sameProductView.setBgColor(color: .white)
        sameProductView.setLeftContraint(value: 0)
        sameProductView.hideSeeMore()
        sameProductView.setColorTitle(color: R.color.d4D4D() ?? .black)
        sameProductView.setTitle(text: "Sản phẩm tương tự")
        sameProductView.selectProdut = { [weak self] item in
            guard let self = self, let id = item.productID else { return }
            self.moveToProductDetail(productID: id, hidesBottomBarWhenPushed: true)
        }
        
        contentSelectPaymentView.addSubview(selectPaymentView)
        selectPaymentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        selectPaymentView.delegate = self
        
        contentOptions.addSubview(optionsView)
        optionsView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        if let id = self.productID {
            self.viewModel.getDetailProduct(productID: id)
        }
    }
    
    func setupRX() {
        self.viewModel.detailProduct.withUnretained(self).bind { owner, item in
            guard let item = item?.data else {
                return
            }
            owner.imgBgView.setValues(value: item)
            owner.introduceView.setDescription(desc: item.productDescription ?? "")
        }.disposed(by: disposeBag)
        
        self.viewModel.searchProduct.withUnretained(self).bind { owner, seachProducts in
            guard let seachProducts = seachProducts else {
                return
            }
            owner.sameProductView.setValues(values: seachProducts.data?.products ?? [])
        }.disposed(by: disposeBag)
    }
    
}
extension ProductDetailVC: ProductCellDelegate {
    func selectAction(action: ProductCell.Action) {
        switch action {
        case .back:
            self.navigationController?.popViewController(animated: true, nil)
        case .cart, .share: break
        }
    }
}
extension ProductDetailVC: SelectPaymentDelegate {
    func selectAction(action: SelectPayment.Action) {
        switch action {
        case .buy, .addToCart:
            let v: PaymentView = .loadXib()
            v.delegate = self
            v.hideCartView()
            v.showView()
            if let item = self.viewModel.detailProduct.value?.data {
                v.setValue(productPayment: item)
            }
        case .heart: break
        }
    }
}
extension ProductDetailVC: IntroduceProductDelegate {
    func selectAction(action: IntroduceProduct.Action) {
        switch action {
        case .writeComment: break
        case .seeComment:
            let vc = RateShopVC.createVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension ProductDetailVC: PaymentViewDelegate {
    func selectAction(action: PaymentView.Action) {
        self.viewModel.addToCart()
    }
}
