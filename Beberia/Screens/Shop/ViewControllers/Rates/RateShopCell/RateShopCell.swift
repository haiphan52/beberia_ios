//
//  RateShopCell.swift
//  Beberia
//
//  Created by haiphan on 11/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class RateShopCell: UITableViewCell, SetUpFunctionBase {

    private let contentRate: ContentRate = .loadXib()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
        setupRX()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension RateShopCell {
    
    func setupUI() {
        self.addSubview(contentRate)
        contentRate.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(16)
        }
    }
    
    func setupRX() {
        
    }
}
