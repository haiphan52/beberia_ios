
//
//  
//  RateShopVC.swift
//  Beberia
//
//  Created by haiphan on 11/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

class RateShopVC: BaseVC, SetupTableView {
    // Add here outlets
    
    // Add here your view model
    private var viewModel: RateShopVM = RateShopVM()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
}
extension RateShopVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        setupTableView(tableView: tableView, delegate: self, name: RateShopCell.self)
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        Observable.just([1,2])
            .bind(to: tableView.rx.items(cellIdentifier: RateShopCell.identifier, cellType: RateShopCell.self)) {(row, element, cell) in
                
            }.disposed(by: disposeBag)
    }
}
extension RateShopVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
