
//
//  
//  PaymentCartVC.swift
//  Beberia
//
//  Created by haiphan on 14/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class PaymentCartVC: BaseVC, SetupTableView {
    
    // Add here outlets
    @IBOutlet weak var heightBottom: NSLayoutConstraint!
    @IBOutlet weak var contentPayment: UIView!
    @IBOutlet weak var btPayment: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    // Add here your view model
    private var viewModel: PaymentCartVM = PaymentCartVM()
    private typealias Section = SectionModel<String, Double>
    private typealias DataSource = RxTableViewSectionedReloadDataSource<Section>
    private lazy var dataSource = makeDataSource()
    
    let items = Observable.just([
        SectionModel(model: "First section", items: [
            1.0,
            2.0,
            3.0
        ]),
        SectionModel(model: "Second section", items: [
            1.0,
            2.0,
            3.0
        ]),
        SectionModel(model: "Third section", items: [
            1.0,
            2.0,
            3.0
        ])
    ])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.fd799D()!,
                                 textColor: .white,
                                 isTranslucent: true)
        title = "Giỏ hàng của bạn"
    }
    
}
extension PaymentCartVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        heightBottom.constant = GetHeightSafeArea.shared.getHeight(type: .bottom)
        contentPayment.layer.setCornerRadius(corner: .verticalTop, radius: 16)
        setupTableView(tableView: tableView, delegate: self, name: PaymentCartCell.self)
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        btPayment.rx.tap
            .withUnretained(self)
            .bind { owner, _ in
                let vc = ShopCartVC.createVC()
                owner.navigationController?.pushViewController(vc)
            }.disposed(by: disposeBag)
        
        items.bind(to: tableView.rx.items(dataSource: dataSource)).disposed(by: disposeBag)
    }
    
    private func makeDataSource() -> DataSource {
        .init(
            configureCell: { dataSource, tableView, indexPath, item in
                let cell = tableView.dequeueReusableCell(withIdentifier: PaymentCartCell.identifier, for: indexPath) as! PaymentCartCell
                cell.backgroundColor = .red
                return cell
            }
        )
    }
}
extension PaymentCartVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.1
        }
        return 56
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v: UIView = UIView()
        
        v.backgroundColor = .white
        let stackView: UIStackView = UIStackView.init(frame: .zero)
        
        v.addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(16)
            make.top.bottom.equalToSuperview()
        }
        
        let img: UIImageView = UIImageView(image: R.image.ic_product_exist())
        stackView.addArrangedSubview(img)
        
        let lbName: UILabel = UILabel(frame: .zero)
        lbName.text = "Sản phẩm không tồn tại"
        lbName.font = UIFont.notoSansFont(weight: .medium, size: 16)
        lbName.textColor = R.color.d4D4D()
        stackView.addArrangedSubview(lbName)
        
        let view: UIView = UIView(frame: .zero)
        stackView.addArrangedSubview(view)
        
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = 8
        
        return v
    }
}
