//
//  PaymentCartCell.swift
//  Beberia
//
//  Created by haiphan on 14/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class PaymentCartCell: UITableViewCell {

    private let paymentCart: PaymentCartVIew = .loadXib()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension PaymentCartCell {
    
    private func setupUI() {
        self.addSubview(paymentCart)
        paymentCart.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
}
