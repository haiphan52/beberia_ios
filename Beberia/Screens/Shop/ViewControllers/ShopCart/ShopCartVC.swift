
//
//  
//  ShopCartVC.swift
//  Beberia
//
//  Created by haiphan on 14/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

class ShopCartVC: BaseVC {
    
    // Add here outlets
    @IBOutlet weak var contentPayment: UIView!
    @IBOutlet weak var heightBottom: NSLayoutConstraint!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var contentTrackingOrder: UIView!
    @IBOutlet weak var contentProductsView: UIView!
    private let trackingOrder: TrackingOrder = .loadXib()
    private let productsView: ProductsView = .loadXib()
    // Add here your view model
    private var viewModel: ShopCartVM = ShopCartVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.fd799D()!,
                                 textColor: .white,
                                 isTranslucent: true)
        title = "Giỏ hàng của bạn"
    }
    
}
extension ShopCartVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        heightBottom.constant = GetHeightSafeArea.shared.getHeight(type: .bottom)
        contentPayment.layer.setCornerRadius(corner: .verticalTop, radius: 16)
        makeName()
        
        contentTrackingOrder.addSubview(trackingOrder)
        trackingOrder.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        contentProductsView.addSubview(productsView)
        productsView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func setupRX() {
        // Add here the setup for the RX
    }
    
    private func makeName() {
        let att: NSMutableAttributedString = NSMutableAttributedString()
        
        let att1: NSAttributedString = NSAttributedString(string: "Phan Thanh Hải", attributes: [NSAttributedString.Key.font: UIFont.notoSansFont(weight: .medium, size: 13),
                                                                                                 NSAttributedString.Key.foregroundColor: R.color.d4D4D() ?? .black])
        att.append(att1)
        
        let att2: NSAttributedString = NSAttributedString(string: "\n0978834101\ntoan.nt141@gmail.com\ntoan.nt141@gmail.com",
                                                          attributes: [NSAttributedString.Key.font: UIFont.notoSansFont(weight: .regular, size: 13),
                                                                                                 NSAttributedString.Key.foregroundColor: R.color.d5B5B() ?? .black])
        att.append(att2)
        
        lbName.attributedText = att
        
    }
}
