
//
//  
//  TradeMarkVC.swift
//  Beberia
//
//  Created by haiphan on 03/10/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

class TradeMarkVC: BaseVC, SetupTableView, ShopActionProtocol {
    
    // Add here outlets
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var contentTradeMarkElement: UIView!
    @IBOutlet weak var tableView: UITableView!
    private let tradeMarkElementView: BigTradeMark = .loadXib()
    // Add here your view model
    private var viewModel: TradeMarkVM = TradeMarkVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.fd799D()!,
                                 textColor: .white,
                                 isTranslucent: true)
        title = "Thương hiêu chính hãng"
    }
    
}
extension TradeMarkVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        getBanner()
        bannerView.clipsToBounds = true
        bannerView.layer.cornerRadius = 0
        
        self.contentTradeMarkElement.addSubview(tradeMarkElementView)
        tradeMarkElementView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.tradeMarkElementView.hideHeaderView()
        self.tradeMarkElementView.setValue(values: ShopStreamManage.shared.malls)
        self.tradeMarkElementView.selectTradeMark = { [weak self] tradeMarkType in
            guard let self = self,
                  let tradeMarkType = tradeMarkType,
                  let mall = ShopStreamManage.shared.malls.first(where: { $0.id == tradeMarkType.idType }) else { return }
            self.moveToSectiondetail(type: .tradeMarkBig, textSearch: mall.name, mallModel: mall)
        }
        
        setupTableView(tableView: tableView, delegate: self, name: TradeMarkBigCell.self)
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        Observable.just(ShopStreamManage.shared.malls)
            .bind(to: tableView.rx.items(cellIdentifier: TradeMarkBigCell.identifier, cellType: TradeMarkBigCell.self)) { [weak self] (row, element, cell) in
                guard let self = self else { return }
                cell.setValue(mall: element)
                cell.actionSeeMore = { [weak self] in
                    guard let self = self else { return }
                    self.moveToSectiondetail(type: .tradeMarkBig, textSearch: element.name ?? "", mallModel: element)
                }
                cell.selectProdut = { [weak self] productID in
                    guard let self = self else { return }
                    self.moveToProductDetail(productID: productID, hidesBottomBarWhenPushed: true)
                }
            }.disposed(by: disposeBag)
    }
    
    private func getBanner(){
        var arrayImage = [String]()
        APIManager.getBanner(type: ConstantApp.shared.bannerType, callbackSuccess: { [weak self]  (banners) in
            guard let self = self else { return }
            for banner in banners {
                arrayImage.append(banner.link ?? "")
            }
            
            self.showBannerView(isShow: arrayImage.count > 0)
            self.bannerView.arrayImage = arrayImage
            self.bannerView.tapBanner = { index in
                guard let bannerType = BannerView.BannerTypeRedirect(rawValue: banners[index].typeRedirect ?? 0 ) else {
                    return
                }
                switch bannerType {
                case .none:
                    if let url = URL.init(string: banners[index].linkAds ?? "")  {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url , options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                case .rankPoint:
                    let rankPointVC = RankPointVC.init(nib: R.nib.rankPointVC)
                    rankPointVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(rankPointVC, animated: true)
                case .giftGame:
                    let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
                    gameVC.modalPresentationStyle = .overFullScreen
                    self.present(gameVC, animated: true, completion: nil)
                case .info:
                    guard let lichKTVC = R.storyboard.main.infoViewController() else{ return }
                    lichKTVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(lichKTVC, animated: true)
                case .momTok:
                    guard let momtokVC = R.storyboard.main.secretViewController() else {
                        return
                    }
                    momtokVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(momtokVC, animated: true)
                case .news:
                    let listNewHotVC = ListNewHotViewController.init(nib: R.nib.listNewHotViewController)
                    listNewHotVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(listNewHotVC, animated: true)
                case .listDiary:
                    let postVC = ListDiaryViewController.init(nib: R.nib.listDiaryViewController)
                    postVC.hidesBottomBarWhenPushed = true
                    postVC.gettype = "3"
                    postVC.title = "Mẹo cho bạn"
                    self.navigationController?.pushViewController(postVC, animated: true)
                case .home:
                    guard let tabbar  = self.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 1
                case .diaary:
                    guard let tabbar  = self.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 2
                }
            }
            self.bannerView.backgroundColor = .clear
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        })
    }
    
    private func showBannerView(isShow: Bool) {
        self.bannerView.isHidden = !isShow
    }
}
extension TradeMarkVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
