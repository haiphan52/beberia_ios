//
//  TradeMarkBigCell.swift
//  Beberia
//
//  Created by haiphan on 06/10/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class TradeMarkBigCell: UITableViewCell, SetUpFunctionBase {

    var actionSeeMore: (() -> Void)?
    var selectProdut: ((Int) -> Void)?
    
    private let sameProductView: GoodDealView = .loadXib()
    private let getProductTradeMark: PublishSubject<(Int, Int)> = PublishSubject.init()
    private var shopID: Int = 0
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
        setupRX()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension TradeMarkBigCell {
    
    func setupUI() {
        selectionStyle = .none
        self.addSubview(sameProductView)
        sameProductView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        sameProductView.setStatus(status: .productDetail)
        sameProductView.setBgColor(color: .white)
        sameProductView.setLeftContraint(value: 0)
        sameProductView.setColorTitle(color: R.color.d4D4D() ?? .black)
        sameProductView.setTitle(text: "Sản phẩm tương tự")
        sameProductView.setTextColorSeeAll(color: R.color.fE() ?? .blue)
        sameProductView.selectProdut = { [weak self] item in
            guard let self = self, let id = item.productID else { return }
            self.selectProdut?(id)
        }
        sameProductView.actionSeeMore = { [weak self] in
            guard let self = self else { return }
            self.actionSeeMore?()
        }
        sameProductView.loadNextPage = { [weak self] nextPage in
            guard let self = self else { return }
            self.getProductTradeMark.onNext((self.shopID, nextPage))
        }
    }
    
    func setValue(mall: MallShopModel) {
        sameProductView.setTitle(text: mall.name ?? "")
        if let shopId = mall.id {
            self.shopID = shopId
            getProductTradeMark.onNext((shopId, 1))
        }
    }
    
    func setupRX() {
        getProductTradeMark
            .asObservable()
            .flatMap { return AlamorfireService.shared.getProductTradeMark(shopID: $0.0, productTradeMarkType: .newest, page: $0.1) }
            .withUnretained(self)
            .bind { owner, item in
                guard let products = item.data?.products else {
                    return
                }
                owner.sameProductView.setTotalItem(totalItem: item.data?.total ?? 0)
                owner.sameProductView.setValues(values: products)
            }.disposed(by: disposeBag)
    }
    
    func setValue() {
        
    }
    
}
