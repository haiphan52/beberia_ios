
//
//  
//  SectionDetailVC.swift
//  Beberia
//
//  Created by haiphan on 11/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

class SectionDetailVC: BaseVC, ShopActionProtocol {
    
    enum Action: Int, CaseIterable {
        case menus, sort, filter
    }
    
    enum Openfrom {
        case search, homeShop, tradeMarkBig
    }
    
    var statusOpen: Openfrom = .homeShop
    var textSearch: String?
    var mallModel: MallShopModel?
    
    // Add here outlets
    @IBOutlet weak var contentSearchView: UIView!
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var contentMomGuess: UIView!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var menusView: UIView!
    private let searchView: MomSearch = .loadXib()
    private let guessMomView: GuessMom = .loadXib()
    
    // Add here your view model
    private var viewModel: SectionDetailVM = SectionDetailVM()
    private var nextPage: Int = 1
    private var totalPage: Int = 1
    private let searchProductTrigger: PublishSubject<Void> = PublishSubject.init()
    private var typeSort: ProductTradeMarkType = .newest
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}
extension SectionDetailVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        self.contentSearchView.addSubview(searchView)
        searchView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        searchView.delegate = self
        searchView.setImageBackButton(img: R.image.ic_Back())
        if let text = self.textSearch {
            searchView.setText(text: text)
        }
        
        getBanner()
        
        self.contentMomGuess.addSubview(guessMomView)
        guessMomView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        guessMomView.hideTitleView()
        guessMomView.enableScroll()
        guessMomView.disableHeightCollection()
        guessMomView.selectIndex = { [weak self] index in
            guard let self = self else {
                return
            }
            self.moveToProductDetail(productID: 1, hidesBottomBarWhenPushed: true)
        }
        guessMomView.loadNextPage = { [weak self] in
            guard let self = self else { return }
            //            self.viewModel.searchProduct(text: "men vi sinh")
        }
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        Action.allCases.forEach { type in
            let bt = self.bts[type.rawValue]
            bt.rx.tap
                .withUnretained(self)
                .bind { owner, _ in
                    switch type {
                    case .menus:
                        let menusView: SectionsView = .loadXib()
                        menusView.setValueSources(values: ShopStreamManage.shared.malls)
                        if let mall = owner.mallModel {
                            menusView.selectType(type: mall)
                        }
                        owner.view.addSubview(menusView)
                        menusView.snp.makeConstraints { make in
                            make.left.right.bottom.equalToSuperview()
                            make.top.equalTo(self.menusView.snp.bottom)
                        }
                        menusView.selectIndex = { [weak self] index in
                            guard let self = self else { return }
                            let item = ShopStreamManage.shared.malls[index]
                            self.mallModel = item
                            self.guessMomView.resetValues()
                            self.searchProductTrigger.onNext(())
                        }
                    case .sort:
                        let menusView: SectionsView = .loadXib()
                        menusView.setValueSources(values: ProductTradeMarkType.allCases)
                        menusView.selectType(type: owner.typeSort)
                        owner.view.addSubview(menusView)
                        menusView.snp.makeConstraints { make in
                            make.left.right.bottom.equalToSuperview()
                            make.top.equalTo(self.menusView.snp.bottom)
                        }
                        menusView.selectIndex = { [weak self] index in
                            guard let self = self else { return }
                            let item = ProductTradeMarkType.allCases[index]
                            self.typeSort = item
                            self.guessMomView.resetValues()
                            self.searchProductTrigger.onNext(())
                        }
                    case .filter:
                        let filterView: FilterView = .loadXib()
                        owner.view.addSubview(filterView)
                        filterView.snp.makeConstraints { make in
                            make.edges.equalToSuperview()
                        }
                    }
                    
                }.disposed(by: disposeBag)
        }
        
        self.searchProductTrigger
            .startWith(())
            .withUnretained(self)
            .bind { owner, _ in
                switch owner.statusOpen {
                case .search:
                    owner.searchView.hideSearchButton()
                    owner.menusView.isHidden = true
                    owner.viewModel.searchProduct(text: "men vi sinh")
                case .homeShop, .tradeMarkBig:
                    if let shopID = self.mallModel?.id {
                        owner.viewModel.productTradeMark(shopID: shopID, page: 1, typeSort: owner.typeSort)
                    }
                }
        }.disposed(by: disposeBag)
        
        self.viewModel
            .searchProduct
            .withUnretained(self)
            .bind { owner, item in
                guard let item = item else {
                    return
                }
                owner.nextPage = item.data?.nextPage ?? 1
                owner.totalPage = item.data?.total ?? 1
                if let list = item.data?.products {
                    owner.guessMomView.setValue(values: list, totalItem: item.data?.total ?? 0)
                }
            }.disposed(by: disposeBag)
    }
    private func getBanner(){
        var arrayImage = [String]()
        APIManager.getBanner(type: ConstantApp.shared.bannerType, callbackSuccess: { [weak self]  (banners) in
            guard let self = self else { return }
            for banner in banners {
                arrayImage.append(banner.link ?? "")
            }
            
            self.showBannerView(isShow: arrayImage.count > 0)
            self.bannerView.arrayImage = arrayImage
            self.bannerView.tapBanner = { index in
                guard let bannerType = BannerView.BannerTypeRedirect(rawValue: banners[index].typeRedirect ?? 0 ) else {
                    return
                }
                switch bannerType {
                case .none:
                    if let url = URL.init(string: banners[index].linkAds ?? "")  {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url , options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                case .rankPoint:
                    let rankPointVC = RankPointVC.init(nib: R.nib.rankPointVC)
                    rankPointVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(rankPointVC, animated: true)
                case .giftGame:
                    let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
                    gameVC.modalPresentationStyle = .overFullScreen
                    self.present(gameVC, animated: true, completion: nil)
                case .info:
                    guard let lichKTVC = R.storyboard.main.infoViewController() else{ return }
                    lichKTVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(lichKTVC, animated: true)
                case .momTok:
                    guard let momtokVC = R.storyboard.main.secretViewController() else {
                        return
                    }
                    momtokVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(momtokVC, animated: true)
                case .news:
                    let listNewHotVC = ListNewHotViewController.init(nib: R.nib.listNewHotViewController)
                    listNewHotVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(listNewHotVC, animated: true)
                case .listDiary:
                    let postVC = ListDiaryViewController.init(nib: R.nib.listDiaryViewController)
                    postVC.hidesBottomBarWhenPushed = true
                    postVC.gettype = "3"
                    postVC.title = "Mẹo cho bạn"
                    self.navigationController?.pushViewController(postVC, animated: true)
                case .home:
                    guard let tabbar  = self.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 1
                case .diaary:
                    guard let tabbar  = self.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 2
                }
            }
            self.bannerView.backgroundColor = .clear
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        })
    }
    
    private func showBannerView(isShow: Bool) {
        self.bannerView.isHidden = !isShow
    }
}
extension SectionDetailVC: MomSearchDelegate {
    func action(action: MomSearch.ActionBt) {
        switch action {
        case .search:
            self.moveToSearch(text: "", item: nil)
        case .close:
            self.navigationController?.popViewController()
        case .cart, .noti: break
        }
    }
}
