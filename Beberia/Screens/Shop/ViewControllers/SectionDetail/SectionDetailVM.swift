
//
//  ___HEADERFILE___
//
import Foundation
import RxCocoa
import RxSwift

class SectionDetailVM {
    
    let searchProduct: BehaviorRelay<ShopRecommenDatadMode?> = BehaviorRelay.init(value: nil)
    private let disposeBag = DisposeBag()
    init() {
        
    }
    
    func productTradeMark(shopID: Int, page: Int, typeSort: ProductTradeMarkType) {
        AlamorfireService.shared.getProductTradeMark(shopID: shopID, productTradeMarkType: typeSort, page: page).bind(to: searchProduct).disposed(by: disposeBag)
    }
    
    func searchProduct(text: String) {
        let page = searchProduct.value?.data?.nextPage ?? 1
        AlamorfireService.shared.searchShop(text: text, page: page, searchType: .relateProduct).bind(to: searchProduct).disposed(by: disposeBag)
    }
}
