//
//  TradeMarkElementCell.swift
//  Beberia
//
//  Created by haiphan on 06/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import Kingfisher

class TradeMarkElementCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var ratio: NSLayoutConstraint?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
extension TradeMarkElementCell {

    func unActiveRatio(icActive: Bool) {
        if let r = self.ratio {
            r.isActive = icActive
        }
        
    }
    
    func hideName(isHidden: Bool) {
        lbName.isHidden = isHidden
    }
    
    func setValueModel(model: TradeMarkElementModelType) {
        lbName.text = model.name
        img.kf.setImage(with: URL.init(string: model.linkImage ?? ""), placeholder: R.image.placeholder())
    }
    
    func setValueModel(model: MallShopModel) {
        lbName.text = model.shopName
        img.kf.setImage(with: URL.init(string: model.shopAvatar ?? ""), placeholder: R.image.placeholder())
    }
    
}
