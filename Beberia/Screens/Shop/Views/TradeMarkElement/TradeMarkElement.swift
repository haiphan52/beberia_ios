//
//  TradeMarkElement.swift
//  Beberia
//
//  Created by haiphan on 06/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol TradeMarkElementModelType {
    var name: String? { get }
    var linkImage: String? { get }
    var idType: Int? { get }
}

class TradeMarkElement: UIView, SetUpFunctionBase, SetupBaseCollection {
    
    enum StatusCollection {
        case tradeMark, jobHot, sectionProduct
    }
    
    struct Constant {
        static let sizeImageSize: CGSize = CGSize(width: 104, height: 58)
        static let sizeTextSize: CGSize = CGSize(width: 100, height: 140)
        static let sizeSectionProduct: CGSize = CGSize(width: 100, height: 140)
    }
    var status: StatusCollection = .tradeMark
    @IBOutlet weak var lbTradeMark: UILabel!
    @IBOutlet weak var topCollectionView: UICollectionView!
    @IBOutlet weak var heightTopCollectionView: NSLayoutConstraint!
    
    private let sources: BehaviorRelay<[TradeMarkElementModelType]> = BehaviorRelay.init(value: [])
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
}
extension TradeMarkElement {
    
    func setupUI() {
        setupCollectionView(collectionView: topCollectionView, delegate: self, name: TradeMarkElementCell.self)
    }
    
    func setupRX() {
        sources
            .bind(to: self.topCollectionView.rx.items(cellIdentifier: TradeMarkElementCell.identifier, cellType: TradeMarkElementCell.self)) { row, data, cell in
                cell.hideName(isHidden: self.status == .tradeMark)
                cell.unActiveRatio(icActive: self.status != .tradeMark)
                cell.setValueModel(model: data)
            }.disposed(by: disposeBag)
    }
    
    func setValue(values: [TradeMarkElementModelType]) {
        sources.accept(values)
    }
    
    func setTitle(title: String) {
        lbTradeMark.text = title
    }
    
    func setStatus(status: StatusCollection) {
        self.status = status
        let heights = [self.heightTopCollectionView]
        heights.forEach { h in
            switch self.status {
            case .tradeMark: h?.constant = Constant.sizeImageSize.height
            case .jobHot: h?.constant = Constant.sizeTextSize.height
            case .sectionProduct: h?.constant = Constant.sizeSectionProduct.height
            }
        }
        self.topCollectionView.reloadData()
    }
    
    
    
}
extension TradeMarkElement: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch self.status {
        case .tradeMark: return Constant.sizeImageSize
        case .jobHot: return Constant.sizeTextSize
        case .sectionProduct: return Constant.sizeSectionProduct
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}
