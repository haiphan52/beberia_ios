//
//  ContentRate.swift
//  Beberia
//
//  Created by haiphan on 10/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class ContentRate: UIView, SetUpFunctionBase {
    
    @IBOutlet weak var contentRateView: UIView!
    private let rateView: RateView = .loadXib()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
}
extension ContentRate {
    
    func setupUI() {
        contentRateView.addSubview(rateView)
        rateView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func setupRX() {
        
    }
    
}
