//
//  GuessMom.swift
//  Beberia
//
//  Created by haiphan on 07/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GuessMom: UIView, SetUpFunctionBase, SetupBaseCollection {
    
    struct Constnat {
        static let heightCell: CGFloat = 281
    }
    var selectIndex: ((Int) -> Void)?
    var loadNextPage: (() -> Void)?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var lbName: UILabel!
    private var sources: BehaviorRelay<[ShopRecommendModel]> = BehaviorRelay.init(value: [])
    private var totalItem: Int = 0
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }

}
extension GuessMom {
    
    func setupUI() {
        setupCollectionView(collectionView: collectionView, delegate: self, name: GoodDealCell.self)
        self.collectionView.prefetchDataSource = self
    }
    
    func setupRX() {
        sources
            .bind(to: self.collectionView.rx.items(cellIdentifier: GoodDealCell.identifier, cellType: GoodDealCell.self)) { row, data, cell in
                cell.setLayout()
                cell.setSizeStar(size: 15)
                cell.setValue(value: data)
//                if let quantity = data.productReviewcntt, quantity <= 0 {
//                    cell.showOutOfOrder(isShow: true)
//                } else {
//                    cell.showOutOfOrder(isShow: false)
//                }
            }.disposed(by: disposeBag)
        
        self.collectionView
            .rx
            .itemSelected
            .withUnretained(self)
            .bind { owner, idx in
                guard let item = owner.sources.value[idx.row].productID else {
                    return
                }
                
                owner.selectIndex?(item)
        }.disposed(by: self.disposeBag)
    }
    
    func disableHeightCollection() {
        if self.heightCollectionView.isActive {
            self.heightCollectionView.isActive = false
        }
    }
    
    func enableScroll() {
        self.collectionView.isScrollEnabled = true
    }
    
    func setTtile(text: String) {
        lbName.text = text
    }
    
    func setValue(values: [ShopRecommendModel], totalItem: Int) {
        self.totalItem = totalItem
        let list = sources.value + values
        sources.accept(list)
    }
    
    func resetValues() {
        sources.accept([])
        totalItem = 1
    }
    
    func hideTitleView() {
        titleView.isHidden = true
    }
    
    private func calculateSize() -> CGFloat {
        collectionView.layoutIfNeeded()
        let width = (collectionView.frame.width - 11) / 2
        return width
    }
    
}
extension GuessMom: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        guard ManageApp.shared.detectLoadPaging(indexPaths: indexPaths, value: sources.value.count, totalItem: totalItem) else {
            return
        }
        self.loadNextPage?()
        print("======= max")
    }
}
extension GuessMom: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.calculateSize(), height: Constnat.heightCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
