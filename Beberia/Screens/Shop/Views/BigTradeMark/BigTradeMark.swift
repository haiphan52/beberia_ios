//
//  BigTradeMark.swift
//  Beberia
//
//  Created by haiphan on 26/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BigTradeMark: UIView,
                    SetUpFunctionBase,
                    SetupBaseCollection {
    
    
    var actionSeeAll: (() -> Void)?
    var selectTradeMark: ((TradeMarkElementModelType?) -> Void)?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btSeeAll: UIButton!
    @IBOutlet weak var contentHeader: UIView!
    private let sources: BehaviorRelay<[[TradeMarkElementModelType]]> = BehaviorRelay.init(value: [])
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupRX()
    }

}
extension BigTradeMark {
    
    func setupUI() {
        setupCollectionView(collectionView: collectionView, delegate: self, name: BigTradeMarkCell.self)
        
    }
    
    func setupRX() {
        sources
            .bind(to: self.collectionView.rx.items(cellIdentifier: BigTradeMarkCell.identifier, cellType: BigTradeMarkCell.self)) { [weak self] row, data, cell in
                guard let self = self else { return }
                cell.setValues(values: data)
                cell.selectTyped = { [weak self] type in
                    guard let self = self else { return }
                    let mall = (type == .first) ? data.first : data.last
                    self.selectTradeMark?(mall)
                }
            }.disposed(by: disposeBag)
        
        self.collectionView.rx
            .itemSelected
            .withUnretained(self)
            .bind { owner, idx in
        }.disposed(by: disposeBag)
        
        btSeeAll.rx.tap
            .withUnretained(self)
            .bind { owner, _ in
                owner.actionSeeAll?()
            }.disposed(by: disposeBag)
    }
    
    func setValue(values: [TradeMarkElementModelType]) {
        let spilt = values.chunked(into: 2)
        sources.accept(spilt)
    }
    
    func hideHeaderView() {
        contentHeader.isHidden = true
    }
    
}
extension BigTradeMark: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       return CGSize(width: 109, height: 136)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}
