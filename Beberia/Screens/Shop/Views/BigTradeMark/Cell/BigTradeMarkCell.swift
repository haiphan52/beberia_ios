//
//  BigTradeMarkCell.swift
//  Beberia
//
//  Created by haiphan on 26/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class BigTradeMarkCell: UICollectionViewCell {

    var selectTyped: ((BigTradeMarkView.StatusSources) -> Void)?
    private let bigTradeView: BigTradeMarkView = .loadXib()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addSubview(bigTradeView)
        bigTradeView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        bigTradeView.selectTyped = { [weak self] selectType in
            guard let self = self else { return }
            self.selectTyped?(selectType)
        }
    }

}
extension BigTradeMarkCell {
    
    func setValues(values: [TradeMarkElementModelType]) {
        bigTradeView.setValues(values: values)
    }
    
}
