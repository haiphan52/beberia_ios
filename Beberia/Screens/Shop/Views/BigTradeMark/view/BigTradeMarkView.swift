//
//  BigTradeMarkView.swift
//  Beberia
//
//  Created by haiphan on 26/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift

class BigTradeMarkView: UIView, SetUpFunctionBase {
    
    enum StatusSources {
        case first, last
    }
    
    var selectTyped: ((StatusSources) -> Void)?
    
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var btFirst: UIButton!
    @IBOutlet weak var btLast: UIButton!
    private var sources: [TradeMarkElementModelType] = []
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupRX()
    }
    
}
extension BigTradeMarkView {
    
    func setupUI() {
        let first = btFirst.rx.tap.map { StatusSources.first }
        let last = btLast.rx.tap.map { StatusSources.last }
        
        Observable.merge(first, last)
            .withUnretained(self)
            .bind { owner, type in
                owner.selectTyped?(type)
            }.disposed(by: disposeBag)
        
    }
    
    func setupRX() {
        
    }
    
    func setValues(values: [TradeMarkElementModelType]) {
        self.sources = values
        if let f = values.first {
            firstImageView.kf.setImage(with: URL.init(string: f.linkImage ?? ""), placeholder: R.image.placeholder())
        }
        
        if let last = values.last {
            secondImageView.kf.setImage(with: URL.init(string: last.linkImage ?? ""), placeholder: R.image.placeholder())
        }
    }
    
}
