//
//  GoodDealCell.swift
//  Beberia
//
//  Created by haiphan on 07/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import Cosmos

class GoodDealCell: UICollectionViewCell, SetUpFunctionBase {

    @IBOutlet weak var containverView: UIView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var bottomCell: NSLayoutConstraint!
    @IBOutlet weak var topCell: NSLayoutConstraint!
    private let productCell: ProductCell = .loadXib()
    private var status: GoodDealView.Status = .other
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
        setupRX()
    }

}
extension GoodDealCell {
    
    func setupUI() {
        containverView.addSubview(productCell)
    }
    
    func setValue(value: ShopRecommendModel) {
        productCell.setValues(value: value)
    }
    
    func setLayout() {
        switch self.status {
        case .other:
            productCell.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        case .productDetail:
            productCell.snp.makeConstraints { make in
                make.left.right.equalToSuperview().inset(4)
                make.top.equalToSuperview().inset(6)
                make.bottom.equalToSuperview().inset(9)
            }
            self.backgroundColor = .clear
            self.cellView.backgroundColor = .clear
            let h = [self.topCell, self.bottomCell]
            h.forEach { t in
                t?.constant = 10
            }
            cellView.layer.applySketchShadow(type: .normal)
            
        }
    }
    
    func setStatus(status: GoodDealView.Status) {
        self.status = status
    }
    
    
    func setupRX() {
        
    }
    
    func showOutOfOrder(isShow: Bool) {
        (isShow) ? productCell.showOutOfOrder() : productCell.hideOutOfOrder()
    }
    
    func setSizeStar(size: CGFloat) {
        productCell.setSizeStar(size: size)
    }
    
}
