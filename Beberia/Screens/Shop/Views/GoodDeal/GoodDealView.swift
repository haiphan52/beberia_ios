//
//  GoodDealView.swift
//  Beberia
//
//  Created by haiphan on 07/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GoodDealView: UIView, SetUpFunctionBase, SetupBaseCollection {
    
    struct Constant {
        static let sizeCell: CGSize = CGSize(width: 140, height: 258)
        static let sizeProductDetailCell: CGSize = CGSize(width: 148, height: 273)
    }
    
    enum Status {
        case productDetail, other
    }
    
    var selectProdut: ((ShopRecommendModel) -> Void)?
    var loadNextPage: ((Int) -> Void)?
    var actionSeeMore: (() -> Void)?
    
    private var status: Status = .other
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSeeMore: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var leftCollectionView: NSLayoutConstraint!
    @IBOutlet weak var btSeeMore: UIButton!
    private var sources: BehaviorRelay<[ShopRecommendModel]> = BehaviorRelay.init(value: [])
    private var totalItem: Int = 0
    private var nextPage: Int = 1
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
    
}
extension GoodDealView {
    
    func setupUI() {
        containerView.layer.cornerRadius = 8
        setupCollectionView(collectionView: collectionView, delegate: self, name: GoodDealCell.self)
        collectionView.prefetchDataSource = self
    }
    
    func setupRX() {
        
        btSeeMore.rx.tap
            .withUnretained(self)
            .bind { owner, _ in
                owner.actionSeeMore?()
            }.disposed(by: disposeBag)
        
        sources
            .bind(to: self.collectionView.rx.items(cellIdentifier: GoodDealCell.identifier, cellType: GoodDealCell.self)) { row, data, cell in
                cell.setStatus(status: self.status)
                cell.setLayout()
                cell.setSizeStar(size: 15)
                cell.setValue(value: data)
//                if let quantity = data.sale?.productSaleQuantityRemain, quantity <= 0 {
//                    cell.showOutOfOrder(isShow: true)
//                } else {
//                    cell.showOutOfOrder(isShow: false)
//                }
                
            }.disposed(by: disposeBag)
        
        self.collectionView
            .rx
            .itemSelected
            .withUnretained(self)
            .bind { owner, idx in
                let item = owner.sources.value[idx.row]
                owner.selectProdut?(item)
            }.disposed(by: disposeBag)
    }
    
    func setValues(values: [ShopRecommendModel]) {
        let list = sources.value + values
        sources.accept(list)
    }
    
    func setTextColorSeeAll(color: UIColor) {
        lbSeeMore.textColor = color
    }
    
    func setTotalItem(totalItem: Int) {
        self.totalItem = totalItem
    }
    
    func setTitle(text: String) {
        lbTitle.text = text
    }
    
    func setStatus(status: Status) {
        self.status = status
        self.collectionView.reloadData()
    }
    
    func hideSeeMore() {
        lbSeeMore.isHidden = true
    }
    
    func setColorTitle(color: UIColor) {
        lbTitle.textColor = color
    }
    
    func setLeftContraint(value: CGFloat) {
        leftCollectionView.constant = value
    }
    
    func setBgColor(color: UIColor) {
        containerView.backgroundColor = color
    }
    
}
extension GoodDealView: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        guard ManageApp.shared.detectLoadPaging(indexPaths: indexPaths, value: sources.value.count, totalItem: totalItem) else {
            return
        }
        nextPage += 1
        loadNextPage?(nextPage)
    }
}
extension GoodDealView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.status == .productDetail {
            return Constant.sizeProductDetailCell
        }
        return Constant.sizeCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}
