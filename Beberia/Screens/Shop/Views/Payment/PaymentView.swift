//
//  PaymentView.swift
//  Beberia
//
//  Created by haiphan on 11/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol PaymentViewDelegate: AnyObject {
    func selectAction(action: PaymentView.Action)
}

class PaymentView: UIView {
    
    enum Action: Int, CaseIterable {
        case close, minus, plus, addToCart, buyNow
    }
    
    weak var delegate: PaymentViewDelegate?
    
    @IBOutlet weak var contentCartVoew: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbCode: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var lbCount: UILabel!
    
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
    
}
extension PaymentView {
    
    func setupUI() {
        containerView.clipsToBounds = true
        containerView.layer.setCornerRadius(corner: .verticalTop, radius: 16)
    }
    
    func setupRX() {
        Action.allCases.forEach { type in
            let bt = self.bts[type.rawValue]
            bt.rx.tap
                .withUnretained(self)
                .bind { owner, _ in
                    var value = Int(owner.lbCount.text ?? "0") ?? 0
                    switch type {
                    case .minus:
                        if value <= 0 {
                            return
                        }
                        value -= 1
                    case .plus:
                        value += 1
                    case .close:
                        owner.removeFromSuperview()
                    case .addToCart, .buyNow:
                        owner.delegate?.selectAction(action: type)
                    }
                    owner.lbCount.text = "\(value)"
                }.disposed(by: disposeBag)
        }
    }
    
    func setValue(productPayment: PaymentShopType) {
        self.lbName.text = productPayment.productName
        self.lbCode.text = productPayment.productCode
        img.kf.setImage(with: URL.init(string: productPayment.productThumnail ?? ""), placeholder: R.image.placeholder())
    }
    
    func hideCartView() {
        contentCartVoew.isHidden = true
    }
    
    func showView() {
        guard let topvc = ManageApp.shared.topViewController() else {
            return
        }
        topvc.view.addSubview(self)
        self.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
}
