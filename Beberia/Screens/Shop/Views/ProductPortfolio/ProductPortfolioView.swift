//
//  ProductPortfolioView.swift
//  Beberia
//
//  Created by haiphan on 11/10/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ProductPortfolioView: UIView,
                            SetUpFunctionBase,
                            SetupBaseCollection {
    
    struct Constant {
        static let sizeCell: CGSize = CGSize(width: 80, height: 84)
        static let spacingCell: CGFloat = 8
        static let spacingSection: CGFloat = 16
    }
    
    
    var actionSeeAll: (() -> Void)?
    
    @IBOutlet weak var btSeeAll: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    private let sources: BehaviorRelay<[TradeMarkElementModelType]> = BehaviorRelay.init(value: [])
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupRX()
    }
    
}
extension ProductPortfolioView {
    
    func setupUI() {
        setupCollectionView(collectionView: collectionView, delegate: self, name: ProductPortfolioCell.self)
    }
    
    func setupRX() {
        
        sources
            .withUnretained(self)
            .bind { owner, list in
                var count = list.count / 4
                if list.count % 4 != 0 {
                    count += 1
                }
                owner.heightCollectionView.constant = (Constant.sizeCell.height * CGFloat(count)) + (CGFloat((count - 1)) * Constant.spacingSection)
        }.disposed(by: disposeBag)
        
        sources
            .bind(to: self.collectionView.rx.items(cellIdentifier: ProductPortfolioCell.identifier, cellType: ProductPortfolioCell.self)) { row, data, cell in
                cell.setValueModel(model: data)
            }.disposed(by: disposeBag)
        
        btSeeAll.rx.tap
            .withUnretained(self)
            .bind { owner, _ in
                owner.actionSeeAll?()
            }.disposed(by: disposeBag)
    }
    
    func setValue(values: [TradeMarkElementModelType]) {
        sources.accept(values)
    }
    
    private func getWidthCell() -> CGFloat {
        let width = (self.collectionView.bounds.width - (Constant.spacingCell * 3)) / 4
        return width
    }
    
}
extension ProductPortfolioView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.getWidthCell(), height: Constant.sizeCell.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constant.spacingCell
    }
    
}
