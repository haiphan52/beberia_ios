//
//  ProductPortfolioCell.swift
//  Beberia
//
//  Created by haiphan on 11/10/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class ProductPortfolioCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setValueModel(model: TradeMarkElementModelType) {
        lbName.text = model.name
        img.kf.setImage(with: URL.init(string: model.linkImage ?? ""), placeholder: R.image.placeholder())
    }

}
