//
//  TrackingOrder.swift
//  Beberia
//
//  Created by haiphan on 14/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift

class TrackingOrder: UIView, SetUpFunctionBase {
    
    enum Action: Int, CaseIterable {
        case waiting, preparing, shipping, delivered
    }
    
    @IBOutlet var lbNames: [UILabel]!
    @IBOutlet var imgs: [UIImageView]!
    @IBOutlet var lbDates: [UILabel]!
    @VariableReplay private var status: Action = .waiting
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
}
extension TrackingOrder {
    
    func setupUI() {
    }
    
    func setupRX() {
        self.$status
            .withUnretained(self)
            .bind { owner, type in
                owner.lbDates.forEach { lb in
                    lb.isHidden = true
                }
                owner.lbDates[type.rawValue].isHidden = false
                owner.imgs.forEach { img in
                    img.image = R.image.img_checkStatus()
                }
                owner.imgs[type.rawValue].image = R.image.img_checkedStatus()
                owner.lbNames.forEach { lb in
                    lb.textColor = R.color.cececE()
                }
                owner.lbNames[type.rawValue].textColor = R.color.fE()
        }.disposed(by: disposeBag)
    }
}
