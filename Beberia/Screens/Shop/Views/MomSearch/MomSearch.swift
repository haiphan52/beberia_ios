//
//  MomSearch.swift
//  Beberia
//
//  Created by haiphan on 06/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol MomSearchDelegate: AnyObject {
    func action(action: MomSearch.ActionBt)
}

class MomSearch: UIView, SetUpFunctionBase {
    
    enum ActionBt: Int, CaseIterable {
        case close, noti, cart, search
    }
    
    var searchText: ((String) -> Void)?
    
    weak var delegate: MomSearchDelegate?
    private let disposeBag = DisposeBag()
    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var tfSearch: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
}
extension MomSearch {
    
    func setupUI() {
        ActionBt.allCases.forEach { type in
            let bt = self.bts[type.rawValue]
            bt.rx.tap
                .withUnretained(self)
                .bind { owner, _ in
                    owner.delegate?.action(action: type)
                }.disposed(by: disposeBag)
        }
        
        tfSearch.rx.text
            .orEmpty
            .debounce(.milliseconds(300), scheduler: MainScheduler.asyncInstance)
            .withUnretained(self)
            .bind { owner, text in
                owner.searchText?(text)
            }.disposed(by: disposeBag)
    }
    
    func setText(text: String) {
        tfSearch.text = text
    }
    
    func getText() -> String? {
        return self.tfSearch.text
    }
    
    func isHideSearch() -> Bool {
        return self.tfSearch.text?.isEmpty ?? true
    }
    
    func setImageBackButton(img: UIImage?) {
        bts[ActionBt.close.rawValue].setImage(img, for: .normal)
    }
    
    func hideCartButton() {
        bts[ActionBt.cart.rawValue].isHidden = true
    }
    
    func hideNotiButton() {
        bts[ActionBt.noti.rawValue].isHidden = true
    }
    
    func hideSearchButton() {
        bts[ActionBt.search.rawValue].isHidden = true
    }
    
    func setupRX() {
        
    }
    
    func hideBtClose() {
        bts[ActionBt.close.rawValue].isHidden = true
    }
    
}
