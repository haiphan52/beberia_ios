//
//  FilterView.swift
//  Beberia
//
//  Created by haiphan on 12/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

protocol FilterViewDelegate: AnyObject {
    func selectAction(action: FilterView.Action)
}

class FilterView: UIView, SetUpFunctionBase {
    
    struct Constant {
        static let heightHeader: CGFloat = 50
        static let heightCell: CGFloat = 44
    }
    
    enum Action: Int, CaseIterable {
        case reset, apply
    }
    
    weak var delegate: FilterViewDelegate?
    
    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var collectionView: UICollectionView!
    private typealias Section = SectionModel<String, Double>
    private typealias DataSource = RxCollectionViewSectionedReloadDataSource<Section>
    private lazy var dataSource = makeDataSource()
    
    let items = Observable.just([
        SectionModel(model: "First section", items: [
            1.0,
            2.0,
            3.0
        ]),
        SectionModel(model: "Second section", items: [
            1.0,
            2.0,
            3.0
        ]),
        SectionModel(model: "Third section", items: [
            1.0,
            2.0,
            3.0
        ])
    ])
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.setupUI()
            self.setupRX()
        }
    }
    
}
extension FilterView {
    
    func setupUI() {
        collectionView.register(cellType: FilterCell.self)
        self.collectionView?.register(FilterHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: FilterHeaderCell.identifier)
        setupLayout()
        bindViewModelToCollectionView()
    }
    
    func setupRX() {
        Action.allCases.forEach { type in
            let bt = self.bts[type.rawValue]
            bt.rx.tap
                .withUnretained(self)
                .bind { owner, _ in
                    owner.removeFromSuperview()
                    owner.delegate?.selectAction(action: type)
                }.disposed(by: disposeBag)
        }
    }
    private func setupLayout(){
        self.layoutIfNeeded()
        let layout = UICollectionViewFlowLayout()
        layout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: Constant.heightHeader)
        let width = (self.collectionView.frame.size.width - 32 - 16) / 2
        layout.itemSize = CGSize(width: width, height: Constant.heightCell)
        collectionView.setCollectionViewLayout(layout, animated: false)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    private func bindViewModelToCollectionView(){
        dataSource.configureSupplementaryView = {(dataSource, collectionView, kind, indexPath) -> UICollectionReusableView in
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: FilterHeaderCell.identifier, for: indexPath) as! FilterHeaderCell
            header.backgroundColor = .red
            return header
        }
        items.bind(to: collectionView.rx.items(dataSource: dataSource)).disposed(by: disposeBag)
    }
    
    
    private func makeDataSource() -> DataSource {
        return DataSource { _, collectionView, indexPath, model in
            let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: FilterCell.self)
            cell.backgroundColor = .blue
            return cell
        }
    }
}
