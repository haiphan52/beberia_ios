//
//  IntroduceProduct.swift
//  Beberia
//
//  Created by haiphan on 10/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol IntroduceProductDelegate: AnyObject {
    func selectAction(action: IntroduceProduct.Action)
}

class IntroduceProduct: UIView, SetUpFunctionBase {
    
    enum Action: Int, CaseIterable {
        case writeComment, seeComment
    }
    
    enum ActionHeader: Int, CaseIterable {
        case introduce, rate
    }
    
    weak var delegate: IntroduceProductDelegate?
    @IBOutlet weak var contentRateView: UIView!
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet var btsIntroduce: [UIButton]!
    @IBOutlet var viewsIntroduce: [UIView]!
    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var imgIntroduce: UIImageView!
    @IBOutlet weak var contentRateIntroduceView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var contentLbDescription: UIView!
    private let rateView: RateView = .loadXib()
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
    
}
extension IntroduceProduct {
    
    func setupUI() {
        contentRateView.addSubview(rateView)
        rateView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        for i in 0...2 {
            let v: ContentRate = .loadXib()
            contentStackView.addArrangedSubview(v)
        }
        
        contentRateIntroduceView.isHidden = true
    }
    
    func setupRX() {
        Action.allCases.forEach { type in
            let bt = self.bts[type.rawValue]
            bt.rx.tap
                .withUnretained(self)
                .bind { owner, _ in
                    owner.delegate?.selectAction(action: type)
                }.disposed(by: disposeBag)
        }
        
        ActionHeader.allCases.forEach { type in
            let bt = self.btsIntroduce[type.rawValue]
            bt.rx.tap
                .withUnretained(self)
                .bind { owner, _ in
                    owner.contentRateIntroduceView.isHidden = type == .introduce
                    owner.imgIntroduce.isHidden = type == .rate
                    owner.contentLbDescription.isHidden = type == .rate
                    owner.btsIntroduce[ActionHeader.introduce.rawValue].setTitleColor( (type == .introduce) ? R.color.fd799D() : R.color.neutral3() , for: .normal)
                    owner.btsIntroduce[ActionHeader.rate.rawValue].setTitleColor( (type == .rate) ? R.color.fd799D() : R.color.neutral3() , for: .normal)
                    owner.moveLineView(baseView: owner.viewsIntroduce[type.rawValue])
                }.disposed(by: disposeBag)
        }
        
        
    }
    
    func setDescription(desc: String) {
        lbDescription.text = desc
    }
    
    private func moveLineView(baseView: UIView) {
        var f = self.lineView.frame
        UIView.animate(withDuration: 0.3) {
            f.origin.x = (baseView.frame.origin.x == 0) ? 0 : baseView.frame.origin.x
            self.lineView.frame = f
        }
    }
    
}
