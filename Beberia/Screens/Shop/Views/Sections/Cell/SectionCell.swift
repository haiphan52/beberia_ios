//
//  SectionCell.swift
//  Beberia
//
//  Created by haiphan on 11/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class SectionCell: UITableViewCell {

    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension SectionCell {
    
    func setValue(mallModel: TradeMarkElementModelType) {
        lbTitle.text = mallModel.name
    }
    
    func typeDeSelect() {
        lbTitle.textColor = R.color.d4D4D()
        imgSelect.isHidden = true
    }
    
    func typeSelect() {
        lbTitle.textColor = R.color.fd799D()
        imgSelect.isHidden = false
    }
    
    func setRadius(radius: CGFloat) {
        self.layer.setCornerRadius(corner: .verticalBot, radius: radius)
    }
    
}
