//
//  SectionsView.swift
//  Beberia
//
//  Created by haiphan on 11/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SectionsView: UIView, SetUpFunctionBase, SetupTableView {
    
    
    var selectIndex: ((Int) -> Void)?
    
    @IBOutlet weak var tableView: UITableView!
    private var sources: BehaviorRelay<[TradeMarkElementModelType]> = BehaviorRelay.init(value: [])
    private var selectType: TradeMarkElementModelType?
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
}
extension SectionsView {
    
    func setupUI() {
        setupTableView(tableView: tableView, delegate: self, name: SectionCell.self)
        tableView.clipsToBounds = true
        tableView.layer.setCornerRadius(corner: .verticalBot, radius: 12)
    }
    
    func setupRX() {
        sources
            .bind(to: tableView.rx.items(cellIdentifier: SectionCell.identifier, cellType: SectionCell.self)) { [weak self] (row, element, cell) in
                guard let self = self else { return }
                cell.setValue(mallModel: element)
                cell.setRadius(radius: ( row == 1 ) ? 12 : 0)
                if let type = self.selectType, element.idType == type.idType {
                    cell.typeSelect()
                } else {
                    cell.typeDeSelect()
                }
            }.disposed(by: disposeBag)

        self.tableView
            .rx
            .itemSelected
            .withUnretained(self)
            .bind { owner, idx in
                owner.removeFromSuperview()
                owner.selectIndex?(idx.row)
        }.disposed(by: disposeBag)
    }
    
    func selectType(type: TradeMarkElementModelType) {
        self.selectType = type
    }
    
    func setValueSources(values: [TradeMarkElementModelType]) {
        sources.accept(values)
    }

}
extension SectionsView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
