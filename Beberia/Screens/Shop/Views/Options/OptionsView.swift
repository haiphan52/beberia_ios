//
//  OptionsView.swift
//  Beberia
//
//  Created by haiphan on 08/10/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class OptionsView: UIView, SetUpFunctionBase, SetupBaseCollection {
    
    struct Constant {
        static let sizeCell: CGSize = CGSize(width: 64, height: 64)
    }
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupRX()
    }
}
extension OptionsView {
    
    func setupUI() {
        makeTitle(count: 2)
        setupCollectionView(collectionView: collectionView, delegate: self, name: TradeMarkElementCell.self)
    }
    
    func setupRX() {
        Observable.just([1,2])
            .bind(to: self.collectionView.rx.items(cellIdentifier: TradeMarkElementCell.identifier, cellType: TradeMarkElementCell.self)) { row, data, cell in
                cell.hideName(isHidden: true)
            }.disposed(by: disposeBag)
    }
    
    private func makeTitle(count: Int) {
        let att: NSMutableAttributedString = NSMutableAttributedString(string: "Chọn loại hàng ",
                                                                       attributes: [NSAttributedString.Key.font: UIFont.notoSansFont(weight: .bold, size: 14),
                                                                                    NSAttributedString.Key.foregroundColor: R.color.d4D4D() ?? UIColor.black])
        let att1 = NSMutableAttributedString(string:" (\(count) màu sắc) ", attributes: [NSAttributedString.Key.font: UIFont.notoSansFont(weight: .regular, size: 14),
                                                                                       NSAttributedString.Key.foregroundColor: R.color.c8686() ?? UIColor.black ])
        att.append(att1)
        lbTitle.attributedText = att
        
    }
    
}
extension OptionsView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return Constant.sizeCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}
