//
//  ProductCell.swift
//  Beberia
//
//  Created by Hai Phan Thanh on 09/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Cosmos

protocol ProductCellDelegate: AnyObject {
    func selectAction(action: ProductCell.Action)
}

class ProductCell: UIView, SetUpFunctionBase {
    
    enum Action: Int, CaseIterable {
        case back, share, cart
    }
    
    weak var delegate: ProductCellDelegate?
    
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var heightLessThan: NSLayoutConstraint!
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var onlyRateView: UIView!
    @IBOutlet weak var stackViewContent: UIStackView!
    @IBOutlet weak var stackViewImage: UIStackView!
    @IBOutlet weak var stackViewPrice: UIStackView!
    @IBOutlet weak var rateSaleView: UIView!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var lbCountRate: UILabel!
    @IBOutlet weak var lbCountSale: UILabel!
    @IBOutlet weak var discountPriceView: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbMaxPrice: UILabel!
    @IBOutlet weak var lbMinPrice: UILabel!
    @IBOutlet weak var actionHeaderView: UIView!
    @IBOutlet weak var lbMaxPrices2: UILabel!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var lbPercent: UILabel!
    @IBOutlet weak var percentView: UIView!
    @IBOutlet weak var imgOutOfOrder: UIImageView!
    @IBOutlet weak var outOfOrderView: UIView!
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
}
extension ProductCell {
    
    func setupUI() {
        cosmosView.settings.starMargin = 0
        rateSaleView.isHidden = true
        discountPriceView.isHidden = true
        actionHeaderView.isHidden = true
        
        percentView.isHidden = true
        discountView.isHidden = true
    }
    
    func setupRX() {
        Action.allCases.forEach { type in
            let bt = self.bts[type.rawValue]
            bt.rx.tap
                .withUnretained(self)
                .bind { owner, _ in
                    owner.delegate?.selectAction(action: type)
                }.disposed(by: disposeBag)
        }
    }
    
    func setValues(value: ShopRecommendModel) {
        img.kf.setImage(with: URL.init(string: value.productThumnail ?? ""), placeholder: R.image.placeholder())
        lbName.text = value.productName
        lbMaxPrice.text = value.productMaxprice?.currency
        lbMinPrice.text = "\(value.productMinprice?.currency ?? "") - \(value.productMaxprice?.currency ?? "")"
        if value.productMinprice == value.productMaxprice {
            lbMinPrice.text = value.productMinprice?.currency
        }
        lbMaxPrices2.text = value.productMaxprice?.currency
        
        if let percent = value.sale?.productSaleQuantityRemain,
           let minPrice = value.sale?.productSaleprice,
           percent > 0 {
            percentView.isHidden = false
            discountView.isHidden = false
            self.lbPercent.text = "-\(Int(percent))%"
            self.lbMinPrice.text = minPrice.currency
            self.lbMaxPrice.text = "\(value.productMinprice?.currency ?? "") - \(value.productMaxprice?.currency ?? "")"
            if value.productMinprice == value.productMaxprice {
                lbMaxPrice.text = value.productMinprice?.currency
            }
        } else {
            percentView.isHidden = true
            discountView.isHidden = true
        }
    }
    
    func showOutOfOrder() {
        outOfOrderView.isHidden = false
        imgOutOfOrder.isHidden = false
    }
    
    func hideOutOfOrder() {
        outOfOrderView.isHidden = true
        imgOutOfOrder.isHidden = true
    }
    
    func showHeaderAction() {
        actionHeaderView.isHidden = false
    }
    
    func enableDiscountPriceView() {
        discountPriceView.isHidden = false
    }
    
    func enableRateView() {
        rateSaleView.isHidden = false
    }
    
    func spacingValueStackViewPrice(spacing: CGFloat) {
        stackViewPrice.spacing = spacing
    }
    
    func spacingValueStackViewImage(spacing: CGFloat) {
        stackViewImage.spacing = spacing
    }
    
    func spacingValueStackViewContent(spacing: CGFloat) {
        stackViewContent.spacing = spacing
    }
    
    func hideOnlyRateView() {
        onlyRateView.isHidden = true
    }
    
    func hideDiscountView() {
        discountView.isHidden = true
    }
    
    func setHeightImage(value: CGFloat) {
        heightImage.isActive = true
        heightImage.priority = .defaultHigh
        heightImage.constant = value
    }
    
    func enableHeightLessThan(isActive: Bool) {
        heightLessThan.isActive = isActive
    }
    
    func setSizeStar(size: CGFloat) {
        cosmosView.settings.starSize = size
    }
    
}
extension Array {
    func split() -> [[Element]] {
        let ct = self.count
        let half = ct / 2
        let leftSplit = self[0 ..< half]
        let rightSplit = self[half ..< ct]
        return [Array(leftSplit), Array(rightSplit)]
    }
}
extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
