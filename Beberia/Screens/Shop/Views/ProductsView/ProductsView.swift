//
//  ProductsView.swift
//  Beberia
//
//  Created by haiphan on 14/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductsView: UIView, SetUpFunctionBase {
    
    @IBOutlet weak var stackView: UIStackView!
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
}
extension ProductsView {
    
    func setupUI() {
    }
    
    func setupRX() {
        
        for i in 0...2 {
            let v: PaymentDetailView = .loadXib()
            
            v.snp.makeConstraints { make in
                make.height.equalTo(88)
            }
            
            stackView.addArrangedSubview(v)
            
        }
    }
}
