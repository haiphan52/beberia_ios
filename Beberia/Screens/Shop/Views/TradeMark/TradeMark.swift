//
//  TradeMark.swift
//  Beberia
//
//  Created by haiphan on 06/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol TradeMarkDelegate: AnyObject {
    func selecteđInex(index: Int)
}

class TradeMark: UIView, SetUpFunctionBase, SetupBaseCollection {
    
    enum ElementCells: Int, CaseIterable {
        case tradeMark, jobHot, dealGood, menus
        
        var img: UIImage? {
            switch self {
            case .tradeMark: return R.image.ic_shop_header_thuong_hieu_chinh_hang()
            case .jobHot: return R.image.ic_shop_header_nganh_hang_hot()
            case .dealGood: return R.image.ic_shop_header_deal_tot()
            case .menus: return R.image.ic_shop_header_menu()
            }
        }
        
        var text: String {
            switch self {
            case .tradeMark: return "Thương hiệu chính hãng"
            case .jobHot: return "Ngành hàng HOT"
            case .dealGood: return "Deal tốt cho mẹ"
            case .menus: return "Danh mục sản phẩm"
            }
        }
    }
    
    weak var delegate: TradeMarkDelegate?
    @IBOutlet weak var collectionView: UICollectionView!
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
    
}
extension TradeMark {
    
    func setupUI() {
        setupCollectionView(collectionView: collectionView, delegate: self, name: TradeMarkCell.self)
    }
    
    func setupRX() {
        Observable.just(ElementCells.allCases)
            .bind(to: self.collectionView.rx.items(cellIdentifier: TradeMarkCell.identifier, cellType: TradeMarkCell.self)) { row, data, cell in
                cell.setValue(value: data)
            }.disposed(by: disposeBag)
        
        self.collectionView
            .rx
            .itemSelected
            .withUnretained(self)
            .bind { owner, idx in
                owner.delegate?.selecteđInex(index: idx.row)
            }.disposed(by: disposeBag)
    }
    
    private func calculateSize() -> CGFloat {
        collectionView.layoutIfNeeded()
        let spacingBetweenCell: CGFloat = 16
        let width = (collectionView.frame.width - (spacingBetweenCell * 3)) / 4
        return width
    }
    
    
}
extension TradeMark: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.calculateSize(), height: 64)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}
