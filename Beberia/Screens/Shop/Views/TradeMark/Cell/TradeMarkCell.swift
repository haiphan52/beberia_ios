//
//  TradeMarkCell.swift
//  Beberia
//
//  Created by haiphan on 06/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class TradeMarkCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var text: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
extension TradeMarkCell {
    
    func setValue(value: TradeMark.ElementCells) {
        img.image = value.img
        text.text = value.text
    }
    
}
