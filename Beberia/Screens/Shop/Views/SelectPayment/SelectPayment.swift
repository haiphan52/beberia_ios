//
//  SelectPayment.swift
//  Beberia
//
//  Created by haiphan on 11/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

protocol SelectPaymentDelegate: AnyObject {
    func selectAction(action: SelectPayment.Action)
}

class SelectPayment: UIView {
    
    enum Action: Int, CaseIterable {
        case heart, buy, addToCart
    }
    weak var delegate: SelectPaymentDelegate?
    @IBOutlet var bts: [UIButton]!
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
    
}
extension SelectPayment {
    
    func setupUI() {
    }
    
    func setupRX() {
        Action.allCases.forEach { type in
            let bt = self.bts[type.rawValue]
            bt.rx.tap
                .withUnretained(self)
                .bind { owner, _ in
                    owner.delegate?.selectAction(action: type)
                }.disposed(by: disposeBag)
        }
        
    }
    
}
