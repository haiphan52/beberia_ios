//
//  PaymentDetailView.swift
//  Beberia
//
//  Created by haiphan on 14/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PaymentDetailView: UIView {
    
    @IBOutlet weak var lbPrice: UILabel!
    
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
}
extension PaymentDetailView {
    
    func setupUI() {
    }
    
    func setupRX() {
        makePrice(count: 1, price: 5000)
    }
    
    private func makePrice(count: Int, price: Double) {
        let att: NSMutableAttributedString = NSMutableAttributedString(string: "\(count)",
                                                                       attributes: [NSAttributedString.Key.font: UIFont.notoSansFont(weight: .medium, size: 14),
                                                                                    NSAttributedString.Key.foregroundColor: R.color.f3C() ?? .black])
        let att1: NSAttributedString = NSAttributedString(string: "  \(price.currency)",
                                                          attributes: [NSAttributedString.Key.font: UIFont.notoSansFont(weight: .medium, size: 14),
                                                                       NSAttributedString.Key.foregroundColor: R.color.fE() ?? .black])
        att.append(att1)
        lbPrice.attributedText = att
    }
}
