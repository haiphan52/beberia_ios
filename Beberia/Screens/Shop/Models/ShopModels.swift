//
//  ShopModels.swift
//  Beberia
//
//  Created by haiphan on 16/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct ShopHomeModel: Codable {
    let data: ShopHomeListModel
}

// MARK: - DataClass
struct ShopHomeListModel: Codable {
    let mall: [MallShopModel]?
    let categoryHot, category: [CategoryShopModel]?

    enum CodingKeys: String, CodingKey {
        case mall
        case categoryHot = "category_hot"
        case category
    }
}

// MARK: - Category
struct CategoryShopModel: Codable, TradeMarkElementModelType {
    var idType: Int? {
        return self.id
    }
    
    var name: String? {
        return self.categoryName
    }
    
    var linkImage: String? {
        return self.categoryThumbnail
    }
    let categoryCode, categoryName: String?
    let categoryThumbnail: String?
    let sort, id: Int?
    let categoryID: Int?
    let categoryHide, categoryDepth, categorySortnum: Int?
    
    enum CodingKeys: String, CodingKey {
        case categoryCode = "category_code"
        case categoryName = "category_name"
        case categoryThumbnail = "category_thumbnail"
        case sort, id
        case categoryID = "category_id"
        case categoryHide = "category_hide"
        case categoryDepth = "category_depth"
        case categorySortnum = "category_sortnum"
    }
}

// MARK: - Mall
struct MallShopModel: Codable, TradeMarkElementModelType {
    
    var idType: Int? {
        return self.id
    }
    
    var name: String? {
        return self.shopName
    }
    
    var linkImage: String? {
        return self.shopAvatar
    }
    
    let id: Int?
    let shopName: String?
    let shopAvatar: String?

    enum CodingKeys: String, CodingKey {
        case id
        case shopName = "shop_name"
        case shopAvatar = "shop_avatar"
    }
}

struct ShopRecommenDatadMode: Codable {
    let data: ShopRecommensdModel?
}

// MARK: - DataClass
struct ShopRecommensdModel: Codable {
    let products: [ShopRecommendModel]?
    let total, nextPage: Int?
    let tokenPage: String?

    enum CodingKeys: String, CodingKey {
        case products, total
        case nextPage = "next_page"
        case tokenPage = "token_page"
    }
}

struct DetailProductDateModel: Codable {
    let data: ShopRecommendModel?
}

protocol PaymentShopType {
    var productID: Int? { get }
    var productCode: String? { get }
    var productName: String? { get }
    var productThumnail: String? { get }
}

// MARK: - Product
struct ShopRecommendModel: Codable, PaymentShopType {
    let productID: Int?
    let sale: SaleShopModel?
    let productCode, productName: String?
    let productSoldout: Int?
    let productThumnail: String?
//    let productImgs: [JSONAny]
    let productMinprice: Double?
    let productMaxprice: Double?
    let productQuantity, productStarpoint: Double?
    let productBuycntt, productReviewcntt, productWeight, productWidth: Int?
    let productLength: Int?
    let productDescription: String?
//    let brand: Brand
    let category: [CategoryShopModel]?
    let attributes: [AttributeShopModel]?
//    let optionPrices, optionPriceSales: [JSONAny]

    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case sale
        case productCode = "product_code"
        case productName = "product_name"
        case productSoldout = "product_soldout"
        case productThumnail = "product_thumnail"
//        case productImgs = "product_imgs"
        case productQuantity = "product_quantity"
        case productMinprice = "product_minprice"
        case productMaxprice = "product_maxprice"
        case productStarpoint = "product_starpoint"
        case productBuycntt = "product_buycntt"
        case productReviewcntt = "product_reviewcntt"
        case productWeight = "product_weight"
        case productWidth = "product_width"
        case productLength = "product_length"
        case productDescription = "product_description"
        case category, attributes
//        case brand
//        case optionPrices = "option_prices"
//        case optionPriceSales = "option_price_sales"
    }
}

// MARK: - Brand
struct Brand: Codable {
}
// MARK: - Attribute
struct AttributeShopModel: Codable {
    let attributeID: Int?
    let attributeName: String?
    let options: [OptionShopModel]?

    enum CodingKeys: String, CodingKey {
        case attributeID = "attribute_id"
        case attributeName = "attribute_name"
        case options
    }
}

// MARK: - Option
struct OptionShopModel: Codable {
    let optionID, productAttributeID: Int?
    let productOptionName, productThumbnail: String?

    enum CodingKeys: String, CodingKey {
        case optionID = "option_id"
        case productAttributeID = "product_attribute_id"
        case productOptionName = "product_option_name"
        case productThumbnail = "product_thumbnail"
    }
}

enum SearchShopType: Int {
    case relateProduct = 1, newest, bestSeller, prices
}

enum ProductTradeMarkType: Int, TradeMarkElementModelType, CaseIterable {
    var name: String? {
        return self.text
    }
    
    var linkImage: String? {
        return nil
    }
    
    var idType: Int? {
        return self.rawValue
    }
    
    case fromLow = 2, fromHigh, newest, rate, bestSeller
    
    var text: String {
        switch self {
        case .fromLow: return "Giá từ: Thấp - Cao"
        case .fromHigh: return "Giá từ: Cao - Thấp"
        case .newest: return "Mới nhất"
        case .rate: return "Giảm giá nhiều nhất"
        case .bestSeller: return "Bán chạy"
        }
    }
}

// MARK: - Sale
struct SaleShopModel: Codable {
    let productSaleprice, percentSale, productSaleQuantity, productSaleQuantityRemain: Double?

    enum CodingKeys: String, CodingKey {
        case productSaleprice = "product_saleprice"
        case percentSale = "percent_sale"
        case productSaleQuantity = "product_sale_quantity"
        case productSaleQuantityRemain = "product_sale_quantity_remain"
    }
}
