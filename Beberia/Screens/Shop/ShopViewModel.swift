//
//  ShopViewModel.swift
//  Beberia
//
//  Created by haiphan on 16/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class ShopViewModel {
    
    let shopHomeModel: BehaviorRelay<ShopHomeModel?> = BehaviorRelay.init(value: nil)
    let shopRecommend: PublishSubject<ShopRecommenDatadMode?> = PublishSubject.init()
    let dealHot: PublishSubject<ShopRecommenDatadMode?> = PublishSubject.init()
    private let disposeBag = DisposeBag()
    init() {
    }
    
    func getShopHome() {
        AlamorfireService.shared.getHomeShop().bind(to: shopHomeModel).disposed(by: disposeBag)
        AlamorfireService.shared.getRecommend(page: 1).bind(to: shopRecommend).disposed(by: disposeBag)
        
    }
    
    func dealHot(pageToken: String) {
        AlamorfireService.shared.dealShop(pageToken: pageToken).bind(to: dealHot).disposed(by: disposeBag)
    }
    
    
}
