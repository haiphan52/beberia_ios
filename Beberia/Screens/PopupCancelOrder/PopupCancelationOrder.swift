//
//  PopupCancelationOrder.swift
//  Beberia
//
//  Created by haiphan on 09/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol PopupCancelationOrderDelegate {
    func actionPopup(action: PopupCancelationOrder.Action)
}

class PopupCancelationOrder: UIViewController {
    
    enum Status {
        case cancelOrder, paymentFail, pregnant
    }
    
    enum Action: Int, CaseIterable {
        case cancelView, homeBaby, orders, pregnant
    }

    var deleegate: PopupCancelationOrderDelegate?
    var statusView: Status = .cancelOrder
    var subPayment: String?
    
    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var paymentFailView: UIView!
    @IBOutlet weak var pregnantView: UIView!
    @IBOutlet weak var lbPregnant: UILabel!
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
}
extension PopupCancelationOrder {
    
    private func setupUI() {
        self.showView(type: self.statusView)
    }
    
    private func setupRX() {
        Action.allCases.forEach { [weak self] type in
            guard let wSelf = self else { return }
            let bt = wSelf.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                wSelf.dismiss(animated: true) {
                    wSelf.deleegate?.actionPopup(action: type)
                }
            }.disposed(by: wSelf.disposeBag)
        }
    }
    
    func showView(type: Status) {
        switch type {
        case .cancelOrder:
            self.cancelView.isHidden = false
        case .paymentFail:
            self.paymentFailView.isHidden = false
        case .pregnant:
            self.pregnantView.isHidden = false
        }
    }
    
    func loadValuePregnant(strDay: String, isNotNumberWeek: Bool = true) {
        guard isNotNumberWeek else {
            self.lbPregnant.text = "Số tuần phải nhỏ hơn 42"
            return
        }
        self.lbPregnant.text = "Ngày sinh dự kiến không hợp lệ, bạn phải chọn một ngày trong tương lai."
        if !strDay.isEmpty {
            let day = "Ngày sinh dự kiến không hợp lệ, nó cách quá xa ngày hôm nay. Thời gian thai kỳ được giới hạn 42 tuần. Xin hãy chọn ngày dự kiến trong hôm nay tới ngày %@"
            self.lbPregnant.text = String(format: day, strDay)
        }
    }
}
