//
//  PopupGuideGetGift.swift
//  Beberia
//
//  Created by Lap on 11/28/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import UIKit
import WebKit
import RxSwift
import RxCocoa

class PopupGuideGetGift: UIView {
    
    @IBOutlet weak var btnGotIt: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var btPolicy: UIButton!
    private let disposeBag = DisposeBag()
  
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    override func removeFromSuperview() {
        superview?.removeFromSuperview()
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupGuideGetGift.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> PopupGuideGetGift {
        return R.nib.popupGuideGetGift.instantiate(withOwner: nil)[0] as! PopupGuideGetGift
    }
  
}
extension PopupGuideGetGift {
  
    func loadURL(urlStr: String) {
      self.btPolicy.contentHorizontalAlignment = .left
      self.btPolicy.isHidden = false
      self.btnGotIt.isEnabled = false
      self.btnGotIt.backgroundColor = R.color.neutralgray4()
      if let url = URL(string: urlStr) {
        let urlRequest = URLRequest(url: url)
        self.webView.load(urlRequest)
      }
    }
    
    func updateTextView(string: String) {
        self.textView.text = string
    }
    
    func updateWebView(text: String) {
        let fontSize = 30
        let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
        self.webView.loadHTMLString( fontSetting + text, baseURL: nil)
    }
    
}

