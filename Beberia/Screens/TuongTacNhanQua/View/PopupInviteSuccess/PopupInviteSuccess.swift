//
//  PopupInviteSuccess.swift
//  Beberia
//
//  Created by Lap on 11/28/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol PopupInviteSuccessDelegate {
    func shareCode()
}

class PopupInviteSuccess: UIView {
    
    struct Constant {
        static let heightRow: CGFloat = 50
    }
    
    @IBOutlet weak var imgEmpty: UIImageView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var tbvFriend: UITableView!
    @IBOutlet weak var lbInviteSuccess: UILabel!
    @IBOutlet weak var lbReferral: UILabel!
    
    var delegate: PopupInviteSuccessDelegate?
    
    private var listUser = BehaviorRelay.init(value: [GameUser]())
    private var disposeBag = DisposeBag()
    private var page = 1
    private var nextPage = 1
    private var total = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
        self.getListInvite()
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupInviteSuccess.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> PopupInviteSuccess {
        return R.nib.popupInviteSuccess.instantiate(withOwner: nil)[0] as! PopupInviteSuccess
    }
    
}

extension PopupInviteSuccess {
    
    private func setupUI() {
        self.lbReferral.text = UserInfo.shareUserInfo.name
        self.tbvFriend.delegate = self
        self.tbvFriend.register(R.nib.popupInviteSuccessCell)
        self.tbvFriend.rowHeight = Constant.heightRow
//        self.tbvFriend.indicatorStyle = .white
//        self.tbvFriend.sets
        
        self.tbvFriend.addInfiniteScroll { (tableView) in
            if self.listUser.value.count >= self.total {
                tableView.finishInfiniteScroll()
            } else  if self.nextPage != 0 {
                self.getListInvite()
            }else{
                self.self.tbvFriend.endRefreshing(at: .top)
            }
        }
        
    }
    
    private func setupRX() {
        self.listUser.asObservable().bind { [weak self] list in
            guard let wSelf = self else { return }
            wSelf.tbvFriend.isHidden = (list.count < 0) ? true : false
            wSelf.imgEmpty.isHidden = (list.count > 0) ? true : false
        }.disposed(by: disposeBag)
        
        listUser.bind(to: self.tbvFriend.rx.items(cellIdentifier: R.reuseIdentifier.popupInviteSuccessCell.identifier, cellType: PopupInviteSuccessCell.self)) { row, data, cell in
                cell.lbNo.text = "\(row + 1)"
                cell.updateValue(gameUser: data)
            }.disposed(by: disposeBag)
        
        self.btnShare.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.delegate?.shareCode()
        }.disposed(by: self.disposeBag)
    }
    
    func setScrollIndicatorColor(color: UIColor) {
        for view in self.tbvFriend.subviews {
            if view.isKind(of: UIImageView.self),
               let imageView = view as? UIImageView,
               let _ = imageView.image  {
                imageView.image = nil
                view.backgroundColor = color
            }
        }
        self.tbvFriend.flashScrollIndicators()
    }
    
    private func getListInvite(){
        APIManager.listInvite(page: self.nextPage) { [weak self] gameUsers, nextPage, total in
            guard let wSelf = self else { return }
            var myListTemp = wSelf.listUser.value
            myListTemp.append(contentsOf: gameUsers)
            wSelf.listUser.accept(myListTemp)
            wSelf.nextPage = nextPage
            wSelf.total = total
            wSelf.tbvFriend.endRefreshing(at: .top)
            wSelf.tbvFriend.finishInfiniteScroll(completion: { (collection) in
            })
            wSelf.lbInviteSuccess.text = "Giới thiệu thành công (\(myListTemp.count))"
        } failed: { error in
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
        }
        
    }
}
extension PopupInviteSuccess: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
}
