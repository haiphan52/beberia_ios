//
//  PopupInviteSuccessCell.swift
//  Beberia
//
//  Created by haiphan on 13/12/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit

class PopupInviteSuccessCell: UITableViewCell {

    @IBOutlet weak var lbNo: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateValue(gameUser: GameUser) {
        self.lbName.text = gameUser.displayName
        if let avatar = gameUser.avatar {
            self.imgAvatar.kf.setImage(with: URL.init(string: avatar))
        }
    }
    
    func setValueImageText(value: BaseModelImageText) {
        self.lbName.text = value.text
        if let avatar = value.img {
            self.imgAvatar.kf.setImage(with: URL.init(string: avatar))
        }
    }
    
}
