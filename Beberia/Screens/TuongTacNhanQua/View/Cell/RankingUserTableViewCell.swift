//
//  RankingUserTableViewCell.swift
//  Beberia
//
//  Created by Lap on 11/30/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit

class RankingUserTableViewCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var sttLable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
