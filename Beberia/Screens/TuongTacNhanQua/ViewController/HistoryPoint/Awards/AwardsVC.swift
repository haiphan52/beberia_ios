//
//  AwardsVC.swift
//  Beberia
//
//  Created by haiphan on 18/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AwardsVC: UIViewController {

    var gifts: BehaviorRelay<[GiftRankingModel]> = BehaviorRelay.init(value: [])
    
    @IBOutlet weak var tableView: UITableView!
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
}
extension AwardsVC {
    
    private func setupUI() {
        tableView.register(AwardCell.nib, forCellReuseIdentifier: AwardCell.identifier)
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    private func setupRX() {
        self.gifts.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: AwardCell.identifier, cellType: AwardCell.self)) {(row, element, cell) in
                cell.lbNo.text = "\(row + 1)"
                cell.setupValue(model: element)
            }.disposed(by: disposeBag)
    }
    
}
extension AwardsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
