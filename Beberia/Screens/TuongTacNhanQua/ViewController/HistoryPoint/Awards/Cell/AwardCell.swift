//
//  AwardCell.swift
//  Beberia
//
//  Created by haiphan on 18/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class AwardCell: UITableViewCell {

    @IBOutlet weak var lbNo: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupValue(model: GiftRankingModel) {
        self.lbTitle.text = "\(model.orderLabel ?? "Giải Khuyến Khích")"
        self.imgProduct.setImage(url: URL.init(string: model.media ?? "")!)
        self.lbSubTitle.text = "\(model.name ?? "")"
    }
    
}
