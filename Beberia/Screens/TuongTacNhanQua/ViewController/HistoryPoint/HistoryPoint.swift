//
//  HistoryPoint.swift
//  Beberia
//
//  Created by haiphan on 18/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HistoryPoint: UIViewController {
    
    enum ValueSegment: Int, CaseIterable {
        case users, gifts
    }

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btDismiss: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var lbStartDate: UILabel!
    @IBOutlet weak var lbStartTime: UILabel!
    @IBOutlet weak var lbEndDate: UILabel!
    @IBOutlet weak var lbEndTime: UILabel!
    private var pageViewController: UIPageViewController!
    private var rankingByTiemModel: RankingByTimeModel?
    private lazy var usersVC: UsersHistory = {
        let vc = UsersHistory.createVC()
        return vc
    }()
    private lazy var awardsVC: AwardsVC = {
        let vc = AwardsVC.createVC()
        return vc
    }()
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? UIPageViewController {
            pageViewController = vc
            pageViewController.setViewControllers([usersVC], direction: .forward, animated: true, completion: nil)
        }
    }

}
extension HistoryPoint {
    
    private func setupUI() {
        self.contentView.clipsToBounds = true
        self.contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        //segmetn select
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: R.color.ff0046() ?? UIColor.black], for: .selected)
        // color of other options
        self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        
        self.getRanking()
    }
    
    private func setupRX() {
        self.segment.rx.value.changed.asObservable().bind { [weak self] idx in
            guard let wSelf = self, let type = ValueSegment(rawValue: idx) else { return }
            switch type {
            case .users:
                wSelf.pageViewController.setViewControllers([wSelf.usersVC], direction: .reverse, animated: true, completion: nil)
            case .gifts:
                wSelf.pageViewController.setViewControllers([wSelf.awardsVC], direction: .forward, animated: true, completion: nil)
            }
        }.disposed(by: self.disposeBag)
        
        self.btDismiss.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.dismiss(animated: true, completion: nil)
        }.disposed(by: self.disposeBag)
    }
    
    private func getRanking() {
        APIManager.listHistory { [weak self] (rankingModel) in
            guard let wSelf = self, let list = rankingModel.list else { return }
            if let gifts = rankingModel.gift {
                wSelf.awardsVC.gifts.accept(gifts)
            }
            wSelf.usersVC.listUser.accept(list)
            wSelf.usersVC.rankingModel = rankingModel
            wSelf.updateTime(model: rankingModel.time)
        } failed: { (error) in
            print(error)
        }
    }
    
    private func updateTime(model: TimeRankingModel?) {
        self.lbStartDate.text = model?.toStringStartDate()
        self.lbStartTime.text = model?.toStringStartTime()
        self.lbEndDate.text = model?.toStringEndDate()
        self.lbEndTime.text = model?.toStringEndTime()
    }
    
    
}
