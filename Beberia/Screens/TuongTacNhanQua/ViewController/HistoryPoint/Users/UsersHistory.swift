//
//  UsersHistory.swift
//  Beberia
//
//  Created by haiphan on 18/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UsersHistory: UIViewController {

    var listUser: BehaviorRelay<[ListUser]> = BehaviorRelay.init(value: [])
    var rankingModel: RankingByTimeModel?
    
    @IBOutlet weak var tableView: UITableView!
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
}
extension UsersHistory {
    
    private func setupUI() {
        tableView.register(UserHistoryCell.nib, forCellReuseIdentifier: UserHistoryCell.identifier)
        tableView.delegate = self
        tableView.rowHeight = 60
    }
    
    private func setupRX() {
        self.listUser.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: UserHistoryCell.identifier, cellType: UserHistoryCell.self)) {(row, element, cell) in
                cell.lbNo.text = "\(row + 1)"
                cell.setupData(data: element)
                cell.imgFirst.isHidden = (row == 0) ? false : true
            }.disposed(by: disposeBag)
        
        self.tableView.rx.itemSelected.bind { [weak self] idx in
            guard let wSelf = self else { return }
            let userId = wSelf.listUser.value[idx.row]
            if let timeId = wSelf.rankingModel?.time?.id {
                let vc = ArchievermentDetailVC.createVC()
                vc.timeId = timeId
                vc.user = userId
                vc.index = idx.row + 1
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overFullScreen
                wSelf.present(vc, animated: true, completion: nil)
            }
        }.disposed(by: disposeBag)
    }
    
}
extension UsersHistory: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
