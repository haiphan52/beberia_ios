//
//  UserHistoryCell.swift
//  Beberia
//
//  Created by haiphan on 18/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class UserHistoryCell: UITableViewCell {

    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var contentDataView: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lbPoint: UILabel!
    @IBOutlet weak var lbNo: UILabel!
    @IBOutlet weak var wPoint: NSLayoutConstraint!
    @IBOutlet weak var imgFirst: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension UserHistoryCell {
    
    private func setupUI() {
        
    }
    
    func setupData(data: ListUser){
        self.lbName.text = data.user?.display_name ?? ""
        self.imgUser.setImage(url: URL.init(string: data.user?.avatar ?? "")!)
        self.lbPoint.text = "\(data.point?.formatString(separator: ",") ?? "") điểm"
        self.wPoint.constant = self.lbPoint.sizeThatFits(self.lbPoint.bounds.size).width
        
    }
    
}
