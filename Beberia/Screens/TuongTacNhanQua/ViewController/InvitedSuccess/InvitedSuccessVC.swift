
//
//  
//  InvitedSuccessVC.swift
//  Beberia
//
//  Created by haiphan on 23/08/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

class InvitedSuccessVC: UIViewController {
    
    // Add here outlets
    @IBOutlet weak var contentView: UIView!
    private let successView = PopupInviteSuccess.instanceFromNib()
    // Add here your view model
    private var viewModel: InvitedSuccessVM = InvitedSuccessVM()
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
}
extension InvitedSuccessVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        self.contentView.addSubview(self.successView)
        self.successView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.successView.delegate = self
        
        let tapView: UITapGestureRecognizer = UITapGestureRecognizer()
        self.view.addGestureRecognizer(tapView)
        tapView.rx.event
            .withUnretained(self)
            .bind { owner, _ in
                owner.dismiss(animated: true, completion: nil)
            }.disposed(by: disposeBag)
    }
    
    private func setupRX() {
        // Add here the setup for the RX
    }
}
extension InvitedSuccessVC: PopupInviteSuccessDelegate {
    func shareCode() {
        ManageApp.shared.shareCode(viewcontroller: self)
    }
}
