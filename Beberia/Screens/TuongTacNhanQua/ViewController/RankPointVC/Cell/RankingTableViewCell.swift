//
//  RankingTableViewCell.swift
//  Beberia
//
//  Created by Lap on 11/28/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit

class RankingTableViewCell: UITableViewCell {

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblStt: UILabel!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgStt: UIImageView!
    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var imgFirst: UIImageView!
    @IBOutlet weak var img1st: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    func setupData(data: ListUser){
        lblName.text = data.user?.display_name ?? ""
        imgUser.setImage(url: URL.init(string: data.user?.avatar ?? "")!)
        lblPoint.text = "\(data.point?.formatString(separator: ",") ?? "") điểm"
        
    }
    
}
