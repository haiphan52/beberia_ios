//
//  RankPointVC.swift
//  Beberia
//
//  Created by haiphan on 13/12/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class RankPointVC: BaseViewController, NavigationProtocol {
    
    enum Action: Int, CaseIterable {
        case back, get500, getPoint, history
    }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lbIndex: UILabel!
    @IBOutlet weak var lbMyPoint: UILabel!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var giftView: UIView!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var contentTableView: UIView!
    @IBOutlet weak var lbStartDate: UILabel!
    @IBOutlet weak var lbStartTime: UILabel!
    @IBOutlet weak var lbEndDate: UILabel!
    @IBOutlet weak var lbEndTime: UILabel!
    @IBOutlet weak var indexView: UIView!
    private var dataRanking: BehaviorRelay<[ListUser]> = BehaviorRelay.init(value: [])
    private var rankingModel: RankingByTimeModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

}
extension RankPointVC {
    private func setupUI() {
        self.tableView.register(R.nib.rankingTableViewCell)
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.rowHeight = 60
        self.tableView.delegate = self
        self.tableView.clipsToBounds = true
        self.tableView.layer.cornerRadius = 8
        
        self.timeView.clipsToBounds = true
        
        self.giftView.clipsToBounds = true
        self.giftView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        self.setupProfile()
        self.getRanking()
    }
    
    private func setupRX() {
        self.dataRanking.asObservable().bind { [weak self] list in
            guard let wSelf = self else { return }
            wSelf.contentTableView.isHidden = (list.count <= 0) ? true : false
            wSelf.emptyView.isHidden = (list.count <= 0) ? false : true
            if list.count <= 0 {
                wSelf.timeView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            } else {
                wSelf.timeView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            }
            wSelf.lbIndex.isHidden = (list.count <= 0) ? true : false
            wSelf.indexView.isHidden = (list.count <= 0) ? true : false
        }.disposed(by: self.disposeBag)
        
        dataRanking.asObservable().bind(to: self.tableView.rx.items(cellIdentifier: R.reuseIdentifier.rankingTableViewCell.identifier,
                                                                    cellType: RankingTableViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
            
            cell.setupData(data: data)
            cell.lblStt.text = "\(row + 1)"
            cell.imgStt.image = R.image.group5654()
            cell.viewBg.backgroundColor = UIColor.init(hexString: "FEF0C3", alpha: 1)
            cell.lblStt.textColor = .white
            cell.img1st.isHidden = (row == 0) ? false : true
        }.disposed(by: disposeBag)
        
        self.tableView.rx.itemSelected.bind { [weak self] idx in
            guard let wSelf = self else { return }
            let userId = wSelf.dataRanking.value[idx.row]
            if let timeId = wSelf.rankingModel?.time?.id {
                let vc = ArchievermentDetailVC.createVC()
                vc.timeId = timeId
                vc.user = userId
                vc.index = idx.row + 1
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overFullScreen
                wSelf.present(vc, animated: true, completion: nil)
            }
        }.disposed(by: disposeBag)
        
        Action.allCases.forEach { [weak self] type in
            guard let wSelf = self else { return }
            let bt = wSelf.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                switch type {
                case .back:
                    wSelf.navigationController?.popViewController(animated: true)
                case .get500:
                    wSelf.moveToInvited()
                case .getPoint:
                    let guideGetPoint = GuideGetPointViewController.init(nib: R.nib.guideGetPointViewController)
                    wSelf.navigationController?.pushViewController(guideGetPoint, animated: true)
                case .history:
                    let vc = HistoryPoint.createVCfromStoryBoard()
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overCurrentContext
                    wSelf.present(vc, animated: true, completion: nil)
                }
            }.disposed(by: wSelf.disposeBag)
        }
    }
    
    
    private func setupProfile() {
        self.imgAvatar.kf.setImage(with: URL.init(string: UserInfo.shareUserInfo.avartar))
    }
    
    private func getRanking() {
        APIManager.rankingListByTime { [weak self] (rankingModel) in
            guard let wSelf = self, let list = rankingModel.list else { return }
            wSelf.rankingModel = rankingModel
            wSelf.dataRanking.accept(list)
            wSelf.lbMyPoint.text = "\(rankingModel.myPoint?.formatString(separator: ",") ?? "")"
            wSelf.getIndex(list: list)
            wSelf.updateTime(model: rankingModel.time)
        } failed: { (error) in
            print(error)
        }
    }
    
    private func updateTime(model: TimeRankingModel?) {
        self.lbStartDate.text = model?.toStringStartDate()
        self.lbStartTime.text = model?.toStringStartTime()
        self.lbEndDate.text = model?.toStringEndDate()
        self.lbEndTime.text = model?.toStringEndTime()
    }
    
    private func getIndex(list: [ListUser]) {
        guard let id = UserInfo.shareUserInfo.id else {
            return
        }
        if let index = list.firstIndex(where: { $0.user?.id == id }) {
            self.lbIndex.text = "\(index + 1)"
        }
    }
    
}
extension RankPointVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
extension RankPointVC: PopupInviteSuccessDelegate {
    func shareCode() {
        ManageApp.shared.shareCode(viewcontroller: self)
    }
    
    
}
