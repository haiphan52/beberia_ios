//
//  RankPointViewController.swift
//  Beberia
//
//  Created by Lap on 11/25/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class RankPointViewController: BaseViewController {

    @IBOutlet weak var btnGet200Point: UIButton!
    @IBOutlet weak var btnGetPoint: UIButton!
    @IBOutlet weak var tbvRanking: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var contentTextView: UIView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var btHistory: UIButton!
    
    var dataRanking = BehaviorRelay.init(value: [ListUser]())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getRanking()
        initRx()
        initUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

extension RankPointViewController {

    func initUI(){
        tbvRanking.layer.cornerRadius = 12
        tbvRanking.tableFooterView = UIView.init()
        tbvRanking.register(R.nib.rankingTableViewCell)
        tbvRanking.rowHeight = 60
        
        self.contentTextView.clipsToBounds = true
        self.contentTextView.layer.cornerRadius = 28
        self.contentTextView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        self.setupProfile()
    }
    
    func initRx(){
        
        dataRanking.asObservable().bind(to: self.tbvRanking.rx.items(cellIdentifier: R.reuseIdentifier.rankingTableViewCell.identifier, cellType: RankingTableViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
            
            cell.setupData(data: data)
            cell.lblStt.text = "\(row + 1)"
            if row == 0 {
                cell.imgStt.image = R.image.group5654()
                cell.viewBg.backgroundColor = UIColor.init(hexString: "FED845", alpha: 0.5)
                cell.lblStt.textColor = .white
            } else if row == 1{
                cell.imgStt.image = R.image.group()
                cell.viewBg.backgroundColor = UIColor.init(hexString: "D9D9D9", alpha: 0.5)
                cell.lblStt.textColor = .white
            } else {
                cell.imgStt.image = R.image.ellipse423()
                cell.lblStt.textColor = .black
            }
            
        }.disposed(by: disposeBag)
        
        btnBack.rx.tap.subscribe { (_) in
            self.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
        
        btnGet200Point.rx.tap.subscribe { (_) in
            let view = PopupInviteSuccess.instanceFromNib()
            let _ = self.showPopup(view: view, height: ScreenSize.screenHeight - 300)
        }.disposed(by: disposeBag)
        
        btnGetPoint.rx.tap.subscribe { (_) in
            let guideGetPoint = GuideGetPointViewController.init(nib: R.nib.guideGetPointViewController)
            self.navigationController?.pushViewController(guideGetPoint, animated: true)
        }.disposed(by: disposeBag)
        
        self.btHistory.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
//            let vc = HistoryPointVC.createVC()
//            vc.modalTransitionStyle = .crossDissolve
//            vc.modalPresentationStyle = .overCurrentContext
//            wSelf.present(vc, animated: true, completion: nil)
        }.disposed(by: self.disposeBag)
        
    }
    
    private func setupProfile() {
        self.imgAvatar.kf.setImage(with: URL.init(string: UserInfo.shareUserInfo.avartar))
    }
    
    func getRanking() {
        APIManager.rankingList { [weak self] (rankingModel) in
            guard let wSelf = self, let list = rankingModel.list else { return }
            DispatchQueue.main.async {
                wSelf.dataRanking.accept(list)
//                wSelf.lbMyPoint.text = "\(rankingModel.my_point ?? 0)"
                wSelf.getIndex(list: list)
            }
        } failed: { (error) in
            print(error)
        }
    }
    
    private func getIndex(list: [ListUser]) {
        guard let id = UserInfo.shareUserInfo.id else {
            return
        }
        if let index = list.firstIndex(where: { $0.user?.id == id }) {
//            self.lbIndex.text = "\(index + 1)"
        }
    }
}
