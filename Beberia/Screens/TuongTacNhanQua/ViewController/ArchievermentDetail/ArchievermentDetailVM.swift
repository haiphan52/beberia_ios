
//
//  ___HEADERFILE___
//
import Foundation
import RxCocoa
import RxSwift

class ArchievermentDetailVM {
    
    let achievementEvent: PublishSubject<AchievementsModel> = PublishSubject.init()
    let msgError: PublishSubject<String> = PublishSubject.init()
    
    private let disposeBag = DisposeBag()
    init() {
    }
    
    func getRankingDetail (timeId: Int, userId: Int) {
        APIManager.rankingDetail(timeId: timeId, userId: userId) { [weak self] item in
            guard let wSelf = self else { return }
            wSelf.achievementEvent.onNext(item)
        } failed: { [weak self] txtError in
            guard let wSelf = self else { return }
            wSelf.msgError.onNext(txtError)
        }
    }
}
