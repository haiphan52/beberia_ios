//
//  ArchievermentModel.swift
//  Beberia
//
//  Created by haiphan on 25/03/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

// MARK: - DataClass
struct AchievementsModel: Codable {
    let list: [AchievementModel]?
    
    enum CodingKeys: String, CodingKey {
        case list = "list"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([AchievementModel].self, forKey: .list)
    }
}

// MARK: - List
struct AchievementModel: Codable {
    let number, label: String?
    enum CodingKeys: String, CodingKey {
        case number = "number"
        case label = "label"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        number = try values.decodeIfPresent(String.self, forKey: .number)
        label = try values.decodeIfPresent(String.self, forKey: .label)
    }
}
