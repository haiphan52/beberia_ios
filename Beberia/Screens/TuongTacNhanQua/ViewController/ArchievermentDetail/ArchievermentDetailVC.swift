
//
//  
//  ArchievermentDetailVC.swift
//  Beberia
//
//  Created by haiphan on 25/03/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

class ArchievermentDetailVC: UIViewController {
    
    enum StatusLabel: Int, CaseIterable {
        case invited, liked, commented, posted
    }
    
    // Add here outlets
    var timeId: Double?
    var user: ListUser?
    var index: Int = 1
    
    @IBOutlet weak var btClose: UIButton!
    @IBOutlet var lbsTitle: [UILabel]!
    @IBOutlet var lbsNumber: [UILabel]!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPoint: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lbIndex: UILabel!
    @IBOutlet weak var img1st: UIImageView!
    // Add here your view model
    private var viewModel: ArchievermentDetailVM = ArchievermentDetailVM()
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
}
extension ArchievermentDetailVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        if let timeId = self.timeId, let user = self.user?.user, let userId = user.id {
            self.viewModel.getRankingDetail(timeId: Int(timeId), userId: userId)
        }
        
        if let data = self.user {
            self.lbName.text = data.user?.display_name ?? ""
            self.imgUser.setImage(url: URL.init(string: data.user?.avatar ?? "")!)
            self.lbPoint.text = "\(data.point?.formatString(separator: ",") ?? "") điểm"
        }
        
        if self.index != 1 {
            self.img1st.isHidden = true
        }
        self.lbIndex.text = "\(self.index)"
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        self.btClose.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.dismiss(animated: true, completion: nil)
        }.disposed(by: self.disposeBag)
        
        self.viewModel.achievementEvent.asObservable().bind { [weak self] item in
            guard let wSelf = self, let list = item.list else { return }
            list.enumerated().forEach { item in
                let element = item.element
                let offset = item.offset
                if let status = StatusLabel(rawValue: offset) {
                    wSelf.lbsTitle[status.rawValue].text = element.label
                    wSelf.lbsNumber[status.rawValue].text = element.number
                }
            }
        }.disposed(by: self.disposeBag)
    }
}
