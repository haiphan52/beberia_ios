//
//  GuideGetPointViewController.swift
//  Beberia
//
//  Created by Lap on 11/28/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

struct GuideModel {
    var title: String?
    var point: String?
    var action: Int?
}

class GuideGetPointViewController: BaseViewController {
    
    enum openfrom {
        case guide, other
    }
    
    enum PointType: Int {
        case profile = 1
        case invite = 7
        case homeLike = 2
        case homeComment = 4
        case homePost = 6
        case DiaryImage = 5
        case DiaryFrame = 19
        case momTok = 18
        case invited = 8
    }
    
    struct Constant {
        static let sizeCellNormal: CGFloat = 64
        static let sizeCellReferral: CGFloat = 84
        static let sizeApply: CGFloat = 96
        static let heightPopup: CGFloat = 600
    }
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tbvGuide: UITableView!
    @IBOutlet weak var btnNote: UIButton!
    
    private var policyStr: String = ""
    var dataGuide = BehaviorRelay.init(value: [GuidePoint]())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let data = [GuideModel.init(title: "Cập nhật thông tin mẹ và bé *", point: "+200", action: 0),
//                    GuideModel.init(title: "Mời bạn bè tải ứng dụng thành công", point: "+500", action: 0),
//                    GuideModel.init(title: "Thả tim", point: "+10", action: 0),
//                    GuideModel.init(title: "Nhận tim", point: "+50", action: 0),
//                    GuideModel.init(title: "Bình luận/ Nhận bình luận", point: "+100", action: 0),
//                    GuideModel.init(title: "Đăng bài viết trên Diễn đàn", point: "+200", action: 0),
//                    GuideModel.init(title: "Đăng bài viết lên Nhật ký", point: "+200", action: 0),
//                    GuideModel.init(title: "Đăng bài viết lên Momtok", point: "+200", action: 0),
//                    GuideModel.init(title: "Được bạn bè giới thiệu tải ứng dụng, nhập mã giới thiệu thành công", point: "+50", action: 0),
//                    GuideModel.init(title: "Apply", point: "+50", action: 0)]
//
//        dataGuide.accept(data)
        
        initUI()
        initRx()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    func initUI(){
        tbvGuide.layer.cornerRadius = 12
        tbvGuide.tableFooterView = UIView.init()
        tbvGuide.register(R.nib.guideGetPointTableViewCell)
        tbvGuide.contentInset = UIEdgeInsets(top: 45, left: 0, bottom: 0, right: 0)
        tbvGuide.delegate = self
        
        self.getRanking()
        self.getPolicy()
    }
    
   
    func initRx(){
        btnBack.rx.tap.subscribe { (_) in
            self.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
        
        btnNote.rx.tap.subscribe { [weak self] (_) in
            guard let wSelf = self else { return }
            let view = PopupGuideGetGift.instanceFromNib()
            view.updateWebView(text: wSelf.policyStr)
            let distance = ScreenSize.screenHeight - Constant.heightPopup
            let popup = wSelf.showPopup(view: view, height: ScreenSize.screenHeight - distance)
            view.btnGotIt.rx.tap.subscribe { (_) in
                popup.dismiss(animated: true)
            }
            .disposed(by: wSelf.disposeBag)
        }.disposed(by: disposeBag)
        
        dataGuide.asObservable().bind(to: self.tbvGuide.rx.items(cellIdentifier: R.reuseIdentifier.guideGetPointTableViewCell.identifier, cellType: GuideGetPointTableViewCell.self)) { [weak self]  row, data, cell in
            guard let wSelf = self else { return }
            cell.updateValue(guide: data)
            cell.tapAction = { [weak self] in
                guard let wSelf = self else { return }
                wSelf.moveToVC(row: row)
            }
            
            if row == wSelf.dataGuide.value.count - 1 {
                cell.contentApply.isHidden = false
            } else {
                cell.contentApply.isHidden = true
            }
        }.disposed(by: disposeBag)
    }
    
    private func moveToVC(row: Int) {
        switch PointType.init(rawValue: self.dataGuide.value[row].type ?? 0) {
        case .profile:
            let profileVC = R.storyboard.main.profileViewController()
            profileVC?.user.id = UserInfo.shareUserInfo.id
            let naviProfile = UINavigationController.init(rootViewController: profileVC!)
            naviProfile.modalPresentationStyle = .fullScreen
            Utils.getTopMostViewController()?.navigationController?.present(naviProfile, animated: true, completion: nil)
        case .invite:
            let vc = InvitedSuccessVC.createVC()
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
        case .homeLike, .homeComment, .homePost:
            let tabBarVC = R.storyboard.main.homeViewController()!
            let naviProfile = UINavigationController.init(rootViewController: tabBarVC)
            naviProfile.modalPresentationStyle = .fullScreen
            tabBarVC.openFrom = .guide
            Utils.getTopMostViewController()?.navigationController?.present(naviProfile, animated: true, completion: nil)
        case .DiaryImage, .DiaryFrame:
            let tabBarVC = R.storyboard.main.diaryViewController()!
            let naviProfile = UINavigationController.init(rootViewController: tabBarVC)
            naviProfile.modalPresentationStyle = .fullScreen
            tabBarVC.openFrom = .guide
            Utils.getTopMostViewController()?.navigationController?.present(naviProfile, animated: true, completion: nil)
        case .momTok:
            guard let momtokVC = R.storyboard.main.secretViewController() else {
                return
            }
//            momtokVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(momtokVC, animated: true)
        
        default: break
        }
    }
    
    private func getRanking() {
        APIManager.rankingListPoint { [weak self] (rankingModel) in
            guard let wSelf = self, var list = rankingModel.datas  else { return }
            DispatchQueue.main.async {
                wSelf.dataGuide.accept(list)
            }
        } failed: { (error) in
            print(error)
        }
    }
    
    private func getPolicy() {
        APIManager.policy { [weak self] (rankingModel) in
            guard let wSelf = self else { return }
            wSelf.policyStr = rankingModel.policy ?? ""
        } failed: { (error) in
            print(error)
        }
    }

}
extension GuideGetPointViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension GuideGetPointViewController: PopupInviteSuccessDelegate {
    func shareCode() {
        ManageApp.shared.shareCode(viewcontroller: self)
    }
}
