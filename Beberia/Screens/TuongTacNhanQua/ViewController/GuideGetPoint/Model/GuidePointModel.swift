//
//  GuidePointModel.swift
//  Beberia
//
//  Created by haiphan on 17/12/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
// MARK: - WelcomeData
struct GuidePointData: Codable {
    let datas: [GuidePoint]?
    enum CodingKeys: String, CodingKey {
        case datas = "datas"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        datas = try values.decodeIfPresent([GuidePoint].self, forKey: .datas)
    }
}

// MARK: - DataElement
struct GuidePoint: Codable {
    let name: String?
    let type, status, point: Int?
    let des: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case type = "type"
        case status = "status"
        case point = "point"
        case des = "description"
    }
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        type = try values.decodeIfPresent(Int.self, forKey: .type)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        point = try values.decodeIfPresent(Int.self, forKey: .point)
        des = try values.decodeIfPresent(String.self, forKey: .des)
    }
    
}
