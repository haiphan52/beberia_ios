//
//  GuideGetPointTableViewCell.swift
//  Beberia
//
//  Created by Lap on 11/30/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxSwift

class GuideGetPointTableViewCell: UITableViewCell {
    
    struct Constant {
        static let typeFriend: Int = 8
    }

    var tapAction: (() -> Void)?
    
    @IBOutlet weak var btAction: UIButton!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lbSub: UILabel!
    @IBOutlet weak var contentApply: UIView!
    
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        self.setupRX()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupRX() {
        self.btAction.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.tapAction?()
        }.disposed(by: self.disposeBag)
    }
    
    func updateValue(guide: GuidePoint) {
        self.lbltitle.text = guide.name
        self.lblPoint.text = "+\(guide.point?.formatString(separator: ",") ?? "")"
        let img = (guide.status == 1) ? R.image.ic_done_guide() : R.image.ic_willDo_guide()
        self.btAction.setImage(img, for: .normal)
        self.btAction.isEnabled = (guide.status == 1) ? false : true
        self.lbSub.text = "\(guide.des ?? "")"
        if guide.type == Constant.typeFriend {
            self.btAction.isHidden = true
        } else {
            self.btAction.isHidden = false
        }
    }
    
}
