//
//  RankingModel.swift
//  Beberia
//
//  Created by Lap on 11/28/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation

struct RankingModel: Codable {
    var list: [ListUser]?
    var my_point: Int?
}

struct ListUser : Codable {
    var point: Int?
    var user: UserRanking?
}

struct UserRanking: Codable {
    var id: Int?
    var avatar: String?
    var display_name: String?
    var is_follow: Int?
}

// MARK: - DataClass
struct RankingByTimeModel: Codable {
    let list: [ListUser]?
    let myPoint: Int?
    let time: TimeRankingModel?
    let gift: [GiftRankingModel]?

    enum CodingKeys: String, CodingKey {
        case list
        case myPoint = "my_point"
        case time, gift
    }
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([ListUser].self, forKey: .list)
        myPoint = try values.decodeIfPresent(Int.self, forKey: .myPoint)
        time = try values.decodeIfPresent(TimeRankingModel.self, forKey: .time)
        gift = try values.decodeIfPresent([GiftRankingModel].self, forKey: .gift)
    }
}
// MARK: - Gift
struct GiftRankingModel: Codable {
    let id, order: Int?
    let media: String?
    let name: String?
    let timeID: Int?
    let orderLabel: String?

    enum CodingKeys: String, CodingKey {
        case id, order, media, name
        case timeID = "time_id"
        case orderLabel = "order_label"
    }
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        order = try values.decodeIfPresent(Int.self, forKey: .order)
        media = try values.decodeIfPresent(String.self, forKey: .media)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        timeID = try values.decodeIfPresent(Int.self, forKey: .timeID)
        orderLabel = try values.decodeIfPresent(String.self, forKey: .orderLabel)
    }
}

// MARK: - Time
struct TimeRankingModel: Codable {
    let id, startTime, endTime: Double?

    enum CodingKeys: String, CodingKey {
        case id
        case startTime = "start_time"
        case endTime = "end_time"
    }
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Double.self, forKey: .id)
        startTime = try values.decodeIfPresent(Double.self, forKey: .startTime)
        endTime = try values.decodeIfPresent(Double.self, forKey: .endTime)
    }
    
    func toStringStartDate() -> String {
        guard let start = self.startTime else {
            return Date().toString(format: .ddMMyyyy)
        }
        return start.toDate().toString(format: .ddMMyyyy)
    }
    
    func toStringStartTime() -> String {
        guard let start = self.startTime else {
            return Date().toString(format: .HHmma)
        }
        return start.toDate().toString(format: .HHmma)
    }
    
    func toStringEndDate() -> String {
        guard let end = self.endTime else {
            return Date().toString(format: .ddMMyyyy)
        }
        return end.toDate().toString(format: .ddMMyyyy)
    }
    
    func toStringEndTime() -> String {
        guard let end = self.endTime else {
            return Date().toString(format: .HHmma)
        }
        return end.toDate().toString(format: .HHmma)
    }
}

struct PolicyRankingModel: Codable {
    let policy: String?
    
    enum CodingKeys: String, CodingKey {
        case policy
    }
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        policy = try values.decodeIfPresent(String.self, forKey: .policy)
    }
}
