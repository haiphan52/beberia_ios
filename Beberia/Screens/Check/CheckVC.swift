//
//  CheckVC.swift
//  Beberia
//
//  Created by haiphan on 12/12/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Photos

class CheckVC: UIViewController {

    private var playVideo = AVPlayer()
    @IBOutlet weak var videoView: UIView!
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        guard let path = Bundle.main.path(forResource: "video_select_print", ofType:"mp4")else {
//            debugPrint("video.m4v not found")
//            return
//        }
//        let url = URL(fileURLWithPath: path)
//        DispatchQueue.main.asyncAfter(deadline: .now()) {
//            self.playVideo(url: url)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func playVideo(url: URL) {
        
        self.playVideo = AVPlayer(url: url)
        let playerLayer = AVPlayerLayer(player: self.playVideo)
        playerLayer.frame = videoView.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resize
        self.videoView.layer.addSublayer(playerLayer)
        self.playVideo.play()
       
    }
}
