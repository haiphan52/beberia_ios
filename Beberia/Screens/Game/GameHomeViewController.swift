//
//  GameHomeViewController.swift
//  Beberia
//
//  Created by Lap on 09/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GameHomeViewController: BaseViewController {
    
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var btnGift: UIButton!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var lblInviteSuccess: UILabel!
    @IBOutlet weak var lblTitleButon: UILabel!
    @IBOutlet weak var imgButton: UIImageView!
    @IBOutlet weak var heightBottom: NSLayoutConstraint!
    @IBOutlet weak var lbNameCode: UILabel!
    @IBOutlet weak var btSeeMore: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var listUser = BehaviorRelay.init(value: [GameUser]())
    var total = 0
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: R.font.notoSansSemiBold(size: 16)!,
         .foregroundColor: UIColor.black,
         .underlineStyle: NSUnderlineStyle.single.rawValue
     ]
    var page = 1
    var nextPage = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tfCode.isEnabled = false
        lbNameCode.text = UserInfo.shareUserInfo.name
        btnGift.isHidden = true
        lbltitle.isHidden = true
        heightBottom.constant = GetHeightSafeArea.shared.getHeight(type: .bottom)
      
        let attributeString = NSMutableAttributedString(
                string: "Xem tất cả",
                attributes: yourAttributes
             )
        btnViewAll.setAttributedTitle(attributeString, for: .normal)
        
        tableView.rowHeight = 50
        tableView.showsVerticalScrollIndicator = false
        tableView.register(R.nib.popupInviteSuccessCell)
        tableView.tableFooterView = UIView()
        tableView.addInfiniteScroll { (tableView) in
            if self.listUser.value.count >= self.total {
                self.tableView.endRefreshing(at: .top)
                self.tableView.finishInfiniteScroll(completion: { (collection) in
                })
                return
            }
            if self.nextPage != 0{
                self.getListInvite()
            }else{
                self.tableView.endRefreshing(at: .top)
                self.tableView.finishInfiniteScroll(completion: { (collection) in
                })
            }
        }
        
        getListInvite()
        updateTicket()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateTicket), name: NSNotification.Name.updateNumberTicket, object: nil)
        setupRX()
    }
    
    private func setupRX() {
        btSeeMore.rx.tap
            .withUnretained(self)
            .bind { owner, _ in
                let view = PopupInvite.instanceFromNib()
                let _ = owner.showPopup(view: view, height: 350)
            }.disposed(by: disposeBag)
        
        listUser
            .bind(to: tableView.rx.items(cellIdentifier: R.reuseIdentifier.popupInviteSuccessCell.identifier, cellType: PopupInviteSuccessCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                cell.lbNo.text = "\(row + 1)"
                cell.setValueImageText(value: data)
        }.disposed(by: disposeBag)
        
        listUser.withUnretained(self).bind { owner, list in
            owner.btSeeMore.isHidden = list.isEmpty
        }.disposed(by: disposeBag)
    }
    
    @objc func updateTicket() {
        if UserInfo.shareUserInfo.total_invite == 0 {
            imgButton.image = R.image.ic_join_now_game()
            lblTitleButon.text = "Tham Gia Ngay"
        } else{
            imgButton.image = R.image.ic_join_now_game()
            lblTitleButon.text = "Tham Gia Ngay (\(UserInfo.shareUserInfo.total_invite))"
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true) 
    }

    @IBAction func didPressGuide(_ sender: Any) {
        let view = PopupGuide.instanceFromNib()
        let _ = self.showPopup(view: view, height: 400)
    }
    
    @IBAction func didPressInvete(_ sender: Any) {
        let view = PopupInvite.instanceFromNib()
        let _ = self.showPopup(view: view, height: 350)
    }
    
    @IBAction func didPressBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didPressGo(_ sender: Any) {
     //   if UserInfo.shareUserInfo.total_invite != 0 {
            let gameVC = GameViewController.init(nib: R.nib.gameViewController)
            gameVC.modalPresentationStyle = .overFullScreen
            self.present(gameVC, animated: true, completion: nil)
     //   }
    }
    
    @IBAction func didPressOK(_ sender: Any) {
        let myWebsite = NSURL(string: ConstantApp.shared.linkGame)
        let text = ConstantApp.shared.contentShareGame(text: lbNameCode.text ?? "")
        
        guard let url = myWebsite else {
            print("nothing found")
            return
        }
        
        let shareItems:Array = [url,text] as [Any]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        activityViewController.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
           if completed {
               let view = PopupShare.instanceFromNib()
               let _ = self.showPopup(view: view, height: 150)
           }
        }
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func didPressListGiftRecive(_ sender: Any) {
        let view = PopupVoucher.instanceFromNib()
        let _ = self.showPopup(view: view, height: 400)
    }
}

extension GameHomeViewController {
    
    
    func handleJoinGame(){
        APIManager.inviteCode(code: tfCode.text ?? "") { codeStatus in
            print(codeStatus)
            if codeStatus == 200 {
                
                let view = PopupShare.instanceFromNib()
                let _ = self.showPopup(view: view, height: 300)
              
            } else if codeStatus == 400{
               
            } else {
                Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: "\(codeStatus)")
            }
            
        } failed: { error in
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
        }
    }
    
    func getListGift(){
        APIManager.listGift(game_id: "1") { gifts in
           // print(gifts[0].name ?? "")
        } failed: { error in
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
        }
    }
    
  
        func getListInvite(){
            APIManager.listInvite(page: nextPage) { gameUsers, nextPage, total in
                self.total = total
                self.lblInviteSuccess.text = "Giới thiệu thành công (\(total))"
//                var user = gameUsers
//                user.insert(GameUser.init(id: 0, avatar: "", displayName: ""), at: 0)
//                self.listUser.value = user
                
                var myListTemp = self.listUser.value
                let user = gameUsers
                
    //            if self.nextPage == 1 {
    //                user.insert(GameUser.init(id: 0, avatar: "", displayName: ""), at: 0)
    //            }

                myListTemp.append(contentsOf: user)
                self.listUser.accept(myListTemp)
                
                self.nextPage = nextPage
                
                
                self.tableView.endRefreshing(at: .top)
                self.tableView.finishInfiniteScroll(completion: { (collection) in
                })
                
            } failed: { error in
               // Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
            }

        
    }
}
