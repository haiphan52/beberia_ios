//
//  VoucherTableViewCell.swift
//  Beberia
//
//  Created by Lap on 08/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit

class VoucherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupData(gift: MyGiftModel){
        lblDesc.text = gift.giftCode
        lblNote.text = gift.gift?.giftDescription
        lblDesc.text = gift.gift?.name
        if let text = gift.gift?.image, let url = URL.init(string: text) {
            imgProduct.setImage(url: url)
        }
    }
    
}
