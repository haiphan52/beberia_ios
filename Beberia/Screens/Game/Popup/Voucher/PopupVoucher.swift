//
//  PopupVoucher.swift
//  Beberia
//
//  Created by Lap on 08/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PopupVoucher: UIView {

    @IBOutlet weak var tbvVoucher: UITableView!
    
    var nextPage = 1
    
    var myGifts = BehaviorRelay.init(value: [MyGiftModel]())
    var disposeBag = DisposeBag()
   
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupVoucher.firstView(owner: nil)
    }

    class func instanceFromNib() -> PopupVoucher {

        return R.nib.popupVoucher.instantiate(withOwner: nil)[0] as! PopupVoucher
    }
    
    override func awakeFromNib() {
        
        getListGift()
        
        tbvVoucher.showsVerticalScrollIndicator = false
        tbvVoucher.register(R.nib.voucherTableViewCell)
        tbvVoucher.tableFooterView = UIView()
        tbvVoucher.addInfiniteScroll { (tableView) in
            if self.nextPage != 0{
                self.getListGift()
            }else{
                self.tbvVoucher.endRefreshing(at: .bottom)
                self.tbvVoucher.finishInfiniteScroll(completion: { (collection) in
                })
            }
        }
        
        myGifts.asObservable().observeOn(MainScheduler.asyncInstance)
            .bind(to: tbvVoucher.rx.items(cellIdentifier: R.reuseIdentifier.voucherTableViewCell.identifier, cellType: VoucherTableViewCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                cell.setupData(gift: data)
        }.disposed(by: disposeBag)
        
//        tbvVoucher.rx.modelSelected(VaccineModel.self).subscribe { (value) in
//          
//        }.disposed(by: disposeBag)
    }

}

extension PopupVoucher {
    func getListGift(){
        APIManager.myListReceived(game_id: UserInfo.shareUserInfo.game.id ?? 0, page: nextPage) { myGifts, total, nextPage in
            self.nextPage = nextPage
            var myListTemp = self.myGifts.value
            myListTemp.append(contentsOf: myGifts)
            self.myGifts.accept(myListTemp)
            
            self.tbvVoucher.endRefreshing(at: .bottom)
            self.tbvVoucher.finishInfiniteScroll(completion: { (collection) in
            })
            
        } failed: { error in
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
        }

    }
}
