//
//  PopupGifGame.swift
//  Beberia
//
//  Created by Lap on 07/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import FFPopup

class PopupGifGame: UIView {

    
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var greenImg: UIImageView!
    @IBOutlet weak var lblButton: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDes: UILabel!
    @IBOutlet weak var lbGuide: UILabel!
    @IBOutlet weak var icGame: UIImageView!
    
 //   var giftModel: RingGiftModel?
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupGifGame.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> PopupGifGame {
      
        return R.nib.popupGifGame.instantiate(withOwner: nil)[0] as! PopupGifGame
    }
    
    func showData(giftModel: RingGiftModel){
        
        lblButton.text = "Tiếp tục tham gia(\(UserInfo.shareUserInfo.total_invite))"
        
        if UserInfo.shareUserInfo.total_invite == 0 {
            greenImg.image = R.image.greenButton1()
        } else {
            greenImg.image = R.image.img_bg_popup_gift2()
        }
        
        switch giftModel.statusGift() {
        case .voucher, .gift:
            lbTitle.text = "QUÀ HIỆN VẬT"
            lbDes.text = "Các mom vào TIỆN ÍCH > Trang cá nhân > Ảnh đại diện > Cập nhật ảnh và thông tin Họ tên, SĐT, địa chỉ để sau khi Big game kết thúc Beberia sẽ gửi quà cho các mom theo địa chỉ đã cập nhật\nMọi thắc mắc vui lòng inbox fanpage Beberia để được hỗ trợ kịp thời "
            lbGuide.text = "Hướng dẫn nhận quà"
            icGame.image = R.image.ic_game_voucher()
        case .point: break
        }
        
        lblCode.text = giftModel.code
        lblNote.text = giftModel.gameDescription
        lblDesc.text = giftModel.name
        guard let url = URL.init(string: giftModel.image ) else {
            return
        }
        imgProduct.setImage(url: url)
        
        
    }
    
    override func awakeFromNib() {
        lblNote.adjustsFontSizeToFitWidth = true
    }
    
    @IBAction func didPressNext(_ sender: Any) {
        FFPopup.dismiss(contentView: self, animated: true)
    }
    
    @IBAction func didPressContineu(_ sender: Any) {
        
        
        FFPopup.dismiss(contentView: self, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
            Utils.getTopMostViewController()?.dismiss(animated: true, completion: {
                
            })
        }
    }
}
