//
//  PopupInvite.swift
//  Beberia
//
//  Created by Lap on 14/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class PopupInvite: UIView {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tbvInvite: UITableView!
    
    var listUser = BehaviorRelay.init(value: [GameUser]())
    var disposeBag = DisposeBag()
    var page = 1
    var nextPage = 1
    var total = 0
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupInvite.firstView(owner: nil)
    }

    class func instanceFromNib() -> PopupInvite {

        return R.nib.popupInvite.instantiate(withOwner: nil)[0] as! PopupInvite
    }
    
    override func awakeFromNib() {
        
        getListInvite()
        
        
        tbvInvite.rowHeight = 50
        tbvInvite.showsVerticalScrollIndicator = false
        tbvInvite.register(R.nib.popupInviteSuccessCell)
        tbvInvite.tableFooterView = UIView()
        tbvInvite.addInfiniteScroll { (tableView) in
            if self.nextPage != 0{
                self.getListInvite()
            }else{
                self.tbvInvite.endRefreshing(at: .top)
                self.tbvInvite.finishInfiniteScroll(completion: { (collection) in
                })
            }
        }
        
        listUser.asObservable().observe(on: MainScheduler.asyncInstance)
            .bind(to: tbvInvite.rx.items(cellIdentifier: R.reuseIdentifier.popupInviteSuccessCell.identifier, cellType: PopupInviteSuccessCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                cell.lbNo.text = "\(row + 1)"
                cell.setValueImageText(value: data)
//                if row == 0 {
//                    cell.lblStt.text = "STT"
//                    cell.lblName.text = "TÊN TRÊN APP"
//                } else{
//                    cell.lblStt.text = "\(row)"
//                    cell.lblName.text = data.displayName
//                }
                
        }.disposed(by: disposeBag)
        
//        tbvInvite.rx.modelSelected(VaccineModel.self).subscribe { (value) in
//          
//        }.disposed(by: disposeBag)
    }
}

extension PopupInvite {
    
    func getListInvite(){
        APIManager.listInvite(page: self.nextPage) { gameUsers, nextPage, total in
            self.lblTitle.text = "Giới thiệu thành công (\(total))"
           
           
            var myListTemp = self.listUser.value
            var user = gameUsers
            
//            if self.nextPage == 1 {
//                user.insert(GameUser.init(id: 0, avatar: "", displayName: ""), at: 0)
//            }

            myListTemp.append(contentsOf: user)
            self.listUser.accept(myListTemp)
            
            self.nextPage = nextPage
            
            
            self.tbvInvite.endRefreshing(at: .top)
            self.tbvInvite.finishInfiniteScroll(completion: { (collection) in
            })
        } failed: { error in
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
        }

    }
}
