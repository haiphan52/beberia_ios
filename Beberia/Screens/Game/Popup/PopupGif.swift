//
//  PopupGif.swift
//  Beberia
//
//  Created by Lap on 07/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import FFPopup

class PopupGif: UIView {

    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var heihgtLalbeError: NSLayoutConstraint!
    @IBOutlet weak var lblError: UILabel!
 
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupGif.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> PopupGif {
      
        return R.nib.popupGif.instantiate(withOwner: nil)[0] as! PopupGif
    }
    
    override func awakeFromNib() {
        lblError.isHidden = true
        heihgtLalbeError.constant = 25
    }

    @IBAction func didPressGo(_ sender: Any) {
        handleJoinGame()
    }
    
    @IBAction func didPressOK(_ sender: Any) {
        handleJoinGame()
    }
    
    func handleJoinGame(){
        APIManager.inviteCode(code: tfCode.text ?? "") { codeStatus in
            print(codeStatus)
            if codeStatus == 200 {
                
                FFPopup.dismissAllPopups()
                let deadlineTime = DispatchTime.now() + .milliseconds(200)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
//                    let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
//                    gameVC.hidesBottomBarWhenPushed = true
//                    Utils.getTopMostViewController()?.navigationController?.pushViewController(gameVC, animated: true)
                    
                    let gameVC = GameViewController.init(nib: R.nib.gameViewController)
                    gameVC.hidesBottomBarWhenPushed = true
                    Utils.getTopMostViewController()?.present(gameVC, animated: true, completion: nil)
                }
                
            } else if codeStatus == 400{
                self.lblError.isHidden = false
                self.heihgtLalbeError.constant = 40
            } else {
                Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: "\(codeStatus)")
            }
            
        } failed: { error in
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
        }
    }
}
