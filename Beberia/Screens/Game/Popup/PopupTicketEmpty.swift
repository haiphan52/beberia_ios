//
//  PopupTicketEmpty.swift
//  Beberia
//
//  Created by Lap on 16/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import UIKit
import FFPopup

class PopupTicketEmpty: UIView {

    @IBOutlet weak var lblNumberTicket: UILabel!
    
    @IBAction func btnContine(_ sender: Any) {
        Utils.getTopMostViewController()?.dismiss(animated: true, completion: {
            
        })
    }
    
    override func awakeFromNib() {
        lblNumberTicket.text = "Ticket còn lại : (0)"
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupTicketEmpty.firstView(owner: nil)
    }

    class func instanceFromNib() -> PopupTicketEmpty {

        return R.nib.popupTicketEmpty.instantiate(withOwner: nil)[0] as! PopupTicketEmpty
    }
}
