//
//  PopupShare.swift
//  Beberia
//
//  Created by Lap on 08/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit

class PopupShare: UIView {

    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupShare.firstView(owner: nil)
    }

    class func instanceFromNib() -> PopupShare {

        return R.nib.popupShare.instantiate(withOwner: nil)[0] as! PopupShare
    }
}
