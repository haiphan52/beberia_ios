//
//  PopupGif.swift
//  Beberia
//
//  Created by Lap on 07/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit

class PopupGuide: UIView {
    
    @IBOutlet weak var setpOne: UILabel!
    
    @IBOutlet weak var stepTwo: UILabel!
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupGuide.firstView(owner: nil)
    }

    class func instanceFromNib() -> PopupGuide {

        return R.nib.popupGuide.instantiate(withOwner: nil)[0] as! PopupGuide
    }
    
    //
    
    override func awakeFromNib() {
        setpOne.attributedText =
            NSMutableAttributedString()
                .bold("Bước 1: ")
                .normal("Tải ứng dụng Beberia và đăng ký tài khoản theo hướng dẫn (Nếu đã đăng ký có thể bỏ qua bước này)")
        
        
        stepTwo.attributedText =
            NSMutableAttributedString()
                .bold("Bước 2: ")
                .normal("Chia sẻ mã giới thiệu cho bạn bè.")
              
    }

}

extension NSMutableAttributedString {
    var fontSize:CGFloat { return 14 }
    var boldFont:UIFont { return R.font.notoSansSemiBold(size: fontSize)! }
    var normalFont:UIFont { return R.font.notoSansRegular(size: fontSize)!}
    
    func boldNotoSan(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normalNotoSan(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : normalFont,
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    /* Other styling methods */
    func orangeHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func blackHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}
