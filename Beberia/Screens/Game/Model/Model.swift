//
//  Model.swift
//  Beberia
//
//  Created by  Lap on 09/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation

// MARK: - Game
struct GiftModel: Codable {
    let name: String
    let image: String
}

// MARK: - Game
struct ListMyGiftModel: Codable {
    let datas: [MyGiftModel]
    let nextPage: Int

    enum CodingKeys: String, CodingKey {
        case datas
        case nextPage = "next_page"
    }
}

// MARK: - DataElement
struct MyGiftModel: Codable {
    var giftCode: String?
    var gift: Gift?
    var user : GameUser?
    var createdAt: Int?

    enum CodingKeys: String, CodingKey {
        case giftCode = "gift_code"
        case gift, user
        case createdAt = "created_at"
    }
}

// MARK: - Gift
struct Gift: Codable {
    let type: Int
    let name: String
    let image: String
    let id: Int
    let giftDescription: String

    enum CodingKeys: String, CodingKey {
        case type, name, image, id
        case giftDescription = "description"
    }
}



struct RingGiftModel: Codable {
    
    enum GiftType: Int {
        case voucher = 1, gift, point
        
        static func status(type: Int) -> Self {
            guard let type = GiftType(rawValue: type) else {
                return .voucher
            }
            return type
        }
    }
    
    let id: Int
    let name: String
    let image: String
    let gameDescription, code: String
    let totalUsed, number, type: Int

    enum CodingKeys: String, CodingKey {
        case id, name, image
        case gameDescription = "description"
        case code
        case totalUsed = "total_used"
        case number, type
    }
    
    func statusGift() -> GiftType {
        return GiftType.status(type: type)
    }
}

// MARK: - Game
struct GameUser: Codable, BaseModelImageText {
    var text: String? {
        return self.displayName
    }
    
    var img: String? {
        return self.avatar
    }
    
    let id: Int?
    let avatar: String?
    let displayName: String?
    
    enum CodingKeys: String, CodingKey {
        case id, avatar
        case displayName = "display_name"
    }
}



