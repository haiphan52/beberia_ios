//
//  ResultGameCollectionViewCell.swift
//  Beberia
//
//  Created by Lap on 09/07/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import FSPagerView

class ResultGameCollectionViewCell: FSPagerViewCell {

    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblNameProduct: UILabel!
    @IBOutlet weak var lblNameUser: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
