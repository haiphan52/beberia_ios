//
//  GameViewController.swift
//  Beberia
//
//  Created by Lap on 18/06/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import Kingfisher
import FSPagerView
import AVFoundation

class GameViewController: BaseViewController {
    
    var gameActive = false
    var score = 0
    var timeOfLastShake = 0
    var timer = Timer()
    var listUserREceived = [MyGiftModel]()
    var player: AVAudioPlayer?
    
    @IBOutlet weak var lblTicket: UILabel!
    @IBOutlet weak var animationShake: AnimatedImageView!
    @IBOutlet weak var fsPageView: FSPagerView!
    @IBOutlet weak var animationGif: AnimatedImageView!
    @IBOutlet weak var imgGif: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playSound()
        setupSlideShow()
        listReceived()
        
        let path = Bundle.main.url(forResource: "Icon-shake3", withExtension: "gif")!
        let resource = LocalFileImageDataProvider(fileURL: path)
        self.animationShake.kf.setImage(with: resource)
        
        lblTicket.text = "Ticket của tôi (\(UserInfo.shareUserInfo.total_invite))"

    }
    
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "[no copyright music] jazz me blues cute background music (1)", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            player.numberOfLoops  = -1
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func setupSlideShow(){
        fsPageView.scrollDirection = .vertical
        fsPageView.dataSource = self
        fsPageView.delegate = self
        fsPageView.automaticSlidingInterval = 3.5
        fsPageView.interitemSpacing = 40
        fsPageView.register(UINib(nibName: "ResultGameCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ResultGameCollectionViewCell")
        fsPageView.itemSize = CGSize(width: (UIScreen.main.bounds.width - 60), height: 50)
        fsPageView.transformer = FSPagerViewTransformer(type: .cubic)
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            self.shakeView(viewToShake: imgGif)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                self.getGiftRing()
            }
        }
    }
    
    func shakeView(viewToShake: UIView){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.1
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: viewToShake.center.x - 15, y: viewToShake.center.y ))
        animation.toValue = NSValue(cgPoint: CGPoint(x: viewToShake.center.x + 15, y: viewToShake.center.y ))
        
        viewToShake.layer.add(animation, forKey: "position")
        
        
    }
    
    @IBAction func didPressTicket(_ sender: Any) {
        
    }
    
    @IBAction func didPressMyGift(_ sender: Any) {
        let view = PopupVoucher.instanceFromNib()
        let _ = self.showPopup(view: view, height: 400) 
    }
    
    @IBAction func didPressBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: FSPagerViewDelegate
extension GameViewController: FSPagerViewDataSource,FSPagerViewDelegate{
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.listUserREceived.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "ResultGameCollectionViewCell", at: index) as! ResultGameCollectionViewCell
        
        cell.contentView.layer.shadowRadius = 0
        cell.contentView.layer.shadowOpacity = 0
        
        cell.imgAvatar.setImage(imageString: self.listUserREceived[index].user?.avatar ?? "")
        cell.lblNameProduct.text = self.listUserREceived[index].gift?.name
        cell.lblNameUser.text = self.listUserREceived[index].user?.displayName
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        
    }
}

extension GameViewController {
    func getGiftRing(){
        APIManager.ringGift(game_id: UserInfo.shareUserInfo.game.id ?? 0) { giftRingModel in
            
            
            let path = Bundle.main.url(forResource: "confetti-GIF", withExtension: "gif")!
            let resource = LocalFileImageDataProvider(fileURL: path)
            self.animationGif.kf.setImage(with: resource)
            
            UserInfo.shareUserInfo.total_invite = UserInfo.shareUserInfo.total_invite - 1
            
            if UserInfo.shareUserInfo.total_invite < 0 {
                UserInfo.shareUserInfo.total_invite = 0
            }
            self.lblTicket.text = "Ticket của tôi (\(UserInfo.shareUserInfo.total_invite))"
            
            let view = PopupGifGame.instanceFromNib()
            view.showData(giftModel: giftRingModel)
            let _ = self.showPopup(view: view, height: 550)
            
            
            NotificationCenter.default.post(name: NSNotification.Name.updateNumberTicket, object: nil, userInfo: nil)
            
        } failed: { error in
            
            if error == "Bạn đã hết lượt" {
                let view = PopupTicketEmpty.instanceFromNib()
                let _ = self.showPopup(view: view, height: 120)
            } else {
                Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
            }
        }
    }
    
    func listReceived(){
        APIManager.listReceived(game_id: UserInfo.shareUserInfo.game.id ?? 0) { [weak self] gifts in
            guard let wSelf = self else { return }
            wSelf.listUserREceived = gifts
            wSelf.fsPageView.reloadData()
        } failed: { error in
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
        }
    }
}

