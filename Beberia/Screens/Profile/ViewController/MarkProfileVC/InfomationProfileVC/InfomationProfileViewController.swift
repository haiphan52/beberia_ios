//
//  InfomationProfileViewController.swift
//  Beberia
//
//  Created by OS on 9/19/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import PullToRefresh

class InfomationProfileViewController: BaseViewController {
    //MARK: Outlet
    @IBOutlet weak var tbvListInfo: UITableView!
    
    //MARK: Properties
    var gettype = ""
    var page = 1
    var nextpage = 0
    let refresher = PullToRefresh()
    var listInfoMark = [PostDetailModel]()
    var isLoadingMore = false
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getInfoMark(type: gettype, page: page)
        // Do any additional setup after loading the view.
    }
    
    //MARK: Setup UI
    func setupUI(){
        tbvListInfo.register(R.nib.detailInfoTableViewCell)
        tbvListInfo.delegate = self
        tbvListInfo.dataSource = self
        tbvListInfo.separatorStyle = .none
        customLeftBarButton()
    //    tbvListInfo.infiniteScrollTriggerOffset = 700
        
        tbvListInfo.showsVerticalScrollIndicator = false
        tbvListInfo.addInfiniteScroll { (tableView) in
            tableView.performBatchUpdates({ () -> Void in
                // update collection view
               self.isLoadingMore = true
                if self.nextpage > 0 {
                    self.page = self.page + 1
                    self.getInfoMark(type: self.gettype, page: self.page)
                }else{
                    self.tbvListInfo.finishInfiniteScroll(completion: { (collection) in
                    })
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                tableView.finishInfiniteScroll()
            });
            
        }
        
        tbvListInfo.addPullToRefresh(refresher) {
            self.isLoadingMore = true
            self.page = 1
            self.getInfoMark(type: self.gettype, page: self.page)
            self.tbvListInfo.startRefreshing(at: .top)
        }
    }
    
    //MARK: Request API
    func getInfoMark(type: String,page:Int){
        if !isLoadingMore{
            SVProgressHUD.show()
        }
        APIManager.getListInfoFavorite(type: type, page: page, callbackSuccess: { [weak self] (infoMark, next_page) in
            guard let self = self else { return }
            self.nextpage = next_page
            if self.page == 1{
                self.listInfoMark.removeAll()
            }
            if self.listInfoMark.count == 0 {
                self.listInfoMark = infoMark
            }else{
                self.listInfoMark.append(contentsOf: infoMark)
            }
            self.tbvListInfo.reloadData()
            self.tbvListInfo.endRefreshing(at: .top)
            self.tbvListInfo.finishInfiniteScroll(completion: { (collection) in
            })
            SVProgressHUD.dismiss()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        }
    }
    
    func deleteMark(indext:Int){
        self.showActionBottom(datasource: [R.string.localizable.homeDelete()], isNoTextColorRed: false) { (int) in
            APIManager.favouritePost(idPost: self.listInfoMark[indext].id ?? 0, callbackSuccess: { [weak self] (isSuccess) in
                guard let self = self else { return }
                self.listInfoMark.remove(at: indext)
                self.tbvListInfo.reloadData()
                
            }) { (error) in
                Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
            }
        }
    }
    
}

//MARK: UITableview Delegate
extension InfomationProfileViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listInfoMark.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.detailInfoTableViewCell, for: indexPath)!
        cell.setupDataMore(listDetailMore: self.listInfoMark[indexPath.row])
        cell.widthBtnOption.constant = 34
        cell.imgDelete.isHidden = false
        cell.btnDelete.isHidden = false
        cell.btnDelete.isHidden = true
        if self.gettype == "2" {
            cell.btnDelete.isHidden = false
            cell.imgDelete.isHidden = false
        }
        
        cell.btnDelete.tag = indexPath.row
        cell.tapDeleteAlert = { index in
            self.deleteMark(indext: index)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DetailInfoTableViewCell.heightRow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let postVC = PostDetailViewController.init(nibName: "PostDetailViewController", bundle: nil)
//        postVC.hidesBottomBarWhenPushed = true
//        postVC.idPost = listInfoMark[indexPath.row].id ?? 0
//        Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
        
        let idPost = listInfoMark[indexPath.row].id ?? 0
        self.pushPostDetail(array: listInfoMark, idPost: idPost, indexPath.row) { (posts) in
            self.listInfoMark = posts
            self.tbvListInfo.reloadData()
        }
    }
}
