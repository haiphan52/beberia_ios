//
//  DiaryProfileViewController.swift
//  Beberia
//
//  Created by iMAC on 9/7/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import PullToRefresh
import RxCocoa
import RxSwift
import RxDataSources

class DiaryProfileViewController: BaseViewController {
    
    //MARK: Outlet
//    @IBOutlet weak var tbvDiary: UITableView!
    @IBOutlet weak var clvDiary: UICollectionView!
    
    //MARK: Properties
    let sections = BehaviorRelay.init(value: [MenuSectionCLV]())
    var dataSource:RxCollectionViewSectionedAnimatedDataSource<MenuSectionCLV>?
    var listDiaryNew = BehaviorRelay.init(value: [Diaries]())
    
    var listFavoriteDiary = [Diaries]()
    var gettype = ""
    var page = 1
    var nextpage = 0
    let refresher = PullToRefresh()
    var isLoadingMore = false
    var isCheckLoadding = false
    let heightImageClv = (UIScreen.main.bounds.width / 2) - 32 + 156
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      //  setupUI()
        initRX()
        getlistFavorite(type: gettype, page: page)
        // Do any additional setup after loading the view.
       // NotificationCenter.default.addObserver(self, selector: #selector(checkEditDiarySave), name: NSNotification.Name.checkEditDiarySave, object: nil)
    }
    
//    //MARK: Reques API
//    @objc func checkEditDiarySave(notification: Notification){
//        guard let object = notification.object as? [String:Any] else {
//            return
//        }
//        let idFeed = object[Key.KeyNotification.idDiary] as? Int
//        self.getCheckCompleteDiary(id: idFeed ?? 0, completion: { diary in
//            let index = self.listFavoriteDiary.firstIndex(where: { (diaryEdit) -> Bool in
//                return diary.id == diaryEdit.id
//            })
//            if index != nil{
//                self.listFavoriteDiary[index!] = diary
//            }
//            self.tbvDiary.reloadData()
//        })
//    }
    
//    //MARK: Setup UI
//    func setupUI(){
//        self.title = R.string.localizable.diaryDiary()
//        tbvDiary.register(R.nib.diaryNewTableViewCell)
//        tbvDiary.delegate = self
//        tbvDiary.dataSource = self
//        tbvDiary.separatorStyle = .none
//        customLeftBarButton()
//    //    tbvDiary.infiniteScrollTriggerOffset = 700
//        tbvDiary.showsVerticalScrollIndicator = false
//        tbvDiary.addInfiniteScroll { (tableView) in
//            tableView.performBatchUpdates({ () -> Void in
//                // update collection view
//                self.isLoadingMore = true
//                if self.nextpage > 0 {
//                    self.page = self.page + 1
//                    self.getlistFavorite(type: self.gettype, page: self.page)
//                }else {
//                    self.tbvDiary.finishInfiniteScroll(completion: { (collection) in
//                    })
//                }
//            }, completion: { (finished) -> Void in
//                // finish infinite scroll animations
//                tableView.finishInfiniteScroll()
//            });
//        }
//
//        tbvDiary.addPullToRefresh(refresher) {
//            self.isLoadingMore = true
//            self.page = 1
//            self.getlistFavorite(type: self.gettype, page: self.page)
//            self.tbvDiary.startRefreshing(at: .top)
//        }
//    }
    
    private func initRX(){
        // MARK: // init CollectionView
        let flowLayoutAfterBefore = UICollectionViewFlowLayout()
        flowLayoutAfterBefore.itemSize = CGSize(width: (UIScreen.main.bounds.width / 2) - 8, height: heightImageClv)
        flowLayoutAfterBefore.scrollDirection = .vertical
        flowLayoutAfterBefore.minimumInteritemSpacing = 0
        flowLayoutAfterBefore.minimumLineSpacing = 0
        flowLayoutAfterBefore.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvDiary.setCollectionViewLayout(flowLayoutAfterBefore, animated: true)
        clvDiary.showsHorizontalScrollIndicator = false
        clvDiary.register(R.nib.diaryNewCollectionViewCell)
        
        clvDiary.addInfiniteScroll { (tableView) in
            if self.page != 0{
              //  self.page += 1
                self.getlistFavorite(type: self.gettype, page: self.page)
            }else{
                // self.tableView.endRefreshing(at: .top)
                tableView.finishInfiniteScroll(completion: { (collection) in
                })
            }
        }
        
//        listDiaryNew.asObservable().bind(to: self.clvDiary.rx.items(cellIdentifier: R.reuseIdentifier.diaryNewCollectionViewCell.identifier, cellType: DiaryNewCollectionViewCell.self)) { [weak self]  row, data, cell in
//            guard let _ = self else { return }
//
//            cell.setupData(data: data)
//            cell.contentView.layer.shadowRadius = 0
//            cell.contentView.layer.shadowOpacity = 0
//
//        }.disposed(by: disposeBag)

        // bind to data TableView
        let dataSource = RxCollectionViewSectionedAnimatedDataSource<MenuSectionCLV>(
            
            configureCell: { ds, tv, ip, item in
                let cell = tv.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.diaryNewCollectionViewCell.identifier, for: ip)
                    as! DiaryNewCollectionViewCell
                cell.setupData(data: item)
                            cell.contentView.layer.shadowRadius = 0
                            cell.contentView.layer.shadowOpacity = 0
                return cell
        })
       
        self.dataSource = dataSource
        
        sections
            .bind(to: clvDiary.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        Observable.zip(clvDiary.rx.modelSelected(Diaries.self), clvDiary.rx.itemSelected).subscribe { (diary, item) in
            if Utils.checkLogin(selfView: self) {
                Router.share.pushToViewDiaryDetail(tableView: UITableView.init(), array: self.listDiaryNew.value, indexPath: IndexPath.init(),row: item.row, viewSelf: self, idFeed: diary.id ?? 0, editFromView: .Profile) { (diary) in
                    self.listDiaryNew.accept(diary)
                }
            }
        }.disposed(by: disposeBag)
    }
    
    func getlistFavorite(type: String, page:Int){
        if !isCheckLoadding{
            SVProgressHUD.show()
        }
        APIManager.getListDiaryFavorite(type: type, page: page, callbackSuccess: { [weak self] (diarysMore,next_page) in
            guard let self = self else { return }
            if self.page != 0 {
                var listDiaryNewTemp = self.listDiaryNew.value
                listDiaryNewTemp.append(contentsOf: diarysMore)
                self.listDiaryNew.accept(listDiaryNewTemp)
                self.sections.accept([MenuSectionCLV.init(header: "", items: self.listDiaryNew.value)])
            }
            self.page = next_page
            self.isCheckLoadding = self.page != 1
            
            self.clvDiary.endRefreshing(at: .top)
            self.clvDiary.finishInfiniteScroll(completion: { (collection) in
            })
            SVProgressHUD.dismiss()
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        }
    }
    
//    func getCheckCompleteDiary(id: Int){
//        self.showNotiCreatePost()
//        let timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { (timer) in
//            APIManager.checkCompletedDiary(id: id, callbackSuccess: { (diary, status) in
//                if status == 1{
//                    let index = self.listFavoriteDiary.firstIndex(where: { (diaryEdit) -> Bool in
//                        return diary.id == diaryEdit.id
//                    })
//                    if index != nil{
//                        self.listFavoriteDiary[index!] = diary
//                    }
//
//                    self.tbvDiary.reloadData()
//                    timer.invalidate()
//                    self.dismissPopup()
//                }
//            }) { (error) in
//                self.dismissPopup()
//                //   Utils.showAlertView(controller: self, title: "Error", message: error)
//            }
//        }
//    }
    
//    func deleteMark(indext:Int){
//        self.showActionBottom(datasource: [R.string.localizable.homeDelete()], isNoTextColorRed: false) { (int) in
//            APIManager.favouritePost(idPost: self.listFavoriteDiary[indext].id ?? 0, callbackSuccess: { [weak self] (isSuccess) in
//                guard let self = self else { return }
//                self.listFavoriteDiary.remove(at: indext)
//                self.tbvDiary.reloadData()
//
//            }) { (error) in
//                Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
//            }
//        }
//    }
    
    //MARK: Deinit
    deinit {
        print("Remove NotificationCenter Deinit")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.checkEditDiarySave, object: nil)
    }

}

////MARK: UITableview Delegate
//extension DiaryProfileViewController: UITableViewDelegate,UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return listFavoriteDiary.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.diaryNewTableViewCell, for: indexPath)!
//        cell.setupDataMore(listDiaryMore: self.listFavoriteDiary[indexPath.row])
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return DiaryNewTableViewCell.heightRow
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        guard let selfStrong = Utils.getTopMostViewController() else {
//            return
//        }
//        Router.share.pushToViewDiaryDetail(tableView: tableView, array: self.listFavoriteDiary, indexPath: indexPath,row: indexPath.row, viewSelf: selfStrong, idFeed: self.listFavoriteDiary[indexPath.row].id ?? 0, editFromView: .Save) { (diary) in
//            self.listFavoriteDiary = diary
//            self.tbvDiary.reloadData()
//        }
//    }
//}
