//
//  InfoMarkProfileViewController.swift
//  Beberia
//
//  Created by OS on 9/10/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class InfoMarkProfileViewController: BaseViewController, CustomSegmentedControlDelegate {
    
    // MARK: - CustomSegmentedControlDelegate
       
       func changeToIndex(index: Int) {
           print(index)
           pageMenu?.moveToPage(index)
       }
    
    //MARK: Outlet
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var customSegmetnView: CustomSegmentView!
      {
          didSet{
              customSegmetnView.setButtonTitles(buttonTitles: [R.string.localizable.profileTitlePost(),R.string.localizable.profileTitleDiary(), R.string.localizable.profileTitleInfomation()])
              customSegmetnView.selectorViewColor = customSegmetnView.selectorTextColor
              customSegmetnView.selectorTextColor = customSegmetnView.selectorTextColor
              customSegmetnView.delegate = self
          }
      }
    
    //MARK: Properties
    var PostVC: PostProfileViewController!
    var listDiaryProfile: DiaryProfileViewController!
    var listInfoProfile: InfomationProfileViewController!
    var pageMenu : CAPSPageMenu?
    var test:CGFloat = 0
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupPageMenu()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
 //       setupPageMenu()
    }
    
    //MARK: Setup UI
    func setupUI(){
        self.title = R.string.localizable.profileTitleMark()
        customLeftBarButton()
    }
    
    func setupPageMenu(){
        var controllerArray : [UIViewController] = []
        PostVC = PostProfileViewController.init(nib: R.nib.postProfileViewController)
        PostVC.title = R.string.localizable.profileTitlePost()
        PostVC.gettype = 2
        controllerArray.append(PostVC)
        listDiaryProfile = DiaryProfileViewController.init(nib: R.nib.diaryProfileViewController)
        listDiaryProfile.title = R.string.localizable.profileTitleDiary()
        listDiaryProfile.gettype = "1"
        controllerArray.append(listDiaryProfile)
        listInfoProfile = InfomationProfileViewController.init(nib: R.nib.infomationProfileViewController)
        listInfoProfile.title = R.string.localizable.profileTitleInfomation()
        listInfoProfile.gettype = "2"
        controllerArray.append(listInfoProfile)
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor.init(hexString: AppColor.colorBGMenu, alpha: 1.0)!),
            .selectionIndicatorColor(UIColor.init(hexString: AppColor.colorYellowDark, alpha: 1.0)!),
            .menuItemFont(UIFont.init(name: AppFont.HelveticaNeueBold, size: 16)!),
            .unselectedMenuItemLabelColor(UIColor.init(hexString: AppColor.colorBlack, alpha: 0.5)!),
            .selectedMenuItemLabelColor(UIColor.black),
            .menuHeight(0),
            .menuItemSeparatorWidth(0),
            .selectionIndicatorHeight(3),
            .menuItemWidth(80),
            .enableHorizontalBounce(false),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .centerMenuItems(true)
        ]

        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: view.frame.width, height: containerView.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        containerView.addSubview(pageMenu!.view)
        
    }
    

  
}

//MARK: CAPSPageMenuDelegate
extension InfoMarkProfileViewController: CAPSPageMenuDelegate{
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        customSegmetnView.setIndex(index: index)
    }
    
}
