//
//  ChangePassProfileVC.swift
//  Beberia
//
//  Created by OS on 9/12/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftKeychainWrapper
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class ChangePassProfileVC: BaseViewController {
    //MARK: Outlet
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var viewPass: UIView!
    @IBOutlet weak var btnPass: UIButton!
    @IBOutlet weak var checkPass: UIImageView!
    @IBOutlet weak var lblPass: UILabel!
    @IBOutlet weak var txtNewPass: UITextField!
    @IBOutlet weak var btnNewPass: UIButton!
    @IBOutlet weak var checkNewPass: UIImageView!
    @IBOutlet weak var viewNewPass: UIView!
    @IBOutlet weak var lblNewPass: UILabel!
    @IBOutlet weak var txtRenewPass: UITextField!
    @IBOutlet weak var btnRenewPass: NSLayoutConstraint!
    @IBOutlet weak var checkRenewPass: UIImageView!
    @IBOutlet weak var viewRenewPass: UIView!
    @IBOutlet weak var lblRenewPass: UILabel!
    @IBOutlet weak var btnChangePass: UIButton!
    
    //MARK: Properties
    var checkedOldPass = true
    var chekedNewPass = true
    var checkedRenewPass = true
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: Setup UI
    func setupUI(){
        self.title = R.string.localizable.settingChangePass()
        btnChangePass.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 20)
        customLeftBarButton()
    }
    
    //MARK: Action
    @IBAction func btnShowOldPass(_ sender: UIButton) {
        if checkedOldPass {
            sender.setImage(R.image.ic_SelectedView(), for: .normal)
            checkedOldPass = false
            txtPass.isSecureTextEntry = false
        } else {
            sender.setImage(R.image.ic_View(), for: .normal)
            checkedOldPass = true
            txtPass.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnShowNewPass(_ sender: UIButton) {
        if chekedNewPass {
            sender.setImage(R.image.ic_SelectedView(), for: .normal)
            chekedNewPass = false
            txtNewPass.isSecureTextEntry = false
        } else {
            sender.setImage(R.image.ic_View(), for: .normal)
            chekedNewPass = true
            txtNewPass.isSecureTextEntry = true
        }
    }
    
    @IBAction func btnShowReNewPass(_ sender: UIButton) {
        if checkedRenewPass {
            sender.setImage(R.image.ic_SelectedView(), for: .normal)
            checkedRenewPass = false
            txtRenewPass.isSecureTextEntry = false
        } else {
            sender.setImage(R.image.ic_View(), for: .normal)
            checkedRenewPass = true
            txtRenewPass.isSecureTextEntry = true
        }
    }
    @IBAction func btnChangePass(_ sender: Any) {
        if txtRenewPass.text == txtNewPass.text {
            self.viewRenewPass.backgroundColor = .lightGray
            changePass()
        }else {
            self.viewPass.backgroundColor = .lightGray
            self.checkPass.image = UIImage(named: "")
            self.lblPass.text = ""
            self.lblNewPass.text = R.string.localizable.registerCorrectPass()
            self.checkNewPass.image =  R.image.ic_Close()//UIImage(named: "close-1")
            self.checkRenewPass.image = R.image.ic_Close()//UIImage(named: "close-1")
            self.lblRenewPass.text = R.string.localizable.registerCorrectPass()
            self.viewRenewPass.backgroundColor = .red
        }
    }
    
    //MARK: Request API
    func changePass(){
        SVProgressHUD.show()
        self.viewPass.backgroundColor = .red
        self.viewNewPass.backgroundColor = .red
        self.checkPass.image = R.image.ic_Close()
        self.checkNewPass.image = R.image.ic_Close()
        APIManager.changePassword(old_password: txtPass.text!, new_password: txtNewPass.text!, callbackSuccess: { [weak self] (issucces) in
            guard let self = self else { return }
            self.viewPass.backgroundColor = .lightGray
            self.checkPass.image = R.image.ic_Succes()
            self.lblPass.text = ""
            self.viewNewPass.backgroundColor = .lightGray
            self.checkNewPass.image =  R.image.ic_Succes()
            self.lblNewPass.text = ""
            self.viewRenewPass.backgroundColor = .lightGray
            self.checkRenewPass.image =  R.image.ic_Succes()
            self.lblRenewPass.text = ""

            
            let vc = AlertViewController.init(nibName: "AlertViewController", bundle: nil)
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.dismissCpmpleted = {
                
                
                APIManager.login(user: UserInfo.shareUserInfo.name, providerType: AppSettings.loginType.rawValue, pass: self.txtNewPass.text!, fcmToken: "", callbackSuccess: { [weak self] (user) in
                    guard let self = self else { return }

                    KeychainWrapper.standard.set(UserInfo.shareUserInfo.name, forKey: KeychainKeys.userName.rawValue)
                    KeychainWrapper.standard.set(self.txtNewPass.text!, forKey: KeychainKeys.passWord.rawValue)
                    KeychainWrapper.standard.set(true, forKey: KeychainKeys.isLoggedIn.rawValue)
                    
                    self.navigationController?.popToRootViewController(animated: true)
                    
                }) { (error, statusCode,display_name,password ) in
                    print(error)
                }
                
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//                    Utils.getTopMostViewController()?.dismiss(animated: true) {
//                        // reset data
//                        let defaults = UserDefaults.standard
//                        KeychainWrapper.standard.set(false, forKey: KeychainKeys.isLoggedIn.rawValue)
//                        UIApplication.shared.applicationIconBadgeNumber = 0
//                     //   UserInfo.shareUserInfo = UserInfo()
//                        let loginManager = LoginManager()
//                        loginManager.logOut()
//                        defaults.set("" , forKey: "userIDGG")
//                        defaults.set("" , forKey: "userIDApple")
//                        
//                        defaults.set(false, forKey: "tunrOnOff")
//                        APIManager.login(user: UserInfo.shareUserInfo.name, pass: self.txtNewPass.text!, fcmToken: "", callbackSuccess: { [weak self] (user) in
//                            guard let self = self else { return }
//
//                            KeychainWrapper.standard.set(UserInfo.shareUserInfo.name, forKey: KeychainKeys.userName.rawValue)
//                            KeychainWrapper.standard.set(self.txtNewPass.text!, forKey: KeychainKeys.passWord.rawValue)
//                            KeychainWrapper.standard.set(true, forKey: KeychainKeys.isLoggedIn.rawValue)
//                        }) { (error, statusCode,display_name,password ) in
//                            print(error)
//                        }
//                    }
//                }
                
                
            }
            self.present(vc, animated: true)
            
            SVProgressHUD.dismiss()
        }) { (old_password, new_password,statusCode,mess) in
            if statusCode == 414 {
                if old_password == "" {
                    self.viewPass.backgroundColor = .lightGray
                    self.checkPass.image = UIImage(named: "")
                }else if new_password == "" {
                    self.viewNewPass.backgroundColor = .lightGray
                    self.checkNewPass.image = UIImage(named: "")
                    self.checkRenewPass.image = UIImage(named: "")
                }
                self.lblNewPass.text = new_password
                self.lblPass.text = old_password
            }else if statusCode == 456 {
                self.lblPass.text = mess
                self.viewNewPass.backgroundColor = .lightGray
                self.lblNewPass.text = new_password
                self.checkPass.image = R.image.ic_Close()//UIImage(named: "close-1")
                self.checkNewPass.image =  R.image.ic_Succes()//UIImage(named: "checked")
                self.checkRenewPass.image =  R.image.ic_Succes()//UIImage(named: "checked")
                self.lblRenewPass.text = ""
                self.viewRenewPass.backgroundColor = .lightGray
            }
            SVProgressHUD.dismiss()
        }
    }
}
