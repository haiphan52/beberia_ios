//
//  AlertViewController.swift
//  Beberia
//
//  Created by OS on 9/12/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {
    
    //MARK: Outlet
    @IBOutlet weak var viewpooup: UIView!
    @IBOutlet weak var btnOk: UIButton!
    
    var dismissCpmpleted: ()->() = {}
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Hide the Navigation Bar

    }
    
    //MARK: SetupUI
    func setupUI(){
        viewpooup.cornerRadius = 10
        btnOk.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 20)
        showAnimate()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    //MARK: Action
    @IBAction func btnOK(_ sender: Any) {
//        UserInfo.shareUserInfo = UserInfo()
//        self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: {
            self.dismissCpmpleted()
        })
//        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    func showAnimate (){
        self.view.transform = CGAffineTransform(scaleX: 1.3,y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0,y: 1.0)
        }
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3,y: 1.3)
            self.view.alpha = 0.0
        }) { (finished : Bool) in
            if (finished){
                self.view.removeFromSuperview()
            }
        }
    }
}
