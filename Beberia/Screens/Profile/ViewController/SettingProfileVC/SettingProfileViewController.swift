//
//  SettingProfileViewController.swift
//  Beberia
//
//  Created by OS on 9/11/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import SwiftKeychainWrapper

struct ObjectKind {
    let title:String
    let content:[String]
    let switchis:[Bool]
}

class SettingProfileViewController: BaseViewController {
    
    //MARK: Properties
    var object = [ObjectKind]()
    
    //MARK: Outlet
    @IBOutlet weak var tbvSetting: UITableView!
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: SetUP UI
    func setupUI(){
        self.title = R.string.localizable.profileTitleSetting()
        tbvSetting.register(R.nib.settingProfileCell)
        tbvSetting.registerHeaderFooterView(R.nib.headerSectionTableViewCell)
        tbvSetting.delegate = self
        tbvSetting.dataSource = self
        tbvSetting.tableFooterView = UIView()
    //    tbvSetting.separatorStyle = .none
   //     let taikhoan = ObjectKind(title: R.string.localizable.settingAccountMe(), content: [R.string.localizable.settingAddressBook(),R.string.localizable.settingChangePass()], switchis: [false,false])
        let taikhoan = ObjectKind(title: R.string.localizable.settingAccountMe(), content: [R.string.localizable.settingChangePass()], switchis: [false])
        let thongbao = ObjectKind(title: R.string.localizable.settingAlert(), content: [R.string.localizable.settingAlertDiscount(),R.string.localizable.settingAlertProductFavor(),R.string.localizable.settingAlertProductStatus()], switchis: [true,true,true])
        let ngonngu =  ObjectKind(title: R.string.localizable.settingLanguage(), content: [R.string.localizable.settingLanguageVN()], switchis: [false,false])
        let chinhsach = ObjectKind(title: R.string.localizable.settingPolicy(), content: [], switchis: [])
        let hotro = ObjectKind(title: R.string.localizable.settingSupport(), content: [], switchis: [])
        let logout = ObjectKind(title: R.string.localizable.settingLogout(), content: [], switchis: [])
        
        if KeychainWrapper.standard.string(forKey: KeychainKeys.userName.rawValue) ?? "" != "" && KeychainWrapper.standard.string(forKey: KeychainKeys.passWord.rawValue) ?? "" != "" {
            object.append(taikhoan)
        }

        object.append(thongbao)
        object.append(ngonngu)
        object.append(chinhsach)
        object.append(hotro)
        object.append(logout)
      
        customLeftBarButton()
    }
    
    func showAlertViewLogout(title : String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Huỷ", style: .default, handler: nil))
        alert.addAction(UIAlertAction.init(title: "Đồng ý", style: .default, handler: { (UIAlertAction) in
             
            
            APIManager.logout(callbackSuccess: { [weak self] (isSucces) in
                guard let self = self else { return }
                
                let defaults = UserDefaults.standard
                KeychainWrapper.standard.set(false, forKey: KeychainKeys.isLoggedIn.rawValue)
                UIApplication.shared.applicationIconBadgeNumber = 0
                UserInfo.shareUserInfo = UserInfo()
                let loginManager = LoginManager()
                loginManager.logOut()
                
                
                defaults.set(false, forKey: "tunrOnOff")
                
                Utils.getAppDelegate().isCheckGetData = false
                
                UserDefault.userIDSocial = ""
                KeychainWrapper.standard.set("", forKey: KeychainKeys.userName.rawValue)
                KeychainWrapper.standard.set("", forKey: KeychainKeys.passWord.rawValue)
                
                // unRegisterDevicePushToken
//                SendBirdManager.share.unRegisterDevicePushToken()
//                
//                // Disconnect SEndBird
//                SBDMain.disconnect(completionHandler: {
//                    // A current user is disconnected from Sendbird server.
//                    Utils.getAppDelegate().isCheckConnectSendbird = false
//                })
                
                
                // logout switch tabbar index == 1
                
            //    self.navigationController?.popToRootViewController(animated: true)
                self.navigationController?.popViewController(animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
                    guard let view = Utils.getTopMostViewController() as? MenuViewController else { return }
                    guard let tabbar  = view.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 1
                }
                
            }, failed: { ( error) in
                print(error)
            })
            
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

//MARK: UITableview Delegate
extension SettingProfileViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return object.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return object[section].content.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.settingProfileCell, for: indexPath)!
        cell.selectionStyle = .none
        cell.lblContent.text = object[indexPath.section].content[indexPath.row]
        cell.switchOnOff.isHidden = false
        if object[indexPath.section].switchis[indexPath.row] == false {
            cell.switchOnOff.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.reuseIdentifier.headerSectionTableviewCell)!
        headerView.btnAction.tag = section
        headerView.lblTitle.font = UIFont.init(name: "\(AppFont.HelveticaNeueBold)", size: 17)
        headerView.lblTitle.text = object[section].title
        if object[section].title == R.string.localizable.settingLogout() {
            
            
            headerView.tapViewLogout = { index in
                self.showAlertViewLogout(title: "Thông báo", message: "Bạn có muốn đăng xuất tài khoản")
            }
        }
        return headerView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let cell:SettingProfileCell = tableView.cellForRow(at: indexPath) as! SettingProfileCell
        if cell.lblContent.text == R.string.localizable.settingChangePass(){
            let postVC =  ChangePassProfileVC.init(nib: R.nib.changePassProfileVC)
            postVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
        }
        if cell.lblContent.text == R.string.localizable.settingAddressBook(){
            let postVC =  AddressProfileVC.init(nib: R.nib.addressProfileVC)
            postVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
        }
    }
}

