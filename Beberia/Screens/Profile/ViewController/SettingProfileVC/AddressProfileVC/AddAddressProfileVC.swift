//
//  AddAddressProfileVC.swift
//  Beberia
//
//  Created by OS on 9/16/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class AddAddressProfileVC: BaseViewController {
    
    //MARK: Outlet
    @IBOutlet weak var viewNR: UIView!
    @IBOutlet weak var viewCty: UIView!
    @IBOutlet weak var btnAddAddress: UIButton!
    @IBOutlet weak var switchOnOff: UISwitch!
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK: Setup UI
    func setupUI() {
        self.title = R.string.localizable.settingAddressBook()
        viewNR.borderWidth = 1
        viewNR.cornerRadius = 10
        viewNR.borderColor = UIColor.init(hexString: AppColor.colorText, alpha: 1.0)!
         btnAddAddress.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 20)
        viewCty.cornerRadius = 10
        switchOnOff.transform = CGAffineTransform(scaleX: 0.75, y: 0.60)
        customLeftBarButton()
    }

    //MARK: Action
    @IBAction func btnNR(_ sender: Any) {
        viewCty.borderWidth = 0
        viewCty.backgroundColor = UIColor.init(hexString: AppColor.colorBGMenu, alpha: 0.4)!
        viewNR.borderWidth = 1
        viewNR.backgroundColor = UIColor.init(hexString: AppColor.colorBGMenu, alpha: 1.0)!
        viewNR.borderColor = UIColor.init(hexString: AppColor.colorText, alpha: 1.0)!
    }
    
    @IBAction func btnCongty(_ sender: Any) {
        viewNR.borderWidth = 0
        viewNR.backgroundColor = UIColor.init(hexString: AppColor.colorBGMenu, alpha: 0.4)!
        viewCty.borderWidth = 1
        viewCty.backgroundColor = UIColor.init(hexString: AppColor.colorBGMenu, alpha: 1.0)!
        viewCty.borderColor = UIColor.init(hexString: AppColor.colorText, alpha: 1.0)!
    }
    
    @IBAction func btnAddAddress(_ sender: Any) {
        
    }
}
