//
//  AddressProfileVC.swift
//  Beberia
//
//  Created by OS on 9/13/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class AddressProfileVC: BaseViewController {
    //MARK: Outlet
    @IBOutlet weak var tbvListAdress: UITableView!
    @IBOutlet weak var btnAddAdress: UIButton!
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK: Setup UI
    func setupUI() {
        self.title = R.string.localizable.settingAddressBook()
        customLeftBarButton()
        tbvListAdress.tableFooterView = UIView()
        btnAddAdress.borderWidth = 1
        btnAddAdress.cornerRadius = 5
        btnAddAdress.borderColor = UIColor.init(hexString: AppColor.Info, alpha: 1.0)!
    }
    
    //MARK Action
    @IBAction func btnAddAdress(_ sender: Any) {
        let postVC =  AddAddressProfileVC.init(nib: R.nib.addAddressProfileVC)
        postVC.hidesBottomBarWhenPushed = true
        Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
    }
    
}
