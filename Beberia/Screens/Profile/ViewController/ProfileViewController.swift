//
//  ProfileViewController.swift
//  Beberia
//
//  Created by iMAC on 9/7/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import Kingfisher
import RxSwift
import RxCocoa

class ProfileViewController: BaseViewController, CustomSegmentedControlDelegate, SetUpFunctionBase {
    
    // MARK: - CustomSegmentedControlDelegate
    
    func changeToIndex(index: Int) {
        print(index)
        pageMenu?.moveToPage(index)
    }
    
    
    //MARK: Outlet
    @IBOutlet weak var viewAcceptCancel: ViewAcceptCancelFollow!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnMark: UIButton!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblBaby: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var viewAva: UIView!
    @IBOutlet weak var customSegmetnView: CustomSegmentView!
    {
        didSet{
            customSegmetnView.setButtonTitles(buttonTitles: [R.string.localizable.profileTitlePost(),R.string.localizable.profileTitleDiary()])
            customSegmetnView.selectorViewColor = customSegmetnView.selectorTextColor
            customSegmetnView.selectorTextColor = customSegmetnView.selectorTextColor
            customSegmetnView.delegate = self
        }
    }
    
    //MARK: Properties
    var PostVC: PostProfileViewController!
    var ListDiaryVC: ListDiaryCLVViewController!
    var pageMenu : CAPSPageMenu?
    var check = 0
    var user = User.init(json: "")
    var id = 0
    var updateStatusUser:(User)->() = {_ in}
    private var dismissTriggerLightBox: Bool = false
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupRX()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadProfileInfo), name: NSNotification.Name.editProfileInfo, object: nil)
        
        // check user is it's me hidden view accept cancel
        print(id)
        if id != 0 && id != UserInfo.shareUserInfo.id {
            getProfile()
        } else {
            checkStatusFriend()
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(150)) {
                self.setupPageMenu()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // check user is it's me hidden view accept cancel
        setupUI()
        print(id)
        if id != 0 && id != UserInfo.shareUserInfo.id{
            getProfile()
        } else {
            checkStatusFriend()
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(150)) {
                if !self.dismissTriggerLightBox {
                    self.setupPageMenu()
                    self.dismissTriggerLightBox = false
                }
            }
        }
    }
    
    deinit {
        print("Remove NotificationCenter Deinit")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.editProfileInfo, object: nil)
    }
    
    func checkStatusFriend(){
        // action accept , cancel
        viewAcceptCancel.isHidden = UserInfo.shareUserInfo.id == user.id
        viewAcceptCancel.checkShowButton(isFollow: user.isFollow ?? 0)
        viewAcceptCancel.userCreate = user
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // setupPageMenu()
    }
    
    //MARK: @objc
    @objc private func reloadProfileInfo(notification: NSNotification){
        setupUI()
    }
    
    //    @objc func tapSetting(){
    //          let postVC =  SettingProfileViewController.init(nib: R.nib.settingProfileViewController)
    //          postVC.hidesBottomBarWhenPushed = true
    //          Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
    //      }
    
    override func backVC() {
        
        if check == 1 {
            self.navigationController?.popViewController(animated: true)
            updateStatusUser(user)
        }else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func moveToEditProfile() {
        let postVC = R.storyboard.main.editProfileInfoViewController()!
        postVC.hidesBottomBarWhenPushed = true
        Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
    }
    
    //MARK: Action
    @IBAction func btnEdit(_ sender: Any) {
        self.moveToEditProfile()
    }
    
    @IBAction func btnMark(_ sender: Any) {
        let postVC = InfoMarkProfileViewController.init(nib: R.nib.infoMarkProfileViewController)
        postVC.hidesBottomBarWhenPushed = true
        Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
    }
    
    //MARK: Setup UI
    func setupUI(){
//        self.title = R.string.localizable.profileTitle()
//        let label = UILabel()
//        label.text = R.string.localizable.profileTitle()
//        label.font = UIFont(name:"\(AppFont.HelveticaNeueBold)", size: 30)
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: label)
        
        customLeftBarButton()
        title = R.string.localizable.profileTitle()
        
        self.navigationController?.navigationBar.isTranslucent = false
//        self.setTitleLarge()
//        let rightBarButtonSetting = UIBarButtonItem.menuButton(self, action: #selector(self.tapSetting), imageName: R.image.ic_Setting() ?? UIImage.init(), height: 25, width: 25)
//        self.navigationItem.rightBarButtonItems = [rightBarButtonSetting]
        if check == 1 {
            self.title = R.string.localizable.profileTitleUser()
            customLeftBarButton()
            self.navigationItem.rightBarButtonItems = nil
            btnEdit.isHidden = true
            btnMark.isHidden = true
        }
        
        imgAva.layer.cornerRadius = imgAva.frame.height/2
        imgAva.clipsToBounds = true
        viewAva.layer.cornerRadius = viewAva.frame.height/2
        viewAva.clipsToBounds = true
        viewAva.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: viewAva.frame.height/2)
        lblName.text = user.displayName ?? UserInfo.shareUserInfo.name
        
        imgAva.kf.setImage(with: URL.init(string: user.avatar ?? UserInfo.shareUserInfo.avartar ) ,placeholder: UIImage.init(named: Key.ImagePlaceholder))
        
        let babyInfo = user.babyInfo ?? UserInfo.shareUserInfo.baby_info
        
        if babyInfo.count > 0 {
            if babyInfo[0].ageRange == 0{
                lblBaby.text = "Đang mang thai"
            }else{
                var gende = ""
                var year = ""
                var month = ""
                if babyInfo[0].gender ?? 0 == 0 {
                    gende = R.string.localizable.profileMale()
                }else {
                    gende = R.string.localizable.profileFemale()
                }
                if babyInfo[0].year ?? 0 > 0 {
                    year = " - \(babyInfo[0].year ?? 0) \(R.string.localizable.profileAge())"
                }
                if babyInfo[0].month ?? 0 > 0 {
                    month = " - \(babyInfo[0].month ?? 0) \(R.string.localizable.profileMonth())"
                }
                lblBaby.text = "\(babyInfo[0].nickname ?? "")\(year)\(month) - \(gende)"
            }
        }else {
            lblBaby.text = "Chưa cập nhật thông tin bé"
        }
    }
    
    func setupRX() {
        let tapViewAvatar: UITapGestureRecognizer = UITapGestureRecognizer()
        self.viewAva.addGestureRecognizer(tapViewAvatar)
        
        let tapLabelName: UITapGestureRecognizer = UITapGestureRecognizer()
        self.lblName.addGestureRecognizer(tapLabelName)
        
        Observable.merge(tapViewAvatar.rx.event.mapToVoid(), tapLabelName.rx.event.mapToVoid())
            .withUnretained(self)
            .bind { owner, _ in
                if owner.check != 1 {
                    owner.moveToEditProfile()
                }
            }.disposed(by: disposeBag)
    }
    
    private func updateBabyInfo() {
        PostVC.title = R.string.localizable.profileTitlePost()
        PostVC.user = user
    }
    
    func setupPageMenu(){
        var controllerArray : [UIViewController] = []
        PostVC = PostProfileViewController.init(nib: R.nib.postProfileViewController)
        PostVC.title = R.string.localizable.profileTitlePost()
        PostVC.user = user
        PostVC.gettype = 1
        PostVC.delegate = self
        
        controllerArray.append(PostVC)
        ListDiaryVC = ListDiaryCLVViewController.init(nib: R.nib.listDiaryCLVViewController)
        ListDiaryVC.title = R.string.localizable.profileTitleDiary()
        
        ListDiaryVC.idDiary = user.id ?? 0
        ListDiaryVC.type = "4"
        
        
        controllerArray.append(ListDiaryVC)
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor.init(hexString: AppColor.colorBGMenu, alpha: 1.0)!),
            .selectionIndicatorColor(UIColor.init(hexString: AppColor.colorYellowDark, alpha: 1.0)!),
            .menuItemFont(UIFont.init(name: AppFont.HelveticaNeueBold, size: 16)!),
            .unselectedMenuItemLabelColor(UIColor.init(hexString: AppColor.colorBlack, alpha: 0.5)!),
            .selectedMenuItemLabelColor(UIColor.black),
            .menuHeight(0),
            .menuItemSeparatorWidth(0),
            .selectionIndicatorHeight(3),
            .menuItemWidth(80),
            .enableHorizontalBounce(true),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .centerMenuItems(true)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.containerView.frame.width, height: self.containerView.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        containerView.addSubview(pageMenu!.view)
    }
    
}

//MARK: CAPSPageMenu Delegate
extension ProfileViewController: CAPSPageMenuDelegate{
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        customSegmetnView.setIndex(index: index)
    }
}

//MARK: Call API
extension ProfileViewController {
    func getProfile(){
        APIManager.getProfile(userID: id, callbackSuccess: { (user) in
            self.user = user
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(150)) {
                self.setupPageMenu()
            }
            self.checkStatusFriend()
            self.setupUI()
        }) { (error) in
//            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            // SVProgressHUD.dismiss()
        }
    }
}
extension ProfileViewController: PostProfileViewControllerDelegate {
    func dismissLightBosTrigger() {
        self.dismissTriggerLightBox = true
    }
    
}
