//
//  EditProfileParentViewController.swift
//  Beberia
//
//  Created by iMAC on 9/7/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import Kingfisher
import SVProgressHUD
import DKImagePickerController
import Photos

class EditProfileParentViewController: UIViewController {
    
    //MARK: Outlet
    @IBOutlet weak var txtDistrict: UITextField!
    @IBOutlet weak var viewAva: UIView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtBirthDay: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtDuSinh: UITextField!
    @IBOutlet weak var tfphoneNumber: UITextField!
    @IBOutlet weak var tfAndress: UITextField!
    
    //MARK: Properties
    let dateFormat: DateFormatter = DateFormatter()
    let datePicker: UIDatePicker = UIDatePicker()
    let dateFormatDS: DateFormatter = DateFormatter()
    let datePickerDS: UIDatePicker = UIDatePicker()
    var idCity = UserInfo.shareUserInfo.city_name.locationCode
    var idDistric = UserInfo.shareUserInfo.district.districtCode
    var arrImage = [String]()
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
     //   NotificationCenter.default.addObserver(self, selector: #selector(reloadProfile), name: NSNotification.Name.editProfileParent, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        imgAva.layer.cornerRadius = imgAva.frame.height/2
        imgAva.clipsToBounds = true
        viewAva.layer.cornerRadius = viewAva.frame.height/2
        viewAva.clipsToBounds = true
        viewAva.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: viewAva.frame.height/2)
    }

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
      {
          let pickerController = DKImagePickerController()
          pickerController.maxSelectableCount = 1
          pickerController.didSelectAssets = { (assets: [DKAsset]) in
              print("didSelectAssets")
              if assets.count > 0{
                  let requestOptions = PHImageRequestOptions()
                  requestOptions.isNetworkAccessAllowed = true
                  requestOptions.deliveryMode = .highQualityFormat
                  requestOptions.isSynchronous = true
                  guard let image = assets[0].originalAsset?.image(targetSize: CGSize.init(width: assets[0].originalAsset?.pixelWidth ?? 0, height: assets[0].originalAsset?.pixelHeight ?? 0), contentMode: .default, options: requestOptions) else {
                      Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Vui lòng thử lại ảnh khác!")
                      return
                  }
                  
                  let imagedata = image.resizeImage(CGSize(width: 80, height: 80))
                  let imageData = imagedata?.jpegData(compressionQuality: 0.4)
                  let imageStr = imageData?.base64EncodedString(options: .lineLength64Characters) ?? ""
                  self.arrImage = ["\(Key.HeaderJsonImage.HeaderJsonImage)\(imageStr)"]
                  self.imgAva.image = imagedata
              }
          }
          Utils.getTopMostViewController()?.present(pickerController, animated: true) {}
      }
      
      @objc func updateDateField(sender: UIDatePicker) {
          if sender == datePicker
          {
              txtBirthDay.text = dateFormat.string(from: datePicker.date)
          }else {
              txtDuSinh.text = dateFormatDS.string(from: datePickerDS.date)
          }
      }
    
    func loadData(){
        imgAva.kf.setImage(with: URL.init(string: UserInfo.shareUserInfo.avartar ) ,placeholder: UIImage.init(named: Key.ImagePlaceholder))
         txtPhone.text = UserInfo.shareUserInfo.email
         
         let birthday = UserInfo.shareUserInfo.birthday
         txtBirthDay.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(String(birthday).count > 10 ? birthday/1000 : birthday)), format: Key.DateFormat.DateFormatddMMyyyy)
        
         if birthday <= 0 {
             txtBirthDay.text = ""
         }

         if UserInfo.shareUserInfo.baby_info.count > 0 {
             let baby = UserInfo.shareUserInfo.baby_info.filter { (baby) -> Bool in
                 return baby.ageRange == 0
             }
             if baby.count > 0{
                 let birthday = baby[0].birthday ?? 0
                 txtDuSinh.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(String(birthday).count > 10 ? birthday/1000 : birthday)), format: Key.DateFormat.DateFormatddMMyyyy)
             }

         }

         txtCity.text = UserInfo.shareUserInfo.city_name.locationNameVi
         lblName.text = UserInfo.shareUserInfo.name
         txtDistrict.text = UserInfo.shareUserInfo.district.districtName
        
        tfphoneNumber.text = UserInfo.shareUserInfo.phone
        tfAndress.text = UserInfo.shareUserInfo.address
    }
    
    //MARK: SetUP UI
    func setupUI(){
        loadData()
        txtPhone.isEnabled = false
        txtCity.rightViewMode = UITextField.ViewMode.always
        let imageViewCity = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 5))
        let imageCity = R.image.ic_BlackArrowDown()
        imageViewCity.image = imageCity
        txtCity.rightView = imageViewCity
        
        txtDistrict.rightViewMode = UITextField.ViewMode.always
        let imageViewDistrict = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 5))
        let imageDistrict = R.image.ic_BlackArrowDown()
        imageViewDistrict.image = imageDistrict
        txtDistrict.rightView = imageViewDistrict

        btnSave.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 20)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgAva.isUserInteractionEnabled = true
        imgAva.addGestureRecognizer(tapGestureRecognizer)
        setupDatePicker()
    }
    
    //MARK: Action
    @IBAction func didPressDistrict(_ sender: Any) {
        let locationVC = DistrictViewController.init(nib: R.nib.districtViewController)
        locationVC.locationID = idCity ?? 0
        locationVC.hidesBottomBarWhenPushed = true
        locationVC.didSelectedLocation = { city in
            self.idDistric = city.districtCode ?? ""
            self.txtDistrict.text = city.districtName
    
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    @IBAction func btnCity(_ sender: Any) {
        let locationVC = LocationViewController.init(nib: R.nib.locationViewController)
        locationVC.hidesBottomBarWhenPushed = true
        locationVC.didSelectedLocation = { city in
            self.idCity = city.locationCode ?? 0
            self.txtCity.text = city.locationNameVi
            if self.idCity == 79 {
                self.txtCity.text = R.string.localizable.commonTPHCMShort()
            }
            
            // reset distric
            self.idDistric = ""
            self.txtDistrict.text = ""
            UserInfo.shareUserInfo.district.districtName = ""
            UserInfo.shareUserInfo.district.districtCode = ""
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: true)
    }

        //MARK: Request API
    func editProfile(birthday:Double,city_code:Int, birthday_baby:Double, id_baby:Int){
        var idbb = 0
        if UserInfo.shareUserInfo.baby_info.count > 0 {
            idbb = UserInfo.shareUserInfo.baby_info[0].id ?? 0
        }
        
        if idDistric == ""{
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng chọn quận!")
            return
        }
        
        guard let phone = tfphoneNumber.text, phone.count > 0, phone.isValidPhone(phone: phone) else {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập số điện thoại!")
            return
        }
        
        guard let address = tfAndress.text else {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập địa chỉ!")
            return
        }

        SVProgressHUD.show()
        APIManager.editProfile(image: arrImage, birthday: birthday, birthday_baby: birthday_baby, id_baby: idbb, city_code: city_code, district_code: idDistric ?? "",address: address, phone: phone, callbackSuccess: { [weak self] (Users) in
            guard let self = self else { return }
            self.loadData()
            
      //      NotificationCenter.default.post(name: NSNotification.Name.editProfileParent, object: nil, userInfo: nil)
            NotificationCenter.default.post(name: NSNotification.Name.editProfileInfo, object: nil, userInfo: nil)
            SVProgressHUD.dismiss()
            Utils.showAlertOK(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.profileTitleUpdateInfoSuccess())
            
            // update avatar sendbird
//            SBDMain.updateCurrentUserInfo(withNickname: UserInfo.shareUserInfo.name, profileUrl: UserInfo.shareUserInfo.avartar, completionHandler: { (error) in
//                guard error == nil else {   // Error.
//                    return
//                }
//            })
            
        }) { (error) in
             Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
             SVProgressHUD.dismiss()
        }
    }
    
    @IBAction func btnSave(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyyyy
        let dateString = dateFormatter.date(from: txtBirthDay.text ?? "")
        let dateTimeStamp  = dateString?.timeIntervalSince1970
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = Key.DateFormat.DateFormatddMMyyyy
        let dateString1 = dateFormatter1.date(from: txtDuSinh.text ?? "")
        let dateTimeStamp1  = dateString1?.timeIntervalSince1970
        
        self.dismiss(animated: true, completion:nil)
        editProfile(birthday: dateTimeStamp ?? 0, city_code: idCity ?? 0, birthday_baby: dateTimeStamp1 ?? 0, id_baby: 0)
    }

        //MARK: Setup date Picker
        func setupDatePicker (){
            dateFormat.dateStyle = DateFormatter.Style.medium
            dateFormat.timeStyle = DateFormatter.Style.none
            dateFormat.dateFormat = Key.DateFormat.DateFormatddMMyyyy
            datePicker.datePickerMode = UIDatePicker.Mode.date
            if #available(iOS 13.4, *) {
                datePicker.preferredDatePickerStyle = .wheels
            } else {
                // Fallback on earlier versions
            }
            datePicker.addTarget(self, action: #selector(updateDateField(sender:)), for: UIControl.Event.valueChanged)
            txtBirthDay.inputView = datePicker
            txtBirthDay.rightViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            let image = R.image.ic_Calendar()
            imageView.image = image
            txtBirthDay.rightView = imageView

            dateFormatDS.dateStyle = DateFormatter.Style.medium
            dateFormatDS.timeStyle = DateFormatter.Style.none
            dateFormatDS.dateFormat = Key.DateFormat.DateFormatddMMyyyy
            datePickerDS.datePickerMode = UIDatePicker.Mode.date
            if #available(iOS 13.4, *) {
                datePickerDS.preferredDatePickerStyle = .wheels 
            } else {
                // Fallback on earlier versions
            }
            datePickerDS.minimumDate = Date()
            datePickerDS.addTarget(self, action: #selector(updateDateField(sender:)), for: UIControl.Event.valueChanged)
            txtDuSinh.rightViewMode = UITextField.ViewMode.always
            let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            let image1 = R.image.ic_Calendar()
            imageView1.image = image1
            txtDuSinh.rightView = imageView1
            txtDuSinh.inputView = datePickerDS
        }
         
}
