//
//  EditProfileInfoViewController.swift
//  Beberia
//
//  Created by iMAC on 9/7/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class EditProfileInfoViewController: BaseViewController, CustomSegmentedControlDelegate {
    
    // MARK: - CustomSegmentedControlDelegate
          
          func changeToIndex(index: Int) {
              print(index)
              pageMenu?.moveToPage(index)
          }

    //MARK: Outlet
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var customSegmetnView: CustomSegmentView!
         {
             didSet{
                 customSegmetnView.setButtonTitles(buttonTitles: [R.string.localizable.profileMe(),R.string.localizable.profileBaby()])
                 customSegmetnView.selectorViewColor = customSegmetnView.selectorTextColor
                 customSegmetnView.selectorTextColor = customSegmetnView.selectorTextColor
                 customSegmetnView.delegate = self
             }
         }
    
    //MARK: Properties
    var parentVC: EditProfileParentViewController!
    var childVC: EditProfileChildVC!
    var pageMenu : CAPSPageMenu?
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupPageMenu()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // setupPageMenu()
    }
    
    //MARK: Setup UI
    func setupUI(){
        self.title = R.string.localizable.settingTitleEdit()
        customLeftBarButton()
    }
    
    func setupPageMenu(){
        var controllerArray : [UIViewController] = []
        parentVC = EditProfileParentViewController.init(nib: R.nib.editProfileParentViewController)
        parentVC.title = R.string.localizable.profileMe()
        parentVC.hidesBottomBarWhenPushed = true
        controllerArray.append(parentVC)
        childVC = EditProfileChildVC.init(nib: R.nib.editProfileChildVC)
        childVC.title = R.string.localizable.profileBaby()
        controllerArray.append(childVC)
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor.init(hexString: AppColor.colorBGMenu, alpha: 1.0)!),
            .selectionIndicatorColor(UIColor.init(hexString: AppColor.colorYellowDark, alpha: 1.0)!),
            .menuItemFont(UIFont.init(name: AppFont.HelveticaNeueBold, size: 16)!),
            .unselectedMenuItemLabelColor(UIColor.init(hexString: AppColor.colorBlack, alpha: 0.5)!),
            .selectedMenuItemLabelColor(UIColor.black),
            .menuHeight(0),
            .menuItemSeparatorWidth(0),
            .selectionIndicatorHeight(3),
            .menuItemWidth(80),
            .enableHorizontalBounce(false),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .centerMenuItems(true)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: containerView.frame.width, height: containerView.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        containerView.addSubview(pageMenu!.view)
    }
    

}

//MARK: CAPSPageMenuDelegate
extension EditProfileInfoViewController: CAPSPageMenuDelegate{
    func didMoveToPage(_ controller: UIViewController, index: Int) {
         customSegmetnView.setIndex(index: index)
    }
}
