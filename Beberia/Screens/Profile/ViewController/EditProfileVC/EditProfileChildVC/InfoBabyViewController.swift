//
//  InfoBabyViewController.swift
//  Beberia
//
//  Created by OS on 9/10/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
class InfoBabyViewController: BaseViewController {
    
    //MARK: Outlet
    @IBOutlet weak var imageDeleteBaby: UIImageView!
    @IBOutlet weak var tbvInfoBaby: UITableView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    //MARK: Properties
    var getgender = 0
    var getindex = 0
    var gettype = ""
    var gende:Bool = false {
        didSet{
            getgender = gende ? 1 : 0
        }
    }
    var arrayBaby = UserInfo.shareUserInfo.baby_info.filter({ (baby) -> Bool in
        return baby.ageRange != 0
    })
    var deleteBabySuccess: (_ idBaby: Int)->() = {_ in}
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }
    
    //MARK: Setup UI
    func setupUI(){
        self.title = R.string.localizable.profileTitleInfoBaby()
        tbvInfoBaby.register(R.nib.infoLoginTableViewCell)
        tbvInfoBaby.delegate = self
        tbvInfoBaby.dataSource = self
        tbvInfoBaby.separatorStyle = .none
        tbvInfoBaby.allowsSelection = false
        btnSave.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 20)
        customLeftBarButton()
        
        if gettype != "edit" || (gettype == "edit" && arrayBaby.count == 1) {
            imageDeleteBaby.isHidden = true
            btnDelete.isEnabled = false
        }
    }

    //MARK: Action
    @IBAction func btnSave(_ sender: Any) {
        if gettype == "edit" {
            editBabyProfile()
        }else {
            addBabyProfile()
        }
    }
    
    @IBAction func didPressDelete(_ sender: Any) {
        Utils.showAlertOKWithAction(controller: self, title: R.string.localizable.commonNoti(), message: "Bạn có chắc chắn xoá thông tin con ?", cancel: true) {
            self.deleteBaby()
        }
    }
    
    //MARK: Request API
    func editBabyProfile(){
        
        
        
//        let index = IndexPath(row: 0, section: 0)
//        let cell: InfoLoginTableViewCell = self.tbvInfoBaby.cellForRow(at: index) as! InfoLoginTableViewCell
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyy
//        let dateString = dateFormatter.date(from: cell.lblBirthday.text ?? "")
//        let dateTimeStamp  = dateString?.timeIntervalSince1970 ?? 0
//      
//        guard let name = cell.lblName.text, name.count > 0 else {
//            Utils.showAlertOK(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập tên con!")
//            return
//        }
//        
//        gende = cell.isGenderNam
//        let getid = arrayBaby[getindex].id ?? 0
//        
//        btnSave.isEnabled = false
//        
//        SVProgressHUD.show()
//        APIManager.editBabyProfile(id: getid, gender: getgender, nickname: name, birthday: dateTimeStamp, callbackSuccess: { [weak self] (succes) in
//            guard let self = self else { return }
//            let index = UserInfo.shareUserInfo.baby_info.firstIndex { (baby) -> Bool in
//                return baby.id == succes.id
//            }
//            if let indexBaby = index { UserInfo.shareUserInfo.baby_info[indexBaby] = succes }
// 
//            Utils.showAlertOK(controller: self, title: "Thành công", message: "Cập nhật thông tin thành công")
//            NotificationCenter.default.post(name: NSNotification.Name.editProfileChild, object: nil, userInfo: nil)
//            NotificationCenter.default.post(name: NSNotification.Name.editProfileInfo, object: nil, userInfo: nil)
//            SVProgressHUD.dismiss()
//        }) { (error) in
//            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
//            SVProgressHUD.dismiss()
//        }
    }
    
    func deleteBaby(){
       let getid = arrayBaby[getindex].id ?? 0
        SVProgressHUD.show()
        APIManager.deleteBabyProfile(idBaby: getid, callbackSuccess: { [weak self] (baby) in
            guard let self = self else { return }
            NotificationCenter.default.post(name: NSNotification.Name.editProfileInfo, object: nil, userInfo: nil)
            self.navigationController?.popViewController(animated: true)
            self.deleteBabySuccess(getid)
            
            SVProgressHUD.dismiss()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        }
    }
    
    func addBabyProfile() {
        
        
        
        
        let index = IndexPath(row: 0, section: 0)
        let cell: InfoLoginTableViewCell = self.tbvInfoBaby.cellForRow(at: index) as! InfoLoginTableViewCell
        
        if cell.lblBirthday.text == "" || cell.lblName.text == "" {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: R.string.localizable.loginTitleAlertMess())
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyy
        let dateString = dateFormatter.date(from: cell.lblBirthday.text ?? "")
        let dateTimeStamp  = dateString?.timeIntervalSince1970 ?? 0
        let name = cell.lblName.text ?? ""
        gende = cell.isGenderNam
        
        btnSave.isEnabled = false
        
        SVProgressHUD.show()
        APIManager.addBabyProfile( gender: getgender, nickname: name, birthday: dateTimeStamp, callbackSuccess: { [weak self] (succes) in
            guard let self = self else { return }
            UserInfo.shareUserInfo.baby_info.append(succes)
            NotificationCenter.default.post(name: NSNotification.Name.editProfileChild, object: nil, userInfo: nil)
            NotificationCenter.default.post(name: NSNotification.Name.editProfileInfo, object: nil, userInfo: nil)
            Utils.showAlertOK(controller: self, title: "Thành công", message: "Cập nhật thông tin thành công")

            SVProgressHUD.dismiss()
            self.btnSave.isEnabled = true
        }) { (error) in
           Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: R.string.localizable.loginTitleAlertMess())
            SVProgressHUD.dismiss()
            self.btnSave.isEnabled = true
        }
    }

}

//MARK: UITableview Delegate
extension InfoBabyViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.infoLoginTableViewCell, for: indexPath)!
        cell.btnClose.tag = indexPath.row
        cell.lblTitleInfo.text = "\(R.string.localizable.profileInfoBaby()) (\(getindex + 1))"
        cell.btnClose.isHidden = true
        cell.btnAddChildren.isHidden = true
        cell.viewAddChildren.isHidden = true
        if gettype == "edit" {
            let gender = arrayBaby[getindex].gender ?? 0
            cell.lblTitleInfo.text = "\(R.string.localizable.profileInfoBaby()) (\(getindex + 1))"
            cell.lblName.text = arrayBaby[getindex].nickname ?? ""
            let birthday = arrayBaby[getindex].birthday ?? 0
            cell.lblBirthday.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(String(birthday).count > 10 ? birthday/1000 : birthday)), format: Key.DateFormat.DateFormatddMMyyyy)
            if gender == 0 {
                cell.isGenderNam = false
            }else {
                cell.isGenderNam = true
            }
        }else {
            cell.lblTitleInfo.text = "\(R.string.localizable.profileInfoBaby())"
            cell.lblName.text = ""
            cell.lblBirthday.text = ""
            cell.isGenderNam = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 270
    }
}
