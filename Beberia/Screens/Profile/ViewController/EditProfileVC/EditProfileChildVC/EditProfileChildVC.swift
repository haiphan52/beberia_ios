//
//  EditProfileChildVC.swift
//  Beberia
//
//  Created by OS on 9/10/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class EditProfileChildVC: UIViewController {
   //MARK: Outlet
    @IBOutlet weak var tbvBaby: UITableView!
    
    //MARK: Properties
    var arrayBaby = [[String: Any]]()
    var arrayChildrens = UserInfo.shareUserInfo.baby_info
//    var arrayChildrens = UserInfo.shareUserInfo.baby_info.filter({ (baby) -> Bool in
//        return baby.ageRange != 0
//    })
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadProfileChild), name: NSNotification.Name.editProfileChild, object: nil)
        // Do any additional setup after loading the view.
    }
    
    //MARK: @objc
    @objc private func reloadProfileChild(notification: NSNotification){
        arrayChildrens = UserInfo.shareUserInfo.baby_info
//        arrayChildrens = UserInfo.shareUserInfo.baby_info.filter({ (baby) -> Bool in
//            return baby.ageRange != 0
//        })
        
        tbvBaby.reloadData()
    }
    
    //Mark: Setup UI
    func setupUI(){
        tbvBaby.register(R.nib.viewEditProfileChildCell)
         tbvBaby.register(R.nib.cellEmpty)
        tbvBaby.delegate = self
        tbvBaby.dataSource = self
        tbvBaby.separatorStyle = .none
        tbvBaby.allowsSelection = false
        
//        if UserInfo.shareUserInfo.baby_info[0].ageRange == 0{
//            arrayChildrens.removeAll()
//        }else{
//            tbvBaby.isHidden = true
//        }

    }
    
    func pushToViewAddBaby(){
        let addBaby = AddBabyViewController.init(nib: R.nib.addBabyViewController)
        addBaby.hidesBottomBarWhenPushed = true
        addBaby.backToUpdateBaby = { baby in
            self.arrayChildrens.insert(baby, at: 0)
            self.tbvBaby.reloadData()
            UserInfo.shareUserInfo.baby_info = self.arrayChildrens
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(addBaby, animated: true)
    }
    
    func handelEditBaby(baby: BabyInfo){
        let addBaby = AddBabyViewController.init(nib: R.nib.addBabyViewController)
        addBaby.hidesBottomBarWhenPushed = true
        addBaby.baby = baby
        addBaby.deleteBaby = { _ in
            if let index = self.arrayChildrens.firstIndex(where: { $0.id ==  baby.id }) {
                self.arrayChildrens.remove(at: index)
                self.tbvBaby.reloadData()
                UserInfo.shareUserInfo.baby_info = self.arrayChildrens
            }  
        }
        addBaby.editBaby = { baby in
            if let index = self.arrayChildrens.firstIndex(where: { $0.id ==  baby.id }) {
                self.arrayChildrens[index] = baby
                self.tbvBaby.reloadData()
                UserInfo.shareUserInfo.baby_info = self.arrayChildrens
            }
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(addBaby, animated: true)
    }
    
    //MARK: deinit
    deinit {
        print("Remove NotificationCenter Deinit")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.editProfileChild, object: nil)
    }
}

//MARK: UITableview Delegate
extension EditProfileChildVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayChildrens.count > 0 {
             return arrayChildrens.count
        }else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if arrayChildrens.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.viewEditProfileChildCell, for: indexPath)!
                cell.viewAddChild.isHidden = false
                cell.lblName.text = arrayChildrens[indexPath.row].nickname
                cell.btnEdit.tag = indexPath.row
                cell.btnAddChild.tag = indexPath.row
                cell.tapViewEdit = { index in
//                    let postVC = InfoBabyViewController.init(nib: R.nib.infoBabyViewController)
//                    postVC.deleteBabySuccess = { [weak self] idBaby in
//                        guard let self = self else { return }
////                        let index = UserInfo.shareUserInfo.baby_info.firstIndex { (baby) -> Bool in
////                            return baby.id == idBaby
////                        }
////                        if index != nil {
////                            UserInfo.shareUserInfo.baby_info.remove(at: index!)
//                            self.arrayChildrens = UserInfo.shareUserInfo.baby_info.filter({ (baby) -> Bool in
//                                return baby.ageRange != 0
//                            })
//                            self.tbvBaby.reloadData()
//                 //       }
//                    }
//                    postVC.hidesBottomBarWhenPushed = true
//                    postVC.getindex = index
//                    postVC.gettype = "edit"
                    self.handelEditBaby(baby: self.arrayChildrens[index])
                   // Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
                }
                
                cell.tapAddChild = { index in
                    self.pushToViewAddBaby()
                }
                if self.arrayChildrens.count > 1{
                    if (indexPath.row != self.arrayChildrens.count - 1){
                        cell.viewAddChild.isHidden = true
                    }
                }
              return cell
        }else {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cellEmpty, for: indexPath)!
            cell1.btnAddChild.tag = indexPath.row
            cell1.tapAddChild = {index in
                self.pushToViewAddBaby()
            }
            return cell1
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if self.arrayChildrens.count > 0{
            if (indexPath.row != self.arrayChildrens.count - 1){
                return 55
            }
            return 100
        }else {
             return 120
        }
       
    }
    
}
