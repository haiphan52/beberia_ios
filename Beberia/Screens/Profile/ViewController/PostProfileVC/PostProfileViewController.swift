//
//  PostProfileViewController.swift
//  Beberia
//
//  Created by iMAC on 9/7/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import PullToRefresh

protocol PostProfileViewControllerDelegate: AnyObject {
    func dismissLightBosTrigger()
}

class PostProfileViewController: BaseViewController {
    
    //MARK: Outlet
    @IBOutlet weak var tbvListPost: UITableView!
    
    var delegate: PostProfileViewControllerDelegate?
    
    //MARK: Properties
    var arrayHomeFeeds = [HomeFeed]()
    var gettype = 0
    var page = 1
    let refresher = PullToRefresh()
    var user = User.init(json: "")
    var isLoadingMore = false
    var nextpage = 0
    var heightDictionary: [IndexPath : CGFloat] = [:]
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getProfileFeed(type: gettype, page: page)
        NotificationCenter.default.addObserver(self, selector: #selector(checkEditHomeProfile), name: NSNotification.Name.checkEditHomeProfile, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkEditHomeProfile), name: NSNotification.Name.checkEditHomeSave, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateFeedNew), name: NSNotification.Name.updateFeedNew, object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tbvListPost.isUserInteractionEnabled = true
    }
    
    //MARK: @objc
    @objc func updateFeedNew(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        guard let homeFeedNew = object["FeedNew"] as? HomeFeed else{
            return
        }
        self.arrayHomeFeeds.insert(homeFeedNew, at: 0)
        self.tbvListPost.reloadData()
    }
    
    @objc func checkEditHomeProfile(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        let idFeed = object[Key.KeyNotification.idFeed] as? Int
        self.getCheckCompleteFeed(id: idFeed ?? 0, completion: { homeFeed in
            let index = self.arrayHomeFeeds.firstIndex(where: { (home) -> Bool in
                return home.id == homeFeed.id
            })
            if index != nil{
                self.arrayHomeFeeds[index!] = homeFeed
            }
            self.tbvListPost.reloadData()
        })
    }
    
    //MARK Setup UI
    func setupUI(){
        tbvListPost.register(R.nib.homeTableViewCell)
        tbvListPost.register(R.nib.homeQCTableViewCell)
        tbvListPost.delegate = self
        tbvListPost.dataSource = self
        tbvListPost.separatorStyle = .none
      //  tbvListPost.infiniteScrollTriggerOffset = 700
        tbvListPost.showsVerticalScrollIndicator = false
        tbvListPost.addInfiniteScroll { (tableView) in
            tableView.performBatchUpdates({ () -> Void in
                // update collection view
                if self.nextpage > 0{
                    self.isLoadingMore = true
                    self.page = self.page + 1
                    self.getProfileFeed(type: self.gettype, page: self.page)
                }else{
                    self.tbvListPost.finishInfiniteScroll(completion: { (collection) in
                    })
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                tableView.finishInfiniteScroll()
            });
        }
        
        tbvListPost.addPullToRefresh(refresher) {
            self.isLoadingMore = true
            self.page = 1
            self.getProfileFeed(type: self.gettype, page: self.page)
            self.tbvListPost.startRefreshing(at: .top)
        }
    }

    //MARK: Request API
    func getProfileFeed(type:Int,page:Int){
        if !isLoadingMore{
            SVProgressHUD.show()
        }
      
        
        APIManager.getProfileFeed(id: user.id == nil ? UserInfo.shareUserInfo.id! : user.id ?? 0, type: type, page: page, callbackSuccess: { [weak self] (homeFeeds, page) in
            guard let self = self else { return }
            self.nextpage = page
            if self.page == 1{
                self.arrayHomeFeeds.removeAll()
            }
            if self.arrayHomeFeeds.count == 0 {
                self.arrayHomeFeeds = homeFeeds
            }else{
                self.arrayHomeFeeds.append(contentsOf: homeFeeds)
            }
            self.tbvListPost.reloadData()
            self.tbvListPost.endRefreshing(at: .top)
            self.tbvListPost.finishInfiniteScroll(completion: { (collection) in
            })
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    deinit {
        print("Remove NotificationCenter Deinit")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.checkEditHomeProfile, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.checkEditHomeSave, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.updateFeedNew, object: nil)
    }
    
    
    func deleteMark(indext:Int){
        self.showActionBottom(datasource: [R.string.localizable.homeDelete()], isNoTextColorRed: false) { (int) in
            APIManager.saveHomeFeed(id: self.arrayHomeFeeds[indext].id ?? 0, checkSave: 1 , callbackSuccess: { [weak self] (checkSave) in
                guard let self = self else { return }
                self.arrayHomeFeeds.remove(at: indext)
                self.tbvListPost.reloadData()
            }) { (error) in
                Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
            }
        }
    }
    
    func likePost(idFeed: Int, index: Int, isLike: Int){
        let homeFeed = self.arrayHomeFeeds[index]
        guard let numberLike = homeFeed.numberLike, numberLike > -1 else {
            return
        }
        
        homeFeed.checkLike = isLike == 1 ? 0 : 1
        homeFeed.numberLike = homeFeed.checkLike == 0 ? numberLike - 1 :numberLike + 1
        self.arrayHomeFeeds[index] = homeFeed
        self.tbvListPost.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        APIManager.likeHomeFeed(id: idFeed, type: 1, checkLike:isLike , callbackSuccess: { [weak self] (numberLike, checkLike) in
            guard let self = self else { return }
            if numberLike != homeFeed.numberLike {
                let homeFeed = self.arrayHomeFeeds[index]
                homeFeed.checkLike = checkLike
                homeFeed.numberLike = numberLike
                self.arrayHomeFeeds[index] = homeFeed
                self.tbvListPost.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
            
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func favouritePost(idFeed: Int, index: Int, isFavourite: Int){
        let homeFeed = self.arrayHomeFeeds[index]
        homeFeed.checkSave = isFavourite == 1 ? 0 : 1
        self.arrayHomeFeeds[index] = homeFeed
        self.tbvListPost.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        
        APIManager.saveHomeFeed(id: idFeed, checkSave: isFavourite , callbackSuccess: { [weak self] (checkSave) in
            guard let self = self else { return }
            if checkSave != homeFeed.checkSave{
                let homeFeed = self.arrayHomeFeeds[index]
                homeFeed.checkSave = checkSave
                self.arrayHomeFeeds[index] = homeFeed
                self.tbvListPost.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
            
            
        }) { (error) in
            Utils.showAlertView(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func handelLike(indexRow: Int, homefeed: HomeFeed){
        if Utils.checkLogin(selfView: self) {
            guard let id = homefeed.id else { return }
            guard let checkLike = homefeed.checkLike else { return }
            self.likePost(idFeed: id, index: indexRow, isLike: checkLike)
        }
    }
    
    func handelSave(indexRow: Int, homefeed: HomeFeed){
        if Utils.checkLogin(selfView: self) {
            guard let id = homefeed.id else { return }
            guard let checkSave = homefeed.checkSave else { return }
            self.favouritePost(idFeed: id, index: indexRow, isFavourite: checkSave)
        }
    }
    
    func handelComment(indexRow: Int, homefeed: HomeFeed){
        if Utils.checkLogin(selfView: self) {
            let bottomCommentTableView = BottomTableViewViewController.init(nib: R.nib.bottomTableViewViewController)
            bottomCommentTableView.topCornerRadius = 20
            bottomCommentTableView.presentDuration = 0.25
            bottomCommentTableView.dismissDuration = 0.25
            bottomCommentTableView.isFeed = homefeed.id ?? 0
            bottomCommentTableView.updateComment = {
                homefeed.numberComment = homefeed.numberComment ?? 0 + 1
                self.tbvListPost.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .none)
            }
           // bottomCommentTableView.modalPresentationStyle = .custom
            Utils.getTopMostViewController()?.present(bottomCommentTableView, animated: true, completion: nil)
        }
    }

}

// MARK: - UITableViewDelegate

extension PostProfileViewController: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayHomeFeeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.arrayHomeFeeds.count > 0{
            let homefeed = self.arrayHomeFeeds[indexPath.row]
            if homefeed.ads == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeTableViewCell, for: indexPath)!
                cell.setupDataHomeFeed(homeFeed: homefeed)
                cell.tapComment = { self.handelComment(indexRow: indexPath.row, homefeed: homefeed) }
                cell.tapLike = { self.handelLike(indexRow: indexPath.row, homefeed: homefeed)}
                cell.tapSave = { self.handelSave(indexRow: indexPath.row, homefeed: homefeed) }
                cell.dismissLightBoxTrigger = { [weak self] in
                    guard let self = self else { return }
                    self.delegate?.dismissLightBosTrigger()
                }
                
                return cell
            }else{
                let cellQC = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.homeQCTableViewCell, for: indexPath)!
                cellQC.setupDataHomeFeedQC(homeFeed: homefeed)
                cellQC.tapComment = { self.handelComment(indexRow: indexPath.row, homefeed: homefeed) }
                cellQC.tapLike = { self.handelLike(indexRow: indexPath.row, homefeed: homefeed)}
                cellQC.tapSave = { self.handelSave(indexRow: indexPath.row, homefeed: homefeed) }
                
                return cellQC
            }
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selfStrong = Utils.getTopMostViewController() else {
            return
        }
        let editFromView: EditFromView = (gettype == 2) ? .Save : .Profile
        
        Router.share.pushToViewHomeDetail(tableView: tableView, array: self.arrayHomeFeeds,indexPath: indexPath, viewSelf: selfStrong, idFeed: self.arrayHomeFeeds[indexPath.row].id ?? 0, editFromView: editFromView, completion: { homeFeeds in
            self.arrayHomeFeeds = homeFeeds
            self.tbvListPost.reloadData()
        })
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        heightDictionary[indexPath] = cell.frame.size.height
    //
    //        if nextPage == 2 || (nextPage == 0 && self.arrayHomeFeeds.count < 10){
    //            cell.alpha = 0
    //            UIView.animate(
    //                withDuration: 0.3,
    //                delay: 0.05 * Double(indexPath.row),
    //                animations: {
    //                    cell.alpha = 1
    //            })
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return height ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath] = cell.frame.size.height
    }
}
