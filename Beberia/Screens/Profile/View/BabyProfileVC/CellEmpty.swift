//
//  CellEmpty.swift
//  Beberia
//
//  Created by OS on 12/25/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class CellEmpty: UITableViewCell {

    @IBOutlet weak var btnAddChild: UIButton!
    var tapAddChild: (_ index :Int) -> () = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnAddChild(_ sender: Any) {
         tapAddChild(btnAddChild.tag)
    }
}
