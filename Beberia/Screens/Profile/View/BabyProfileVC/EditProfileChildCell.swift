//
//  EditProfileChildCell.swift
//  Beberia
//
//  Created by OS on 9/10/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class EditProfileChildCell: UITableViewCell {

    @IBOutlet weak var btnEdit: UIButton!
   
    @IBOutlet weak var viewBaby: UIView!
    @IBOutlet weak var viewAddChild: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnAddChild: UIButton!
    var tapViewEdit: (_ index :Int) -> () = {_ in}
    var tapAddChild: (_ index :Int) -> () = {_ in}
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBaby.cornerRadius = 20
        viewBaby.borderWidth = 1
        viewBaby.borderColor = UIColor.init(hexString: "EBEBEB", alpha: 1.0)!
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnEdit(_ sender: Any) {
         tapViewEdit(btnEdit.tag)
    }
    
    @IBAction func btnAddChild(_ sender: Any) {
        tapAddChild(btnAddChild.tag)
    }
}
