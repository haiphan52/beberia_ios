//
//  SettingProfileCell.swift
//  Beberia
//
//  Created by OS on 9/11/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class SettingProfileCell: UITableViewCell {

    @IBOutlet weak var lblContent: UILabel!
    
    @IBOutlet weak var switchOnOff: UISwitch!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        switchOnOff.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
