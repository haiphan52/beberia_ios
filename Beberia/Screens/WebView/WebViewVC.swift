
//
//  
//  WebViewVC.swift
//  Beberia
//
//  Created by haiphan on 20/07/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift
import WebKit

class WebViewVC: BaseVC {
    
    // Add here outlets
    @IBOutlet weak var webView: WKWebView!
    
    var webURL: String?
    // Add here your view model
    private var viewModel: WebViewVM = WebViewVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
        self.setupNavigationVer2()
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.fd799D()!,
                                 textColor: .white,
                                 isTranslucent: true)
    }
    
}
extension WebViewVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        if let urlStr = self.webURL, let url = URL(string: urlStr) {
            let req = URLRequest(url: url) 
            webView.load(req)
        }
    }
    
    private func setupRX() {
        // Add here the setup for the RX
    }
}
