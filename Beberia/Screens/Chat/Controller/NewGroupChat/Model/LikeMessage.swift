//
//  LikeMessage.swift
//  Beberia
//
//  Created by haiphan on 02/03/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

// MARK: - DataClass
struct LikeMessageModel: Codable {
    let isLiked, number: Int?

    enum CodingKeys: String, CodingKey {
        case isLiked = "is_liked"
        case number
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isLiked = try values.decodeIfPresent(Int.self, forKey: .isLiked)
        number = try values.decodeIfPresent(Int.self, forKey: .number)
        
    }
}
