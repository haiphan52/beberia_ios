//
//  SearchGroup.swift
//  Beberia
//
//  Created by haiphan on 04/03/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

struct SearchGroupModel: Codable {
    let list: [NewGroupModel]?
    let nextPage: Int?
    let total: Int?

    enum CodingKeys: String, CodingKey {
        case list
        case nextPage = "next_page"
        case total
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([NewGroupModel].self, forKey: .list)
        nextPage = try values.decodeIfPresent(Int.self, forKey: .nextPage)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
    }
}
