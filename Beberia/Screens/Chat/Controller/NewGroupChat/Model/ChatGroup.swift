//
//  ChatGroup.swift
//  Beberia
//
//  Created by haiphan on 24/02/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import SwiftyJSON

// MARK: - DataClass
struct ChatGroupsModel: Codable {
    let list: [ChatGroup]?
    let nextPage: Int?

    enum CodingKeys: String, CodingKey {
        case list
        case nextPage = "next_page"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([ChatGroup].self, forKey: .list)
        nextPage = try values.decodeIfPresent(Int.self, forKey: .nextPage)
    }
}

// MARK: - DataClass
struct ChatGroup: Codable {
    let edited: Int?
    let sender: Sender?
    let messageType, groupID: Int?
    let media, message: String?
    let reply: ReplyChatModel?
    let created, messageID: Double?
    var isLike: Int?
    var numberLike: Int?

    enum CodingKeys: String, CodingKey {
        case edited, sender
        case messageType = "message_type"
        case groupID = "group_id"
        case media, message, reply, created
        case messageID = "message_id"
        case isLike = "is_liked"
        case numberLike = "number_like"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        media = try values.decodeIfPresent(String.self, forKey: .media)
        created = try values.decodeIfPresent(Double.self, forKey: .created)
        sender = try values.decodeIfPresent(Sender.self, forKey: .sender)
        edited = try values.decodeIfPresent(Int.self, forKey: .edited)
        messageID = try values.decodeIfPresent(Double.self, forKey: .messageID)
        messageType = try values.decodeIfPresent(Int.self, forKey: .messageType)
        groupID = try values.decodeIfPresent(Int.self, forKey: .groupID)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        reply = try values.decodeIfPresent(ReplyChatModel.self, forKey: .reply)
        isLike = try values.decodeIfPresent(Int.self, forKey: .isLike)
        numberLike = try values.decodeIfPresent(Int.self, forKey: .numberLike)
    }
    
    func getIsLike() -> Bool {
        guard let isLike = self.isLike else {
            return false
        }
        
        if isLike == 1 {
            return true
        }
        
        return false
    }
}

public final class MessageNotifyModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let edited = "edited"
    static let sender = "sender"
    static let messageType = "message_type"
    static let groupID = "group_id"
    static let media = "media"
    static let message = "message"
    static let created = "created"
    static let messageID = "message_id"
    static let isLike = "is_liked"
    static let numberLike = "number_like"
//    static let message = "message"
  }

  // MARK: Properties
  public var edited: Int?
  public var sender: SenderNotifyModel?
  public var messageType: Int?
  public var numberLike: Int?
  public var groupID: Int?
  public var media: String?
  public var message: String?
  public var created: Double?
  public var messageID: Double?
  public var isLike: Int?
//    public var message: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
      edited = json[SerializationKeys.edited].int
      sender = SenderNotifyModel(json: json[SerializationKeys.sender])
      messageType = json[SerializationKeys.messageType].int
      numberLike = json[SerializationKeys.numberLike].int
      groupID = json[SerializationKeys.groupID].int
      media = json[SerializationKeys.media].string
      message = json[SerializationKeys.message].string
      created = json[SerializationKeys.message].double
      messageID = json[SerializationKeys.messageID].double
      isLike = json[SerializationKeys.isLike].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = edited { dictionary[SerializationKeys.edited] = value }
    if let value = sender { dictionary[SerializationKeys.sender] = value.dictionaryRepresentation() }
    if let value = messageType { dictionary[SerializationKeys.messageType] = value }
    if let value = numberLike { dictionary[SerializationKeys.numberLike] = value }
    if let value = groupID { dictionary[SerializationKeys.groupID] = value }
    if let value = media { dictionary[SerializationKeys.media] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = messageID { dictionary[SerializationKeys.messageID] = value }
    if let value = isLike { dictionary[SerializationKeys.isLike] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.edited = aDecoder.decodeObject(forKey: SerializationKeys.edited) as? Int
    self.sender = aDecoder.decodeObject(forKey: SerializationKeys.sender) as? SenderNotifyModel
    self.messageType = aDecoder.decodeObject(forKey: SerializationKeys.messageType) as? Int
    self.groupID = aDecoder.decodeObject(forKey: SerializationKeys.groupID) as? Int
    self.media = aDecoder.decodeObject(forKey: SerializationKeys.media) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.created = aDecoder.decodeObject(forKey: SerializationKeys.created) as? Double
    self.messageID = aDecoder.decodeObject(forKey: SerializationKeys.messageID) as? Double
    self.isLike = aDecoder.decodeObject(forKey: SerializationKeys.isLike) as? Int
    self.numberLike = aDecoder.decodeObject(forKey: SerializationKeys.numberLike) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(edited, forKey: SerializationKeys.edited)
    aCoder.encode(sender, forKey: SerializationKeys.sender)
    aCoder.encode(messageType, forKey: SerializationKeys.messageType)
    aCoder.encode(groupID, forKey: SerializationKeys.groupID)
    aCoder.encode(media, forKey: SerializationKeys.media)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(created, forKey: SerializationKeys.created)
    aCoder.encode(messageID, forKey: SerializationKeys.messageID)
    aCoder.encode(isLike, forKey: SerializationKeys.isLike)
    aCoder.encode(numberLike, forKey: SerializationKeys.numberLike)
  }

}

// MARK: - DataReply
struct ReplyChatModel: Codable {
    let media: String?
    let created: Int?
    let sender: Sender?
//    let reply: ChatGroup?
    let edited, messageID, messageType, groupID: Int?
    let message: String?

    enum CodingKeys: String, CodingKey {
        case media, created, sender, edited
        case messageID = "message_id"
        case messageType = "message_type"
        case groupID = "group_id"
        case message
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        media = try values.decodeIfPresent(String.self, forKey: .media)
        created = try values.decodeIfPresent(Int.self, forKey: .created)
        sender = try values.decodeIfPresent(Sender.self, forKey: .sender)
        edited = try values.decodeIfPresent(Int.self, forKey: .edited)
        messageID = try values.decodeIfPresent(Int.self, forKey: .messageID)
        messageType = try values.decodeIfPresent(Int.self, forKey: .messageType)
        groupID = try values.decodeIfPresent(Int.self, forKey: .groupID)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}

// MARK: - Sender
struct Sender: Codable {
    let avatar: String?
    let displayName: String?
    let id: Int?
//    let babyInfo: [JSONAny]
    let isFollow: Int?

    enum CodingKeys: String, CodingKey {
        case avatar
        case displayName = "display_name"
        case id
//        case babyInfo = "baby_info"
        case isFollow = "is_follow"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
        displayName = try values.decodeIfPresent(String.self, forKey: .displayName)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        isFollow = try values.decodeIfPresent(Int.self, forKey: .isFollow)
    }
}

public final class SenderNotifyModel: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let avatar = "avatar"
        static let displayName = "display_name"
        static let id = "id"
        static let isFollow = "is_follow"
    }
    
    // MARK: Properties
    public var avatar: String?
    public var displayName: String?
    public var id: Int?
    public var isFollow: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        avatar = json[SerializationKeys.avatar].string
        displayName = json[SerializationKeys.displayName].string
        id = json[SerializationKeys.id].int
        isFollow = json[SerializationKeys.isFollow].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = avatar { dictionary[SerializationKeys.avatar] = value }
        if let value = displayName { dictionary[SerializationKeys.displayName] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = isFollow { dictionary[SerializationKeys.isFollow] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.avatar = aDecoder.decodeObject(forKey: SerializationKeys.avatar) as? String
        self.displayName = aDecoder.decodeObject(forKey: SerializationKeys.displayName) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.isFollow = aDecoder.decodeObject(forKey: SerializationKeys.isFollow) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(avatar, forKey: SerializationKeys.avatar)
        aCoder.encode(displayName, forKey: SerializationKeys.displayName)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(isFollow, forKey: SerializationKeys.isFollow)
    }
    
}


struct NewMessageModel: Codable {
    let message: ChatGroup?
    let group: NewGroupModel?
    enum CodingKeys: String, CodingKey {
        case message
        case group
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(ChatGroup.self, forKey: .message)
        group = try values.decodeIfPresent(NewGroupModel.self, forKey: .group)
    }
}
