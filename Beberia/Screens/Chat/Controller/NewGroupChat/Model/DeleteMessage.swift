//
//  DeleteMessage.swift
//  Beberia
//
//  Created by haiphan on 04/03/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

// MARK: - DataClass
struct DeleteMessageModel: Codable {
    let messageId: Int?
    let group: NewGroupModel?

    enum CodingKeys: String, CodingKey {
        case messageId = "message_id"
        case group = "group"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        messageId = try values.decodeIfPresent(Int.self, forKey: .messageId)
        group = try values.decodeIfPresent(NewGroupModel.self, forKey: .group)
        
    }
}
