//
//  ChatMembers.swift
//  Beberia
//
//  Created by haiphan on 27/02/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
// MARK: - DataClass
struct ChatMembersModel: Codable {
    let list: [ChatMemberModel]?
    let nextPage: Int?

    enum CodingKeys: String, CodingKey {
        case list
        case nextPage = "next_page"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([ChatMemberModel].self, forKey: .list)
        nextPage = try values.decodeIfPresent(Int.self, forKey: .nextPage)
    }
}

// MARK: - List
struct ChatMemberModel: Codable {
    let id: Int?
    let displayName, avatar: String?
//    let babyInfo: [JSONAny]
    let isFollow, isOnline: Int?
    let timeOnline: Double?

    enum CodingKeys: String, CodingKey {
        case id
        case displayName = "display_name"
        case avatar
//        case babyInfo = "baby_info"
        case isFollow = "is_follow"
        case isOnline = "is_online"
        case timeOnline = "time_online"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        displayName = try values.decodeIfPresent(String.self, forKey: .displayName)
        avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
        isFollow = try values.decodeIfPresent(Int.self, forKey: .isFollow)
        isOnline = try values.decodeIfPresent(Int.self, forKey: .isOnline)
        timeOnline = try values.decodeIfPresent(Double.self, forKey: .timeOnline)
        
    }
}
