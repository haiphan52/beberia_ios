//
//  NewGroupModel.swift
//  Beberia
//
//  Created by haiphan on 24/02/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import SwiftyJSON
//// MARK: - DataClass
struct NewGroupsModel: Codable {
    let list: [NewGroupModel]?
    let nextPage: Int?

    enum CodingKeys: String, CodingKey {
        case list
        case nextPage = "next_page"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([NewGroupModel].self, forKey: .list)
        nextPage = try values.decodeIfPresent(Int.self, forKey: .nextPage)
    }
}

// MARK: - List
struct NewGroupModel: Codable {
    let groupID: Int?
    let groupName: String?
    let numberMember: Int?
    let coverURL: String?
    let members: [MemberGroupModel]?
    let data: [DataGroupModel]?
    let lastMessage: LastMessageNewModel?
//    let messagePin: MessagePin
    var numberUnread: Int?
    let notificationOff: Int?

    enum CodingKeys: String, CodingKey {
        case groupID = "group_id"
        case groupName = "group_name"
        case numberMember = "number_member"
        case coverURL = "cover_url"
        case members = "members"
        case data
        case lastMessage = "last_message"
//        case messagePin = "message_pin"
        case numberUnread = "number_unread"
        case notificationOff = "notification_off"
    }
//    public init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        groupID = try values.decodeIfPresent(Int.self, forKey: .groupID)
//        groupName = try values.decodeIfPresent(String.self, forKey: .groupName)
//        numberMember = try values.decodeIfPresent(Int.self, forKey: .numberMember)
//        coverURL = try values.decodeIfPresent(String.self, forKey: .coverURL)
//        lastMessage = try values.decodeIfPresent(LastMessageNewModel.self, forKey: .lastMessage)
//        members = try values.decodeIfPresent([MemberGroupModel].self, forKey: .members)
//        numberUnread = try values.decodeIfPresent(Int.self, forKey: .numberUnread)
//        data = try values.decodeIfPresent([DataGroupModel].self, forKey: .data)
//    }
    func getNotify() -> Bool {
        if self.notificationOff == 0 {
            return true
        }
        return false
    }
}

public final class GroupNotifyModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let groupName = "group_name"
    static let sender = "sender"
    static let numberMember = "number_member"
    static let groupID = "group_id"
    static let coverURL = "cover_url"
    static let numberUnread = "number_unread"
      static let notificationOff = "notification_off"
//    static let message = "message"
  }

  // MARK: Properties
  public var groupName: String?
  public var sender: SenderNotifyModel?
  public var numberMember: Int?
  public var numberLike: Int?
  public var groupID: Int?
  public var coverURL: String?
  public var numberUnread: Int?
    public var notificationOff: Int?
//    public var message: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
      groupName = json[SerializationKeys.groupName].string
      sender = SenderNotifyModel(json: json[SerializationKeys.sender])
      numberMember = json[SerializationKeys.numberMember].int
      numberUnread = json[SerializationKeys.numberUnread].int
      groupID = json[SerializationKeys.groupID].int
      coverURL = json[SerializationKeys.coverURL].string
      notificationOff = json[SerializationKeys.notificationOff].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = groupName { dictionary[SerializationKeys.groupName] = value }
    if let value = sender { dictionary[SerializationKeys.sender] = value.dictionaryRepresentation() }
    if let value = numberMember { dictionary[SerializationKeys.numberMember] = value }
    if let value = numberUnread { dictionary[SerializationKeys.numberUnread] = value }
    if let value = groupID { dictionary[SerializationKeys.groupID] = value }
    if let value = coverURL { dictionary[SerializationKeys.coverURL] = value }
      if let value = notificationOff { dictionary[SerializationKeys.notificationOff] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.groupName = aDecoder.decodeObject(forKey: SerializationKeys.groupName) as? String
    self.sender = aDecoder.decodeObject(forKey: SerializationKeys.sender) as? SenderNotifyModel
    self.numberMember = aDecoder.decodeObject(forKey: SerializationKeys.numberMember) as? Int
    self.groupID = aDecoder.decodeObject(forKey: SerializationKeys.groupID) as? Int
    self.coverURL = aDecoder.decodeObject(forKey: SerializationKeys.coverURL) as? String
    self.numberUnread = aDecoder.decodeObject(forKey: SerializationKeys.numberUnread) as? Int
      self.notificationOff = aDecoder.decodeObject(forKey: SerializationKeys.notificationOff) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(groupName, forKey: SerializationKeys.groupName)
    aCoder.encode(sender, forKey: SerializationKeys.sender)
    aCoder.encode(numberMember, forKey: SerializationKeys.numberMember)
    aCoder.encode(groupID, forKey: SerializationKeys.groupID)
    aCoder.encode(coverURL, forKey: SerializationKeys.coverURL)
    aCoder.encode(numberUnread, forKey: SerializationKeys.numberUnread)
      aCoder.encode(notificationOff, forKey: SerializationKeys.notificationOff)
  }

}

public final class MemberbersNotifyModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
      static let displayName = "display_name"
      static let isFollow = "is_follow"
      static let avatar = "avatar"
  }

  // MARK: Properties
  public var id: Int?
    public var displayName: String?
    public var isFollow: Int?
    public var avatar: String?
//    public var message: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
      id = json[SerializationKeys.id].int
      displayName = json[SerializationKeys.displayName].string
      isFollow = json[SerializationKeys.isFollow].int
      avatar = json[SerializationKeys.avatar].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
      if let value = displayName { dictionary[SerializationKeys.displayName] = value }
      if let value = isFollow { dictionary[SerializationKeys.isFollow] = value }
      if let value = avatar { dictionary[SerializationKeys.avatar] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
      self.displayName = aDecoder.decodeObject(forKey: SerializationKeys.displayName) as? String
      self.isFollow = aDecoder.decodeObject(forKey: SerializationKeys.isFollow) as? Int
      self.avatar = aDecoder.decodeObject(forKey: SerializationKeys.avatar) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(id, forKey: SerializationKeys.id)
      aCoder.encode(displayName, forKey: SerializationKeys.displayName)
      aCoder.encode(isFollow, forKey: SerializationKeys.isFollow)
      aCoder.encode(avatar, forKey: SerializationKeys.avatar)
  }

}
// MARK: - LastMessage
struct LastMessageNewModel: Codable {
    let messageID: Int?
    let message, media: String?
    let messageType: Int?
//    let sender, reply: MessagePin?
    let created, groupID: Double?

    enum CodingKeys: String, CodingKey {
        case messageID = "message_id"
        case message, media
        case messageType = "message_type"
//        case sender, reply
        case created
        case groupID = "group_id"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        messageID = try values.decodeIfPresent(Int.self, forKey: .messageID)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        media = try values.decodeIfPresent(String.self, forKey: .media)
        messageType = try values.decodeIfPresent(Int.self, forKey: .messageType)
        created = try values.decodeIfPresent(Double.self, forKey: .created)
        groupID = try values.decodeIfPresent(Double.self, forKey: .groupID)
    }
}

// MARK: - Member
struct MemberGroupModel: Codable {
    let id: Int?
    let displayName: String?
//    let babyInfo: [JSONAny]
    let isFollow: Int?
    let avatar: String?

    enum CodingKeys: String, CodingKey {
        case id
        case displayName = "display_name"
//        case babyInfo = "baby_info"
        case isFollow = "is_follow"
        case avatar
    }
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        displayName = try values.decodeIfPresent(String.self, forKey: .displayName)
        isFollow = try values.decodeIfPresent(Int.self, forKey: .isFollow)
        avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
    }
}

// MARK: - Datum
struct DataGroupModel: Codable {
    let groupID: Int?
    let thumbnail: String?
    let link: String?

    enum CodingKeys: String, CodingKey {
        case groupID = "group_id"
        case thumbnail, link
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        groupID = try values.decodeIfPresent(Int.self, forKey: .groupID)
        thumbnail = try values.decodeIfPresent(String.self, forKey: .thumbnail)
        link = try values.decodeIfPresent(String.self, forKey: .link)
    }
}
