//
//  NewGroupChatViewController.swift
//  Beberia
//
//  Created by IMAC on 6/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Alamofire
import SVProgressHUD

class NewGroupChatViewController: BaseViewController {
    
    @IBOutlet weak var heightViewHeader: NSLayoutConstraint!
    @IBOutlet weak var heightBtnSelectedImage: NSLayoutConstraint!
    @IBOutlet weak var btnSelectedUserDone: UIButton!
    @IBOutlet weak var btnSelectedImageGroup: UIButton!
    @IBOutlet weak var tfNameGroup: UITextField!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var clvSelectedUser: UICollectionView!
    @IBOutlet weak var tbvSelectedUser: UITableView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    
    var groupChat: NewGroupModel?
    var isFromViewAddFriend = false
    var arrayUser = BehaviorRelay.init(value: [User]())
    
    let arrayUserSelected = BehaviorRelay.init(value: [User]())
    var arrayUserSelected1 = [User]()
    var arrayIDUser: [Int] = []
    var page = 1
    var keySearch = ""
    let imageSelected = BehaviorRelay<[UIImage]>(value: [])
    private var imagePicker: ImagePicker!
    var dataImage = Data()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customLeftBarButton()
        initRx()
        setupUI()
        searchNameFriend(key: "", page: 1)
        self.title = R.string.localizable.chatTitleNewGroup()
        imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupUI(){
        if isFromViewAddFriend{
            btnSelectedUserDone.isHidden = true
            tfNameGroup.isHidden = true
            heightViewHeader.constant = 110
            heightBtnSelectedImage.constant = 0
            self.title = "Thêm bạn bè"
        }else{
            btnSelectedUserDone.isHidden = false
            tfNameGroup.isHidden = false
            heightViewHeader.constant = 170
            heightBtnSelectedImage.constant = 60
            
        }
    }
    
    func initRx(){
        
        // init TableView
        tbvSelectedUser.register(R.nib.selectUserTableViewCell)
        tbvSelectedUser.tableFooterView = UIView()
        tbvSelectedUser.separatorStyle = .none
        tbvSelectedUser.showsVerticalScrollIndicator = false
        tbvSelectedUser.addInfiniteScroll { (tableView) in
            if self.page != 0{
                self.page += 1
                self.searchNameFriend(key: self.keySearch, page: self.page)
            }else{
                self.tbvSelectedUser.endRefreshing(at: .top)
                self.tbvSelectedUser.finishInfiniteScroll(completion: { (collection) in
                })
            }
        }
        
        clvSelectedUser.register(R.nib.selectUserCollectionViewCell)
        let flowLayoutAfterBefore = UICollectionViewFlowLayout()
        flowLayoutAfterBefore.itemSize = CGSize(width: 60, height: 60)
        flowLayoutAfterBefore.scrollDirection = .horizontal
        flowLayoutAfterBefore.minimumInteritemSpacing = 6
        flowLayoutAfterBefore.minimumLineSpacing = 6
        flowLayoutAfterBefore.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvSelectedUser.setCollectionViewLayout(flowLayoutAfterBefore, animated: true)
        clvSelectedUser.showsHorizontalScrollIndicator = false
        
        // Action seach user
        tfSearch.rx.text.orEmpty.asObservable().skip(1).distinctUntilChanged().debounce(.milliseconds(500), scheduler: MainScheduler.asyncInstance).subscribe { (text) in
            self.arrayUser.accept([])
            self.page = 1
            self.keySearch = text.element ?? ""
            self.searchNameFriend(key: self.keySearch, page: self.page)
        }.disposed(by: disposeBag)
        
        //bind data to TableView
        self.arrayUser.asObservable().observeOn(MainScheduler.asyncInstance)
            .bind(to: tbvSelectedUser.rx.items(cellIdentifier: R.reuseIdentifier.selectUserTableViewCell.identifier, cellType: SelectUserTableViewCell.self)) { [weak self]  row, data, cell in
                guard let self = self else { return }
                
                cell.nameUserLable.text = data.displayName ?? ""
                cell.timeOnlineLable.text = Utils.getDateFromTimeStamp(timeStamp: Double(data.timeOnline! * 1000), dateFormat: Key.DateFormat.DateFormatddMMyyyy)
                cell.imgAvatar.kf.setImage(with: URL.init(string: data.avatar ?? ""))
                
                cell.imgSelected.image = R.image.icUnSelectedUser1()
                if data.isSelected {
                    cell.imgSelected.image = R.image.icSelectedUser1()
                    
                    // add userId selected to array
                    self.arrayIDUser.append(data.id ?? 0)
                }
            }.disposed(by: disposeBag)
        
        //bind data to CollectionView
        
        arrayUserSelected.asObservable().observeOn(MainScheduler.asyncInstance)
            .bind(to: clvSelectedUser.rx.items(cellIdentifier: R.reuseIdentifier.selectUserCollectionViewCell.identifier, cellType: SelectUserCollectionViewCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                
                cell.imageAvatarUser.kf.setImage(with: URL.init(string: data.avatar ?? ""))
                
                // handel remove user CollectionView
                cell.btnRemoveUser.rx.tap.subscribe { (_) in
                    
                    self?.clvSelectedUser.collectionViewLayout.invalidateLayout()
                    
                    /////
                    self?.arrayUserSelected1.remove(at: row)
                    self?.arrayUserSelected.accept(self!.arrayUserSelected1)
                    
                    /////
                    var array = self?.arrayUser.value
                    let data = data
                    let index = array!.firstIndex(where: { (user) -> Bool in
                        return user.id == data.id
                    })
                    if index != nil {
                        data.isSelected = false
                        array![index!] = data
                        self?.arrayUser.accept(array!)
                    }
                    
                    
                }.disposed(by: cell.bag)
                
            }.disposed(by: disposeBag)
        
        
        //selected TableView
        Observable
            .zip(tbvSelectedUser.rx.itemSelected, tbvSelectedUser.rx.modelSelected(User.self))
            .bind { [weak self] indexPath, item in
                guard let self = self else { return }
                
                self.clvSelectedUser.collectionViewLayout.invalidateLayout()
                
                // reset arry UserID
                self.arrayIDUser.removeAll()
                
                var array = self.arrayUser.value
                let item = item
                item.isSelected = !item.isSelected
                array[indexPath.row] = item
                self.arrayUser.accept(array)
                
                if item.isSelected {
                    self.arrayUserSelected1.insert(item, at: 0)
                    self.arrayUserSelected.accept(self.arrayUserSelected1)
                }else{
                    let index = self.arrayUserSelected1.firstIndex(where: { (user) -> Bool in
                        return user.id == item.id
                    })
                    if index != nil {
                        self.arrayUserSelected1.remove(at: index!)
                    }
                    self.arrayUserSelected.accept(self.arrayUserSelected1)
                }
            }
            .disposed(by: disposeBag)
        
        // Action Select Image Group
        btnSelectedImageGroup.rx.tap.subscribe { (_) in
            self.showViewSelectImageGroup()
        }.disposed(by: disposeBag)
        
        // Action Create Group Chat
        btnSelectedUserDone.rx.tap.subscribe { [weak self] (_) in
            guard let wSelf = self else { return }
            if wSelf.isFromViewAddFriend {
                if let group = wSelf.groupChat, let groupId = group.groupID {
                    wSelf.addMembers(groupId: groupId, userId: wSelf.stringUserId())
                }
//                SendBirdManager.share.addMembers(userIds: wSelf.arrayIDUser, groupChannel: wSelf.groupChannel!) {
//                    Utils.showAlertOKWithAction(controller: wSelf, title: R.string.localizable.commonNoti(), message: "Thêm bạn thành công!", cancel: false) {
//                        wSelf.navigationController?.popViewController(animated: true)
//                    }
//                }
            }else{
                guard let nameGroup = wSelf.tfNameGroup.text, nameGroup.count > 0,
                      var cover = wSelf.btnSelectedImageGroup.imageView?.image else {
                    let viewToast = ViewToastPopup.instanceFromNib()
                    viewToast.titleLable.text = R.string.localizable.chatErrorMissNameGroup()
                    wSelf.showToastPopup(view: viewToast)
                    return
                }
                
//                                                SendBirdManager.share.createChannel(nameGroup: nameGroup, userIds: wSelf.arrayIDUser, dataImage: wSelf.dataImage)
//                                                SendBirdManager.share.createChaneSuccess = { groupChannel in
//                                                    print(groupChannel)
//                                                    let listMessage = ListMessageViewController.init(nib: R.nib.listMessageViewController)
//                                                    listMessage.groupChannel = groupChannel
//                                                    wSelf.navigationController?.pushViewController(listMessage, animated: true)
//                                                }
                if cover.isEqualToImage(image:  R.image.icAvatarGroupDefaul()!) {
                    cover = R.image.placeholder()!
                }
                wSelf.craeteGroup(nameGroup: nameGroup, cover: cover)
            }
        }.disposed(by: disposeBag)
        
        // Show / Hide ColllectionView
        arrayUserSelected.subscribe { (value) in
            
            self.btnSelectedUserDone.isHidden = !(value.element!.count > 0)
            self.clvSelectedUser.isHidden = !(value.element!.count > 0)
            self.heightView.constant = value.element!.count > 0 ? 75 : 0
            UIView.animate(withDuration: 0.5) {
                self.updateViewConstraints()
                self.view.layoutIfNeeded()
            }
            
        }.disposed(by: disposeBag)
        
        // Acion send file image
        imageSelected.subscribe { (image) in
            
            if image.element!.count > 0{
                guard let data = image.element![0].jpegData(compressionQuality: 0.5) else { return }
                
                self.dataImage = data
                
                self.btnSelectedImageGroup.setImage(image.element![0], for: .normal)
            }
        }.disposed(by: disposeBag)
    }
    
    private func addMembers(groupId: Int, userId: String) {
        APIManager.addMembers(groupId: groupId, userId: userId) { [weak self] in
            guard let wSelf = self else { return }
            Utils.showAlertOKWithAction(controller: wSelf, title: R.string.localizable.commonNoti(), message: "Thêm bạn thành công!", cancel: false) {
                wSelf.navigationController?.popViewController(animated: true)
            }
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }

    }
    
    private func craeteGroup(nameGroup: String, cover: UIImage) {
        SVProgressHUD.show()
        let param: Parameters = [
            "user_ids": self.stringUserId(),
            "name": nameGroup,
            "cover": Key.HeaderJsonImage.HeaderJsonImage + (cover.convertImageTobase64(format: .jpeg(1), image: cover) ?? "")
        ]
        APIManager.createGroup(param: param) { [weak self] groupdChat in
            guard let wSelf = self else { return }
            let listMessage = ListMessageViewController.init(nib: R.nib.listMessageViewController)
            listMessage.groupdChat = groupdChat
            wSelf.navigationController?.pushViewController(listMessage, animated: true)
            SVProgressHUD.dismiss()
        } failed: { message in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }
    }
    
    private func stringUserId() -> String {
        let jsonEncode = Utils.json(from: self.arrayIDUser.map { "\($0)" })
        return jsonEncode ?? ""
    }
    
    func showViewSelectImageGroup(){
        let bottomVC = UpdateAvatarGroupChatViewController.init(nib: R.nib.updateAvatarGroupChatViewController)
        bottomVC.tapCamera = { type in
            bottomVC.dismiss(animated: true, completion: {
                self.imagePicker.presentPickerView(type: type)
            })
        }
        
        bottomVC.tapTakePhoto = { type in
            bottomVC.dismiss(animated: true, completion: {
                self.imagePicker.presentPickerView(type: type)
            })
        }
        bottomVC.height = 195
        bottomVC.topCornerRadius = 20
        bottomVC.presentDuration = 0.25
        bottomVC.dismissDuration = 0.25
        Utils.getTopMostViewController()?.navigationController?.present(bottomVC, animated: true)
    }
    
}

extension NewGroupChatViewController {
    func searchNameFriend(key: String, page : Int){
        APIManager.searchNameFriend(key: key, page: page, callbackSuccess: { (users, nextPage) in
            
            if self.page != 0 {
                var arrayUserTemp = self.arrayUser.value
                arrayUserTemp.append(contentsOf: users)
                self.arrayUser.accept(arrayUserTemp)
            }
            self.page = nextPage
            
            self.tbvSelectedUser.endRefreshing(at: .top)
            self.tbvSelectedUser.finishInfiniteScroll(completion: { (collection) in
            })
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            // SVProgressHUD.dismiss()
        }
    }
}

extension NewGroupChatViewController: ImagePickerDelegate {
    
    func didSelect(image: UIImage?, fileName: String?, fileUrl: String?) {
        guard let image = image else { return }
        imageSelected.accept([image])
        
    }
}

