//
//  InfoGroupChatTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 9/1/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class InfoGroupChatTableViewCell: UITableViewCell {

    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet weak var titleLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
