//
//  InfoGroupChatViewController.swift
//  Beberia
//
//  Created by IMAC on 9/1/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

struct DataGroup {
    let title: String
    let img : UIImage
}

protocol InfoGroupChatDelegate {
    func updatePush(status: Bool)
}

class InfoGroupChatViewController: BaseViewController {
    
    var delegate: InfoGroupChatDelegate?
    
    @IBOutlet weak var imgGroupName: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var heightActionGroup: NSLayoutConstraint!
    @IBOutlet weak var tbvActionGroup: UITableView!
    @IBOutlet weak var tbvActivityGroup: UITableView!
    @IBOutlet weak var heightTbvActivityGroup: NSLayoutConstraint!
    
    private final var bag = DisposeBag()
    private var dataActionGroup = BehaviorRelay.init(value: [DataGroup]())
    private var dataActivityGroup = BehaviorRelay.init(value: [DataGroup]())
    var isPushEnabled = true
    var groupdChat: NewGroupModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customLeftBarButton()
        title = groupdChat?.groupName
        lblTitle.text = groupdChat?.groupName
        imgGroupName.setImage(imageString: groupdChat?.coverURL ?? "")
        initRx()
        dumyData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func didPressBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension InfoGroupChatViewController {
    func dumyData(){
        let data = [
            DataGroup.init(title: "Thêm bạn vào nhóm", img: R.image.icAddFriendChat()!),
            DataGroup.init(title: "Xem thành viên", img: R.image.icViewMember()!)
        ]
        heightTbvActivityGroup.constant = CGFloat(data.count * 45)
        dataActivityGroup.accept(data)
        
        let data1 = [
            DataGroup.init(title: self.isPushEnabled ? "Tắt thông báo" : "Bật thông báo", img: R.image.icNotiChat()!),
            DataGroup.init(title: "Rời khỏi nhóm", img: R.image.icLeftGroup()!)
        ]
        
        heightActionGroup.constant = CGFloat(data1.count * 45)
        dataActionGroup.accept(data1)
        
    }
    
    private func initRx(){
        
        tbvActivityGroup.register(R.nib.infoGroupChatTableViewCell)
        tbvActivityGroup.tableFooterView = UIView()
        tbvActivityGroup.separatorStyle = .none
        tbvActivityGroup.rowHeight = 45
        
        self.dataActivityGroup.asObservable().observeOn(MainScheduler.asyncInstance).bind(to: tbvActivityGroup.rx.items){(tv, row, item) -> UITableViewCell in
            let cell = tv.dequeueReusableCell(withIdentifier: R.nib.infoGroupChatTableViewCell.identifier) as! InfoGroupChatTableViewCell
            cell.titleLable.text = item.title
            cell.imgImage.image = item.img
            cell.titleLable.textColor = UIColor.init(hexString: "484646")
            return cell
        }.disposed(by: bag)
        
        // ...................//
        tbvActionGroup.register(R.nib.infoGroupChatTableViewCell)
        tbvActionGroup.tableFooterView = UIView()
        tbvActionGroup.separatorStyle = .none
        tbvActionGroup.rowHeight = 45
        
        self.dataActionGroup.asObservable().observeOn(MainScheduler.asyncInstance).bind(to: tbvActionGroup.rx.items){(tv, row, item) -> UITableViewCell in
            let cell = tv.dequeueReusableCell(withIdentifier: R.nib.infoGroupChatTableViewCell.identifier) as! InfoGroupChatTableViewCell
            cell.titleLable.text = item.title
            cell.imgImage.image = item.img
            cell.titleLable.textColor = UIColor.init(hexString: "7a8190")
            return cell
        }.disposed(by: bag)
        
        tbvActionGroup.rx.itemSelected.subscribe { [weak self] (indexPath) in
            guard let wSelf = self else { return }
            guard let index  = indexPath.element else { return }
            wSelf.tbvActionGroup.deselectRow(at: index, animated: true)
            
            if index.row == 0 {
                let groupId = wSelf.groupdChat?.groupID ?? 0
                let status = (wSelf.isPushEnabled) ? 1 : 0
                APIManager.settingNotificationV2(groupId: groupId, status: status) { [weak self] _ in
                    guard let wSelf = self else { return }
                    wSelf.navigationController?.popViewController(animated: true)
                    let view = ViewToastPopup.instanceFromNib()
                    view.titleLable.text = (status == 0) ? "Đã bật thông báo " : "Đã tắt thông báo "
                    wSelf.showToastPopup(view: view)
                    wSelf.delegate?.updatePush(status: (status == 0) ? true : false)
                } failed: { message in
                    print(message)
                }
            }
            
            if index.row == 1 {
                Utils.showAlertOKWithAction(controller: Utils.getTopMostViewController()!, title: "Thông báo", message: "Bạn có muốn rời nhóm?", cancel: true) {
//                    SendBirdManager.share.leaveChannel(channel: self.groupChannel!, completion: {
//                        self.navigationController?.popToRootViewController(animated: true)
//                    })
                    guard let id = UserInfo.shareUserInfo.id, let group = wSelf.groupdChat, let groupId = group.groupID else {
                        return
                    }
                    let encodeString = ManageApp.shared.encodeString(usersIds: ["\(id)"])
                    wSelf.leaveGroup(usersID: encodeString, groupId: groupId)
                }
            }
        }.disposed(by: bag)
        
        
        tbvActivityGroup.rx.itemSelected.subscribe { (indexPath) in
            
            guard let index  = indexPath.element else { return }
            self.tbvActivityGroup.deselectRow(at: index, animated: true)
            
            if index.row == 0 {
                let chatVC = NewGroupChatViewController.init(nib: R.nib.newGroupChatViewController)
                chatVC.hidesBottomBarWhenPushed = true
                chatVC.groupChat = self.groupdChat
                chatVC.isFromViewAddFriend = true
                Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
            }
            
            if index.row == 1 {
                let viewMember = ViewMemberViewController.init(nib: R.nib.viewMemberViewController)
                viewMember.groupdChat = self.groupdChat
                Utils.getTopMostViewController()?.navigationController?.pushViewController(viewMember, animated: true)
            }

            //                if index == 2 {
            //                    self.setEnableNoti()
            //                }

        }.disposed(by: bag)
    }
    
    private func leaveGroup(usersID: String, groupId: Int) {
        APIManager.leaveGroup(usersID: usersID, groupId: groupId) { [weak self] user in
            guard let wSelf = self else { return }
            SocketIOChatManager.shared.chatSocketEvent.onNext(.leaveGroup(groupId))
            wSelf.navigationController?.popToRootViewController(animated: true)
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }

    }
}
