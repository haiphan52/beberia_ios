//
//  ListMessageViewController.swift
//  Beberia
//
//  Created by IMAC on 6/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import PullToRefresh
import IQKeyboardManagerSwift
import SwiftEntryKit
import SVProgressHUD
import Differentiator

enum CellTypeChat {
    case text
    case file
    case replyImage
}

enum CellTypeChatNew: Int {
    case normal = 1, file, admin
}

protocol ListMessageViewControllerDelegate {
    func updateGroup(group: NewGroupModel)
}

class ListMessageViewController: BaseViewController {
    
    struct Constant {
        static let fullMessage: Int = 0
    }
    var delegate: ListMessageViewControllerDelegate?
    
    @IBOutlet weak var lblNameGroupCahnnel: UILabel!
    @IBOutlet weak var imgGroupChannel: UIImageView!
    @IBOutlet weak var btnScrollToBottom: UIButton!
    @IBOutlet weak var btnNotiMessageUnReadLable: UIButton!
    @IBOutlet weak var notiMessageUnReadLable: UILabel!
    @IBOutlet weak var viewNotiMessageunRead: UIView!
    @IBOutlet weak var bottomViewInput: NSLayoutConstraint!
    @IBOutlet weak var widthImageReply: NSLayoutConstraint!
    @IBOutlet weak var imageReply: UIImageView!
    @IBOutlet weak var heightViewReply: NSLayoutConstraint!
    @IBOutlet weak var btnCloseViewRepply: UIButton!
    @IBOutlet weak var contentReplyLable: UILabel!
    @IBOutlet weak var replySenderLable: UILabel!
    @IBOutlet weak var viewReply: UIView!
    @IBOutlet weak var heightConstantBannerView: NSLayoutConstraint!
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var messageTf: UITextField!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tbvListMessage: UITableView!
    @IBOutlet weak var lblMemberCount: UILabel!
    
    var groupdChat: NewGroupModel?
    var listMessage = BehaviorRelay<[ChatGroup]>(value: [])
    private var imagePicker: ImagePicker!
    let imageSelected = BehaviorRelay<[UIImage]>(value: [])
    var fileName: String?
    let refresher = PullToRefresh()
    let isPushEnabled = BehaviorRelay<Bool>(value: false)
    fileprivate final var heightDictionary: [IndexPath : CGFloat] = [:]
    var metaArrayValue = [String: [String]]()
    var tapTableView = UITapGestureRecognizer()
    var tapLink = UITapGestureRecognizer()
    private var isKeySelectedImageSend = false
    private var dataImage = Data()
    private var fileUrl = String()
    private let viewTitle = ViewHeaderGroupChat.instanceFromNib()
    private var isLoadMessagesFromMessageId = false
    private var countUnread = 0
    private var loadMore = false
    let statusBar =  UIView()
    private var metaArrayLikeComment = [String: [String]]()
    private var nextPage: Int = 1
    private var replyId: Int = 0
    private var isProcessingLike: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
           
        self.setupUI()
        self.getListMessage()
        self.parseModelBanner()
        self.readMessageAPI()
        
        self.listMessage.accept([])
//        customNavication()
        self.customLeftBarButton()
        self.initRx()
//        self.loadMessage()
        if let group = self.groupdChat, let count = group.numberUnread {
            self.countUnread = count
            self.isPushEnabled.accept(group.getNotify())
        }
        
        self.viewNotiMessageunRead.isHidden = countUnread <= 0
        self.btnScrollToBottom.isHidden = countUnread <= 0
        self.notiMessageUnReadLable.text = "\(countUnread) tin nhắn chưa đọc"
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self, isFullImage: false)
//        self.groupChannel?.markAsRead()
        // get status notification
//        SendBirdManager.share.getEnablePushNoti(groupChannel: groupChannel!, competion: { value in
//            self.isPushEnabled.accept(value)
//        })
        
        self.hideShowViewReply(isShow: false, indexPath: 0)
        
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        self.tapTableView = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.tapTableView.numberOfTapsRequired = 1
        self.tbvListMessage.addGestureRecognizer(tapTableView)
        
        self.tapLink = UITapGestureRecognizer.init(target: self, action: #selector(tapLinkCell))
        self.tapLink.numberOfTapsRequired = 1
        self.tbvListMessage.addGestureRecognizer(tapLink)
        
        //Long Press
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        self.tbvListMessage.addGestureRecognizer(longPressGesture)
        
        // getListUserAdmin()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        
        self.view.isMultipleTouchEnabled = true
        self.view.isUserInteractionEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = true
        SVProgressHUD.dismiss()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // Send Message
    @IBAction func sendMessage(_ sender: Any) {
        guard let message = messageTf.text, !message.isEmpty else {
            return
        }
        guard let groupChannel = self.groupdChat, let groupId = groupChannel.groupID else { return }
        self.sendMessageAPI(message: message, replyID: self.replyId, messageType: .normal, groupId: groupId)
        
//        SendBirdManager.share.sendMessage(groupChannel: groupChannel, message: message, metaArrayValue: self.metaArrayValue, completion: { message in
//            self.listMessage.accept(self.listMessage.value + [message])
//            self.tbvListMessage.scrollToBottom()
//        })
        messageTf.text = ""
        // self.metaArrayValue = [String:[String]]()
        self.hideShowViewReply(isShow: false, indexPath: 0)
    }
    
    @IBAction func didPressAvatarCover(_ sender: Any) {
        self.isKeySelectedImageSend = false
        self.imagePicker = ImagePicker(presentationController: self, delegate: self, isFullImage: !self.isKeySelectedImageSend)
        self.imagePicker.present(from: self.viewTitle.imgAvatarUser)
    }
    
    @IBAction func didPressOption(_ sender: Any) {
        setupShowBottom()
    }
    
    @IBAction func didPressBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //  self.navigationController?.navigationBar.setBottomBorderColor(color: .lightGray)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func backVC() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
}
extension ListMessageViewController {
    
    private func setupUI() {
        guard let groupChannel = self.groupdChat else {
            return
        }
        self.lblNameGroupCahnnel.text = groupChannel.groupName
        self.lblMemberCount.text = "\(groupChannel.numberMember ?? 0) thành viên"
        self.imgGroupChannel.setImage(imageString: groupChannel.coverURL ?? "")
        
        // init TableView
        tbvListMessage.register(R.nib.mySelfChatTextTableViewCell)
        tbvListMessage.register(R.nib.mySelfChatImageTableViewCell)
        tbvListMessage.register(R.nib.chatTextTableViewCell)
        tbvListMessage.register(R.nib.chatImageTableViewCell)
        tbvListMessage.register(R.nib.userChatTextTableViewCell)
        tbvListMessage.register(R.nib.fileImageTableViewCell)
        
        tbvListMessage.allowsSelection = false
        tbvListMessage.estimatedRowHeight = UITableView.automaticDimension
        
        tbvListMessage.rx.setDelegate(self).disposed(by: disposeBag)
        tbvListMessage.addPullToRefresh(refresher) { [weak self] in
            guard let wSelf = self else { return }
            guard wSelf.nextPage > Constant.fullMessage else {
                wSelf.tbvListMessage.endRefreshing(at: .top)
                return
            }
            wSelf.getPreviousMessage()
            wSelf.loadMore = true
        }
    }
    
    private func initRx() {
        
        btnNotiMessageUnReadLable.rx.tap.subscribe { (_) in
            self.viewNotiMessageunRead.isHidden = true
        }.disposed(by: disposeBag)
        
        btnScrollToBottom.rx.tap.subscribe { (_) in
            self.tbvListMessage.scrollToBottom()
            self.viewNotiMessageunRead.isHidden = true
        }.disposed(by: disposeBag)
        
        btnCloseViewRepply.rx.tap.subscribe { [weak self] (_) in
            guard let wSelf = self else { return }
            wSelf.hideShowViewReply(isShow: false, indexPath: 0)
            wSelf.replyId = 0
        }.disposed(by: disposeBag)
        
        // Bind Data to TableView
        self.listMessage.asObservable().bind(to: self.tbvListMessage.rx.items){(tv, row, item) -> UITableViewCell in
            guard let messageType = item.messageType, let typeCell = CellTypeChatNew(rawValue: messageType) else {
                return UITableViewCell.init()
            }
//            let isLike = self.isCheckLikeComment(item: item)
            switch typeCell {
            case .normal, .admin:
                let cell = tv.dequeueReusableCell(withIdentifier: R.nib.userChatTextTableViewCell.identifier) as! UserChatTextTableViewCell

                //cell.viewContainer.removeGradientLayer()

                cell.setupData(item: item, isLike: (item.getIsLike(), (item.numberLike ?? 0)), typeCell: typeCell)

                cell.tapAvatar = {
                    self.pushToViewProfile(userID: Int((item.sender!.id ?? 0)) )
                }

                cell.tapMessage = { [weak self] in
                    guard let wSelf = self else { return }
                    wSelf.moveToIndexWhenTap(item: wSelf.listMessage.value[row])
                }

                cell.tapLike = { [weak self] in
//                    self.addMetaArraylikeComment(message: self.listMessage.value[row])
                    guard let wSelf = self else { return }
                    if !wSelf.isProcessingLike {
                        wSelf.likeMessage(messageId: Int((item.messageID ?? 0)), groupId: (wSelf.groupdChat?.groupID ?? 0), isLike: (item.isLike ?? 0), index: row)
                    }
                }

                return cell
                
            case .file:
                let cell = tv.dequeueReusableCell(withIdentifier: R.nib.fileImageTableViewCell.identifier) as! FileImageTableViewCell
                cell.setupData(item: item, isLike: (item.getIsLike(), (item.numberLike ?? 0)), typeCell: typeCell)

                cell.tapAvatar = {
                    self.pushToViewProfile(userID: Int((item.sender!.id ?? 0)))
                }

                cell.tapImage = { [weak self] in
                    guard let wSelf = self, let link = item.media else { return }
                    let viewImageVC = ViewImageViewController.init(nib: R.nib.viewImageViewController)
                    viewImageVC.url = link
                    wSelf.navigationController?.present(viewImageVC, animated: true, completion: nil)
                }

                cell.tapLike = { [weak self] in
//                    self.addMetaArraylikeComment(message: self.listMessage.value[row])
                    guard let wSelf = self else { return }
                    if !wSelf.isProcessingLike {
                        wSelf.likeMessage(messageId: Int((item.messageID ?? 0)), groupId: (wSelf.groupdChat?.groupID ?? 0), isLike: (item.isLike ?? 0), index: row)
                    }
                }

                return cell
            }
            
        }.disposed(by: disposeBag)
        
//        self.listMessage.asObservable().observeOn(MainScheduler.asyncInstance).bind(to: tbvListMessage.rx.items){(tv, row, item) -> UITableViewCell in
//            let typeCell = self.checkTypeCell(item: item)
//            let isLike = self.isCheckLikeComment(item: item)
//
//            switch typeCell {
//            case .text, .replyImage:
//                let cell = tv.dequeueReusableCell(withIdentifier: R.nib.userChatTextTableViewCell.identifier) as! UserChatTextTableViewCell
//
//                //cell.viewContainer.removeGradientLayer()
//
//                cell.setupData(item: item, isLike: isLike, typeCell: typeCell)
//
//                cell.tapAvatar = {
//                    self.pushToViewProfile(userID: Int((item as! SBDUserMessage).sender!.userId) ?? 0)
//                }
//
//                cell.tapMessage = {
//                    if let content = item.metaArrays(withKeys: [Key.MessageID]).first , content.value.count > 0{
//                        if content.value[0].count > 0{
//                            print(content.value[0])
//                            let index = self.listMessage.value.firstIndex(where: { $0.messageId == Int64(content.value[0])})
//                            if index != nil {
//                                //    print(index)
//
//                                let pathToLastRow = IndexPath.init(row: index!, section: 0)
//                                if self.tbvListMessage.indexPathExists(indexPath: pathToLastRow)
//                                {
//                                    self.tbvListMessage.scrollToRow(at: pathToLastRow, at: .middle, animated: false)
//                                }
//                            }
//                        }
//                    }
//                }
//
//                cell.tapLike = {
//                    self.addMetaArraylikeComment(message: self.listMessage.value[row])
//                }
//
//                return cell
//            case .file:
//                let cell = tv.dequeueReusableCell(withIdentifier: R.nib.fileImageTableViewCell.identifier) as! FileImageTableViewCell
//                cell.setupData(item: item, isLike: isLike, typeCell: typeCell)
//
//                cell.tapAvatar = {
//                    self.pushToViewProfile(userID: Int((item as! SBDFileMessage).sender!.userId) ?? 0)
//                }
//
//                cell.tapImage = {
//                    let viewImageVC = ViewImageViewController.init(nib: R.nib.viewImageViewController)
//                    viewImageVC.url = (item as! SBDFileMessage).url
//                    self.navigationController?.present(viewImageVC, animated: true, completion: nil)
//
//                }
//
//                cell.tapLike = {
//                    self.addMetaArraylikeComment(message: self.listMessage.value[row])
//                }
//
//                return cell
//            }
//        }.disposed(by: disposeBag)
        
        tbvListMessage.rx
            .willDisplayCell
               .subscribe(onNext: { cell, indexPath in
                guard let cell1 = cell as? UserChatTextTableViewCell else { return }
             //   cell1.layer.insertSublayer(self.gradient(frame: cell1.viewContainer.bounds), at:0)
             //   cell.backgroundColor = UIColor.clear
             //   cell.textLabel?.textColor = UIColor(red:0, green:0.102, blue: 0.2, alpha: 1)
             //   cell1.viewContainer.removeGradientLayer()
             //   cell1.viewContainer.addGradientLayer([UIColor.init(hex: "fd8aaa"),UIColor.init(hex: "feb0c5")])
                    //   cell1.viewContainer.resizeGradientLayer()
                  })
               .disposed(by: disposeBag)
        
        // Action Send Message
//        SendBirdManager.share.sendMessageSuccess.subscribe { [weak self] (_) in
//            self?.messageTf.text = ""
//            self?.tbvListMessage.scrollToBottom()
//        }.disposed(by: disposeBag)
        
        // Action Show Library Select Photo
        cameraButton.rx.tap.subscribe { (_) in
            self.isKeySelectedImageSend = true
            self.imagePicker = ImagePicker(presentationController: self, delegate: self, isFullImage: !self.isKeySelectedImageSend, isScaleImage: false)
            self.imagePicker.present(from: self.cameraButton)
        }.disposed(by: disposeBag)
        
        // Acion send file image
        imageSelected.subscribe { (image) in
            guard let groupChannel = self.groupdChat else { return }
            
            if image.element!.count > 0 {
                if self.isKeySelectedImageSend{
//                    guard let data = image.element![0].jpegData(compressionQuality: 0.5) else { return }
//                    SendBirdManager.share.sendFile(groupChannel: groupChannel, data: data , fileName: self.fileName, metaArrayValue: self.metaArrayValue, completion: { message in
////                        self.listMessage.accept(self.listMessage.value + [message])
//                        self.tbvListMessage.scrollToBottom()
//                    })
                    guard let groupChannel = self.groupdChat, let groupId = groupChannel.groupID else { return }
                    let media = Key.HeaderJsonImage.HeaderJsonImage + (image.element![0].convertImageTobase64(format: .jpeg(1), image: image.element![0]) ?? "")
                    self.sendMessageAPI(message: "", replyID: 0, messageType: .file, groupId: groupId, media: media)
                } else{
                    guard let data = image.element![0].jpegData(compressionQuality: 0.7) else { return }
                    
                    self.dataImage = data
                    
//                    APISendBirdManager.updateLoadFile(fileData: self.dataImage, channel_url: groupChannel.channelUrl){ (channel) in
//                        self.viewTitle.imgAvatarUser.kf.setImage(with: URL.init(string: channel.coverUrl ?? ""), placeholder: R.image.placeholder())
//                    }
                }
            }
            
        }.disposed(by: disposeBag)
        
//        SendBirdManager.share.pushNotificationMessage.bind(to: self.isPushEnabled).disposed(by: self.disposeBag)
        
        SocketIOChatManager.shared.chatSocketEvent.asObservable().bind { [weak self] eventChat in
            guard let wSelf = self else { return }
            switch eventChat {
            case .onMessageReceived(let newmessage):
                guard let new = newmessage.message, let messageGroupId = new.groupID, let groupId = wSelf.groupdChat?.groupID, messageGroupId == groupId  else {
                    return
                }
                var list = wSelf.listMessage.value
                if (list.firstIndex(where: { $0.messageID == new.messageID }) == nil) {
                    list.append(new)
                    wSelf.listMessage.accept(list)
                    wSelf.tbvListMessage.scrollToBottom()
                }
            case .onMessageDelete(let deleteMessage):
                guard let messageId = deleteMessage.messageId else {
                    return
                }
                var list = wSelf.listMessage.value
                if let index = list.firstIndex(where: { $0.messageID == Double(messageId) }) {
                    list.remove(at: index)
                    wSelf.listMessage.accept(list)
                }
            case .onMessageUpdate, .leaveGroup, .onChannelChanged, .onGroupCreate: break
            }
        }.disposed(by: self.disposeBag)
        
    }
    
    private func moveToIndexWhenTap(item: ChatGroup) {
        if let reply = item.reply, let replyId = reply.messageID {
            if let index = self.listMessage.value.firstIndex(where: { $0.messageID == Double(replyId)}) {
                let pathToLastRow = IndexPath.init(row: index, section: 0)
                if self.tbvListMessage.indexPathExists(indexPath: pathToLastRow)
                {
                    self.tbvListMessage.scrollToRow(at: pathToLastRow, at: .middle, animated: false)
                }
            } else {
                self.getMesssageToScroll(item: item)
            }
        }
    }
    
    
    private func likeMessage(messageId: Int, groupId: Int, isLike: Int, index: Int) {
        self.isProcessingLike = true
        APIManager.likeMessage(groupId: groupId, messageId: messageId, isLike: isLike) { [weak self] item in
            guard let wSelf = self else { return }
            var list = wSelf.listMessage.value
            list[index].isLike = item.isLiked
            list[index].numberLike = item.number
            wSelf.listMessage.accept(list)
            wSelf.isProcessingLike = false
        } failed: { message in
            self.isProcessingLike = false
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }
    }
    
    private func deleteMessage(messageId: Int) {
        APIManager.deleteMessage(messageId: messageId) { [weak self] in
            guard let wSelf = self else { return }
            var list = wSelf.listMessage.value
            if let index = list.firstIndex(where: { $0.messageID == Double(messageId) }) {
                list.remove(at: index)
                wSelf.listMessage.accept(list)
            }
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }

    }
    
    private func readMessageAPI() {
        guard let group = self.groupdChat, let groupId = group.groupID else {
            return
        }
        APIManager.readMessage(groupId: groupId) { [weak self] in
            guard let wSelf = self else { return }
            wSelf.delegate?.updateGroup(group: group)
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }
    }
    
    private func sendMessageAPI(message: String, replyID: Int, messageType: CellTypeChatNew, groupId: Int, media: String = "") {
        APIManager.sendMessage(message: message, replyID: replyID, messageType: messageType, groupId: groupId, media: media) { [weak self] chatGroup in
            guard let wSelf = self else { return }
            var list = wSelf.listMessage.value
            list.append(chatGroup)
            wSelf.listMessage.accept(list)
            wSelf.messageTf.text = ""
            wSelf.tbvListMessage.scrollToBottom()
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }
    }
    
    private func getListMessage() {
        guard let group = self.groupdChat, let groupId = group.groupID, self.nextPage > Constant.fullMessage else {
            return
        }
        APIManager.listMesaage(nextPage: self.nextPage, groupId: groupId) { [weak self] chatgroup in
            guard let wSelf = self, let list = chatgroup.list, let nextPage = chatgroup.nextPage else { return }
            wSelf.listMessage.accept(list)
            wSelf.nextPage = nextPage
            let messages = wSelf.listMessage.value
            if messages.count < wSelf.countUnread {
                wSelf.getPreviousMessage()
            }else{
                wSelf.readMessageAPI()
                DispatchQueue.main.async {
                    let row =  messages.count - wSelf.countUnread - 1
                    if (0 <= row) && (row <=  wSelf.listMessage.value.count - 1) {
                        let pathToLastRow = IndexPath.init(row: row, section: 0)
                        if wSelf.tbvListMessage.indexPathExists(indexPath: pathToLastRow)
                        {
                            wSelf.tbvListMessage.scrollToRow(at: pathToLastRow, at: .middle, animated: false)
                        }
                    }
                }
            }
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }
    }
    
    private func getPreviousMessage() {
        guard let group = self.groupdChat, let groupId = group.groupID, self.nextPage > Constant.fullMessage else {
            return
        }
        APIManager.listMesaage(nextPage: self.nextPage, groupId: groupId) { [weak self] chatgroup in
            guard let wSelf = self, let listMessage = chatgroup.list, let nextPage = chatgroup.nextPage else { return }
            var listMessageTemp = wSelf.listMessage.value
            listMessageTemp.insert(contentsOf: listMessage, at: 0)
            wSelf.listMessage.accept(listMessageTemp)
            wSelf.nextPage = nextPage
            print("self.listMessage.value.count\(wSelf.listMessage.value.count)")
            
            if (listMessageTemp.count < wSelf.countUnread && listMessage.count > 0) {
                wSelf.getPreviousMessage()
            }
            else{
                
                if wSelf.loadMore {
                    wSelf.countUnread = listMessageTemp.count - listMessage.count
                }
                
                //    self.listMessage.accept(listMessageTemp)
                
                DispatchQueue.main.async {
                    let row = wSelf.listMessage.value.count - wSelf.countUnread - 1
                    if (0 <= row) && (row <=  wSelf.listMessage.value.count - 1) {
                        let pathToLastRow = IndexPath.init(row: row, section: 0)
                        if wSelf.tbvListMessage.indexPathExists(indexPath: pathToLastRow)
                        {
                            wSelf.tbvListMessage.scrollToRow(at: pathToLastRow, at: .middle, animated: false)
                        }
                    }
                }
                
                wSelf.readMessageAPI()
            }
            wSelf.tbvListMessage.endRefreshing(at: .top)
            wSelf.tbvListMessage.finishInfiniteScroll(completion: { (collection) in
            })
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }
    }
    
    private func getMesssageToScroll(item: ChatGroup) {
        guard let group = self.groupdChat, let groupId = group.groupID, self.nextPage > Constant.fullMessage else {
            return
        }
        APIManager.listMesaage(nextPage: self.nextPage, groupId: groupId) { [weak self] chatgroup in
            guard let wSelf = self, let listMessage = chatgroup.list, let nextPage = chatgroup.nextPage else { return }
            var listMessageTemp = wSelf.listMessage.value
            listMessageTemp.insert(contentsOf: listMessage, at: 0)
            wSelf.listMessage.accept(listMessageTemp)
            wSelf.nextPage = nextPage
            print("self.listMessage.value.count\(wSelf.listMessage.value.count)")
            
            if (listMessageTemp.count < wSelf.countUnread && listMessage.count > 0) {
                wSelf.getPreviousMessage()
            }
            else{
                
                if wSelf.loadMore {
                    wSelf.countUnread = listMessageTemp.count - listMessage.count
                }
                wSelf.readMessageAPI()
            }
            wSelf.tbvListMessage.endRefreshing(at: .top)
            wSelf.tbvListMessage.finishInfiniteScroll(completion: { (collection) in
            })
            wSelf.moveToIndexWhenTap(item: item)
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }
    }
    
    func parseModelBanner(){
        guard let listBanner = self.groupdChat?.data else {
            self.heightConstantBannerView.constant =  0
            self.bannerView.isHidden = true
            return
        }
        var arrayImage: [String] = []
        for banner in listBanner {
            arrayImage.append(banner.thumbnail ?? "")
        }
        let height = (Screen.width * 780 ) / 2050
        self.heightConstantBannerView.constant = listBanner.count > 0 ? height : 0
        self.bannerView.isHidden = listBanner.count == 0
        self.bannerView.arrayImage = arrayImage
        
        self.bannerView.tapBanner = { index in
            guard let url = URL.init(string: listBanner[index].link ?? "") else {
                return
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url , options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func tapLinkCell(_ tap: UITapGestureRecognizer){
//        let p = tap.location(in: self.tbvListMessage)
//        let indexPath = self.tbvListMessage.indexPathForRow(at: p)
//        if indexPath == nil {
//            print("Long press on table view, not row.")
//        } else {
//            print(indexPath!.row)
//            
//            let urlPattern = "(^|[\\s.:;?\\-\\]<\\(])" +
//                "((https?://|www\\.|pic\\.)[-\\w;/?:@&=+$\\|\\_.!~*\\|'()\\[\\]%#,☺]+[\\w/#](\\(\\))?)" +
//                "(?=$|[\\s',\\|\\(\\).:;?\\-\\[\\]>\\)])"
//            
//            let regex = try! NSRegularExpression(pattern: urlPattern, options: [])
//            
//            guard let meesage = self.listMessage.value[safe: indexPath!.row] as? SBDUserMessage, let input = meesage.message else {
//                return
//            }
//            
//            // let input = (self.listMessage.value[indexPath!.row] as! SBDUserMessage).message ?? ""
//            let range = NSRange(location: 0, length: input.utf16.count)
//            
//            for match in regex.matches(in: input, options: [], range: range) {
//                
//                let url1 = input.subStr(s: match.range.location, l: match.range.length).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
//                guard let url = URL(string: url1) else { return }
//                UIApplication.shared.open(url)
//                
//                break
//            }
//            
//        }
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        self.tbvListMessage.removeGestureRecognizer(tapLink)
        self.tbvListMessage.addGestureRecognizer(tapTableView)
        
        statusBar.frame = UIApplication.shared.statusBarFrame
        statusBar.backgroundColor = .white
        UIApplication.shared.keyWindow?.addSubview(statusBar)
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
        self.tbvListMessage.removeGestureRecognizer(tapTableView)
        self.tbvListMessage.addGestureRecognizer(tapLink)
    }

    @objc private func handleLongPress(longPressGesture: UILongPressGestureRecognizer) {
        
        self.dismissKeyboard()
        
        let p = longPressGesture.location(in: self.tbvListMessage)
        let indexPath = self.tbvListMessage.indexPathForRow(at: p)
        if indexPath == nil {
            print("Long press on table view, not row.")
        } else if longPressGesture.state == UIGestureRecognizer.State.began {
            print("Long press on row, at \(indexPath!.row)")
            showBottomAction(indexPath: indexPath!.row)
        }
    }
    
    func hideShowViewReply(isShow: Bool, indexPath: Int){
        // show view reply
        self.heightViewReply.constant = isShow ? 56 : 0
        self.viewReply.isHidden = !isShow
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
        self.widthImageReply.constant = 0
        
        if isShow {
            let item = self.listMessage.value[indexPath]
            if let messageType = item.messageType, let cellType = CellTypeChatNew(rawValue: messageType) {
                switch cellType {
                case .normal:
                    self.contentReplyLable.text = isShow ? "\((self.listMessage.value[indexPath]).message ?? "")" : ""
                    self.replySenderLable.text = isShow ? "Trả lời \((self.listMessage.value[indexPath]).sender?.displayName ?? "")" : ""
                case .file:
                    self.widthImageReply.constant = 44
                    self.imageReply.kf.setImage(with: URL.init(string: item.sender?.avatar ?? ""))
                    self.contentReplyLable.text = isShow ? "[Hình ảnh]" : ""
                    self.replySenderLable.text = isShow ? "Trả lời \(item.sender?.displayName ?? "")" : ""
                case .admin: break
                }
            }
        }
        
        if !isShow{
            self.metaArrayValue = [String:[String]]()
            self.replyId = 0
        }
    }
    
    @objc func setupShowBottom(){
        
        let infoGroup = InfoGroupChatViewController.init(nib: R.nib.infoGroupChatViewController)
//        infoGroup.groupChannel = groupChannel
        infoGroup.groupdChat = self.groupdChat
        infoGroup.isPushEnabled = isPushEnabled.value
        infoGroup.delegate = self
        self.navigationController?.pushViewController(infoGroup, animated: true)
        
    }
    
    
//    func loadMessage(){
//        SendBirdManager.share.loadMessage(groupChannel: groupChannel!, completion: { messages in
//            //     self.listMessage.accept(listMessage)
//
//            //   guard let messages = listMessage.element, messages.count > 0 else { return }
//
////            self.listMessage.accept(messages)
//
//            if messages.count < self.countUnread {
//                self.loadPreviousMessages()
//            }else{
//
//                if let _ = Utils.getTopMostViewController() as? ListMessageViewController{
//                    self.groupChannel?.markAsRead()
//                }
//
//                DispatchQueue.main.async {
//                    let row =  messages.count - self.countUnread - 1
//                    if (0 <= row) && (row <=  self.listMessage.value.count - 1) {
//                        let pathToLastRow = IndexPath.init(row: row, section: 0)
//                        if self.tbvListMessage.indexPathExists(indexPath: pathToLastRow)
//                        {
//                            self.tbvListMessage.scrollToRow(at: pathToLastRow, at: .middle, animated: false)
//                        }
//                    }
//                }
//            }
//
//        })
//    }
    
//    func loadPreviousMessages(){
//        SendBirdManager.share.loadPreviousMessages(completion: { listMessage in
//            var listMessageTemp = self.listMessage.value
////            listMessageTemp.insert(contentsOf: listMessage, at: 0)
//            self.listMessage.accept(listMessageTemp)
//
//            print("self.listMessage.value.count\(self.listMessage.value.count)")
//
//            if (listMessageTemp.count < self.countUnread && listMessage.count > 0) {
//                self.loadPreviousMessages()
//            }
//            else{
//
//                if self.loadMore {
//                    self.countUnread = listMessageTemp.count - listMessage.count
//                }
//
//                //    self.listMessage.accept(listMessageTemp)
//
//                DispatchQueue.main.async {
//                    let row = self.listMessage.value.count - self.countUnread - 1
//                    if (0 <= row) && (row <=  self.listMessage.value.count - 1) {
//                        let pathToLastRow = IndexPath.init(row: row, section: 0)
//                        if self.tbvListMessage.indexPathExists(indexPath: pathToLastRow)
//                        {
//                            self.tbvListMessage.scrollToRow(at: pathToLastRow, at: .middle, animated: false)
//                        }
//                    }
//                }
//
//                if let _ = Utils.getTopMostViewController() as? ListMessageViewController{
//                    self.groupChannel?.markAsRead()
//                }
//            }
//            self.tbvListMessage.endRefreshing(at: .top)
//            self.tbvListMessage.finishInfiniteScroll(completion: { (collection) in
//            })
//        })
//    }
    
//    func saveImage(rowImage: UIImage) -> UIImage {
//        let bottomImage = rowImage
//     //   let topImage = UIImage(named: "myLogo")!
//
//        let newSize = CGSize.init(width: 41, height: 41) // set this to what you need
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
//
//        bottomImage.draw(in: CGRect(origin: .zero, size: newSize))
//    //    topImage.draw(in: CGRect(origin: .zero, size: newSize))
//
//        let newImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//
//        return newImage!
//    }
    
    
    //MARK:  Show Bottom Action
    func showBottomAction(indexPath: Int){
        let item = self.listMessage.value[indexPath]
        //get the cell
//        if let cell = tbvListMessage.cellForRow(at: IndexPath(row: indexPath, section: 0)) as? UserChatTextTableViewCell {
//            //hide button
//
//
//            let rectOfCellInTableView = tbvListMessage.rectForRow(at: IndexPath(row: indexPath, section: 0))
//
//            let rectOfCellInSuperview = tbvListMessage.convert(rectOfCellInTableView, to: tbvListMessage.superview)
//
//            let image1  = self.view.snapshot(of: rectOfCellInSuperview)
//
//            let finalImage = saveImage(rowImage: image1)
//            //test final image
//          //  snapViewImage.image = finalImage
//        }
        
        var isCheckUserCurren = false
//        let isLike = self.isCheckLikeComment(item: self.listMessage.value[indexPath])
        
        
        
        // check user current
        if let messageType = item.messageType, let cellType = CellTypeChatNew(rawValue: messageType) {
            switch cellType {
            case .normal:
                isCheckUserCurren = String(item.sender?.id ?? 0) == String(UserInfo.shareUserInfo.id ?? 0)
            case .file:
                isCheckUserCurren = String(item.sender?.id ?? 0) == String(UserInfo.shareUserInfo.id ?? 0)
            case .admin: break
            }
        }
        
        let viewBottomAction = ViewReplyComment.instanceFromNib()
        viewBottomAction.viewDeleteMessage.isHidden = !isCheckUserCurren
        viewBottomAction.icHeart.image = ((item.isLike ?? 0) == 1) ? R.image.unHeart()! : R.image.heart()!
        viewBottomAction.titleLikeLable.text = ((item.isLike ?? 0) == 1) ? "Bỏ tim" : "Yêu thích"
        var attributes = EKAttributes.default
        attributes.screenBackground =  .color(color: UIColor.init(hexString: "000000", alpha: 0.4)!)
        attributes.position = .bottom
        // Animate in and out using default translation
        attributes.entryInteraction = .forward
        attributes.screenInteraction = .dismiss
        attributes.displayDuration = .infinity
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 10, offset: .zero))
        attributes.roundCorners = .all(radius: 15)
        // attributes.positionConstraints.size = .init(width: .offset(value: 20), height: .intrinsic)
        SwiftEntryKit.display(entry: viewBottomAction, using: attributes)
        
        //MARK:  Reply Message
        viewBottomAction.didTapLike = { [weak self] in
            guard let wSelf = self else { return }
            SwiftEntryKit.dismiss()
//            self.addMetaArraylikeComment(message: self.listMessage.value[indexPath])
            wSelf.likeMessage(messageId: Int((item.messageID ?? 0)), groupId: (wSelf.groupdChat?.groupID ?? 0), isLike: (item.isLike ?? 0), index: indexPath)
        }
        
        //MARK:  Reply Message
        viewBottomAction.didTapReply = { [weak self] in
            guard let wSelf = self else { return }
            SwiftEntryKit.dismiss()
            
            wSelf.hideShowViewReply(isShow: true, indexPath: indexPath)
            
            // chekc message is SBDUserMessage
            wSelf.replyId = Int(item.messageID ?? 0)
        }
        
        //MARK:  Copy Message
        viewBottomAction.didTapCopy = { [weak self] in
            guard let wSelf = self else { return }
            SwiftEntryKit.dismiss()
            UIPasteboard.general.string = "\(item.message ?? "")"
        }
        
        //MARK:  Delete Message
        viewBottomAction.didTapDelete = {
            
            SwiftEntryKit.dismiss(.displayed) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
                    Utils.showAlertOKWithAction(controller: Utils.getTopMostViewController()!,
                                                title: R.string.localizable.commonNoti(),
                                                message: "Bạn có chắc chắn xoá tin nhắn ?", cancel: true) { [weak self] in
                        guard let wSelf = self, let messageId = item.messageID else { return }
                        wSelf.deleteMessage(messageId: Int(messageId))
//                        SendBirdManager.share.deleteMessage(groupChannel: self.groupChannel!, message: self.listMessage.value[indexPath])
                    }
                }
            }
        }
    }
    
    func gradient(frame:CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = frame
        print(frame)
        layer.startPoint = CGPoint.init(x: 0, y: 0.5)//CGPointMake(0,0.5)
        layer.endPoint = CGPoint.init(x: 1, y: 0.5) //CGPointMake(1,0.5)
        layer.colors = [UIColor.init(hex: "fd8aaa"),UIColor.init(hex: "feb0c5")]
        return layer
    }
    
    func pushToViewProfile(userID: Int){
        let viewProfile = R.storyboard.main.profileViewController()
        viewProfile?.id = userID
        let naviProfile = UINavigationController.init(rootViewController: viewProfile!)
        naviProfile.modalPresentationStyle = .fullScreen
        self.present(naviProfile, animated: true, completion: nil)
    }
    
    // Chekc type Text or File
    func checkTypeCell(item: ChatGroup) -> CellTypeChatNew {
//        if item.isKind(of: SBDFileMessage.self){
//            return .file
//        }else {
//            if let content = item.metaArrays(withKeys: [Key.Image]).first {
//                if content.value.count > 0{
//                    return .replyImage
//                }
//            }
//            return .text
//        }
        guard let type = item.messageType, let cellType = CellTypeChatNew(rawValue: type) else {
            return .normal
        }
        return cellType
    }
    
    // Custom Navication
    func customNavication(){
        lblNameGroupCahnnel.text = self.groupdChat?.groupName
        lblMemberCount.text = "\(self.groupdChat?.numberMember ?? 0) thành viên"
        imgGroupChannel.setImage(imageString: self.groupdChat?.coverURL ?? "")
        
    }
    
    func getListUserAdmin(){
//        SendBirdManager.share.getListUserAdmin(groupChannel: groupChannel!)
    }
}

// MARK: ImagePickerDelegate
extension ListMessageViewController: ImagePickerDelegate {
    
    func didSelect(image: UIImage?, fileName: String?, fileUrl: String?) {
        guard let image = image else { return }
        self.fileName = fileName
        self.fileUrl = fileUrl ?? ""
        imageSelected.accept([image])
        
    }
}

// MARK: UITableViewDelegate

extension ListMessageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath] = cell.frame.size.height
        if indexPath.row + 1 == self.listMessage.value.count {
            print("do something")
            btnScrollToBottom.isHidden = true
            viewNotiMessageunRead.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return height ?? UITableView.automaticDimension
    }
}

extension Collection {
    
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

// MARK: Hide/Show Keyboard

extension ListMessageViewController {
    @objc
    func keyboardWillAppear(notification: NSNotification?) {
        
        if let keyboardFrame: NSValue = notification?.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            if(self.tbvListMessage.contentSize.height > keyboardHeight)
            {
                UIView.animate(withDuration: 0.25) {
                    self.bottomViewInput.constant = 0
                    self.view.layoutIfNeeded()
                }
            } else {
                UIView.animate(withDuration: 0.25) {
                    self.bottomViewInput.constant = -keyboardHeight
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        statusBar.backgroundColor = .clear
        UIView.animate(withDuration: 0.25) {
            self.bottomViewInput.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}

extension UIView {

    /// Create image snapshot of view.
    ///
    /// - Parameters:
    ///   - rect: The coordinates (in the view's own coordinate space) to be captured. If omitted, the entire `bounds` will be captured.
    ///   - afterScreenUpdates: A Boolean value that indicates whether the snapshot should be rendered after recent changes have been incorporated. Specify the value false if you want to render a snapshot in the view hierarchy’s current state, which might not include recent changes. Defaults to `true`.
    ///
    /// - Returns: The `UIImage` snapshot.

    func snapshot(of rect: CGRect? = nil, afterScreenUpdates: Bool = true) -> UIImage {
        return UIGraphicsImageRenderer(bounds: rect ?? bounds).image { _ in
            drawHierarchy(in: bounds, afterScreenUpdates: afterScreenUpdates)
        }
    }
}
extension ListMessageViewController: InfoGroupChatDelegate {
    func updatePush(status: Bool) {
        self.isPushEnabled.accept(status)
    }
}
