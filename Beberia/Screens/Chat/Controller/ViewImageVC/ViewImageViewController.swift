//
//  ViewImageViewController.swift
//  Beberia
//
//  Created by IMAC on 7/29/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ViewImageViewController: BaseViewController {

    @IBOutlet weak var imageChat: UIImageView!
    
    var url = ""
    var fileName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageChat.kf.setImage(with: URL.init(string: url))
    }

    @IBAction func didPressClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func didPressSaveImage(_ sender: Any) {
        guard let image = imageChat.image else {
            return
        }

        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)

    }
    
  //MARK: - Save Image callback

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {

        if let error = error {

            print(error.localizedDescription)

        } else {

            
            let view = ViewToastPopup.instanceFromNib()
            view.titleLable.text = "Lưu ảnh thành công!"
            self.showToastPopup(view: view)
        }
    }

}
