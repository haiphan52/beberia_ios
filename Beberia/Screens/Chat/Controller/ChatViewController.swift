//
//  ChatViewController.swift
//  Beberia
//
//  Created by IMAC on 6/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SwiftyJSON
import FBSDKCoreKit
import SVProgressHUD
import IQKeyboardManagerSwift
import FFPopup
import Alamofire

class ChatViewController: BaseViewController {
    
    struct Constant {
        static let distanceBesides: CGFloat = 16
    }
    
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var heightBannerView: NSLayoutConstraint!
    //    @IBOutlet weak var heightImg: NSLayoutConstraint!
//    @IBOutlet weak var imgGame: UIImageView!
    @IBOutlet weak var heightTbv: NSLayoutConstraint!
    @IBOutlet weak var clvGroupSuggest: UICollectionView!
    @IBOutlet weak var tbvChat: UITableView!
    @IBOutlet weak var btnAddGroupChat: UIButton!
    @IBOutlet weak var scollView: UIScrollView!
    @IBOutlet weak var heightViewCollectionView: NSLayoutConstraint!
    @IBOutlet weak var wAddView: NSLayoutConstraint!
    @IBOutlet weak var hAddView: NSLayoutConstraint!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var btAdd: UIButton!
    
//    final let litsChanel: BehaviorRelay<[SBDGroupChannel]> = BehaviorRelay.init(value: [SBDGroupChannel]())
    final let litsChanel: BehaviorRelay<[NewGroupModel]> = BehaviorRelay.init(value: [])
    fileprivate final let litsChanelSuggest: BehaviorRelay<[ChannelSBModel]> = BehaviorRelay.init(value: [ChannelSBModel]())
    private var listChanelSgest: [NewGroupModel] = []
    private let addGroupEvent: PublishSubject<Void> = PublishSubject.init()
    fileprivate final var bag = DisposeBag()
    fileprivate final var heightDictionary: [IndexPath : CGFloat] = [:]
    fileprivate final var isFristLoadView = true
    private var heightCell: CGFloat = 0
    private var nextPage = 1
    
    private var colos = [UIColor.init(hexString: "ffc440"),UIColor.init(hexString: "ff99bb"),UIColor.init(hexString: "48c7ff")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = .white
        self.setupUI()
        self.initRx()
//        SBDMain.add(self, identifier: "")
        self.showSuggestGroupView(isShow: false)
     //   self.getBannerUpdateApp()

        Utils.getAppDelegate().isCheckLogin.subscribe { (value) in
            guard let isLogin = value.element else {
                return
            }
            if isLogin{
                self.getGroupAPI()
//                self.connectSendBird()
                self.getBanner()
//                self.channelSyncManager()
                self.getList()
                self.handelClickNotification()
                self.getBannerEven()
//                FirebaseDataBaseRealtime.share.getValue(userId: UserInfo.shareUserInfo.id ?? 0)
//                FirebaseDataBaseRealtime.share.valueNewsFeed = { value in
//                    self.tabBarController?.tabBar.items?[1].badgeValue = value == 1 ? "" : nil
//                }
                self.showPopupMiniGmae()
                SocketIOChatManager.shared.configureSocketClient(userID: UserInfo.shareUserInfo.id)
              //  self.showImageGame()
                
            }else{ }
        }.disposed(by: disposeBag)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleEvent), name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        IQKeyboardManager.shared.enableAutoToolbar = true
//        IQKeyboardManager.shared.enable = true
        
  //      IQKeyboardManager.shared.enabledDistanceHandlingClasses.append(ListMessageViewController.self)
   //     IQKeyboardManager.shared.enabledToolbarClasses.append(ListMessageViewController.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  self.navigationController?.navigationBar.setBottomBorderColor(color: .lightGray)
        self.navigationController?.isNavigationBarHidden = false
        self.customNavication()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ManageApp.shared.hideImageEvent.onNext(())
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
}

extension ChatViewController {
    
    private func setupUI() {
        // MARK: // init TableView
        tbvChat.register(R.nib.chatTableViewCell)
        tbvChat.tableFooterView = UIView()
        tbvChat.separatorStyle = .none
        tbvChat.rowHeight = 70
        tbvChat.isScrollEnabled = false
        tbvChat.rx.setDelegate(self).disposed(by: disposeBag)
        scollView.addInfiniteScroll { [weak self] (tableView) in
            guard let wSelf = self else { return }
            wSelf.getGroupAPI()
            wSelf.scollView.endRefreshing(at: .top)
            wSelf.scollView.finishInfiniteScroll(completion: { (collection) in

            })
//            SendBirdManager.share.loadNextPage { (groupChannels) in
//                self.litsChanel.accept(self.litsChanel.value + groupChannels)
//                self.scollView.endRefreshing(at: .top)
//                self.scollView.finishInfiniteScroll(completion: { (collection) in
//
//                })
//                self.removeElementDulicate(arrayListChanelSuggest: self.litsChanelSuggest.value, arrayListChanel: self.litsChanel.value)
//            }
        }
        
        // MARK: // init CollectionView
        let flowLayoutAfterBefore = UICollectionViewFlowLayout()
        let width = (UIScreen.main.bounds.width / 4) + 10
        flowLayoutAfterBefore.itemSize = CGSize(width: width , height: width + 10)
        self.wAddView.constant = width - Constant.distanceBesides
        self.heightCell = width + 10
        flowLayoutAfterBefore.scrollDirection = .horizontal
        flowLayoutAfterBefore.minimumInteritemSpacing = 0
        flowLayoutAfterBefore.minimumLineSpacing = 0
        flowLayoutAfterBefore.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvGroupSuggest.setCollectionViewLayout(flowLayoutAfterBefore, animated: true)
        clvGroupSuggest.showsHorizontalScrollIndicator = false
        clvGroupSuggest.register(R.nib.groupSuggestCollectionViewCell)
        clvGroupSuggest.register(R.nib.testCollectionViewCell)
        clvGroupSuggest.register(R.nib.addViewCollectionViewCell)
        clvGroupSuggest.dataSource = self
        clvGroupSuggest.delegate = self
    }
    
    func showPopupMiniGmae(){
        
        if UserInfo.shareUserInfo.new_device == 1 {
            if let id = UserInfo.shareUserInfo.game.id, id != 0 {
                let view = PopupGif.instanceFromNib()
                let _ = self.showPopup(view: view, height: 230)
            } else {
                let vc = ReferralVC(nibName: "ReferralVC", bundle: nil)
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }

        }
    }
    
    func showImageGame(){
//        let idGame = UserInfo.shareUserInfo.game.id ?? 0
//        if idGame > 0 {
//            UIView.animate(withDuration: 0.3) {
//                self.heightImg.constant = 352
//                self.view.layoutIfNeeded()
//            }
//
//            imgGame.setImage(url: URL.init(string: UserInfo.shareUserInfo.game.image ?? "")!)
//
//            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//            imgGame.isUserInteractionEnabled = true
//            imgGame.addGestureRecognizer(tapGestureRecognizer)
//        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
//        let gameVC = GameViewController.init(nib: R.nib.gameViewController)
//        gameVC.modalPresentationStyle = .overFullScreen
//        self.present(gameVC, animated: true, completion: nil)
        if UserInfo.shareUserInfo.new_device == 1 {
            let view = PopupGif.instanceFromNib()
            let _ = self.showPopup(view: view, height: 230)
        } else {
            let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
            gameVC.modalPresentationStyle = .overFullScreen
            self.present(gameVC, animated: true, completion: nil)
        }
        
    }
    
    @objc func handleEvent() {
        //   Utils.getVersionUpdateApp(fromToView: self)
       }

    func handelClickNotification(){
        // notification Server
//        if Utils.getStringFromUserDefaul(name: UserDefault.KeyNotification) != UserDefault.KeyNotificationEnd && Utils.getStringFromUserDefaul(name: UserDefault.KeyNotification) != nil{
//            let noti = Noti.init(json: JSON.init(parseJSON: Utils.getStringFromUserDefaul(name: UserDefault.KeyNotification)!))
//            var fetchedListNoti = [Noti]()
//            fetchedListNoti = [noti]
//            let listNoti = NotificationList(rawValue: fetchedListNoti[0].type ?? 0)
//            self.handelClickNotification(noti: listNoti, notification: noti, viewVC: self)
//            Utils.saveStringToUserDefault(name: UserDefault.KeyNotification, value: UserDefault.KeyNotificationEnd)
//        }
//        
//        // notification SendBird
//        if Utils.getStringFromUserDefaul(name: UserDefault.KeyNotificationSendBird) != UserDefault.KeyNotificationEndSendBird && Utils.getStringFromUserDefaul(name: UserDefault.KeyNotificationSendBird) != nil{
//
//            if let channelURL = Utils.getStringFromUserDefaul(name: UserDefault.KeyNotificationSendBird) {
//                SendBirdManager.share.joinChannelbyChanelUrl(listChanels: [channelURL], completion: { string in
//                    print(string)
//                })
//                SendBirdManager.share.joinChanelSuccess  =  { (groupChannel) in
//                 let chatVC = ListMessageViewController.init(nib: R.nib.listMessageViewController)
//                    chatVC.groupChannel = groupChannel
//                    chatVC.hidesBottomBarWhenPushed = true
//                    Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
//                }
//            }
//            Utils.saveStringToUserDefault(name: UserDefault.KeyNotificationSendBird, value: UserDefault.KeyNotificationEndSendBird)
//        }
        
                
    }

     private final func initRx(){
//        self.litsChanelSuggest.asObservable().bind(to: self.clvGroupSuggest.rx.items(cellIdentifier: R.reuseIdentifier.groupSuggestCollectionViewCell.identifier, cellType: GroupSuggestCollectionViewCell.self)) { [weak self]  row, data, cell in
//            guard let _ = self else { return }
//
//            cell.nameGroupLable.text = data.name
//            cell.imgCoverGroup.kf.setImage(with: URL.init(string: data.coverUrl ?? ""), placeholder: R.image.placeholder()!)
////            if row
//        }.disposed(by: disposeBag)
         
         SocketIOChatManager.shared.chatSocketEvent.asObservable().bind { [weak self] eventSocketChat in
             guard let wSelf = self else { return }
             switch eventSocketChat {
             case .onGroupCreate(let newGroup):
                 var list = wSelf.litsChanel.value
                 if (list.firstIndex(where: { $0.groupID == newGroup.groupID }) == nil) {
                     list.append(newGroup)
                     wSelf.litsChanel.accept(list)
                 }
             case .leaveGroup(let groupId):
                 var list = wSelf.litsChanel.value
                 if let index = list.firstIndex(where: { $0.groupID == groupId }) {
                     list.remove(at: index)
                 }
                 wSelf.litsChanel.accept(list)
             case .onMessageDelete(let deleteMessage):
                 guard let group = deleteMessage.group else {
                     return
                 }
                 var list = wSelf.litsChanel.value
                 if let index = list.firstIndex(where: { $0.groupID == group.groupID }) {
                     list[index] = group
                     wSelf.litsChanel.accept(list)
                 }
             case .onMessageReceived(let newmessage):
                 guard let group = newmessage.group, let _ = group.lastMessage else {
                     return
                 }
                 var list = wSelf.litsChanel.value
                 if let index = list.firstIndex(where: { $0.groupID == group.groupID }) {
                     list.remove(at: index)
                     list.insert(group, at: index)
                     wSelf.litsChanel.accept(list)
                 }
                 
             case .onMessageUpdate, .onChannelChanged: break
             }
         }.disposed(by: self.bag)

        // MARK: // set contentSize tableview
        tbvChat.rx.observe(CGSize.self, "contentSize").asObservable().observeOn(MainScheduler.asyncInstance).subscribe { [weak self] (size) in
            guard let self = self else { return }
            self.heightTbv.constant = size.element!!.height
            
            UIView.animate(withDuration: 0.5) {
                self.updateViewConstraints()
                self.view.layoutIfNeeded()
            }
        }.disposed(by: disposeBag)
        
        // MARK: // Bind Data to TableView
        self.litsChanel.asObservable().observeOn(MainScheduler.asyncInstance).bind(to: tbvChat.rx.items){(tv, row, item) -> UITableViewCell in
            let cell = tv.dequeueReusableCell(withIdentifier: R.nib.chatTableViewCell.identifier) as! ChatTableViewCell
            cell.selectionStyle = .none
            cell.nameChatGroupLable.text = item.groupName
            cell.imgChatGroup.kf.setImage(with: URL.init(string: item.coverURL ?? ""), placeholder: R.image.placeholder())
            cell.lastMessageLable.text = ""
            cell.timeLastMessageLable.text = ""
            cell.numberMemberLable.text = "\(item.numberMember ?? 0)"
            
           // print(item.memberCount)
            
            // MARK: // check exits last message
            if let lastMessage = item.lastMessage {
                if let messageType = lastMessage.messageType, let typeCell = CellTypeChatNew(rawValue: messageType) {
                    switch typeCell {
                    case .normal: cell.lastMessageLable.text = lastMessage.message ?? ""
                    case .file: cell.lastMessageLable.text = "[Hình ảnh]"
                    case .admin: break
                    }
                }
                cell.timeLastMessageLable.text = lastMessage.created?.toDate().timeAgoDisplayHomeFeed()
            }
            
            // MARK: // show / hide unread Message
            cell.unreadMessageLable.isHidden = true
            cell.widthUnreadLable.constant = 0
            if (item.numberUnread ?? 0) > 0 {
                cell.unreadMessageLable.isHidden = false
                cell.widthUnreadLable.constant = 20
                cell.unreadMessageLable.text = "\(item.numberUnread ?? 0)"

                if (item.numberUnread ?? 0) >= 100 {
                    cell.unreadMessageLable.text = "99+"
                }
            }
            
            // MARK: // get on/off notication
//            item.getMyPushTriggerOption(completionHandler: { (option, error) in
//                guard error == nil else {   // Error.
//                    print(error.debugDescription)
//                    return
//                }
//
//                cell.imageNoti.image = UIImage()
//                cell.imageNoti.isHidden = true
//                cell.widthImgNoti.constant = 0
//                if  option.rawValue == 2 {
//                    cell.imageNoti.isHidden = false
//                    cell.imageNoti.image = R.image.bell()
//                    cell.widthImgNoti.constant = 12
//                }
//            })
            
            return cell
        }.disposed(by: bag)
        
//      // MARK:  // push to view create new group
        let btAddGroup = self.btnAddGroupChat.rx.tap.mapToVoid()
        let btAddGroupAtSuggest = self.btAdd.rx.tap.mapToVoid()
        Observable.merge(btAddGroup, btAddGroupAtSuggest)
            .subscribe { (_) in
             
           AppEvents.logEvent(AppEvents.Name.init(rawValue: "Create group chat"))
            
            
            let groupChatNameVC = SearchNameGroupViewController.init(nib: R.nib.searchNameGroupViewController)
            groupChatNameVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(groupChatNameVC, animated: true)
        }.disposed(by: disposeBag)
        
        // MARK: // push to view list message
         self.tbvChat.rx.itemSelected.bind { [weak self] idx in
             guard let wSelf = self else { return }
             let item = wSelf.litsChanel.value[idx.row]
             let chatVC = ListMessageViewController.init(nib: R.nib.listMessageViewController)
             chatVC.groupdChat = item
             chatVC.delegate = self
             chatVC.hidesBottomBarWhenPushed = true
             Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
         }.disposed(by: disposeBag)
//        tbvChat.rx.modelSelected(SBDGroupChannel.self).observeOn(MainScheduler.asyncInstance).subscribe { (item) in
//            let chatVC = ListMessageViewController.init(nib: R.nib.listMessageViewController)
//            chatVC.groupID = item.group
//            chatVC.hidesBottomBarWhenPushed = true
//            chatVC.updateDeleteChanel = { groupChannel in
//                var list = self.litsChanel.value
//                let index = list.firstIndex { (gr) -> Bool in
//                    return gr.channelUrl == groupChannel.channelUrl
//                }
//                if index != nil {
//                    list.remove(at: index!)
//                }
//                self.litsChanel.accept(list)
//            }
//            Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
//        }.disposed(by: disposeBag)
        
        
        // MARK: // joni channel from channel suggest
//        clvGroupSuggest.rx.modelSelected(ChannelSBModel.self).observeOn(MainScheduler.asyncInstance).subscribe { (item) in
//
//            Utils.showAlertOKWithAction(controller: self, title: R.string.localizable.commonNoti(), message: "Bạn có chắc chắc muốn tham gia nhóm ?", cancel: true) {
//
//                SendBirdManager.share.joinChannelbyChanelUrl(listChanels: [item.element!.channelUrl ?? ""], completion: { message in
//                    print(message)
//                    Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: message)
//                })
//
//                SendBirdManager.share.joinChanelSuccess = { groupChannel in
//
//                    // MARK: // remove channel ở channel suggest
//                    var channelTemp = self.litsChanelSuggest.value
//                    let index  = channelTemp.firstIndex(where: { $0.channelUrl == item.element!.channelUrl })
//                    if index != nil {
//                        channelTemp.remove(at: index!)
//                        self.litsChanelSuggest.accept(channelTemp)
//                        self.showSuggestGroupView(isShow: self.litsChanelSuggest.value.count != 0)
//                    }
//
//                    // MARK: // push to view message view controller
//                    let chatVC = ListMessageViewController.init(nib: R.nib.listMessageViewController)
//                    chatVC.groupChannel = groupChannel
//                    chatVC.listMessage.accept([])
//                    chatVC.hidesBottomBarWhenPushed = true
//                    chatVC.updateDeleteChanel = { groupChannel in
//                        var list = self.litsChanel.value
//                        let index = list.firstIndex { (gr) -> Bool in
//                            return gr.channelUrl == groupChannel.channelUrl
//                        }
//                        if index != nil {
//                            list.remove(at: index!)
//                        }
//                        self.litsChanel.accept(list)
//                    }
//                    Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
//                }
//            }
//
//        }.disposed(by: disposeBag)
        
        
//        let isCheck = Observable.combineLatest(self.litsChanelSuggest, self.litsChanel){
//            $0.count > 0 && $1.count > 0
//        }
//
//        isCheck.observeOn(MainScheduler.asyncInstance).subscribe { (value) in
//            if value.element ?? false {
//                self.removeElementDulicate(arrayListChanelSuggest: self.litsChanelSuggest.value, arrayListChanel: self.litsChanel.value)
//            }
//        }.disposed(by: disposeBag)
        
    }
    
    
    
//    func removeElementDulicate(arrayListChanelSuggest: [ChannelSBModel], arrayListChanel: [SBDGroupChannel]){
//        if arrayListChanelSuggest.count > 0 && arrayListChanel.count > 0{
//            var channelTemp  = arrayListChanelSuggest
//            for (_,element) in arrayListChanel.enumerated() {
//                let index = channelTemp.firstIndex(where: { $0.channelUrl == element.channelUrl })
//                if index != nil{
//                    channelTemp.remove(at: index!)
//                }
//            }
//            
////            self.showSuggestGroupView(isShow: channelTemp.count != 0)
//            
//            channelTemp.sort {
//                $0.memberCount ?? 0 < $1.memberCount ?? 0
//            }
//        
//            self.litsChanelSuggest.accept(channelTemp)
////            self.listChanelSgest = channelTemp
//            self.clvGroupSuggest.reloadData()
//        }
//        
//
//    }
    
    private final func getListChannel(){

        let customType = "beberia_channel_marketing"
        APISendBirdManager.getListChannelCustomType(customType: customType) { [weak self] (channels) in
            guard let wSelf = self else { return }
            wSelf.litsChanelSuggest.accept(channels)
//            wSelf.listChanelSgest = channels
            wSelf.clvGroupSuggest.reloadData()
//            wSelf.removeElementDulicate(arrayListChanelSuggest: channels, arrayListChanel: wSelf.litsChanel.value)
        }
    }
    
    private func getGroupAPI() {
        let param: Parameters = [
            "page": self.nextPage
        ]
        APIManager.listGroup(param: param) { [weak self] item in
            guard let wSelf = self, let list = item.list, let nextPage = item.nextPage else { return }
            let l = wSelf.litsChanel.value + list
            wSelf.litsChanel.accept(l)
            wSelf.nextPage = nextPage
        } failed: { error in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }

    }
    
    private func getSearchGroup(searchText: String) {
        self.nextPage = 1
        APIManager.searchGroup(page: self.nextPage, searchText: searchText) { [weak self] searchGroup in
            guard let wSelf = self, let list = searchGroup.list, let nextPage = searchGroup.nextPage, let total = searchGroup.total else { return }
            let l = list.sorted(by: { ($0.numberMember ?? 0) < ($1.numberMember ?? 0) })
            wSelf.litsChanel.accept(l)
            wSelf.nextPage = nextPage
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }
    }
    
    // get List Chanel Messgae
//    private final func getListChanelMessage(){
//     //   LoaddingManager.startLoadding()
//        SendBirdManager.share.getListChannels { value in
////            self.litsChanel.accept(value)
//            self.getListChannel()
//        //    LoaddingManager.stopLoadding()
//        }
//    }
//    
//    private func connectSendBird() {
//        SendBirdManager.share.connectUserToSendBird {
//
//        }
//    }
    
    
    // Custom Navication
    private final func customNavication() {

        let viewTitle = ViewSreachGroupChat.instanceFromNib()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "f1f1f1")
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: viewTitle)]

        // MARK: Search channel
       // viewTitle.tfSreach.placeholder = "Tìm kiếm nhóm đã tham gia..."
        viewTitle.tfSreach.rx.text.orEmpty.asObservable().skip(1).distinctUntilChanged().debounce(.milliseconds(300), scheduler: MainScheduler.asyncInstance).subscribe { [weak self] (text) in
//            SendBirdManager.share.searchChannel(textSearch: text.element ?? "") { (channels) in
//                self.litsChanel.accept(channels)
//            }
            guard let wSelf = self else { return }
            if text.element == "" || text.element == nil {
                wSelf.nextPage = 1
                wSelf.litsChanel.accept([])
                wSelf.getGroupAPI()
            } else {
                wSelf.getSearchGroup(searchText: text.element ?? "")
            }
        }.disposed(by: bag)
        
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = R.color.f1F1F1()
            
            if let navBar = self.navigationController {
                let bar = navBar.navigationBar
                bar.standardAppearance = appearance
                bar.scrollEdgeAppearance = appearance
            }
        }
    }
    
    
  // Sync Chanel Manager
//    private final func channelSyncManager(){
//        let options = SBSMSyncManagerOptions()
//        options.messageResendPolicy = .manual
//        SBSMSyncManager.setup(withUserId: "\(UserInfo.shareUserInfo.id ?? 0)", options: options)
//
//        guard let query = SBDGroupChannel.createMyGroupChannelListQuery() else { return }
//        query.order = .latestLastMessage
//        let collection = SBSMChannelCollection(query: query)
////        collection?.delegate = self
//        collection?.fetch(completionHandler: { (error) in
//        })
//    }
    
    func getBannerEven(){

        APIManager.getBanner(type: 6, callbackSuccess: { (banners) in
            if banners.count > 0{
                Utils.checkShowViewEven(baner: banners[0])
                }
            }, failed: { (error) in
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        })
    }

    
    private func getBannerUpdateApp(){
        
        RemoteConfigFB.share.fetchValueUpdateApp()
        
        RemoteConfigFB.share.getValueUpdateApp = { valueUpdateApp in
            if valueUpdateApp {
                Utils.appStoreVersion { (isVersion, version) in
                    print(version)
                    if !isVersion {
                        
                        print("version \(version)")
                        
                        let popupUpdateApp = PopupUpdateApp.instanceFromNib()
                        let popup = self.showPopup(view: popupUpdateApp, height: 360) as! FFPopup
                        popup.shouldDismissOnBackgroundTouch = false
                        popupUpdateApp.tapUpdate = {
                            if let url = URL(string: LibKey.linkURLAppStore) {
                                DispatchQueue.main.async {
                                    UIApplication.shared.open(url)
                                }
                            }
                        }
                        
    
                        
          //              APIManager.getBanner(type: 8, callbackSuccess: { (banners) in
          //                    if banners.count > 0{
//                            var banener = [Baner.init(json: "")]
//                            banener[0].link = "https://i.picsum.photos/id/433/200/300.jpg?hmac=Y75_deyseM49Q8smDAbeRflgTmOchUngpd-QeDllW0g"
//                            banener[0].linkAds = "https://beberiavn.page.link/ezHe"
//
          //                      Utils.checkShowViewUpdate(baner: banners[0])
          
            //        }
                    
           //     }
//                        }, failed: { (error) in
//                            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
//                        })
                    } else {
                        print("chua co version moi")
                    }
                }
            }
        }
         
    }
    
    private func getList() {
        APIManager.getListSuggest( callbackSuccess: { [weak self] suggests in
            guard let wSelf = self, let list = suggests.list else { return }
            wSelf.listChanelSgest = list
            wSelf.showSuggestGroupView(isShow: list.count > 0)
            wSelf.clvGroupSuggest.reloadData()
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        })
    }
    
    private func getBanner(){
        var arrayImage = [String]()
        APIManager.getBanner(type: 7, callbackSuccess: { [weak self]  (banners) in
            guard let self = self else { return }
            for banner in banners {
                arrayImage.append(banner.link ?? "")
            }
            
            self.showBannerView(isShow: arrayImage.count > 0)
            self.bannerView.arrayImage = arrayImage
            self.bannerView.tapBanner = { index in
                guard let bannerType = BannerView.BannerTypeRedirect(rawValue: banners[index].typeRedirect ?? 0 ) else {
                    return
                }
                switch bannerType {
                case .none:
                    if let url = URL.init(string: banners[index].linkAds ?? "")  {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url , options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                case .rankPoint:
                    let rankPointVC = RankPointVC.init(nib: R.nib.rankPointVC)
                    rankPointVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(rankPointVC, animated: true)
                case .giftGame:
                    let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
                    gameVC.modalPresentationStyle = .overFullScreen
                    self.present(gameVC, animated: true, completion: nil)
                case .info:
                    guard let lichKTVC = R.storyboard.main.infoViewController() else{ return }
                    lichKTVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(lichKTVC, animated: true)
                case .momTok:
                    guard let momtokVC = R.storyboard.main.secretViewController() else {
                        return
                    }
                    momtokVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(momtokVC, animated: true)
                case .news:
                    let listNewHotVC = ListNewHotViewController.init(nib: R.nib.listNewHotViewController)
                    listNewHotVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(listNewHotVC, animated: true)
                case .listDiary:
                    let postVC = ListDiaryViewController.init(nib: R.nib.listDiaryViewController)
                    postVC.hidesBottomBarWhenPushed = true
                    postVC.gettype = "3"
                    postVC.title = "Mẹo cho bạn"
                    self.navigationController?.pushViewController(postVC, animated: true)
                case .home:
                    guard let tabbar  = self.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 1
                case .diaary:
                    guard let tabbar  = self.navigationController?.tabBarController else { return }
                    tabbar.selectedIndex = 2
                }
            }
            self.bannerView.backgroundColor = .clear
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        })
    }
    
    private func showBannerView(isShow: Bool){
//        let height = (Screen.width * 780 ) / 2050
        heightBannerView.constant = isShow ? 120 : 0
        bannerView.isHidden = isShow ? false : true

        UIView.animate(withDuration: 0.3) {
            self.updateViewConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    private func showSuggestGroupView(isShow: Bool){
        self.clvGroupSuggest.isHidden = (isShow) ? false : true
        self.addView.isHidden = (isShow) ? false : true
        self.hAddView.constant = isShow ? self.heightCell : 0
        heightViewCollectionView.constant = isShow ? self.heightCell : 0
           
           UIView.animate(withDuration: 0.3) {
               self.updateViewConstraints()
               self.view.layoutIfNeeded()
           }
       }
    
    private func addMembers(groupId: Int, userId: String) {
        APIManager.addMembers(groupId: groupId, userId: userId) { [weak self] in
            guard let wSelf = self else { return }
            let chatVC = ListMessageViewController.init(nib: R.nib.listMessageViewController)
            if let index = wSelf.listChanelSgest.firstIndex(where: { $0.groupID == groupId }) {
                chatVC.groupdChat = wSelf.listChanelSgest[index]
                SocketIOChatManager.shared.chatSocketEvent.onNext(.onGroupCreate(wSelf.listChanelSgest[index]))
            }
            chatVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }
    }
}


extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}

//extension ChatViewController: SBSMChannelCollectionDelegate ,SBDConnectionDelegate {
//
//    func collection(_ collection: SBSMChannelCollection, didReceiveEvent action: SBSMChannelEventAction, channels: [SBDGroupChannel]) {
//        switch (action) {
//        case .insert:
//            // TODO: Add channels to the view.
//
////            if !isFristLoadView {
////
////                var list = self.litsChanel.value
////                let index = list.firstIndex { (gr) -> Bool in
////                    return gr.channelUrl == channels[0].channelUrl
////                }
////                if index != nil {
////                    list.remove(at: index!)
////                    list.insert(channels[0], at: 0)
////                }else{
////                    if channels.count > 0{
////                        list.insert(channels[0], at: 0)
////                        //  self.litsChanel.accept(list)
////                    }
////                }
////
////                self.litsChanel.accept(list)
////            }else{
////                isFristLoadView = false
////                self.litsChanel.accept(channels)
////            }
//
//
//
//            break
//        case .update:
//            // TODO: Update channels to the view.
////            var list = self.litsChanel.value
////            let index = list.firstIndex { (gr) -> Bool in
////                return gr.channelUrl == channels[0].channelUrl
////            }
////            if index != nil {
////                list[index!] = channels[0]
////            }
////            print(channels)
////            self.litsChanel.accept(list)
//
//            break
//        case .remove:
//            // TODO: Remove channels from the view.
////            print(channels)
////
////            var list = self.litsChanel.value
////            let index = list.firstIndex { (gr) -> Bool in
////                return gr.channelUrl == channels[0].channelUrl
////            }
////            if index != nil {
////                list.remove(at: index!)
////            }
////            print(channels)
////            self.litsChanel.accept(list)
//
//            break
//        case .move:
//            // TODO: Change the position of channels in the view.
//
////            var list = self.litsChanel.value
////            let index = list.firstIndex { (gr) -> Bool in
////                return gr.channelUrl == channels[0].channelUrl
////            }
////            if index != nil {
////                list.remove(at: index!)
////                list.insert(channels[0], at: 0)
////            }
////            print(channels)
////            self.litsChanel.accept(list)
//
//            break
//        case .clear:
//            // TODO: Clear the view.
//            print(channels)
//            break
//        case .none:
//            break;
//        @unknown default:
//            fatalError("")
//        }
//    }
//
//    // SBDConnectionDelegate
//    func didStartReconnection() {
//        SBSMSyncManager.pauseSynchronize()
//        print("SBSMSyncManager.pauseSynchronize()")
//    }
//
//    func didSucceedReconnection() {
//        SBSMSyncManager.resumeSynchronize()
//        print("SBSMSyncManager.resumeSynchronize()")
//    }
//
//    func didFailReconnection() {
//        print("didFailReconnection")
//    }
//
//    func didCancelReconnection() {
//        print("didCancelReconnection")
//    }
//}

extension ChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return height ?? UITableView.automaticDimension
    }
}

extension UICollectionViewCell {
    func shadowDecorate() {
        let radius: CGFloat = 10
        contentView.layer.cornerRadius = radius
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.4
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius).cgPath
        layer.cornerRadius = radius
    }
}
extension ChatViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listChanelSgest.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.groupSuggestCollectionViewCell.identifier,
                                                            for: indexPath) as? GroupSuggestCollectionViewCell else {
            fatalError()
        }
        let data = self.listChanelSgest[indexPath.row]
        cell.nameGroupLable.text = data.groupName
        cell.imgCoverGroup.kf.setImage(with: URL.init(string: data.coverURL ?? ""), placeholder: R.image.placeholder()!)
        
        return cell

    }
    
    
}
extension ChatViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let group = self.listChanelSgest[indexPath.row]
        Utils.showAlertOKWithAction(controller: self, title: R.string.localizable.commonNoti(), message: "Bạn có chắc chắn muốn tham gia nhóm ?", cancel: true) { [weak self] in
            guard let wSelf = self, let id = UserInfo.shareUserInfo.id, let groupId = group.groupID else {
                return
            }
            let encodeString = ManageApp.shared.encodeString(usersIds: ["\(id)"])
            wSelf.addMembers(groupId: groupId, userId: encodeString)
            
//            SendBirdManager.share.joinChannelbyChanelUrl(listChanels: [item.coverURL ?? ""], completion: { message in
//                print(message)
//                Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: message)
//            })
//
//            SendBirdManager.share.joinChanelSuccess = { [weak self] groupChannel in
//                guard let wSelf = self else { return }
//                // MARK: // remove channel ở channel suggest
//                wSelf.getListChanelMessage()
//                var channelTemp = wSelf.litsChanelSuggest.value
//                let index  = channelTemp.firstIndex(where: { $0.channelUrl == item.coverURL })
//                if index != nil {
//                    channelTemp.remove(at: index!)
//                    wSelf.litsChanelSuggest.accept(channelTemp)
//                    wSelf.showSuggestGroupView(isShow: wSelf.litsChanelSuggest.value.count != 0)
//                }
//
//                // MARK: // push to view message view controller
//                let chatVC = ListMessageViewController.init(nib: R.nib.listMessageViewController)
//                chatVC.groupChannel = groupChannel
//                chatVC.listMessage.accept([])
//                chatVC.hidesBottomBarWhenPushed = true
//                chatVC.updateDeleteChanel = { groupChannel in
//                    var list = wSelf.litsChanel.value
//                    let index = list.firstIndex { (gr) -> Bool in
//                        return gr.channelUrl == groupChannel.channelUrl
//                    }
//                    if index != nil {
//                        list.remove(at: index!)
//                    }
//                    wSelf.litsChanel.accept(list)
//                }
//                Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
//            }
        }
    }
}
extension ChatViewController: ListMessageViewControllerDelegate {
    func updateGroup(group: NewGroupModel) {
        var list = self.litsChanel.value
        if let index = list.firstIndex(where: { $0.groupID == group.groupID }) {
            list[index].numberUnread = 0
            self.litsChanel.accept(list)
        }
    }
}
