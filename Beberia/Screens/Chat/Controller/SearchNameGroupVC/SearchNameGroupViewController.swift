//
//  SearchNameGroupViewController.swift
//  Beberia
//
//  Created by IMAC on 7/22/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Differentiator

class SearchNameGroupViewController: BaseViewController {

    @IBOutlet weak var btnCreateGroup: UIButton!
    @IBOutlet weak var tbvNameGroup: UITableView!
    
    private var channels: BehaviorRelay<[NewGroupModel]> = BehaviorRelay.init(value: [])
    private var nextPage: Int = 0
    private var searchText: String = ""
    private var total: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customLeftBarButton()
        customRightBarButton(image: UIImage.init())
        initRx()
        btnCreateGroup.isHidden = true
        self.getSearchGroup(reset: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.f1F1F1() ?? .white,
                                 textColor: UIColor.black,
                                 isTranslucent: true)
        self.customNavication()
        self.navigationController?.isNavigationBarHidden = false
    }

}
extension SearchNameGroupViewController {
    
    func initRx(){
        
        
        // init TableView
        tbvNameGroup.register(R.nib.nameGroupTableViewCell)
        tbvNameGroup.tableFooterView = UIView()
        tbvNameGroup.separatorStyle = .none
        tbvNameGroup.rowHeight = 50
        tbvNameGroup.addInfiniteScroll { [weak self] (tableView) in
            guard let wSelf = self, wSelf.nextPage < wSelf.total else {
                self?.tbvNameGroup.endAllRefreshing()
                return
            }
            wSelf.getSearchGroup(reset: false)
        }
        
        self.channels.asObservable().observeOn(MainScheduler.asyncInstance).bind(to: tbvNameGroup.rx.items){(tv, row, item) -> UITableViewCell in
            let cell = tv.dequeueReusableCell(withIdentifier: R.nib.nameGroupTableViewCell.identifier) as! NameGroupTableViewCell
            cell.nameGroupLable.text = item.groupName ?? ""
            cell.imgCoverGroup.kf.setImage(with: URL.init(string: item.coverURL ?? ""), placeholder: R.image.placeholder()!)
            return cell
        }.disposed(by: disposeBag)
        
        // Push to view Create Group
        btnCreateGroup.rx.tap.subscribe { (_) in
            let chatVC = NewGroupChatViewController.init(nib: R.nib.newGroupChatViewController)
            chatVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
        }.disposed(by: disposeBag)
        
        // seledted Item Tableview
        Observable
        .zip(tbvNameGroup.rx.itemSelected, tbvNameGroup.rx.modelSelected(NewGroupModel.self))
        .bind { [weak self] indexPath, channel in
            guard let self = self else { return }
            
            self.tbvNameGroup.deselectRow(at: indexPath, animated: true)
            let item = self.channels.value[indexPath.row]
            if let groupId = item.groupID {
                let encodeString = ManageApp.shared.encodeString(usersIds: ["\(UserInfo.shareUserInfo.id ?? 0)"])
                self.addMembers(groupId: groupId, userId: encodeString)
            }
        }.disposed(by: disposeBag)
        
    }
    
    // Custom Navication
       func customNavication(){
           
           let titleView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
           let viewTitle = ViewSreachGroupChat.instanceFromNib()
           viewTitle.frame = titleView.bounds
           titleView.addSubview(viewTitle)
           viewTitle.customViewForChatGroup()
           viewTitle.tfSreach.placeholder = "Nhập tên group cần tìm..."
        
           viewTitle.tfSreach.rx.text.orEmpty.asObservable().skip(2).distinctUntilChanged().debounce(.milliseconds(500), scheduler: MainScheduler.asyncInstance).subscribe { [weak self] (text) in
               guard let wSelf = self else { return }
               wSelf.nextPage = 0
               wSelf.total = 0
               wSelf.searchText = text.element ?? ""
               wSelf.getSearchGroup(reset: true)
           }.disposed(by: disposeBag)
           
           viewTitle.autoresizingMask = [.flexibleWidth, .flexibleHeight]
           
           navigationItem.titleView = viewTitle
           
           if #available(iOS 15, *) {
               let appearance = UINavigationBarAppearance()
               appearance.configureWithOpaqueBackground()
               appearance.backgroundColor = R.color.f1F1F1()
               
               if let navBar = self.navigationController {
                   let bar = navBar.navigationBar
                   bar.standardAppearance = appearance
                   bar.scrollEdgeAppearance = appearance
               }
           }
       }
    
    private func addMembers(groupId: Int, userId: String) {
        Utils.showAlertOKWithAction(controller: self, title: R.string.localizable.commonNoti(), message: "Bạn có chắc chắn muốn tham gia nhóm ?", cancel: true) {
            APIManager.addMembers(groupId: groupId, userId: userId) { [weak self] in
                guard let wSelf = self else { return }
                let chatVC = ListMessageViewController.init(nib: R.nib.listMessageViewController)
                if let index = wSelf.channels.value.firstIndex(where: { $0.groupID == groupId }) {
                    chatVC.groupdChat = wSelf.channels.value[index]
                    SocketIOChatManager.shared.chatSocketEvent.onNext(.onGroupCreate(wSelf.channels.value[index]))
                }
                chatVC.hidesBottomBarWhenPushed = true
                Utils.getTopMostViewController()?.navigationController?.pushViewController(chatVC, animated: true)
            } failed: { message in
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
            }
            
        }
    }
    
    private func getSearchGroup(reset: Bool) {
        APIManager.searchGroup(page: self.nextPage, searchText: self.searchText) { [weak self] searchGroup in
            guard let wSelf = self, let list = searchGroup.list, let nextPage = searchGroup.nextPage, let total = searchGroup.total else { return }
            if reset {
                let l = list.sorted(by: { ($0.numberMember ?? 0) < ($1.numberMember ?? 0) })
                wSelf.channels.accept(l)
                wSelf.nextPage = nextPage
                wSelf.total = total
            } else {
                let l = wSelf.channels.value + list
                let l1 = l.sorted(by: { ($0.numberMember ?? 0) < ($1.numberMember ?? 0) })
                wSelf.channels.accept(l1)
                wSelf.nextPage = nextPage
                wSelf.total = total
            }
            wSelf.btnCreateGroup.isHidden = false
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }


    }
    
//    func getListChannel(nameSearch: String){
//        APISendBirdManager.getListChannel(nameSearch: nameSearch,  completion: { (channels) in
//            self.channels.accept(channels.sorted(by: { $0.memberCount ?? 0 < $1.memberCount ?? 0}))
//            //.shuffled())
//            self.btnCreateGroup.isHidden = false
//        })
//    }
}
