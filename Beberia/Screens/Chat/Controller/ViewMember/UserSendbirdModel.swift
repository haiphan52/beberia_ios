//
//  UserSendbirdModel.swift
//
//  Created by IMAC on 9/1/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class UserSendbirdModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let profileUrl = "profile_url"
    static let metadata = "metadata"
    static let state = "state"
    static let lastSeenAt = "last_seen_at"
    static let isActive = "is_active"
    static let nickname = "nickname"
    static let isOnline = "is_online"
    static let deliveredTs = "delivered_ts"
    static let role = "role"
    static let userId = "user_id"
    static let readTs = "read_ts"
  }

  // MARK: Properties
  public var profileUrl: String?
  public var metadata: Metadata?
  public var state: String?
  public var lastSeenAt: Int?
  public var isActive: Bool? = false
  public var nickname: String?
  public var isOnline: Bool? = false
  public var deliveredTs: Int?
  public var role: String?
  public var userId: String?
  public var readTs: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    profileUrl = json[SerializationKeys.profileUrl].string
    metadata = Metadata(json: json[SerializationKeys.metadata])
    state = json[SerializationKeys.state].string
    lastSeenAt = json[SerializationKeys.lastSeenAt].int
    isActive = json[SerializationKeys.isActive].boolValue
    nickname = json[SerializationKeys.nickname].string
    isOnline = json[SerializationKeys.isOnline].boolValue
    deliveredTs = json[SerializationKeys.deliveredTs].int
    role = json[SerializationKeys.role].string
    userId = json[SerializationKeys.userId].string
    readTs = json[SerializationKeys.readTs].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = profileUrl { dictionary[SerializationKeys.profileUrl] = value }
    if let value = metadata { dictionary[SerializationKeys.metadata] = value.dictionaryRepresentation() }
    if let value = state { dictionary[SerializationKeys.state] = value }
    if let value = lastSeenAt { dictionary[SerializationKeys.lastSeenAt] = value }
    dictionary[SerializationKeys.isActive] = isActive
    if let value = nickname { dictionary[SerializationKeys.nickname] = value }
    dictionary[SerializationKeys.isOnline] = isOnline
    if let value = deliveredTs { dictionary[SerializationKeys.deliveredTs] = value }
    if let value = role { dictionary[SerializationKeys.role] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = readTs { dictionary[SerializationKeys.readTs] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.profileUrl = aDecoder.decodeObject(forKey: SerializationKeys.profileUrl) as? String
    self.metadata = aDecoder.decodeObject(forKey: SerializationKeys.metadata) as? Metadata
    self.state = aDecoder.decodeObject(forKey: SerializationKeys.state) as? String
    self.lastSeenAt = aDecoder.decodeObject(forKey: SerializationKeys.lastSeenAt) as? Int
    self.isActive = aDecoder.decodeBool(forKey: SerializationKeys.isActive)
    self.nickname = aDecoder.decodeObject(forKey: SerializationKeys.nickname) as? String
    self.isOnline = aDecoder.decodeBool(forKey: SerializationKeys.isOnline)
    self.deliveredTs = aDecoder.decodeObject(forKey: SerializationKeys.deliveredTs) as? Int
    self.role = aDecoder.decodeObject(forKey: SerializationKeys.role) as? String
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? String
    self.readTs = aDecoder.decodeObject(forKey: SerializationKeys.readTs) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(profileUrl, forKey: SerializationKeys.profileUrl)
    aCoder.encode(metadata, forKey: SerializationKeys.metadata)
    aCoder.encode(state, forKey: SerializationKeys.state)
    aCoder.encode(lastSeenAt, forKey: SerializationKeys.lastSeenAt)
    aCoder.encode(isActive, forKey: SerializationKeys.isActive)
    aCoder.encode(nickname, forKey: SerializationKeys.nickname)
    aCoder.encode(isOnline, forKey: SerializationKeys.isOnline)
    aCoder.encode(deliveredTs, forKey: SerializationKeys.deliveredTs)
    aCoder.encode(role, forKey: SerializationKeys.role)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
    aCoder.encode(readTs, forKey: SerializationKeys.readTs)
  }

}
