//
//  ViewMemberViewController.swift
//  Beberia
//
//  Created by IMAC on 9/1/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewMemberViewController: BaseViewController {
    
    @IBOutlet weak var tbvViewMember: UITableView!
    
    private final var bag = DisposeBag()
    private final var userSendbirds: BehaviorRelay<[ChatMemberModel]> = BehaviorRelay.init(value: [])
    private final var token = ""
    private var nextPage: Int = 1
    var groupdChat: NewGroupModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customLeftBarButton()
        title = "Thành viên"
        initRx()
        self.getChatMembers()
    }
    
    private func initRx(){
        
        tbvViewMember.register(R.nib.selectUserTableViewCell)
        tbvViewMember.tableFooterView = UIView()
        tbvViewMember.separatorStyle = .none
        
        tbvViewMember.addInfiniteScroll { [weak self] (tableView) in
            guard let wSelf = self else { return }
            if wSelf.token != ""{
                wSelf.getChatMembers()
            } else{
                wSelf.tbvViewMember.endRefreshing(at: .top)
                wSelf.tbvViewMember.finishInfiniteScroll(completion: { (collection) in
                })
            }
        }
        
        self.userSendbirds.asObservable().observeOn(MainScheduler.asyncInstance).bind(to: tbvViewMember.rx.items){(tv, row, item) -> UITableViewCell in
            let cell = tv.dequeueReusableCell(withIdentifier: R.nib.selectUserTableViewCell.identifier) as! SelectUserTableViewCell
            cell.imgAvatar.kf.setImage(with: URL.init(string:  item.avatar ?? ""))
            cell.imgSelected.isHidden = true
            cell.nameUserLable.text = item.displayName ?? ""
           // let time = Utils.getDateFromTimeStamp(timeStamp: Double(item.lastSeenAt ?? 0), dateFormat: "yyyy/MM/dd HH:MM")
            let time1 = item.timeOnline?.toDate().timeAgoDisplayHomeFeed()
            cell.timeOnlineLable.text = (item.isOnline == 1) ? "Online" : "Online \(time1 ?? "")"
            return cell
        }.disposed(by: bag)
    }
    
}

extension ViewMemberViewController {
    
    private func getChatMembers() {
        guard let group = self.groupdChat, let groupId = group.groupID else {
            return
        }
        APIManager.chatMembers(nextPage: self.nextPage, groupId: groupId) { [weak self] chatMembers in
            guard let wSelf = self, let list = chatMembers.list, let nextPage = chatMembers.nextPage else { return }
            wSelf.nextPage = nextPage
            let l = wSelf.userSendbirds.value + list
            wSelf.userSendbirds.accept(l)
            wSelf.tbvViewMember.finishInfiniteScroll(completion: { (collection) in
            })
        } failed: { message in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: message)
        }

    }
    
//    private func getListMember(token: String){
//        APISendBirdManager.getListUser(token: token, channel_url: groupChannel?.channelUrl ?? "") { (users, token) in
//            if self.token == ""{
//                self.userSendbirds.accept(users)
//            }else{
//                self.userSendbirds.accept(self.userSendbirds.value + users)
//            }
//
//            self.tbvViewMember.finishInfiniteScroll(completion: { (collection) in
//            })
//
//            self.token = token
//        }
//    }
}
