//
//  SuggestsModel.swift
//  Beberia
//
//  Created by haiphan on 18/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

// MARK: - DataClass
struct SuggestsModel: Codable {
    let list: [NewGroupModel]?
    
    enum CodingKeys: String, CodingKey {
        case list
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([NewGroupModel].self, forKey: .list)
    }
}

// MARK: - List
struct SuggestModel: Codable {
    let name, url: String?
    let cover: String?
    
    enum CodingKeys: String, CodingKey {
        case name, url, cover
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        cover = try values.decodeIfPresent(String.self, forKey: .cover)
    }
}
