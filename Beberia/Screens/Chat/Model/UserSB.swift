//
//  User.swift
//
//  Created by IMAC on 7/22/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class UserSB: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let profileUrl = "profile_url"
    static let metadata = "metadata"
    static let nickname = "nickname"
    static let userId = "user_id"
  }

  // MARK: Properties
  public var profileUrl: String?
  public var metadata: Metadata?
  public var nickname: String?
  public var userId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    profileUrl = json[SerializationKeys.profileUrl].string
    metadata = Metadata(json: json[SerializationKeys.metadata])
    nickname = json[SerializationKeys.nickname].string
    userId = json[SerializationKeys.userId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = profileUrl { dictionary[SerializationKeys.profileUrl] = value }
    if let value = metadata { dictionary[SerializationKeys.metadata] = value.dictionaryRepresentation() }
    if let value = nickname { dictionary[SerializationKeys.nickname] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.profileUrl = aDecoder.decodeObject(forKey: SerializationKeys.profileUrl) as? String
    self.metadata = aDecoder.decodeObject(forKey: SerializationKeys.metadata) as? Metadata
    self.nickname = aDecoder.decodeObject(forKey: SerializationKeys.nickname) as? String
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(profileUrl, forKey: SerializationKeys.profileUrl)
    aCoder.encode(metadata, forKey: SerializationKeys.metadata)
    aCoder.encode(nickname, forKey: SerializationKeys.nickname)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
  }

}
