//
//  ChannelSBModel.swift
//
//  Created by IMAC on 7/22/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ChannelSBModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let name = "name"
    static let inviter = "inviter"
    static let data = "data"
    static let coverUrl = "cover_url"
    static let unreadMentionCount = "unread_mention_count"
    static let readReceipt = "read_receipt"
    static let isEphemeral = "is_ephemeral"
    static let maxLengthMessage = "max_length_message"
    static let customType = "custom_type"
    static let lastMessage = "last_message"
    static let channelUrl = "channel_url"
    static let freeze = "freeze"
    static let unreadMessageCount = "unread_message_count"
    static let isSuper = "is_super"
    static let deliveryReceipt = "delivery_receipt"
    static let isPublic = "is_public"
    static let isAccessCodeRequired = "is_access_code_required"
    static let memberCount = "member_count"
    static let isDistinct = "is_distinct"
    static let createdAt = "created_at"
    static let joinedMemberCount = "joined_member_count"
    static let invitedAt = "invited_at"
    static let members = "members"
  }

  // MARK: Properties
  public var name: String?
  public var inviter: Inviter?
  public var data: String?
  public var coverUrl: String?
  public var unreadMentionCount: Int?
  public var readReceipt: ReadReceipt?
  public var isEphemeral: Bool? = false
  public var maxLengthMessage: Int?
  public var customType: String?
  public var lastMessage: LastMessage?
  public var channelUrl: String?
  public var freeze: Bool? = false
  public var unreadMessageCount: Int?
  public var isSuper: Bool? = false
  public var deliveryReceipt: DeliveryReceipt?
  public var isPublic: Bool? = false
  public var isAccessCodeRequired: Bool? = false
  public var memberCount: Int?
  public var isDistinct: Bool? = false
  public var createdAt: Int?
  public var joinedMemberCount: Int?
  public var invitedAt: Int?
  public var members: [Members]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    name = json[SerializationKeys.name].string
    inviter = Inviter(json: json[SerializationKeys.inviter])
    data = json[SerializationKeys.data].string
    coverUrl = json[SerializationKeys.coverUrl].string
    unreadMentionCount = json[SerializationKeys.unreadMentionCount].int
    readReceipt = ReadReceipt(json: json[SerializationKeys.readReceipt])
    isEphemeral = json[SerializationKeys.isEphemeral].boolValue
    maxLengthMessage = json[SerializationKeys.maxLengthMessage].int
    customType = json[SerializationKeys.customType].string
    lastMessage = LastMessage(json: json[SerializationKeys.lastMessage])
    channelUrl = json[SerializationKeys.channelUrl].string
    freeze = json[SerializationKeys.freeze].boolValue
    unreadMessageCount = json[SerializationKeys.unreadMessageCount].int
    isSuper = json[SerializationKeys.isSuper].boolValue
    deliveryReceipt = DeliveryReceipt(json: json[SerializationKeys.deliveryReceipt])
    isPublic = json[SerializationKeys.isPublic].boolValue
    isAccessCodeRequired = json[SerializationKeys.isAccessCodeRequired].boolValue
    memberCount = json[SerializationKeys.memberCount].int
    isDistinct = json[SerializationKeys.isDistinct].boolValue
    createdAt = json[SerializationKeys.createdAt].int
    joinedMemberCount = json[SerializationKeys.joinedMemberCount].int
    invitedAt = json[SerializationKeys.invitedAt].int
    if let items = json[SerializationKeys.members].array { members = items.map { Members(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = inviter { dictionary[SerializationKeys.inviter] = value.dictionaryRepresentation() }
    if let value = data { dictionary[SerializationKeys.data] = value }
    if let value = coverUrl { dictionary[SerializationKeys.coverUrl] = value }
    if let value = unreadMentionCount { dictionary[SerializationKeys.unreadMentionCount] = value }
    if let value = readReceipt { dictionary[SerializationKeys.readReceipt] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.isEphemeral] = isEphemeral
    if let value = maxLengthMessage { dictionary[SerializationKeys.maxLengthMessage] = value }
    if let value = customType { dictionary[SerializationKeys.customType] = value }
    if let value = lastMessage { dictionary[SerializationKeys.lastMessage] = value.dictionaryRepresentation() }
    if let value = channelUrl { dictionary[SerializationKeys.channelUrl] = value }
    dictionary[SerializationKeys.freeze] = freeze
    if let value = unreadMessageCount { dictionary[SerializationKeys.unreadMessageCount] = value }
    dictionary[SerializationKeys.isSuper] = isSuper
    if let value = deliveryReceipt { dictionary[SerializationKeys.deliveryReceipt] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.isPublic] = isPublic
    dictionary[SerializationKeys.isAccessCodeRequired] = isAccessCodeRequired
    if let value = memberCount { dictionary[SerializationKeys.memberCount] = value }
    dictionary[SerializationKeys.isDistinct] = isDistinct
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = joinedMemberCount { dictionary[SerializationKeys.joinedMemberCount] = value }
    if let value = invitedAt { dictionary[SerializationKeys.invitedAt] = value }
    if let value = members { dictionary[SerializationKeys.members] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.inviter = aDecoder.decodeObject(forKey: SerializationKeys.inviter) as? Inviter
    self.data = aDecoder.decodeObject(forKey: SerializationKeys.data) as? String
    self.coverUrl = aDecoder.decodeObject(forKey: SerializationKeys.coverUrl) as? String
    self.unreadMentionCount = aDecoder.decodeObject(forKey: SerializationKeys.unreadMentionCount) as? Int
    self.readReceipt = aDecoder.decodeObject(forKey: SerializationKeys.readReceipt) as? ReadReceipt
    self.isEphemeral = aDecoder.decodeBool(forKey: SerializationKeys.isEphemeral)
    self.maxLengthMessage = aDecoder.decodeObject(forKey: SerializationKeys.maxLengthMessage) as? Int
    self.customType = aDecoder.decodeObject(forKey: SerializationKeys.customType) as? String
    self.lastMessage = aDecoder.decodeObject(forKey: SerializationKeys.lastMessage) as? LastMessage
    self.channelUrl = aDecoder.decodeObject(forKey: SerializationKeys.channelUrl) as? String
    self.freeze = aDecoder.decodeBool(forKey: SerializationKeys.freeze)
    self.unreadMessageCount = aDecoder.decodeObject(forKey: SerializationKeys.unreadMessageCount) as? Int
    self.isSuper = aDecoder.decodeBool(forKey: SerializationKeys.isSuper)
    self.deliveryReceipt = aDecoder.decodeObject(forKey: SerializationKeys.deliveryReceipt) as? DeliveryReceipt
    self.isPublic = aDecoder.decodeBool(forKey: SerializationKeys.isPublic)
    self.isAccessCodeRequired = aDecoder.decodeBool(forKey: SerializationKeys.isAccessCodeRequired)
    self.memberCount = aDecoder.decodeObject(forKey: SerializationKeys.memberCount) as? Int
    self.isDistinct = aDecoder.decodeBool(forKey: SerializationKeys.isDistinct)
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? Int
    self.joinedMemberCount = aDecoder.decodeObject(forKey: SerializationKeys.joinedMemberCount) as? Int
    self.invitedAt = aDecoder.decodeObject(forKey: SerializationKeys.invitedAt) as? Int
    self.members = aDecoder.decodeObject(forKey: SerializationKeys.members) as? [Members]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(inviter, forKey: SerializationKeys.inviter)
    aCoder.encode(data, forKey: SerializationKeys.data)
    aCoder.encode(coverUrl, forKey: SerializationKeys.coverUrl)
    aCoder.encode(unreadMentionCount, forKey: SerializationKeys.unreadMentionCount)
    aCoder.encode(readReceipt, forKey: SerializationKeys.readReceipt)
    aCoder.encode(isEphemeral, forKey: SerializationKeys.isEphemeral)
    aCoder.encode(maxLengthMessage, forKey: SerializationKeys.maxLengthMessage)
    aCoder.encode(customType, forKey: SerializationKeys.customType)
    aCoder.encode(lastMessage, forKey: SerializationKeys.lastMessage)
    aCoder.encode(channelUrl, forKey: SerializationKeys.channelUrl)
    aCoder.encode(freeze, forKey: SerializationKeys.freeze)
    aCoder.encode(unreadMessageCount, forKey: SerializationKeys.unreadMessageCount)
    aCoder.encode(isSuper, forKey: SerializationKeys.isSuper)
    aCoder.encode(deliveryReceipt, forKey: SerializationKeys.deliveryReceipt)
    aCoder.encode(isPublic, forKey: SerializationKeys.isPublic)
    aCoder.encode(isAccessCodeRequired, forKey: SerializationKeys.isAccessCodeRequired)
    aCoder.encode(memberCount, forKey: SerializationKeys.memberCount)
    aCoder.encode(isDistinct, forKey: SerializationKeys.isDistinct)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(joinedMemberCount, forKey: SerializationKeys.joinedMemberCount)
    aCoder.encode(invitedAt, forKey: SerializationKeys.invitedAt)
    aCoder.encode(members, forKey: SerializationKeys.members)
  }

}
