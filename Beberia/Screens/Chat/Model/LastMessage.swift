//
//  LastMessage.swift
//
//  Created by IMAC on 7/22/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class LastMessage: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let data = "data"
    static let updatedAt = "updated_at"
    static let user = "user"
    static let translations = "translations"
    static let customType = "custom_type"
    static let file = "file"
    static let mentionType = "mention_type"
    static let type = "type"
    static let channelUrl = "channel_url"
    static let mentionedUsers = "mentioned_users"
    static let isRemoved = "is_removed"
    static let createdAt = "created_at"
    static let message = "message"
    static let messageId = "message_id"
  }

  // MARK: Properties
  public var data: String?
  public var updatedAt: Int?
  public var user: UserSB?
  public var translations: Translations?
  public var customType: String?
  public var file: File?
  public var mentionType: String?
  public var type: String?
  public var channelUrl: String?
  public var mentionedUsers: [Any]?
  public var isRemoved: Bool? = false
  public var createdAt: Int?
  public var message: String?
  public var messageId: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    data = json[SerializationKeys.data].string
    updatedAt = json[SerializationKeys.updatedAt].int
    user = UserSB(json: json[SerializationKeys.user])
    translations = Translations(json: json[SerializationKeys.translations])
    customType = json[SerializationKeys.customType].string
    file = File(json: json[SerializationKeys.file])
    mentionType = json[SerializationKeys.mentionType].string
    type = json[SerializationKeys.type].string
    channelUrl = json[SerializationKeys.channelUrl].string
    if let items = json[SerializationKeys.mentionedUsers].array { mentionedUsers = items.map { $0.object} }
    isRemoved = json[SerializationKeys.isRemoved].boolValue
    createdAt = json[SerializationKeys.createdAt].int
    message = json[SerializationKeys.message].string
    messageId = json[SerializationKeys.messageId].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = data { dictionary[SerializationKeys.data] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = user { dictionary[SerializationKeys.user] = value.dictionaryRepresentation() }
    if let value = translations { dictionary[SerializationKeys.translations] = value.dictionaryRepresentation() }
    if let value = customType { dictionary[SerializationKeys.customType] = value }
    if let value = file { dictionary[SerializationKeys.file] = value.dictionaryRepresentation() }
    if let value = mentionType { dictionary[SerializationKeys.mentionType] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = channelUrl { dictionary[SerializationKeys.channelUrl] = value }
    if let value = mentionedUsers { dictionary[SerializationKeys.mentionedUsers] = value }
    dictionary[SerializationKeys.isRemoved] = isRemoved
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = messageId { dictionary[SerializationKeys.messageId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.data = aDecoder.decodeObject(forKey: SerializationKeys.data) as? String
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? Int
    self.user = aDecoder.decodeObject(forKey: SerializationKeys.user) as? UserSB
    self.translations = aDecoder.decodeObject(forKey: SerializationKeys.translations) as? Translations
    self.customType = aDecoder.decodeObject(forKey: SerializationKeys.customType) as? String
    self.file = aDecoder.decodeObject(forKey: SerializationKeys.file) as? File
    self.mentionType = aDecoder.decodeObject(forKey: SerializationKeys.mentionType) as? String
    self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? String
    self.channelUrl = aDecoder.decodeObject(forKey: SerializationKeys.channelUrl) as? String
    self.mentionedUsers = aDecoder.decodeObject(forKey: SerializationKeys.mentionedUsers) as? [Any]
    self.isRemoved = aDecoder.decodeBool(forKey: SerializationKeys.isRemoved)
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? Int
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.messageId = aDecoder.decodeObject(forKey: SerializationKeys.messageId) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(data, forKey: SerializationKeys.data)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(user, forKey: SerializationKeys.user)
    aCoder.encode(translations, forKey: SerializationKeys.translations)
    aCoder.encode(customType, forKey: SerializationKeys.customType)
    aCoder.encode(file, forKey: SerializationKeys.file)
    aCoder.encode(mentionType, forKey: SerializationKeys.mentionType)
    aCoder.encode(type, forKey: SerializationKeys.type)
    aCoder.encode(channelUrl, forKey: SerializationKeys.channelUrl)
    aCoder.encode(mentionedUsers, forKey: SerializationKeys.mentionedUsers)
    aCoder.encode(isRemoved, forKey: SerializationKeys.isRemoved)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(messageId, forKey: SerializationKeys.messageId)
  }

}
