//
//  BannerGroupChatModel.swift
//  Beberia
//
//  Created by Lap on 11/19/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import  UIKit

struct BannerGroupChatModel: Codable {
    var thumbnail: String?
    var link: String?
    var id: Double?
}
