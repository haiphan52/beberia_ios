//
//  DeliveryReceipt.swift
//
//  Created by IMAC on 7/22/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class DeliveryReceipt: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let jin = "Jin"
    static let jay = "Jay"
    static let david = "David"
  }

  // MARK: Properties
  public var jin: Int?
  public var jay: Int?
  public var david: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    jin = json[SerializationKeys.jin].int
    jay = json[SerializationKeys.jay].int
    david = json[SerializationKeys.david].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = jin { dictionary[SerializationKeys.jin] = value }
    if let value = jay { dictionary[SerializationKeys.jay] = value }
    if let value = david { dictionary[SerializationKeys.david] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.jin = aDecoder.decodeObject(forKey: SerializationKeys.jin) as? Int
    self.jay = aDecoder.decodeObject(forKey: SerializationKeys.jay) as? Int
    self.david = aDecoder.decodeObject(forKey: SerializationKeys.david) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(jin, forKey: SerializationKeys.jin)
    aCoder.encode(jay, forKey: SerializationKeys.jay)
    aCoder.encode(david, forKey: SerializationKeys.david)
  }

}
