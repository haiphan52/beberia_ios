//
//  FileImageTableViewCell.swift
//  Beberia
//
//  Created by Lap on 12/16/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class FileImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var numberLike: UILabel!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var viewLike: UIView!
    @IBOutlet weak var timeMessageLable: UILabel!
    @IBOutlet weak var nameUserLable: UILabel!
    @IBOutlet weak var imgAvatarUser: UIImageView!
    
    @IBOutlet weak var constrainsTrailingView: NSLayoutConstraint!
    @IBOutlet weak var widthImageAvatarUser: NSLayoutConstraint!
    @IBOutlet weak var widthImageReply: NSLayoutConstraint!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var constrainBottomUserName: NSLayoutConstraint!
    @IBOutlet weak var heightUserName: NSLayoutConstraint!
    @IBOutlet weak var imgMessage: UIImageView!
    
    var tapLike:()->() = {}
    var tapImage:()->() = {}
    var tapAvatar: ()->() = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupData(item: ChatGroup, isLike: (Bool ,Int), typeCell: CellTypeChatNew){
        
        
        numberLike.textColor = isLike.0 ? UIColor.init(hexString: "ff99bb") :  UIColor.init(hexString: "000000")
        imageLike.image = isLike.0 ? R.image.heartComment() : R.image.ic_like()
        
        numberLike.text = String(isLike.1)
        if numberLike.text == "0" {
            numberLike.text = ""
        }else{
            
        }
        timeMessageLable.text = item.created?.toDate().timeAgoDisplayHomeFeed()
        imgMessage.setImage(imageString:  item.media ?? "")
        
        imgAvatarUser.kf.setImage(with: URL.init(string: (item.sender?.avatar ?? "")))
        
        nameUserLable.text = item.sender?.displayName ?? ""

        layoutIfNeeded()
        
        if Int((item.sender!.id ?? 0)) == (UserInfo.shareUserInfo.id ?? 0) {
            constrainsTrailingView.constant = 8
            widthImageAvatarUser.constant = 70
            imgAvatarUser.isHidden = true
            heightUserName.constant = 0
            constrainBottomUserName.constant = 0
        }else{
            constrainsTrailingView.constant = 60
            widthImageAvatarUser.constant = 30
            imgAvatarUser.isHidden = false
            heightUserName.constant = 30
            constrainBottomUserName.constant = 10
        }
        
        
    }
    
    @IBAction func didPressTapLike(_ sender: Any) {
        tapLike()
    }
    
    @IBAction func didPressAvatar(_ sender: Any) {
        tapAvatar()
    }
    
    @IBAction func didPressImage(_ sender: Any) {
        tapImage()
    }
    
}
