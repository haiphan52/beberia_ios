//
//  GroupSuggestCollectionViewCell.swift
//  Beberia
//
//  Created by IMAC on 7/22/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class GroupSuggestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewColor: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgCoverGroup: UIImageView!
    @IBOutlet weak var nameGroupLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     //   viewContainer.layer.masksToBounds = false
     //   viewContainer.dropShadow(offsetX: <#T##CGFloat#>, offsetY: <#T##CGFloat#>, color: <#T##UIColor#>, opacity: <#T##Float#>, radius: <#T##CGFloat#>, scale: <#T##Bool#>)
     //   viewContainer.dropShadow(offsetX: 0, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 10)
    }
}
