//
//  UserChatTextTableViewswift
//  Beberia
//
//  Created by Lap on 12/16/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class UserChatTextTableViewCell: UITableViewCell {
    
    @IBOutlet weak var constrainsBottomViewReply: NSLayoutConstraint!
    @IBOutlet weak var numberLike: UILabel!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var viewLike: UIView!
    @IBOutlet weak var imageReply: UIImageView!
    @IBOutlet weak var heightContentReply: NSLayoutConstraint!
  //  @IBOutlet weak var hieghtReplySender: NSLayoutConstraint!
    @IBOutlet weak var replySenderLable: UILabel!
    @IBOutlet weak var contentReplyLable: UILabel!
    @IBOutlet weak var contentMessageLable: UILabel!
    @IBOutlet weak var timeMessageLable: UILabel!
    @IBOutlet weak var imgAvatarUser: UIImageView!
    @IBOutlet weak var nameUserLable: UILabel!
    @IBOutlet weak var viewReply: UIView!
    @IBOutlet weak var constrainsTrailingView: NSLayoutConstraint!
    @IBOutlet weak var widthImageAvatarUser: NSLayoutConstraint!
    @IBOutlet weak var widthImageReply: NSLayoutConstraint!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var constrainTopUserName: NSLayoutConstraint!
    @IBOutlet weak var heightUserName: NSLayoutConstraint!
    
  //  let gradientLayer = CAGradientLayer()
    var tapMessage:()->() = {}
    var tapAvatar: ()->() = {}
    var tapLike: ()->() = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
  //      viewContainer.addGradientLayer([UIColor.init(hex: "fd8aaa"),UIColor.init(hex: "feb0c5")])
    }
    
//    override func layoutSublayers(of layer: CALayer) {
//            super.layoutSublayers(of: self.layer)
//            gradientLayer.frame = viewContainer.bounds
//
//            let colorSet = [UIColor.init(hex: "fd8aaa"),UIColor.init(hex: "feb0c5")]
//            let location = [0.2, 1.0]
//
//        viewContainer.addGradient(with: gradientLayer, colorSet: colorSet, locations: location)
//    }

    func setupData(item: ChatGroup, isLike: (Bool ,Int) , typeCell: CellTypeChatNew) {
        numberLike.textColor = isLike.0 ? UIColor.init(hexString: "ff99bb") :  UIColor.init(hexString: "000000")
        imageLike.image = isLike.0 ? R.image.heartComment() : R.image.ic_like()
        
        numberLike.text = String(isLike.1)
        if numberLike.text == "0" {
            numberLike.text = ""
        }else{
            
        }
        
        if let dateDouble = item.created {
            timeMessageLable.text = dateDouble.toDate().timeAgoDisplayHomeFeed()
        } else {
            timeMessageLable.text = Date().timeAgoDisplayHomeFeed()
        }
        
        imgAvatarUser.kf.setImage(with: URL.init(string: item.sender?.avatar ?? ""))
        contentMessageLable.text = item.message ?? ""
        nameUserLable.text = item.sender?.displayName ?? ""
        
        if let content = item.reply {
            replySenderLable.text = content.sender?.displayName ?? ""
        }
        
        replySenderLable.text = item.reply?.message ?? ""
        viewReply.isHidden = true
        imageReply.isHidden = true
        heightContentReply.constant = 0
        contentReplyLable.text = ""
        constrainsBottomViewReply.constant = 0
        if let content = item.reply {
            if let messageType = content.messageType, let cellType = CellTypeChatNew(rawValue: messageType) {
                switch cellType {
                case .normal:
                    heightContentReply.constant = 50
                    imageReply.isHidden = true
                    widthImageReply.constant = 0
                    contentReplyLable.text = content.message
                    viewReply.isHidden = false
                    constrainsBottomViewReply.constant = 8.5
                case .file:
                    heightContentReply.constant = 50
                    widthImageReply.constant = 44
                    imageReply.isHidden = false
                    contentReplyLable.text = "[Hình ảnh]"
                    viewReply.isHidden = false
                    constrainsBottomViewReply.constant = 8.5
                case .admin: break
                }
            }
            
        }
        layoutIfNeeded()
        
        
        if Int(item.sender!.id ?? 0) == (UserInfo.shareUserInfo.id ?? 0) {
            // viewContainer.addGradientLayer([UIColor.init(hex: "fd8aaa"),UIColor.init(hex: "feb0c5")])
            viewContainer.backgroundColor = UIColor.init(hexString: "ff99bb", alpha: 0.7)
            constrainsTrailingView.constant = 8
            widthImageAvatarUser.constant = 70
            //            viewContainer.layer.borderWidth = 1
            //            viewContainer.layer.borderColor = UIColor.init(hexString: "ffc440")?.cgColor
            constrainTopUserName.constant = 0
            heightUserName.constant = 0
            imgAvatarUser.isHidden = true
        }else{
            //   viewContainer.addGradientLayer([UIColor.init(hex: "ffe3a5"),UIColor.init(hex: "ffcb57")])
            viewContainer.backgroundColor = UIColor.init(hexString: "ffffff", alpha: 1.0)
            constrainsTrailingView.constant = 60
            widthImageAvatarUser.constant = 30
            //     viewContainer.layer.borderWidth = 0
            constrainTopUserName.constant = 8
            heightUserName.constant = 20
            imgAvatarUser.isHidden = false
        }
        
        
    }
    
    @IBAction func didPressLike(_ sender: Any) {
        tapLike()
    }
    
    @IBAction func didPressAvatar(_ sender: Any) {
        tapAvatar()
    }
    
    @IBAction func didPressMessage(_ sender: Any) {
        tapMessage()
    }
}

extension UIView {
    func addGradient(with layer: CAGradientLayer, gradientFrame: CGRect? = nil, colorSet: [UIColor],
                     locations: [Double], startEndPoints: (CGPoint, CGPoint)? = nil) {
        layer.frame = gradientFrame ?? self.bounds
        layer.frame.origin = .zero

        let layerColorSet = colorSet.map { $0.cgColor }
        let layerLocations = locations.map { $0 as NSNumber }

        layer.colors = layerColorSet
        layer.locations = layerLocations

        if let startEndPoints = startEndPoints {
            layer.startPoint = startEndPoints.0
            layer.endPoint = startEndPoints.1
        }

        self.layer.insertSublayer(layer, above: self.layer)
    }
}
