//
//  MySelfChatImageTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 7/29/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class MySelfChatImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var numberLike: UILabel!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var viewLike: UIView!
    @IBOutlet weak var btnTapImage: UIButton!
    @IBOutlet weak var imageReply: UIImageView!
    @IBOutlet weak var heightContentReply: NSLayoutConstraint!
    @IBOutlet weak var hieghtReplySender: NSLayoutConstraint!
    @IBOutlet weak var replySenderLable: UILabel!
    @IBOutlet weak var contentReplyLable: PaddingLabel!
    @IBOutlet weak var nameUserLable: UILabel!
    @IBOutlet weak var imgAvatarUser: UIImageView!
    @IBOutlet weak var timeMessageLable: UILabel!
    @IBOutlet weak var imgMessage: UIImageView!
    
    var tapImage:(Int)->() = {_ in}
    var tapAvatar: ()->() = {}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didPressTapImage(_ sender: UIButton) {
        tapImage(sender.tag)
    }
    
    @IBAction func didPressAvatar(_ sender: UIButton) {
       // tapAvatar()
    }
}
