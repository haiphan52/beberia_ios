//
//  SelectUserCollectionViewCell.swift
//  Beberia
//
//  Created by IMAC on 6/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SelectUserCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageAvatarUser: UIImageView!
    @IBOutlet weak var btnRemoveUser: UIButton!
    
    var bag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
           super.prepareForReuse()
           bag = DisposeBag()
           
           
       }
       

}
