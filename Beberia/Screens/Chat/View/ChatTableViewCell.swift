//
//  ChatTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 6/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var widthImgNoti: NSLayoutConstraint!
    @IBOutlet weak var numberMemberLable: UILabel!
    @IBOutlet weak var widthUnreadLable: NSLayoutConstraint!
    @IBOutlet weak var unreadMessageLable: UILabel!
    @IBOutlet weak var timeLastMessageLable: UILabel!
    @IBOutlet weak var imageNoti: UIImageView!
    @IBOutlet weak var lastMessageLable: UILabel!
    @IBOutlet weak var nameChatGroupLable: UILabel!
    @IBOutlet weak var imgChatGroup: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
