//
//  TestCollectionViewCell.swift
//  Beberia
//
//  Created by IMAC on 7/27/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class TestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCover: UIView!
    @IBOutlet weak var viewColor: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgCoverGroup: UIImageView!
    @IBOutlet weak var nameGroupLable: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imgCoverGroup.layer.cornerRadius = 23
        imgCoverGroup.clipsToBounds = true
        viewCover.layer.cornerRadius = 26
    }

}
