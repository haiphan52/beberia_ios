//
//  NameGroupTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 7/22/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class NameGroupTableViewCell: UITableViewCell {

    @IBOutlet weak var nameGroupLable: UILabel!
    @IBOutlet weak var imgCoverGroup: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
