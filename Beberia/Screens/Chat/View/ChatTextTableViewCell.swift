//
//  ChatTextTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 6/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ChatTextTableViewCell: UITableViewCell {

    @IBOutlet weak var numberLike: UILabel!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var viewLike: UIView!
    @IBOutlet weak var btnTapAvatar: UIButton!
    @IBOutlet weak var imageReply: UIImageView!
    @IBOutlet weak var heightContentReply: NSLayoutConstraint!
    @IBOutlet weak var hieghtReplySender: NSLayoutConstraint!
    @IBOutlet weak var replySenderLable: UILabel!
    @IBOutlet weak var contentReplyLable: PaddingLabel!
    @IBOutlet weak var contentMessageLable: UILabel!
    @IBOutlet weak var timeMessageLable: UILabel!
    @IBOutlet weak var imgAvatarUser: UIImageView!
    @IBOutlet weak var nameUserLable: UILabel!
    
    var tapAvatar: ()->() = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func didPressAvatar(_ sender: UIButton) {
        tapAvatar()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

@IBDesignable class PaddingLabel: UILabel {

    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        numberOfLines = 0
        let size = super.intrinsicContentSize
        return CGSize(width: min(size.width + leftInset + rightInset, 10000), height: min(size.height + topInset + bottomInset, 10000))
    }

//    override var bounds: CGRect {
//        didSet {
//            // ensures this works within stack views if multi-line
//            preferredMaxLayoutWidth = bounds.width - (leftInset + rightInset)
//        }
//    }
    
    override func textRect(forBounds bounds:CGRect,
                               limitedToNumberOfLines n:Int) -> CGRect {
        let b = bounds
        let tr = b.inset(by: UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset))
        let ctr = super.textRect(forBounds: tr, limitedToNumberOfLines: 0)
        // that line of code MUST be LAST in this function, NOT first
        return ctr
    }
}
