//
//  AccountVerifySuccessVC.swift
//  Beberia
//
//  Created by IMAC on 3/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class AccountVerifySuccessVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSuTitle: UILabel!
    
    var typeView = TypeView.forgetPass
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch typeView {
        case .forgetPass:
            lblTitle.text = R.string.localizable.loginTitleSetPassSuccess()
            lblSuTitle.text = R.string.localizable.loginSutitleSetPassSuccess()
        default:
            lblTitle.text = R.string.localizable.loginVerifySetPassSuccess()
            lblSuTitle.text = ""
        }
    }

}
