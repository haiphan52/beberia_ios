//
//  InfoViewController.swift
//  Beberia
//
//  Created by IMAC on 8/26/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import RxSwift
import RxCocoa

class InfoLoginViewController: BaseViewController{
    
    //MARK: Outlet
    @IBOutlet weak var btnHCM: UIButton!
    @IBOutlet weak var btnHaNoi: UIButton!
    @IBOutlet weak var btnKhac: UIButton!
    @IBOutlet weak var btnFinish: UIButton!
   // @IBOutlet weak var tbvAddChildren: UITableView!
    @IBOutlet weak var tagViewLocation: UIView!
    @IBOutlet weak var viewBTN: UIView!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnDistrict: UIButton!
    @IBOutlet weak var viewDistrict: UIView!
    @IBOutlet weak var imgArrowDistrict: UIImageView!
    @IBOutlet weak var phoneNumberTf: UITextField!
    @IBOutlet weak var btn1824Age: UIButton!
     @IBOutlet weak var btn2529Age: UIButton!
     @IBOutlet weak var btn3034Age: UIButton!
     @IBOutlet weak var btn3540Age: UIButton!
    
    //MARK: Properties

    var ageRange = 0
    var errorPhone = ""
    var isValidPhone = false
    var userId = 0
    var locationID = 0
    var idDistrict = ""

    var bag = DisposeBag()
    var username = ""
    var password = ""
    var isCheckPhone = false

    var index = 0
    {
        didSet{
            locationID = index == 0 ? 1: 79
        }
    }
    var arrayChildrens = [InfoChildren.init(json: "")]
    var isLocationHN : Bool = true {
        didSet{
            if isLocationHN{
                Utils.updateColorLocation(btnSelect: btnHaNoi, btnUnSelect: btnKhac, viewSelect: UIView(), view: viewBTN)
            }
        }
    }
    var arrayBaby = [[String: Any]]()
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        customLeftBarButton()
        setupUI()
        initRx()
    }
    
    func initRx(){
        phoneNumberTf.rx.text.orEmpty.asObservable().skip(1).distinctUntilChanged().debounce(.milliseconds(300), scheduler: MainScheduler.asyncInstance).subscribe { (text) in

            let isValid  = "".isValidPhone(phone: text.element ?? "")

            self.errorPhone = isValid ? "" : "Số điện thoại không đúng định dạng"
            if isValid {
                self.checkPhone(phone: text.element ?? "", completion: { message in
                    self.errorPhone = message
                })
            }else{
                self.isValidPhone = false
            }
            
        }.disposed(by: disposeBag)
    }
    
    //MARK: Setup UI
    func setupUI(){
        self.title = R.string.localizable.loginTitleInfo()
        isLocationHN = true
        btnFinish.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 20)
        Utils.updateColorLocation(btnSelect: btnHaNoi, btnUnSelect: btnDistrict, viewSelect: UIView(), view: viewDistrict)
        setColorButton(buttons: [btn1824Age,btn2529Age,btn3034Age,btn3540Age])
    }

    func setColorButton(buttons: [UIButton]){
        for button in buttons {
            button.setTitleColor(UIColor.init(hexString: AppColor.colorSelect,alpha: 0.3), for: .normal)
            button.backgroundColor =  UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.4)
            button.borderColor = UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.5)
            button.borderWidth = 1
            button.cornerRadius = 15
        }
    }
    

    func setColorBnt(btnSelect:UIButton, btnUnselect:UIButton, viewSelect:UIView, view:UIView){
        Utils.updateColorLocation(btnSelect: btnSelect, btnUnSelect: btnUnselect, viewSelect: viewSelect, view:view)
    }
    
    func checkPhone(phone: String, completion:@escaping (String)->()){
        APIManager.checkPhone(phone: phone, callbackSuccess: { (isValue) in
            if isValue {
                self.isValidPhone = true
            }else{
                self.errorPhone = "Số điện thoại đã được sử dụng!"
                self.isValidPhone = false
            }
        }) { (error, code) in
            self.isValidPhone = false
            self.errorPhone = "Số điện thoại đã được sử dụng!"
        }
    }
    
    //MARK: Action
    @IBAction func didPressFinish(_ sender: Any) {
 
//        guard let sdt = phoneNumberTf.text, sdt.count > 0 else {
//            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Số điện thoại không được để trống")
//            return
//        }
//
//        if !self.isValidPhone{
//            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: errorPhone)
//            return
//        }

        if locationID == 0 {

//        if self.arrayChildrens[0].nickname ?? "" == ""{
//             Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Số điện thoại không được để trống")
//             return
//         }
//
//        else if !self.isCheckPhone {
//            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Số điện thoại đã được đăng ký")
//            return
//        }
        
//         else{
//             Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Số điện thoại sai định dạng")
//
//         }
//
//        if self.arrayChildrens[0].nickname ?? "" == "" || (self.arrayChildrens[0].nickname ?? "").isValidEmail() == false{
//            if self.arrayChildrens[0].nickname ?? "" == ""{
//                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Số điện thoại không được để trống")
//                return
//            }
//
//            else{
//                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Số điện thoại sai định dạng")
//
//            }
//        }
 //       else if locationID == 0 {

            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Mời bạn chọn thành phố")
            return
        }
        else if idDistrict == "" {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Mời bạn chọn quận")
            return
        }
        else if ageRange == 0 {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Mời bạn chọn độ tuổi")
            return
        }
        else{
            let postVC = InfoChildViewController.init(nib: R.nib.infoChildViewController)
            postVC.hidesBottomBarWhenPushed = true
            postVC.getbirthday = self.arrayChildrens[0].birthday ?? 0
            postVC.ageRange = ageRange
            postVC.getSDT = phoneNumberTf.text ?? ""
            postVC.getlocationID = locationID
            postVC.getDistrictID = idDistrict

            self.navigationController?.pushViewController(postVC, animated: true)
        }
    }
    
    @IBAction func didPressAge(_ sender: UIButton) {
           setColorButton(buttons: [btn1824Age,btn2529Age,btn3034Age,btn3540Age])
           
           sender.setTitleColor(UIColor.init(hexString: AppColor.colorYellow,alpha: 0.9), for: .normal)
           sender.backgroundColor =  UIColor.init(hexString: AppColor.colorBGMenu,alpha: 0.4)
           sender.borderColor = UIColor.init(hexString: AppColor.colorYellow,alpha: 0.9)
           
           ageRange = sender.tag
       }

    @IBAction func didPressDistrict(_ sender: Any) {
        let locationVC = DistrictViewController.init(nib: R.nib.districtViewController)
        locationVC.locationID = self.locationID
        locationVC.hidesBottomBarWhenPushed = true
        locationVC.didSelectedLocation = { city in
            self.idDistrict = city.districtCode ?? ""
            self.btnDistrict.setTitle(city.districtName, for: .normal)
            self.setColorBnt(btnSelect: self.btnDistrict, btnUnselect: self.btnHCM,viewSelect: self.viewDistrict, view: UIView())
            self.imgArrowDistrict.image = R.image.ic_ArrowDownYL()
            if city.districtCode == "" {
                Utils.districtColorDefault(btn: self.btnDistrict, img: self.imgArrowDistrict, view: self.viewDistrict)
            }
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    @IBAction func didPressOther(_ sender: Any) {

        setColorBnt(btnSelect: btnKhac, btnUnselect: btnKhac,viewSelect: UIView(), view: viewBTN)

        let locationVC = LocationViewController.init(nib: R.nib.locationViewController)
        locationVC.hidesBottomBarWhenPushed = true
        locationVC.didSelectedLocation = { city in
            self.locationID = city.locationCode ?? 0
            self.btnKhac.setTitle(city.locationNameVi, for: .normal)
            if self.locationID == 79 {
                self.btnKhac.setTitle(R.string.localizable.commonTPHCMShort(), for: .normal)
            }
            self.setColorBnt(btnSelect: self.btnKhac, btnUnselect: self.btnHCM,viewSelect: self.viewBTN, view: UIView())
            self.imgArrow.image = R.image.ic_ArrowDownYL()
            
            // resetDistrict
            self.idDistrict = ""
            Utils.districtColorDefault(btn: self.btnDistrict, img: self.imgArrowDistrict, view: self.viewDistrict)
            self.btnDistrict.setTitle(R.string.localizable.commonDistrict(), for: .normal)
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: true)
    }
}


//MARK: UItableview Delegate
extension InfoLoginViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChildrens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.infoMeTableViewCell, for: indexPath)!
        cell.btnClose.tag = indexPath.row
        cell.lblTitleInfo.text = "\(R.string.localizable.loginInfo())"
        let children = self.arrayChildrens[indexPath.row]
        cell.btnClose.isHidden = true
        if children.birthday == nil{
            cell.lblBirthday.text = ""
        }else{
            cell.lblBirthday.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(children.birthday ?? 0 )), format: Key.DateFormat.DateFormatddMMyyyy)
        }
        
        if children.gender == 1 {
            Utils.updateColorButton(btnSelect: cell.btnNam, btnUnSelect: cell.btnNu)
        }else{
            Utils.updateColorButton(btnSelect: cell.btnNu, btnUnSelect: cell.btnNam)
        }
        cell.lblSDT.text = children.nickname

//        cell.tapCloseCell = { index in
//            if self.arrayChildrens.count > 1{
//                self.arrayChildrens.remove(at: index)
//                self.tbvAddChildren.reloadData()
//            }
//        }
//        cell.tapAddChildren = {
//            let childrenModel = InfoChildren.init(json: "")
//            self.arrayChildrens.append(childrenModel)
//            self.tbvAddChildren.reloadData()
//        }
//        cell.viewAddChildren.isHidden = false
        if self.arrayChildrens.count > 1{
            if (indexPath.row != self.arrayChildrens.count - 1){
                cell.viewAddChildren.isHidden = true
            }
        }
        cell.updateGender = { index , gender in
            self.arrayChildrens[index].gender = gender
        }
        cell.updateSDT = { index , name in
            self.arrayChildrens[index].nickname = name
        }
        
        cell.lblSDT.rx.text.orEmpty
        .throttle(.milliseconds(300), scheduler: MainScheduler.instance)
        .distinctUntilChanged()
            .subscribe({ text in
                print(text.element!)
                self.checkPhone(phone: text.element!)
        }).disposed(by: bag)
        
        cell.updateBirthday = { index , date in
            let dateFormatter = DateFormatter()
                   dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyyyy
            cell.lblBirthday.text = dateFormatter.string(from: date)
            
//            cell.lblBirthday.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(homeFeed.created ?? 0)), format: Key.DateFormat.DateFormatddMMyy)
            self.arrayChildrens[index].birthday = date.timeIntervalSince1970
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.arrayChildrens.count > 1{
            if (indexPath.row != self.arrayChildrens.count - 1){
                return 230
            }
            return 270
        }
        return 270
    }
}

extension InfoLoginViewController{
    func checkPhone(phone: String){
        APIManager.checkPhone(phone: phone, callbackSuccess: { (isCheck) in

            self.isCheckPhone = isCheck

        }) { (error, status) in
            print(status)
            self.isCheckPhone = false
           // Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Số điện thoại đã được đăng ký")
        }
    }
}



