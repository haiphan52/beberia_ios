//
//  InfoChildren.swift
//
//  Created by IMAC on 8/27/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class InfoChildren: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let nickname = "nickname"
    static let gender = "gender"
    static let birthday = "birthday"
  }

  // MARK: Properties
  public var nickname: String? = ""
  public var gender: Int? = 1
  public var birthday: Double?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    nickname = json[SerializationKeys.nickname].string
    gender = json[SerializationKeys.gender].int ?? 1
    birthday = json[SerializationKeys.birthday].double
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = nickname { dictionary[SerializationKeys.nickname] = value }
    if let value = gender { dictionary[SerializationKeys.gender] = value }
    if let value = birthday { dictionary[SerializationKeys.birthday] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.nickname = aDecoder.decodeObject(forKey: SerializationKeys.nickname) as? String
    self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? Int
    self.birthday = aDecoder.decodeObject(forKey: SerializationKeys.birthday) as? Double
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(nickname, forKey: SerializationKeys.nickname)
    aCoder.encode(gender, forKey: SerializationKeys.gender)
    aCoder.encode(birthday, forKey: SerializationKeys.birthday)
  }

}
