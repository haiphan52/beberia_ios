//
//  InfoLoginTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 8/26/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class InfoLoginTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblBirthday: UITextField!
    @IBOutlet weak var viewSex: UIView!
    @IBOutlet weak var lblName: UITextField!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnAddChildren: UIButton!
    @IBOutlet weak var viewAddChildren: UIView!
    @IBOutlet weak var btnNu: UIButton!
    @IBOutlet weak var btnNam: UIButton!
    @IBOutlet weak var lblTitleInfo: UILabel!
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .foregroundColor: UIColor.init(hexString: AppColor.colorYellow),
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    var tapAddChildren: ()->() = {}
    var tapCloseCell  : (_ index: Int)->() = {_ in}
    var updateBirthday : (_ index: Int,_ date: Date)->() = {_,_ in}
    var updateNameChildren : (_ index: Int,_ name: String)->() = {_,_ in}
    var updateGender : (_ index: Int,_ gender: Int)->() = {_,_ in}
    var dateBrithday = Date()
    
    var isGenderNam : Bool = true {
        didSet{
            if isGenderNam{
                Utils.updateColorButton(btnSelect: btnNam, btnUnSelect: btnNu)
            }else{
                Utils.updateColorButton(btnSelect: btnNu, btnUnSelect: btnNam)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isGenderNam = true
        lblBirthday.delegate = self
        lblName.delegate = self
        let attributeString = NSMutableAttributedString(string: "Thêm con",
                                                        attributes: yourAttributes)
        btnAddChildren.setAttributedTitle(attributeString, for: .normal)
        
        lblName.addTarget(self, action: #selector(InfoLoginTableViewCell.fieldChanged(textfieldChange:)), for: .editingChanged)
    }
    
    @objc func fieldChanged(textfieldChange: UITextField){
        updateNameChildren(btnClose.tag, textfieldChange.text ?? "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func didPressName(_ sender: Any) {
        isGenderNam = true
        updateGender(btnClose.tag, 1)
    }
    
    @IBAction func didPressNu(_ sender: Any) {
        isGenderNam = false
        updateGender(btnClose.tag, 0)
    }
    
    @IBAction func didPressAddChildren(_ sender: Any) {
        tapAddChildren()
    }
    
    @IBAction func didPressClose(_ sender: Any) {
        tapCloseCell(btnClose.tag)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyyyy
        lblBirthday.text = dateFormatter.string(from: sender.date)
        dateBrithday = sender.date
    }
}

extension InfoLoginTableViewCell: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == lblBirthday
        {
            let datePickerView:UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePicker.Mode.date
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(InfoLoginTableViewCell.datePickerValueChanged), for: UIControl.Event.valueChanged)
            
            if #available(iOS 13.4, *) {
                datePickerView.preferredDatePickerStyle = .wheels
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == lblBirthday{
           updateBirthday(btnClose.tag, dateBrithday)
        } else{
           // print(textField.text!)
            
           // updateNameChildren(btnClose.tag, textField.text ?? "")
        }
    }
    
    
    
}


