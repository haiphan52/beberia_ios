//
//  RegisterViewController.swift
//  Beberia
//
//  Created by iMAC on 8/27/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

class RegisterViewController: BaseViewController {
    
    //MARK: Outlet
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var imgUserName: UIImageView!
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtPassWord: UITextField!
    @IBOutlet weak var imgPassWord: UIImageView!
    @IBOutlet weak var viewPassWord: UIView!
    @IBOutlet weak var lblPassWord: UILabel!
    @IBOutlet weak var imgRePassWord: UIImageView!
    @IBOutlet weak var txtRePassWord: UITextField!
    @IBOutlet weak var lblRePassWord: UILabel!
    @IBOutlet weak var viewRePassWord: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var imgPhone: UIImageView!
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnShowPass: UIButton!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var btnShowRePass: UIButton!
    @IBOutlet weak var lblBirtDay: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var imgEmail: UIImageView!
    @IBOutlet weak var viewEmail: UIView!
    
    //MARK: Properties
 
    var city_code = 0
    
    var isCheckShowPass = true
    {
        didSet{
            btnShowPass.setImage(isCheckShowPass ? R.image.ic_SelectedView() : R.image.ic_View(), for: .normal)
            txtPassWord.isSecureTextEntry = !isCheckShowPass
        }
    }
    
    var isCheckShowRePass = true
    {
        didSet{
            btnShowRePass.setImage(isCheckShowRePass ? R.image.ic_SelectedView() : R.image.ic_View(), for: .normal)
            txtRePassWord.isSecureTextEntry = !isCheckShowRePass
        }
    }
    
    var checkedMark = false{
        didSet{
            imgCheck.image = checkedMark ? R.image.ic_CheckBox() : R.image.ic_Box()
            //            imgCheck.setBackgroundImage(checkedMark ? R.image.ic_CheckBox() : R.image.ic_Box() , for: .normal)
            //            btnCheck.backgroundColor = checkedMark ? UIColor(red: 255/255, green: 241/255, blue: 151/255, alpha: 1) : .white
            //            btnCheck.borderWidth = checkedMark ? 0 : 1
            //            btnCheck.borderColor = checkedMark ? UIColor.clear : UIColor.white
        }
    }
    var checkedname = false
    var isCheckPhone = false
    var isCheckPass = false
    {
        didSet{
            self.lblPassWord.text = isCheckPass ? "" : R.string.localizable.registerCorectLengPass()
            self.viewPassWord.backgroundColor = isCheckPass ? .lightGray : .red
            self.imgPassWord.image = isCheckPass ? R.image.ic_Succes() : R.image.ic_Close()
            if !isCheckPass { self.lblPassWord.textColor  = .red }
        }
    }
    
    var isCheckPassDulicate = false
    {
        didSet{
            self.imgRePassWord.image = isCheckPassDulicate ? R.image.ic_Succes() : R.image.ic_Close()
            self.lblRePassWord.text = isCheckPassDulicate ? "" :  R.string.localizable.registerCorrectPass()
            self.viewRePassWord.backgroundColor = isCheckPassDulicate ? .lightGray : .red
        }
    }
    
    var isCheckEmail = false
    {
        didSet{
            self.imgEmail.image = isCheckEmail ? R.image.ic_Succes() : R.image.ic_Close()
            if (txtEmail.text ?? "").count > 0{
                self.lblBirtDay.text = isCheckEmail ? "" :  "Email sai định dạng"
            }else{
                self.lblBirtDay.text = isCheckEmail ? "" :  "Email không được để trống!"
            }
            
            self.viewEmail.backgroundColor = isCheckEmail ? .lightGray : .red
        }
    }
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        btnNext.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 20)
        
    }
    
    //MARK: Setup UI
    func setupUI(){
        self.lblUserName.text = R.string.localizable.registerCorectLengUserName()
        self.lblPassWord.text = R.string.localizable.registerCorectLengPass()
        self.lblUserName.textColor = .lightGray
        self.title = R.string.localizable.loginTitleRegis()
        txtUserName.becomeFirstResponder()
        txtUserName.delegate = self
        txtPhone.delegate = self
        txtPassWord.delegate = self
        txtRePassWord.delegate = self
        txtEmail.delegate = self
        txtUserName.tag = 1
        txtEmail.tag = 2
        txtPassWord.tag = 3
        txtRePassWord.tag = 4
        customLeftBarButton()
        
        isCheckShowPass = false
        isCheckShowRePass = false
        checkedMark = false
        //  isCheckPass = false
        
        txtUserName.addTarget(self, action: #selector(RegisterViewController.fieldUserNameChanged(textfieldChange:)), for: .editingChanged)
        txtPhone.addTarget(self, action: #selector(RegisterViewController.textfieldPhoneChange(textfieldChange:)), for: .editingChanged)
        txtPassWord.addTarget(self, action: #selector(RegisterViewController.fieldPassChanged(textfieldChange:)), for: .editingChanged)
        txtRePassWord.addTarget(self, action: #selector(RegisterViewController.textfieldRePassChange(textfieldChange:)), for: .editingChanged)
        
    }
    
    func setupUIInit(){
        if isCheckPassDulicate && isCheckPass /*&& isCheckPhone*/ && checkedname && checkedMark{
            self.lblPassWord.text = ""
            self.viewPassWord.backgroundColor = .lightGray
            self.imgPassWord.image = UIImage.init(named: "")
            self.lblRePassWord.text = ""
            self.viewRePassWord.backgroundColor = .lightGray
            self.imgRePassWord.image = UIImage.init(named: "")
            self.register()
            //            self.loginWithPhone()
            //                let postVC = InfoLoginViewController.init(nib: R.nib.infoLoginViewController)
            //                postVC.hidesBottomBarWhenPushed = true
            //                postVC.username = txtUserName.text!
            //                postVC.password = txtPassWord.text!
            //                self.navigationController?.pushViewController(postVC, animated: true)
        }
    }
    
    //MARK: @objc
    @objc func fieldUserNameChanged(textfieldChange: UITextField){
        checkedname = false
    }
    
    @objc func textfieldPhoneChange(textfieldChange: UITextField){
        isCheckPhone = false
    }
    
    @objc func fieldPassChanged(textfieldChange: UITextField){
        isCheckPass = false
        txtPassWord.text = textfieldChange.text
        checkPassWord()
        checkPassDulicate()
    }
    
    @objc func textfieldRePassChange(textfieldChange: UITextField){
        isCheckPassDulicate = false
        txtRePassWord.text = textfieldChange.text
        checkPassDulicate()
    }
    
    //MARK: Action
    @IBAction func didPressShowPass(_ sender: UIButton) {
        isCheckShowPass = !isCheckShowPass
        
    }
    
    @IBAction func didPressShowRePass(_ sender: Any) {
        isCheckShowRePass = !isCheckShowRePass
    }
    
    
    @IBAction func btnCheck(_ sender: UIButton) {
        checkedMark = !checkedMark
        
    }
    
    @IBAction func btnPrivacy(_ sender: Any) {
        let privacy = PrivacyViewController.init(nib: R.nib.privacyViewController)
        self.navigationController?.pushViewController(privacy, animated: true)
    }
    
    func checkPassDulicate(){
        isCheckPassDulicate = (txtRePassWord.text != txtPassWord.text) ? false : true
    }
    
    @IBAction func btnCity(_ sender: Any) {
        let locationVC = LocationViewController.init(nib: R.nib.locationViewController)
        locationVC.hidesBottomBarWhenPushed = true
        
        locationVC.didSelectedLocation = { city in
            switch city.locationCode {
            case 1:
                self.txtPhone.text = city.locationNameVi ?? ""
                self.city_code = city.locationCode ?? 0
            case 79:
                self.txtPhone.text = R.string.localizable.commonTPHCMShort()
                self.city_code = city.locationCode ?? 0
            default:
                self.txtPhone.text = city.locationNameVi ?? ""
                self.city_code = city.locationCode ?? 0
                break
                
            }
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    
    
    @IBAction func btnNext(_ sender: Any) {
        
        validateField()
        
    }
    
    //MARK: Validate
    func validateField(){
        if !self.checkedname{
            checkName()
            return
        }
        
//        if let email = txtEmail.text, email.count == 0{
//            lblBirtDay.text = "Email không được để trống"
//            return
//        }
        
        if txtEmail.text!.isValidEmail() == false{
            lblBirtDay.text = "Email sai định dạng"
            return
        }
        
        if !isCheckEmail{
            lblBirtDay.text = "Email đã tồn tại!"
            return
        }
        
        if !self.isCheckPass{
            checkPassWord()
            return
        }
        
        if !isCheckPassDulicate{
            checkPassDulicate()
            return
        }
        if checkedMark == false {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: R.string.localizable.registerErrorMess())
            return
        }
        //        if city_code == 0 {
        //            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: R.string.localizable.registerErrorMessCity())
        //            return
        //        }
        
        
        setupUIInit()
    }
    
    
    
    func checkName (){
        
        if txtUserName.text?.containsSpecialCharacter == true {
            self.lblUserName.text = "Tài khoản không được chứa ký tự đặc biệt"
            self.lblUserName.textColor = .red
            self.viewUserName.backgroundColor = .red
            self.imgUserName.image = R.image.ic_Close()
            return
        }
        guard let userName1 = txtUserName.text , userName1.count <= 25 else {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.registerCorectLengUserName())
            self.imgUserName.image = R.image.ic_Close()
            return
        }
        guard let userName = txtUserName.text , userName.count > 0 else {
            
            self.lblUserName.text = R.string.localizable.registerCorectRequirePass()
            self.lblUserName.textColor = .red
            self.viewUserName.backgroundColor = .red
            self.imgUserName.image = R.image.ic_Close()//UIImage(named: "close-1")
            
            return
        }
        if (txtUserName.text?.contains(" "))!{
            self.lblUserName.text = R.string.localizable.registerCorectRequirePass()
            self.lblUserName.textColor = .red
            self.viewUserName.backgroundColor = .red
            self.imgUserName.image = R.image.ic_Close()
        }else{
            if !(txtUserName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
                SVProgressHUD.show()
                APIManager.checkName(display_name: txtUserName.text!, callbackSuccess: { [weak self] (isSuccess) in
                    guard let self = self else { return }
                    self.imgUserName.image = R.image.ic_Succes()//UIImage(named: "checked")
                    self.lblUserName.text = ""
                    self.viewUserName.backgroundColor = .lightGray
                    self.checkedname = true
                    SVProgressHUD.dismiss()
                    //    self.validateField()
                    //  self.setupUIInit()
                }) { (error, statusCode) in
                    self.imgUserName.image = R.image.ic_Close()//UIImage(named: "close-1")
                    self.lblUserName.text = R.string.localizable.registerCheckName()
                    self.viewUserName.backgroundColor = .red
                    self.checkedname = false
                    SVProgressHUD.dismiss()
                }
            }else {
                // agjad
            }
        }
        
    }
    
    func validatePhone() -> Bool{
        if txtPhone.text?.isValidPhoneNum(phone: txtPhone.text!) == false {
            self.imgPhone.image = R.image.ic_Close()//UIImage(named: "close-1")
            self.lblPhone.text = R.string.localizable.registerCorectFomartPhone()
            self.viewPhone.backgroundColor = .red
            return false
        }
        return true
    }
    
    //MARK: Request API
    func checkEmail(){
        SVProgressHUD.show()
        APIManager.checkEmail(email: txtEmail.text!, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else { return }
            self.imgPhone.image = R.image.ic_Succes()
            self.lblBirtDay.text = ""
            self.viewPhone.backgroundColor = .lightGray
            SVProgressHUD.dismiss()
            self.isCheckEmail = true
          //  self.validateField()
            
        }) { (error, statusCode) in
            self.isCheckEmail = false
            self.imgPhone.image = R.image.ic_Close()
            self.lblBirtDay.text = "Email đã tồn tại!"
            self.viewPhone.backgroundColor = .red
            
            SVProgressHUD.dismiss()
        }
    }
    
    func checkPassWord(){
        isCheckPass =  txtPassWord.text!.count < 6 || (txtPassWord.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! ? false : true
        
    }

    
    func register(){
        SVProgressHUD.show()
        APIManager.register(email: txtEmail.text!, city_code: city_code, display_name: txtUserName.text!, password: txtPassWord.text!, fcmToken: Utils.getAppDelegate().tokennv, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else { return }
            self.imgUserName.image = R.image.ic_Succes()//UIImage(named: "checked")
            self.lblUserName.text = ""
            self.viewUserName.backgroundColor = .lightGray
            self.lblPassWord.text = ""
            self.viewPassWord.backgroundColor = .lightGray
            self.imgPassWord.image = R.image.ic_Succes()//UIImage(named: "checked")
            self.lblRePassWord.text = ""
            self.viewRePassWord.backgroundColor = .lightGray
            self.imgRePassWord.image = R.image.ic_Succes()//UIImage(named: "checked")
            
            //            let postVC = InfoChildViewController.init(nib: R.nib.infoChildViewController)
            //            postVC.hidesBottomBarWhenPushed = true
            //            postVC.getemail = self.txtBirthDay.text ?? ""
            //            postVC.getlocationID = self.city_code
            let postVC = InfoLoginViewController.init(nib: R.nib.infoLoginViewController)
            postVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(postVC, animated: true)
            SVProgressHUD.dismiss()
            
            
            Utils.getAppDelegate().username = self.txtUserName.text!
            Utils.getAppDelegate().password = self.txtPassWord.text!
            
        }, failed: { (statusCode, error,phone,password) in
            
            self.imgUserName.image = UIImage(named: "")
            self.lblUserName.text = ""
            self.viewUserName.backgroundColor = .lightGray
            self.lblPassWord.text = ""
            self.viewPassWord.backgroundColor = .lightGray
            self.imgPassWord.image = UIImage(named: "")
            if error != "" || password != "" {
                if error != "" {
                    self.imgUserName.image = R.image.ic_Close()//UIImage(named: "close-1")
                    self.lblUserName.text = R.string.localizable.registerCheckName()
                    self.viewUserName.backgroundColor = .red
                }
                if password != "" {
                    self.lblPassWord.text = password
                    self.viewPassWord.backgroundColor = .red
                    self.imgPassWord.image = R.image.ic_Close()//UIImage(named: "close-1")
                }
            }
            if phone != "" {
                self.lblBirtDay.text = "Email đã tồn tại!"
                self.viewEmail.backgroundColor = .red
                self.imgEmail.image = R.image.ic_Close()//UIImage(named: "close-1")
            }
            SVProgressHUD.dismiss()
        })
        
    }
}

//MARK: UITextField Delegate
extension RegisterViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        switch textField.tag {
        case 1:
            checkName()
            break
        case 2:
            
            isCheckEmail = txtEmail.text!.isValidEmail()
            if isCheckEmail { checkEmail() }
            break
            //        case 3:
            //            checkPassWord()
            //        case 4:
            //            checkPassDulicate()
            
        default: break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range = string.rangeOfCharacter(from: whitespaceSet)
        if let _ = range {
            return false
        }
        else {
            return true
        }
    }
    
}

