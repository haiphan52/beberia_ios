//
//  ForgetPassVC.swift
//  Beberia
//
//  Created by IMAC on 3/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

class ForgetPassVC: BaseViewController {
    
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var tfMail: UITextField!
    @IBOutlet weak var titleLable: UILabel!
    
    var typeView = TypeView.forgetPass
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customLeftBarButton()
        
        titleLable.text = R.string.localizable.loginTitleForgetPass()
        if typeView == .forgetPass{
            self.title = R.string.localizable.loginForgetPass()
        }else{
            self.title = "Xác nhận Email"
        }
        
        
    }
    
    func checkMail()->Bool{
        guard let email = tfMail.text, email.count > 0 else {
            lblError.text = "Email không được để trống!"
            return false
        }
        if email.isValidEmail() == false{
            lblError.text = "Email sai định dạng"
            return false
        }
        
        sendOTP(mail: email)
        
        return true
        
    }
    
    func sendOTP(mail: String){
        SVProgressHUD.show()
        APIManager.sendOtp(email: mail, type: typeView, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else  { return }
            SVProgressHUD.dismiss()
            if isSuccess{
                let accountVerifyVC = VerifyAccountViewController.init(nib: R.nib.verifyAccountViewController)
                accountVerifyVC.typeView = self.typeView
                accountVerifyVC.mail = mail
                self.navigationController?.pushViewController(accountVerifyVC, animated: true)
            }
        }) { (error, code) in
            SVProgressHUD.dismiss()
            if code == 414{
                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: "Email đã được sử dụng!")
            } else if code == 404 {
                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: "Không tìm thấy user nào cho email của bạn!")
            }
            else{
              Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: error)
            }  
        }
    }
    
    @IBAction func didPressConfirm(_ sender: Any) {
        if !checkMail() { return }
    }  
}
