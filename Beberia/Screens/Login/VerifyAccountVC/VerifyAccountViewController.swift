//
//  VerifyAccountViewController.swift
//  Beberia
//
//  Created by IMAC on 3/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

enum TypeView{
    case forgetPass
    case register
}

class VerifyAccountViewController: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tfNumberVerOne: UITextField!
    @IBOutlet weak var tfNumberVerTwo: UITextField!
    @IBOutlet weak var tfNumberVerThree: UITextField!
    @IBOutlet weak var tfNumberVerFour: UITextField!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblError: UILabel!
    
    var typeView = TypeView.register
    var otp = ""
    var mail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI(){
        customLeftBarButton()
        self.title = R.string.localizable.loginCodeOTP()
        self.lblTitle.text = R.string.localizable.loginTitleCodeOTP()
        
        switch typeView {
        case .forgetPass:
            
            break
        default:
            break
        }
        
        
        tfNumberVerOne.becomeFirstResponder()
        tfNumberVerOne.delegate   = self
        tfNumberVerTwo.delegate   = self
        tfNumberVerThree.delegate = self
        tfNumberVerFour.delegate  = self
    }
    
    func sendOTP(mail: String){
        SVProgressHUD.show()
        APIManager.sendOtp(email: mail, type: typeView, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else  { return }
            SVProgressHUD.dismiss()
            if isSuccess{
                Utils.showAlertOK(controller: self, title: R.string.localizable.commonNoti(), message: "Đã gửi thông báo ")
            }
        }) { (error, code) in
            
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func verifyOtp(otp: String){
        APIManager.verifyOtp(otp: otp, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else  { return }
            SVProgressHUD.dismiss()
            if isSuccess{
                let verifySuccessVC = AccountVerifySuccessVC.init(nib: R.nib.accountVerifySuccessVC)
                verifySuccessVC.modalPresentationStyle = .fullScreen
                verifySuccessVC.typeView = .register
                self.present(verifySuccessVC, animated: true) {
                    let _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
                        timer.invalidate()
                        self.dismiss(animated: true) {
                            print("1212121212")
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                }
            }
        }) { (error, code) in
            SVProgressHUD.dismiss()
            if code == 450 {
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "OTP không đúng!")
            }
        }
    }
    
    func verifyOtpPassForgot(otp: String){
        APIManager.verifyReset(otp: otp, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else  { return }
            SVProgressHUD.dismiss()
            if isSuccess{ 
                let setPassVC = SetPassVC.init(nib: R.nib.setPassVC)
                setPassVC.typeView = .forgetPass
                self.navigationController?.pushViewController(setPassVC, animated: true)
            }
        }) { (error, code) in
            SVProgressHUD.dismiss()
            if code == 450 {
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "OTP không đúng!")
            }
        }
    }
    
    @IBAction func didPressReSend(_ sender: Any) {
        sendOTP(mail: mail)
    }
    
    @IBAction func didPressConfirm(_ sender: Any) {
        
        guard let otpOne = tfNumberVerOne.text, let otpTwo = tfNumberVerTwo.text,let otpThree = tfNumberVerThree.text,let otpFour = tfNumberVerFour.text, otpOne.count > 0, otpTwo.count > 0,otpThree.count > 0,otpFour.count > 0 else {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonError(), message: R.string.localizable.secretError_OTP())
            return
        }
        otp = "\(otpOne)\(otpTwo)\(otpThree)\(otpFour)"
        print(otp)
        
        switch typeView {
        case .forgetPass:
            verifyOtpPassForgot(otp: otp)
            break
            
        default:
            verifyOtp(otp: otp) 
            break
        }
    }
}

extension VerifyAccountViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text?.count)! < 1  && string.count > 0){
            if(textField == tfNumberVerOne)
            {
                tfNumberVerTwo.becomeFirstResponder()
                textField.text = string
            }
            if(textField == tfNumberVerTwo)
            {
                tfNumberVerThree.becomeFirstResponder()
                textField.text = string
            }
            if(textField == tfNumberVerThree)
            {
                tfNumberVerFour.becomeFirstResponder()
                textField.text = string
            }
            if(textField == tfNumberVerFour)
            {
                textField.resignFirstResponder()
                textField.text = string
            }
            
            
            return false
        }
        else if ((textField.text?.count)! >= 1  && string.count == 0){
            // on deleting value from Textfield
            if(textField == tfNumberVerTwo)
            {
                tfNumberVerOne.becomeFirstResponder()
            }
            if(textField == tfNumberVerThree)
            {
                tfNumberVerTwo.becomeFirstResponder()
            }
            if(textField == tfNumberVerFour)
            {
                tfNumberVerThree.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }
        else if ((textField.text?.count)! >= 1  )
        {
            textField.text = string
            textField.becomeFirstResponder()
            return false
        }
        
        
        return true
    }
}
