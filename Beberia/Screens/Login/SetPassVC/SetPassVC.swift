//
//  SetPassVC.swift
//  Beberia
//
//  Created by IMAC on 3/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

class SetPassVC: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var btnShowRePass: UIButton!
    @IBOutlet weak var btnShowPass: UIButton!
    @IBOutlet weak var lblErrorRePassNew: UILabel!
    @IBOutlet weak var imgRePassNew: UIImageView!
    @IBOutlet weak var tfRePassNew: UITextField!
    @IBOutlet weak var lblErrorPassNew: UILabel!
    @IBOutlet weak var imgPassNew: UIImageView!
    @IBOutlet weak var tfPassNew: UITextField!
    
    var typeView = TypeView.forgetPass
    
    var isCheckShowPass = true
    {
        didSet{
            btnShowPass.setImage(isCheckShowPass ? R.image.ic_SelectedView() : R.image.ic_View(), for: .normal)
            tfPassNew.isSecureTextEntry = !isCheckShowPass
        }
    }
    
    var isCheckShowRePass = true
    {
        didSet{
            btnShowRePass.setImage(isCheckShowRePass ? R.image.ic_SelectedView() : R.image.ic_View(), for: .normal)
            tfRePassNew.isSecureTextEntry = !isCheckShowRePass
        }
    }
    
    var isCheckPass = false
    {
        didSet{
            self.lblErrorPassNew.text = isCheckPass ? "" : R.string.localizable.registerCorectLengPass()
            self.imgPassNew.image = isCheckPass ? R.image.ic_Succes() : R.image.ic_Close()
            if !isCheckPass { self.lblErrorPassNew.textColor  = .red }
        }
    }
    
    var isCheckPassDulicate = false
    {
        didSet{
            self.imgRePassNew.image = isCheckPassDulicate ? R.image.ic_Succes() : R.image.ic_Close()
            self.lblErrorRePassNew.text = isCheckPassDulicate ? "" :  R.string.localizable.registerCorrectPass()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customLeftBarButton()
        isCheckShowPass = false
        isCheckShowRePass = false
        tfPassNew.delegate = self
        tfRePassNew.delegate = self
        
        self.title = R.string.localizable.loginSetPass()
        
        tfPassNew.addTarget(self, action: #selector(SetPassVC.fieldPassChanged(textfieldChange:)), for: .editingChanged)
        tfRePassNew.addTarget(self, action: #selector(SetPassVC.textfieldRePassChange(textfieldChange:)), for: .editingChanged)
    }
    
    @objc func fieldPassChanged(textfieldChange: UITextField){
        isCheckPass = false
        tfPassNew.text = textfieldChange.text
        checkPassWord()
        checkPassDulicate()
    }
    
    @objc func textfieldRePassChange(textfieldChange: UITextField){
        isCheckPassDulicate = false
        tfRePassNew.text = textfieldChange.text
        checkPassDulicate()
    }
    
    func checkPassWord(){
        isCheckPass =  tfPassNew.text!.count < 6 || (tfPassNew.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! ? false : true
        
    }
    
    func checkPassDulicate(){
        isCheckPassDulicate = (tfRePassNew.text != tfPassNew.text) ? false : true
    }
    
    func setPassword(){
        APIManager.changePassForgot(password: tfRePassNew.text ?? "", callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else  { return }
            SVProgressHUD.dismiss()
            if isSuccess{
                let verifySuccessVC = AccountVerifySuccessVC.init(nib: R.nib.accountVerifySuccessVC)
                verifySuccessVC.modalPresentationStyle = .fullScreen
                verifySuccessVC.typeView = .forgetPass
                self.present(verifySuccessVC, animated: true) {
                    let _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
                        timer.invalidate()
                        self.dismiss(animated: true) {
                            print("1212121212")
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                }
            }
        }) { (error, code) in
            SVProgressHUD.dismiss()
            // if code == 450 {
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            //  }
        }
    }
    
    @IBAction func didPressShowPassNew(_ sender: UIButton) {
        switch sender.tag {
        case 28:
            isCheckShowPass = !isCheckShowPass
        case 29:
            isCheckShowRePass = !isCheckShowRePass
        default:
            print("!233")
        }
        
    }
    
    @IBAction func didPressConfirm(_ sender: Any) {
        if isCheckPass && isCheckPassDulicate {
            print("21212121")
            setPassword()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = NSCharacterSet.whitespaces
        let range = string.rangeOfCharacter(from: whitespaceSet)
        if let _ = range {
            return false
        }
        else {
            return true
        }
    }
}

