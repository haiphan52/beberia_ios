//
//  LoginSocialViewController.swift
//  Beberia
//
//  Created by IMAC on 9/7/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD
import SwiftKeychainWrapper

class LoginSocialViewController: UIViewController {
    
    enum LoginType: Int, Codable {
        case number = 1, gooogle, apple, facebook
    }
    
    @IBOutlet weak var btnLoginFaceBook: UIButton!
    @IBOutlet weak var btnLoginGoogle: UIButton!
    @IBOutlet weak var btnLoginZalo: UIButton!
    @IBOutlet weak var btnLoginApple: UIButton!
    @IBOutlet weak var btnLoginWithAccount: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btMomHelp: UIButton!
    
    var isCheckLogin = false
    var disposeBag = DisposeBag()
    var mail = ""
    var name = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        btnLoginFaceBook.rx.tap.subscribe { (_) in
            AppSettings.loginType = .facebook
            LoginSocialManager.share.loginFaceBook()
        }.disposed(by: disposeBag)
        
        btnLoginGoogle.rx.tap.subscribe { (_) in
            AppSettings.loginType = .gooogle
            LoginSocialManager.share.loginGoogle()
        }.disposed(by: disposeBag)
        
        btnLoginApple.rx.tap.subscribe { (_) in
            AppSettings.loginType = .apple
            LoginSocialManager.share.loginApple()
        }.disposed(by: disposeBag)
        
        btnLoginZalo.rx.tap.subscribe { (_) in
            LoginSocialManager.share.loginZalo()
        }.disposed(by: disposeBag)
        
        btnLoginWithAccount.rx.tap.subscribe { (_) in
//            let loginVC = R.storyboard.main.loginViewController()
//            self.navigationController?.pushViewController(loginVC!, animated: true)
        }.disposed(by: disposeBag)
        
        btnBack.rx.tap.subscribe { (_) in
            self.dismiss(animated: true, completion: nil)
        }.disposed(by: disposeBag)
        
        LoginSocialManager.share.loginSocialSuccess = { mail ,userId, name in
            print(mail)
            print(userId)
          
            if userId != "0" {
                self.mail = mail
                self.name = name ?? ""
                self.loginSocial(provider_id: userId)
            }else{
                Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Có lỗi khi đăng nhập . Vui lòng thử lại!")
            }
            
        }
        
        if isCheckLogin{
           // customLeftBar()
        }
        
        btnBack.isHidden = !isCheckLogin
        
        self.btMomHelp.rx.tap.bind { _ in
            ManageApp.shared.openLink(link: ConstantApp.shared.linkMomHelp)
        }.disposed(by: self.disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    final func checkLogin(){
        
        let isLoggedIn = KeychainWrapper.standard.bool(forKey: KeychainKeys.isLoggedIn.rawValue) ?? false
        
        if isLoggedIn {
            
            if UserDefault.userIDSocial != ""{
                self.loginSocial(provider_id: UserDefault.userIDSocial)
            } else {
                if KeychainWrapper.standard.string(forKey: KeychainKeys.passWord.rawValue) ?? "" != "" {
                    
                    guard let userName = KeychainWrapper.standard.string(forKey: KeychainKeys.userName.rawValue) else { return }
                    guard let passWord = KeychainWrapper.standard.string(forKey: KeychainKeys.passWord.rawValue) else { return }
                    
                    self.loginWithAccount(userName: userName, pass: passWord)
                }else{
                    guard let tabBarVC = R.storyboard.main.tabBarVC() else {
                        return
                    }
                    tabBarVC.selectedIndex = 1
                }
            }
            
        }else{
            
            guard let tabBarVC = R.storyboard.main.tabBarVC() else {
                return
            }
            tabBarVC.modalPresentationStyle = .fullScreen
            // self.present(tabBarVC, animated: true, completion: nil)
            
            Utils.getTopMostViewController()?.present(tabBarVC, animated: true, completion: {
                if !Utils.checkLogin(selfView: self, showAlert: false) {
                    tabBarVC.selectedIndex = 1
                }
            })
        }
    }
    
    func handelLoginSuccess(userID: Int){
        guard let tabBarVC = R.storyboard.main.tabBarVC() else {
            return
        }
        tabBarVC.modalPresentationStyle = .fullScreen
        self.present(tabBarVC, animated: true, completion: {
             Utils.checkShowPopupVerifyAccount()
        })
        
        
//        SendBirdManager.share.connectUserToSendBird(completion: {})
        SocketIOManager.connectSocket(userID: userID)
        
        let defaultsAppGroup = UserDefaults.init(suiteName: UserDefault.KeyEntitlement)
        defaultsAppGroup?.set(UserInfo.shareUserInfo.id, forKey: UserDefault.userID)
        
        KeychainWrapper.standard.set(true, forKey: KeychainKeys.isLoggedIn.rawValue)
        Utils.getAppDelegate().badgeCount = UserInfo.shareUserInfo.unWatch
        Utils.getAppDelegate().isCheckLogin.accept(true)
    }

}

extension LoginSocialViewController{
     private final func loginSocial(provider_id:String) {
        SVProgressHUD.show()
        APIManager.loginSocial(provider_id: provider_id,
                               fcm_token: Utils.getAppDelegate().tokennv,
                               providerType: AppSettings.loginType.rawValue,
                               callbackSuccess: { [weak self] (isSuccess, user) in
            guard let self = self else { return }
            
            UserDefault.userIDSocial = provider_id
            self.handelLoginSuccess(userID: user.id ?? 0)
            
            SVProgressHUD.dismiss()
        }) { (error,statusCode, user )  in
            Utils.getAppDelegate().isCheckLogin.accept(false)
            if statusCode == 404 {
                let postVC = RegisterFBViewController.init(nib: R.nib.registerFBViewController)
                postVC.hidesBottomBarWhenPushed = true
                postVC.getprovider = "\(provider_id)"
                postVC.getemail = self.mail
                postVC.name = self.name
                
                self.navigationController?.pushViewController(postVC, animated: true)
            }
            if statusCode == 412 {
                let infoLogin = InfoLoginViewController.init(nib: R.nib.infoLoginViewController)
                self.navigationController?.pushViewController(infoLogin, animated: true)
            }
            
            if statusCode == 416 {
                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.loginAccountBan())
            }
            
//            if statusCode == 0 {
//                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: error)
//            }
            
            SVProgressHUD.dismiss()
        }
    }
    
    private final func loginWithAccount(userName: String, pass: String){
        SVProgressHUD.show()
        APIManager.login(user: userName, providerType: AppSettings.loginType.rawValue, pass: pass, fcmToken: "", callbackSuccess: { [weak self] (user) in
            guard let self = self else { return }
            self.handelLoginSuccess(userID: user.id ?? 0)
            KeychainWrapper.standard.set(userName, forKey: KeychainKeys.userName.rawValue)
            KeychainWrapper.standard.set(pass, forKey: KeychainKeys.passWord.rawValue)
            SVProgressHUD.dismiss()
            
        }) { (error, statusCode,display_name,password ) in
            print(error)
            Utils.getAppDelegate().isCheckLogin.accept(false)
            SVProgressHUD.dismiss()
            switch statusCode{
                
            case 412:
                let infoLogin = InfoLoginViewController.init(nib: R.nib.infoLoginViewController)
                self.navigationController?.pushViewController(infoLogin, animated: true)
                Utils.getAppDelegate().username = userName
                Utils.getAppDelegate().password = pass
                
            case 416:
                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.loginAccountBan())
                
            case 404:
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: R.string.localizable.loginPassWrongAndUserWrong())
         
            default :
                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: error)
            }
        }
    }
}
