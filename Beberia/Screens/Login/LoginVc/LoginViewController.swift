//
//  LoginViewController.swift
//  Beberia
//
//  Created by iMAC on 8/24/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import SwiftKeychainWrapper
import AuthenticationServices

class LoginViewController: UIViewController {
    
    //MARK: - Outlets

    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var ViewUsername: UIView!
    @IBOutlet weak var txtPassWord: UITextField!
    @IBOutlet weak var ViewPassWord: UIView!
    @IBOutlet weak var imgUserName: UIImageView!
    @IBOutlet weak var btnShowPass: UIButton!
    @IBOutlet weak var lblPasswordCR: UILabel!
    @IBOutlet weak var lblUserNameCR: UILabel!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRMPass: UIButton!
    @IBOutlet weak var imgRMPass: UIImageView!
    
    //MARK: - Properties
    var checkShowPass = false
    var checkdk = true
    var isCheckLogin = false
    var email = ""
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        customLeftBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    //MARK: - Setup UI
    func customLeftBar(){
        let leftBarButtonItem = UIBarButtonItem.menuButton(self, action: #selector(self.backVC), imageName: R.image.ic_Back() ?? UIImage.init(), height: 30, width: 40, view: self.view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc func backVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupUI() {
        self.title = R.string.localizable.loginTitle()
        btnLogin.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 20)
        txtUserName.delegate = self
        txtUserName.tag = 1
        txtPassWord.tag = 2
        txtPassWord.delegate = self
        
        imgRMPass.image = Utils.getRememberPass() ? R.image.ic_CheckBox() : R.image.ic_Box()
    }
    
    //MARK: Action
    @IBAction func btnRMPass(_ sender: Any) {
            imgRMPass.image = checkdk ? R.image.ic_CheckBox() : R.image.ic_Box()
            checkdk = !checkdk
            Utils.saveRememberPass(isSave: checkdk)
    }
    
    @IBAction func btnForgotPass(_ sender: Any) {
        let accountVerifyVC = ForgetPassVC.init(nib: R.nib.forgetPassVC)
        self.navigationController?.pushViewController(accountVerifyVC, animated: true)
    }

    @IBAction func didPressBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        txtUserName.text = ""
        txtPassWord.text = ""
        let postVC = RegisterViewController.init(nib: R.nib.registerViewController)
        postVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(postVC, animated: true)
    }
    
    @IBAction func btnShowPass(_ sender: UIButton) {
        checkShowPass = !checkShowPass
        sender.setImage(checkShowPass ?R.image.ic_SelectedView() : R.image.ic_View(), for: .normal)
        
        txtPassWord.isSecureTextEntry = !checkShowPass
    }
    
    
    @IBAction func btnLogin(_ sender: Any) {
        Login (userName: txtUserName.text!, pass: txtPassWord.text!)
    }
    
    //MARK: Request API
    func Login (userName:String , pass:String){
        SVProgressHUD.show()
        self.ViewUsername.backgroundColor = .lightGray
        self.imgUserName.image = UIImage(named: "")
        
        self.ViewPassWord.backgroundColor = .lightGray
        self.imgPassword.image = UIImage(named: "")
        
        self.lblPasswordCR.text = ""
        self.ViewPassWord.backgroundColor = .lightGray
        self.lblUserNameCR.text = ""
        self.ViewUsername.backgroundColor = .lightGray

        AppSettings.loginType = .number
        APIManager.login(user: userName, providerType: AppSettings.loginType.rawValue, pass: pass,fcmToken: Utils.getAppDelegate().tokennv, callbackSuccess: { [weak self] (user) in
            guard let self = self else { return }
            let defaults = UserDefaults.standard
            defaults.set(true, forKey: "islogin")
            
            Utils.getAppDelegate().badgeCount = UserInfo.shareUserInfo.unWatch
            
            KeychainWrapper.standard.set(userName, forKey: KeychainKeys.userName.rawValue)
            KeychainWrapper.standard.set(pass, forKey: KeychainKeys.passWord.rawValue)
            let defaultsAppGroup = UserDefaults.init(suiteName: UserDefault.KeyEntitlement)
            defaultsAppGroup?.set(UserInfo.shareUserInfo.id, forKey: UserDefault.userID)
            
            self.imgPassword.image = R.image.ic_Succes()//UIImage(named: "checked")
            self.imgUserName.image = R.image.ic_Succes()//UIImage(named: "checked")
            self.lblPasswordCR.text = ""
            self.lblUserNameCR.text = ""
            
            self.moveToTabbar(user: user)
            
        }) { (error, statusCode,display_name,password ) in
            Utils.getAppDelegate().isCheckLogin.accept(false)
            switch statusCode{
            case 414:
                if display_name == "" {
                    self.lblPasswordCR.text = R.string.localizable.loginRequirePass()
                    self.ViewPassWord.backgroundColor = .red
                    self.imgPassword.image = R.image.ic_Close()//UIImage(named: "close-1")
                }
                else if password == "" {
                    self.lblUserNameCR.text = R.string.localizable.loginRequireUserName()
                    self.ViewUsername.backgroundColor = .red
                    self.imgUserName.image = R.image.ic_Close()//UIImage(named: "close-1")
                }
                else {
                    self.lblPasswordCR.text = R.string.localizable.loginRequirePass()
                    self.ViewPassWord.backgroundColor = .red
                    self.imgPassword.image = R.image.ic_Close()//UIImage(named: "close-1")
                    self.lblUserNameCR.text = R.string.localizable.loginRequireUserName()
                    self.ViewUsername.backgroundColor = .red
                    self.imgUserName.image = R.image.ic_Close()//UIImage(named: "close-1")
                }
                
            case 413:
                self.lblPasswordCR.text = R.string.localizable.registerErrorPass()
                self.ViewPassWord.backgroundColor = .red
                self.imgPassword.image = R.image.ic_Close()//UIImage(named: "close-1")
                
            case 412:
                let infoLogin = InfoLoginViewController.init(nib: R.nib.infoLoginViewController)
                self.navigationController?.pushViewController(infoLogin, animated: true)
                Utils.getAppDelegate().username = userName
                Utils.getAppDelegate().password = pass
                
            case 416:
                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: R.string.localizable.loginAccountBan())
                
            case 404:
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: R.string.localizable.loginPassWrongAndUserWrong())
                
            default :  
                Utils.showAlertView(controller: self, title: R.string.localizable.commonNoti(), message: error)
            }
            
            SVProgressHUD.dismiss()
        }
    }
    
    private func moveToTabbar(user: User) {
        guard let tabBarVC = R.storyboard.main.tabBarVC() else {
            return
        }
        tabBarVC.modalPresentationStyle = .fullScreen
        self.present(tabBarVC, animated: true, completion: {
            Utils.checkShowPopupVerifyAccount()
        })

//        SendBirdManager.share.connectUserToSendBird(completion: {})
        SocketIOManager.connectSocket(userID: user.id ?? 0)

        self.imgPassword.image = UIImage(named: "")
        self.imgUserName.image = UIImage(named: "")
        if !Utils.getRememberPass(){
            self.txtUserName.text = ""
            self.txtPassWord.text = ""
        }
        KeychainWrapper.standard.set(true, forKey: KeychainKeys.isLoggedIn.rawValue)
        Utils.getAppDelegate().isCheckLogin.accept(true)
        SVProgressHUD.dismiss()
    }
}



//MARK: Textfield Delegate
extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField.tag {
        case 1:
            let whitespaceSet = NSCharacterSet.whitespaces
            let range = string.rangeOfCharacter(from: whitespaceSet)
            if let _ = range {
                return false
            }
            else {
                return true
            }
        case 2:
            self.imgPassword.image = UIImage(named: "")
            self.lblPasswordCR.text = ""
            self.ViewPassWord.backgroundColor = .lightGray
        default:
            break
        }
        return true
    }
    
}
extension LoginViewController: ReferralDelegate {
    func dismiss(user: User?) {
        guard let user = user else {
            return
        }
        self.moveToTabbar(user: user)
    }
}
