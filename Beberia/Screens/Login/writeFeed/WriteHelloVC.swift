//
//  WriteHelloVC.swift
//  Beberia
//
//  Created by OS on 2/11/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

class WriteHelloVC: BaseViewController {
    
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var txtContent: RichTextEditor!
    @IBOutlet weak var helloLable: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    
    var ageRange = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
        
        let attrString = NSMutableAttributedString(string: R.string.localizable.loginHelloMom(), attributes: nil)
        
        attrString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "fb7ba5", alpha: 1.0) as Any,
                                  NSAttributedString.Key.font :  UIFont.init(name: AppFont.HelveticaNeueItalic, size: 16)!],
                                 range: NSRange(location: 0, length: 18))
        
        attrString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "1ab9ff", alpha: 1.0) as Any,
                                  NSAttributedString.Key.font :  UIFont.init(name: AppFont.HelveticaNeueItalic, size: 17)!],
                                 range: NSRange(location: 19, length: 2))
        attrString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "ff99bb", alpha: 1.0) as Any,
                                  NSAttributedString.Key.font :  UIFont.init(name: AppFont.HelveticaNeueItalic, size: 17)!],
                                 range: NSRange(location: 21, length: 2))
        attrString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "ebb41d", alpha: 1.0) as Any,
                                  NSAttributedString.Key.font :  UIFont.init(name: AppFont.HelveticaNeueItalic, size: 17)!],
                                 range: NSRange(location: 23, length: 3))
        attrString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "fb7ba5", alpha: 1.0) as Any,
                                  NSAttributedString.Key.font :  UIFont.init(name: AppFont.HelveticaNeueItalic, size: 16)!],
                                 range: NSRange(location: 27, length: 269))
        attrString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "1ab9ff", alpha: 1.0) as Any,
                                  NSAttributedString.Key.font :  UIFont.init(name: AppFont.HelveticaNeueItalic, size: 17)!],
                                 range: NSRange(location: 296, length: 2))
        attrString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "ff99bb", alpha: 1.0) as Any,
                                  NSAttributedString.Key.font :  UIFont.init(name: AppFont.HelveticaNeueItalic, size: 17)!],
                                 range: NSRange(location: 298, length: 2))
        attrString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "ebb41d", alpha: 1.0) as Any,
                                  NSAttributedString.Key.font :  UIFont.init(name: AppFont.HelveticaNeueItalic, size: 17)!],
                                 range: NSRange(location: 300, length: 3))
        attrString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "fb7ba5", alpha: 1.0) as Any,
                                  NSAttributedString.Key.font :  UIFont.init(name: AppFont.HelveticaNeueItalic, size: 17)!],
                                 range: NSRange(location: 304, length: 1))
        
        helloLable.attributedText = attrString
        
        txtContent.text = R.string.localizable.loginPlaceholderMom()
        txtContent.textColor = UIColor.lightGray
        txtContent.delegate = self
        errorLabel.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utils.getAppDelegate().loginSocial(provider_id: UserDefault.userIDSocial, isUpdateLogin: false)
    }
    
    func setupUI(){
        // customLeftBarButton()
        
    }
    
    @IBAction func btnOk(_ sender: Any) {
        
        if txtContent.textColor == UIColor.black {
            SVProgressHUD.show()
            let title = "Xin chào các mom"
            let textHTML = NSMutableAttributedString.init(attributedString: txtContent.attributedText).generateHtmlString()
            APIManager.createHomeFeed(title: title, content: textHTML, images: "[]", videos: "[]", city_code: UserInfo.shareUserInfo.city_name.locationCode ?? 0, about: 2, category: 2, content_search: txtContent.text! + title, ageRange: ageRange, district_code: UserInfo.shareUserInfo.district.districtCode ?? "", subTitle: 0, callbackSuccess: { [weak self] (homeFeed) in
                guard let self = self else { return }
                
                self.moveToTabbar()
            }) { (error) in
                SVProgressHUD.dismiss()
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            }
        } else {
            self.moveToTabbar()
        }
    }
    
    private func detectMoveToReferral() {
        if UserInfo.shareUserInfo.new_device == 0 {
            let vc = ReferralVC(nibName: "ReferralVC", bundle: nil)
            vc.modalPresentationStyle = .overFullScreen
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        } else {
            self.moveToTabbar()
        }
    }
    
    private func moveToTabbar() {
        guard let tabBarVC = R.storyboard.main.tabBarVC() else {
            return
        }
      
        tabBarVC.modalPresentationStyle = .fullScreen
        self.present(tabBarVC, animated: true, completion: nil)
        Utils.getAppDelegate().isCheckLogin.accept(true)
    }
}

extension WriteHelloVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = R.string.localizable.loginPlaceholderMom()
            textView.textColor = UIColor.lightGray
        }
    }
}
extension WriteHelloVC: ReferralDelegate {
    func dismiss(user: User?) {
        self.moveToTabbar()
    }
}
