//
//  RegisterFBViewController.swift
//  Beberia
//
//  Created by iMAC on 8/26/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import RxSwift
import RxCocoa
import SwiftKeychainWrapper

class RegisterFBViewController: BaseViewController {
    
    //MARK: Outlets
    @IBOutlet weak var txtDisplayName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgUserName: UIImageView!
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var imgPhone: UIImageView!
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var districtTextField: UITextField!
    @IBOutlet weak var errorDictrisLable: UILabel!
    @IBOutlet weak var imgDictrict: UIImageView!
 //   @IBOutlet weak var emailTf: UITextField!
 //   @IBOutlet weak var lblErrorMail: UILabel!
    
    @IBOutlet weak var btn1824Age: UIButton!
     @IBOutlet weak var btn2529Age: UIButton!
     @IBOutlet weak var btn3034Age: UIButton!
     @IBOutlet weak var btn3540Age: UIButton!
    
    private let registerNewView: RegisterNewUI = RegisterNewUI.loadXib()
    
    //MARK: Properties
    var getprovider = ""
    var checked = true
    var isCheckName = false
    var isCheckMail = false
    var city_code = 1
    var district_code = "1_001"
    var getemail = ""
    var ageRange = 2
    var name: String = ""
    private var previousName: String = ""
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.setupRX()
    }
    
    override func viewWillLayoutSubviews() {
        btnNext.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 20)
        btnCheck.borderWidth = 1
        btnCheck.borderColor = .lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
        self.setupNavigationVer2()
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.ffc440Color()!,
                                 textColor: .white,
                                 isTranslucent: true)
        self.removeBorder(font: UIFont.notoSansFont(weight: .bold, size: 18),
                          bgColor: R.color.ffc440Color()!,
                          textColor: .white)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: Setup UI
    func setupUI (){
        self.title = "Đăng ký tài khoản"
        txtDisplayName.delegate = self
        txtPhone.keyboardType = .numberPad
        customLeftBarButton()
        if !name.isEmpty {
            txtDisplayName.text = name
        }
        
      //  emailTf.delegate = self
        isCheckMail = false
        setColorButton(buttons: [btn1824Age,btn2529Age,btn3034Age,btn3540Age])
        
        self.registerNewView.addImageBg(view: self.view)
        self.view.addSubview(self.registerNewView)
        self.registerNewView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.registerNewView.delegate = self
      
        if !name.isEmpty {
          self.registerNewView.updateValue(name: self.name)
        }
        
    }
    
    func setupUIInit(){
        if /*isCheckPhone &&*/ isCheckName {
            self.lblPhone.text = ""
            self.viewPhone.backgroundColor = .lightGray
            self.isCheckName = false
            //            self.isCheckPhone = false
            //            self.imgPhone.image = R.image.ic_Succes()
//            self.registerSocial()
            //            self.loginWithPhone()
        }
    }
    
    private func setupRX() {
        self.txtDisplayName.rx.text.orEmpty.asObservable()
            .debounce(.milliseconds(200), scheduler: MainScheduler.asyncInstance)
            .bind { [weak self] value in
                guard let wSelf = self, wSelf.previousName != value
                else { return }
                wSelf.previousName = value
                wSelf.validateName()
        }.disposed(by: disposeBag)
    }
    
    func setColorButton(buttons: [UIButton]){
          for button in buttons {
              button.setTitleColor(UIColor.init(hexString: AppColor.colorSelect,alpha: 0.3), for: .normal)
              button.backgroundColor =  UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.4)
              button.borderColor = UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.5)
              button.borderWidth = 1
              button.cornerRadius = 15
          }
      }
    
    @IBAction func didPressAge(_ sender: UIButton) {
        setColorButton(buttons: [btn1824Age,btn2529Age,btn3034Age,btn3540Age])
        
        sender.setTitleColor(UIColor.init(hexString: AppColor.colorYellow,alpha: 0.9), for: .normal)
        sender.backgroundColor =  UIColor.init(hexString: AppColor.colorBGMenu,alpha: 0.4)
        sender.borderColor = UIColor.init(hexString: AppColor.colorYellow,alpha: 0.9)
        
        ageRange = sender.tag
    }
    
    //MARK: ACTION
    @IBAction func btnCity(_ sender: Any) {
        let locationVC = LocationViewController.init(nib: R.nib.locationViewController)
        locationVC.hidesBottomBarWhenPushed = true
        
        locationVC.didSelectedLocation = { city in
            switch city.locationCode {
            case 1:
                self.txtPhone.text = city.locationNameVi ?? ""
                self.city_code = city.locationCode ?? 0
            case 79:
                self.txtPhone.text = R.string.localizable.commonTPHCMShort()
                self.city_code = city.locationCode ?? 0
            default:
                self.txtPhone.text = city.locationNameVi ?? ""
                self.city_code = city.locationCode ?? 0
                break
            }
            
            // reset distric
            self.district_code = ""
            self.districtTextField.text = ""
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: true)
    }
    
   
    @IBAction func didPressDictrict(_ sender: Any) {
        let locationVC = DistrictViewController.init(nib: R.nib.districtViewController)
        locationVC.locationID = self.city_code
        locationVC.hidesBottomBarWhenPushed = true
        locationVC.didSelectedLocation = { city in
            self.districtTextField.text = city.districtName ?? ""
            self.district_code = city.districtCode ?? ""
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    @IBAction func btnCheck(_ sender: UIButton) {
        if checked {
            sender.setImage(R.image.ic_Succes(), for: .normal)
            btnCheck.backgroundColor = UIColor(red: 255/255, green: 241/255, blue: 151/255, alpha: 1)
            btnCheck.borderWidth = 0
            checked = false
        } else {
            sender.setImage(UIImage(named:""), for: .normal)
            btnCheck.backgroundColor = .white
            btnCheck.borderWidth = 1
            btnCheck.borderColor = .lightGray
            checked = true
        }
    }
    
    @IBAction func btnNext(_ sender: Any) {
        validateField()
    }
    
    @IBAction func btnPrivacy(_ sender: Any) {
        let privacy = PrivacyViewController.init(nib: R.nib.privacyViewController)
        self.navigationController?.pushViewController(privacy, animated: true)
    }
    

    
    //MARK: Request API
//    func checkEmail(completion: @escaping (Bool)->()){
//        //        if validatePhone(){
//        SVProgressHUD.show()
//        APIManager.checkEmail(email: emailTf.text!, callbackSuccess: { [weak self] (isSuccess) in
//            guard let self = self else { return }
//            self.imgPhone.image = R.image.ic_Succes()//UIImage(named: "checked")
//            self.lblErrorMail.text = ""
//            self.viewPhone.backgroundColor = .lightGray
//            SVProgressHUD.dismiss()
//           // self.isCheckMail = true
//            completion(true)
//          //  self.validateField()
//            //                self.setupUIInit()
//
//        }) { (error, statusCode) in
//            self.imgPhone.image = R.image.ic_Close()//UIImage(named: "close-1")
//            self.lblErrorMail.text = "Email đã tồn tại!"
//            self.viewPhone.backgroundColor = .red
//         //   self.isCheckMail = false
//            completion(false)
//            SVProgressHUD.dismiss()
//        }
//        //        }
    
    func validateName() {
        guard let userName = txtDisplayName.text?.trimmingCharacters(in: .whitespacesAndNewlines) , userName.count > 0 else {
            self.lblUserName.text = R.string.localizable.registerCorectRequirePass()
            self.lblUserName.textColor = .red
            self.viewUserName.backgroundColor = .red
            self.imgUserName.image = R.image.ic_Close()//UIImage(named: "close-1")
            return
        }
        
        if userName.containsSpecialCharacter == true {
                   self.lblUserName.text = "Tài khoản không được chứa ký tự đặc biệt"
                   self.lblUserName.textColor = .red
                   self.viewUserName.backgroundColor = .red
                   self.imgUserName.image = R.image.ic_Close()
                   return
        }
        
      //  if !(txtDisplayName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            SVProgressHUD.show()
            APIManager.checkName(display_name: userName, callbackSuccess: { [weak self] (isSuccess) in
                guard let self = self else { return }
                self.imgUserName.image = R.image.ic_Succes()//UIImage(named: "checked")
                self.lblUserName.text = ""
                self.viewUserName.backgroundColor = .lightGray
                self.isCheckName = true
                SVProgressHUD.dismiss()
            }) { (error, statusCode) in
                if statusCode == 414 {
                    self.lblUserName.text = R.string.localizable.registerCheckName()
                }
                self.imgUserName.image = R.image.ic_Close()//UIImage(named: "close-1")
                self.viewUserName.backgroundColor = .red
                self.isCheckName = false
                SVProgressHUD.dismiss()
            }
     //   }else {
            // agjad
      //  }
    }
    func checkName() {
        guard let userName = txtDisplayName.text?.trimmingCharacters(in: .whitespacesAndNewlines) , userName.count > 0 else {
            self.lblUserName.text = R.string.localizable.registerCorectRequirePass()
            self.lblUserName.textColor = .red
            self.viewUserName.backgroundColor = .red
            self.imgUserName.image = R.image.ic_Close()//UIImage(named: "close-1")
            return
        }
        
        if userName.containsSpecialCharacter == true {
                   self.lblUserName.text = "Tài khoản không được chứa ký tự đặc biệt"
                   self.lblUserName.textColor = .red
                   self.viewUserName.backgroundColor = .red
                   self.imgUserName.image = R.image.ic_Close()
                   return
        }
        
      //  if !(txtDisplayName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            SVProgressHUD.show()
            APIManager.checkName(display_name: userName, callbackSuccess: { [weak self] (isSuccess) in
                guard let self = self else { return }
                self.imgUserName.image = R.image.ic_Succes()//UIImage(named: "checked")
                self.lblUserName.text = ""
                self.viewUserName.backgroundColor = .lightGray
                self.isCheckName = true
                SVProgressHUD.dismiss()
                self.validateField()
                //  self.setupUIInit()
            }) { (error, statusCode) in
                if statusCode == 414 {
                    self.lblUserName.text = R.string.localizable.registerCheckName()
                }
                self.imgUserName.image = R.image.ic_Close()//UIImage(named: "close-1")
                self.viewUserName.backgroundColor = .red
                self.isCheckName = false
                SVProgressHUD.dismiss()
            }
     //   }else {
            // agjad
      //  }
    }
    
//    func registerSocial(){
//        SVProgressHUD.show()
//        APIManager.registerSocial(ageRange: ageRange ,email: getemail, display_name: txtDisplayName.text!, provider_id: getprovider, city_code: city_code, fcmToken: Utils.getAppDelegate().tokennv,district_code: district_code, callbackSuccess: { [weak self] (isSuccess) in
//
//                guard let self = self else { return }
//               let postVC = InfoChildViewController.init(nib: R.nib.infoChildViewController)
//               postVC.hidesBottomBarWhenPushed = true
//               postVC.provider_id = self.getprovider
//               postVC.getlocationID = self.city_code
//               postVC.getDistrictID =  self.district_code
//               self.navigationController?.pushViewController(postVC, animated: true)
//
//               SVProgressHUD.dismiss()
//           }) { (error,provider_id,phone,display_name,statusCode) in
//               self.lblUserName.text = ""
//               self.viewUserName.backgroundColor = .lightGray
//               self.imgUserName.image = UIImage(named: "")
//               self.lblPhone.text = ""
//               self.imgPhone.image = UIImage(named: "")
//               self.viewPhone.backgroundColor = .lightGray
//               if statusCode == 414 {
//                   if display_name != "" || phone != ""{
//                       if display_name != "" &&  phone != ""{
//                           self.lblUserName.text = display_name
//                           self.viewUserName.backgroundColor = .red
//                           self.imgUserName.image = R.image.ic_Close()
//                         //  self.lblPhone.text = phone
//                         //  self.imgPhone.image = R.image.ic_Close()
//                        //   self.viewPhone.backgroundColor = .red
//                       }
//                       else if phone != "" && display_name == "" {
//                        //   self.lblPhone.text = phone
//                        //   self.imgPhone.image = R.image.ic_Close()
//                       //    self.viewPhone.backgroundColor = .red
//                        Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: phone)
//                       }
//                       else {
//                           self.lblUserName.text = display_name
//                           self.viewUserName.backgroundColor = .red
//                           self.imgUserName.image = R.image.ic_Close()
//                       }
//                   }
//                   else {
//                       Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: provider_id)
//                   }
//               }
//               SVProgressHUD.dismiss()
//           }
//       }
    
    //MARK: Validate
    func validateField(){
       
        
        
    if !self.isCheckName{
        checkName()
        return
    }
    
//    if city_code == 0{
//        lblPhone.text = "Vui lòng chọn Thành phố!"
//        return
//    }
//    lblPhone.text = ""
//    
//    if district_code == ""{
//        errorDictrisLable.text = "Vui lòng chọn quận!"
//        return
//    }
//    errorDictrisLable.text = ""
        
//    if ageRange == 0 {
//        Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: "Mời bạn chọn độ tuổi")
//        return
//    }
        
        
    self.setupUIInit()
    
//        if (emailTf.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
//        lblErrorMail.text = "Email không được để trống!"
//        return
//    }
//    lblErrorMail.text = ""
//
//    if !emailTf.text!.isValidEmail(){
//        lblErrorMail.text = "Email sai định dạng!"
//        return
//    }
//    lblErrorMail.text = ""
//
//    if checked == true {
//        Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: R.string.localizable.registerErrorMess())
//        return
//    }
//
//        checkEmail { (isCheck) in
//            if isCheck{
//                self.setupUIInit()
//            }else{
//                self.lblErrorMail.text = "Email đã tồn tại!"
//            }
//        }
    }
  
  func checkNameRegisterNew() {
    guard let userName = self.registerNewView.txtDisplayName.text?.trimmingCharacters(in: .whitespacesAndNewlines),
          userName.count > 0 else {
      self.registerNewView.updateUserName(type: .lessZero)
      return
    }
    
    if userName.containsSpecialCharacter == true {
      self.registerNewView.updateUserName(type: .containerSpecial)
      return
    }
    
    //  if !(txtDisplayName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
    SVProgressHUD.show()
    APIManager.checkName(display_name: userName, callbackSuccess: { [weak self] (isSuccess) in
      guard let self = self else { return }
      self.registerNewView.updateUserName(type: .success)
      self.isCheckName = true
      SVProgressHUD.dismiss()
      self.validateField()
      //  self.setupUIInit()
    }) { (error, statusCode) in
        self.registerNewView.updateUserName(type: .fail)
      SVProgressHUD.dismiss()
    }
    //   }else {
    // agjad
    //  }
  }
  
  private func moveToSelectLocation() {
    let locationVC = LocationViewController.init(nib: R.nib.locationViewController)
    locationVC.hidesBottomBarWhenPushed = true
    
    locationVC.didSelectedLocation = { [weak self] city in
      guard let self = self else {
        return
      }
      self.registerNewView.updateCity(city: city)
    }
    Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: true)
  }
    
    func registerSocial(ageRange: Int,
                        displayName: String,
                        providerId: String,
                        cityCode: Int,
                        phone: String,
                        birthdayBaby: Int,
                        birthdayPreg: Int?) {
        SVProgressHUD.show()
        APIManager.registerSocial(phone: phone,
                                  ageRange: ageRange,
                                  birthdayBaby: birthdayBaby,
                                  birthdayPreg: birthdayPreg,
                                  display_name: displayName,
                                  provider_id: providerId,
                                  city_code: cityCode,
                                  fcmToken: Utils.getAppDelegate().tokennv,
                                  callbackSuccess: { [weak self] (isSuccess) in

                guard let self = self else { return }
           
            let postVC = WriteHelloVC.init(nib: R.nib.writeHelloVC)
            postVC.ageRange = ageRange
            UserDefault.userIDSocial = providerId
            KeychainWrapper.standard.set(true, forKey: KeychainKeys.isLoggedIn.rawValue)
            KeychainWrapper.standard.set(Utils.getAppDelegate().username, forKey: KeychainKeys.userName.rawValue)
            KeychainWrapper.standard.set(Utils.getAppDelegate().password, forKey: KeychainKeys.passWord.rawValue)
            Utils.getAppDelegate().username = ""
            Utils.getAppDelegate().password = ""
            postVC.modalPresentationStyle = .fullScreen
            self.present(postVC, animated: true, completion: nil)
               SVProgressHUD.dismiss()
           }) { (error,provider_id,phone,display_name,statusCode) in
               if statusCode == 414 {
                   if display_name != "" || phone != ""{
                       if display_name != "" &&  phone != ""{
                           self.registerNewView.setValueAPIFail(displayName: displayName)
                       }
                       else if phone != "" && display_name == "" {
                         self.registerNewView.statusPhone(typePhone: .used, text: phone)
                        Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: phone)
                       }
                       else {
                           self.registerNewView.setValueAPIFail(displayName: displayName)
                       }
                   }
                   else {
                       Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: provider_id)
                   }
               }
               SVProgressHUD.dismiss()
           }
       }
    
}
extension RegisterFBViewController: RegisterNewUIDelegate {
  func registerSocialNew(ageRange: Int, displayName: String, cityCode: Int, phone: String, birthdayBaby: Int, birthdayPreg: Int?) {
      self.registerSocial(ageRange: ageRange,
                          displayName: displayName,
                          providerId: self.getprovider,
                          cityCode: cityCode,
                          phone: phone,
                          birthdayBaby: birthdayBaby,
                          birthdayPreg: birthdayPreg)
  }
    
  func moveToLocation() {
    self.moveToSelectLocation()
  }
  
  func validateNameNewUI() {
    self.checkNameRegisterNew()
  }
  
}

//MARK: UITextField Delegate
extension RegisterFBViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
           switch textField.tag {
           case 9:
           //    isCheckMail = emailTf.text!.isValidEmail()
           //    if isCheckMail { checkEmail() }
               break
            
           default: break
           }
       }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let whitespaceSet = NSCharacterSet.whitespaces

        let range = string.rangeOfCharacter(from: whitespaceSet)
        if let _ = range {
            return false
        }
        else {
            return true
        }
    }
    
}
