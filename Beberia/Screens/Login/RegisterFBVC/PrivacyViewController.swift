//
//  PrivacyViewController.swift
//  Beberia
//
//  Created by OS on 11/22/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class PrivacyViewController: BaseViewController {

    @IBOutlet weak var txtPrivacy: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        customLeftBarButton()
        let string = txtPrivacy.text!
        let font = UIFont(name: AppFont.HelveticaNeue, size: 16)
        let boldFont = UIFont(name: AppFont.HelveticaNeueBold, size: 16)
        txtPrivacy.attributedText = string.withBoldText(
            boldPartsOfString: ["1. Truy cập và sử dụng Dịch vụ", "2. Đăng ký thành viên","3. Quyền lợi bảo mật thông tin của khách hàng","4.Nghĩa vụ cho các thành viên","5. Các liên kết", "6. Bảo mật thông tin cá nhân"], font: font, boldFont: boldFont)
        // Do any additional setup after loading the view.
    }

}

