//
//  RegisterNewUI.swift
//  Beberia
//
//  Created by haiphan on 25/07/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

protocol RegisterNewUIDelegate: AnyObject {
    func validateNameNewUI()
    func moveToLocation()
    func registerSocialNew(ageRange: Int,
                           displayName: String,
                           cityCode: Int,
                           phone: String,
                           birthdayBaby: Int,
                           birthdayPreg: Int?)
}

class RegisterNewUI: UIView {
    
    enum UserNameType {
        case lessZero, containerSpecial, success, fail, lessSix
    }
    
    enum PhoneType {
        case zero, invalid, valid, used
        
        static func getStt(count: Int, isValid: Bool) -> Self {
            if count <= 0 {
                return .zero
            }
            
            if isValid {
                return .valid
            }
            
            return .invalid
        }
        
    }
    
    enum SelectMomAge: Int, CaseIterable {
        case one, two, three, four
        
        var value: Int {
            return self.rawValue + 1
        }
    }
    
    struct Constant {
        static let alpha: Float = 1
        static let offsetX: CGFloat = 0
        static let offsetY: CGFloat = 2
        static let blur: CGFloat = 8
        static let spread: CGFloat = 0
        static let limitName: Int = 6
    }
    
    var delegate: RegisterNewUIDelegate?
    
    @IBOutlet weak var txtDisplayName: UITextField!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserName: UIImageView!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet var inforViews: [UIView]!
    @IBOutlet weak var displayNameView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var lbPhoneError: UILabel!
    @IBOutlet weak var imgPhone: UIImageView!
    @IBOutlet weak var btSelectCity: UIButton!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet var btAgesMom: [UIButton]!
    @IBOutlet weak var btCheck: UIButton!
    @IBOutlet weak var imgChecked: UIImageView!
    @IBOutlet weak var btCompleted: UIButton!
    @IBOutlet weak var birthDayBaby: UITextField!
    @IBOutlet weak var birthDayPreg: UITextField!
    @IBOutlet weak var cityErrorView: UIView!
    private let dateFormat: DateFormatter = DateFormatter()
    private let datePicker: UIDatePicker = UIDatePicker()
    var cityCode: Int = 0
    private var previousName: String = ""
    var ageRange = 2
    private var isName: Bool = false
    private let disposeBag = DisposeBag()
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
}
extension RegisterNewUI {
    
    private func setupUI() {
        inforViews.forEach { v in
            v.layer.applySketchShadow(color: R.color.shadow()!,
                                      alpha: Constant.alpha,
                                      x: Constant.offsetX,
                                      y: Constant.offsetY,
                                      blur: Constant.blur,
                                      spread: Constant.spread)
        }
        self.txtDisplayName.delegate = self
        self.detectConfirm()
        self.setupDatePicker()
    }
    
    private func setupRX() {
        self.txtDisplayName.rx.text.orEmpty.asObservable()
            .debounce(.milliseconds(200), scheduler: MainScheduler.asyncInstance)
            .bind { [weak self] value in
                guard let wSelf = self, wSelf.previousName != value
                else { return }
                wSelf.previousName = value
                if value.count <= Constant.limitName {
                    wSelf.updateUserName(type: .lessSix)
                } else {
                    wSelf.delegate?.validateNameNewUI()
                }
                wSelf.detectConfirm()
            }.disposed(by: disposeBag)
        
        tfPhone.rx.text.orEmpty.skip(1).map { text in
            return (text.isValidPhone(phone: text), text.count)
        }.bind { [weak self] item in
            guard let self = self else {
                return
            }
            let isValid = item.0
            let count = item.1
            let type = PhoneType.getStt(count: count, isValid: isValid)
            self.statusPhone(typePhone: type)
            self.detectConfirm()
        }.disposed(by: disposeBag)
        
        self.btSelectCity.rx.tap.bind { [weak self] _ in
            guard let self = self else {
                return
            }
            self.delegate?.moveToLocation()
        }.disposed(by: disposeBag)
        
        self.tfCity.rx.controlEvent(.editingDidBegin).bind { [weak self] _ in
            guard let self = self else { return }
            self.delegate?.moveToLocation()
            self.tfCity.resignFirstResponder()
        }.disposed(by: disposeBag)
        
        SelectMomAge.allCases.forEach { type in
            let bt = btAgesMom[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let self = self else {
                    return
                }
                self.btAgesMom.forEach { bt in
                    bt.setTitleColor(R.color.c8686(), for: .normal)
                    bt.backgroundColor = R.color.f1F1F1()
                }
                bt.backgroundColor = R.color.ffc440Color()
                bt.setTitleColor(R.color.f3C(), for: .normal)
                self.ageRange = type.value
            }.disposed(by: disposeBag)
        }
        
        self.btCheck.rx.tap.bind { [weak self] in
            guard let self = self else { return }
            if let img = self.imgChecked.image, img == R.image.ic_checkbox_selected() {
                self.imgChecked.image = R.image.ic_checkBox_uncheck()
            } else {
                self.imgChecked.image = R.image.ic_checkbox_selected()
            }
            self.detectConfirm()
        }.disposed(by: disposeBag)
        
        self.btCompleted.rx.tap.bind { [weak self] _ in
            guard let self = self,
                  let displayName = self.txtDisplayName.text,
                  let phone = self.tfPhone.text else { return }
            
            var numberBaby: Int = 0
            
            if let text = self.birthDayBaby.text,
               !text.isEmpty,
               let date = text.convertToDate(formatDate: .ddMMyyyy) {
                numberBaby = date.secondsSince1970
            }
            
            var birthdayPreg: Int?
            if let text = self.birthDayPreg.text,
               !text.isEmpty,
               let date = text.convertToDate(formatDate: .ddMMyyyy) {
                birthdayPreg = date.secondsSince1970
            }
            
            self.delegate?.registerSocialNew(ageRange: self.ageRange,
                                             displayName: displayName,
                                             cityCode: self.cityCode,
                                             phone: phone,
                                             birthdayBaby: numberBaby,
                                             birthdayPreg: birthdayPreg)
        }.disposed(by: disposeBag)
    }
    
    func statusPhone(typePhone: PhoneType, text: String? = nil) {
        switch typePhone{
        case .valid:
            self.phoneView.isHidden = true
            self.imgPhone.image = R.image.ic_Succes()
            self.imgPhone.isHidden = false
        case .zero:
            self.phoneView.isHidden = false
            self.lbPhoneError.text = "Vui lòng nhập số điện thoại"
            self.imgPhone.image = R.image.ic_Close()
            self.imgPhone.isHidden = false
        case .invalid:
            self.phoneView.isHidden = false
            self.lbPhoneError.text = "Vui lòng nhập đúng định dạng số điện thoại"
            self.imgPhone.image = R.image.ic_Close()
            self.imgPhone.isHidden = false
        case .used:
            self.phoneView.isHidden = false
            self.lbPhoneError.text = "Vui lòng nhập lại số điện thoại"
            if let text = text {
                self.lbPhoneError.text = text
            }
            self.imgPhone.image = R.image.ic_Close()
            self.imgPhone.isHidden = false
        }
        
    }
    
    private func detectConfirm() {
        if let cityName = self.tfCity.text, !cityName.isEmpty {
            self.cityErrorView.isHidden = true
        } else {
            self.cityErrorView.isHidden = false
        }
        self.btCompleted.isEnabled = self.canNext()
        self.btCompleted.alpha = self.canNext() ? 1 : 0.5
    }
    
    private func canNext() -> Bool {
        guard self.imgChecked.image == R.image.ic_checkbox_selected(),
              let name = self.txtDisplayName.text,
              !name.isEmpty,
              isName,
              self.imgPhone.image == R.image.ic_Succes(),
              let city = tfCity.text,
              !city.isEmpty else {
            return false
        }
        return true
    }
    
    func setValueAPIFail(displayName: String) {
        self.lblUserName.text = displayName
        self.imgUserName.image = R.image.ic_Close()
        self.imgUserName.isHidden = false
        self.isName = false
        self.detectConfirm()
    }
    
    func updateCity(city: CityName) {
        switch city.locationCode {
        case 1:
            self.tfCity.text = city.locationNameVi ?? ""
            self.cityCode = city.locationCode ?? 0
        case 79:
            self.tfCity.text = R.string.localizable.commonTPHCMShort()
            self.cityCode = city.locationCode ?? 0
        default:
            self.tfCity.text = city.locationNameVi ?? ""
            self.cityCode = city.locationCode ?? 0
        }
        self.detectConfirm()
    }
    
    func updateUserName(type: UserNameType) {
        switch type {
        case .lessZero:
            self.lblUserName.text = R.string.localizable.registerCorectRequirePass()
            self.lblUserName.textColor = .red
            self.imgUserName.image = R.image.ic_Close()
            self.imgUserName.isHidden = false
            self.displayNameView.isHidden = false
            self.isName = false
        case .containerSpecial:
            self.lblUserName.text = "Tài khoản không được chứa ký tự đặc biệt"
            self.lblUserName.textColor = .red
            self.imgUserName.image = R.image.ic_Close()
            self.imgUserName.isHidden = false
            self.displayNameView.isHidden = false
            self.isName = false
        case .success:
            self.imgUserName.image = R.image.ic_Succes()
            self.imgUserName.isHidden = false
            self.displayNameView.isHidden = true
            self.isName = true
        case .fail:
            self.lblUserName.text = R.string.localizable.registerCheckName()
            self.imgUserName.image = R.image.ic_Close()
            self.imgUserName.isHidden = false
            self.displayNameView.isHidden = false
            self.lblUserName.textColor = .red
            self.isName = false
        case .lessSix:
            self.lblUserName.text = "Tài khoản không được nhỏ hơn 6 ký tự"
            self.lblUserName.textColor = .red
            self.imgUserName.image = R.image.ic_Close()
            self.imgUserName.isHidden = false
            self.displayNameView.isHidden = false
            self.isName = false
        }
        self.detectConfirm()
    }
    
    func updateValue(name: String) {
        self.txtDisplayName.text = name
    }
    
    func addImageBg(view: UIView) {
        let imageView: UIImageView = UIImageView(image: R.image.img_bg_regiser_nav())
        view.addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.left.right.top.equalToSuperview()
        }
    }
    
    //MARK: Setup date Picker
    private func setupDatePicker() {
        self.dateFormat.dateStyle = DateFormatter.Style.medium
        self.dateFormat.timeStyle = DateFormatter.Style.none
        self.dateFormat.dateFormat = Key.DateFormat.DateFormatddMMyyyy
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        self.datePicker.addTarget(self, action: #selector(updateDateField(sender:)), for: UIControl.Event.valueChanged)
        self.birthDayBaby.inputView = datePicker
        self.birthDayBaby.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        let image = R.image.ic_pregnant_calcelator()
        imageView.image = image
        self.birthDayBaby.rightView = imageView
        
        self.birthDayPreg.inputView = datePicker
        self.birthDayPreg.rightViewMode = UITextField.ViewMode.always
        let imageViewReal = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        let imageReal = R.image.ic_pregnant_calcelator()
        imageViewReal.image = imageReal
        self.birthDayPreg.rightView = imageViewReal
    }
    
    @objc private func updateDateField(sender: UIDatePicker) {
        if self.birthDayBaby.isFirstResponder {
            self.birthDayBaby.text = dateFormat.string(from: datePicker.date)
        }
        
        if self.birthDayPreg.isFirstResponder {
            self.birthDayPreg.text = dateFormat.string(from: datePicker.date)
        }
    }
    
}
extension RegisterNewUI: UITextFieldDelegate {
    
}
