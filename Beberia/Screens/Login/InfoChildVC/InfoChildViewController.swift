//
//  InfoChildViewController.swift
//  Beberia
//
//  Created by OS on 10/18/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftKeychainWrapper

class InfoChildViewController: BaseViewController {
    //MARK: outlet
    @IBOutlet weak var viewDuSinh: UIView!
    @IBOutlet weak var tftNgayDuSinh: UITextField!
    @IBOutlet weak var btnNotHaveBaby: UIButton!
    @IBOutlet weak var btnHaveBaby: UIButton!
    @IBOutlet weak var tbvAddChildren: UITableView!
    @IBOutlet weak var btnFinish: UIButton!
    
    //MARK: Properties
    var ageRange = 0
    var arrayChildrens = [InfoChildren.init(json: "")]
    var arrayBaby = [[String: Any]]()
    var getlocationID = 0
    var getDistrictID = ""
    var userID = 0
    var getbirthday = 0.0
    var getSDT = ""
    var dateDuSinh = Date()
    var typeMom = 0
    var provider_id = ""
    
     let listChannels = ["sendbird_group_channel_19224561_64953fda33be4b774c4d3fb5f8070d4136e258c8","sendbird_group_channel_19611922_087c66246816e8f9d9ec30080cbfcd99d2d3d54a"]
    
    var isHaveBaby : Bool = true {
        didSet{
            if isHaveBaby{
                Utils.updateColorButton(btnSelect: btnHaveBaby, btnUnSelect: btnNotHaveBaby)
                tbvAddChildren.isHidden = false
                viewDuSinh.isHidden = true
                typeMom = 0
            }else{
                Utils.updateColorButton(btnSelect: btnNotHaveBaby, btnUnSelect: btnHaveBaby)
                tbvAddChildren.isHidden = true
                viewDuSinh.isHidden = false
                typeMom = 1
            }
        }
    }
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: Setup UI
    func setupUI(){
        self.title = R.string.localizable.loginTitleInfoMe()
        tbvAddChildren.register(R.nib.infoLoginTableViewCell)
        tbvAddChildren.delegate = self
        tbvAddChildren.dataSource = self
        tbvAddChildren.separatorStyle = .none
        tbvAddChildren.allowsSelection = false
        btnFinish.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 20)
      //  customHideLeftBarButton()
        customLeftBarButton()
        isHaveBaby = false
        tftNgayDuSinh.delegate = self
        
    }
    
    //MARK: Action
    @IBAction func didPressNotHaveBabu(_ sender: Any) {
        isHaveBaby = false
    }
  
    @IBAction func didPressHaveBaby(_ sender: Any) {
        isHaveBaby = true
    }
    
    @IBAction func btnSkip(_ sender: Any) {
        Utils.presentTabbar()
    }
    
    @IBAction func btnFinish(_ sender: Any) {

        btnFinish.isEnabled = false
        arrayBaby.removeAll()
        
        if isHaveBaby{
             
            for children in arrayChildrens {
                if children.birthday == nil || children.nickname == nil{
                    self.btnFinish.isEnabled = true
                    Utils.showAlertView(controller: self, title: R.string.localizable.loginTitleAlert(), message: R.string.localizable.loginTitleAlertMess())
                    return
                }
                var dicBaby = [String: Any]()
                dicBaby["birthday"] = children.birthday
                dicBaby["gender"] = children.gender
                dicBaby["nickname"] = children.nickname
                arrayBaby.append(dicBaby)
            }
            
        }else{

            if tftNgayDuSinh.text == ""{
                Utils.showAlertView(controller: self, title: R.string.localizable.loginTitleAlert(), message: R.string.localizable.loginTitleAlertMess())
                self.btnFinish.isEnabled = true
                return
            }

            var dicBaby = [String: Any]()
            dicBaby["birthday"] = dateDuSinh.timeIntervalSince1970
            dicBaby["gender"] = 1
            dicBaby["nickname"] = ""
            arrayBaby.append(dicBaby)
        }

        updateInfo()
    }
    
    func updateInfo(){
        SVProgressHUD.show()
        
        APIManager.updateInfo(ageRange: ageRange ,userId: UserInfo.shareUserInfo.id!, birthday: getbirthday, phone: getSDT , cityCode: getlocationID, baby_info: Utils.json(from: arrayBaby) ?? "",typeMom : typeMom, district_code: getDistrictID, callbackSuccess: { [weak self] (user) in
            guard let self = self else { return }
            let postVC = WriteHelloVC.init(nib: R.nib.writeHelloVC)
            postVC.ageRange = self.isHaveBaby ? 6 : 0
            self.present(postVC, animated: true, completion: nil)
            self.btnFinish.isEnabled = true
            if UserInfo.shareUserInfo.token != ""{
                UserDefault.userIDSocial = self.provider_id
                KeychainWrapper.standard.set(true, forKey: KeychainKeys.isLoggedIn.rawValue)
                KeychainWrapper.standard.set(Utils.getAppDelegate().username, forKey: KeychainKeys.userName.rawValue)
                KeychainWrapper.standard.set(Utils.getAppDelegate().password, forKey: KeychainKeys.passWord.rawValue)
                Utils.getAppDelegate().username = ""
                Utils.getAppDelegate().password = ""
            }
            
            // join channel defaul
//            SendBirdManager.share.connectUserToSendBird {
//                SendBirdManager.share.joinChannelbyChanelUrl(listChanels: self.listChannels, offNoti: true)
//                SendBirdManager.share.joinChanelSuccess  =  { (groupChannel) in
//                }
//            }
            
            
            SVProgressHUD.dismiss()
        }) { (error) in
            self.btnFinish.isEnabled = true
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyyyy
        tftNgayDuSinh.text = dateFormatter.string(from: sender.date)
        dateDuSinh = sender.date
    }
}


//MARK: UITableview Delegate
extension InfoChildViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChildrens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.infoLoginTableViewCell, for: indexPath)!
        cell.btnClose.tag = indexPath.row
        cell.lblTitleInfo.text = "\(R.string.localizable.loginTitleInfoChild()) (\(indexPath.row + 1))"
        let children = self.arrayChildrens[indexPath.row]
        
        if children.birthday == nil{
            cell.lblBirthday.text = ""
        }else{
            cell.lblBirthday.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(children.birthday ?? 0 )), format: Key.DateFormat.DateFormatddMMyyyy)
        }
        
        if children.gender == 1 {
            Utils.updateColorButton(btnSelect: cell.btnNam, btnUnSelect: cell.btnNu)
        }else{
            Utils.updateColorButton(btnSelect: cell.btnNu, btnUnSelect: cell.btnNam)
        }
        cell.lblName.text = children.nickname
        
        
        
        cell.tapCloseCell = { index in
            if self.arrayChildrens.count > 1{
                self.arrayChildrens.remove(at: index)
                self.tbvAddChildren.reloadData()
            }
        }
                cell.tapAddChildren = {
                    let childrenModel = InfoChildren.init(json: "")
                    self.arrayChildrens.append(childrenModel)
                    self.tbvAddChildren.reloadData()
                }
                cell.viewAddChildren.isHidden = false
        if self.arrayChildrens.count > 1{
            if (indexPath.row != self.arrayChildrens.count - 1){
                cell.viewAddChildren.isHidden = true
            }
        }
        cell.updateGender = { index , gender in
            self.arrayChildrens[index].gender = gender
        }
        cell.updateNameChildren = { index , name in
            self.arrayChildrens[index].nickname = name
        }
        cell.updateBirthday = { index , date in
            self.arrayChildrens[index].birthday = date.timeIntervalSince1970
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.arrayChildrens.count > 1{
            if (indexPath.row != self.arrayChildrens.count - 1){
                return 230
            }
            return 270
        }
        return 270
    }
}

//MARK: UItextfield Delegate
extension InfoChildViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tftNgayDuSinh
        {
            let datePickerView:UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePicker.Mode.date
            datePickerView.minimumDate = Date()
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(InfoChildViewController.datePickerValueChanged), for: UIControl.Event.valueChanged)
            
            if #available(iOS 13.4, *) {
                datePickerView.preferredDatePickerStyle = .wheels
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tftNgayDuSinh{
          //  updateBirthday(btnClose.tag, dateBrithday)
        }
    }
}
