//
//  OrderDetail.swift
//  Beberia
//
//  Created by haiphan on 07/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol OrderDetailDelegate {
    func updateOrder(order: ManageOrderModel)
}

class OrderDetail: BaseNavigationColor {
    
    enum StatusOrder: Int, CaseIterable {
        case notYetPay, preparing, completed, sent
        
        static func getStatus(status: Int) -> Self {
            if status == StatusOrderGlobal.notYetPay.rawValue {
                return .notYetPay
            }
            
            if status == StatusOrderGlobal.preparing.rawValue {
                return .preparing
            }
            
            if status == StatusOrderGlobal.completed.rawValue {
                return .completed
            }
            
            return .sent
        }
    }
    
    enum StatusCancel {
        case cancel, other, completed
    }
    
    enum LabelText: Int, CaseIterable {
        case orderId, requestDay, fullName, numberPhone, email, titelIamge, priceImage, total, totalPayment
    }
    
    enum Action: Int, CaseIterable {
        case copy, cancel, reOrder
    }
    
    var detailOrder: ManageOrderModel?
    var delegate: OrderDetailDelegate?

    @IBOutlet var lbs: [UILabel]!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageBaby: UIImageView!
    @IBOutlet var lbsStatus: [UILabel]!
    @IBOutlet var lbsStatusDate: [UILabel]!
    @IBOutlet var imgsStatus: [UIImageView]!
    @IBOutlet var viewsStatus: [UIView]!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var cancelPaymentView: UIView!
    @IBOutlet weak var refundView: UIView!
    @IBOutlet weak var reseaonView: UIView!
    @IBOutlet weak var lbRefundMoney: UILabel!
    @IBOutlet weak var paymentTypeView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var lbReason: UILabel!
    @IBOutlet weak var lbTextRequestDay: UILabel!
    @VariableReplay private var statusCancel: StatusCancel = .other
    private let eventBackWithButton: PublishSubject<Void> = PublishSubject.init()
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
}
extension OrderDetail {
    
    private func setupUI() {
        title = "Thông tin đơn hàng"
        
        if let order = self.detailOrder {
            self.lbs[LabelText.orderId.rawValue].text = "#\(order.id ?? 20)"
            let date = Date(timeIntervalSince1970: order.created ?? 20)
            self.lbs[LabelText.requestDay.rawValue].text = "\(date.toString(format: String.FormatDate.ddMMyyyy))"
            self.lbs[LabelText.fullName.rawValue].text = "\(order.contactName ?? "PTH")"
            self.lbs[LabelText.numberPhone.rawValue].text = "\(order.phone ?? "")"
            self.lbs[LabelText.email.rawValue].text = "\(order.contactEmail ?? "")"
            self.lbs[LabelText.titelIamge.rawValue].text = "\(order.item?.name ?? "")"
            self.lbs[LabelText.priceImage.rawValue].text = order.item?.showPricce()
            self.lbs[LabelText.total.rawValue].text = "\(order.payAmount?.currency ?? "")"
            self.lbs[LabelText.totalPayment.rawValue].text = "\(order.payAmount?.currency ?? "")"
            self.lbRefundMoney.text = "\(order.payAmount?.currency ?? "")"
            self.lbReason.text = "\(order.cancelReason ?? "")"
            
            if let textUrl = order.image, let url = URL(string: textUrl) {
                self.imageBaby.kf.setImage(with: url)
            }
            
            self.setupStatusView(order: order)
            
            if order.status == StatusOrderGlobal.cancel.rawValue {
                self.statusCancel = .cancel
            } else  if  order.status == StatusOrderGlobal.completed.rawValue || order.status == StatusOrderGlobal.sent.rawValue {
                self.statusCancel = .completed
            } else if order.status == StatusOrderGlobal.notYetPay.rawValue {
                self.bts[Action.reOrder.rawValue].setTitle("Thanh toán lại", for: .normal)
            }
        }
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
    
    private func setupRX() {
        Action.allCases.forEach { [weak self] type in
            guard let wSelf = self else { return }
            let bt = wSelf.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                switch type {
                case .cancel:
                    if wSelf.statusCancel == .cancel || wSelf.statusCancel == .completed {
                        wSelf.navigationController?.popViewController(animated: true, { [weak self] in
                            guard let wSelf = self else { return }
                            wSelf.eventBackWithButton.onNext(())
                        })
                        
                    } else {
                        let vc = CancelationOrder.createVC()
                        vc.order = wSelf.detailOrder
                        vc.delegate = self
                        wSelf.navigationController?.pushViewController(vc, animated: true)
                    }
                case .copy:  UIPasteboard.general.string = wSelf.lbs[LabelText.orderId.rawValue].text
                case .reOrder:
                    if let order = wSelf.detailOrder {
                        if order.status == StatusOrderGlobal.notYetPay.rawValue {
                            wSelf.moveToTabbarTORepay(order: order)
                        } else {
                            wSelf.moveToTabbar(order: order)
                        }
                    }
                    

                }
            }.disposed(by: wSelf.disposeBag)
        }
        
        Observable.merge(self.eventBackWithButton.asObservable(), self.eventBackNavigation.asObservable()).bind { [weak self] _ in
            guard let wSelf = self else { return }
            if let sort = wSelf.detailOrder {
                wSelf.delegate?.updateOrder(order: sort)
            }
        }.disposed(by: self.disposeBag)
        
        self.$statusCancel.asObservable().bind { [weak self] stt in
            guard let wSelf = self else { return }
            switch stt {
            case .cancel:
                wSelf.handleShowCancel()
                wSelf.statusView.isHidden = true
                wSelf.bts[Action.cancel.rawValue].setTitle("Đóng", for: .normal)
                wSelf.reseaonView.isHidden = false
                wSelf.infoView.isHidden = true
                wSelf.paymentView.isHidden = true
                wSelf.totalView.isHidden = true
            case .completed:
                wSelf.bts[Action.cancel.rawValue].setTitle("Đóng", for: .normal)
                wSelf.cancelView.isHidden = true
                wSelf.cancelPaymentView.isHidden = true
                wSelf.statusView.isHidden = false
                wSelf.refundView.isHidden = true
                wSelf.reseaonView.isHidden = true
                wSelf.infoView.isHidden = false
                wSelf.paymentView.isHidden = false
                wSelf.totalView.isHidden = false
            case .other: break
            }
        }.disposed(by: self.disposeBag)
    }
    
    private func handleShowCancel() {
        if let order = self.detailOrder, let pay = order.payDate, pay > 0 {
            self.cancelPaymentView.isHidden = false
            self.cancelView.isHidden = true
            self.paymentTypeView.isHidden = false
            self.refundView.isHidden = false
        } else {
            self.cancelView.isHidden = false
            self.cancelPaymentView.isHidden = true
            self.paymentTypeView.isHidden = true
            self.refundView.isHidden = true
        }
        
        if let order = self.detailOrder {
            let createDate = order.cancelDate?.toDate() ?? Date()
            self.lbs[LabelText.requestDay.rawValue].text = "\(createDate.toString(format: String.FormatDate.ddMMyyyy))"
        }
        
        self.lbTextRequestDay.text = "Ngày huỷ đơn:"
        self.bts[Action.reOrder.rawValue].setTitle("Đặt lại", for: .normal)
        
    }
    
    private func moveToTabbarTORepay(order: ManageOrderModel) {
        if let tabBarVC = R.storyboard.main.tabBarVC() {
            tabBarVC.selectedIndex = 3
            tabBarVC.passValueBabyToRepay(order: order)
            self.navigationController?.pushViewController(tabBarVC, animated: true)
        }
    }
    
    private func moveToTabbar(order: ManageOrderModel) {
        if let tabBarVC = R.storyboard.main.tabBarVC() {
            tabBarVC.selectedIndex = 3
            tabBarVC.passValueBaby(order: order)
            self.navigationController?.pushViewController(tabBarVC, animated: true)
        }
    }
    
    private func setupStatusView(order: ManageOrderModel) {
        switch StatusOrder.getStatus(status: order.status ?? 0) {
        case .notYetPay:
            self.imgsStatus.forEach { img in
                img.image = R.image.img_checkStatus()
            }
            self.imgsStatus[StatusOrder.notYetPay.rawValue].image = R.image.img_checkedStatus()
            
            self.lbsStatus.forEach { lb in
                lb.textColor = R.color.cececE()
            }
            self.lbsStatus[StatusOrder.notYetPay.rawValue].textColor = R.color.d5B5B()
            
            self.lbsStatusDate.forEach { lb in
                lb.isHidden = true
            }
            let date = order.created?.toDate() ?? Date()
            self.lbsStatusDate[StatusOrder.notYetPay.rawValue].text = "\(date.toString(format: String.FormatDate.HHmmEEEEddMMyyyy))"
            self.lbsStatusDate[StatusOrder.notYetPay.rawValue].isHidden = false
            
        case .preparing:
            self.imgsStatus[StatusOrder.notYetPay.rawValue].image = R.image.img_checkedStatus()
            self.imgsStatus[StatusOrder.preparing.rawValue].image = R.image.img_checkedStatus()
            self.imgsStatus[StatusOrder.completed.rawValue].image = R.image.img_checkStatus()
            self.imgsStatus[StatusOrder.sent.rawValue].image = R.image.img_checkStatus()
            
            self.lbsStatus[StatusOrder.notYetPay.rawValue].textColor = R.color.d5B5B()
            self.lbsStatus[StatusOrder.preparing.rawValue].textColor = R.color.d5B5B()
            self.lbsStatus[StatusOrder.completed.rawValue].textColor = R.color.cececE()
            self.lbsStatus[StatusOrder.sent.rawValue].textColor = R.color.cececE()
            
            let createDate = order.created?.toDate() ?? Date()
            self.lbsStatusDate[StatusOrder.notYetPay.rawValue].text = "\(createDate.toString(format: String.FormatDate.HHmmEEEEddMMyyyy))"
            self.lbsStatusDate[StatusOrder.notYetPay.rawValue].isHidden = false
            let prepareDate = order.payDate?.toDate() ?? Date()
            self.lbsStatusDate[StatusOrder.preparing.rawValue].text = "\(prepareDate.toString(format: String.FormatDate.HHmmEEEEddMMyyyy))"
            self.lbsStatusDate[StatusOrder.preparing.rawValue].isHidden = false
            self.lbsStatusDate[StatusOrder.completed.rawValue].isHidden = true
            self.lbsStatusDate[StatusOrder.sent.rawValue].isHidden = true
            
        case .completed:
            self.imgsStatus.forEach { img in
                img.image = R.image.img_checkedStatus()
            }
            self.imgsStatus[StatusOrder.sent.rawValue].image = R.image.img_checkStatus()
            
            self.lbsStatus.forEach { lb in
                lb.textColor = R.color.d5B5B()
            }
            self.lbsStatus[StatusOrder.sent.rawValue].textColor = R.color.cececE()
            
            self.lbsStatusDate.enumerated().forEach { item in
                let offset = item.offset
                let element = item.element
                element.isHidden = false
                switch StatusOrder.init(rawValue: offset) {
                case .notYetPay:
                    let createDate = order.created?.toDate() ?? Date()
                    self.lbsStatusDate[StatusOrder.notYetPay.rawValue].text = "\(createDate.toString(format: String.FormatDate.HHmmEEEEddMMyyyy))"
                case .preparing:
                    let prepareDate = order.payDate?.toDate() ?? Date()
                    self.lbsStatusDate[StatusOrder.preparing.rawValue].text = "\(prepareDate.toString(format: String.FormatDate.HHmmEEEEddMMyyyy))"
                case .completed:
                    let prepareDate = order.sendDate?.toDate() ?? Date()
                    self.lbsStatusDate[StatusOrder.completed.rawValue].text = "\(prepareDate.toString(format: String.FormatDate.HHmmEEEEddMMyyyy))"
                case .sent, .none: break
                }
            }
            self.lbsStatusDate[StatusOrder.sent.rawValue].isHidden = true
        case .sent:
            self.imgsStatus.forEach { img in
                img.image = R.image.img_checkedStatus()
            }
            self.lbsStatus.forEach { lb in
                lb.textColor = R.color.d5B5B()
            }
            
            self.lbsStatusDate.enumerated().forEach { item in
                let offset = item.offset
                let element = item.element
                element.isHidden = false
                switch StatusOrder.init(rawValue: offset) {
                case .notYetPay:
                    let createDate = order.created?.toDate() ?? Date()
                    self.lbsStatusDate[StatusOrder.notYetPay.rawValue].text = "\(createDate.toString(format: String.FormatDate.HHmmEEEEddMMyyyy))"
                case .preparing:
                    let prepareDate = order.payDate?.toDate() ?? Date()
                    self.lbsStatusDate[StatusOrder.preparing.rawValue].text = "\(prepareDate.toString(format: String.FormatDate.HHmmEEEEddMMyyyy))"
                case .completed:
                    let prepareDate = order.sendDate?.toDate() ?? Date()
                    self.lbsStatusDate[StatusOrder.completed.rawValue].text = "\(prepareDate.toString(format: String.FormatDate.HHmmEEEEddMMyyyy))"
                case .sent:
                    let prepareDate = order.sendDate?.toDate() ?? Date()
                    self.lbsStatusDate[StatusOrder.sent.rawValue].text = "\(prepareDate.toString(format: String.FormatDate.HHmmEEEEddMMyyyy))"
                case .none: break
                }
            }
            
        }
    }
}
extension OrderDetail: CancelationOrderDelegate {
    func cancelSuccess(reason: String) {
        self.detailOrder?.cancelDate = Date().timeIntervalSince1970
        self.detailOrder?.status = StatusOrderGlobal.cancel.rawValue
        self.statusCancel = .cancel
        self.lbReason.text = reason
    }
}
