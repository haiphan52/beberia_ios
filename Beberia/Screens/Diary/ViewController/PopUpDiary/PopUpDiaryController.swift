//
//  PopUpDiaryController.swift
//  Beberia
//
//  Created by OS on 10/31/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class PopUpDiaryController: UIViewController {

    // MARK: - Properties
    
    var writePopupDiary: (_ index :Int) -> () = {_ in}
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        showAnimate()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    //MARK: - Helper methods
    
    func showAnimate (){
        self.view.transform = CGAffineTransform(scaleX: 1.3,y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0,y: 1.0)
        }
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3,y: 1.3)
            self.view.alpha = 0.0
        }) { (finished : Bool) in
            if (finished){
                self.view.removeFromSuperview()
            }
        }
    }
    
    // MARK: - Action
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOff(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "tunrOnOff")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnWrite(_ sender: Any) {
        writePopupDiary(1)
        self.dismiss(animated: true, completion: nil)
    }

}
