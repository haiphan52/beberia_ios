//
//  DiaryViewController.swift
//  Beberia
//
//  Created by IMAC on 8/24/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import FSPagerView
import Kingfisher
import SVProgressHUD
import PullToRefresh
import RxSwift
import RxCocoa

class DiaryViewController: BaseViewController {
    
    var openFrom: GuideGetPointViewController.openfrom = .other
    
    //MARK: - Outlet
    @IBOutlet weak var btnNewMore: UIButton!
    @IBOutlet weak var btnTipMore: UIButton!
    @IBOutlet weak var heightClv: NSLayoutConstraint!
    @IBOutlet weak var clvDiaryNew: UICollectionView!
    @IBOutlet weak var widthProgressBar: NSLayoutConstraint!
    @IBOutlet weak var progressBar: UIView!
    @IBOutlet weak var bGProgressBar: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintHeightTbv: NSLayoutConstraint!
    @IBOutlet weak var btnButttonAdd: UIButton!
    @IBOutlet weak var tbvDiary: UITableView!
    @IBOutlet weak var lblToDay: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewNoti: UIView!
    @IBOutlet weak var constrainHeightViewNoti: NSLayoutConstraint!
    @IBOutlet weak var lblNotiFullDate: UILabel!
    @IBOutlet weak var imgNotiDate: UIImageView!
    @IBOutlet weak var imgGiftBox: UIImageView!
    @IBOutlet weak var heightViewDateWrite: NSLayoutConstraint!
    @IBOutlet weak var viewDataWrite: UIView!
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var tbvDiaryNew: UITableView!
    @IBOutlet weak var hieghtTbvDiaryNew: NSLayoutConstraint!
    @IBOutlet weak var pageViewDiary: FSPagerView!
    @IBOutlet weak var heightPageViewdiary: NSLayoutConstraint!
    
    //MARK: - Properties
    var isShowViewNoti = false{
        didSet{
            if isShowViewNoti{
            //    UIView.animate(withDuration: 0.5) {
                    self.constrainHeightViewNoti.constant = 30
                    self.viewNoti.isHidden = false
                    self.view.layoutIfNeeded()
//                }
                
            }else{
            //    UIView.animate(withDuration: 0.5) {
                    self.constrainHeightViewNoti.constant = 0
                    self.viewNoti.isHidden = true
                    self.view.layoutIfNeeded()
//                }
            }
        }
    }
    
    var isFullDay = false {
        didSet{
            lblToDay.isHidden = isFullDay
            lblDate.isHidden = isFullDay
            lblNotiFullDate.isHidden = !isFullDay
            imgNotiDate.isHidden = !isFullDay
            imgGiftBox.isHidden = isFullDay
        }
    }
    
    var isCheckWrite = false{
        didSet{
            btnButttonAdd.isHidden = isCheckWrite
            //  if !isCheckWrite{ self.showPopupWrite() }
        }
    }
    
    var dayWrite = 0
    var listDiaryHome = [DiaryHome]()
    var listDiaryNew = BehaviorRelay.init(value: [Diaries]())
//    var listDiaryTop = BehaviorRelay.init(value: [Diaries]())
    var listDiaryTip = BehaviorRelay.init(value: [Diaries]())
    var listDiaryTop = [Diaries]()
    var arrayImage = [String]()
    var cellHeightsDictionary: [IndexPath: CGFloat] = [:]
    let refresher = PullToRefresh()
    let heightImageClv = (UIScreen.main.bounds.width / 2) - 32 + 156
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        initRx()
        LoaddingManager.initLoadding(view: self.view)
        self.getBanner()
        gethomeDiary()
        
        self.tbvDiary.remembersLastFocusedIndexPath = true
        NotificationCenter.default.addObserver(self, selector: #selector(checkEditDiary), name: NSNotification.Name.checkEditDiary, object: nil)
        
        // hide view đếm số ngày viết nhật ký
        heightViewDateWrite.constant = 0
        viewDataWrite.isHidden = true
        self.view.layoutIfNeeded()
        self.setupSlideShow()
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tbvDiary.isUserInteractionEnabled = true
        self.tabBarController?.delegate = self
        
        if Utils.checkLogin(selfView: self,showAlert: false){
            getDate()
        }else{
            heightViewDateWrite.constant = 0
            viewDataWrite.isHidden = true
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func checkEditDiary(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        let idFeed = object[Key.KeyNotification.idDiary] as? Int
        self.getCheckCompleteDiary(id: idFeed ?? 0, completion: { diary in
            
            var listDiaryNewTep = self.listDiaryNew.value
            // for (section, diaries) in self.listDiaryNew.value.enumerated(){
            let index = listDiaryNewTep.firstIndex(where: { (diaryEdit) -> Bool in
                return diary.id == diaryEdit.id
            })
            if index != nil{
                listDiaryNewTep[index!] = diary
                self.listDiaryNew.accept(listDiaryNewTep)
            }
            // }
            
        })
    }
    
    func setupSlideShow(){
        pageViewDiary.backgroundColor = .clear
        pageViewDiary.dataSource = self
        pageViewDiary.delegate = self
        pageViewDiary.automaticSlidingInterval = 3.0
        pageViewDiary.interitemSpacing = 10
        pageViewDiary.register(UINib(nibName: "DiaryNewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DiaryNewCollectionViewCell")
        pageViewDiary.itemSize = CGSize(width: (UIScreen.main.bounds.width / 2), height: 327)
        pageViewDiary.transformer = FSPagerViewTransformer(type: .linear)
    }
    

    
    private func initRx(){
        // MARK: // init CollectionView
        let flowLayoutAfterBefore = UICollectionViewFlowLayout()
        
        flowLayoutAfterBefore.itemSize = CGSize(width: (UIScreen.main.bounds.width / 2) , height: heightImageClv)
     //   flowLayoutAfterBefore.estimatedItemSize = CGSize(width: (UIScreen.main.bounds.width / 2) , height: 60)
        flowLayoutAfterBefore.scrollDirection = .vertical
        flowLayoutAfterBefore.minimumInteritemSpacing = 0
        flowLayoutAfterBefore.minimumLineSpacing = 0
        flowLayoutAfterBefore.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvDiaryNew.setCollectionViewLayout(flowLayoutAfterBefore, animated: true)
        clvDiaryNew.showsHorizontalScrollIndicator = false
        clvDiaryNew.register(R.nib.diaryNewCollectionViewCell)
        clvDiaryNew.isScrollEnabled = false

        
        listDiaryNew.asObservable().bind(to: self.clvDiaryNew.rx.items(cellIdentifier: R.reuseIdentifier.diaryNewCollectionViewCell.identifier, cellType: DiaryNewCollectionViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }

            cell.setupData(data: data)
            cell.contentView.layer.shadowRadius = 0
            cell.contentView.layer.shadowOpacity = 0

        }.disposed(by: disposeBag)
        
//        tbvDiaryNew.register(R.nib.diaryNewsTableViewCell)
//        tbvDiaryNew.separatorStyle = .none
//        listDiaryNew.asObservable().bind(to: self.tbvDiaryNew.rx.items(cellIdentifier: R.reuseIdentifier.diaryNewsTableViewCell.identifier, cellType: DiaryNewsTableViewCell.self)) { [weak self]  row, data, cell in
//            guard let _ = self else { return }
//
//            cell.setupData(data: data)
//        }.disposed(by: disposeBag)
        
        tbvDiary.register(R.nib.tipTableViewCell)
        listDiaryTip.asObservable().bind(to: self.tbvDiary.rx.items(cellIdentifier: R.reuseIdentifier.tipTableViewCell.identifier, cellType: TipTableViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }

            cell.setupDataMore(listDiaryMore: data)
        }.disposed(by: disposeBag)
        
        tbvDiary.rx.observe(CGSize.self, "contentSize").asObservable().observeOn(MainScheduler.asyncInstance).subscribe { [weak self] (size) in
            guard let self = self else { return }
        //    self.tbvDiary.layer.removeAllAnimations()
            self.constraintHeightTbv.constant = size.element!!.height
//            UIView.animate(withDuration: 0.5) {
//                self.updateViewConstraints()
//                self.view.layoutIfNeeded()
//            }

        }.disposed(by: disposeBag)
        
        clvDiaryNew.rx.observe(CGSize.self, "contentSize").asObservable().observeOn(MainScheduler.asyncInstance).subscribe { [weak self] (size) in
            guard let self = self else { return }
            // self.hieghtTbvDiaryNew.constant = size.element!!.height
            self.heightClv.constant = size.element!!.height > self.heightImageClv * 2 ? self.heightImageClv * 2 : size.element!!.height
            
            self.heightPageViewdiary.constant = self.heightImageClv
            UIView.animate(withDuration: 0.3) {
                self.updateViewConstraints()
                self.view.layoutIfNeeded()
            }
        }.disposed(by: disposeBag)
        
        Observable.zip(clvDiaryNew.rx.itemSelected, clvDiaryNew.rx.modelSelected(Diaries.self))
            .bind { [weak self] indexPath, item in

                guard let self = self else { return }

                        let item = self.listDiaryNew.value[indexPath.row]
                        let idDiary = item.id ?? 0
                        if Utils.checkLogin(selfView: self) {

                            Router.share.pushToViewDiaryDetail(tableView: UITableView.init(), array: self.listDiaryNew.value, indexPath: IndexPath.init(),row: indexPath.row, viewSelf: self, idFeed: idDiary, editFromView: .Home) { (diary) in
                                self.listDiaryNew.accept(diary)

                                let indexFind = self.listDiaryTop.firstIndex(where: { $0.id == diary[indexPath.row].id })
                                if indexFind != nil {
                                    var itemTemp = self.listDiaryTop
                                    itemTemp[indexFind!] = diary[indexPath.row]
                                    self.listDiaryTop = itemTemp
                                    self.pageViewDiary.reloadData()
                                }
                            }
                        }
        }.disposed(by: disposeBag)
//
//        Observable.zip(tbvDiaryNew.rx.itemSelected, tbvDiaryNew.rx.modelSelected(Diaries.self))
//            .bind { [weak self] indexPath, item in
//                guard let self = self else { return }
//                let idDiary = item.id ?? 0
//                if Utils.checkLogin(selfView: self) {
//                    Router.share.pushToViewDiaryDetail(tableView: UITableView.init(), array: self.listDiaryNew.value, indexPath: indexPath, row: indexPath.row, viewSelf: self, idFeed: idDiary, editFromView: .Home) { (diary) in
//                        self.listDiaryNew.accept(diary)
//                        self.getDate()
//                        self.tbvDiary.reloadData()
//                    }
//                }
//            }.disposed(by: disposeBag)
        
        Observable.zip(tbvDiary.rx.itemSelected, tbvDiary.rx.modelSelected(Diaries.self))
            .bind { [weak self] indexPath, item in
                guard let self = self else { return }
                
                let idDiary = item.id ?? 0
                if Utils.checkLogin(selfView: self) {
                    self.pushPostDetailWithObjectDiary(idPost: idDiary) { (numberLike, isLike, numberComment) in
                        
                        let itemTemp = self.listDiaryTip.value
                        
                        itemTemp[indexPath.row].likeNumber = numberLike
                        itemTemp[indexPath.row].isLiked = isLike
                        itemTemp[indexPath.row].commentNumber = numberComment
                        self.listDiaryTip.accept(itemTemp)
                    }
                }
                
        }.disposed(by: disposeBag)
        
        
        btnTipMore.rx.tap
            .subscribe { (_) in
                let postVC = ListDiaryViewController.init(nib: R.nib.listDiaryViewController)
                postVC.hidesBottomBarWhenPushed = true
                postVC.gettype = "3"
                postVC.title = "Mẹo cho bạn"
                Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
        }.disposed(by: disposeBag)
        
        btnNewMore.rx.tap
            .subscribe { (_) in
                let postVC = ListDiaryCLVViewController.init(nib: R.nib.listDiaryCLVViewController)
                postVC.hidesBottomBarWhenPushed = true
                postVC.title = "Nhật ký"
                postVC.type = "2"
                Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
        }.disposed(by: disposeBag)
    }
    
    //MARK: - Request API
    func getDate(){
        APIManager.getDate(callbackSuccess: { [weak self] (today , day_number, is_writed)  in
            guard let self = self else { return }
            let date = Date(timeIntervalSince1970: TimeInterval(today ))
            let istoday = Utils.dateToString(date: date, format: Key.DateFormat.DateFormatddMMyyyy)
            self.lblToDay.text = "\(istoday)"
            self.lblDate.text = "Bạn đã viết: \(day_number)/88 ngày"
            self.isFullDay = (Int(day_number) ?? 0 >= 88)
            self.dayWrite = Int(day_number) ?? 0
            self.setupProgressBar(day: Int(day_number) ?? 0)
            //  self.btnButttonAdd.isHidden = is_writed == 1 ? true : false
            self.isCheckWrite = is_writed == 1 ? true : false
            
            //            self.heightViewDateWrite.constant = 65
            //            self.viewDataWrite.isHidden = false
            //            self.view.layoutIfNeeded()
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func getBanner(){
        arrayImage = [String]()
        APIManager.getBanner(type: 1, callbackSuccess: { [weak self]  (banners) in
            guard let self = self else { return }
            for banner in banners {
                self.arrayImage.append(banner.link ?? "")
            }
            self.bannerView.arrayImage = self.arrayImage
            
            self.bannerView.tapBanner = { index in
                guard let url = URL.init(string: banners[index].linkAds ?? "") else {
                    return
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url , options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
            
            }, failed: { (error) in
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        })
    }
    
    func gethomeDiary(){
        LoaddingManager.startLoadding()
        APIManager.getDiaryHome(callbackSuccess: { [weak self] (homediary) in
            guard let self = self else { return }
            self.listDiaryHome = homediary
            
            print("ádadadasđ")
            if self.listDiaryHome.count > 0 {
                
                for (_,value) in self.listDiaryHome.enumerated() {
                    
                    if value.type == "2" {
                        self.listDiaryNew.accept(value.diaries ?? [])

//                        let array = Array((value.diaries ?? []).prefix(2))
//                        self.listDiaryNew.accept(array)
                        self.btnNewMore.isHidden = (value.diaries ?? []).count < 5
                    }
                    
                    if value.type == "1" {
                        self.listDiaryTop = value.diaries ?? []

                      //  self.listDiaryTop.accept(value.diaries ?? [])
                    }

                    if value.type == "3" {
                        self.listDiaryTip.accept(value.diaries ?? [])
                        self.btnTipMore.isHidden = (value.diaries ?? []).count < 5
                    }
                }
                self.pageViewDiary.reloadData()
            }

            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
                self.tbvDiary.reloadData()
            }
            
            self.scrollView.endRefreshing(at: .top)
            self.scrollView.finishInfiniteScroll(completion: { (collection) in
            })
            LoaddingManager.stopLoadding()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            LoaddingManager.stopLoadding()
        }
    }
    
//    func getObjectDiaryNews() -> DiaryHome?{
//        getDate()
//        let diaryNews = listDiaryHome.filter { (diary) -> Bool in
//            return diary.type == "2"
//        }
//        if diaryNews.count > 0 {
//            return diaryNews[0]
//        }
//        return nil
//    }
    
    func writeDiary (){
        if Utils.checkLogin(selfView: self) {
            let writeHome = WriteHomeViewController.init(nib: R.nib.writeHomeViewController)
            writeHome.hidesBottomBarWhenPushed = true
            writeHome.fromView = .Diary
            writeHome.checkComplete = { id in
                self.getCheckCompleteDiary(id: id ,isEdit: false, completion: { diary in
//guard let diaryNews = self.getObjectDiaryNews() else { return }
 //                   diaryNews.diaries?.insert(diary, at: 0)
                    
                    var listDiary = self.listDiaryNew.value
                    listDiary.insert(diary, at: 0)
                    self.listDiaryNew.accept(listDiary)
                    
                
                    NotificationCenter.default.post(name: NSNotification.Name.updateDiaryNew, object: ["DiaryNew": diary], userInfo: nil)
                    
                  //  self.dayWrite += 1
                  //  self.setupProgressBar(day: self.dayWrite)
                })
            }
            self.navigationController?.pushViewController(writeHome, animated: true)
        }
    }
    // MARK: - SetupUI
    func setupProgressBar(day:Int){
        
        widthProgressBar.constant = (CGFloat(day >= 88 ? 88 : day) * bGProgressBar.frame.width) / 88
        
        bGProgressBar.layer.cornerRadius = 8
        bGProgressBar.backgroundColor = .white
        bGProgressBar.layer.borderWidth = 1.0
        bGProgressBar.layer.borderColor = UIColor.init(hexString: "f5e08e")?.cgColor
        
        progressBar.layer.cornerRadius = 8
        progressBar.backgroundColor = UIColor.init(hexString: "ff996f")
    }
    
    func setupUI(){
        setupNavigation()
        self.extendedLayoutIncludesOpaqueBars = true
        isShowViewNoti = false
        tbvDiary.register(R.nib.diaryNewTableViewCell)
        //        tbvDiary.registerHeaderFooterView(R.nib.headerSectionTableViewCell)
        //        tbvDiary.delegate = self
        //        tbvDiary.dataSource = self
        tbvDiary.rx.setDelegate(self).disposed(by: disposeBag)
        tbvDiary.rowHeight = 143
        tbvDiary.separatorStyle = .none
        self.tbvDiary.isScrollEnabled = false
        scrollView.isScrollEnabled = true
        
        isFullDay = false
        
        btnButttonAdd.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: btnButttonAdd.frame.height / 2)
        scrollView.addPullToRefresh(refresher) {
            self.gethomeDiary()
            if Utils.checkLogin(selfView: self,showAlert: false){
                self.getDate()
            }
            self.scrollView.startRefreshing(at: .top)
        }
    }
    
    //    func showPopupWrite(){
    //        if Utils.checkLogin(selfView: self, showAlert: false){
    //            if UserDefaults.standard.bool(forKey: "tunrOnOff") == false {
    //                let vc = PopUpDiaryController.init(nibName: "PopUpDiaryController", bundle: nil)
    //                vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
    //                vc.writePopupDiary = { index in
    //                    if Utils.checkLogin(selfView: self){
    //                       self.writeDiary()
    //                    }
    //                }
    //                UserDefaults.standard.set(true, forKey: "tunrOnOff")
    //                self.present(vc, animated: true)
    //            }
    //        }
    //    }
    
    func setupNavigation(){
        let label = UILabel.init(frame: CGRect.init(x: -50, y: 0, width: 100, height: 40))
        label.text = R.string.localizable.diaryDiary()
        label.font = UIFont(name:"\(AppFont.NotoSansBold)", size: 24)
        label.textColor = UIColor.init(hexString: "0299fe")
        
        let leftBarButtonItem = UIBarButtonItem.menuButton(self, action: #selector(self.backToGuide),
                                                           imageName: R.image.ic_Back() ?? UIImage.init(),
                                                           height: 30, width: 40, view: self.view)
        
        switch self.openFrom {
        case .other:
            self.navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: label)]
        case .guide:
            self.navigationItem.leftBarButtonItems = [leftBarButtonItem, UIBarButtonItem.init(customView: label)]
        }
        
        self.navigationController?.navigationBar.isTranslucent = false
        let rightBarButtonSetting = UIBarButtonItem.menuButton(self, action: #selector(InfoViewController.tapSreach), imageName: R.image.ic_SearchLength() ?? UIImage.init(), height: 35, width: 150)
        self.navigationItem.rightBarButtonItems = [rightBarButtonSetting]
        
    }
    
    @objc func backToGuide(){
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Action tap Search
    @objc func tapSreach(){
        if Utils.checkLogin(selfView: self) {
            let homeVC = HomeInfo.init(nib: R.nib.homeInfo)
            homeVC.indext = 1
            homeVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
    func reloadTableView() {
        let indexPaths = tbvDiary.indexPathsForSelectedRows
        tbvDiary.reloadData()
        for path in indexPaths ?? [] {
            tbvDiary.selectRow(at: path, animated: false, scrollPosition: .none)
        }
    }
    
    //MARK: - Action Add
    @IBAction func didPressAdd(_ sender: Any) {
        writeDiary()
    }
    
    @IBAction func didPressDiaryHot(_ sender: Any) {
        let postVC = ListDiaryCLVViewController.init(nib: R.nib.listDiaryCLVViewController)
        postVC.hidesBottomBarWhenPushed = true
        postVC.type = "1"
        postVC.title = "Nhật ký"
        Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
    }
    
    //MARK: - deinit
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.checkEditDiary, object: nil)
        self.tbvDiary?.removeObserver(self, forKeyPath: "contentSize")  
    }
}

//MARK: - UITableview Delegate
extension DiaryViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.cellHeightsDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height =  self.cellHeightsDictionary[indexPath] {
            return height
        }
        return UITableView.automaticDimension
    }
}

//MARK: FSPagerViewDelegate
extension DiaryViewController: FSPagerViewDataSource,FSPagerViewDelegate{
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.listDiaryTop.count
    }

    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "DiaryNewCollectionViewCell", at: index) as! DiaryNewCollectionViewCell

        cell.contentView.layer.shadowRadius = 0
        cell.contentView.layer.shadowOpacity = 0

        cell.setupData(data: self.listDiaryTop[index])

        return cell
    }

    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        let item = self.listDiaryTop[index]
        let idDiary = item.id ?? 0
        if Utils.checkLogin(selfView: self) {

            Router.share.pushToViewDiaryDetail(tableView: UITableView.init(), array: self.listDiaryTop, indexPath: IndexPath.init(),row: index, viewSelf: self, idFeed: idDiary, editFromView: .Home) { (diary) in
                self.listDiaryTop = diary
                self.pageViewDiary.reloadData()

                let indexFind = self.listDiaryNew.value.firstIndex(where: { $0.id == diary[index].id })
                if indexFind != nil {
                    var itemTemp = self.listDiaryNew.value
                    itemTemp[indexFind!] = diary[index]
                    self.listDiaryNew.accept(itemTemp)
                }
            }
        }
    }
}

