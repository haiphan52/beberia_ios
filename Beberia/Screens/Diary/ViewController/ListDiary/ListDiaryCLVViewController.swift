//
//  ListDiaryCLVViewController.swift
//  Beberia
//
//  Created by IMAC on 9/4/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD
import RxDataSources

struct MenuSectionCLV {
    var header: String
    var items: [T]
}

extension Diaries: IdentifiableType {
    public var identity: Int {
        return id!
    }
}

extension Diaries: Equatable {
    public static func == (lhs: Diaries, rhs: Diaries) -> Bool {
        return (lhs.identity) == (rhs.identity)
    }
}

extension MenuSectionCLV: AnimatableSectionModelType{
    typealias T = Diaries
    
    var identity: String{
        return header
    }
    
    init(original: Self, items: [Self.T]) {
        self = original
        self.items = items
    }
}

class ListDiaryCLVViewController: BaseViewController {
    
    @IBOutlet weak var clvDiary: UICollectionView!
    
    let sections = BehaviorRelay.init(value: [MenuSectionCLV]())
    var dataSource:RxCollectionViewSectionedAnimatedDataSource<MenuSectionCLV>?
    var listDiaryNew = BehaviorRelay.init(value: [Diaries]())
    var idDiary = 0
    var page = 1
    var type = "1"
    var isCheckLoadding = false
    let heightImageClv = (UIScreen.main.bounds.width / 2) - 32 + 156
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoaddingManager.initLoadding(view: self.view)
        customLeftBarButton()
        initRX()
        self.getDiaryMore(page: self.page)
    }
    
    
    private func initRX(){
        // MARK: // init CollectionView
        let flowLayoutAfterBefore = UICollectionViewFlowLayout()
        flowLayoutAfterBefore.itemSize = CGSize(width: (UIScreen.main.bounds.width / 2) - 8, height: heightImageClv)
        flowLayoutAfterBefore.scrollDirection = .vertical
        flowLayoutAfterBefore.minimumInteritemSpacing = 0
        flowLayoutAfterBefore.minimumLineSpacing = 0
        flowLayoutAfterBefore.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvDiary.setCollectionViewLayout(flowLayoutAfterBefore, animated: true)
        clvDiary.showsHorizontalScrollIndicator = false
        clvDiary.register(R.nib.diaryNewCollectionViewCell)
        
        clvDiary.addInfiniteScroll { (tableView) in
            if self.page != 0{
              //  self.page += 1
                self.getDiaryMore(page: self.page)
            }else{
                // self.tableView.endRefreshing(at: .top)
                tableView.finishInfiniteScroll(completion: { (collection) in
                })
            }
        }
        
//        listDiaryNew.asObservable().bind(to: self.clvDiary.rx.items(cellIdentifier: R.reuseIdentifier.diaryNewCollectionViewCell.identifier, cellType: DiaryNewCollectionViewCell.self)) { [weak self]  row, data, cell in
//            guard let _ = self else { return }
//
//            cell.setupData(data: data)
//            cell.contentView.layer.shadowRadius = 0
//            cell.contentView.layer.shadowOpacity = 0
//
//        }.disposed(by: disposeBag)

        // bind to data TableView
        let dataSource = RxCollectionViewSectionedAnimatedDataSource<MenuSectionCLV>(
            
            configureCell: { ds, tv, ip, item in
                let cell = tv.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.diaryNewCollectionViewCell.identifier, for: ip)
                    as! DiaryNewCollectionViewCell
                cell.setupData(data: item)
                            cell.contentView.layer.shadowRadius = 0
                            cell.contentView.layer.shadowOpacity = 0
                return cell
        })
       
        self.dataSource = dataSource
        
        sections
            .bind(to: clvDiary.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        Observable.zip(clvDiary.rx.modelSelected(Diaries.self), clvDiary.rx.itemSelected).subscribe { (diary, item) in
            if Utils.checkLogin(selfView: self) {
                guard let selfView = Utils.getTopMostViewController() else {
                    return
                }
                Router.share.pushToViewDiaryDetail(tableView: UITableView.init(), array: self.listDiaryNew.value, indexPath: IndexPath.init(),row: item.row, viewSelf: selfView, idFeed: diary.id ?? 0, editFromView: .Profile) { (diary) in
                    self.listDiaryNew.accept(diary)
                }
            }
        }.disposed(by: disposeBag)
    }
}

extension ListDiaryCLVViewController {
    
    func getDiaryMore(page:Int){
        if !isCheckLoadding{
            LoaddingManager.startLoadding()
        }
        
        APIManager.getListDiaryMore(type: type,page: page, id: idDiary, callbackSuccess: { [weak self] (diarysMore, next_page) in
            guard let self = self else { return }
            if self.page != 0 {
                var listDiaryNewTemp = self.listDiaryNew.value
                listDiaryNewTemp.append(contentsOf: diarysMore)
                self.listDiaryNew.accept(listDiaryNewTemp)
                self.sections.accept([MenuSectionCLV.init(header: "", items: self.listDiaryNew.value)])
            }
            self.page = next_page
            self.isCheckLoadding = self.page != 1
            
            self.clvDiary.endRefreshing(at: .top)
            self.clvDiary.finishInfiniteScroll(completion: { (collection) in
            })
            LoaddingManager.stopLoadding()
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            LoaddingManager.stopLoadding()
        }
    }
}
