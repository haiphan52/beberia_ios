//
//  ListDiaryViewController.swift
//  Beberia
//
//  Created by iMAC on 9/5/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import PullToRefresh
class ListDiaryViewController: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tbvListDiary: UITableView!
    
    // MARK: - Properties
    
    var listDiaryMore = [Diaries]()
    var gettype = ""
    var page = 1
    var nextpage = 0
    var id: Int? = nil
    let refresher = PullToRefresh()
    var isLoadingMore = false
    var cellHeightsDictionary: [IndexPath: CGFloat] = [:]
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoaddingManager.initLoadding(view: self.view)
        setupUI()
        getDiaryMore(type: gettype, page: page)
        NotificationCenter.default.addObserver(self, selector: #selector(checkEditDiaryProfile), name: NSNotification.Name.checkEditDiaryProfile, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateDiaryNew), name: NSNotification.Name.updateDiaryNew, object: nil)
    }

    //MARK: - Helper methods
    
    @objc private func updateDiaryNew(notification: NSNotification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        guard let homeFeedNew = object["DiaryNew"] as? Diaries else{
            return
        }
        self.listDiaryMore.insert(homeFeedNew, at: 0)
        self.tbvListDiary.reloadData()
    }
    
    @objc func checkEditDiaryProfile(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        let idFeed = object[Key.KeyNotification.idDiary] as? Int
        self.getCheckCompleteDiary(id: idFeed ?? 0, completion: { diary in
            let index = self.listDiaryMore.firstIndex(where: { (diaryEdit) -> Bool in
                return diary.id == diaryEdit.id
            })
            if index != nil{
                self.listDiaryMore[index!] = diary
            }
            self.tbvListDiary.reloadData()
        })
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        customLeftBarButton()
        tbvListDiary.register(R.nib.diaryNewTableViewCell)
        tbvListDiary.register(R.nib.tipTableViewCell)
        tbvListDiary.delegate = self
        tbvListDiary.dataSource = self
        tbvListDiary.separatorStyle = .none
        tbvListDiary.rowHeight = 143
        tbvListDiary.infiniteScrollTriggerOffset = 700
        tbvListDiary.showsVerticalScrollIndicator = false
        tbvListDiary.addInfiniteScroll { (tableView) in
            tableView.performBatchUpdates({ () -> Void in
                // update collection view
                self.isLoadingMore = true
                if self.nextpage > 0 {
                    self.page = self.page + 1
                    self.getDiaryMore(type: self.gettype, page: self.page)
                }else{
//                    self.tbvListDiary.finishInfiniteScroll(completion: { (collection) in
//                    })
                    tableView.finishInfiniteScroll()
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                tableView.finishInfiniteScroll()
            });
            
        }

        tbvListDiary.addPullToRefresh(refresher) {
            self.isLoadingMore = true
            self.page = 1
            self.getDiaryMore(type: self.gettype, page: self.page)
            self.tbvListDiary.startRefreshing(at: .top)
        }
        
    }
    
    // MARK: - Request API
    
    /// Get list Diary for type
    /// - Parameters:
    ///   - type: type 1 is hot, 2 is newest, 3 is guide (meo), 4 my diary, 5 news (tin hot trong info) (String)
    ///   - page: Page Number (Int)
    
    func getDiaryMore(type: String, page:Int){
        if !isLoadingMore{
            LoaddingManager.startLoadding()
        }
        APIManager.getListDiaryMore(type: type,page: page, id: id, callbackSuccess: { [weak self] (diarysMore, next_page) in
            guard let self = self else { return }
            self.nextpage = next_page
            if self.page == 1{
                self.listDiaryMore.removeAll()
            }
            if self.listDiaryMore.count == 0 {
                self.listDiaryMore = diarysMore
            }else{
                self.listDiaryMore.append(contentsOf: diarysMore)
            }
            self.tbvListDiary.reloadData()
            self.tbvListDiary.endRefreshing(at: .top)
//            self.tbvListDiary.finishInfiniteScroll()
//            self.tbvListDiary.finishInfiniteScroll(completion: { (collection) in
//            })
            LoaddingManager.stopLoadding()
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            LoaddingManager.stopLoadding()
        }
    }
    
    // MARK: - Deinit
    
    deinit {
        print("Remove NotificationCenter Deinit")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.checkEditDiaryProfile, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.updateDiaryNew, object: nil)
    }
    
    
}

// MARK: - UITableViewDelegate

extension ListDiaryViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDiaryMore.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if gettype == "3"{
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.tipTableViewCell, for: indexPath)!
            cell.setupDataMore(listDiaryMore: self.listDiaryMore[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.diaryNewTableViewCell, for: indexPath)!
            cell.setupDataMore(listDiaryMore: self.listDiaryMore[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // return DiaryNewTableViewCell.heightRow
        
        return 143
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utils.checkLogin(selfView: self) {
            Router.share.pushToViewDiaryDetail(tableView: tableView, array: self.listDiaryMore, indexPath: indexPath,row: indexPath.row, viewSelf: Utils.getTopMostViewController()!, idFeed: self.listDiaryMore[indexPath.row].id ?? 0, editFromView: .Profile) { (diary) in
                self.listDiaryMore = diary
                self.tbvListDiary.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
          self.cellHeightsDictionary[indexPath] = cell.frame.size.height
      }
      
      func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
          if let height =  self.cellHeightsDictionary[indexPath] {
              return height
          }
          return 143
      }
}
