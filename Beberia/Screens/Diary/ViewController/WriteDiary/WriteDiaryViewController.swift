//
//  WriteDiaryViewController.swift
//  Beberia
//
//  Created by IMAC on 8/26/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class WriteDiaryViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tbvDiary: UITableView!
    @IBOutlet weak var tfTitleDiary: UITextField!
    @IBOutlet weak var btnPrivate: UIButton!
    @IBOutlet weak var viewStyleText : UIView!
    @IBOutlet weak var viewPrivate: UIView!
    @IBOutlet weak var constraintBottomViewStyleText: NSLayoutConstraint!
    
    // MARK: - Properties
    
    var checkCompleteDiary: (_ id:Int) -> () = {_ in}
    var frameOrigin = CGRect()
    var tapEndEdit: UITapGestureRecognizer!
    var viewSelectStyleText: ViewOptionStyleText = ViewOptionStyleText.instanceFromNib()
    var isEnableI = false
    var isEnableU = false
    var isEnableB = false
    var itemPosts = [ItemPost]()
    var cell: WriteHomeTableViewCell?
    var privateInt = 0
    var idDiary = 0
    var isPrivate = false
    {
        didSet{
            btnPrivate.setImage(UIImage.init(named: !isPrivate ? "ic_untickBox" : "ic_tickBox"), for: .normal)
            privateInt = isPrivate ? 1 : 0
        }
    }
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        customLeftBarButton()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        tapEndEdit = UITapGestureRecognizer.init(target: self, action: #selector(WriteDiaryViewController.endEditting(_:)))
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        self.title = R.string.localizable.diaryTilteWrite()
//        tbvDiary.register(UINib(nibName: "WriteHomeTableViewCell", bundle: nil), forCellReuseIdentifier: "WriteHomeTableViewCell")
        tbvDiary.register(R.nib.writeHomeTableViewCell)
        tbvDiary.delegate = self
        tbvDiary.dataSource = self
        tbvDiary.separatorStyle = .none
        
//        let rightBarButtonItem = UIBarButtonItem.menuButton(self, action: #selector(WriteDiaryViewController.createPost), imageName: "heart", height: 20, width: 12)
//        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        tfTitleDiary.autocorrectionType = .no
        viewSelectStyleText.frame = viewStyleText.bounds
        viewStyleText.addSubview(viewSelectStyleText)
        viewStyleText.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 5)
        let itemPost = ItemPost()
        itemPost.image = nil
        itemPost.content = NSMutableAttributedString.init(string: "")
        self.itemPosts.append(itemPost)
        viewSelectStyleText.tapTextI = {
            self.isEnableI = !self.isEnableI
            self.cell?.tvContent.formatItalicSeletedText()
        }
        
        viewSelectStyleText.tapTextU = {
            self.isEnableU = !self.isEnableU
            self.cell?.tvContent.formatUnderlinedSeletedText()
        }
        
        viewSelectStyleText.tapTextB = {
            self.isEnableB = !self.isEnableB
            self.cell?.tvContent.formatBoldSeletedText()
        }
        
        viewSelectStyleText.tapSelectImage = { images in
            self.viewSelectStyleText.isSelectI = false
            self.viewSelectStyleText.isSelectU = false
            self.viewSelectStyleText.isSelectB = false
            // adđ object new
            let itemPost = ItemPost()
            itemPost.image = images[0].0
            itemPost.content = NSMutableAttributedString.init(string: "")
            self.itemPosts.append(itemPost)
            self.tbvDiary.beginUpdates()
            self.tbvDiary.insertRows(at: [IndexPath.init(row: self.itemPosts.count - 1, section: 0)], with: .automatic)
            self.tbvDiary.endUpdates()
            self.tbvDiary.scrollToRow(at: IndexPath.init(row: self.itemPosts.count - 1, section: 0), at: .none, animated: false)
            // kepp status style text
            self.viewSelectStyleText.isSelectI = self.isEnableI
            self.viewSelectStyleText.isSelectU = self.isEnableU
            self.viewSelectStyleText.isSelectB = self.isEnableB
            
            if self.isEnableI{
                self.cell?.tvContent.isItalicEnabled = false
                self.cell?.tvContent.formatItalicSeletedText()
            }
            
            if self.isEnableU{
                self.cell?.tvContent.isUnderlineEnabled = false
                self.cell?.tvContent.formatUnderlinedSeletedText()
            }
            
            if self.isEnableB{
                self.cell?.tvContent.isBoldEnabled = false
                self.cell?.tvContent.formatBoldSeletedText()
            }
        }
        frameOrigin = viewStyleText.frame
    }

    @IBAction func didPressPrivate(_ sender: Any) {
        isPrivate = !isPrivate
    }
    
    @objc func createPost(){
     //  createDiary()
    }
    
//    func createDiary(){
//        guard let title = tfTitleDiary.text, title.count > 0 else { return }
//        convertAttribuiteStringToHtml()
//        SVProgressHUD.show()
//        APIManager.createDiary(title: title, content: itemPosts[0].contentHtml ?? "" , images: createJsonImage(), isPrivate: privateInt , callbackSuccess: { (homeFeed) in
//            SVProgressHUD.dismiss()
//            print(homeFeed)
//            self.idDiary = homeFeed.id ?? 0
//            self.navigationController?.popViewController(animated: true)
//            self.checkCompleteDiary(self.idDiary)
//        }) { (error) in
//            SVProgressHUD.dismiss()
//            Utils.showAlertView(controller: self, title: "Error", message: error)
//        }
//    }
    
    func createJsonImage()->String{
        if itemPosts.count > 0{
            var params = [Parameters]()
            for (index,item) in itemPosts.enumerated(){
                if index > 0{
                    let imageResize = (item.image?.resizeImage(CGSize.init(width: 150, height: 100)))!
                    /* get size image
                     let imgData = NSData(data: (imageResize).jpegData(compressionQuality: 1)!)
                     let imageSize: Int = imgData.count
                     print("size of image in KB: %f ", Double(imageSize) / 1000.0)
                     */
                    var stringBase64: String = ""
                    if let base64 = imageResize.convertImageTobase64(format: .jpeg(1), image: imageResize) {
                        stringBase64 = Key.HeaderJsonImage.HeaderJsonImage + base64
                    }
                    
                    let param = self.createDic(content: item.contentHtml ?? "", base64: stringBase64)
                    params.append(param)
                }
            }
            let jsonEncode = Utils.json(from: params)
            return jsonEncode ?? ""
        }
        return ""
    }
    
    @objc func endEditting(_ sender: UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    @objc func keyboardWasShown(notification: NSNotification) {
        self.tbvDiary.addGestureRecognizer(tapEndEdit)
        frameOrigin = viewStyleText.frame
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        UIView.animate(withDuration: 0.25) {
            self.constraintBottomViewStyleText.constant = (-keyboardRect!.height) + 34
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.tbvDiary.removeGestureRecognizer(tapEndEdit)
        print(notification)
        UIView.animate(withDuration: 0.25) {
            self.viewStyleText.frame.origin.y =  self.frameOrigin.origin.y
            self.constraintBottomViewStyleText.constant = 0
        }
    }
    
    func createDic(content: String, base64: String) -> Parameters{
        var param = Parameters()
        param["content"] = content
        param["base64"] = base64
        
        return param
    }
    
    func convertAttribuiteStringToHtml(){
        for item in itemPosts{
            item.contentHtml = item.content?.generateHtmlString()
        }
    }
    
    func getHeightTextView(text: String, cell: WriteHomeTableViewCell){
        let height = text.height(withConstrainedWidth: cell.tvContent.frame.width - 10, font: UIFont.systemFont(ofSize: 14))
        cell.heightTextView.constant = height + 20
        cell.layoutIfNeeded()
    }
    
    func getHeightAttributedString(text: NSAttributedString, cell: WriteHomeTableViewCell){
        let height = text.height(containerWidth: cell.tvContent.frame.width - 10)
        cell.heightTextView.constant = height + 20
        cell.layoutIfNeeded()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

//MARK: - UITableview Delegate
extension WriteDiaryViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.itemPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "WriteHomeTableViewCell", for: indexPath) as! WriteHomeTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.writeHomeTableViewCell, for: indexPath)!
        cell.btnDelete.tag = indexPath.row
        self.cell = cell
        if indexPath.row == 0{
            cell.heightImage.constant = 0
            cell.tvContent.attributedText = self.itemPosts[0].content ?? NSAttributedString.init(string: "")
            cell.btnDelete.isHidden = true
            self.getHeightAttributedString(text: self.itemPosts[indexPath.row].content ?? NSMutableAttributedString.init(string: ""), cell: cell)
        }else{
            if let img = self.itemPosts[indexPath.row].image {
                cell.btnDelete.isHidden = false
                print(img.getCropRatio())
                cell.heightImage.constant = cell.frame.width / img.getCropRatio()
                cell.imgPost?.image = img
            }
            self.getHeightAttributedString(text: self.itemPosts[indexPath.row].content ?? NSMutableAttributedString.init(string: ""), cell: cell)
            self.getHeightTextView(text: self.itemPosts[indexPath.row].content?.string ?? "", cell: cell)
        }
        
        cell.tapDeleteImage = { index, sender in
            if let cell = sender.superview?.superview as? WriteHomeTableViewCell{
                let indexPath = self.tbvDiary.indexPath(for: cell)
                // Now delete the table cell
                let index = indexPath?.row ?? 0
                if index > 0 {
                    self.itemPosts[index - 1].content?.append(self.itemPosts[index].content ?? NSMutableAttributedString.init(string: ""))
                    self.itemPosts.remove(at: index)
                    self.tbvDiary.beginUpdates()
                    self.tbvDiary.deleteRows(at: [IndexPath.init(row: index - 1, section: 0)], with: .none)
                    self.tbvDiary.endUpdates()
                    self.tbvDiary.reloadRows(at: [IndexPath.init(row: index - 1, section: 0)], with: .none)
                }
            }
        }
        
        cell.tvContent.checkAttribuite = { isEnableBold, isEnableItalic, isEnableUtralic in
            self.viewSelectStyleText.isSelectI = isEnableItalic
            self.viewSelectStyleText.isSelectU = isEnableUtralic
            self.viewSelectStyleText.isSelectB = isEnableBold
        }
        
        cell.updateHeightOfRow = { textView in
            self.itemPosts[cell.btnDelete.tag].content = NSMutableAttributedString.init(attributedString: textView.attributedText)
            cell.tvContent.attributedText = self.itemPosts[cell.btnDelete.tag].content
            
            // update height textview
            self.getHeightAttributedString(text: cell.tvContent.attributedText, cell: cell)
            
            UIView.setAnimationsEnabled(false)
            self.tbvDiary.beginUpdates()
            self.tbvDiary.endUpdates()
            UIView.setAnimationsEnabled(true)
            // Scoll up your textview if required
            if let thisIndexPath = self.tbvDiary.indexPath(for: cell) {
                self.tbvDiary.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

