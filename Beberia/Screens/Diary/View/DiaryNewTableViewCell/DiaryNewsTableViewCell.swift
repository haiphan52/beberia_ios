//
//  DiaryNewTableViewCell.swift
//  Beberia
//
//  Created by Lap on 12/19/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class DiaryNewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgAvatarUser: UIImageView!
    @IBOutlet weak var nameUserLable: UILabel!
    @IBOutlet weak var imgDiary: UIImageView!
    @IBOutlet weak var contentDiaryLable: UILabel!
    @IBOutlet weak var numberLikeLable: UILabel!
    @IBOutlet weak var numberCommentLable: UILabel!
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var imgLike: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupData(data: Diaries){
       imgDiary.image = UIImage.init(named: Key.ImagePlaceholder)
       if data.media?.count ?? 0 > 0 {
           imgDiary.kf.setImage(with: URL(string: data.media?[0].link ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
           imgDiary.isHidden = false
       }
       imgLike.image = R.image.icLikePink()
       if data.isLiked == 1 {
           imgLike.image = R.image.icLikedPink()
       }
       contentDiaryLable.text = data.title
       nameUserLable.text = "\(data.user_create?.displayName ?? "")"
       numberLikeLable.text  = "\(data.likeNumber ?? 0)"
       numberCommentLable.text = "\(data.commentNumber ?? 0)"
       imgAvatarUser.kf.setImage(with: URL(string: data.user_create?.avatar ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
       
       let date = Date(timeIntervalSince1970: TimeInterval(data.created ?? 0))
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyy
       timeLable.text = date.timeAgoDisplayHomeFeed()
   }
    
}
