//
//  TipTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 10/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class TipTableViewCell: UITableViewCell {
    
       @IBOutlet weak var lblNumberComment: UILabel!
       @IBOutlet weak var lnlNumberLike: UILabel!
       @IBOutlet weak var lblNameCreate: UILabel!
       @IBOutlet weak var lblContentDiary: UILabel!
       @IBOutlet weak var imgDiary: UIImageView!
       @IBOutlet weak var imgLike: UIImageView!
       @IBOutlet weak var viewContainer: UIView!
       @IBOutlet weak var lblDateCreate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewContainer.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.2, radius: 10)
        imgDiary.roundCorners([.topRight, .bottomRight], radius: 10)
    }

        func setupDataMore(listDiaryMore: Diaries){
        
            imgDiary.isHidden = true
            imgDiary.image = UIImage.init(named: Key.ImagePlaceholder)
            if listDiaryMore.media?.count ?? 0 > 0 {
                imgDiary.kf.setImage(with: URL(string: listDiaryMore.media?[0].link ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
               
                imgDiary.isHidden = false
            }
            imgLike.image = R.image.icLikePink()
            if listDiaryMore.isLiked == 1 {
                imgLike.image = R.image.icLikedPink()
            }
            lblNameCreate.text = listDiaryMore.title
            lblContentDiary.text = listDiaryMore.quote ?? ""
          //  let name = listDiaryMore.user_create?.displayName ?? ""
          
            lnlNumberLike.text  = "\(listDiaryMore.likeNumber ?? 0)"
            lblNumberComment.text = "\(listDiaryMore.commentNumber ?? 0)"
          
            let date = Date(timeIntervalSince1970: TimeInterval(listDiaryMore.created ?? 0))

            lblDateCreate.text = date.timeAgoDisplayHomeFeed()
           
        }
    
}
