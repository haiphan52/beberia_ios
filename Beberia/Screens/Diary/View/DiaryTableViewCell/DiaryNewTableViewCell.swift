//
//  DiaryNewTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 11/12/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class DiaryNewTableViewCell: UITableViewCell {

    // MARK: - Outlets
    

    @IBOutlet weak var lblNumberComment: UILabel!
    @IBOutlet weak var lnlNumberLike: UILabel!
    @IBOutlet weak var lblNameCreate: UILabel!
    @IBOutlet weak var imgUserCreate: UIImageView!
    @IBOutlet weak var lblContentDiary: UILabel!
    @IBOutlet weak var imgDiary: UIImageView!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblDateCreate: UILabel!

    
    // MARK: - Properties
    
    static let heightRow:CGFloat = 125
    static let heightRowWithButton:CGFloat = 155
    
    var tapViewMore: (_ index :Int) -> () = {_ in}
    var tapDeleteAlert: (_ index :Int) -> () = {_ in}
    
    // MARK: - View Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
       
        imgUserCreate.cornerRadius = imgUserCreate.frame.height / 2
        viewContainer.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.2, radius: 10)
        imgDiary.roundCorners([.topRight, .bottomRight], radius: 10)
    }
    
    // MARK: - SetupData
    
    func setupData(listDiarys: DiaryHome,row:Int,totalRows:Int){
   
        imgDiary.isHidden = true
        
        imgDiary.image = UIImage.init(named: Key.ImagePlaceholder)
        if listDiarys.diaries?[row].media?.count ?? 0 > 0 {
         //   imgDiary.kf.setImage(with: URL(string: listDiarys.diaries?[row].media?[0].link ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
            imgDiary.setImage(imageString: listDiarys.diaries?[row].media?[0].link ?? "")
            imgDiary.isHidden = false
        }
        imgLike.image = R.image.icLikePink()
        if listDiarys.diaries?[row].isLiked == 1 {
            imgLike.image = R.image.icLikedPink()
        }
        lblContentDiary.text = listDiarys.diaries?[row].title
        let name = listDiarys.diaries?[row].user_create?.displayName ?? ""
        lblNameCreate.text = name == "beberia_admin" ? "" : name
        lnlNumberLike.text  = "\(listDiarys.diaries?[row].likeNumber ?? 0)"
        lblNumberComment.text = "\(listDiarys.diaries?[row].commentNumber ?? 0)"
      //  imgUserCreate.kf.setImage(with: URL(string: listDiarys.diaries?[row].user_create?.avatar ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        imgDiary.setImage(imageString: listDiarys.diaries?[row].user_create?.avatar ?? "")
        
        let date = Date(timeIntervalSince1970: TimeInterval(listDiarys.diaries?[row].created ?? 0))
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyy
        lblDateCreate.text = date.timeAgoDisplay()
        
        if listDiarys.is_load_more ?? 0 == 1 {
            if row == totalRows - 1 {
              
            }
        }
    }
    func setupDataMore(listDiaryMore: Diaries){
    
        imgDiary.isHidden = true
        imgDiary.image = UIImage.init(named: Key.ImagePlaceholder)
        if listDiaryMore.media?.count ?? 0 > 0 {
         //   imgDiary.kf.setImage(with: URL(string: listDiaryMore.media?[0].link ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
            imgDiary.setImage(imageString: listDiaryMore.media?[0].link ?? "")
            imgDiary.isHidden = false
        }
        imgLike.image = R.image.ic_like()
        if listDiaryMore.isLiked == 1 {
            imgLike.image = R.image.icLikeActive()
        }
        lblContentDiary.text = listDiaryMore.title
        let name = listDiaryMore.user_create?.displayName ?? ""
        lblNameCreate.text = name == "beberia_admin" ? "" : name
        lnlNumberLike.text  = "\(listDiaryMore.likeNumber ?? 0)"
        lblNumberComment.text = "\(listDiaryMore.commentNumber ?? 0)"
       // imgUserCreate.kf.setImage(with: URL(string: listDiaryMore.user_create?.avatar ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        imgUserCreate.setImage(imageString: listDiaryMore.user_create?.avatar ?? "")
        let date = Date(timeIntervalSince1970: TimeInterval(listDiaryMore.created ?? 0))
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyy
        lblDateCreate.text = date.timeAgoDisplayHomeFeed()
       
    }
    
    // MARK: - Action
    
    @IBAction func diidPressViewMore(_ sender: Any) {
      //  tapViewMore(btnViewMore.tag)
    }
    
    @IBAction func didPressShowAlert(_ sender: Any) {
      //  tapDeleteAlert(btnDelete.tag)
    }
}
