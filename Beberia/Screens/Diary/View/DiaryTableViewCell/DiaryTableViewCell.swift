//
//  DiaryTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 8/26/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class DiaryTableViewCell: UITableViewCell {
    
    // MARK: - Outlets

    @IBOutlet weak var btnViewMore: UIButton!
    @IBOutlet weak var lblNumberComment: UILabel!
    @IBOutlet weak var lnlNumberLike: UILabel!
    @IBOutlet weak var lblNameCreate: UILabel!
    @IBOutlet weak var imgUserCreate: UIImageView!
    @IBOutlet weak var lblContentDiary: UILabel!
    @IBOutlet weak var imgDiary: UIImageView!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var imgDelete: UIImageView!
    @IBOutlet weak var widthBtnOption: NSLayoutConstraint!
    
     // MARK: - Properties
    
    static let heightRow:CGFloat = 110
    static let heightRowWithButton:CGFloat = 130
    
    var tapViewMore: (_ index :Int) -> () = {_ in}
    var tapDeleteAlert: (_ index :Int) -> () = {_ in}
    
    // MARK: - View Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgDiary.cornerRadius = 5
        btnViewMore.cornerRadius = 12
        imgUserCreate.cornerRadius = imgUserCreate.frame.height / 2
    }
    
    // MARK: - SetupData
    
    func setupData(listDiarys: DiaryHome,row:Int,totalRows:Int){
        widthBtnOption.constant = 0
        imgDelete.isHidden = true
        btnDelete.isHidden = true
        btnViewMore.isHidden = true
        btnViewMore.isHidden = true
        imgDiary.image = UIImage.init(named: Key.ImagePlaceholder)
        if listDiarys.diaries?[row].media?.count ?? 0 > 0 {
             imgDiary.kf.setImage(with: URL(string: listDiarys.diaries?[row].media?[0].link ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        }
        imgLike.image = R.image.ic_like()
        if listDiarys.diaries?[row].isLiked == 1 {
            imgLike.image = R.image.icLikeActive()
        }
        lblContentDiary.text = listDiarys.diaries?[row].title
        lblNameCreate.text = "\(listDiarys.diaries?[row].user_create?.displayName ?? "")"
        lnlNumberLike.text  = "\(listDiarys.diaries?[row].likeNumber ?? 0)"
        lblNumberComment.text = "\(listDiarys.diaries?[row].commentNumber ?? 0)"
        imgUserCreate.kf.setImage(with: URL(string: listDiarys.diaries?[row].user_create?.avatar ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))

        if listDiarys.is_load_more ?? 0 == 1 {
            if row == totalRows - 1 {
                btnViewMore.isHidden = false
            }
        }
    }
    func setupDataMore(listDiaryMore: Diaries){
        widthBtnOption.constant = 0
        imgDelete.isHidden = true
        btnDelete.isHidden = true
        imgDiary.image = UIImage.init(named: Key.ImagePlaceholder)
        if listDiaryMore.media?.count ?? 0 > 0 {
             imgDiary.kf.setImage(with: URL(string: listDiaryMore.media?[0].link ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        }
        imgLike.image = R.image.ic_like()
        if listDiaryMore.isLiked == 1 {
            imgLike.image = R.image.icLikeActive()
        }
        lblContentDiary.text = listDiaryMore.title
        lblNameCreate.text = listDiaryMore.user_create?.displayName ?? ""
        lnlNumberLike.text  = "\(listDiaryMore.likeNumber ?? 0)"
        lblNumberComment.text = "\(listDiaryMore.commentNumber ?? 0)"
        imgUserCreate.kf.setImage(with: URL(string: listDiaryMore.user_create?.avatar ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        btnViewMore.isHidden = true
    }
    
    // MARK: - Action
    
    @IBAction func diidPressViewMore(_ sender: Any) {
        tapViewMore(btnViewMore.tag)
    }
    
    @IBAction func didPressShowAlert(_ sender: Any) {
        tapDeleteAlert(btnDelete.tag)
    }
}
