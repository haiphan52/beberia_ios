//
//  IntroViewController.swift
//  Beberia
//
//  Created by IMAC on 12/17/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum Pages: Int, CaseIterable {
    case pageZero
    case pageOne
    case pageTwo
    case pageThree
    case pageFour
    case pageFive
    
    var name: String {
        switch self {
        case .pageZero:
            return "Chat nhóm - Theo khu vực, theo độ tuổi con, theo độ tuổi mẹ, theo chủ đề"
        case .pageOne:
            return "Diễn đàn - hỏi đáp về các vấn đề mẹ và bé, có bác sĩ Beberia tư vấn MIỄN PHÍ"
        case .pageTwo:
            return "Nhật ký - nơi lưu lại những khoảnh khắc của bé yêu bằng những khung hình xinh xắn"
        case .pageThree:
            return "Thông tin \n Cẩm nang quý giá"
        case .pageFour:
            return "Menu - trợ lý không thể thiếu của mẹ "
        case .pageFive:
            return "Menu - trợ lý không thể thiếu của mẹ "
        }
    }
    
    var imageIntro: String {
        switch self {
        case .pageZero:
            return "img_intro_1"
        case .pageOne:
            return "img_intro_2"
        case .pageTwo:
            return "img_intro_3"
        case .pageThree:
            return "img_intro_4"
        case .pageFour:
            return "img_intro_5"
        case .pageFive:
            return "img_intro_6"
        }
    }
    
    var index: Int {
        switch self {
        case .pageZero:
            return 0
        case .pageOne:
            return 1
        case .pageTwo:
            return 2
        case .pageThree:
            return 3
        case .pageFour:
            return 4
        case .pageFive:
            return 5
        }
    }
}


class IntroViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btNext: UIButton!
    private let disposeBag = DisposeBag()
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

}
extension IntroViewController {
    
    private func setupUI() {
        self.collectionView.register(R.nib.introCell)
        self.collectionView.delegate = self
        
        self.pageControl.numberOfPages = 6
        self.pageControl.currentPage = 0
    }
    
    private func setupRX() {
        Observable.just(Pages.allCases)
            .bind(to: self.collectionView.rx.items(cellIdentifier: IntroCell.identifier, cellType: IntroCell.self)) { row, data, cell in
                cell.imgShow.image = UIImage.init(named: data.imageIntro)
            }.disposed(by: disposeBag)
        
        self.btNext.rx.tap.bind { [weak self] _ in
            guard let wSelf = self, let type = Pages(rawValue: wSelf.pageControl.currentPage) else { return }
            switch type {
            case .pageZero, .pageOne, .pageTwo, .pageThree, .pageFour:
                wSelf.collectionView.scrollToItem(at: IndexPath(item: wSelf.pageControl.currentPage + 1, section: 0),
                                                  at: .centeredHorizontally, animated: true)
                wSelf.pageControl.currentPage += 1
            case .pageFive:
                Utils.presentTabbar()
            }
        }.disposed(by: self.disposeBag)
    }
    
}
extension IntroViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        //Hiển thị page curent
        let page_number = targetContentOffset.pointee.x / (self.view.frame.width)
        pageControl.currentPage = Int(page_number)
    }
}
