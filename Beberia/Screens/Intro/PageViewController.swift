//
//  PageViewController.swift
//  PageControllerTutorial
//
//  Created by IMAC on 12/17/19.
//  Copyright © 2019 Tiger Bomb. All rights reserved.
//

import UIKit

class PageViewController: UIViewController {
    
    //MARK: Outlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTutorial: UIImageView!
    
    @IBOutlet weak var imgTuto: UIImageView!
    //MARK: Properties
    var page: Pages
    
    init(with page: Pages) {
        self.page = page
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = page.name
        lblTitle.adjustsFontSizeToFitWidth = true
        imgTuto.image = UIImage.init(named: page.imageIntro)
    }


}
