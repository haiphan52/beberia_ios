//
//  PolicySaleViewController.swift
//  
//
//  Created by IMAC on 8/5/20.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD

class PolicySaleViewController: BaseViewController {

    @IBOutlet weak var tbvPolicySale: UITableView!
    
    private var arrayPolicySale = BehaviorRelay.init(value: [PolicySaleModel]())
    var page = 1
    var nextPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoaddingManager.initLoadding(view: self.view)
        customLeftBarButton()
        getPolicySale(page: page)
        initRx()
        title = "Chính sách thẻ giảm giá"
        
    }

    func initRx(){
        
        tbvPolicySale.register(R.nib.policyTableViewCell)
        tbvPolicySale.rowHeight = UITableView.automaticDimension
        tbvPolicySale.tableFooterView = UIView()
        tbvPolicySale.showsVerticalScrollIndicator = false
        tbvPolicySale.allowsSelection = false
        tbvPolicySale.addInfiniteScroll { (tableView) in
                   if self.nextPage != 0{
                       self.page += 1
                       self.getPolicySale(page: self.page)
                   }else{
                       self.tbvPolicySale.endRefreshing(at: .top)
                       self.tbvPolicySale.finishInfiniteScroll(completion: { (collection) in
                       })
                   }
               }
        
        arrayPolicySale.asObservable().observeOn(MainScheduler.asyncInstance)
            .bind(to: tbvPolicySale.rx.items(cellIdentifier: R.reuseIdentifier.policyTableViewCell.identifier, cellType: PolicyTableViewCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                var categoryString = ""
                if (data.categories ?? [Categories]()).count > 0 {
                    for category in data.categories! {
                        if categoryString == ""{
                            categoryString += (category.name ?? "")
                        }else{
                            categoryString += "\n\n\(category.name ?? "")"
                        }
                    }
                }
                
                var brandString = ""
                if (data.store?.brandList ?? [BrandList]()).count > 0 {
                    for category in (data.store?.brandList!)! {
                        if brandString == ""{
                            brandString += (category.address ?? "")
                        }else{
                            brandString += "\n\n\(category.address ?? "")"
                        }
                    }
                }
                
                cell.categoryLable.text = categoryString
                
                
                cell.saleLable.text = ""
                
                let value = data.saleValue == "0 " ? "" : "\(data.saleValue ?? "")%"
                let note = data.note == "" ? "" : "(\(data.note ?? ""))"
                
                cell.saleLable.text = value + note

                cell.adressLable.text = brandString
                cell.brandtLable.text = data.store?.name ?? ""
        }.disposed(by: disposeBag)
    }

}

extension PolicySaleViewController {
    private func getPolicySale(page: Int){
        LoaddingManager.startLoadding()
        APIManager.getPolicySale(page: page, callbackSuccess: { (policies, page) in
            print(policies.count)
           // self.arrayPolicySale.accept(policies)
            
            if self.page != 0 {
                self.arrayPolicySale.accept(self.arrayPolicySale.value + policies)
                
            }
            
            self.nextPage = page
            self.tbvPolicySale.finishInfiniteScroll(completion: { (collection) in
            })
            LoaddingManager.stopLoadding()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            LoaddingManager.stopLoadding()
        }
    }
}
