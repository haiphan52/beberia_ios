//
//  AddEASYViewController.swift
//  Beberia
//
//  Created by IMAC on 6/18/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import FFPopup

class AddEASYViewController: BaseViewController {

    @IBOutlet weak var timeToLable: UILabel!
    @IBOutlet weak var timeFromLable: UILabel!
    @IBOutlet weak var btnSelectActivity: UIButton!
    @IBOutlet weak var btnTimeTo: UIButton!
    @IBOutlet weak var btnTimeFrom: UIButton!
    @IBOutlet weak var activityLable: UILabel!
    @IBOutlet weak var tvNote: UITextView!
    @IBOutlet weak var btnAdd: UIButton!
    
    let popupTime = ViewPickerViewTime.instanceFromNib()
    var isCheckClickTimeFrom = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customLeftBarButton()
        initRx()
   
    }
    
    func initRx(){
        btnTimeFrom.rx.tap.subscribe { [weak self] (_) in
            self?.isCheckClickTimeFrom = true
            self?.showPopupAndHandle()
        }.disposed(by: disposeBag)
        
        btnTimeTo.rx.tap.subscribe { [weak self] (_) in
            self?.isCheckClickTimeFrom = false
            self?.showPopupAndHandle()
        }.disposed(by: disposeBag)
        
        btnSelectActivity.rx.tap.subscribe { [weak self] (_) in
            self?.showViewActivity()
        }.disposed(by: disposeBag)
    }
    
    func showViewActivity(){
        let bottomVC = SelectActivityViewController.init(nib: R.nib.selectActivityViewController)
        
        bottomVC.selectedActivity = { activity in
            self.activityLable.text = activity
            bottomVC.dismiss(animated: true, completion: nil)
        }
        bottomVC.height = 260
        bottomVC.topCornerRadius = 20
        bottomVC.presentDuration = 0.25
        bottomVC.dismissDuration = 0.25
        Utils.getTopMostViewController()?.navigationController?.present(bottomVC, animated: true)
    }
    
    func showPopupAndHandle(){
        self.popupTime.frame = CGRect.init(x: 0, y: 0, width: 160, height: 200)
        let popup = FFPopup.init(contentView: self.popupTime)
        popup.showType = .growIn
        popup.dismissType = .shrinkOut
        let layout = FFPopupLayout(horizontal: .center, vertical: .center)
        popup.show(layout: layout)
        
        self.popupTime.tapExit = { popup.dismiss(animated: true) }
        
        self.popupTime.tapOK = { time in
            if self.isCheckClickTimeFrom{
                self.timeFromLable.text = time
            }else {
                self.timeToLable.text = time
            }
            popup.dismiss(animated: true)
        }
    }
    
}
