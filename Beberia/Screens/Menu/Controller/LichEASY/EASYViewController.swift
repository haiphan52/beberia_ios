//
//  EASYViewController.swift
//  Beberia
//
//  Created by IMAC on 6/16/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class EASYViewController: BaseViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var heightViewMyEASY: NSLayoutConstraint!
    @IBOutlet weak var viewMyEASY: UIView!
    @IBOutlet weak var easyMySelfLable: UILabel!
    @IBOutlet weak var esayCommonLAble: UILabel!
    @IBOutlet weak var tbvEASY: UITableView!
    @IBOutlet weak var btnEASYMySelf: UIButton!
    @IBOutlet weak var bntEASYCommon: UIButton!

    // MARK: - Properties
    var listEASY = BehaviorRelay.init(value: [ListEASYModel]())
    
    // MARK: - Function
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Custom Navication
        
        customLeftBarButton()
        customRightBarButton(image: UIImage.init())
        let viewTitle = ViewTitleVaccin.instanceFromNib()
        viewTitle.titleLable.text = R.string.localizable.easyTitle()
        viewTitle.imageTitle.image = R.image.icLichEASY()!
        customTitleView(view: viewTitle)
        
        // init Rx
        initRX()
      
        // Call API
        self.getListEASY(userID: 0, page: 1)
        heightViewMyEASY.constant = 0
        viewMyEASY.isHidden = true
    }
    
    override func actionRightBar() {
        let introEASY = AddMyEASYViewController.init(nib: R.nib.addMyEASYViewController)
        self.navigationController?.pushViewController(introEASY, animated: true)
    }
    
    func changeUIButtonEASY(tag: Int){
        esayCommonLAble.textColor = tag == 31 ? UIColor.black : UIColor.init(hexString: "ffffff", alpha: 0.5)!
        easyMySelfLable.textColor = tag == 31 ? UIColor.init(hexString: "ffffff", alpha: 0.5)! : UIColor.white
        
        self.getListEASY(userID: tag != 31 ? 0 : UserInfo.shareUserInfo.id ?? 0, page: 1)
    }
    
    func initRX(){
        tbvEASY.register(R.nib.easyTableViewCell)
        tbvEASY.estimatedRowHeight = 80
        tbvEASY.tableFooterView = UIView()
        tbvEASY.separatorStyle = .none
        
        listEASY.asObservable().observeOn(MainScheduler.asyncInstance)
        .bind(to: tbvEASY.rx.items(cellIdentifier: R.reuseIdentifier.easyTableViewCell.identifier, cellType: EASYTableViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
            cell.selectionStyle = .none
            cell.nameEASYLable.text = data.name
            cell.countUserLable.text = "\(data.numberUser ?? 0)" + R.string.localizable.easyUser()
        }.disposed(by: disposeBag)
        
        Observable
        .zip(tbvEASY.rx.itemSelected, tbvEASY.rx.modelSelected(ListEASYModel.self))
        .bind { [weak self] indexPath, item in
            let introEASY = IntroEASYViewController.init(nib: R.nib.introEASYViewController)
            introEASY.idEASY = item.id ?? 0
            
            self?.navigationController?.pushViewController(introEASY, animated: true)

//            let introEASY = AddEASYViewController.init(nib: R.nib.addEASYViewController)
//            self?.navigationController?.pushViewController(introEASY, animated: true)
        }
        .disposed(by: disposeBag)
    }
    
    @IBAction func didPressEsay(_ sender: UIButton) {
      //  changeUIButtonEASY(tag: sender.tag)
    }
    
}

extension EASYViewController{
    func getListEASY(userID: Int, page: Int){
        APIManager.getListEasy(userID: userID, page: page, callbackSuccess: { (listEASYs) in
            self.listEASY.accept(listEASYs)
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
}
