//
//  IntroEASYViewController.swift
//  Beberia
//
//  Created by IMAC on 6/16/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FFPopup
import SwiftEntryKit

class IntroEASYViewController: BaseViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var hiehgtViewMore: NSLayoutConstraint!
    @IBOutlet weak var btnSetTime: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var viewMore: UIView!
    @IBOutlet weak var heightTbv: NSLayoutConstraint!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var introEASYLable: UILabel!
    @IBOutlet weak var tbvStepIntro: ContentSizedTableView!
    @IBOutlet weak var viewSetTime: UIView!
    @IBOutlet weak var heihgtViewSetTime: NSLayoutConstraint!
    @IBOutlet weak var spaceBottomViewSetTime: NSLayoutConstraint!
    
    var groupEASYFull = [Group]()
    var groupEASY = BehaviorRelay.init(value: [Group]())
    var easyModel = BehaviorRelay.init(value: ESAYModel.init(json: ""))
    var idEASY = 0
    let timeAttributes : [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.font : UIFont.init(name: AppFont.HelveticaNeueMedium, size: 18)!,
        NSAttributedString.Key.foregroundColor : R.color.ffc440Color()!,
        NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initRX()
        customLeftBarButton()
        self.showViewMore(bool: true)

        viewSetTime.isHidden = true
        heihgtViewSetTime.constant = 0
        spaceBottomViewSetTime.constant = 0
        
        getEASYDetail()
        btnApply.isHidden = false
            
    }

    func showData(){
        introEASYLable.text = easyModel.value.note ?? ""
        
        let viewTitle = ViewTitleEASY.instanceFromNib()
        customTitleView(view: viewTitle)
        viewTitle.title.text = easyModel.value.name ?? ""
        
        let toWeek = easyModel.value.toWeek ?? 0
        
        viewTitle.subTitle.text = "(\(R.string.localizable.easyFromWeek()) \(easyModel.value.fromWeek ?? 0)-\(toWeek) \(R.string.localizable.easyWeek()))"
        
        if toWeek == 0{
            viewTitle.subTitle.text = "(\(R.string.localizable.easyFromWeek()) \(easyModel.value.fromWeek ?? 0) \(R.string.localizable.easyWeek()))"
        }
    }
    
    func initRX(){
        
        // handle data
        self.easyModel.subscribe { eSAYModel in
            guard let eSAYModel = eSAYModel.element else { return }
            self.groupEASYFull = eSAYModel.group ?? [Group]()
            let group = eSAYModel.group ?? [Group]()
            
            // add stt group
            for (index, item) in group.enumerated() {
                item.stt = index + 1
            }
            
            // Show data
            self.showData()
            self.groupEASY.accept(self.groupEASYFull)
   
        }.disposed(by: disposeBag)

        
        btnMore.rx.tap.subscribe { (_) in
            self.groupEASY.accept(self.groupEASYFull)
            self.showViewMore(bool: true)
        }.disposed(by: disposeBag)
        
        // init TableView
        tbvStepIntro.register(R.nib.stepEASYTableViewCell)
        tbvStepIntro.register(R.nib.stepOtherTableViewCell)
        tbvStepIntro.tableFooterView = UIView()
        tbvStepIntro.separatorStyle = .none
        tbvStepIntro.isScrollEnabled = false
        tbvStepIntro.estimatedRowHeight = UITableView.automaticDimension
        
        // show data bind to tableView
        groupEASY.asObservable().observeOn(MainScheduler.asyncInstance).bind(to: tbvStepIntro.rx.items){(tv, row, data) -> UITableViewCell in
            
            if data.type == 1 {
                
                let cell = tv.dequeueReusableCell(withIdentifier: R.reuseIdentifier.stepOtherTableViewCell.identifier, for: IndexPath.init(row: row, section: 0)) as! StepOtherTableViewCell
                cell.selectionStyle = .none
                cell.setupData(data: data)
                
                return cell
            }else{
                let cell = tv.dequeueReusableCell(withIdentifier: R.reuseIdentifier.stepEASYTableViewCell.identifier, for: IndexPath.init(row: row, section: 0)) as! StepEASYTableViewCell
                cell.selectionStyle = .none
                cell.sttViewLable.text = "\(data.stt ?? 0)"
                cell.setupData(timeLines: data.timelines ?? [Timelines]())
                return cell
            }
            
        }.disposed(by: disposeBag)
        
        // set contentSize tableview
        tbvStepIntro.rx.observe(CGSize.self, "contentSize").asObservable().observeOn(MainScheduler.asyncInstance).subscribe { [weak self] (size) in
            guard let self = self else { return }
            self.heightTbv.constant = size.element!!.height
            
            //            UIView.animate(withDuration: 0.5) {
            //                self.updateViewConstraints()
            //                self.view.layoutIfNeeded()
            //            }
        }.disposed(by: disposeBag)
    }
    
    func showViewMore(bool: Bool){
        self.viewMore.isHidden = bool
        self.hiehgtViewMore.constant = bool ? 0 : 60
    }
    
    @IBAction func didPressApply(_ sender: Any) {
        applyEASY()
    }
}



// MARK: - Request API

extension IntroEASYViewController{
    func getEASYDetail(){
        APIManager.getEasyDetail(idEASY: self.idEASY, callbackSuccess: { (easyModel) in
            self.easyModel.accept(easyModel)
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func applyEASY(time: Double = 0){
        APIManager.easyAction(easy_id: self.easyModel.value.id ?? 0, wake_up: time, callbackSuccess: { (easyModel) in
            UserInfo.shareUserInfo.is_easy = 1

            let easyAction = EASYActiveViewController.init(nib: R.nib.easyActiveViewController)
            self.navigationController?.pushViewController(easyAction, animated: true)
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
        
    }
}

final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return contentSize
    }
}
