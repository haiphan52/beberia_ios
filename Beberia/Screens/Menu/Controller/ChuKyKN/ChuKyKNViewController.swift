//
//  ChuKyKNViewController.swift
//  Beberia
//
//  Created by IMAC on 7/1/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ChuKyKNViewController: BaseViewController {
    
    var distance = 0
    var cycle_time = 0
    var date_cycle = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customLeftBarButton()
        customRightBarButton(image: UIImage.init())
        let view = ViewTitleVaccin.instanceFromNib()
       // view.frame = CGRect.init(x: -30, y: 0, width: view.frame.width, height: view.frame.height)
        view.imageTitle.image = R.image.icChuKyKN()!
        view.titleLable.text = R.string.localizable.menstrualTitle()
        customTitleView(view: view)
        showPopupCalendar()
    }
    
    override func backVC() {
        if self.navigationController?.viewControllers.first == self {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func showPopupCalendar(){
        let viewCalendar = ViewCalendar.instanceFromNib()
        viewCalendar.titlePopup.text = R.string.localizable.menstrualMenstrualCycle()
        viewCalendar.titlePopup.textColor = .white
        viewCalendar.fsCalendar.appearance.selectionColor = UIColor.init(hexString: "ff99bb")
        viewCalendar.btnOK.tintColor = UIColor.init(hexString: "ffffff")
        viewCalendar.btnOK.backgroundColor = UIColor.init(hexString: "fd799d")
        viewCalendar.fsCalendar.appearance.headerTitleColor = UIColor.init(hexString: "000000")
        viewCalendar.fsCalendar.appearance.titleDefaultColor = UIColor.init(hexString: "000000")
        viewCalendar.fsCalendar.appearance.titleFont = UIFont.init(name: AppFont.NotoSans, size: 14)
        viewCalendar.titlePopup.backgroundColor = UIColor.init(hexString: "fd799d")
        for lable in viewCalendar.fsCalendar.calendarWeekdayView.weekdayLabels{
            lable.textColor = UIColor.init(hexString: "000000", alpha: 1.0)
            lable.font =  UIFont.init(name: AppFont.NotoSansMedium, size: 14)
        }
        let popup = showPopup(view: viewCalendar, height: 370)
        viewCalendar.tapOK = { date in
         //   print(date)
         //   print(Utils.dateStringToTimeStamp(date: date))
            self.date_cycle = Utils.dateStringToTimeStamp(date: date)
            print(self.date_cycle)
            popup.dismiss(animated: true)
            self.showPopupSelectedChuKy()
        }
    }
    
    func showPopupSelectedChuKy(){
        let view = ViewSelectedYear.instanceFromNib()
        let popup = showPopup(view: view, height: 315)
        view.titileView.text = R.string.localizable.menstrualMenstrualLongTime()
        var dataInt = [Int]()
        for i in 1...50 {
            dataInt.append(i)
        }
        let data = dataInt.map { String($0) }
        view.data = data
        view.tapOK = { value in
            self.distance = Int(value == "" ? data[0] : value) ?? 0
         //   print(self.cycle_time)
            self.showPopupSelectedChuKy1()
            popup.dismiss(animated: true)
        }
    }
    
    func showPopupSelectedChuKy1(){
        let view = ViewSelectedYear.instanceFromNib()
        let popup = showPopup(view: view, height: 315)
        view.titileView.text = R.string.localizable.menstrualMenstrualDay()
        var dataInt = [Int]()
        for i in 1...10 {
            dataInt.append(i)
        }
        let data = dataInt.map { String($0) }
        view.data = data
        view.tapOK = { value in
            self.cycle_time = Int(value == "" ? data[0] : value) ?? 0
            
            let calendar = CalanderViewController.init(nib: R.nib.calanderViewController)
            calendar.distance = self.distance
            calendar.cycle_time = self.cycle_time
            calendar.date_cycle = self.date_cycle
            self.navigationController?.pushViewController(calendar, animated: true)
            
            popup.dismiss(animated: true)
        }
    }
    
    
    
}

