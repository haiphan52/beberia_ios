//
//  DIYCalendarCell.swift
//  FSCalendarSwiftExample
//
//  Created by dingwenchao on 06/11/2016.
//  Copyright © 2016 wenchao. All rights reserved.
//

import Foundation
import FSCalendar

import UIKit

enum SelectionType : Int {
    case none
    case single
    case leftBorder
    case middle
    case rightBorder
}


class DIYCalendarCell: FSCalendarCell {
    
    weak var circleImageView: UIImageView!
    weak var selectionLayer: CAShapeLayer!
    weak var circleLayer: CAShapeLayer!
    
    var selectionType: SelectionType = .none {
        didSet {
            setNeedsLayout()
        }
    }
    
    required init!(coder aDecoder: NSCoder!) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        let circleImageView = UIImageView(image: R.image.appleButton()!)
//        self.contentView.insertSubview(circleImageView, at: 0)
//        self.circleImageView = circleImageView
        
        let selectionLayer = CAShapeLayer()
        selectionLayer.fillColor = UIColor.init(hexString: "fd799d", alpha: 1.0)?.cgColor
        selectionLayer.actions = ["hidden": NSNull()]
        self.contentView.layer.insertSublayer(selectionLayer, below: self.titleLabel!.layer)
        self.selectionLayer = selectionLayer
        
        self.shapeLayer.isHidden = true
        
        let circleLayer = CAShapeLayer()
        circleLayer.fillColor = UIColor.init(hexString: "ffffff", alpha: 1.0)?.cgColor
        circleLayer.actions = ["hidden": NSNull()]
        self.contentView.layer.insertSublayer(circleLayer, below: self.titleLabel!.layer)
        self.circleLayer = circleLayer
        
        let view = UIView(frame: self.bounds)
        self.backgroundView = view;

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
      //  self.circleImageView.frame = self.contentView.bounds
        self.backgroundView?.frame = self.bounds.insetBy(dx: 1, dy: 1)
        self.selectionLayer.frame = CGRect.init(x: 0, y: 0, width: self.frame.width, height: self.frame.height - 5)
        
        circleLayer.fillColor = UIColor.clear.cgColor
       
        
        if selectionType == .middle {
            self.selectionLayer.path = UIBezierPath(rect: self.selectionLayer.bounds).cgPath
        }
        else if selectionType == .leftBorder {
           // self.titleLabel.textColor = UIColor.red
            circleLayer.fillColor = UIColor.init(hexString: "ffffff", alpha: 1.0)?.cgColor
            self.circleLayer.frame = CGRect.init(x: 0.5, y: 0.5, width: self.frame.height - 6, height: self.frame.height - 6)
            
            self.selectionLayer.path = UIBezierPath(roundedRect: CGRect.init(x: 18 , y: 0, width: self.selectionLayer.frame.width, height: self.selectionLayer.frame.height) , byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: self.selectionLayer.frame.width / 2, height: self.selectionLayer.frame.width / 2)).cgPath
            
            self.circleLayer.path = UIBezierPath(roundedRect: CGRect.init(x: 18, y: 0, width: self.circleLayer.frame.width, height: self.circleLayer.frame.height) , byRoundingCorners: [.topLeft, .bottomLeft, .topRight, .bottomRight], cornerRadii: CGSize(width: self.circleLayer.frame.width / 2, height: self.circleLayer.frame.width / 2)).cgPath
        }
        else if selectionType == .rightBorder {
            self.selectionLayer.path = UIBezierPath(roundedRect: CGRect.init(x: -18, y: 0, width: self.selectionLayer.frame.width, height: self.selectionLayer.frame.height), byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: self.selectionLayer.frame.width / 2, height: self.selectionLayer.frame.width / 2)).cgPath
        }
        else if selectionType == .single {
            let diameter: CGFloat = min(self.selectionLayer.frame.height, self.selectionLayer.frame.width)
            self.selectionLayer.path = UIBezierPath(ovalIn: CGRect(x: self.contentView.frame.width / 2 - diameter / 2, y: self.contentView.frame.height / 2 - diameter / 2, width: diameter, height: diameter)).cgPath
        }
    }
    
    override func configureAppearance() {
        super.configureAppearance()
        // Override the build-in appearance configuration
        if self.isPlaceholder {
            self.eventIndicator.isHidden = true
            self.titleLabel.textColor = UIColor.lightGray
        }
    }
    
}
