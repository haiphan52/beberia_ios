//
//  CalanderViewController.swift
//  Beberia
//
//  Created by IMAC on 6/24/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import FSCalendar

class CalanderViewController: BaseViewController , FSCalendarDelegateAppearance, FSCalendarDataSource{
    
    @IBOutlet weak var btnUpdateCircle: UIButton!
    @IBOutlet weak var fsCalendar: FSCalendar!
    
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    var datesWithConception = [String]()
    var datesWithOvulation = [String]()
    var datesWithMenstrual = [String]()
    var datesWithMenstrualInt = [Int]()
    var distance = 0
    var cycle_time = 0
    var date_cycle = 0
    var groupInt = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = R.string.localizable.menstrualTitleSetup()
        customLeftBarButton()
        customCalendar()
        self.fsCalendar.delegate = self
        fsCalendar.dataSource = self
        
        // if is_cycle == 0 -> create Cycle Menstrual
        // if is_cycle == 1 -> get Cycle Menstrual
        if UserInfo.shareUserInfo.is_cycle == 0 {
             createCycleMenstrual()
        }else{
            let date = self.gregorian.date(byAdding: .day, value: 1, to: fsCalendar.currentPage)!
            let month = Double(Utils.dateStringToTimeStamp(date: self.dateFormatter2.string(from: date), formatString: "yyyy-MM-dd"))
            self.getCycleMenstrual(date: date , monthTimeStamp: month)
        }
        
    }
    
    func customCalendar(){
        fsCalendar.today = nil // Hide the today circle
        fsCalendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
        fsCalendar.appearance.imageOffset = CGPoint(x: 10, y: -20)
        
        self.fsCalendar.calendarHeaderView.scrollEnabled = false
        self.fsCalendar.appearance.headerDateFormat = "MMMM yyyy"
        self.fsCalendar.appearance.headerTitleFont =  UIFont.init(name: AppFont.NotoSansBold, size: 16)
        self.fsCalendar.appearance.headerTitleColor = UIColor.init(hexString: "fd799d", alpha: 0.87)
        self.fsCalendar.appearance.titleFont = UIFont.init(name: AppFont.NotoSans, size: 14)
        
        self.fsCalendar.calendarWeekdayView.weekdayLabels[0].text = "T2"
        self.fsCalendar.calendarWeekdayView.weekdayLabels[1].text = "T3"
        self.fsCalendar.calendarWeekdayView.weekdayLabels[2].text = "T4"
        self.fsCalendar.calendarWeekdayView.weekdayLabels[3].text = "T5"
        self.fsCalendar.calendarWeekdayView.weekdayLabels[4].text = "T6"
        self.fsCalendar.calendarWeekdayView.weekdayLabels[5].text = "T7"
        self.fsCalendar.calendarWeekdayView.weekdayLabels[6].text = "CN"
        
        for lable in self.fsCalendar.calendarWeekdayView.weekdayLabels{
            lable.textColor = UIColor.init(hexString: "fd799d", alpha: 1.0)
            lable.font =  UIFont.init(name: AppFont.NotoSansMedium, size: 14)
        }
        
        //  self.fsCalendar.appearance.titleFont = UIFont.init(name: AppFont.HelveticaNeue, size: 14)
        self.fsCalendar.placeholderType = .none
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        let dateString = self.dateFormatter2.string(from: date)

        if self.cycle_time == 1{
            if self.datesWithMenstrual.contains(dateString) {
                return .white
            }
        }else {
            if self.groupInt.contains(dateString) {
                return UIColor.init(hexString: "ff99bb", alpha: 1.0)
            }
        }
        
        if self.datesWithMenstrual.contains(dateString) {
            return .white
        }
        
        if self.datesWithConception.contains(dateString) {
            return UIColor.init(hexString: "ff8000", alpha: 1.0)
        }
        
        if self.datesWithOvulation.contains(dateString) {
            return UIColor.init(hexString: "48c7ff", alpha: 1.0)
        }
        
        
       
        return UIColor.init(hexString: "000000", alpha: 1.0)
    }
    
    
    @IBAction func didPressToday(_ sender: Any) {
        self.fsCalendar.setCurrentPage(Date(), animated: true)
    }
    
    // MARK:- FSCalendarDataSource
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        self.configure(cell: cell, for: date, at: position)
    }
    
    // MARK:- FSCalendarDelegate
    
    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
        //        let day: Int! = self.gregorian.component(.day, from: date)
        if self.datesWithConception.contains(self.formatter.string(from: date)) {
            return  R.image.group_5006()!
        }
        return nil
        
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(self.formatter.string(from: date))")
        self.configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        print("did deselect date \(self.formatter.string(from: date))")
        self.configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        if self.gregorian.isDateInToday(date) {
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }
    
    // MARK: - Private functions
    
    private func configureVisibleCells() {
        fsCalendar.visibleCells().forEach { (cell) in
            let date = fsCalendar.date(for: cell)
            let position = fsCalendar.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        let dateString = self.dateFormatter2.string(from: date)
        let diyCell = (cell as! DIYCalendarCell)
        // Custom today circle
        //   diyCell.circleImageView.isHidden = false
        //  !self.gregorian.isDateInToday(date)
        // Configure selection layer
        if position == .current {
            
            var selectionType = SelectionType.none
            
            if datesWithMenstrual.contains(dateString) {
                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
                let previousDateString = self.dateFormatter2.string(from: previousDate)
                let nextDateString = self.dateFormatter2.string(from: nextDate)
                if datesWithMenstrual.contains(dateString) {
                    if datesWithMenstrual.contains(previousDateString) && datesWithMenstrual.contains(nextDateString) {
                        selectionType = .middle
                    }
                    else if datesWithMenstrual.contains(previousDateString) && datesWithMenstrual.contains(dateString) {
                        selectionType = .rightBorder
                    }
                    else if datesWithMenstrual.contains(nextDateString) {
                        selectionType = .leftBorder
                    }
                    else {
                        selectionType = .single
                    }
                }
            }
            else {
                selectionType = .none
            }
            if selectionType == .none {
                diyCell.selectionLayer.isHidden = true
                return
            }
            diyCell.selectionLayer.isHidden = false
            diyCell.selectionType = selectionType
            
        } else {
            diyCell.circleImageView.isHidden = true
            diyCell.selectionLayer.isHidden = true
        }
    }
    
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {

        let date = self.gregorian.date(byAdding: .day, value: 1, to: calendar.currentPage)!
        
        let month = Double(Utils.dateStringToTimeStamp(date: self.dateFormatter2.string(from: self.gregorian.date(byAdding: .day, value: 1, to: calendar.currentPage)!), formatString: "yyyy-MM-dd"))
        
        
        self.getCycleMenstrual(date: date , monthTimeStamp: month)
    }
    
    func convertToStringDate(date: Date? = Date(), arrayObject: [Int])-> [String]{
        var dataString = [String]()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date!)
        var month = String(calendar.component(.month, from: date!))
        
        if month.count == 1 { month = "0\(month)" }
        
        for index in arrayObject {
            var date = String(index)
            if date.count == 1 { date = "0\(index)" }
            let dateString = "\(year)-\(month)-\(date)"
            dataString.append(dateString)
        }
        return dataString
    }
    
    func showPopupCalendar(){
        let viewCalendar = ViewCalendar.instanceFromNib()
        viewCalendar.titlePopup.text = R.string.localizable.menstrualMenstrualCycle()
        viewCalendar.titlePopup.textColor = .white
        viewCalendar.fsCalendar.appearance.selectionColor = UIColor.init(hexString: "ff99bb")
        viewCalendar.btnOK.backgroundColor = UIColor.init(hexString: "ff99bb")
        viewCalendar.btnOK.tintColor = UIColor.init(hexString: "ffffff")
        viewCalendar.fsCalendar.appearance.headerTitleColor = UIColor.init(hexString: "ff99bb")
        viewCalendar.titlePopup.backgroundColor = UIColor.init(hexString: "ff99bb")
        viewCalendar.btnOK.backgroundColor = UIColor.init(hexString: "fd799d")
        viewCalendar.fsCalendar.appearance.headerTitleColor = UIColor.init(hexString: "000000")
        viewCalendar.fsCalendar.appearance.titleDefaultColor = UIColor.init(hexString: "000000")
        viewCalendar.fsCalendar.appearance.titleFont = UIFont.init(name: AppFont.NotoSans, size: 14)
        viewCalendar.titlePopup.backgroundColor = UIColor.init(hexString: "fd799d")
        for lable in viewCalendar.fsCalendar.calendarWeekdayView.weekdayLabels{
            lable.textColor = UIColor.init(hexString: "000000", alpha: 1.0)
            lable.font =  UIFont.init(name: AppFont.NotoSansMedium, size: 14)
        }
        let popup = showPopup(view: viewCalendar, height: 370)
        viewCalendar.tapOK = { date in
            print(date)
            print(Utils.dateStringToTimeStamp(date: date))
            self.date_cycle = Utils.dateStringToTimeStamp(date: date)
            print(self.date_cycle)
            popup.dismiss(animated: true)
            self.showPopupSelectedChuKy()
        }
    }
    
    func showPopupSelectedChuKy(){
        let view = ViewSelectedYear.instanceFromNib()
        let popup = showPopup(view: view, height: 315)
        view.titileView.text = R.string.localizable.menstrualMenstrualLongTime()
        var dataInt = [Int]()
        for i in 1...50 {
            dataInt.append(i)
        }
        let data = dataInt.map { String($0) }
        view.data = data
        view.tapOK = { value in
            self.distance = Int(value == "" ? data[0] : value) ?? 0
            print(self.cycle_time)
            self.showPopupSelectedChuKy1()
            popup.dismiss(animated: true)
        }
    }
    
    func showPopupSelectedChuKy1(){
        let view = ViewSelectedYear.instanceFromNib()
        let popup = showPopup(view: view, height: 315)
        view.titileView.text = R.string.localizable.menstrualMenstrualDay()
        var dataInt = [Int]()
        for i in 1...10 {
            dataInt.append(i)
        }
        let data = dataInt.map { String($0) }
        view.data = data
        view.tapOK = { value in
            self.cycle_time = Int(value == "" ? data[0] : value) ?? 0
            self.fsCalendar.setCurrentPage(Date(), animated: true)
            self.createCycleMenstrual()
            popup.dismiss(animated: true)
        }
    }
    
    
    @IBAction func didPressUpdateCricle(_ sender: Any) {
        showPopupCalendar()
    }
    
    @IBAction func didPressNext(_ sender: Any) {
        let previousMonth = Calendar.current.date(byAdding: .month, value: 1, to: fsCalendar.currentPage)
        fsCalendar.setCurrentPage(previousMonth!, animated: true)
    }
    
    @IBAction func didPressPre(_ sender: Any) {
        let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: fsCalendar.currentPage)
        fsCalendar.setCurrentPage(previousMonth!, animated: true)
    }
}

// MARK: - Request API
 
extension CalanderViewController {
    func getCycleMenstrual(date: Date, monthTimeStamp : Double){
        APIManager.getCycleMenstrual(month: monthTimeStamp, callbackSuccess: { (menstrual, ovulation, conception) in
            
//             print(menstrual)
//            print(ovulation)
//            print(conception)
            self.datesWithMenstrualInt = menstrual
            self.datesWithMenstrual = self.convertToStringDate(date: date ,arrayObject: menstrual)
            self.datesWithOvulation = self.convertToStringDate(date: date ,arrayObject: ovulation)
            self.datesWithConception = self.convertToStringDate(date: date ,arrayObject: conception)
            
            var arrayInt = [Int]()
            let array = self.datesWithMenstrualInt.consecutivelyGrouped
            for group in array {
                if group.count > 0 {
                    arrayInt.append(group[0])
                }
            }
            self.groupInt = self.convertToStringDate(date: date ,arrayObject: arrayInt)

            self.fsCalendar.reloadData()
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func createCycleMenstrual(){
        APIManager.createCycleMenstrual(distance: self.distance, cycle_time: cycle_time, date_cycle: Double(date_cycle), callbackSuccess: { (menstrual, ovulation, conception) in
            
            
//            print(menstrual)
//            print(ovulation)
//            print(conception)
            self.datesWithMenstrualInt = menstrual
            self.datesWithMenstrual = self.convertToStringDate(arrayObject: menstrual)
            self.datesWithOvulation = self.convertToStringDate(arrayObject: ovulation)
            self.datesWithConception = self.convertToStringDate(arrayObject: conception)
            
            self.fsCalendar.reloadData()
            
            UserInfo.shareUserInfo.is_cycle = 1
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
}

extension BidirectionalCollection where Element: BinaryInteger, Index == Int {
    var consecutivelyGrouped: [[Element]] {
        return reduce(into: []) {
            $0.last?.last?.advanced(by: 1) == $1 ?
            $0[index(before: $0.endIndex)].append($1) :
            $0.append([$1])
        }
    }
}
