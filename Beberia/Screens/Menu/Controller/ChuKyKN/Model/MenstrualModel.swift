//
//  DataVaccineModel.swift
//
//  Created by IMAC on 6/25/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public struct MenstrualModel {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let from = "from"
    static let to = "to"
  }

  // MARK: Properties
  public var from: Int?
  public var to: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    from = json[SerializationKeys.from].int
    to = json[SerializationKeys.to].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = from { dictionary[SerializationKeys.from] = value }
    if let value = to { dictionary[SerializationKeys.to] = value }
    return dictionary
  }

}
