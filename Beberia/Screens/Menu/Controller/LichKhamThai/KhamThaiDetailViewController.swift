//
//  KhamThaiDetailViewController.swift
//  Beberia
//
//  Created by IMAC on 7/2/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SVProgressHUD

class KhamThaiDetailViewController: BaseViewController {
    
    enum StatusMakeVC {
        case pregnant, other
    }
    
    @IBOutlet weak var tbvKhamThai: UITableView!
    
    var pregnancyModel = PregnancyModel.init(json: "")
    var updateData:(PregnancyModel)->() = {_ in}
    var vaccinesDisplayRx = BehaviorRelay.init(value: [DataCustomVaccine]())
    var dataTitle = [R.string.localizable.pregnancyPregnancy(), R.string.localizable.pregnancyStatus(), R.string.localizable.pregnancyTime(), R.string.localizable.pregnancyContent(), R.string.localizable.pregnancyRequired()]
    var dataContent = [String]()
    var babyID = 0
    var timeStamp = 0
    var openFrom: VaccinationViewController.OpenFrom = .pregnant
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoaddingManager.initLoadding(view: self.view)
        customLeftBarButton()
        title = pregnancyModel.ages ?? ""
        initRx()
        handelData()
    }
    
    override func backVC() {
        updateData(pregnancyModel)
        self.navigationController?.popViewController(animated: true)
    }
    
    func handelData(){
        
        var required = R.string.localizable.commonYes()
        if pregnancyModel.required == 0 {
            required = R.string.localizable.commonNo()
        }
        
        var time = R.string.localizable.pregnancyNotPregnancy()
        if let t = pregnancyModel.time, t != 0 {
            time = "\(R.string.localizable.pregnancyHavePregnancy()) \(Utils.getDateFromTimeStamp(timeStamp: Double(pregnancyModel.time! * 1000), dateFormat: Key.DateFormat.DateFormatddMMyyyy))"
        }
        
        dataContent = [pregnancyModel.ages ?? "", time ,pregnancyModel.timePreg ?? "",pregnancyModel.contentDetail ?? "", required]
        
        var vaccinesDisplay = [DataCustomVaccine]()
        for (index, value) in dataTitle.enumerated(){
            var vaccineDisplay = DataCustomVaccine()
            vaccineDisplay.title = value
            vaccineDisplay.content = dataContent[index]
            vaccinesDisplay.append(vaccineDisplay)
        }
        vaccinesDisplayRx.accept(vaccinesDisplay)
    }
    
    func initRx(){
        tbvKhamThai.register(R.nib.vaccineTableViewCell)
        tbvKhamThai.tableFooterView = UIView()
        
        vaccinesDisplayRx.asObservable().observeOn(MainScheduler.asyncInstance)
            .bind(to: tbvKhamThai.rx.items(cellIdentifier: R.reuseIdentifier.vaccineTableViewCell.identifier, cellType: VaccineTableViewCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                cell.titleLable.text = data.title ?? ""
                cell.spaceButton.constant = 0
                cell.heightButton.constant = 0
                cell.btnMark.isHidden = true
                cell.btnUpdate.isHidden = true
                cell.btnUpdate.setTitle("", for: .normal)
                if row == 1 {
                    cell.btnUpdate.isHidden = false
                    cell.spaceButton.constant = 10
                    cell.heightButton.constant = 25
                    cell.btnMark.isHidden = false
                    cell.btnMark.setImage(R.image.ic_unMark()!, for: .normal)
                    cell.btnUpdate.setTitle(R.string.localizable.pregnancyUpdateNow(), for: .normal)
                    cell.btnUpdate.tag = 11
                    if data.content != R.string.localizable.pregnancyNotPregnancy(){
                        cell.btnUpdate.tag = 12
                        cell.btnUpdate.setTitle(R.string.localizable.pregnancyUpdate(), for: .normal)
                        cell.btnMark.setImage(R.image.ic_mark()!, for: .normal)
                    }
                }
                
                cell.btnUpdate.rx.tap.subscribe { [weak self] (_) in
                    guard let self = self else { return }
                    if cell.btnUpdate.tag == 11 {
                        self.actionUpdateStatus()
                    }else{
                        self.showPopupAndAction()
                    }
                }.disposed(by: cell.bag)
                
                cell.btnMark.rx.tap.subscribe { (_) in
                    
                    self?.actionUpdateStatus()
                    
                }.disposed(by: cell.bag)
                
                cell.contentLable.attributedText = self?.stringAddFontSizeAttributedString(content: data.content ?? "")
        }.disposed(by: disposeBag)
    }
    
    func showPopupAndAction(){
        let view = ViewCalendar.instanceFromNib()
        view.titlePopup.text = R.string.localizable.popupSeletedDatePregnancy()
        let popup = self.showPopup(view: view, height: 360)
        view.tapOK = { date in
            print(date)
            self.timeStamp = Utils.dateStringToTimeStamp(date: date)
            self.markVaccine(schedule_id: self.pregnancyModel.id ?? 0, time: Double(Int(self.timeStamp)) )
            popup.dismiss(animated: true)
        }
    }
    
    func actionUpdateStatus(){
        if self.babyID != 0 {
            if self.pregnancyModel.time == 0{
                self.showPopupAndAction()
            }else{
                var time = Int(Date().timeIntervalSince1970)
                if let p = pregnancyModel.isVaccinated, p == 1 {
                    time = 0
                }
                self.markVaccine(schedule_id: self.pregnancyModel.id ?? 0, time: Double(time))
            }
        }else{
            let view = ViewToastPopup.instanceFromNib()
            view.titleLable.text = R.string.localizable.popupNotificationSeletedBaby()
            self.showToastPopup(view: view)
            
        }
    }
    
    func stringAddFontSizeAttributedString(content: String)->NSAttributedString{
        var content1 = content
        content1 = "<span" + " style='font-size: 14px; font-family: \(AppFont.HelveticaNeue)'>" + content1 + "</span>"
        return content1.html2AttributedString ?? NSAttributedString.init(string: "")
    }
    
}

extension KhamThaiDetailViewController{
    func markVaccine(typeCreateOrUpdate: Int = 0, schedule_id: Int, time: Double){
        LoaddingManager.startLoadding()
        APIManager.markVaccine(typeCreateOrUpdate: typeCreateOrUpdate ,
                               schedule_id: schedule_id,
                               baby_id: babyID,
                               time: Int(time),
                               type: VaccinationViewController.TypeSchedule.baby.rawValue,
                               callbackSuccess: { [weak self] (vaccine, pres) in
            if pres != nil {
                self?.pregnancyModel = pres!
                self?.handelData()
            }
            LoaddingManager.stopLoadding()
        }) { (error) in
            LoaddingManager.stopLoadding()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
}

