//
//  LichKhamThaiViewController.swift
//  Beberia
//
//  Created by IMAC on 6/17/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import FFPopup
import RxCocoa
import RxSwift
import SVProgressHUD

class LichKhamThaiViewController: BaseViewController {
    
    enum StatusView {
        case other, pregnant
    }
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var tbvLichKhamThai: UITableView!
    
    let viewTitle = ViewTitleVaccin.instanceFromNib()
    let arrayLichTiem = BehaviorRelay.init(value: [PregnancyModel]())
    var statusView: StatusView = .other
    
    var isCare = 2 { didSet { getPregnancySchedule() } }
    var babyId = 0 { didSet { getPregnancySchedule() } }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoaddingManager.initLoadding(view: self.view)
        // Custom Navication
        customLeftBarButton()
        customTitleView(view: viewTitle)
        viewTitle.titleLable.text = R.string.localizable.pregnancyTitle()
        viewTitle.imageTitle.image = R.image.icLichKhamThai()
        
        // setup Right Bar
        
        
        if Utils.getAppDelegate().babyKhamThai.id == nil {

            var babys = UserInfo.shareUserInfo.baby_info
            
            babys = babys.filter { (baby) -> Bool in
                return  baby.ageRange == 0
            }
            
            if babys.count > 0{
                Utils.getAppDelegate().babyKhamThai = babys[0]
                setupBabyToRightBar(baby: babys[0])
            }else{
                setupBabyToRightBar(baby: Utils.getAppDelegate().babyKhamThai)
            }

        }else{
            setupBabyToRightBar(baby: Utils.getAppDelegate().babyKhamThai)
        }
        
        initRx()
    }
    
    func initRx(){
        btnFilter.rx.tap.subscribe { [weak self] (_) in
            self?.showPopupFilter()
        }.disposed(by: disposeBag)
        
        self.actionRightBarButton = {
            self.showPopupSelectBaby()
        }
        // init TableView
        tbvLichKhamThai.register(R.nib.vaccinationTableViewCell)
        tbvLichKhamThai.tableFooterView = UIView()
        
        // bind to data
        arrayLichTiem.asObservable().observeOn(MainScheduler.asyncInstance)
            .bind(to: tbvLichKhamThai.rx.items(cellIdentifier: R.reuseIdentifier.vaccinationTableViewCell.identifier, cellType: VaccinationTableViewCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                cell.titleLable.text = data.ages
                cell.descLable.text = data.content
                cell.btnSelected.setImage((data.isCared ?? 0) == 0 ? UIImage.init() : R.image.icCheckedVaccine()!, for: .normal)
        }.disposed(by: disposeBag)
        
        // selected Item
        tbvLichKhamThai.rx.modelSelected(PregnancyModel.self).subscribe { (value) in
            let khamThaiDetail = KhamThaiDetailViewController.init(nib: R.nib.khamThaiDetailViewController)
            khamThaiDetail.pregnancyModel = value.element!
            khamThaiDetail.babyID = self.babyId
            khamThaiDetail.updateData = { vacccine in
                self.handleUpdateData(data: vacccine)
            }
            self.navigationController?.pushViewController(khamThaiDetail, animated: true)
        }.disposed(by: disposeBag)
    }
    
    func handleUpdateData(data: PregnancyModel){
        var arrayTemp = arrayLichTiem.value
        let index = arrayTemp.firstIndex { (vaccin) -> Bool in
            return vaccin.id == data.id
        }
        if index != nil {
            if data.time != arrayTemp[index!].time {
                arrayTemp[index!] = data
                arrayLichTiem.accept(arrayTemp)
            }
        }
    }
    
    func pushViewAadBaby(){
        let addBabyVC = AddBabyViewController.init(nib: R.nib.addBabyViewController)
        addBabyVC.backToUpdateBaby = { baby in
            UserInfo.shareUserInfo.baby_info.insert(baby, at: 0)
            Utils.getAppDelegate().babyKhamThai = baby
            self.setupBabyToRightBar(baby: baby)
        }
        self.navigationController?.pushViewController(addBabyVC, animated: true)
    }
    
    func showPopupSelectBaby(){
        let view =  ViewSelectedBaby.instanceFromNib(isCheckBaby: false)
        let popup = showPopup(view: view, height: 300)
        
        view.didSelectedBaby = { baby in
            
            if baby.id == 0{
                self.pushViewAadBaby()
            }else {
                Utils.getAppDelegate().babyKhamThai = baby
                self.setupBabyToRightBar(baby: baby)
            }
            popup.dismiss(animated: true)
        }
    }
    
    func setupBabyToRightBar(baby: BabyInfo){
        if baby.id == nil {
            self.babyId = baby.id ?? 0
            if self.statusView == .other {
                customRightBarButton(image: R.image.icSelectedBB()!)
            }
        }else{
            self.babyId = baby.id ?? 0
            if self.statusView == .other {
                let viewSelectedBabyRightBar = ViewSelectedBabyRightBar.instanceFromNib(imageUrl: baby.avatar ?? "")
              self.customRighBarView(view: viewSelectedBabyRightBar)
              viewSelectedBabyRightBar.imageAvatarBaby.kf.setImage(with: URL.init(string: baby.avatar ?? ""), placeholder: R.image.placeholder()!)
            }
        }
    }
    
    func showPopupFilter(){
        
        if babyId != 0{
            let view =  ViewFilteKhamThai.instanceFromNib()
            let popup = showPopup(view: view, height: 150)
            
            view.filterKhamThai = { status in
                self.isCare = status
                popup.dismiss(animated: true)
            }
        }else{
            let view = ViewToastPopup.instanceFromNib()
            view.titleLable.text = R.string.localizable.pregnancyNotificationSelectedBabyFilter()
            self.showToastPopup(view: view)
        }
    }
}

extension LichKhamThaiViewController {
    
    func getPregnancySchedule(){
        LoaddingManager.startLoadding()
        APIManager.getPregnancySchedule(is_cared: isCare,
                                        baby_id: babyId,
                                        callbackSuccess: { [weak self] (vaccines) in
            print(vaccines.count)
            self?.arrayLichTiem.accept(vaccines)
            LoaddingManager.stopLoadding()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            LoaddingManager.stopLoadding()
        }
    }
    
}


