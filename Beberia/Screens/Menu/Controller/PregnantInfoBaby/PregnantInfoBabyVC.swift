
//
//  
//  PregnantInfoBabyVC.swift
//  Beberia
//
//  Created by haiphan on 25/05/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

protocol PregnantInfoBabyDelegate: AnyObject {
    func updateBabyInfo(babyInfo: BabyInfo)
}

class PregnantInfoBabyVC: BaseVC {
    
    enum Action: Int, CaseIterable {
        case updateDayBorn, update, calculateBorn, explainBorn
    }
    
    enum DayBornUpdate {
        case invalid, pregnanting, greaterThan42Week(String)
    }
    
    var babyInfo: BabyInfo?
    weak var delegate: PregnantInfoBabyDelegate?
    
    // Add here outlets
    @IBOutlet weak var btUpdate: UIButton!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var tfExpectedBorn: UITextField!
    @IBOutlet weak var tfExpecetedReal: UITextField!
    @IBOutlet weak var bornRealView: UIView!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet var btsBaby: [UIButton]!
    @IBOutlet weak var lbNameError: UILabel!
    
    // Add here your view model
    private let dateFormat: DateFormatter = DateFormatter()
    private let datePicker: UIDatePicker = UIDatePicker()
    private var viewModel: PregnantInfoBabyVM = PregnantInfoBabyVM()
    private var gender: GenderBaby = .unknown
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
        self.setupNavigationVer2()
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.fd799D()!,
                                 textColor: .white,
                                 isTranslucent: true)
        self.removeBorder(font: UIFont.notoSansFont(weight: .bold, size: 18),
                          bgColor: R.color.fd799D()!,
                          textColor: .white)
        self.navigationController?.isNavigationBarHidden = false
        title = "Thông tin bé"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
}
extension PregnantInfoBabyVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        self.btUpdate.contentHorizontalAlignment = .left
        self.bornRealView.isHidden = true
        self.setupDatePicker()
        if let babyInfo = self.babyInfo {
            self.updateBabyInfo(babyInfo: babyInfo)
        }
        self.stateButtonBaby(gender: .unknown)
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        Action.allCases.forEach { type in
            let bt = self.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] in
                guard let wSelf = self else { return }
                switch type {
                case .calculateBorn:
                    let vc = PregnantCalculateVC.createVC()
                    vc.babyInfo = wSelf.babyInfo
                    vc.delegate = wSelf
                    wSelf.navigationController?.pushViewController(vc, animated: true)
                case .updateDayBorn:
                    if wSelf.bts[Action.updateDayBorn.rawValue].isSelected {
                        wSelf.bts[Action.updateDayBorn.rawValue].isSelected = false
                        wSelf.bornRealView.isHidden = true
                    } else {
                        wSelf.bts[Action.updateDayBorn.rawValue].isSelected = true
                        wSelf.bornRealView.isHidden = false
                    }
                case .update:
                    wSelf.editBaby()
                case .explainBorn:
                    let vc = WebViewVC.createVC()
                    vc.webURL = ConstantApp.shared.linkPreferPregnant
                    wSelf.navigationController?.pushViewController(vc, animated: true)
                }
            }.disposed(by: self.disposeBag)
        }
        
        GenderBaby.allCases.forEach { type in
            let bt = self.btsBaby[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                wSelf.gender = type
                wSelf.stateButtonBaby(gender: type)
            }.disposed(by: self.disposeBag)
        }
        
        self.tfName.rx.text.orEmpty.bind(onNext: { [weak self] text in
            guard let wSelf = self else { return }
            if text.isEmpty {
                wSelf.lbNameError.text = "Vui lòng nhập tên của bé"
            } else {
                wSelf.lbNameError.text = nil
            }
        }).disposed(by: self.disposeBag)
        
    }
    
    private func moveToPopupError(strDay: String) {
        let vc = PopupCancelationOrder.createVC()
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.deleegate = self
        vc.statusView = .pregnant
        self.present(vc, animated: true, completion: nil)
        vc.loadValuePregnant(strDay: strDay)
    }
    
    private func updateBabyInfo(babyInfo: BabyInfo) {
        self.tfName.text = babyInfo.nickname
        self.tfExpectedBorn.text = Double(babyInfo.birthday ?? Int(Date().timeIntervalSince1970)).toDate().toString(format: .ddMMyyyy)
        if (babyInfo.ageRange ?? 0) >= 1 {
            self.bornRealView.isHidden = false
            self.tfExpecetedReal.text = Double(babyInfo.birthday ?? Int(Date().timeIntervalSince1970)).toDate().toString(format: .ddMMyyyy)
        }
        self.gender = GenderBaby(rawValue: babyInfo.gender ?? 2) ?? .unknown
        self.stateButtonBaby(gender: self.gender)
    }
    
    private func stateButtonBaby(gender: GenderBaby) {
        switch gender {
        case .male:
            self.btsBaby[GenderBaby.male.rawValue].setImage(R.image.img_select_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.female.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.unknown.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
        case .female:
            self.btsBaby[GenderBaby.male.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.female.rawValue].setImage(R.image.img_select_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.unknown.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
        case .unknown:
            self.btsBaby[GenderBaby.male.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.female.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.unknown.rawValue].setImage(R.image.img_select_button_pregnant(), for: .normal)
        }
    }
    
    //MARK: Setup date Picker
    private func setupDatePicker() {
        self.dateFormat.dateStyle = DateFormatter.Style.medium
        self.dateFormat.timeStyle = DateFormatter.Style.none
        self.dateFormat.dateFormat = Key.DateFormat.DateFormatddMMyyyy
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        self.datePicker.addTarget(self, action: #selector(updateDateField(sender:)), for: UIControl.Event.valueChanged)
        self.tfExpectedBorn.inputView = datePicker
        self.tfExpectedBorn.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        let image = R.image.ic_pregnant_calcelator()
        imageView.image = image
        self.tfExpectedBorn.rightView = imageView
        
        self.tfExpecetedReal.inputView = datePicker
        self.tfExpecetedReal.rightViewMode = UITextField.ViewMode.always
        let imageViewReal = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        let imageReal = R.image.ic_pregnant_calcelator()
        imageViewReal.image = imageReal
        self.tfExpecetedReal.rightView = imageViewReal
    }
    
    @objc private func updateDateField(sender: UIDatePicker) {
        if self.tfExpectedBorn.isFirstResponder {
            self.tfExpectedBorn.text = dateFormat.string(from: datePicker.date)
        }
        
        if self.tfExpecetedReal.isFirstResponder {
            self.tfExpecetedReal.text = dateFormat.string(from: datePicker.date)
        }
    }
    
    private func addBaby() {
        guard let nickName = self.tfName.text,
              nickName.count > 0 else {
            return
        }
        guard let date = self.getBirthDay() else {
            self.showAlert(title: nil, message: "Vui lòng chọn ngày sinh")
            return
        }
        APIManager.addBabyProfile(gender: self.gender.rawValue,
                                  nickname: nickName,
                                  birthday: date.timeIntervalSince1970,
                                  callbackSuccess: { [weak self] (baby) in
            guard let wSelf = self else { return }
            if let index = UserInfo.shareUserInfo.baby_info.firstIndex(where: { $0.id == baby.id }) {
                UserInfo.shareUserInfo.baby_info[index] = baby
            } else {
                UserInfo.shareUserInfo.baby_info.append(baby)
            }
            wSelf.navigationController?.popViewController(animated: true, {
                wSelf.delegate?.updateBabyInfo(babyInfo: baby)
            })
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    private func editBaby() {
        guard let nickName = self.tfName.text,
              nickName.count > 0 else {
                  return
              }
        
        guard let date = self.getBirthDay() else {
            self.showAlert(title: nil, message: "Vui lòng chọn ngày sinh")
            return
        }
        
        switch ManageApp.shared.toNumberWeek(dayExpectedBorn: date.timeIntervalSince1970) {
        case .pregnanting:
            self.pregnanting(nickName: nickName, date: date)
        case .invalid:
            self.moveToPopupError(strDay: "")
        case .greaterThan42Week(let text):
            self.moveToPopupError(strDay: text)
        }
        
    }
    
    private func pregnanting(nickName: String, date: Date) {
        guard let babyInfo = self.babyInfo,
              let id = babyInfo.id else {
                  self.addBaby()
                  return
              }

        APIManager.editBabyProfile(id: id,
                                   gender: self.gender.rawValue,
                                   nickname: nickName,
                                   birthday: date.timeIntervalSince1970,
                                   image: nil,
                                   callbackSuccess: { [weak self] baby in
            guard let wSelf = self else { return }
            if let index = UserInfo.shareUserInfo.baby_info.firstIndex(where: { $0.id == baby.id }) {
                UserInfo.shareUserInfo.baby_info[index] = baby
            }
            wSelf.navigationController?.popViewController(animated: true, {
                wSelf.delegate?.updateBabyInfo(babyInfo: baby)
            })
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    private func getBirthDay() -> Date? {
        if let textReal = self.tfExpecetedReal.text, !textReal.isEmpty {
            return textReal.convertToDate(formatDate: .ddMMyyyy)
        }
        if let textExpected = self.tfExpectedBorn.text, !textExpected.isEmpty {
            return textExpected.convertToDate(formatDate: .ddMMyyyy)
        }
        return nil
    }
    
}
extension PregnantInfoBabyVC: PopupCancelationOrderDelegate {
    func actionPopup(action: PopupCancelationOrder.Action) {
        
    }
}
extension PregnantInfoBabyVC: PregnantCalculateDelegate {
    func updateBaby(babyInfo: BabyInfo) {
        self.babyInfo = babyInfo
        self.updateBabyInfo(babyInfo: babyInfo)
        if let index = UserInfo.shareUserInfo.baby_info.firstIndex(where: { $0.id == babyInfo.id }) {
            UserInfo.shareUserInfo.baby_info[index] = babyInfo
            self.delegate?.updateBabyInfo(babyInfo: babyInfo)
        }
    }
}
