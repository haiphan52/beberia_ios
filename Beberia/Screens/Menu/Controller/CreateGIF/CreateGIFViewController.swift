//
//  CreateGIFViewController.swift
//  Beberia
//
//  Created by IMAC on 11/11/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SDWebImage
import Photos
import ImageIO
import MobileCoreServices
import CoreGraphics
import RxSwift
import RxCocoa
import SwiftEntryKit

class CreateGIFViewController: BaseViewController {
    
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var timeSec: UILabel!
    @IBOutlet weak var clvImages: UICollectionView!
    @IBOutlet weak var imageGif: SDAnimatedImageView!
    @IBOutlet weak var sliderTime: UISlider!
    @IBOutlet weak var imageHandle: UIImageView!
    
    var imageFrames = BehaviorRelay.init(value: [UIImage]())
    private var urlGIF = URL.init(string: "")
    var images = [UIImage]()
    var imagesAfterHandel = [UIImage]()
    var timeLoop = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customLeftBarButton()
        title = "Tạo ảnh động"
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory,
                                                    in: .userDomainMask).first
        let path = documentsURL!.appendingPathComponent("animation.gif")
        self.urlGIF = path
        
        imagesAfterHandel = handleImages()
        //  createGIF(with: images, name: path, frameDelay: 1)
        
        //createGIF(with: self.handleImages(), name: path, frameDelay: 1)
        
        imageHandle.image = images[0]
        
        // MARK:  CollectionView Image Frame
        let flowLayoutImageFrame = UICollectionViewFlowLayout()
        // let widthCell1 = ((UIScreen.main.bounds.width - 0) / 4)
        flowLayoutImageFrame.itemSize = CGSize(width: 100 , height: 100)
        flowLayoutImageFrame.scrollDirection = .horizontal
        flowLayoutImageFrame.minimumInteritemSpacing = 0
        flowLayoutImageFrame.minimumLineSpacing = 0
        flowLayoutImageFrame.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvImages.setCollectionViewLayout(flowLayoutImageFrame, animated: true)
        clvImages.showsVerticalScrollIndicator = false
        clvImages.register(R.nib.imageGIFViewCell)
        clvImages.clipsToBounds = false
        clvImages.layer.masksToBounds = false
        
        
        self.imageFrames.accept(self.images)
        self.imageFrames.asObservable().bind(to: self.clvImages.rx.items(cellIdentifier: R.reuseIdentifier.imageGIFViewCell.identifier, cellType: ImageGIFViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
            
            cell.imgImage.image = data
            
        }.disposed(by: disposeBag)
        
        customRightBarButtonWithText(title: "Xong")
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.init(hexString: "ff99bb")
        
        sliderTime.setValue(Float(timeLoop), animated: true)
        self.timeSec.text = "1/s"
        sliderTime.rx.value.skip(1).throttle(DispatchTimeInterval.milliseconds(500), scheduler: MainScheduler.asyncInstance).distinctUntilChanged()
            .subscribe(onNext: { (value) in
                
                self.timeSec.text = "1/\(Int(value))/s"
                if Int(value) == 1{
                    self.timeSec.text = "1/s"
                }
                
                self.timeLoop = Double(1/value)
                
                self.createGIF(with: self.imagesAfterHandel, name: path, frameDelay: self.timeLoop)
            })
            .disposed(by: disposeBag)
        
        //   self.handleImages()
        
        let tapViewImage = UITapGestureRecognizer.init(target: self, action: #selector(self.tapViewImage))
        imageGif.addGestureRecognizer(tapViewImage)
        
    }
    
    @objc func tapViewImage(){
        btnPlay.isHidden = false
        viewImage.isHidden = false
        imageGif.isHidden = true
        imageHandle.image = images[0]
    }
    
    @IBAction func didPressCreateGif(_ sender: UIButton) {
        
        self.createGIF(with: self.imagesAfterHandel, name: self.urlGIF!, loopCount: 0, frameDelay: self.timeLoop)
        
        viewImage.isHidden = true
        imageGif.isHidden = false
        btnPlay.isHidden = true
        
    }
    
    func handleImages()->[UIImage]{

        viewImage.isHidden = false
        imageGif.isHidden = true
        var imageTemp = [UIImage]()
        for image in images {
            imageHandle.image = image
            imageTemp.append(self.mergeImages(imageView: viewImage)!)
        }
        return imageTemp
    }
    
    func mergeImages(imageView: UIView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(imageView.frame.size, false, 0.0)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    @objc override func actionRightBar(){
        guard let url = urlGIF else {
            return
        }
        self.saveImageGIF(url: url)
    }
    
    func createGIF(with images: [UIImage], name: URL, loopCount: Int = 0, frameDelay: Double) {
        
        let fileProperties: CFDictionary = [kCGImagePropertyGIFDictionary as String: [kCGImagePropertyGIFLoopCount as String: 0]]  as CFDictionary
        let frameProperties: CFDictionary = [kCGImagePropertyGIFDictionary as String: [(kCGImagePropertyGIFDelayTime as String): frameDelay]] as CFDictionary
        
        if let destination = CGImageDestinationCreateWithURL(name as CFURL, kUTTypeGIF, images.count, nil) {
            CGImageDestinationSetProperties(destination, fileProperties)
            for (_, image) in images.enumerated() {
                
                //                    let imageREsize : UIImage
                //
                //                    if image.size.width > image.size.height {
                //                        let scale = image.size.width / image.size.height
                //                        imageREsize = image.resizeImage(CGSize.init(width: imageGif.frame.size.width, height: imageGif.frame.size.width / scale))!
                //
                //                    } else if image.size.width < image.size.height{
                //                        let scale = image.size.height /  image.size.width
                //                        imageREsize = image.resizeImage(CGSize(width: imageGif.frame.size.width, height: imageGif.frame.size.width * scale))!
                //                    }else{
                //                        imageREsize = image.resizeImage(CGSize(width: imageGif.frame.size.width, height: imageGif.frame.size.height))!
                //                    }
                
                //                let rect: CGRect
                //                if index == 0{
                //                    rect = CGRect.init(x: 0, y: 0, width: 375, height: 500)
                //                }else{
                //
                //                    rect = CGRect.init(x: 0, y: 0, width: 375, height: 300)
                //                }
                //  let rect = self.getRect(image: image, imageView: imageGif)
                
                let rect = CGRect.init(x: 0, y: 0, width: imageGif.frame.size.width, height: imageGif.frame.size.height)
                
                UIGraphicsBeginImageContext(rect.size)
                image.draw(in: rect)
                guard let img1 = UIGraphicsGetImageFromCurrentImageContext() else {
                    return
                }
                
                if let cgImage = img1.cgImage {
                    CGImageDestinationAddImage(destination, cgImage, frameProperties)
                }
            }
            if !CGImageDestinationFinalize(destination) {
                print("Failed to finalize the image destination")
            }
            
            print(name)
            
            let animatedImage = SDAnimatedImage.init(contentsOfFile: name.path)
            imageGif.image = animatedImage
            self.urlGIF = name
        }
    }
    
    func saveImageGIF(url: URL){
        PHPhotoLibrary.shared().performChanges({
            let request = PHAssetCreationRequest.forAsset()
            request.addResource(with: .photo, fileURL: url, options: nil)
        }) { (success, error) in
            if let error = error {
                //  completion(.failure(error))
                print(error)
            } else {
                DispatchQueue.main.async {
                    print("save image success")
                    let view = ViewToastPopup.instanceFromNib()
                    view.titleLable.text = "Lưu ảnh GIF thành công!"
                    
                    var attributes = EKAttributes.topFloat
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 12, offset: .zero))
                    attributes.displayDuration = 3
                    attributes.position = .top
                    SwiftEntryKit.display(entry: view, using: attributes)
                } 
            }
        }
    }
    
}
