//
//  MenuViewController.swift
//  Beberia
//
//  Created by IMAC on 6/16/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Social
import FBSDKShareKit
import DKImagePickerController
import GoogleSignIn
import SwiftKeychainWrapper
import FBSDKLoginKit
import MessageUI
import UIKit

// MARK: initModel
struct MenuSection {
    var header: String
    var items: [T]
}

struct ItemMenu {
    var title: String
    var image: UIImage
    var imageActive: UIImage
    var isSelected = false
}

extension ItemMenu: IdentifiableType {
    var identity: String {
        return title
    }
}

extension ItemMenu: Equatable {
    static func == (lhs: ItemMenu, rhs: ItemMenu) -> Bool {
        return (lhs.title) == (rhs.title)
    }
}

extension MenuSection: AnimatableSectionModelType{
    typealias T = ItemMenu
    
    var identity: String{
        return header
    }
    
    init(original: Self, items: [Self.T]) {
        self = original
        self.items = items
    }
}

// MARK: Classs

class MenuViewController: BaseViewController {
    
    enum MenuCell: Int, CaseIterable {
        case rankPointVC,
//             gameGift,
//             myGift,
             diary,
             pregnant,
             infoViewController,
             secretViewController,
             lichTiemPhong,
             indexBMI,
             eASYVC,
             babyCrisis,
             scheduleMilk,
             calanderViewController,
             selectImageVC,
             pickerController
//             lichKTVC,
             
        var item: ItemMenu {
            switch self {
            case .rankPointVC: return ItemMenu.init(title: "Tương tác nhận quà",
                                                    image: R.image.tichDiem()!,
                                                    imageActive: R.image.tichDiem()!)
            case .diary: return ItemMenu.init(title: "Nhật ký",
                                              image: R.image.ic_diary_setting()!,
                                              imageActive: R.image.ic_diary_setting()!)
            case .selectImageVC: return ItemMenu.init(title: "Chỉnh sửa ảnh",
                                                      image: R.image.iconEditImage()!,
                                                      imageActive: R.image.iconEditImage()!)
            case .pickerController: return ItemMenu.init(title: "Ảnh động",
                                                         image: R.image.icImageGif()!,
                                                         imageActive: R.image.icKhamthai()!)
            case .infoViewController: return ItemMenu.init(title: "Cẩm nang",
                                                           image: R.image.icCamNang()!,
                                                           imageActive: R.image.icCamNang()!)
            case .pregnant: return ItemMenu.init(title: "Trợ lý mẹ bầu",
                                                 image: R.image.ic_pregnantassitant()!,
                                                 imageActive: R.image.ic_pregnantassitant()!)
            case .lichTiemPhong: return ItemMenu.init(title: "Lịch tiêm phòng cho bé",
                                                      image: R.image.icTiemPhongNew()! ,
                                                      imageActive: R.image.icTiemPhong()!)
            case .indexBMI: return ItemMenu.init(title: "Chỉ số của bé",
                                                 image: R.image.icIndexBaby()!,
                                                 imageActive: R.image.icChiSoCuaBe()!)
            case .eASYVC: return ItemMenu.init(title: "Lịch E.A.S.Y",
                                               image: R.image.icEASYNew()!,
                                               imageActive: R.image.icEASY()!)
            case .babyCrisis: return ItemMenu.init(title: "Bảng tuần khủng hoảng",
                                                   image: R.image.ic_BabyCrisis()!,
                                                   imageActive: R.image.ic_BabyCrisis()!)
            case .scheduleMilk:  return ItemMenu.init(title: "Bảng lượng Sữa",
                                                      image: R.image.img_schedule_milk()!,
                                                      imageActive: R.image.img_schedule_milk()!)
            case .calanderViewController: return ItemMenu.init(title: "Chu kỳ kinh nguyệt",
                                                               image: R.image.icChuKyKNNew()!,
                                                               imageActive: R.image.icChuKyKNUnActive()!)
            case .secretViewController: return ItemMenu.init(title: "Momtok - Tâm sự ẩn danh",
                                                             image: R.image.icMomTokNew()!,
                                                             imageActive: R.image.icMomTokActive()!)
//            case .gameGift: return ItemMenu.init(title: "Lắc quà",
//                                                 image: R.image.ic_game_gift_menu()!,
//                                                 imageActive: R.image.ic_game_gift_menu()!)
//            case .myGift: return ItemMenu.init(title: "Quà của tôi",
//                                               image: R.image.ic_my_gift_menu()!,
//                                               imageActive: R.image.ic_my_gift_menu()!)
            }
        }
    }
    
    enum UpdateCell: Int {
        case share, feedback, order, logout, deleteAccount
    }
  
  struct Constant {
    static let heightPopup: CGFloat = 600
  }
    
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var tbvMenu: UITableView!
    
    var listItemTienIch = [ItemMenu]()
    var listItemOther = [ItemMenu]()
    var dataSource:RxTableViewSectionedAnimatedDataSource<MenuSection>?
    let viewTitleProfile = ViewTitleProfile.instanceFromNib()
    let sections = BehaviorRelay.init(value: [MenuSection]())
    let _fbButton = FBShareButton.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // custom Navication
//        customHideLeftBarButton()
//        customLeftBarView(view: viewTitleProfile)
        viewTitleProfile.nameUserLable.text = UserInfo.shareUserInfo.name
        viewTitleProfile.imgAvatar.kf.setImage(with: URL.init(string: UserInfo.shareUserInfo.avartar))
        viewTitle.addSubview(viewTitleProfile)
        createData()
        
        // Rx
        initRx()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.setBottomBorderColor(color: .clear)
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        let height: CGFloat = 50 //whatever height you want to add to the existing height
//        let bounds = self.navigationController!.navigationBar.bounds
//        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height + height)
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBottomBorderColor(color: .lightGray)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if #available(iOS 15.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        }
    }
    
    
    func initRx(){
        // Check login and Action tap profile
        viewTitleProfile.tapImageAvatar = {
            if UserInfo.shareUserInfo.token == ""{
                Utils.presentLoginVC()
                return
                
            }else{
                
                let profileVC = R.storyboard.main.profileViewController()
                profileVC?.user.id = UserInfo.shareUserInfo.id
                let naviProfile = UINavigationController.init(rootViewController: profileVC!)
                naviProfile.modalPresentationStyle = .fullScreen
                Utils.getTopMostViewController()?.navigationController?.present(naviProfile, animated: true, completion: nil)
            }
        }
        
        
        // bind to data TableView
               let dataSource = RxTableViewSectionedAnimatedDataSource<MenuSection>(
                   
                configureCell: { ds, tv, ip, item in
                    let cell = tv.dequeueReusableCell(withIdentifier: R.nib.menuTableViewCell.identifier)
                        as! MenuTableViewCell
                    cell.titleLAble.text = item.title
                    cell.imageMenu.image = item.isSelected ? item.imageActive : item.image
                  if ip.section == 0 {
                    switch MenuCell(rawValue: ip.row) {
//                    case .gameGift:
//                      cell.imgNew.isHidden = false
//                      cell.imgNew.image = R.image.ic_menu_textUpdate()
//                      cell.imgNew.image = R.image.img_new_rule()
//                      cell.imgNew.image = R.image.ic_join_now()
                    default:
                      cell.imgNew.isHidden = true
                    }
                  }
                    
                    if ip.section == 1 {
                        switch UpdateCell.init(rawValue: ip.row) {
                        default:
                            cell.titleLAble.textColor = UIColor.init(hexString: "7a8190")
                            cell.titleLAble.font = UIFont.init(name: AppFont.NotoSans, size: 14)
                            cell.imgNew.isHidden = true
                        }
                    } else {
                        cell.titleLAble.textColor = UIColor.init(hexString: "7a8190")
                        cell.titleLAble.font = UIFont.init(name: AppFont.NotoSans, size: 14)
                    }
                    return cell
                }
               )
               
               self.dataSource = dataSource
               
               // init Data Section
               sections.accept([MenuSection.init(header: R.string.localizable.menuUtilities(), items: listItemTienIch),
               MenuSection.init(header: R.string.localizable.menuContactAndUpdate(), items: listItemOther)])
               
               sections
                   .bind(to: tbvMenu.rx.items(dataSource: dataSource))
                   .disposed(by: disposeBag)
        
        
        // init TableView
        tbvMenu.register(R.nib.menuTableViewCell)
        tbvMenu.separatorStyle = .none
        tbvMenu.rx.setDelegate(self).disposed(by: disposeBag)
        tbvMenu.rowHeight = 55
        tbvMenu.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 60, right: 0)
        
        // selected Item TabelView
        tbvMenu.rx.itemSelected.subscribe { [weak self] (value) in
            
            guard let self = self else { return }
            guard let indexPath = value.element else { return }
            
            self.tbvMenu.deselectRow(at: indexPath, animated: true)
            if indexPath.section == 0 {
                switch MenuCell(rawValue: indexPath.row) {
                case .rankPointVC:
                    let rankPointVC = RankPointVC.init(nib: R.nib.rankPointVC)
                    //                    let vc = CheckVC(nibName: "CheckVC", bundle: nil)
                    rankPointVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(rankPointVC, animated: true)
                    
//                    let postVC = RegisterFBViewController.init(nib: R.nib.registerFBViewController)
//                    postVC.hidesBottomBarWhenPushed = true
//
//                    self.navigationController?.pushViewController(postVC, animated: true)
//                case .gameGift:
//                    let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
//                    gameVC.modalPresentationStyle = .overFullScreen
//                    self.present(gameVC, animated: true, completion: nil)
//                case .myGift:
//                    let view = PopupVoucher.instanceFromNib()
//                    let _ = self.showPopup(view: view, height: 400)
                case .diary:
//                    let rankPointVC = RankPointVC.init(nib: R.nib.rankPointVC)
//                    //                    let vc = CheckVC(nibName: "CheckVC", bundle: nil)
//                    rankPointVC.hidesBottomBarWhenPushed = true
//                    self.navigationController?.pushViewController(rankPointVC, animated: true)
                    let tabBarVC = R.storyboard.main.diaryViewController()!
                    let naviProfile = UINavigationController.init(rootViewController: tabBarVC)
                    naviProfile.modalPresentationStyle = .fullScreen
                    tabBarVC.openFrom = .guide
                    Utils.getTopMostViewController()?.navigationController?.present(naviProfile, animated: true, completion: nil)
                case .selectImageVC:
                    print("Chỉnh sửa ảnh")
                    let selectImageVC = SelectImageEditViewController.init(nib: R.nib.selectImageEditViewController)
                    selectImageVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(selectImageVC, animated: true)
                case .pickerController:
                    let pickerController = DKImagePickerController()
                    pickerController.showsCancelButton = true
                    pickerController.didSelectAssets = { (assets: [DKAsset]) in
                        if assets.count > 0{
                            var images = [UIImage]()
                            let requestOptions = PHImageRequestOptions()
                            requestOptions.deliveryMode = .highQualityFormat
                            requestOptions.isSynchronous = true
                            requestOptions.resizeMode = .none
                            
                            for asset in assets {
                                asset.fetchOriginalImage(options: requestOptions, completeBlock: { [weak self] image, info in
                                    if let img = image {
                                        images.append(img)
                                        if images.count == assets.count{
                                            let selectImageVC = CreateGIFViewController.init(nib: R.nib.createGIFViewController)
                                            selectImageVC.hidesBottomBarWhenPushed = true
                                            selectImageVC.images = images
                                            self?.navigationController?.pushViewController(selectImageVC, animated: true)
                                        }
                                    }
                                })
                            }
                            
                        }
                    }
                    
                    pickerController.modalPresentationStyle = .overFullScreen
                    self.present(pickerController, animated: true) {}
//                case .lichKTVC:
//                    let lichKTVC = LichKhamThaiViewController.init(nib: R.nib.lichKhamThaiViewController)
//                    lichKTVC.hidesBottomBarWhenPushed = true
//                    self.navigationController?.pushViewController(lichKTVC, animated: true)
                case .infoViewController:
                    guard let lichKTVC = R.storyboard.main.infoViewController() else{ return }
                    lichKTVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(lichKTVC, animated: true)
                case .pregnant:
                    let pregnantVC = PregnantAssistantVC.createVC()
                    pregnantVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(pregnantVC, animated: true)
                case .lichTiemPhong:
                    let lichTiemPhong = VaccinationViewController.init(nib: R.nib.vaccinationViewController)
                    lichTiemPhong.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(lichTiemPhong, animated: true)
                case .indexBMI:
                    let indexBMI = IndexBMIViewController.init(nib: R.nib.indexBMIViewController)
                    indexBMI.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(indexBMI, animated: true)
                case .eASYVC:
                    // UserInfo.shareUserInfo.is_easy = 0
                    // is_cycle == 1 , user đã setup chu kỳ // is_cycle == 0 user chưa setup chu kỳ
                    if UserInfo.shareUserInfo.is_easy > 0 {
                        let eASYVC = EASYActiveViewController.init(nib: R.nib.easyActiveViewController)
                        eASYVC.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(eASYVC, animated: true)
                    }else {
                        let eASYVC = EASYViewController.init(nib: R.nib.easyViewController)
                        eASYVC.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(eASYVC, animated: true)
                    }
                case .babyCrisis:
                    // UserInfo.shareUserInfo.is_easy = 0
                    // is_cycle == 1 , user đã setup chu kỳ // is_cycle == 0 user chưa setup chu kỳ
                    let vc = BabyCrisis.createVC()
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                case .scheduleMilk:
                    // UserInfo.shareUserInfo.is_easy = 0
                    // is_cycle == 1 , user đã setup chu kỳ // is_cycle == 0 user chưa setup chu kỳ
                    let vc = ScheduleMilk.createVC()
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                case .calanderViewController:
                    // is_cycle == 1 , user đã setup chu kỳ // is_cycle == 0 user chưa setup chu kỳ
                    if UserInfo.shareUserInfo.is_cycle > 0 {
                        let calanderViewController = CalanderViewController.init(nib: R.nib.calanderViewController)
                        calanderViewController.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(calanderViewController, animated: true)
                    }else{
                        let chuKyKNViewController = ChuKyKNViewController.init(nib: R.nib.chuKyKNViewController)
                        chuKyKNViewController.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(chuKyKNViewController, animated: true)
                    }
                case .secretViewController:
                    guard let momtokVC = R.storyboard.main.secretViewController() else {
                        return
                    }
                    momtokVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(momtokVC, animated: true)
                case .none:
                    print("tbvMenu.rx.itemSelected.subscribe")
                }
            }else{
                switch indexPath.row {
//                case 0:
//                    if UserInfo.shareUserInfo.verified_card == 0{
//                        let activeCard = ActiveCardViewController.init(nib: R.nib.activeCardViewController)
//                        activeCard.updateVeryfiCard = {
//                            self.listItemTienIch.removeAll()
//                            self.listItemOther.removeAll()
//                            self.createData()
//                            self.sections.accept([MenuSection.init(header: R.string.localizable.menuUtilities(), items: self.listItemTienIch),
//                                                   MenuSection.init(header: R.string.localizable.menuContactAndUpdate(), items: self.listItemOther)])
//                        }
//                        activeCard.hidesBottomBarWhenPushed = true
//                        self.navigationController?.pushViewController(activeCard, animated: true)
//                    }
//                case 1:
//                  //  self?.openAppStore()
//                    let policySale = PolicySaleViewController.init(nib: R.nib.policySaleViewController)
//                    policySale.hidesBottomBarWhenPushed = true
//                    self.navigationController?.pushViewController(policySale, animated: true)
                    
                case 0:
                    self.shareTextOnFaceBook()
//                    if let name = URL(string: LibKey.linkURLAppStore), !name.absoluteString.isEmpty {
//                        let objectsToShare = [name] as [Any]
//                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//
//                        self.present(activityVC, animated: true, completion: nil)
//                    }else  {
//                        // show alert for not available
//                    }

                 //   self._fbButton.sendActions(for: .touchUpInside)
                
//                    let fbContent = ShareLinkContent()
//                    fbContent.quote = """
//                    BEBERIA ĐỒNG HÀNH CÙNG MIỀN TRUNG VƯỢT LŨ
//
//                    Mỗi lượt TẢI ỨNG DỤNG là bạn đã quyên góp 10.000 VNĐ.Sau khi chiến dịch tải ứng dụng kết thúc, tất cả số tiền quyên góp sẽ được chuyển đến quỹ ủng hộ của Báo Thanh Niên.
//
//                    Bước 1: Tải ứng dụng Beberia theo link bên dưới
//                    Bước 2: Gửi lời chào đến các thành viên kèm theo Hashtag #HuongVeMienTrung
//
//                    Chiến dịch được chia thành 2 đợt:
//                    Đợt 1: 28/10 ~ 11/11
//                    Đợt 2: 12/11 ~ 26/11
//                    """
//                  //  fbContent.hashtag = Hashtag.init("#HuongVeMienTrung")
//                    fbContent.contentURL = URL(string: LibKey.linkURLAppStore)!
//
//                    let dialog = ShareDialog.init()
//                    dialog.fromViewController = self
//                    dialog.shareContent = fbContent
//                    dialog.mode = ShareDialog.Mode.shareSheet
//                    dialog.show()

                case 1:
//                    let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
//                    gameVC.modalPresentationStyle = .overFullScreen
//                    self.present(gameVC, animated: true, completion: nil)
                    ManageApp.shared.openLink(link: ConstantApp.shared.linkForum)
                case 2:
                    let orders = OrdersVC.init(nib: R.nib.ordersVC)
                    orders.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(orders, animated: true)
                case 3:
                    self.showAlertViewLogout(title: "Thông báo", message: "Bạn có muốn đăng xuất tài khoản")
                case 4:
                    if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
                        let deleteAccountView: DeletionAccountView = .loadXib()
                        window.addSubview(deleteAccountView)
                        deleteAccountView.snp.makeConstraints { make in
                            make.edges.equalToSuperview()
                        }
                        deleteAccountView.deleteAccountSuccess = { [weak self] in
                            guard let self = self else { return }
                            deleteAccountView.removeFromSuperview()
                            self.logOutAccount()
                        }
                    }
                default:
                    print("tbvMenu.rx.itemSelected.subscribe")
                }
            }
            
        }.disposed(by: disposeBag)
    }
        
    func shareTextOnFaceBook() {
//        let shareImage = SharePhoto()
//        shareImage.image = R.image.imgShareFb()
//        shareImage.isUserGenerated = true
//        shareImage.caption = self.contentFaceBook()
//
//        let content = ShareMediaContent()
//        content.media = [shareImage]
        
        guard let url = URL(string: ConstantApp.shared.linkShareFB) else {
            return
        }

        let content = ShareLinkContent()
        content.contentURL = url
        content.quote = ConstantApp.shared.contentFBShare
        
        let sharedDialoge = ShareDialog()
        sharedDialoge.shareContent = content
        
        sharedDialoge.fromViewController = self
        sharedDialoge.mode = .automatic
        
        
        if(sharedDialoge.canShow)
        {
            sharedDialoge.show()
        }
        else
        {
            print("Install Facebook client app to share image")
        }
    }
    
    private func logOutAccount() {
        
        APIManager.logout(callbackSuccess: { [weak self] (isSucces) in
            guard let self = self else { return }
            
            let defaults = UserDefaults.standard
            KeychainWrapper.standard.set(false, forKey: KeychainKeys.isLoggedIn.rawValue)
            UIApplication.shared.applicationIconBadgeNumber = 0
            UserInfo.shareUserInfo = UserInfo()
            let loginManager = LoginManager()
            loginManager.logOut()
            
            
            defaults.set(false, forKey: "tunrOnOff")
            
            Utils.getAppDelegate().isCheckGetData = false
            
            UserDefault.userIDSocial = ""
            KeychainWrapper.standard.set("", forKey: KeychainKeys.userName.rawValue)
            KeychainWrapper.standard.set("", forKey: KeychainKeys.passWord.rawValue)
            
            // unRegisterDevicePushToken
//                SendBirdManager.share.unRegisterDevicePushToken()
            
            // Disconnect SEndBird
            SocketIOChatManager.shared.disconnectSocket()
            
            
            // logout switch tabbar index == 1
            
        //    self.navigationController?.popToRootViewController(animated: true)
            self.navigationController?.popViewController(animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
                guard let view = Utils.getTopMostViewController() as? MenuViewController else { return }
                guard let tabbar  = view.navigationController?.tabBarController else { return }
                tabbar.selectedIndex = 1
            }
            
        }, failed: { ( error) in
            print(error)
        })
    }
    
    func showAlertViewLogout(title : String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Huỷ", style: .default, handler: nil))
        alert.addAction(UIAlertAction.init(title: "Đồng ý", style: .default, handler: { [weak self] (UIAlertAction) in
            guard let self = self else { return }
            self.logOutAccount()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func presentActionSheet(_ vc: VisualActivityViewController, from view: UIView) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            vc.popoverPresentationController?.sourceView = view
            vc.popoverPresentationController?.sourceRect = view.bounds
            vc.popoverPresentationController?.permittedArrowDirections = [.right, .left]
        }
        
        present(vc, animated: true, completion: nil)
    }
    
    func openAppStore() {
        if let url = URL(string: LibKey.linkURLAppStore),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:]) { (opened) in
                if(opened){
                    print("App Store Opened")
                }
            }
        } else {
            print("Can't Open URL on Simulator")
        }
    }
    
    
    // Create Data
    func createData(){
        listItemTienIch = MenuCell.allCases.map { $0.item }
        
        let list = [
          //                                ItemMenu.init(title: UserInfo.shareUserInfo.verified_card == 0 ? "Kích hoạt thẻ thành viên" : "Đã kích hoạt", image: UserInfo.shareUserInfo.verified_card == 0 ? R.image.icKickThoatThe()! : R.image.icActiveMemberNew()!, imageActive: R.image.icUpdateAppUnACtive()!), ItemMenu.init(title: "Chính sách thẻ giảm giá", image: R.image.icVoucher()!, imageActive: R.image.icUpdateAppUnACtive()!),
          ItemMenu.init(title: "Chia sẻ ứng dụng",
                        image: R.image.icShareMenu()!,
                        imageActive: R.image.icShareMenu()!),
          ItemMenu.init(title: "Gửi ý kiến",
                        image: R.image.icMessageSetting()!,
                        imageActive: R.image.icMessageSetting()!),
          ItemMenu.init(title: "Quản lý đơn hàng",
                        image: R.image.ic_manageorder()!,
                        imageActive: R.image.ic_manageorder()!),
          ItemMenu.init(title: "Đăng xuất",
                        image: R.image.icLogout()!,
                        imageActive: R.image.icLogout()!),
          ItemMenu.init(title: "Chính sách tài khoản",
                        image: R.image.ic_delete_account()!,
                        imageActive: R.image.icSettingMenu()!)
        ]
        listItemOther.append(contentsOf: list)
        
        
    }
    
}

// Custom Header UITableViw
extension MenuViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int)
        -> UIView? {
            let headerView = ViewHeaderMenu.instanceFromNib()
            headerView.nameSectionLable.text = self.dataSource?[section].header
            return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int)
        -> CGFloat {
            return 50
    }
}
extension MenuViewController: SharingDelegate {
    
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        if sharer.shareContent.pageID != nil {
            print("Share: Success")
        }
    }
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print("Share: Fail")
    }
    func sharerDidCancel(_ sharer: Sharing) {
        print("Share: Cancel")
    }
}

