//
//  ActiveCardViewController.swift
//  Beberia
//
//  Created by IMAC on 7/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ActiveCardViewController: BaseViewController {

    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var imageSuccess: UIImageView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var btnVerifyCard: UIButton!
    @IBOutlet weak var otpView: RxOTPView!
    
    var updateVeryfiCard:()->() = {}
    
    var isActiveCard = 0 {
        didSet {
            viewText.isHidden = isActiveCard == 0
            imageSuccess.isHidden = isActiveCard == 0
            titleView.isHidden = isActiveCard == 1
            btnVerifyCard.isHidden = isActiveCard == 1
            otpView.isHidden = isActiveCard == 1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        otpView.rx.code.map { $0.count == 8 }
        .bind(to: btnVerifyCard.rx.isEnabled)
        .disposed(by: disposeBag)
        
        isActiveCard = UserInfo.shareUserInfo.verified_card
        
        customLeftBarButton()
        title = "Kích hoạt thẻ thành viên"
        
        btnVerifyCard.rx.tap.subscribe { (_) in
            self.activeCard()
        }.disposed(by: disposeBag)
    }
    
    
    override func backVC() {
        self.navigationController?.popViewController(animated: true)
        if isActiveCard == 1 { updateVeryfiCard() }
    }
    

}

extension ActiveCardViewController {
    func activeCard(){
        APIManager.verifedNumberCard(number_card: otpView.code, callbackSuccess: { (veryficard) in
            UserInfo.shareUserInfo.verified_card = veryficard
            self.isActiveCard = veryficard
        }) { (error) in
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
}
