
//
//  
//  PregnantAssistantVC.swift
//  Beberia
//
//  Created by haiphan on 21/05/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift
import SwiftyJSON
import Kingfisher

class PregnantAssistantVC: BaseVC {
    
    enum Action: Int, CaseIterable {
        case schedule, vaccination, pregnant, babyFace
    }
    
    enum Label: Int, CaseIterable {
        case title, weight, tall, bornDay, bornDayLate
    }
    
    enum ActionContent: Int, CaseIterable {
        case standardIndex, updateInfo, supersonicImage, detail
    }
    
    struct Constant {
        static let positionX: CGFloat = 0
        static let positionY: CGFloat = 2
        static let blur: CGFloat = 8
        static let spread: CGFloat = 0
    }
    
    // Add here outlets
    @IBOutlet var views: [UIView]!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet var lbs: [UILabel]!
    @IBOutlet var btsContent: [UIButton]!
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var btDayExpecteBorn: UIButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet var hideViews: [UIView]!
    @IBOutlet weak var infoNoChildView: UIStackView!
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var imgBgPregnant: UIImageView!
    // Add here your view model
    private var viewModel: PregnantAssistantVM = PregnantAssistantVM()
    var babyInfo: BabyInfo?
    
//    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_black()!)
        self.setupNavigationVer2()
        self.setupNavigationBariOS15(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                     bgColor: .white,
                                     textColor: R.color.f3C()!)
        self.removeBorder(font: UIFont.notoSansFont(weight: .bold, size: 18),
                          bgColor: .white,
                          textColor: R.color.f3C()!)
        self.navigationController?.isNavigationBarHidden = false
        if let babyInfo = babyInfo, let id = babyInfo.id {
            self.viewModel.getLastUltraSound(babyId: id)
        }
        title = "Trợ lí mẹ bầu"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
}
extension PregnantAssistantVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        
        self.views.forEach { v in
            v.layer.applySketchShadow(color: R.color.d1C1C()!,
                                                      alpha: 1,
                                                      x: Constant.positionX,
                                                      y: Constant.positionY,
                                                      blur: Constant.blur,
                                                      spread: Constant.spread)
        }
        self.updateBaby()
        self.slider.setThumbImage(UIImage(), for: .normal)
        self.btsContent[ActionContent.standardIndex.rawValue].titleLabel?.minimumScaleFactor = 0.5
        self.btsContent[ActionContent.standardIndex.rawValue].titleLabel?.adjustsFontSizeToFitWidth = true
        
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        Action.allCases.forEach { type in
            let bt = self.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                switch type {
                case .schedule:
                    let lichKTVC = LichKhamThaiViewController.init(nib: R.nib.lichKhamThaiViewController)
                    lichKTVC.statusView = .pregnant
                    lichKTVC.hidesBottomBarWhenPushed = true
                    wSelf.navigationController?.pushViewController(lichKTVC, animated: true)
                case .pregnant:
                    let vc = PregnantVaccinVC.createVC()
                    vc.hidesBottomBarWhenPushed = true
                    wSelf.navigationController?.pushViewController(vc, animated: true)
                case .vaccination:
                    let lichTiemPhong = VaccinationViewController.init(nib: R.nib.vaccinationViewController)
                    lichTiemPhong.openFrom = .pregnant
                    lichTiemPhong.hidesBottomBarWhenPushed = true
                    wSelf.navigationController?.pushViewController(lichTiemPhong, animated: true)
                case .babyFace:
                    if let vc = R.storyboard.main.babyFaceVC() {
                        vc.openFrom = .preganant
                        wSelf.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }.disposed(by: self.disposeBag)
        }
        
        ActionContent.allCases.forEach { type in
            let bt = self.btsContent[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                switch type {
                case .updateInfo:
                    wSelf.moveToInfo()
                case .standardIndex:
                    guard let indexBMI = R.storyboard.main.parentBMIViewController() else { return }
                    indexBMI.hidesBottomBarWhenPushed = true
                    indexBMI.openFrom = .pregnantVaccin
                    wSelf.navigationController?.pushViewController(indexBMI, animated: true)
                case .supersonicImage:
                    let vc = UltraSoundPregnancyVC.createVCfromStoryBoard(storyboard: ConstantApp.UIStoryBoardName.pregnantUltraSound.rawValue)
                    vc.babyInfo = wSelf.babyInfo
                    vc.hidesBottomBarWhenPushed = true
                    wSelf.navigationController?.pushViewController(vc, animated: true)
                case .detail:
                    let vc = PregnantDetailVC.createVC()
                    vc.babyInfo = wSelf.babyInfo
                    vc.hidesBottomBarWhenPushed = true
                    wSelf.navigationController?.pushViewController(vc, animated: true)
                }
            }.disposed(by: self.disposeBag)
        }
        
        self.viewModel.lastUltraSound.bind { [weak self] model in
            guard let wSelf = self else { return }
            wSelf.updateBMI(bmiMode: model)
        }.disposed(by: self.disposeBag)
        
//        self.viewModel.listImage.bind { [weak self] list in
//            guard let wSelf = self, let f = list.last else { return }
//            if let url = URL.init(string: f.imageLink ?? "") {
//                KingfisherManager.shared.retrieveImage(with: url) { result in
//                    do {
//                        let image = try result.get().image
//                        wSelf.btsContent[ActionContent.supersonicImage.rawValue].setImage(image, for: .normal)
//                        wSelf.btsContent[ActionContent.supersonicImage.rawValue].setAttributedTitle(nil, for: .normal)
//                        wSelf.btsContent[ActionContent.supersonicImage.rawValue].clipsToBounds = true
//                        wSelf.btsContent[ActionContent.supersonicImage.rawValue].layer.cornerRadius = wSelf.btsContent[ActionContent.supersonicImage.rawValue].frame.height / 2
//                    } catch {
//                        print(error)
//                    }
//                }
//            }
//        }.disposed(by: self.disposeBag)
        
//        self.viewModel.msgError.bind { [weak self] msg in
//            guard let wSelf = self else { return }
//            wSelf.showAlert(title: nil, message: msg)
//        }.disposed(by: self.disposeBag)
        
    }
    
    private func moveToInfo() {
        let vc = PregnantInfoBabyVC.createVC()
        vc.babyInfo = self.babyInfo
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func updateBMI(bmiMode: UltraSoundModel) {
        self.lbs[Label.weight.rawValue].text = "\(bmiMode.weight ?? "")"
        self.lbs[Label.tall.rawValue].text = "\(bmiMode.length ?? "")"
        if let url = URL.init(string: bmiMode.imageLink ?? "") {
            KingfisherManager.shared.retrieveImage(with: url) { result in
                do {
                    let image = try result.get().image
                    self.imgBaby.image = image
                    self.btsContent[ActionContent.supersonicImage.rawValue].setImage(image, for: .normal)
                    self.btsContent[ActionContent.supersonicImage.rawValue].setAttributedTitle(nil, for: .normal)
                    self.btsContent[ActionContent.supersonicImage.rawValue].clipsToBounds = true
                    self.btsContent[ActionContent.supersonicImage.rawValue].layer.cornerRadius = self.btsContent[ActionContent.supersonicImage.rawValue].frame.height / 2
                } catch {
                    print(error)
                }
            }
        }
    }
    
    private func updateBaby() {
        let babys = UserInfo.shareUserInfo.baby_info.filter { ($0.ageRange == 0) }
        var babyInfo: BabyInfo?
        if babys.count >= 1 {
            guard let babyFirst = babys.first else {
                return
            }
            babyInfo = babyFirst
        }
        self.loadBaby(babyInfo: babyInfo)
    }
    
    private func loadBaby(babyInfo: BabyInfo?) {
        guard let babyInfo = babyInfo, let id = babyInfo.id else {
            self.hideViews.forEach { v in
                v.isHidden = true
            }
            self.infoNoChildView.isHidden = false
            self.imgBg.image = R.image.img_bg_pregnant_empty()
            self.imgBgPregnant.image = R.image.img_bg_unpregnant()
            return
        }
        self.hideViews.forEach { v in
            v.isHidden = false
        }
        self.infoNoChildView.isHidden = true
        self.imgBg.image = R.image.img_bg_line_pregnant()
        self.imgBgPregnant.image = R.image.img_bg_pregnant()
        self.babyInfo = babyInfo
        self.viewModel.getLastBMI(babyID: id)
        self.viewModel.getListImageSound(babyId: id)
        self.lbs[Label.title.rawValue].text = babyInfo.nickname
        self.btDayExpecteBorn.setTitle(Double(babyInfo.birthday ?? Int(Date().timeIntervalSince1970)).toDate().toString(format: .ddMMyyyy), for: .normal)
        self.imgBaby.kf.setImage(with: URL.init(string: babyInfo.avatar ?? ""), placeholder: R.image.placeholder())
        switch ManageApp.shared.calcaulateDayBorn(dayExpectedBorn: Double(babyInfo.birthday ?? Int(Date().timeIntervalSince1970)),
                                                  ageRange: babyInfo.ageRange ?? 0) {
        case .pregnanting(let wasPregnant):
            self.lbs[Label.bornDay.rawValue].text = wasPregnant.toWeek()
            self.slider.value = Float(wasPregnant / Double().day)
            self.lbs[Label.bornDayLate.rawValue].text = ""
        case .bornLate(let x):
            self.lbs[Label.bornDay.rawValue].text = "44 Tuần"
            self.slider.value = Float(ManageApp.Constant.calculateDayBorn)
            self.lbs[Label.bornDayLate.rawValue].text = x.toWeek()
        case .wasBorn:
            self.lbs[Label.bornDay.rawValue].text = "44 Tuần"
            self.slider.value = Float(ManageApp.Constant.calculateDayBorn)
            self.lbs[Label.bornDayLate.rawValue].text = ""
        }
    }
    
}
extension PregnantAssistantVC: PregnantInfoBabyDelegate {
    func updateBabyInfo(babyInfo: BabyInfo) {
        self.loadBaby(babyInfo: babyInfo)
    }
}
