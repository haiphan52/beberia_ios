
//
//  ___HEADERFILE___
//
import Foundation
import RxCocoa
import RxSwift
import Siren

class PregnantAssistantVM {
    
    let lastBIM: PublishSubject<BMIModel> = PublishSubject.init()
    let listImage: BehaviorRelay<[UltraSoundModel]> = BehaviorRelay.init(value: [])
    let msgError: PublishSubject<String> = PublishSubject.init()
    let lastUltraSound: PublishSubject<UltraSoundModel> = PublishSubject.init()
    
    private let disposeBag = DisposeBag()
    init() {
        
    }
    
    func getLastBMI(babyID: Int) {
        APIManager.getLastBMI(baby_id: babyID) { [weak self] user in
            guard let wSelf = self else { return }
            wSelf.lastBIM.onNext(user)
        } failed: { [weak self] message in
            guard let wSelf = self else { return }
            wSelf.msgError.onNext(message)
        }
    }
    
    func getLastUltraSound(babyId: Int) {
        APIManager.lastUltraSound(babyId: babyId) { [weak self] model in
            guard let wSelf = self else { return }
            wSelf.lastUltraSound.onNext(model)
        } failed: { [weak self] message in
            guard let wSelf = self else { return }
            wSelf.msgError.onNext(message)
        }
    }
    
    func getListImageSound(babyId: Int) {
        APIManager.listImageSound(babyId: babyId) { [weak self] list in
            guard let wSelf = self else { return }
            wSelf.listImage.accept(list)
        } failed: { [weak self] message in
            guard let wSelf = self else { return }
            wSelf.msgError.onNext(message)
        }
    }
    
}
