//
//  VaccinationViewController.swift
//  Beberia
//
//  Created by IMAC on 6/15/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FFPopup
import SwiftEntryKit
import SVProgressHUD

class VaccinationViewController: BaseViewController {
    
    enum TypeSchedule: Int {
        case baby, mom
    }
    
    enum OpenFrom {
        case other, pregnant
    }
    
    @IBOutlet weak var tbvVaccin: UITableView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btBack: UIButton!
    @IBOutlet weak var btSelectBaby: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    
    let arrayLichTiem = BehaviorRelay.init(value: [VaccineModel]())
    var openFrom: OpenFrom = .other
    //  is_vaccinated: 2, age_id: 0, vaccine_id: 0, baby_id: 0
    var is_vaccinated = 2
    var age_id = 0
    var vaccine_id = 0
    var baby_id = 0 { didSet {
        self.page = 1
        self.arrayLichTiem.accept([])
        getVaccineSchedule(page: 1)
    } }
    var viewSelectedBabyRightBar = ViewSelectedBabyRightBar.instanceFromNib(imageUrl: "")
    var page = 1
    var nextPage = 1
    private let image: UIImageView = UIImageView(image: R.image.img_tooltipBaby())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoaddingManager.initLoadding(view: self.view)
        if self.openFrom == .other {
            customRightBarButton(image: R.image.icSelectedBB()!)
        }
        
        customLeftBarButton()
        let viewTitle = ViewTitleVaccin.instanceFromNib()
        viewTitle.imageTitle.image = R.image.icTiemPhongNew()
        customTitleView(view: viewTitle)
        
        // setup Right Bar
        if Utils.getAppDelegate().babyTiemPhong.id == nil {
            var babys = UserInfo.shareUserInfo.baby_info
            
            babys = babys.filter { (baby) -> Bool in
                return  baby.ageRange != 0 && baby.ageRange != 6
            }
            if babys.count > 0{
                Utils.getAppDelegate().babyTiemPhong = babys[0]
                setupBabyToRightBar(baby: babys[0])
            }else{
                setupBabyToRightBar(baby: Utils.getAppDelegate().babyTiemPhong)
            }
        }else{
            setupBabyToRightBar(baby: Utils.getAppDelegate().babyTiemPhong)
        }
        
        getDataVaccine()
        initRx()
        self.showSelectBaby()
        
        if self.openFrom == .pregnant {
            self.lbTitle.text = "Lịch tiêm cho mẹ bầu"
            self.lbTime.text = "Phân loại"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}

extension VaccinationViewController {
    
    func initRx(){
        
        self.btBack.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.navigationController?.popViewController()
        }.disposed(by: self.disposeBag)
        
        self.btSelectBaby.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.showPopupSelectBaby()
            AppSettings.isSelectBaby = true
            wSelf.image.isHidden = true
        }.disposed(by: self.disposeBag)
        
        btnFilter.rx.tap.subscribe { [weak self] (_) in
            self?.showPopup()
        }.disposed(by: disposeBag)
        
        tbvVaccin.showsVerticalScrollIndicator = false
        tbvVaccin.register(R.nib.vaccinationTableViewCell)
        tbvVaccin.tableFooterView = UIView()
        tbvVaccin.addInfiniteScroll { (tableView) in
            if self.nextPage != 0{
                self.page += 1
                self.getVaccineSchedule(page: self.page)
            }else{
                self.tbvVaccin.endRefreshing(at: .top)
                self.tbvVaccin.finishInfiniteScroll(completion: { (collection) in
                })
            }
        }
        
        arrayLichTiem.asObservable().observeOn(MainScheduler.asyncInstance)
            .bind(to: tbvVaccin.rx.items(cellIdentifier: R.reuseIdentifier.vaccinationTableViewCell.identifier, cellType: VaccinationTableViewCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                cell.titleLable.text = data.age ?? ""
                cell.descLable.text = data.content
                cell.btnSelected.setImage((data.isVaccinated ?? 0) == 0 ? UIImage.init() : R.image.icCheckedVaccine()!, for: .normal)
                
                cell.dataView.backgroundColor = (row % 2 == 0) ? UIColor.white : R.color.f5F5F5()
            }.disposed(by: disposeBag)
        
        tbvVaccin.rx.modelSelected(VaccineModel.self).subscribe { (value) in
            let vacccineDetail = VaccineDetailViewController.init(nib: R.nib.vaccineDetailViewController)
            vacccineDetail.vaccine = value.element!
            vacccineDetail.babyID = self.baby_id
            vacccineDetail.openFrom = self.openFrom
            vacccineDetail.updateData = { vacccine in
                self.handleUpdateData(data: vacccine)
            }
            self.navigationController?.pushViewController(vacccineDetail, animated: true)
        }.disposed(by: disposeBag)
        
        self.actionRightBarButton = {
            self.showPopupSelectBaby()
        }
    }
    
    private func showSelectBaby() {
        if !AppSettings.isSelectBaby {
            self.view.addSubview(image)
            self.image.snp.makeConstraints { make in
                make.top.equalTo(self.btSelectBaby.snp.bottom)
                make.right.equalTo(self.btSelectBaby)
                make.width.equalTo(137)
                make.height.equalTo(36)
            }
        }
    }
    
    func handleUpdateData(data: VaccineModel){
        var arrayTemp = arrayLichTiem.value
        let index = arrayTemp.firstIndex { (vaccin) -> Bool in
            return vaccin.id == data.id
        }
        if index != nil {
            if data.time != arrayTemp[index!].time {
                arrayTemp[index!] = data
                arrayLichTiem.accept(arrayTemp)
            }
        }
    }
    
    func pushViewAadBaby(){
        let addBabyVC = AddBabyViewController.init(nib: R.nib.addBabyViewController)
        addBabyVC.backToUpdateBaby = { baby in
            UserInfo.shareUserInfo.baby_info.insert(baby, at: 0)
            Utils.getAppDelegate().babyTiemPhong = baby
            self.setupBabyToRightBar(baby: baby)
        }
        self.navigationController?.pushViewController(addBabyVC, animated: true)
    }
    
    func setupBabyToRightBar(baby: BabyInfo) {
        guard self.openFrom == .other else {
            btSelectBaby.isHidden = true
            image.isHidden = true
            getVaccineSchedule(page: 1)
            return
        }
        if baby.id == nil {
            self.baby_id = baby.id ?? 0
            customRightBarButton(image: R.image.icSelectedBB()!)
            
        }else{
            self.baby_id = baby.id ?? 0
            let viewSelectedBabyRightBar = ViewSelectedBabyRightBar.instanceFromNib(imageUrl: baby.avatar ?? "")
            self.customRighBarView(view: viewSelectedBabyRightBar)
            viewSelectedBabyRightBar.imageAvatarBaby.kf.setImage(with: URL.init(string: baby.avatar ?? ""), placeholder: R.image.placeholder()!)
        }
    }
    
    func showPopupSelectBaby(){
        let view =  ViewSelectedBaby.instanceFromNib(isCheckBaby: true)
        let popup = showPopup(view: view, height: 300)
        view.didSelectedBaby = { baby in
            if baby.id == 0{
                self.pushViewAadBaby()
            }else {
                Utils.getAppDelegate().babyTiemPhong = baby
                self.setupBabyToRightBar(baby: baby)
            }
            popup.dismiss(animated: true)
        }
    }
    
    func showPopup() {
        guard self.openFrom == .other else {
            self.showFilter()
            return
        }
        if baby_id != 0 {
            self.showFilter()
        } else {
            let view = ViewToastPopup.instanceFromNib()
            view.titleLable.text = "Vui lòng chọn bé để lọc theo trạng thái !"
            self.showToastPopup(view: view)
        }
    }
    
    private func showFilter() {
        let popupDoctor = ViewFilter.instanceFromNib()
        
        popupDoctor.selectedFilter = { is_vaccinated, age_id, vaccine_id in
            self.is_vaccinated = is_vaccinated
            self.age_id = age_id
            self.vaccine_id = vaccine_id
            self.arrayLichTiem.accept([])
            self.page = 1
            self.getVaccineSchedule(page: 1)
            SwiftEntryKit.dismiss()
        }
        
        var attributes = EKAttributes.default
        
        
        attributes.screenBackground =  .color(color: UIColor.init(hexString: "000000", alpha: 0.4)!)
        attributes.position = .center
        // Animate in and out using default translation
        attributes.entryInteraction = .forward
        attributes.screenInteraction = .dismiss
        attributes.displayDuration = .infinity
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 10, offset: .zero))
        attributes.roundCorners = .all(radius: 15)
        attributes.positionConstraints.size = .init(width: .offset(value: 20), height: .intrinsic)
        SwiftEntryKit.display(entry: popupDoctor, using: attributes)
    }
    
    func getVaccineSchedule(page: Int){
        LoaddingManager.startLoadding()
        var id = baby_id
        var type = TypeSchedule.baby.rawValue
        if self.openFrom == .pregnant {
            id = 0
            type = TypeSchedule.mom.rawValue
        }
        
        APIManager.getVaccineSchedule(page: page,
                                      is_vaccinated: is_vaccinated,
                                      age_id: age_id,
                                      vaccine_id: vaccine_id,
                                      baby_id: id,
                                      type: type,
                                      callbackSuccess: { [weak self] (vaccines, page) in
            print(vaccines.count)
            
            guard let self = self else { return }
            
            if self.page != 0 {
                self.arrayLichTiem.accept(self.arrayLichTiem.value + vaccines)
                
            }
            
            self.nextPage = page
            self.tbvVaccin.finishInfiniteScroll(completion: { (collection) in
            })
            LoaddingManager.stopLoadding()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            LoaddingManager.stopLoadding()
        }
    }
    
    func getDataVaccine(){
        if Utils.getAppDelegate().listDataAges.count == 0 {
            APIManager.getAgesVaccine(callbackSuccess: { (vaccines) in
                Utils.getAppDelegate().listDataAges.append(contentsOf: vaccines)
            }) { (error) in
                print(error)
            }
        }
        
        if Utils.getAppDelegate().listDataVacccines.count == 0 {
            APIManager.getDataVaccine(callbackSuccess: { (vaccines) in
                Utils.getAppDelegate().listDataVacccines.append(contentsOf: vaccines)
            }) { (error) in
                print(error)
            }
        }
    }
    
}

