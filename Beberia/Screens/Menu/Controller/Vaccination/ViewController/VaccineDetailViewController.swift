//
//  VaccineDetailViewController.swift
//  Beberia
//
//  Created by IMAC on 6/29/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

struct DataCustomVaccine {
    var title: String?
    var content: String?
}

class VaccineDetailViewController: BaseViewController {

    @IBOutlet weak var tbvVaccineDetail: UITableView!
    
    var vaccine = VaccineModel.init(json: "")
    var updateData:(VaccineModel)->() = {_ in}
    var vaccinesDisplayRx = BehaviorRelay.init(value: [DataCustomVaccine]())
    var dataTitle = ["Tiêm phòng", "Trạng thái", "Thời gian", "Đến lịch tiêm", "Bệnh", "Tên vắc xin", "Nội dung", "Phản ứng phụ", "Bắt buộc"]
    var dataContent = [String]()
    var babyID = 0
    var timeStamp: Double = 0
    var openFrom: VaccinationViewController.OpenFrom = .other
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customLeftBarButton()
        title = vaccine.vaccine ?? ""
        initRx()
        handelData()
    }
    
    override func backVC() {
        updateData(vaccine)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    func handelData(){

        var required = "Có"
        if vaccine.required == 0 {
            required = "Không"
        }
        
        var time = "Chưa tiêm"
        if let t = vaccine.time, t != 0 {
            time = "Đã tiêm \(Utils.getDateFromTimeStamp(timeStamp: Double(vaccine.time! * 1000), dateFormat: Key.DateFormat.DateFormatddMMyyyy))"
        }
        
        dataContent = [vaccine.vaccine ?? "", time ,vaccine.age ?? "",vaccine.timeVaccine ?? "",vaccine.sick ?? "",vaccine.vaccineName ?? "",vaccine.contentDetail ?? "",vaccine.sideEffect ?? "", required]
        
        var vaccinesDisplay = [DataCustomVaccine]()
        for (index, value) in dataTitle.enumerated(){
            var vaccineDisplay = DataCustomVaccine()
            vaccineDisplay.title = value
            vaccineDisplay.content = dataContent[index]
            vaccinesDisplay.append(vaccineDisplay)
        }
        vaccinesDisplayRx.accept(vaccinesDisplay)
    }
    
    func initRx(){
        tbvVaccineDetail.register(R.nib.vaccineTableViewCell)
        tbvVaccineDetail.tableFooterView = UIView()
        
        vaccinesDisplayRx.asObservable().observeOn(MainScheduler.asyncInstance)
            .bind(to: tbvVaccineDetail.rx.items(cellIdentifier: R.reuseIdentifier.vaccineTableViewCell.identifier, cellType: VaccineTableViewCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                cell.titleLable.text = data.title ?? ""
                cell.spaceButton.constant = 0
                cell.heightButton.constant = 0
                cell.btnMark.isHidden = true
                cell.btnUpdate.isHidden = true
                cell.btnUpdate.setTitle("", for: .normal)
                if row == 1 {
                    cell.btnUpdate.isHidden = false
                    cell.spaceButton.constant = 10
                    cell.heightButton.constant = 25
                    cell.btnMark.isHidden = false
                    cell.btnMark.setImage(R.image.ic_unMark()!, for: .normal)
                    cell.btnUpdate.setTitle("Cập nhật ngay", for: .normal)
                    cell.btnUpdate.tag = 13
                    if data.content != "Chưa tiêm"{
                        cell.btnUpdate.tag = 14
                        cell.btnUpdate.setTitle("Cập nhật", for: .normal)
                        cell.btnMark.setImage(R.image.ic_mark()!, for: .normal)
                    }
                }
                
                cell.btnUpdate.rx.tap.subscribe { [weak self] (_) in
                    guard let self = self else { return }
                    if cell.btnUpdate.tag == 13 {
                        self.actionUpdateStatus()
                    }else{
                        self.showPopupAndAction()
                    }
                }.disposed(by: cell.bag)
                
                cell.btnMark.rx.tap.subscribe { (_) in
                   self?.actionUpdateStatus()
                }.disposed(by: cell.bag)
                
                cell.contentLable.attributedText = self?.stringAddFontSizeAttributedString(content: data.content ?? "")
        }.disposed(by: disposeBag)
    }
    
    func showPopupAndAction(){
           let view = ViewCalendar.instanceFromNib()
           view.titlePopup.text = "Chọn ngày"
           let popup = self.showPopup(view: view, height: 360)
           view.tapOK = { date in
               print(date)
               self.timeStamp = Double(Utils.dateStringToTimeStamp(date: date))
               self.markVaccine(schedule_id: self.vaccine.id ?? 0, time: self.timeStamp )
               popup.dismiss(animated: true)
           }
       }


    func actionUpdateStatus(){
        guard self.openFrom == .other else {
            var time = Int(Date().timeIntervalSince1970)
            if self.vaccine.isVaccinated == 1 {
                time = 0
            }
            self.markVaccine(schedule_id: self.vaccine.id ?? 0, time: Double(time))
            return
        }
           if self.babyID != 0 {
               if self.vaccine.time == 0{
                   self.showPopupAndAction()
               }else{
                   self.markVaccine(schedule_id: self.vaccine.id ?? 0, time: 0)
               }
           }else{
               let view = ViewToastPopup.instanceFromNib()
               view.titleLable.text = "Vui lòng chọn bé để thay đổi trạng thái!"
               self.showToastPopup(view: view)
               
           }
       }
    
    func stringAddFontSizeAttributedString(content: String)->NSAttributedString{
           var content1 = content
           content1 = "<span" + " style='font-size: 14px; font-family: \(AppFont.HelveticaNeue)'>" + content1 + "</span>"
           return content1.html2AttributedString ?? NSAttributedString.init(string: "")
       }
}

extension VaccineDetailViewController{
    func markVaccine(schedule_id: Int, time: Double) {
        var type = VaccinationViewController.TypeSchedule.baby.rawValue
        if self.openFrom == .pregnant {
            type = VaccinationViewController.TypeSchedule.mom.rawValue
        }
        APIManager.markVaccine(typeCreateOrUpdate: 0,
                               schedule_id: schedule_id,
                               baby_id: babyID,
                               time: Int(time),
                               type: type,
                               callbackSuccess: { [weak self] (vaccine, pres) in

            if vaccine != nil {
                self?.vaccine = vaccine!
                self?.handelData()
            }

        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
}
