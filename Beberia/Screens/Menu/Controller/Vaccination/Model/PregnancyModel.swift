//
//  PregnancyModel.swift
//
//  Created by IMAC on 6/25/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class PregnancyModel: NSCoding {

 // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let content = "content"
    static let contentDetail = "content_detail"
    static let timePreg = "time_preg"
    static let id = "id"
    static let ages = "ages"
    static let required = "required"
    static let time = "time"
    static let isCared = "is_cared"
    static let isVaccinated = "is_vaccinated"
      
  }

  // MARK: Properties
  public var content: String?
  public var contentDetail: String?
  public var timePreg: String?
  public var id: Int?
  public var ages: String?
  public var required: Int?
  public var time: Int?
  public var isCared: Int?
  public var isVaccinated: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    content = json[SerializationKeys.content].string
    contentDetail = json[SerializationKeys.contentDetail].string
    timePreg = json[SerializationKeys.timePreg].string
    id = json[SerializationKeys.id].int
    ages = json[SerializationKeys.ages].string
    required = json[SerializationKeys.required].int
    time = json[SerializationKeys.time].int
    isCared = json[SerializationKeys.isCared].int
    isVaccinated = json[SerializationKeys.isVaccinated].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = content { dictionary[SerializationKeys.content] = value }
    if let value = contentDetail { dictionary[SerializationKeys.contentDetail] = value }
    if let value = timePreg { dictionary[SerializationKeys.timePreg] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = ages { dictionary[SerializationKeys.ages] = value }
    if let value = required { dictionary[SerializationKeys.required] = value }
    if let value = time { dictionary[SerializationKeys.time] = value }
    if let value = isCared { dictionary[SerializationKeys.isCared] = value }
    if let value = isVaccinated { dictionary[SerializationKeys.isVaccinated] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.content = aDecoder.decodeObject(forKey: SerializationKeys.content) as? String
    self.contentDetail = aDecoder.decodeObject(forKey: SerializationKeys.contentDetail) as? String
    self.timePreg = aDecoder.decodeObject(forKey: SerializationKeys.timePreg) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.ages = aDecoder.decodeObject(forKey: SerializationKeys.ages) as? String
    self.required = aDecoder.decodeObject(forKey: SerializationKeys.required) as? Int
    self.time = aDecoder.decodeObject(forKey: SerializationKeys.time) as? Int
    self.isCared = aDecoder.decodeObject(forKey: SerializationKeys.isCared) as? Int
    self.isVaccinated = aDecoder.decodeObject(forKey: SerializationKeys.isVaccinated) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(content, forKey: SerializationKeys.content)
    aCoder.encode(contentDetail, forKey: SerializationKeys.contentDetail)
    aCoder.encode(timePreg, forKey: SerializationKeys.timePreg)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(ages, forKey: SerializationKeys.ages)
    aCoder.encode(required, forKey: SerializationKeys.required)
    aCoder.encode(time, forKey: SerializationKeys.time)
    aCoder.encode(isCared, forKey: SerializationKeys.isCared)
    aCoder.encode(isVaccinated, forKey: SerializationKeys.isVaccinated)
  }

}
