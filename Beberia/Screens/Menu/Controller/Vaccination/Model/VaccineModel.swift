//
//  VaccineModel.swift
//
//  Created by IMAC on 6/25/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class VaccineModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let content = "content"
    static let contentDetail = "content_detail"
    static let vaccineName = "vaccine_name"
    static let timeVaccine = "time_vaccine"
    static let id = "id"
    static let isVaccinated = "is_vaccinated"
    static let vaccine = "vaccine"
    static let age = "age"
    static let sideEffect = "side_effect"
    static let required = "required"
    static let time = "time"
    static let sick = "sick"
  }

  // MARK: Properties
  public var content: String?
  public var contentDetail: String?
  public var vaccineName: String?
  public var timeVaccine: String?
  public var id: Int?
  public var isVaccinated: Int?
  public var vaccine: String?
  public var age: String?
  public var sideEffect: String?
  public var required: Int?
  public var time: Int?
  public var sick: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    content = json[SerializationKeys.content].string
    contentDetail = json[SerializationKeys.contentDetail].string
    vaccineName = json[SerializationKeys.vaccineName].string
    timeVaccine = json[SerializationKeys.timeVaccine].string
    id = json[SerializationKeys.id].int
    isVaccinated = json[SerializationKeys.isVaccinated].int
    vaccine = json[SerializationKeys.vaccine].string
    age = json[SerializationKeys.age].string
    sideEffect = json[SerializationKeys.sideEffect].string
    required = json[SerializationKeys.required].int
    time = json[SerializationKeys.time].int
    sick = json[SerializationKeys.sick].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = content { dictionary[SerializationKeys.content] = value }
    if let value = contentDetail { dictionary[SerializationKeys.contentDetail] = value }
    if let value = vaccineName { dictionary[SerializationKeys.vaccineName] = value }
    if let value = timeVaccine { dictionary[SerializationKeys.timeVaccine] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = isVaccinated { dictionary[SerializationKeys.isVaccinated] = value }
    if let value = vaccine { dictionary[SerializationKeys.vaccine] = value }
    if let value = age { dictionary[SerializationKeys.age] = value }
    if let value = sideEffect { dictionary[SerializationKeys.sideEffect] = value }
    if let value = required { dictionary[SerializationKeys.required] = value }
    if let value = time { dictionary[SerializationKeys.time] = value }
    if let value = sick { dictionary[SerializationKeys.sick] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.content = aDecoder.decodeObject(forKey: SerializationKeys.content) as? String
    self.contentDetail = aDecoder.decodeObject(forKey: SerializationKeys.contentDetail) as? String
    self.vaccineName = aDecoder.decodeObject(forKey: SerializationKeys.vaccineName) as? String
    self.timeVaccine = aDecoder.decodeObject(forKey: SerializationKeys.timeVaccine) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.isVaccinated = aDecoder.decodeObject(forKey: SerializationKeys.isVaccinated) as? Int
    self.vaccine = aDecoder.decodeObject(forKey: SerializationKeys.vaccine) as? String
    self.age = aDecoder.decodeObject(forKey: SerializationKeys.age) as? String
    self.sideEffect = aDecoder.decodeObject(forKey: SerializationKeys.sideEffect) as? String
    self.required = aDecoder.decodeObject(forKey: SerializationKeys.required) as? Int
    self.time = aDecoder.decodeObject(forKey: SerializationKeys.time) as? Int
    self.sick = aDecoder.decodeObject(forKey: SerializationKeys.sick) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(content, forKey: SerializationKeys.content)
    aCoder.encode(contentDetail, forKey: SerializationKeys.contentDetail)
    aCoder.encode(vaccineName, forKey: SerializationKeys.vaccineName)
    aCoder.encode(timeVaccine, forKey: SerializationKeys.timeVaccine)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(isVaccinated, forKey: SerializationKeys.isVaccinated)
    aCoder.encode(vaccine, forKey: SerializationKeys.vaccine)
    aCoder.encode(age, forKey: SerializationKeys.age)
    aCoder.encode(sideEffect, forKey: SerializationKeys.sideEffect)
    aCoder.encode(required, forKey: SerializationKeys.required)
    aCoder.encode(time, forKey: SerializationKeys.time)
    aCoder.encode(sick, forKey: SerializationKeys.sick)
  }

}
