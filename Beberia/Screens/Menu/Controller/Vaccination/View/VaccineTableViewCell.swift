//
//  VaccineTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 6/29/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VaccineTableViewCell: UITableViewCell {

  //  @IBOutlet weak var widthBtnUpdate: NSLayoutConstraint!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var spaceButton: NSLayoutConstraint!
    @IBOutlet weak var heightButton: NSLayoutConstraint!
    @IBOutlet weak var contentLable: UILabel!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var btnMark: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        spaceButton.constant = 0
        heightButton.constant = 0
    }

    var bag = DisposeBag()

      
      override func prepareForReuse() {
             super.prepareForReuse()
             bag = DisposeBag()
             
             
         }
    
}
