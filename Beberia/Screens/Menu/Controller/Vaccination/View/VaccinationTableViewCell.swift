//
//  VaccinationTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 6/15/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class VaccinationTableViewCell: UITableViewCell {

    @IBOutlet weak var widthView: NSLayoutConstraint!
    @IBOutlet weak var descLable: UILabel!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var btnSelected: UIButton!
    @IBOutlet weak var dataView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
