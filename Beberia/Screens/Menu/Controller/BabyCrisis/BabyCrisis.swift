//
//  BabyCrisis.swift
//  Beberia
//
//  Created by haiphan on 10/02/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift

class BabyCrisis: UIViewController {

    @IBOutlet weak var btBack: UIButton!
    @IBOutlet weak var btLink: UIButton!
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
}
extension BabyCrisis {
    
    private func setupUI() {
        title = "Tuần khủng hoảng của bé"
    }
    
    private func setupRX() {
        self.btBack.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.navigationController?.popViewController(animated: true)
        }.disposed(by: self.disposeBag)
        
        self.btLink.rx.tap.bind { _ in
            ManageApp.shared.openLink(link: "https://www.healthline.com/health/parenting/wonder-weeks-chart")
        }.disposed(by: self.disposeBag)
    }
    
}
