
//
//  
//  PregnantDetailVC.swift
//  Beberia
//
//  Created by haiphan on 08/06/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift
import WebKit

class PregnantDetailVC: BaseVC {
    
    struct Constant {
        static let sizeCell: CGSize = CGSize(width: 42, height: 48)
    }
    
    var babyInfo: BabyInfo?
    
    // Add here outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbNumberWeek: UILabel!
    @IBOutlet weak var lbLenght: UILabel!
    @IBOutlet weak var lbWeight: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    // Add here your view model
    private var viewModel: PregnantDetailVM = PregnantDetailVM()
    private var selectModel: PregnantDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.fd799D()!,
                                 textColor: .white,
                                 isTranslucent: true)
        self.customRightBarButtonTitle(title: "Hôm nay")
        ManageApp.shared.changeStatusBar(statusBarStyle: .lightContent)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ManageApp.shared.changeStatusBar(statusBarStyle: .default)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
extension PregnantDetailVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        title = "Tuần"
        self.collectionView.register(PregnantDetailCell.nib, forCellWithReuseIdentifier: PregnantDetailCell.identifier)
        self.collectionView.delegate = self
        
        if let b = self.babyInfo, let id = b.id {
            self.viewModel.getListDetail(babyId: id)
        }
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        self.viewModel.pregnantDetailModes
            .bind(to: self.collectionView.rx.items(cellIdentifier: PregnantDetailCell.identifier, cellType: PregnantDetailCell.self)) { [weak self] row, data, cell in
                guard let wSelf = self else { return }
                cell.loadValue(model: data)
                
                if let model = wSelf.selectModel, model.id == data.id {
                    cell.backgroundColor = R.color.ff0046()!
                    cell.lbNumberWeek.textColor = .white
                    cell.lbDay.textColor = .white
                } else {
                    cell.backgroundColor = .white
                    cell.lbNumberWeek.textColor = R.color.ff0046()!
                    cell.lbDay.textColor = R.color.ff0046()!
                }
                
            }.disposed(by: disposeBag)
        
        self.viewModel.pregnantDetailModes.bind { [weak self] list in
            guard let wSelf = self else { return }
            if let index = list.firstIndex(where: { $0.week == wSelf.getNumberWeek() }) {
                wSelf.selectModel = list[index]
                wSelf.collectionView.reloadData()
                wSelf.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: true)
                wSelf.loadValue(model: list[index])
            }
        }.disposed(by: self.disposeBag)
        
        self.collectionView.rx.itemSelected.bind { [weak self] idx in
            guard let wSelf = self else { return }
            let item = wSelf.viewModel.pregnantDetailModes.value[idx.row]
            wSelf.selectModel = item
            wSelf.loadValue(model: item)
            wSelf.collectionView.reloadData()
        }.disposed(by: disposeBag)
    }
    
    func updateWebView(text: String) {
        let fontSize = 30
        let fontSetting = "<span style=\"font-size: \(fontSize)\"</span>"
        self.webView.loadHTMLString(fontSetting + text, baseURL: nil)
    }
    
    private func loadValue(model: PregnantDetailModel) {
        self.lbNumberWeek.text = "Thai nhi tuần " + "\(model.week ?? 0)"
        self.lbLenght.text = "Chiều dài: " + "\(model.length ?? "0")"
        self.lbWeight.text = "Cân nặng: " + "\(model.height ?? "0")"
        self.updateWebView(text: model.content ?? "")
        self.img.kf.setImage(with: URL.init(string: model.image ?? ""), placeholder: R.image.placeholder())
    }
    
    private func getNumberWeek() -> Int {
        guard let babyInfo = self.babyInfo else {
            return 1
        }
        switch ManageApp.shared.calcaulateDayBorn(dayExpectedBorn: Double(babyInfo.birthday ?? Int(Date().timeIntervalSince1970)),
                                                  ageRange: babyInfo.ageRange ?? 0) {
        case .pregnanting(let wasPregnant):
            return Int(wasPregnant) / Int(Double().week)
        case .bornLate, .wasBorn:
            return 1
        }
    }
}
extension PregnantDetailVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return Constant.sizeCell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
