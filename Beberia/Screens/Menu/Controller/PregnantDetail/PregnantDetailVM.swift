
//
//  ___HEADERFILE___
//
import Foundation
import RxCocoa
import RxSwift

class PregnantDetailVM {
    let pregnantDetailModes: BehaviorRelay<[PregnantDetailModel]> = BehaviorRelay.init(value: [])
    let msgError: PublishSubject<String> = PublishSubject.init()
    private let disposeBag = DisposeBag()
    init() {
    }
    
    func getListDetail(babyId: Int) {
        APIManager.listPregnantDetail(babyId: babyId) { [weak self] model in
            guard let wSelf = self, let list = model.list else { return }
            wSelf.pregnantDetailModes.accept(list)
        } failed: { [weak self] message in
            guard let wSelf = self else { return }
            wSelf.msgError.onNext(message)
        }

    }
}
