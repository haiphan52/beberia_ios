//
//  PregnantDetailModel.swift
//  Beberia
//
//  Created by haiphan on 08/06/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation


// MARK: - DataClass
struct PregnantDetailModels: Codable {
    let list: [PregnantDetailModel]?
    
    enum CodingKeys: String, CodingKey {
        case list
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        list = try values.decodeIfPresent([PregnantDetailModel].self, forKey: .list)
    }
}

// MARK: - List
struct PregnantDetailModel: Codable {
    let id, week: Int?
    let length, height: String?
    let image: String?
    let content: String?
    
    enum CodingKeys: String, CodingKey {
        case id, week, length, height, image, content
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        week = try values.decodeIfPresent(Int.self, forKey: .week)
        length = try values.decodeIfPresent(String.self, forKey: .length)
        height = try values.decodeIfPresent(String.self, forKey: .height)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        content = try values.decodeIfPresent(String.self, forKey: .content)
    }
}
