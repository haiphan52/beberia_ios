//
//  PregnantDetailCell.swift
//  Beberia
//
//  Created by haiphan on 08/06/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class PregnantDetailCell: UICollectionViewCell {

    @IBOutlet weak var lbNumberWeek: UILabel!
    @IBOutlet weak var lbDay: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
extension PregnantDetailCell {
    
    func loadValue(model: PregnantDetailModel) {
        self.lbNumberWeek.text = "\(model.week ?? 0)"
        self.lbDay.text = nil
    }
}
