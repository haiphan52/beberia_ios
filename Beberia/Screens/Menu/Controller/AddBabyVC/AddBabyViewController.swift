//
//  AddBabyViewController.swift
//  Beberia
//
//  Created by IMAC on 6/17/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AddBabyViewController: BaseViewController {
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnChangeAvatar: UIButton!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var tfNickName: UITextField!
    @IBOutlet weak var bntGirl: UIButton!
    @IBOutlet weak var btnBoy: UIButton!
    @IBOutlet weak var tfBirthDay: UITextField!
    
    let imageSelected = BehaviorRelay<[UIImage]>(value: [])
    private var imagePicker: ImagePicker!
    var viewToast = ViewToastPopup.instanceFromNib()
    var birthday = 0.0
    var arrayImageAvatar = "[]"
    var backToUpdateBaby:(BabyInfo)->() = {_ in}
    var deleteBaby:(BabyInfo)->() = {_ in}
    var editBaby:(BabyInfo)->() = {_ in}
    var baby = BabyInfo.init(json: "")
    
    var gender = 0 {
        didSet {
            
            btnBoy.backgroundColor = gender == 0 ? UIColor.init(hexString: "f2c955",alpha: 0.05) : UIColor.init(hexString: "4e576c",alpha: 0.05)
            btnBoy.layer.borderColor = gender == 0 ? UIColor.init(hexString: "f2c955")?.cgColor : UIColor.init(hexString: "4e576c",alpha: 0.2)?.cgColor
            btnBoy.tintColor = gender == 0 ? UIColor.init(hexString: "f2c955") : UIColor.init(hexString: "4e576c",alpha: 0.2)
            
            bntGirl.backgroundColor = gender == 1 ? UIColor.init(hexString: "f2c955",alpha: 0.05) : UIColor.init(hexString: "4e576c",alpha: 0.05)
            bntGirl.layer.borderColor = gender == 1 ? UIColor.init(hexString: "f2c955")?.cgColor : UIColor.init(hexString: "4e576c",alpha: 0.2)?.cgColor
            bntGirl.tintColor = gender == 1 ? UIColor.init(hexString: "f2c955") : UIColor.init(hexString: "4e576c",alpha: 0.2)
            btnBoy.layer.borderWidth = 1.0
            bntGirl.layer.borderWidth = 1.0
            
        }
    }
    
    func initRx(){
        
        // Action Select Image Group
        btnChangeAvatar.rx.tap.subscribe { [weak self] (_) in
            guard let strongSelf = self else  { return }
            strongSelf.imagePicker.present(from: strongSelf.btnChangeAvatar)
        }.disposed(by: disposeBag)
        
        // Acion send file image
        imageSelected.subscribe { [weak self] (image) in
            guard let strongSelf = self else  { return }
            if image.element!.count > 0{
                guard let data = image.element![0].jpegData(compressionQuality: 0.5) else { return }
                let imageStr = data.base64EncodedString(options: .lineLength64Characters)
                strongSelf.arrayImageAvatar = Utils.json(from: ["\(Key.HeaderJsonImage.HeaderJsonImage)\(imageStr)"])!
                strongSelf.imageAvatar.image = image.element?[0]
            }
        }.disposed(by: disposeBag)
        
        // show Calendar
        tfBirthDay.rx.controlEvent(.editingDidBegin).subscribe { (_) in
            self.view.endEditing(true)
            self.showViewCalendar()
        }.disposed(by: disposeBag)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customLeftBarButton()
        gender = 0
        title = "Thông tin của con"
        imagePicker = ImagePicker(presentationController: self, delegate: self)
        initRx()
        btnDelete.isHidden = true
        
        // check edit baby 
        if baby.id != nil{
            gender = baby.gender ?? 0
            tfBirthDay.text = Utils.getDateFromTimeStamp(timeStamp: Double(baby.birthday! * 1000), dateFormat: Key.DateFormat.DateFormatddMMyyyy)
            birthday = Double(baby.birthday ?? 0)
            tfNickName.text = baby.nickname ?? ""
            btnDelete.isHidden = false
            imageAvatar.kf.setImage(with: URL.init(string: baby.avatar ?? ""), placeholder: R.image.placeholder()!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func showViewCalendar(){
        let viewPopupDelete =  ViewCalendar.instanceFromNib()
        let popup = showPopup(view: viewPopupDelete, height: 370)
        
        viewPopupDelete.tapOK = { date in
            print(date)
            self.tfBirthDay.text = date
            self.birthday = Double(Utils.dateStringToTimeStamp(date: date, formatString: Key.DateFormat.DateFormatddMMyyyy))
            popup.dismiss(animated: true)
        }
        viewPopupDelete.tapCancel = {
            popup.dismiss(animated: true)
        }
    }
    
    @IBAction func didPressChangeAvatar(_ sender: Any) {
        
    }
    
    @IBAction func didPressDelete(_ sender: Any) {
        guard let idBaby = baby.id else {
            return
        }
        Utils.showAlertOKWithAction(controller: self, title: R.string.localizable.commonNoti(), message: "Bạn có chắc chắn muốn xoá?",cancel: true, completion:{
            self.deleteBaby(idBaby: idBaby)
        })
        
    }
    
    @IBAction func didPressSave(_ sender: Any) {
        guard let idBaby = baby.id else {
            addBaby()
            return
        }
        editBaby(idBaby: idBaby)
    }
    
    @IBAction func didPressGender(_ sender: UIButton) {
        gender = sender.tag == 8 ? 0 : 1
    }
}

extension AddBabyViewController: ImagePickerDelegate {
    
    func didSelect(image: UIImage?, fileName: String?, fileUrl: String?) {
        guard let image = image else { return }
        imageSelected.accept([image])
        
    }
}


extension AddBabyViewController {
    
    func addBaby(){
        
        guard let nickName = tfNickName.text, nickName.count > 0 else {
            viewToast.titleLable.text = "Vui lòng nhập biệt danh bé!"
            self.showToastPopup(view: viewToast)
            return
        }
        
        if birthday == 0 {
            viewToast.titleLable.text = "Vui lòng nhập ngày sinh bé!"
            self.showToastPopup(view: viewToast)
            return
        }
        
        APIManager.addBabyProfile(gender: gender, nickname: nickName, birthday: birthday,image: arrayImageAvatar, callbackSuccess: { (baby) in
            print(baby)
            self.backToUpdateBaby(baby)
            self.navigationController?.popViewController(animated: true)
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func deleteBaby(idBaby:Int){
        APIManager.deleteBabyProfile(idBaby: idBaby, callbackSuccess: { (baby) in
            print(baby)
            self.deleteBaby(baby)
            self.navigationController?.popViewController(animated: true)
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func editBaby(idBaby:Int){
        
        guard let nickName = tfNickName.text, nickName.count > 0 else {
            viewToast.titleLable.text = "Vui lòng nhập biệt danh bé!"
            self.showToastPopup(view: viewToast)
            return
        }
        
        if birthday == 0 {
            viewToast.titleLable.text = "Vui lòng nhập ngày sinh bé!"
            self.showToastPopup(view: viewToast)
            return
        }
        
        APIManager.editBabyProfile(id: idBaby, gender: gender, nickname: nickName, birthday: birthday,image: arrayImageAvatar, callbackSuccess: { (baby) in
            print(baby)
            self.editBaby(baby)
            if let index = UserInfo.shareUserInfo.baby_info.firstIndex(where: { $0.id == baby.id }) {
                UserInfo.shareUserInfo.baby_info[index] = baby
            }
            self.navigationController?.popViewController(animated: true)
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
}
