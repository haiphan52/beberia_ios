//
//  IndexBMIViewController.swift
//  Beberia
//
//  Created by IMAC on 6/11/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import SwiftEntryKit
import FFPopup
import RxSwift
import RxCocoa

class IndexBMIViewController: BaseViewController {
    
    @IBOutlet weak var selectedBabyLAble: UILabel!
    @IBOutlet weak var btnSelectedDate: UIButton!
    @IBOutlet weak var heightViewInput: NSLayoutConstraint!
    @IBOutlet weak var spaceTopViewAvatar: NSLayoutConstraint!
    @IBOutlet weak var spaceViewChart: NSLayoutConstraint!
    @IBOutlet weak var spaceViewResult: NSLayoutConstraint!
    @IBOutlet weak var imgResult: UIImageView!
    @IBOutlet weak var btnViewDetail: UIButton!
    @IBOutlet weak var imgChangeBaby: UIImageView!
    @IBOutlet weak var percentHeightLable: UILabel!
 //   @IBOutlet weak var percentChuviDauLable: UILabel!
    @IBOutlet weak var percentWeightLable: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var heightView: UIView!
    //   @IBOutlet weak var viewChuViDau: UIView!
    @IBOutlet weak var cm1Lable: UILabel!
//    @IBOutlet weak var cmLable: UILabel!
    @IBOutlet weak var viewWeight: UIView!
    @IBOutlet weak var viewAvatar: UIView!
    @IBOutlet weak var kgLable: UILabel!
    @IBOutlet weak var tfWeight: UITextField!
    @IBOutlet weak var tfHeight: UITextField!
  //  @IBOutlet weak var tfChuViDau: UITextField!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var nameBabyLable: UILabel!
    @IBOutlet weak var birthdayLable: UILabel!
 //   @IBOutlet weak var viewHeadCircumfe: UIView!
    @IBOutlet weak var viewHeight: UIView!
    @IBOutlet weak var viewChart: UIView!
    @IBOutlet weak var constraintsViewChart: NSLayoutConstraint!
    @IBOutlet weak var btnStandardBaby: UIButton!
    @IBOutlet weak var detailView: UIView!
    
    var bag = DisposeBag()
    var isPregnant = false
    var birthDay = 0.0
    var idBaby = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        initRx()
        //   setupConstant()
        customLeftBarButton()
        let viewtitle = ViewTitleVaccin.instanceFromNib()
        viewtitle.imageTitle.image = R.image.icChiSoCuaBe()!
        viewtitle.titleLable.text = "Chỉ số của bé"
        customRightBarButton(image: UIImage())
        customTitleView(view: viewtitle)
        
    }
    
    func initRx(){
        btnSave.rx.tap.subscribe { [weak self](_) in
            self?.createBMI()
        }.disposed(by: bag)

//        tfDate.rx.controlEvent(.editingDidBegin).subscribe { (_) in
//            self.view.endEditing(true)
//            self.showViewCalendar()
//        }.disposed(by: disposeBag)
        
        tfDate.isEnabled = false
        selectedBabyLAble.text = "Chọn bé"
        btnSelectedDate.rx.tap.subscribe { [weak self](_) in
           // self.view.endEditing(true)
            self?.showViewCalendar()
        }.disposed(by: bag)
        
        btnStandardBaby.rx.tap.subscribe { [weak self](_) in
           guard let indexBMI = R.storyboard.main.parentBMIViewController() else { return }
           indexBMI.hidesBottomBarWhenPushed = true
            self?.navigationController?.pushViewController(indexBMI, animated: true)
        }.disposed(by: bag)
        
        Observable.merge(self.tfWeight.rx.controlEvent(.editingDidBegin).asObservable(),
                         self.tfHeight.rx.controlEvent(.editingDidBegin).asObservable(),
                         self.tfDate.rx.controlEvent(.editingDidBegin).asObservable())
            .bind { [weak self] in
                guard let self = self else { return }
                if !self.btnSave.isEnabled {
                    self.showAlert(title: nil, message: "Vui lòng chọn bé")
                }
            }.disposed(by: self.disposeBag)
    }
    
    func setupUI(){
      //  viewChart.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: viewAvatar.frame.height/2)
        viewAvatar.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: viewAvatar.frame.height/2)
        for item in [kgLable, cm1Lable] { item!.roundCorners([.topRight, .bottomRight], radius: 14) }
        
    //    constraintsViewChart.constant = 0
        viewHeight.isHidden = isPregnant
        setUpDataChart(bmiModel: BMIModel.init(json: ""))
        setUpDataBaby(baby: BabyInfo.init(json: ""))
    }
    
    func showViewCalendar(){
        let viewPopupDelete =  ViewCalendar.instanceFromNib()
        viewPopupDelete.titlePopup.text = "Chọn ngày"
        let popup = showPopup(view: viewPopupDelete, height: 370)
       
        viewPopupDelete.tapOK = { [unowned self] date in
            self.tfDate.text = date
            self.birthDay = Double(Utils.dateStringToTimeStamp(date: date, formatString: Key.DateFormat.DateFormatddMMyyyy))
            popup.dismiss(animated: true)
        }
        viewPopupDelete.tapCancel = {
            popup.dismiss(animated: true)
        }
    }
    
    private func drawCircleValue(circleContainerView: UIView, value: CGFloat?) {
        // round view
        let roundView = UIView(
            frame: CGRect(
                x: circleContainerView.bounds.origin.x,
                y: circleContainerView.bounds.origin.y,
                width: circleContainerView.bounds.size.width - 4,
                height: circleContainerView.bounds.size.height - 4
            )
        )
        roundView.backgroundColor = .white
        roundView.layer.cornerRadius = roundView.frame.size.width / 2

        // bezier path
        let circlePath1 = UIBezierPath(arcCenter: CGPoint (x: roundView.frame.size.width / 2, y: roundView.frame.size.height / 2),
                                      radius: roundView.frame.size.width / 2,
                                      startAngle: CGFloat(-0.5 * .pi),
                                      endAngle: CGFloat(1.5 * .pi),
                                      clockwise: true)
        // circle shape
        let circleShape1 = CAShapeLayer()
        circleShape1.path = circlePath1.cgPath
        circleShape1.strokeColor = R.color.f1F1F1()?.cgColor
        circleShape1.fillColor = UIColor.clear.cgColor
        circleShape1.lineWidth = 8
        // set start and end values
        circleShape1.strokeStart = 0.0
        circleShape1.strokeEnd = 1

        // add sublayer
        roundView.layer.addSublayer(circleShape1)

        let circlePath = UIBezierPath(arcCenter: CGPoint (x: roundView.frame.size.width / 2, y: roundView.frame.size.height / 2),
                                      radius: roundView.frame.size.width / 2,
                                      startAngle: CGFloat(-0.5 * .pi),
                                      endAngle: CGFloat(1.5 * .pi),
                                      clockwise: true)
        // circle shape
        let circleShape = CAShapeLayer()
        circleShape.path = circlePath.cgPath
        if circleContainerView == self.heightView {
            circleShape.strokeColor = R.color.fE()?.cgColor
        } else {
            circleShape.strokeColor = R.color.fd799D()?.cgColor
        }
        
        circleShape.fillColor = UIColor.clear.cgColor
        circleShape.lineWidth = 8
        // set start and end values
        circleShape.strokeStart = 0.0
        circleShape.strokeEnd = value ?? 0
        circleShape.lineJoin = .round
        circleShape.lineCap = .round

        // add sublayer
        roundView.layer.addSublayer(circleShape)
        // add subview
        circleContainerView.addSubview(roundView)
    }
    
    // draw Circlle View
    func setupCircleView(view: UIView, point: CGPoint, endAngle: CGFloat, colorString: String){
        
        let shapeLayer = CAShapeLayer()
        let trackLayer = CAShapeLayer()
        
        let circularPath1 = UIBezierPath.init(arcCenter: point, radius: 34, startAngle: CGFloat(0), endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLayer.path = circularPath1.cgPath
        
        trackLayer.strokeColor = UIColor.init(hexString: "7a8190", alpha: 0.3)!.cgColor
        trackLayer.lineWidth = 8
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = .round
        
        view.layer.addSublayer(trackLayer)
        
        //
        let circularPath = UIBezierPath.init(arcCenter: point, radius: 34, startAngle: 0, endAngle: endAngle, clockwise: true)
        shapeLayer.path = circularPath.cgPath
        
        shapeLayer.strokeColor = UIColor.init(hexString: colorString, alpha: 1)!.cgColor
        shapeLayer.lineWidth = 8
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = .round
        
        view.layer.addSublayer(shapeLayer)
        
        //
        let basicAnimation = CABasicAnimation.init(keyPath: "strokeEnd")
        
        basicAnimation.toValue = 1
        basicAnimation.duration = 2
        basicAnimation.fillMode = .forwards
        basicAnimation.isRemovedOnCompletion = false
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
    }
    
    func pushViewAadBaby(){
        let addBabyVC = AddBabyViewController.init(nib: R.nib.addBabyViewController)
        addBabyVC.backToUpdateBaby = { baby in
            UserInfo.shareUserInfo.baby_info.insert(baby, at: 0)
            self.setUpDataBaby(baby: baby)
        }
        self.navigationController?.pushViewController(addBabyVC, animated: true)
    }
    
    func setUpDataBaby(baby: BabyInfo){
        guard let babyID = baby.id else {
            btnViewDetail.isHidden = true
            detailView.isHidden = true
            btnSave.isEnabled = false
            selectedBabyLAble.text = "Chọn bé"
            return
        }
        
        imgBaby.kf.setImage(with: URL.init(string: baby.avatar ?? ""), placeholder: R.image.placeholder())
        nameBabyLable.text = baby.nickname ?? ""
        birthdayLable.text = Utils.getDateFromTimeStamp(timeStamp: Double(baby.birthday! * 1000), dateFormat: Key.DateFormat.DateFormatddMMyyyy)
        idBaby = babyID
        btnSave.isEnabled = true
        btnViewDetail.isHidden = false
        selectedBabyLAble.text = ""
        detailView.isHidden = false
    }
    
    func setupConstant(){
        if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208:
                print("iPhone 5 or 5S or 5C")
                spaceViewChart.constant = 5
                spaceViewResult.constant = 10
                spaceTopViewAvatar.constant = 0
                heightViewInput.constant = 150
            default:
                print("Unknown")
                spaceViewChart.constant = 20
                spaceViewResult.constant = 30
                spaceTopViewAvatar.constant = 20
                heightViewInput.constant = 170
            }
        }
    }
    
    func setUpDataChart(bmiModel: BMIModel){
        percentHeightLable.text = "\((bmiModel.percent_height ?? 0.0).rounded(toPlaces: 2))%"
        percentWeightLable.text = "\((bmiModel.percent_weight ?? 0.0).rounded(toPlaces: 2))%"
//        setupCircleView(view: viewHeihgt, point: CGPoint.init(x: 34, y: 34), endAngle: CGFloat(endAngleHeight), colorString: "48c7ff")
//        setupCircleView(view: viewWeight, point: CGPoint.init(x: 34, y: 34), endAngle: CGFloat(endAngleWeight), colorString: "ff99bb")
        let valueWeight = (bmiModel.percent_weight ?? 0) / 100
        let valueHeight = (bmiModel.percent_height ?? 0) / 100
        
        self.drawCircleValue(circleContainerView: self.weightView, value: valueWeight)
        self.drawCircleValue(circleContainerView: self.heightView, value: valueHeight)
        
        
        
        guard let percent_height = bmiModel.percent_height else {
            imgResult.image = UIImage.init()
            return
        }
        
        if (percent_height > 0.0) && (percent_height < 5.0) {
            imgResult.image = R.image.imgCSCBLow()!
        }
        else if (percent_height >= 5.0) && (percent_height < 95.0) {
            imgResult.image = R.image.icCSCBTB()!
        } else{
            imgResult.image = R.image.icCSCBHight()!
        }
    }
    
    @IBAction func didPressViewDetail(_ sender: Any) {
        let bmiDeatiVC = BMIDetailViewController.init(nib: R.nib.bmiDetailViewController)
        bmiDeatiVC.babyID = idBaby
        Utils.getTopMostViewController()?.navigationController?.pushViewController(bmiDeatiVC, animated: true)
    }
    
    @IBAction func didPressChangeBaby(_ sender: Any) {
        let view = ViewSelectedBaby.instanceFromNib(isCheckBaby: true)
        let popup = showPopup(view: view, height: 300)
        
        view.didSelectedBaby = { baby in
            
            if baby.id == 0{
                self.pushViewAadBaby()
            }else {
                self.setUpDataBaby(baby: baby)
            }
            popup.dismiss(animated: true)
        }
    }
}

extension IndexBMIViewController {
    func createBMI(){
        
        
        let height = Double(tfHeight.text ?? "")
        let weight = Double(tfWeight.text ?? "")
        
        if height == nil && weight == nil {
            let viewToast = ViewToastPopup.instanceFromNib()
            viewToast.titleLable.text = R.string.localizable.bmiNotificationMissWeightOrHeight()
            self.showToastPopup(view: viewToast)
            return
        }
        
        APIManager.createBMI(height: height ?? 0.0, weight: weight ?? 0.0, circumference: 0, baby_id: idBaby, date: self.birthDay, callbackSuccess: { (bmiModel) in
            print(bmiModel)
            
            self.setUpDataChart(bmiModel: bmiModel)
            
            self.tfHeight.text = ""
            self.tfWeight.text = ""
            self.tfDate.text = ""
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


