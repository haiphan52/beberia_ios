//
//  ParentBMIViewController.swift
//  Beberia
//
//  Created by IMAC on 8/7/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class ParentBMIViewController: BaseViewController, ParentBIMSegmentViewDelegate {
    
    enum OpenFrom {
        case other, pregnantVaccin
    }
    
    var openFrom: OpenFrom = .other
  @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var customSegmentView: ParentBIMSegmentView!
    {
        didSet{
            customSegmentView.setButtonTitles(buttonTitles: ["Bé trai","Bé gái","Mang thai"],
                                              buttonImages: [R.image.img_boybabie(), R.image.img_girlbabie(), R.image.img_pregnant()])
            customSegmentView.selectorViewColor = customSegmentView.selectorTextColor
            customSegmentView.selectorTextColor = customSegmentView.selectorTextColor
            customSegmentView.delegate = self
        }
    }
    
    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customLeftBarButton()
        title = "Chỉ số tiêu chuẩn"
        //        let viewtitle = ViewTitleVaccin.instanceFromNib()
        //        viewtitle.imageTitle.image = R.image.icChiSoCuaBe()!
        //        viewtitle.titleLable.text = "Chỉ số của bé"
        //        customRightBarButton(image: UIImage())
        //        customTitleView(view: viewtitle)
        
        if self.openFrom == .pregnantVaccin {
            self.customSegmentView.isHidden = true
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(150)) {
            self.setupPageMenu()
            if self.openFrom == .pregnantVaccin {
                self.customSegmentView.setupOnlyPregnant()
                self.pageMenu?.moveToPage(2)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // setupPageMenu()
    }
    
    func setupPageMenu(){
        var controllerArray : [UIViewController] = []
        //           let viewIndexBMI = IndexBMIViewController.init(nib: R.nib.indexBMIViewController)
        //           controllerArray.append(viewIndexBMI)
        
        let viewDataStadantBoy = StandardBabyViewController.init(nib: R.nib.standardBabyViewController)
        viewDataStadantBoy.gender = 1
        controllerArray.append(viewDataStadantBoy)
        
        let viewDataStadantGirl = StandardBabyViewController.init(nib: R.nib.standardBabyViewController)
        viewDataStadantGirl.gender = 0
        controllerArray.append(viewDataStadantGirl)
        
        let viewDataStadantMT = StandardBabyViewController.init(nib: R.nib.standardBabyViewController)
        viewDataStadantMT.gender = 3
        controllerArray.append(viewDataStadantMT)
        
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor.init(hexString: AppColor.colorBGMenu, alpha: 1.0)!),
            .selectionIndicatorColor(UIColor.init(hexString: AppColor.colorYellowDark, alpha: 1.0)!),
            .menuItemFont(UIFont.init(name: AppFont.NotoSansMedium, size: 16)!),
            .unselectedMenuItemLabelColor(UIColor.init(hexString: AppColor.colorBlack, alpha: 0.5)!),
            .selectedMenuItemLabelColor(UIColor.black),
            .menuHeight(0),
            .menuItemSeparatorWidth(0),
            .selectionIndicatorHeight(3),
            .menuItemWidth(80),
            .enableHorizontalBounce(true),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .centerMenuItems(true)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.containerView.frame.width, height: self.containerView.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        containerView.addSubview(pageMenu!.view)
    }
    
    
    // MARK: - CustomSegmentedControlDelegate
    
    func changeToIndex(index: Int) {
        print(index)
        pageMenu?.moveToPage(index)
    }
    
}

//MARK: CAPSPageMenu Delegate
extension ParentBMIViewController: CAPSPageMenuDelegate {
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        if self.openFrom != .pregnantVaccin {
            customSegmentView.setIndex(index: index)
        }
        
    }
}
