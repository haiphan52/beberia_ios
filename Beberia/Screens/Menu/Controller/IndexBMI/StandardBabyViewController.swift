//
//  StandardBabyViewController.swift
//  Beberia
//
//  Created by IMAC on 8/7/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class StandardBabyViewController: BaseViewController {
    
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var stackViewButton: UIStackView!
    @IBOutlet weak var tbvStandardBaby: UITableView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btLink: UIButton!
    @IBOutlet weak var viewStackView: UIView!
    
    private var listDatas = BehaviorRelay.init(value: [DataStandardModel]())
    var page = 1
    var gender = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let viewtitle = ViewTitleVaccin.instanceFromNib()
//        viewtitle.imageTitle.image = R.image.icChiSoCuaBe()
//        viewtitle.titleLable.text = "Chỉ số của bé"
        initRX()
        getList(page: 1)
        
        switch gender {
        case 0,1:
            lblWeight.text = "Cân nặng (kg)"
        default:
            lblWeight.text = "Cân nặng (g)"
        }
        
        self.hideStackView()
    }


   override func viewDidLayoutSubviews() {
        self.view.layoutIfNeeded()
//        viewHeader.roundCorners([.topLeft, .topRight], radius: 15)
//        viewHeader.layer.masksToBounds = true
    }
    

    func initRX(){
        tbvStandardBaby.register(R.nib.indexBMITableViewCell)
        tbvStandardBaby.estimatedRowHeight = 45
        tbvStandardBaby.tableFooterView = UIView()
        tbvStandardBaby.separatorStyle = .none

        listDatas.asObservable().observeOn(MainScheduler.asyncInstance)
            .bind(to: tbvStandardBaby.rx.items(cellIdentifier: R.reuseIdentifier.indexBMITableViewCell.identifier, cellType: IndexBMITableViewCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                cell.canNangLable.text = "\((data.weight ?? 0.0).rounded(toPlaces: 1))"
                cell.chieuCaoLable.text = "\((data.height ?? 0.0).rounded(toPlaces: 1))"
                cell.ngayLable.text = data.label ?? ""
//                if let date = data.date  {
//                    cell.ngayLable.text = Utils.getDateFromTimeStamp(timeStamp: Double(date * 1000), dateFormat: Key.DateFormat.DateFormatddMMyyyy)
//                }
                
        }.disposed(by: disposeBag)
        
        self.btLink.rx.tap.bind { _ in
            ManageApp.shared.openLink(link: "https://www.who.int/tools/child-growth-standards/standards/weight-for-length-height")
        }.disposed(by: self.disposeBag)
    }
    
    func hideStackView() {
        self.viewStackView.isHidden = true
    }
    
    func setupTotalPage(page: Int){
        
        
        self.stackViewButton.arrangedSubviews
        .filter({ $0 is UIButton })
        .forEach({ $0.removeFromSuperview() })
        
      //  if self.stackViewButton.arrangedSubviews.count == 0 {
            stackViewButton.axis  = NSLayoutConstraint.Axis.horizontal
            stackViewButton.distribution  = UIStackView.Distribution.equalSpacing
            stackViewButton.alignment = UIStackView.Alignment.center
            stackViewButton.spacing   = 3
            
            for i in 1 ... page {
                let button = UIButton.init()
                button.titleLabel?.font = UIFont.init(name: AppFont.NotoSansBold, size: 18)
                button.setTitleColor(R.color.ffc440Color(), for: .selected)
                button.setTitleColor(R.color.cececE() , for: .normal)
                button.tag = i
                button.setTitle("\(i)", for: .normal)
                button.addTarget(self,action:#selector(tapPage), for: .touchUpInside)
                self.stackViewButton.addArrangedSubview(button)
                
                button.isSelected = false
                
                if self.page == i {
                    button.isSelected = true
                }
            }
        
        
    }
    
    
    @objc func tapPage(button: UIButton){
        page = button.tag
        getList(page: page)
        
    }

}

extension StandardBabyViewController{
    func getList(page: Int){
        APIManager.getDataStandard(gender: gender,page: page, callbackSuccess: { [weak self] (bmiModels, totalPage) in
            self?.listDatas.accept(bmiModels)
            self?.setupTotalPage(page: totalPage)
          //  self?.page = totalPage
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
}
