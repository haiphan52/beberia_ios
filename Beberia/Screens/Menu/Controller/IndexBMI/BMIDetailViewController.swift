//
//  BMIDetailViewController.swift
//  Beberia
//
//  Created by IMAC on 6/18/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BMIDetailViewController: BaseViewController {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tbvBMI: UITableView!

    var listDatas = BehaviorRelay.init(value: [BMIModel]())
    var babyID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customLeftBarButton()
        let viewtitle = ViewTitleVaccin.instanceFromNib()
        viewtitle.imageTitle.image = R.image.icChiSoCuaBe()
        viewtitle.titleLable.text = "Chỉ số của bé"
        customRightBarButton(image: UIImage())
        customTitleView(view: viewtitle)
        initRX()
        getList()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        self.view.layoutIfNeeded()
        viewHeader.roundCorners([.topLeft, .topRight], radius: 15)
        viewHeader.layer.masksToBounds = true 
    }
    

    func initRX(){
        tbvBMI.register(R.nib.indexBMITableViewCell)
        tbvBMI.estimatedRowHeight = 45
        tbvBMI.tableFooterView = UIView()
        tbvBMI.separatorStyle = .none

        listDatas.asObservable().observeOn(MainScheduler.asyncInstance)
            .bind(to: tbvBMI.rx.items(cellIdentifier: R.reuseIdentifier.indexBMITableViewCell.identifier, cellType: IndexBMITableViewCell.self)) { [weak self]  row, data, cell in
                guard let _ = self else { return }
                cell.canNangLable.text = "\((data.weight ?? 0.0).rounded(toPlaces: 2))"
                cell.chieuCaoLable.text = "\((data.height ?? 0.0).rounded(toPlaces: 2))"
                cell.ngayLable.text = ""
                if let date = data.date  {
                    cell.ngayLable.text = Utils.getDateFromTimeStamp(timeStamp: Double(date * 1000), dateFormat: Key.DateFormat.DateFormatddMMyyyy)
                }
                
        }.disposed(by: disposeBag)
    }
}

extension BMIDetailViewController{
    func getList(){
        APIManager.getListBMI(baby_id: babyID, callbackSuccess: { [weak self] (bmiModels) in
            self?.listDatas.accept(bmiModels)
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
}
