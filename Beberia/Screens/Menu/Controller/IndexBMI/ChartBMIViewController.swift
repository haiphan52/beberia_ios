//
//  ChartBMIViewController.swift
//  Beberia
//
//  Created by IMAC on 7/14/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit




class ChartBMIViewController: BaseViewController {
    
    // MARK: IBOutlet
    
    @IBOutlet weak var heightSelectedHeightLable: UILabel!
    @IBOutlet weak var ageSelectedHeightLable: UILabel!
    @IBOutlet weak var dateSelectedHeightLable: UILabel!
    @IBOutlet weak var constraintHeightHeight: NSLayoutConstraint!
    @IBOutlet weak var viewDetailHeight: UIView!
    
    @IBOutlet weak var weightSelectedWeightLable: UILabel!
    @IBOutlet weak var ageSelectedWeightLable: UILabel!
    @IBOutlet weak var dateSelectedWeightLable: UILabel!
    @IBOutlet weak var constraintHeightWeight: NSLayoutConstraint!
    @IBOutlet weak var viewDetailWeight: UIView!
    @IBOutlet weak var chartHeightOrHeadCircumfe: Chart!
    @IBOutlet weak var chartWeight: Chart!
    
    // MARK: Varible
    let KChartWeight = "chartWeight"
    let KChartHeightOrHeadCircumfe = "chartHeightOrHeadCircumfe"
    
    // MARK: Function
        override func viewDidLoad() {
            super.viewDidLoad()
            
            customLeftBarButton()
            setupChart()
            setupUI()
            
        }
    
    func setupUI(){
        viewDetailWeight.isHidden = true
        constraintHeightWeight.constant = 0
        
        viewDetailHeight.isHidden = true
        constraintHeightHeight.constant = 0
    }
    
    func setupChart(){
        
        // setup chart general
        chartWeight.yLabels = [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000]
        chartWeight.xLabels = [10, 15, 20, 25, 30, 35, 40]
        chartWeight.delegate = self
        chartWeight.highlightLineColor = .clear
        chartWeight.nameChart = KChartWeight
        chartWeight.labelColor = R.color.a8190()!
        
        
        //
        chartHeightOrHeadCircumfe.yLabels = [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000]
        chartHeightOrHeadCircumfe.xLabels = [10, 15, 20, 25, 30, 35, 40]
        chartHeightOrHeadCircumfe.delegate = self
        chartHeightOrHeadCircumfe.highlightLineColor = .clear
        chartHeightOrHeadCircumfe.nameChart = KChartHeightOrHeadCircumfe
        chartHeightOrHeadCircumfe.labelColor = R.color.a8190()!
        
        // MARK: setup data weight
        let seriesLimitLowerWeight = ChartSeries.init(data: [
                  
            (x: 0, y: 0),
            (x: 10, y: 300),
            (x: 15, y: 800),
            (x: 20, y: 1200),
            (x: 35, y: 2700),
            (x: 40, y: 3300)
              ])
              seriesLimitLowerWeight.color = ChartColors.limitLowerColor()
              seriesLimitLowerWeight.area = true

              // Format the labels with a unit
            //  chartWeight.xLabelsFormatter = { String(Int(round($1))) + "t" }
            //  chartWeight.yLabelsFormatter = { String(Int(round($1))) + "g" }

              let seriesLimitHightWeight = ChartSeries.init(data: [
                  (x: 0, y: 0),
                  (x: 10, y: 400),
                  (x: 15, y: 1000),
                  (x: 20, y: 1500),
                  (x: 25, y: 1500),
                  (x: 30, y: 1500),
                  (x: 35, y: 3000),
                  (x: 40, y: 3600)
              ])
        
            //  seriesLimitLower.isCustomCircle = false
              seriesLimitHightWeight.color = ChartColors.limitHightColor()
              seriesLimitHightWeight.area = true

              // A partially filled series
              let seriesMyBabyWeight = ChartSeries.init(data: [(x: 10, y: 400), (x: 20, y: 1000)])
              seriesMyBabyWeight.color = ChartColors.weightColor()
              seriesMyBabyWeight.isCustomCircle = true

              chartWeight.add([seriesLimitLowerWeight,seriesLimitHightWeight, seriesMyBabyWeight])
        
        
        // MARK: setup data heihgt or head circumference
        let seriesLimitLower = ChartSeries.init(data: [
                  (x: 0, y: 0),
                  (x: 10, y: 300),
                  (x: 15, y: 800),
                  (x: 20, y: 1200),
                  (x: 35, y: 2700),
                  (x: 40, y: 3300)
              ])
              seriesLimitLower.color = ChartColors.limitLowerColor()
              seriesLimitLower.area = true

              // Format the labels with a unit
            //  chartWeight.xLabelsFormatter = { String(Int(round($1))) + "t" }
            //  chartWeight.yLabelsFormatter = { String(Int(round($1))) + "g" }

              let seriesLimitHight = ChartSeries.init(data: [
                  
                (x: 0, y: 0),
                (x: 10, y: 400),
                (x: 15, y: 1000),
                (x: 20, y: 1500),
                (x: 35, y: 3000),
                (x: 40, y: 3600)
              ])
        
            //  seriesLimitLower.isCustomCircle = false
              seriesLimitHight.color = ChartColors.limitHightColor()
              seriesLimitHight.area = true

              // A partially filled series
              let seriesMyBaby = ChartSeries.init(data: [(x: 10, y: 400), (x: 20, y: 1000)])
              seriesMyBaby.color = ChartColors.heightColor()
              seriesMyBaby.isCustomCircle = true

              chartHeightOrHeadCircumfe.add([seriesLimitLower,seriesLimitHight, seriesMyBaby])
    }

}


extension ChartBMIViewController :ChartDelegate {
    func didTouchingPoint(_ chart: Chart, _ index: Int, _ name: String) {

        switch chart.nameChart {
            
        case KChartWeight:
     
            viewDetailWeight.isHidden = false
            constraintHeightWeight.constant = 84
            UIView.animate(withDuration: 0.3) {
                self.updateViewConstraints()
                self.view.layoutIfNeeded()
            }
            
        case KChartHeightOrHeadCircumfe:
            print(index)
            
        default:
            print(index)
        }
    }
    
    func didTouchChart(_ chart: Chart, indexes: Array<Int?>, x: Double, left: CGFloat) {
    //           for (seriesIndex, dataIndex) in indexes.enumerated() {
    //               if let value = chart.valueForSeries(seriesIndex, atIndex: dataIndex) {
    //                   print("Touched series: \(seriesIndex): data index: \(dataIndex!); series value: \(value); x-axis value: \(x) (from left: \(left))")
    //               }
    //           }
           }

     func didFinishTouchingChart(_ chart: Chart) {
         //
     }
     
     func didEndTouchingChart(_ chart: Chart) {
         //
     }
}
