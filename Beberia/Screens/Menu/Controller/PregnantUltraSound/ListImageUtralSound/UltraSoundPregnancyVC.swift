
//
//  
//  UltraSoundPregnancyVC.swift
//  Beberia
//
//  Created by haiphan on 06/06/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

class UltraSoundPregnancyVC: BaseVC {
    
    struct Constant {
        static let space: CGFloat = 8
    }
    var babyInfo: BabyInfo?
    
    // Add here outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // Add here your view model
    private var viewModel: UltraSoundPregnancyVM = UltraSoundPregnancyVM()
    private var sizeCell: CGSize = CGSize.zero
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.fd799D()!,
                                 textColor: .white,
                                 isTranslucent: true)
        ManageApp.shared.changeStatusBar(statusBarStyle: .lightContent)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ManageApp.shared.changeStatusBar(statusBarStyle: .default)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
}
extension UltraSoundPregnancyVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        title = "Ảnh Siêu Âm"
        self.collectionView.register(UltraSoundCell.nib, forCellWithReuseIdentifier: UltraSoundCell.identifier)
        self.collectionView.register(AddImageCell.nib, forCellWithReuseIdentifier: AddImageCell.identifier)
        self.collectionView.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.collectionView.reloadData()
        }
        self.getListImage()
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        self.viewModel.listImage.bind(to: collectionView.rx.items) { [weak self] (cl, row, item) -> UICollectionViewCell in
            guard let wSelf = self else { fatalError() }
            if row == 0 {
                guard let cell = cl.dequeueReusableCell(withReuseIdentifier: AddImageCell.identifier, for: IndexPath(row: row, section: 0)) as? AddImageCell else {
                    fatalError()
                }
                cell.updateSizeImage(value: wSelf.calculateSizeCell().width)
                return cell
            } else {
                guard let cell = cl.dequeueReusableCell(withReuseIdentifier: UltraSoundCell.identifier, for: IndexPath(row: row, section: 0)) as? UltraSoundCell else {
                    fatalError()
                }
                cell.updateSizeImage(value: wSelf.calculateSizeCell().width)
                cell.loadValue(model: item)
                return cell
            }

        }.disposed(by: disposeBag)
        
        self.collectionView.rx.itemSelected.bind { [weak self] idx in
            guard let wSelf = self else { return }
            let vc = AddImageUtraSoundVC.createVCfromStoryBoard(storyboard: ConstantApp.UIStoryBoardName.pregnantUltraSound.rawValue)
            vc.hidesBottomBarWhenPushed = true
            vc.babyInfo = wSelf.babyInfo
            if idx.row != 0 {
                let item = wSelf.viewModel.listImage.value[idx.row]
                vc.soundModel = item
                vc.statusView = .update
            }
            vc.delegate = self
            wSelf.navigationController?.pushViewController(vc, animated: true)
        }.disposed(by: disposeBag)
    }
    
    private func getListImage() {
        guard let b = self.babyInfo, let babyId = b.id else {
            return
        }
        self.viewModel.getListImageSound(babyId: babyId)
    }
    private func calculateSizeCell() -> CGSize {
        let spaceBetweenCell = 8
        let widthCollection: CGFloat = CGFloat((Int(self.collectionView.frame.width) - (spaceBetweenCell * 2)) / 3)
        return CGSize(width: widthCollection, height: widthCollection)
    }
}
extension UltraSoundPregnancyVC: AddImageUtraSound {
    func getList() {
        self.getListImage()
    }
}
extension UltraSoundPregnancyVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.calculateSizeCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constant.space
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constant.space
    }
}
