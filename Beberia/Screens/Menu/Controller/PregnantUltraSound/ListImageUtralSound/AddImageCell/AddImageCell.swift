//
//  AddImageCell.swift
//  Beberia
//
//  Created by haiphan on 07/06/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class AddImageCell: UICollectionViewCell {

    @IBOutlet weak var hImage: NSLayoutConstraint!
    @IBOutlet weak var wImage: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
extension AddImageCell {
    
    func updateSizeImage(value: CGFloat) {
        self.hImage.constant = value
        self.wImage.constant = value
    }
    
}
