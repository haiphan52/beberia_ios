//
//  UltraSoundCell.swift
//  Beberia
//
//  Created by haiphan on 07/06/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class UltraSoundCell: UICollectionViewCell {

    @IBOutlet weak var hImage: NSLayoutConstraint!
    @IBOutlet weak var wImage: NSLayoutConstraint!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbtitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
extension UltraSoundCell {
    
    func loadValue(model: UltraSoundModel) {
        self.img.kf.setImage(with: URL.init(string: model.imageLink ?? ""), placeholder: R.image.placeholder())
        let date = model.dayUltra?.toDate().toString(format: .ddMMyyyy)
        let text = "\(model.week ?? 0) Tuần \(Int(model.dayNumber ?? 0)) Ngày - \(date ?? "")"
        self.lbtitle.text = text
    }
    
    func updateSizeImage(value: CGFloat) {
        self.hImage.constant = value
        self.wImage.constant = value
    }
    
}
