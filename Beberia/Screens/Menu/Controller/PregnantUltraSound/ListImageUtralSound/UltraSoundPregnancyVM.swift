
//
//  ___HEADERFILE___
//
import Foundation
import RxCocoa
import RxSwift

class UltraSoundPregnancyVM {
    
    let listImage: BehaviorRelay<[UltraSoundModel]> = BehaviorRelay.init(value: [])
    let msgError: PublishSubject<String> = PublishSubject.init()
    
    private let disposeBag = DisposeBag()
    
    init() {
        
    }
    
    func getListImageSound(babyId: Int) {
        APIManager.listImageSound(babyId: babyId) { [weak self] list in
            guard let wSelf = self else { return }
            let l = [UltraSoundModel.valueDefault] + list
            wSelf.listImage.accept(l.sorted { ($0.dayUltra ?? 0) < ($1.dayUltra ?? 0 )})
        } failed: { [weak self] message in
            guard let wSelf = self else { return }
            wSelf.msgError.onNext(message)
        }
    }
    
}
