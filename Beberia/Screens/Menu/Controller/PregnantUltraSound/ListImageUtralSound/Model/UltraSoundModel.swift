//
//  UltraSoundModel.swift
//  Beberia
//
//  Created by haiphan on 08/06/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
// MARK: - Datum
struct UltraSoundModel: Codable {
    let id, babyID: Int?
    let imageLink: String?
    let week: Int?
    let length, weight: String?
    let dayUltra: Double?
    let dayNumber: Double?

    enum CodingKeys: String, CodingKey {
        case id
        case babyID = "baby_id"
        case imageLink = "image_link"
        case week, length, weight
        case dayUltra = "day_ultra"
        case dayNumber =  "day_number"
    }
    
    public init() {
        self.id = nil
        self.babyID = nil
        self.imageLink = nil
        self.week = nil
        self.length = nil
        self.weight = nil
        self.dayUltra = nil
        self.dayNumber = nil
    }
    
    static var valueDefault = UltraSoundModel()
}
