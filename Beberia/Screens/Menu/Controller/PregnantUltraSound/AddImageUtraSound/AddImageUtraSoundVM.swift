
//
//  ___HEADERFILE___
//
import Foundation
import RxCocoa
import RxSwift

class AddImageUtraSoundVM {
    
    let addSuccess: PublishSubject<Void> = PublishSubject.init()
    let msgError: PublishSubject<String> = PublishSubject.init()
    
    private let disposeBag = DisposeBag()
    init() {
        
    }
    
    func addImageSound(babyId: Int, media: String, week: Int, datePreg: Double, day: Int, length: String, weight: String) {
        APIManager.addUltraSound(babyId: babyId,
                                 imageBase64: media,
                                 week: week,
                                 day: day,
                                 datePreg: datePreg,
                                 length: length,
                                 weight: weight) { [weak self] in
            guard let wSelf = self else { return }
            wSelf.addSuccess.onNext(())
        } failed: { [weak self] message in
            guard let wSelf = self else { return }
            wSelf.msgError.onNext(message)
        }
    }
    
    func updateImageSound(babyId: Int, media: String, week: Int, datePreg: Double, day: Int, length: String, weight: String, imageSound: Int) {
        APIManager.updateUltraSound(babyId: babyId,
                                    imageBase64: media,
                                    week: week,
                                    day: day,
                                    datePreg: datePreg,
                                    length: length,
                                    weight: weight,
                                    imageSound: imageSound) { [weak self] in
            guard let wSelf = self else { return }
            wSelf.addSuccess.onNext(())
        } failed: { [weak self] message in
            guard let wSelf = self else { return }
            wSelf.msgError.onNext(message)
        }
    }
}
