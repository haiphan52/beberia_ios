
//
//  
//  AddImageUtraSoundVC.swift
//  Beberia
//
//  Created by haiphan on 08/06/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift
import SVProgressHUD

protocol AddImageUtraSound: AnyObject {
    func getList()
}

class AddImageUtraSoundVC: BaseVC {
    
    enum StatusView {
        case add, update
    }
    
    // Add here outlets
    var babyInfo: BabyInfo?
    var soundModel: UltraSoundModel?
    var image: UIImage?
    var statusView: StatusView = .add
    weak var delegate: AddImageUtraSound?
    
    @IBOutlet weak var tfExpectedBorn: UITextField!
    @IBOutlet weak var tfNumberWeek: UITextField!
    @IBOutlet weak var tfNumberDay: UITextField!
    @IBOutlet weak var tfLenght: UITextField!
    @IBOutlet weak var tfWeight: UITextField!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btAddImage: UIButton!
    @IBOutlet weak var dayErrorView: UIView!
    @IBOutlet weak var weekErrorView: UIView!
    @IBOutlet weak var lengthErroView: UIView!
    @IBOutlet weak var weightErrorView: UIView!
    @IBOutlet weak var numberDayView: UIView!
    @IBOutlet weak var btConfirm: UIButton!
    @IBOutlet weak var addImageView: UIView!
    
    // Add here your view model
    private var viewModel: AddImageUtraSoundVM = AddImageUtraSoundVM()
    private let dateFormat: DateFormatter = DateFormatter()
    private let datePicker: UIDatePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
//        self.customRightBarButtonVer2(imgArrow: R.image.ic_done_white()!)
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.fd799D()!,
                                 textColor: .white,
                                 isTranslucent: true)
        ManageApp.shared.changeStatusBar(statusBarStyle: .lightContent)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ManageApp.shared.changeStatusBar(statusBarStyle: .default)
        SVProgressHUD.dismiss()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
}
extension AddImageUtraSoundVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        if let img = self.image {
            self.img.image = img
        }
        
        if let model = self.soundModel {
            self.img.kf.setImage(with: URL.init(string: model.imageLink ?? ""), placeholder: R.image.placeholder())
            self.tfNumberWeek.text = "\(model.week ?? 0)"
            self.tfExpectedBorn.text = model.dayUltra?.toDate().toString(format: .ddMMyyyy)
            self.tfNumberDay.text = "\(model.dayNumber ?? 0)"
            self.tfLenght.text = "\(model.length ?? "")"
            self.tfWeight.text = "\(model.weight ?? "")"
            self.img.isHidden = false
            self.addImageView.isHidden = true
        }
        self.setupDatePicker()
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer()
        self.img.addGestureRecognizer(tapGesture)
        tapGesture.rx.event.bind { [weak self] _ in
            guard let self = self else { return }
            self.presentImagePicker()
        }.disposed(by: self.disposeBag)
        
        if self.statusView == .update {
            self.btConfirm.setTitle("Chỉnh sửa thông tin", for: .normal)
        }
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        self.btAddImage.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.presentImagePicker()
        }.disposed(by: self.disposeBag)
        
        self.btConfirm.rx.tap.bind { [weak self] _ in
            guard let wSelf = self,
                  let img = wSelf.img.image,
                  let babyInfo = wSelf.babyInfo,
                  let babyId = babyInfo.id else {
                return
            }
            
            guard let text = wSelf.tfExpectedBorn.text,
                  !text.isEmpty,
                  let date = text.convertToDate(formatDate: .ddMMyyyy) else {
                wSelf.dayErrorView.isHidden = false
                return
            }
                        
            guard let week = wSelf.tfNumberWeek.text,
                  !week.isEmpty,
                  let number = Int(week),
                  number <= ManageApp.Constant.limitWeekPregnant else {
                wSelf.weekErrorView.isHidden = false
                wSelf.checkNumebrWeek()
                return
            }
            
            guard let numberDay = wSelf.tfNumberDay.text, !numberDay.isEmpty else {
                wSelf.numberDayView.isHidden = false
                return
            }
            
            guard let length = wSelf.tfLenght.text, !length.isEmpty else {
                wSelf.lengthErroView.isHidden = false
                return
            }
            
            guard let weight = wSelf.tfWeight.text, !weight.isEmpty else {
                wSelf.weightErrorView.isHidden = false
                return
            }
            
            SVProgressHUD.show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                switch wSelf.statusView {
                case .add:
                    let media = Key.HeaderJsonImage.HeaderJsonImage + (img.convertImageTobase64(format: .jpeg(1), image: img) ?? "")
                    wSelf.viewModel.addImageSound(babyId: babyId,
                                                  media: media,
                                                  week: number,
                                                  datePreg: date.timeIntervalSince1970,
                                                  day: Int(numberDay) ?? 0,
                                                  length: length,
                                                  weight: weight)
                case .update:
                    guard let s = wSelf.soundModel, let id = s.id else {
                        return
                    }
                    let media = Key.HeaderJsonImage.HeaderJsonImage + (img.convertImageTobase64(format: .jpeg(1), image: img) ?? "")
                    wSelf.viewModel.updateImageSound(babyId: babyId,
                                                     media: media,
                                                     week: number,
                                                     datePreg: date.timeIntervalSince1970,
                                                     day: Int(numberDay) ?? 0,
                                                     length: length,
                                                     weight: weight, imageSound: id)
                }
            }
        }.disposed(by: self.disposeBag)
        
        self.tfExpectedBorn.rx.controlEvent([.editingChanged, .editingDidEnd]).bind(onNext: { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.dayErrorView.isHidden = (!(wSelf.tfExpectedBorn.text?.isEmpty ?? true)) ? true : false
        }).disposed(by: disposeBag)
        
        self.tfNumberWeek.rx.controlEvent([.editingChanged, .editingDidEnd]).bind(onNext: { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.weekErrorView.isHidden = (!(wSelf.tfNumberWeek.text?.isEmpty ?? true)) ? true : false
        }).disposed(by: disposeBag)
        
        self.tfWeight.rx.controlEvent([.editingChanged, .editingDidEnd]).bind(onNext: { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.weightErrorView.isHidden = (!(wSelf.tfWeight.text?.isEmpty ?? true)) ? true : false
        }).disposed(by: disposeBag)
        
        self.tfLenght.rx.controlEvent([.editingChanged, .editingDidEnd]).bind(onNext: { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.lengthErroView.isHidden = (!(wSelf.tfLenght.text?.isEmpty ?? true)) ? true : false
        }).disposed(by: disposeBag)
        
        self.tfNumberDay.rx.controlEvent([.editingChanged, .editingDidEnd]).bind(onNext: { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.numberDayView.isHidden = (!(wSelf.tfNumberDay.text?.isEmpty ?? true)) ? true : false
        }).disposed(by: disposeBag)
        
        self.viewModel.addSuccess.bind { [weak self] _ in
            guard let wSelf = self else { return }
            SVProgressHUD.dismiss()
            wSelf.navigationController?.popViewController(animated: true, {
                wSelf.delegate?.getList()
            })
        }.disposed(by: self.disposeBag)
        
        self.viewModel.msgError.bind { [weak self] text in
            guard let wSelf = self else { return }
            wSelf.showAlert(title: nil, message: text)
        }.disposed(by: self.disposeBag)
        
    }
    
    private func getBirthDay() -> Date? {
        if let textReal = self.tfExpectedBorn.text, !textReal.isEmpty {
            return textReal.convertToDate(formatDate: .ddMMyyyy)
        }
        return nil
    }
    
    private func checkNumebrWeek() {
        let vc = PopupCancelationOrder.createVC()
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.statusView = .pregnant
        self.present(vc, animated: true, completion: nil)
        vc.loadValuePregnant(strDay: "")
    }
    
    private func presentImagePicker() {
        let vc = UIImagePickerController()
        vc.delegate = self
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Setup date Picker®
    private func setupDatePicker() {
        self.dateFormat.dateStyle = DateFormatter.Style.medium
        self.dateFormat.timeStyle = DateFormatter.Style.none
        self.dateFormat.dateFormat = Key.DateFormat.DateFormatddMMyyyy
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        self.datePicker.addTarget(self, action: #selector(updateDateField(sender:)), for: UIControl.Event.valueChanged)
        self.tfExpectedBorn.inputView = datePicker
        self.tfExpectedBorn.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        let image = R.image.ic_pregnant_calcelator()
        imageView.image = image
        self.tfExpectedBorn.rightView = imageView
    }
    
    @objc private func updateDateField(sender: UIDatePicker) {
        self.tfExpectedBorn.text = dateFormat.string(from: datePicker.date)
    }
}
extension AddImageUtraSoundVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            picker.dismiss(animated: true) { [weak self] in
                guard let wSelf = self else { return }
                wSelf.img.image = image
                wSelf.addImageView.isHidden = true
                wSelf.img.isHidden = false
            }
        }
    }
}
