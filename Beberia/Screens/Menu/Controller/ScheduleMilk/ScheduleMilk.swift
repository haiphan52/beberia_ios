//
//  ScheduleMilk.swift
//  Beberia
//
//  Created by haiphan on 23/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift

class ScheduleMilk: UIViewController {

    @IBOutlet weak var btBack: UIButton!
    @IBOutlet weak var btLink: UIButton!
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
}
extension ScheduleMilk {
    
    private func setupUI() {
        title = "Bảng lượng sữa"
    }
    
    private func setupRX() {
        self.btBack.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.navigationController?.popViewController(animated: true)
        }.disposed(by: self.disposeBag)
        
        self.btLink.rx.tap.bind { _ in
            ManageApp.shared.openLink(link: "https://www.vinmec.com/vi/tin-tuc/thong-tin-suc-khoe/nhi/huong-dan-bang-pha-ml-sua-cho-be-theo-tuoi/?link_type=related_posts")
        }.disposed(by: self.disposeBag)
    }
    
}
