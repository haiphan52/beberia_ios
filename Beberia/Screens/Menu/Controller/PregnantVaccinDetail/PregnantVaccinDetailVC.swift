
//
//  
//  PregnantVaccinDetailVC.swift
//  Beberia
//
//  Created by haiphan on 16/07/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift
import WebKit

class PregnantVaccinDetailVC: UIViewController {
    
    var pregnantModel: ListPregnantVacciNew?
    // Add here outlets
    @IBOutlet weak var webView: WKWebView!
    
    // Add here your view model
    private var viewModel: PregnantVaccinDetailVM = PregnantVaccinDetailVM()
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
}
extension PregnantVaccinDetailVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        if let model = self.pregnantModel, let content = model.content, let titleA = model.title {
            self.webView.loadHTMLString(content, baseURL: nil)
            title = titleA
//            self.btLink.setTitle(link, for: .normal)
        }
    }
    
    private func setupRX() {
        // Add here the setup for the RX
    }
}
