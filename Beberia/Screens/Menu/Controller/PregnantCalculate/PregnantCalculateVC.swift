
//
//  
//  PregnantCalculateVC.swift
//  Beberia
//
//  Created by haiphan on 25/05/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

protocol PregnantCalculateDelegate: AnyObject {
    func updateBaby(babyInfo: BabyInfo)
}

class PregnantCalculateVC: BaseVC {
    
    // Add here outlets
    @IBOutlet weak var tfExpectedBorn: UITextField!
    @IBOutlet weak var btUpdate: UIButton!
    @IBOutlet weak var lbDateBorn: UILabel!
    
    // Add here your view model
    var babyInfo: BabyInfo?
    weak var delegate: PregnantCalculateDelegate?
    private let dateFormat: DateFormatter = DateFormatter()
    private let datePicker: UIDatePicker = UIDatePicker()
    private var viewModel: PregnantCalculateVM = PregnantCalculateVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
        self.setupNavigationVer2()
        self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                 bgColor: R.color.fd799D()!,
                                 textColor: .white,
                                 isTranslucent: true)
        self.removeBorder(font: UIFont.notoSansFont(weight: .bold, size: 18),
                          bgColor: R.color.fd799D()!,
                          textColor: .white)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
}
extension PregnantCalculateVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        title = "Tính ngày dự sinh"
        self.setupDatePicker()
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        self.btUpdate.rx.tap.bind { [weak self] _ in
            guard let wSelf = self, let text = wSelf.tfExpectedBorn.text, !text.isEmpty, let date = text.convertToDate(formatDate: .ddMMyyyy)  else { return }
            wSelf.viewModel.getDateCalculate(date: date.timeIntervalSince1970)
        }.disposed(by: self.disposeBag)
        
        self.viewModel.preDate.bind { [weak self] model in
            guard let wSelf = self else { return }
            wSelf.lbDateBorn.text = model.preg
            if let baby = wSelf.babyInfo {
                wSelf.editBaby(babyInfo: baby)
            }
            wSelf.showAlert(title: nil, message: "Bạn đã cập nhật Ngày Dự Sinh thành công")
        }.disposed(by: self.disposeBag)
        
        self.viewModel.msgError.bind { [weak self] err in
            guard let wSelf = self else { return }
            wSelf.showAlert(title: nil, message: err)
        }.disposed(by: self.disposeBag)
        
    }
    
    private func getBirthDay() -> Date? {
        if let textReal = self.lbDateBorn.text, !textReal.isEmpty {
            return textReal.convertToDate(formatDate: .ddMMyyyy)
        }
        return nil
    }
    
    private func editBaby(babyInfo: BabyInfo) {
        guard let date = self.getBirthDay() else {
            self.showAlert(title: nil, message: "Vui lòng chọn ngày sinh")
            return
        }
        
        switch ManageApp.shared.toNumberWeek(dayExpectedBorn: date.timeIntervalSince1970) {
        case .pregnanting:
            self.pregnanting(nickName: babyInfo.nickname ?? "", date: date, babyInfo: babyInfo)
        case .invalid:
            self.moveToPopupError(strDay: "")
        case .greaterThan42Week(let text):
            self.moveToPopupError(strDay: text)
        }
    }
    
    private func moveToPopupError(strDay: String) {
        let vc = PopupCancelationOrder.createVC()
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.statusView = .pregnant
        self.present(vc, animated: true, completion: nil)
        vc.loadValuePregnant(strDay: strDay)
    }
    
    private func pregnanting(nickName: String, date: Date, babyInfo: BabyInfo) {
        guard let id = babyInfo.id else {
                  return
        }
        APIManager.editBabyProfile(id: id,
                                   gender: babyInfo.gender ?? 1,
                                   nickname: nickName,
                                   birthday: date.timeIntervalSince1970,
                                   image: nil,
                                   callbackSuccess: { [weak self] baby in
            guard let wSelf = self else { return }
            if let index = UserInfo.shareUserInfo.baby_info.firstIndex(where: { $0.id == baby.id }) {
                UserInfo.shareUserInfo.baby_info[index] = baby
            }
            wSelf.delegate?.updateBaby(babyInfo: baby)
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    //MARK: Setup date Picker
    private func setupDatePicker() {
        self.dateFormat.dateStyle = DateFormatter.Style.medium
        self.dateFormat.timeStyle = DateFormatter.Style.none
        self.dateFormat.dateFormat = Key.DateFormat.DateFormatddMMyyyy
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        self.datePicker.addTarget(self, action: #selector(updateDateField(sender:)), for: UIControl.Event.valueChanged)
        self.tfExpectedBorn.inputView = datePicker
        self.tfExpectedBorn.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        let image = R.image.ic_pregnant_calcelator()
        imageView.image = image
        self.tfExpectedBorn.rightView = imageView
    }
    
    @objc private func updateDateField(sender: UIDatePicker) {
        self.tfExpectedBorn.text = dateFormat.string(from: datePicker.date)
    }
}
