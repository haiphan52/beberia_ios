
//
//  ___HEADERFILE___
//
import Foundation
import RxCocoa
import RxSwift

class PregnantCalculateVM {
    
    let preDate: PublishSubject<PreDatePregnant> = PublishSubject.init()
    let msgError: PublishSubject<String> = PublishSubject.init()
    
    private let disposeBag = DisposeBag()
    init() {
        
    }
    
    func getDateCalculate(date: Double) {
        APIManager.getPreDate(date: date) { [weak self] model in
            guard let wSelf = self else { return }
            wSelf.preDate.onNext(model)
        } failed: { [weak self] text in
            guard let wSelf = self else { return }
            wSelf.msgError.onNext(text)
        }

    }
    
}
