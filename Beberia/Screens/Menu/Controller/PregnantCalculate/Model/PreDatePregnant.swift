//
//  PreDatePregnant.swift
//  Beberia
//
//  Created by haiphan on 06/06/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

struct PreDatePregnant: Codable {
    
    let preg: String?
    
    enum CodingKeys: String, CodingKey {
        case preg = "preg"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        preg = try values.decodeIfPresent(String.self, forKey: .preg)
    }
    
}
