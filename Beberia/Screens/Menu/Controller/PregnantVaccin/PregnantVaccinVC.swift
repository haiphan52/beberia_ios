
//
//  
//  PregnantVaccinVC.swift
//  Beberia
//
//  Created by haiphan on 22/05/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

class PregnantVaccinVC: BaseVC {
        
    // Add here outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btLink: UIButton!
    
    // Add here your view model
    private var viewModel: PregnantVaccinVM = PregnantVaccinVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_black()!)
        self.setupNavigationVer2()
        self.setupNavigationBariOS15(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                     bgColor: .white,
                                     textColor: R.color.f3C()!)
        self.removeBorder(font: UIFont.notoSansFont(weight: .bold, size: 18),
                          bgColor: .white,
                          textColor: R.color.f3C()!)
        self.navigationController?.isNavigationBarHidden = false
        title = "Thai giáo"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
}
extension PregnantVaccinVC {
    
    private func setupUI() {
        // Add here the setup for the UI
        self.tableView.register(PregnantVaccinCell.nib, forCellReuseIdentifier: PregnantVaccinCell.identifier)
        self.tableView.delegate = self
    }
    
    private func setupRX() {
        // Add here the setup for the RX
        self.btLink.rx.tap.bind { _ in
            UIPasteboard.general.string = ConstantApp.shared.linkPreferPregnantGeneral
        }.disposed(by: self.disposeBag)
    
        self.viewModel.sources.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: PregnantVaccinCell.identifier, cellType: PregnantVaccinCell.self)) {(row, element, cell) in
                cell.setValue(model: element)
            }.disposed(by: disposeBag)
        
        self.tableView.rx.itemSelected.bind { [weak self] idx in
            guard let wSelf = self else { return }
            let item = self?.viewModel.sources.value[idx.row]
            let vc = PregnantVaccinDetailVC.createVC()
            vc.pregnantModel = item
            wSelf.navigationController?.pushViewController(vc, animated: true)
        }.disposed(by: disposeBag)
        
    }
}
extension PregnantVaccinVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
