
//
//  ___HEADERFILE___
//
import Foundation
import RxCocoa
import RxSwift

class PregnantVaccinVM {
    
    let sources: BehaviorRelay<[ListPregnantVacciNew]> = BehaviorRelay.init(value: [])
    let msgError: PublishSubject<String> = PublishSubject.init()
    private let disposeBag = DisposeBag()
    init() {
        self.getListPregnant()
    }
    
    func getListPregnant() {
        APIManager.getListPregnant { [weak self] model in
            guard let wSelf = self,
                  let model = model,
                  let list = model.list else { return }
            wSelf.sources.accept(list)
        } failed: { [weak self] message in
            guard let wSelf = self else { return }
            wSelf.msgError.onNext(message)
        }
    }
    
//    func loadPregnants() {
//        ReadJSONFallLove.shared
//              .readJSONObs(offType: [PregnantVaccinModel].self, name: "Pregnant", type: "json")
//              .flatMap({ list -> Observable<[PregnantVaccinModel]> in
//                  return Observable.just(list)
//              })
//              .bind(to: self.sources)
//              .disposed(by: self.disposeBag)
//    }
}
