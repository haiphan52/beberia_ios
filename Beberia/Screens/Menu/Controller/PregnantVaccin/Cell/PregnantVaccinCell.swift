//
//  PregnantVaccinCell.swift
//  Beberia
//
//  Created by haiphan on 16/07/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class PregnantVaccinCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension PregnantVaccinCell {
    
//    func updateValue(model: PregnantVaccinModel) {
//        self.img.image = UIImage(named: model.image ?? ConstantApp.shared.imagePlaceHodler)
//        self.lbTitle.text = model.title
//    }
    
    func setValue(model: ListPregnantVacciNew) {
        self.lbTitle.text = model.title
        if let icon = model.icon, let url = URL(string: icon) {
            self.img.kf.setImage(with: url)
        }
    }
    
}
