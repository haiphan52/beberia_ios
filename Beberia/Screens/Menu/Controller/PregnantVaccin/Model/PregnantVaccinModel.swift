//
//  PregnantVaccinModel.swift
//  Beberia
//
//  Created by haiphan on 16/07/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

struct PregnantVaccinModel: Codable {
    let code: Int?
    let title, image, link: String?
    let content: [String]?
}

// MARK: - DataClass
struct PregnantVacciNewModel: Codable {
    let list: [ListPregnantVacciNew]?
}

// MARK: - List
struct ListPregnantVacciNew: Codable {
    let content, title: String?
    let order, id: Int?
    let icon: String?
}


public struct OptionalMessageDTO<T: Codable>: Codable {
    public typealias Model = Optional<T>
    public var data: Model?
    public var status: Int?
    public var message: String?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
        case message = "message"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try? values.decodeIfPresent(Int.self, forKey: .status) ?? 200
        data = try? values.decodeIfPresent(T.self, forKey: .data)
        message = try? values.decodeIfPresent(String.self, forKey: .message)
    }
}
