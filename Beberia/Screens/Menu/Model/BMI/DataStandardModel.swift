//
//  DataStandardModel.swift
//
//  Created by IMAC on 8/7/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class DataStandardModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let label = "label"
    static let gender = "gender"
    static let weight = "weight"
    static let height = "height"
    static let id = "id"
    static let unit = "unit"
  }

  // MARK: Properties
  public var label: String?
  public var gender: Int?
  public var weight: Double?
  public var height: Double?
  public var id: Int?
  public var unit: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    label = json[SerializationKeys.label].string
    gender = json[SerializationKeys.gender].int
    weight = json[SerializationKeys.weight].double
    height = json[SerializationKeys.height].double
    id = json[SerializationKeys.id].int
    unit = json[SerializationKeys.unit].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = label { dictionary[SerializationKeys.label] = value }
    if let value = gender { dictionary[SerializationKeys.gender] = value }
    if let value = weight { dictionary[SerializationKeys.weight] = value }
    if let value = height { dictionary[SerializationKeys.height] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = unit { dictionary[SerializationKeys.unit] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.label = aDecoder.decodeObject(forKey: SerializationKeys.label) as? String
    self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? Int
    self.weight = aDecoder.decodeObject(forKey: SerializationKeys.weight) as? Double
    self.height = aDecoder.decodeObject(forKey: SerializationKeys.height) as? Double
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.unit = aDecoder.decodeObject(forKey: SerializationKeys.unit) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(label, forKey: SerializationKeys.label)
    aCoder.encode(gender, forKey: SerializationKeys.gender)
    aCoder.encode(weight, forKey: SerializationKeys.weight)
    aCoder.encode(height, forKey: SerializationKeys.height)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(unit, forKey: SerializationKeys.unit)
  }

}
