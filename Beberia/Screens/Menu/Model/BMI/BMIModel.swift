//
//  BMIModel.swift
//
//  Created by IMAC on 7/16/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class BMIModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let height = "height"
    static let weight = "weight"
    static let date = "date"
    static let id = "id"
    static let circumference = "circumference"
    static let percent_height = "percent_height"
    static let percent_weight = "percent_weight"
  }

  // MARK: Properties
  public var height: Double?
  public var weight: Double?
  public var date: Int?
  public var id: Int?
  public var circumference: Int?
    public var percent_height: Double?
    public var percent_weight: Double?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    percent_height = json[SerializationKeys.percent_height].double
    percent_weight = json[SerializationKeys.percent_weight].double
    height = json[SerializationKeys.height].double
    weight = json[SerializationKeys.weight].double
    date = json[SerializationKeys.date].int
    id = json[SerializationKeys.id].int
    circumference = json[SerializationKeys.circumference].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = height { dictionary[SerializationKeys.height] = value }
    if let value = weight { dictionary[SerializationKeys.weight] = value }
    if let value = date { dictionary[SerializationKeys.date] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = circumference { dictionary[SerializationKeys.circumference] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.height = aDecoder.decodeObject(forKey: SerializationKeys.height) as? Double
    self.weight = aDecoder.decodeObject(forKey: SerializationKeys.weight) as? Double
    self.date = aDecoder.decodeObject(forKey: SerializationKeys.date) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.circumference = aDecoder.decodeObject(forKey: SerializationKeys.circumference) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(height, forKey: SerializationKeys.height)
    aCoder.encode(weight, forKey: SerializationKeys.weight)
    aCoder.encode(date, forKey: SerializationKeys.date)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(circumference, forKey: SerializationKeys.circumference)
  }

}
