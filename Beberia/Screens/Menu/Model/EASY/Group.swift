//
//  Group.swift
//
//  Created by IMAC on 6/30/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Group: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let timelines = "timelines"
    static let active = "active"
    static let type = "type"
  }

  // MARK: Properties
  public var id: Int?
public var type: Int?
  public var timelines: [Timelines]?
  public var active: Int?
    public var stt: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    id = json[SerializationKeys.id].int
    type = json[SerializationKeys.type].int
    if let items = json[SerializationKeys.timelines].array { timelines = items.map { Timelines(json: $0) } }
    active = json[SerializationKeys.active].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = timelines { dictionary[SerializationKeys.timelines] = value.map { $0.dictionaryRepresentation() } }
    if let value = active { dictionary[SerializationKeys.active] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.timelines = aDecoder.decodeObject(forKey: SerializationKeys.timelines) as? [Timelines]
    self.active = aDecoder.decodeObject(forKey: SerializationKeys.active) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(timelines, forKey: SerializationKeys.timelines)
    aCoder.encode(active, forKey: SerializationKeys.active)
  }

}
