//
//  Timelines.swift
//
//  Created by IMAC on 6/30/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Timelines: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let content = "content"
    static let startTime = "start_time"
    static let active = "active"
    static let id = "id"
    static let code = "code"
    static let endTime = "end_time"
  }

  // MARK: Properties
  public var content: String?
  public var startTime: Double?
  public var active: Int?
  public var id: Int?
  public var code: Int?
  public var endTime: Double?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    content = json[SerializationKeys.content].string
    startTime = json[SerializationKeys.startTime].double
    active = json[SerializationKeys.active].int
    id = json[SerializationKeys.id].int
    code = json[SerializationKeys.code].int
    endTime = json[SerializationKeys.endTime].double
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = content { dictionary[SerializationKeys.content] = value }
    if let value = startTime { dictionary[SerializationKeys.startTime] = value }
    if let value = active { dictionary[SerializationKeys.active] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = endTime { dictionary[SerializationKeys.endTime] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.content = aDecoder.decodeObject(forKey: SerializationKeys.content) as? String
    self.startTime = aDecoder.decodeObject(forKey: SerializationKeys.startTime) as? Double
    self.active = aDecoder.decodeObject(forKey: SerializationKeys.active) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.code = aDecoder.decodeObject(forKey: SerializationKeys.code) as? Int
    self.endTime = aDecoder.decodeObject(forKey: SerializationKeys.endTime) as? Double
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(content, forKey: SerializationKeys.content)
    aCoder.encode(startTime, forKey: SerializationKeys.startTime)
    aCoder.encode(active, forKey: SerializationKeys.active)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(code, forKey: SerializationKeys.code)
    aCoder.encode(endTime, forKey: SerializationKeys.endTime)
  }

}
