//
//  ESAYModel.swift
//
//  Created by IMAC on 6/30/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ESAYModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let note = "note"
    static let name = "name"
    static let toWeek = "to_week"
    static let id = "id"
    static let fromWeek = "from_week"
    static let isPublic = "is_public"
    static let group = "group"
    static let wakeUp = "wake_up"
    
  }

  // MARK: Properties
  public var note: String?
  public var name: String?
  public var toWeek: Int?
  public var id: Int?
  public var fromWeek: Int?
  public var isPublic: Int?
  public var group: [Group]?
    public var wakeUp: Double?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    note = json[SerializationKeys.note].string
    name = json[SerializationKeys.name].string
    toWeek = json[SerializationKeys.toWeek].int
    id = json[SerializationKeys.id].int
    fromWeek = json[SerializationKeys.fromWeek].int
    isPublic = json[SerializationKeys.isPublic].int
    wakeUp = json[SerializationKeys.wakeUp].double
    if let items = json[SerializationKeys.group].array { group = items.map { Group(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = note { dictionary[SerializationKeys.note] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = toWeek { dictionary[SerializationKeys.toWeek] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = fromWeek { dictionary[SerializationKeys.fromWeek] = value }
    if let value = isPublic { dictionary[SerializationKeys.isPublic] = value }
    if let value = group { dictionary[SerializationKeys.group] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.note = aDecoder.decodeObject(forKey: SerializationKeys.note) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.toWeek = aDecoder.decodeObject(forKey: SerializationKeys.toWeek) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.fromWeek = aDecoder.decodeObject(forKey: SerializationKeys.fromWeek) as? Int
    self.isPublic = aDecoder.decodeObject(forKey: SerializationKeys.isPublic) as? Int
    self.group = aDecoder.decodeObject(forKey: SerializationKeys.group) as? [Group]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(note, forKey: SerializationKeys.note)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(toWeek, forKey: SerializationKeys.toWeek)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(fromWeek, forKey: SerializationKeys.fromWeek)
    aCoder.encode(isPublic, forKey: SerializationKeys.isPublic)
    aCoder.encode(group, forKey: SerializationKeys.group)
  }

}
