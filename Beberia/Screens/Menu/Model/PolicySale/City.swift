//
//  City.swift
//
//  Created by IMAC on 8/5/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class City: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let locationNameVi = "location_name_vi"
    static let locationCode = "location_code"
  }

  // MARK: Properties
  public var locationNameVi: String?
  public var locationCode: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    locationNameVi = json[SerializationKeys.locationNameVi].string
    locationCode = json[SerializationKeys.locationCode].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = locationNameVi { dictionary[SerializationKeys.locationNameVi] = value }
    if let value = locationCode { dictionary[SerializationKeys.locationCode] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.locationNameVi = aDecoder.decodeObject(forKey: SerializationKeys.locationNameVi) as? String
    self.locationCode = aDecoder.decodeObject(forKey: SerializationKeys.locationCode) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(locationNameVi, forKey: SerializationKeys.locationNameVi)
    aCoder.encode(locationCode, forKey: SerializationKeys.locationCode)
  }

}
