//
//  District.swift
//
//  Created by IMAC on 8/5/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class District: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let districtName = "district_name"
    static let districtCode = "district_code"
  }

  // MARK: Properties
  public var districtName: String?
  public var districtCode: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    districtName = json[SerializationKeys.districtName].string
    districtCode = json[SerializationKeys.districtCode].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = districtName { dictionary[SerializationKeys.districtName] = value }
    if let value = districtCode { dictionary[SerializationKeys.districtCode] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.districtName = aDecoder.decodeObject(forKey: SerializationKeys.districtName) as? String
    self.districtCode = aDecoder.decodeObject(forKey: SerializationKeys.districtCode) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(districtName, forKey: SerializationKeys.districtName)
    aCoder.encode(districtCode, forKey: SerializationKeys.districtCode)
  }

}
