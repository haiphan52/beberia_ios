//
//  PolicySaleModel.swift
//
//  Created by IMAC on 8/5/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class PolicySaleModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let note = "note"
    static let store = "store"
    static let typeSale = "type_sale"
    static let id = "id"
    static let categories = "categories"
    static let saleValue = "sale_value"
  }

  // MARK: Properties
  public var note: String?
  public var store: Store?
  public var typeSale: Int?
  public var id: Int?
  public var categories: [Categories]?
  public var saleValue: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    note = json[SerializationKeys.note].string
    store = Store(json: json[SerializationKeys.store])
    typeSale = json[SerializationKeys.typeSale].int
    id = json[SerializationKeys.id].int
    if let items = json[SerializationKeys.categories].array { categories = items.map { Categories(json: $0) } }
    saleValue = json[SerializationKeys.saleValue].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = note { dictionary[SerializationKeys.note] = value }
    if let value = store { dictionary[SerializationKeys.store] = value.dictionaryRepresentation() }
    if let value = typeSale { dictionary[SerializationKeys.typeSale] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = categories { dictionary[SerializationKeys.categories] = value.map { $0.dictionaryRepresentation() } }
    if let value = saleValue { dictionary[SerializationKeys.saleValue] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.note = aDecoder.decodeObject(forKey: SerializationKeys.note) as? String
    self.store = aDecoder.decodeObject(forKey: SerializationKeys.store) as? Store
    self.typeSale = aDecoder.decodeObject(forKey: SerializationKeys.typeSale) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.categories = aDecoder.decodeObject(forKey: SerializationKeys.categories) as? [Categories]
    self.saleValue = aDecoder.decodeObject(forKey: SerializationKeys.saleValue) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(note, forKey: SerializationKeys.note)
    aCoder.encode(store, forKey: SerializationKeys.store)
    aCoder.encode(typeSale, forKey: SerializationKeys.typeSale)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(categories, forKey: SerializationKeys.categories)
    aCoder.encode(saleValue, forKey: SerializationKeys.saleValue)
  }

}
