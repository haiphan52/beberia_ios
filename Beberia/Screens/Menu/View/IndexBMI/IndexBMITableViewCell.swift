//
//  IndexBMITableViewCell.swift
//  Beberia
//
//  Created by IMAC on 6/18/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class IndexBMITableViewCell: UITableViewCell {

    @IBOutlet weak var ngayLable: UILabel!
    @IBOutlet weak var chuViDAuLable: UILabel!
    @IBOutlet weak var canNangLable: UILabel!
    @IBOutlet weak var chieuCaoLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
