//
//  DeletionAccountView.swift
//  Beberia
//
//  Created by haiphan on 30/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class DeletionAccountView: UIView, SetUpFunctionBase {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var btConfirm: UIButton!
    @IBOutlet weak var btClose: UIButton!
    
    var deleteAccountSuccess: (() -> Void)?
    
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupRX()
    }
    
}
extension DeletionAccountView {
    
    func setupUI() {
        makeTitle()
    }
    
    func setupRX() {
        tfName.rx.text.orEmpty.withUnretained(self).bind { owner, text in
            owner.btConfirm.isEnabled = ( String(text).uppercased() == "DELETEACCOUNT".uppercased()) ? true : false
            owner.btConfirm.backgroundColor = ( String(text).uppercased() == "DELETEACCOUNT".uppercased()) ? R.color.fd799D() : R.color.fd799D()?.withAlphaComponent(0.5)
        }.disposed(by: disposeBag)
        
        btConfirm.rx.tap
            .withUnretained(self)
            .bind { owner, _ in
                guard let topVC = ManageApp.shared.topViewController() else {
                    return
                }
                topVC.showAlert(type: .alert, title: nil, message: "Bạn có chắc muốn xoá tài khoản?", buttonTitles: ["Huỷ", "Chấp nhận"], highlightedButtonIndex: nil) { index in
                    if index == 1 {
                        owner.deleteAccount()
                    }
                }
        }.disposed(by: disposeBag)
        
        btClose.rx.tap
            .withUnretained(self)
            .bind { owner, _ in
                owner.removeFromSuperview()
            }.disposed(by: disposeBag)
    }
    
    private func makeTitle() {
        let text = "Vui lòng nhâp DELETEACCOUNT để xác nhận việc xoá tài khoản"
        let att = NSMutableAttributedString(string: text)
        att.addAttribute(NSAttributedString.Key.font,
                         value: UIFont.notoSansFont(weight: .bold, size: 18),
                         range: (att.string as NSString).range(of: "DELETEACCOUNT"))
        lbTitle.attributedText = att
    }
    
    private func deleteAccount() {
        AlamorfireService.shared.deleteAcount().withUnretained(self).bind { owner, _ in
            owner.deleteAccountSuccess?()
        }.disposed(by: disposeBag)
    }
    
}
