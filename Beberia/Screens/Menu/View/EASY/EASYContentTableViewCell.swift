//
//  EASYContentTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 6/18/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class EASYContentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var spaceBottom: NSLayoutConstraint!
    @IBOutlet weak var btnOption: UIButton!
    @IBOutlet weak var titleLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupData(data: Timelines){
        let code = CodeEASY(rawValue: data.code ?? 0)
        
        var title = "\(code!.title): "
        if code == .o {
            title = ""
        }
       
        
        var content = "\(title)\((data.startTime ?? 0.0).toString())-\((data.endTime ?? 0.0).toString()): \(data.content ?? "")"
        
        if code == .o {
            content = "\((data.startTime ?? 0.0).toString())-\((data.endTime ?? 0.0).toString()): \(data.content ?? "")"
        }
        
        if data.endTime == 0{
            content = "\(title)\((data.startTime ?? 0.0).toString()): \(data.content ?? "")"
        }
        
//        if data.endTime == 0 || code == .o{
//            content = "\((data.startTime ?? 0.0).toString()): \(data.content ?? "")"
//        }
        
        
        
        var range = NSRange(location: 0, length: 1)
        if code == .code12 || code == .code34 {
            range = NSRange(location: 0, length: 4)
        }
        
        let anotherAttribute = [ NSAttributedString.Key.font:  UIFont.init(name: AppFont.HelveticaNeueBold, size: 14)  ]
        
        let gString = NSMutableAttributedString.init(string: content)
        gString.addAttributes(anotherAttribute as [NSAttributedString.Key : Any], range: range)
        
        titleLable.attributedText = gString
        
        titleLable.textColor = UIColor.init(hexString: code!.colorEASY)
    }
    
}
