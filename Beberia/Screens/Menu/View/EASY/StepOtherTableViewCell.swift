//
//  StepOtherTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 7/3/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class StepOtherTableViewCell: UITableViewCell {
    @IBOutlet weak var contentLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupData(data: Group){
        var content = ""
        for time in data.timelines ?? [Timelines](){
            let string = "\((time.startTime ?? 0.0).toString()): \(time.content ?? "") \n"
            content += string
        }
        
        contentLable.text = content
    }
    
}
