//
//  StepEASYTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 6/16/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SwiftyAttributes

class StepEASYTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contentLable: PaddingLabel!
    @IBOutlet weak var viewDot: UIView!
    @IBOutlet weak var heightViewDot: NSLayoutConstraint!
    @IBOutlet weak var btnAddTime: UIButton!
    @IBOutlet weak var sttViewLable: UILabel!
    //  @IBOutlet weak var tbvContent: ContentSizedTableView!
    @IBOutlet weak var spaceViewYellow: NSLayoutConstraint!
    @IBOutlet weak var heightViewYellow: NSLayoutConstraint!
    @IBOutlet weak var viewYellow: UIView!
    //  @IBOutlet weak var heightTbv: NSLayoutConstraint!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var spaceButtonAddTime: NSLayoutConstraint!
    @IBOutlet weak var heightButtonAdTime: NSLayoutConstraint!
    
    var timeLines = BehaviorRelay.init(value: [Timelines]())
    var bag = DisposeBag()
    var updateCell : ()->() = {}
    var isBool = true
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        // bag = DisposeBag()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //  spaceButtonAddTime.constant = 10
        //  heightButtonAdTime.constant = 0
        //  btnAddTime.isHidden = true
        initRx()
    }
    
    func setupData(timeLines: [Timelines]){
        //        print(timeLines.count)
        //        self.timeLines.accept(timeLines)
        
        
        var attributedStringFull = NSMutableAttributedString.init()
        for data in timeLines{
            
            let code = CodeEASY(rawValue: data.code ?? 0)
            //var content = ""
            var title = "\(code!.title): "
            if code == .o {
                title = ""
            }
            
            
            var content = "\(title)\((data.startTime ?? 0.0).toString())-\((data.endTime ?? 0.0).toString()): \(data.content ?? "")"
            
            if code == .o {
                content = "\((data.startTime ?? 0.0).toString())-\((data.endTime ?? 0.0).toString()): \(data.content ?? "")"
            }
            
            if data.endTime == 0{
                content = "\(title)\((data.startTime ?? 0.0).toString()): \(data.content ?? "")"
            }
            
            let attributedString = NSAttributedString(string:content,
                                                      attributes:[NSAttributedString.Key.foregroundColor: UIColor.init(hexString: code!.colorEASY)!,
                                                                  NSAttributedString.Key.font: UIFont.init(name: AppFont.HelveticaNeue, size: 14) as Any])
            
            attributedStringFull += attributedString + NSMutableAttributedString.init(string: "\n") + NSMutableAttributedString.init(string: "\n")
            
        }
        
        contentLable.attributedText = attributedStringFull
    }
    
    func initRx(){
        //        // init TableView
        //        tbvContent.register(R.nib.easyContentTableViewCell)
        //        tbvContent.tableFooterView = UIView()
        //        tbvContent.separatorStyle = .none
        //        tbvContent.isScrollEnabled = false
        //        tbvContent.allowsSelection = false
        //        tbvContent.rowHeight = UITableView.automaticDimension
        //      //  self.layoutIfNeeded()
        //
        //        timeLines.asObservable().observeOn(MainScheduler.asyncInstance)
        //            .bind(to: tbvContent.rx.items(cellIdentifier: R.reuseIdentifier.easyContentTableViewCell.identifier, cellType: EASYContentTableViewCell.self)) { row, data, cell in
        //                cell.selectionStyle = .none
        //                cell.spaceBottom.constant = (row + 1 == self.timeLines.value.count) ? 7 : 0
        //
        //                cell.setupData(data: data)
        //
        //
        //        }.disposed(by: bag)
        //
        //        // set contentSize tableview
        //        tbvContent.rx.observe(CGSize.self, "contentSize").asObservable().observeOn(MainScheduler.asyncInstance).subscribe { [weak self] (size) in
        //            guard let self = self else { return }
        //          //  print(size.element!!.height)
        //            self.heightTbv.constant = size.element!!.height
        //
        //            UIView.animate(withDuration: 0.5) {
        //                self.setNeedsLayout()
        //                self.layoutIfNeeded()
        //                self.updateCell()
        //            }
        //
        //        }.disposed(by: bag)
    }
    
    //    override func layoutSubviews() {
    //        super.layoutSubviews()
    //
    //    }
    
}

extension Double {
    func roundToNearestValue() -> Double {
        return (self * 1000).rounded() / 1000
    }
    
    func toString() -> String {
        return String(format: "%.2f",self)
    }
}
