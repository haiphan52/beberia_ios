//
//  CancelationOrder.swift
//  Beberia
//
//  Created by haiphan on 08/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD
import IQKeyboardManagerSwift

protocol CancelationOrderDelegate {
    func cancelSuccess(reason: String)
}

class CancelationOrder: BaseNavigationColor {
    
    enum Action: Int, CaseIterable {
        case copy, agree
    }
    
    enum LabelText: Int, CaseIterable {
        case orderId, requestDay
    }
    
    var order: ManageOrderModel?
    var delegate: CancelationOrderDelegate?

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var heightTextView: NSLayoutConstraint!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet var lbs: [UILabel]!
    @IBOutlet weak var bottomStackView: NSLayoutConstraint!
    @IBOutlet weak var heightViewBottom: NSLayoutConstraint!
    private var keyboardHelper: KeyboardHelper?
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
}
extension CancelationOrder {
    
    private func setupUI() {
        title = "Huỷ đơn hàng"
        
        if let order = self.order {
            self.lbs[LabelText.orderId.rawValue].text = "#\(order.id ?? 20)"
            let date = Date(timeIntervalSince1970: order.created ?? 20)
            self.lbs[LabelText.requestDay.rawValue].text = "\(date.toString(format: String.FormatDate.ddMMyyyy))"
        }
        
        self.textView.text = "Vui lòng cho biết lý do hủy đơn hàng"
        self.textView.textColor = UIColor.lightGray
        self.heightViewBottom.constant = ConstantApp.shared.getHeightSafeArea(type: .bottom)
    }
    
    private func setupRX() {
        
        Action.allCases.forEach { [weak self] type in
            guard let wSelf = self else { return }
            let bt = wSelf.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                switch type {
                case .copy:
                    UIPasteboard.general.string = wSelf.lbs[LabelText.orderId.rawValue].text
                case .agree: wSelf.apiRefundOrder()
                }
            }.disposed(by: wSelf.disposeBag)
        }
        
        self.textView.rx.didBeginEditing.asObservable().bind { [weak self] _ in
            guard let wSelf = self else { return }
            if wSelf.textView.textColor == UIColor.lightGray {
                wSelf.textView.text = nil
                wSelf.textView.textColor = R.color.d4D4D()
            }
        }.disposed(by: self.disposeBag)
        
        self.textView.rx.didEndEditing.asObservable().bind { [weak self] _ in
            guard let wSelf = self else { return }
            if wSelf.textView.text.isEmpty {
                wSelf.textView.text = "Vui lòng cho biết lý do hủy đơn hàng"
                wSelf.textView.textColor = UIColor.lightGray
            }
        }.disposed(by: self.disposeBag)
        
        self.textView.rx.didChange.asObservable().bind { [weak self] _ in
            guard let wSelf = self else { return }
            let fixedWidth = wSelf.textView.frame.size.width
            wSelf.textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = wSelf.textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = wSelf.textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            wSelf.heightTextView.constant = newFrame.height
        }.disposed(by: self.disposeBag)
        
        keyboardHelper = KeyboardHelper { [unowned self] animation, keyboardFrame, duration in
            switch animation {
            case .keyboardWillShow:
                self.bottomStackView.constant = keyboardFrame.height
                self.heightViewBottom.constant = 0
            case .keyboardWillHide:
                self.bottomStackView.constant = 0
                self.heightViewBottom.constant = ConstantApp.shared.getHeightSafeArea(type: .bottom)
            }
        }
    }
    
    private func getText() -> String {
        if self.textView.textColor == UIColor.lightGray {
            return ""
        } else {
            return self.textView.text
        }
    }
    
    private func apiRefundOrder() {
        guard let id = self.order?.id, self.getText().count > 0 else {
            self.showAlert(title: nil, message: "Vui lòng nhập lí do huỷ đơn")
            return
        }
        SVProgressHUD.show()
        APIManager.refundOrder(orderId: id, cancelReason: self.getText()) { [weak self] _ in
            guard let wSelf = self else { return }
            SVProgressHUD.dismiss()
            let vc = PopupCancelationOrder.createVC()
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overFullScreen
            vc.deleegate = self
            vc.statusView = .cancelOrder
            wSelf.present(vc, animated: true, completion: nil)
        } failed: { error in
            SVProgressHUD.dismiss()
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
            SVProgressHUD.dismiss()
        }
    }
    
}
extension CancelationOrder: PopupCancelationOrderDelegate {
    func actionPopup(action: PopupCancelationOrder.Action) {
        switch action {
        case .cancelView:
            self.navigationController?.popViewController(animated: true, {
                self.delegate?.cancelSuccess(reason: self.getText())
            })
        default: break
        }
    }
}
