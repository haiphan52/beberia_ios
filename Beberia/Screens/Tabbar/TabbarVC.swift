//
//  TabbarVC.swift
//  Beberia
//
//  Created by haiphan on 27/12/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import SwiftKeychainWrapper

class TabbarVC: UITabBarController {
    
    enum InstallAppStatus {
        case first, update, normal
        
        static func getStatus(versionOfLastRun: String?, currentVersion: String?) -> Self {
            if versionOfLastRun == nil {
                return .first
            }
            
            if versionOfLastRun != currentVersion {
                return .update
            }
            
            return .normal
        }
        
    }
    
    struct Constant {
        static let widthIamge: CGFloat = 113
        static let dispaceImageToTrailing: CGFloat = 22
        static let toolTipView: Int = 4
    }

//    private let imgNewFeatures: UIImageView = UIImageView(image: R.image.img_new_face())
    private let imgNewFeatures: UIImageView = UIImageView(image: R.image.img_new_features())
    private let eventViewAppear: PublishSubject<Void> = PublishSubject.init()
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.eventViewAppear.onNext(())
    }
}
extension TabbarVC {
    
    private func setupUI() {
        self.delegate = self
        if #available(iOS 15.0, *) {
            let appearance: UITabBarAppearance = UITabBarAppearance()
            appearance.configureWithOpaqueBackground()
            tabBar.standardAppearance = appearance
            tabBar.scrollEdgeAppearance = appearance
        }
    }
    
    private func setupRX() {
        self.eventViewAppear.take(1).asObservable().bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.setupIamge()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                wSelf.showTooltip()
            }
        }.disposed(by: self.disposeBag)
        
        
        ManageApp.shared.hideImageEvent.asObservable().bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.imgNewFeatures.isHidden = true
        }.disposed(by: self.disposeBag)
        
        ManageApp.shared.typeBanner.asObservable().bind(onNext: { [weak self] bannerType in
            guard let self = self, let bannerType = bannerType else { return }
            switch bannerType {
            case .none: break
            case .rankPoint:
                let rankPointVC = RankPointVC.init(nib: R.nib.rankPointVC)
                rankPointVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(rankPointVC, animated: true)
            case .giftGame:
                let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
                gameVC.modalPresentationStyle = .overFullScreen
                self.present(gameVC, animated: true, completion: nil)
            case .info:
                guard let lichKTVC = R.storyboard.main.infoViewController() else{ return }
                lichKTVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(lichKTVC, animated: true)
            case .momTok:
                guard let momtokVC = R.storyboard.main.secretViewController() else {
                    return
                }
                momtokVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(momtokVC, animated: true)
            case .news:
                let listNewHotVC = ListNewHotViewController.init(nib: R.nib.listNewHotViewController)
                listNewHotVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(listNewHotVC, animated: true)
            case .listDiary:
                let postVC = ListDiaryViewController.init(nib: R.nib.listDiaryViewController)
                postVC.hidesBottomBarWhenPushed = true
                postVC.gettype = "3"
                postVC.title = "Mẹo cho bạn"
                self.navigationController?.pushViewController(postVC, animated: true)
            case .home:
                guard let tabbar  = self.navigationController?.tabBarController else { return }
                tabbar.selectedIndex = 1
            case .diaary:
                guard let tabbar  = self.navigationController?.tabBarController else { return }
                tabbar.selectedIndex = 2
            }
        }).disposed(by: self.disposeBag)
    }
    
    func passValueBabyToRepay(order: ManageOrderModel) {
        let viewC = self.viewControllers?[3]
        if let nav = viewC as? UINavigationController {
            if let v = nav.visibleViewController as? BabyFaceViewController {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    v.pageMenu?.moveToPage(1)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        v.passValueOrderToRepay(order: order)
                    }
                }
            }
        }
    }
    
    func passValueBaby(order: ManageOrderModel) {
        let viewC = self.viewControllers?[3]
        if let nav = viewC as? UINavigationController {
            if let v = nav.visibleViewController as? BabyFaceViewController {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    v.pageMenu?.moveToPage(1)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        v.passValueOrder(order: order)
                    }
                    
                }
            }
        }
    }
    
    private func setupIamge() {
        guard let view = self.tabBar.items?[Constant.toolTipView].value(forKey: "view") as? UIView else  { return }
        
        let frame = view.frame
        self.view.bringSubviewToFront(imgNewFeatures)
        self.view.addSubview(imgNewFeatures)
        
        let lastPoint = frame.origin.x + (frame.size.width / 2)
        // layout for new features
        let originX = lastPoint - Constant.widthIamge + Constant.dispaceImageToTrailing
        
        //layout for new face
//        let originX = lastPoint - (Constant.widthIamge / 2)
        self.imgNewFeatures.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(originX)
            make.bottom.equalToSuperview().inset(self.tabBar.frame.height)
            make.height.equalTo(36)
            make.width.equalTo(Constant.widthIamge)
        }
        self.imgNewFeatures.isHidden = true
    }
    
    private func showTooltip() {
        let currentVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        let versionOfLastRun = UserDefaults.standard.object(forKey: "VersionOfLastRun") as? String
        
        switch InstallAppStatus.getStatus(versionOfLastRun: versionOfLastRun, currentVersion: currentVersion) {
        case .first, .update:
            self.imgNewFeatures.isHidden = false
            if let tabItems = self.tabBar.items {
                let tabItem = tabItems[Constant.toolTipView]
                tabItem.badgeValue = ""
            }
        case .normal: break
            
        }
        
    }
    
}
extension TabbarVC: UITabBarControllerDelegate {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let listTabbar = self.tabBar.items else {
            return
        }
        let isLoggedIn = KeychainWrapper.standard.bool(forKey: KeychainKeys.isLoggedIn.rawValue) ?? false
        self.imgNewFeatures.isHidden = true
        if item == listTabbar[Constant.toolTipView] && isLoggedIn {
            if let tabItems = self.tabBar.items {
                let tabItem = tabItems[Constant.toolTipView]
                tabItem.badgeValue = nil
            }
            let currentVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
            UserDefaults.standard.set(currentVersion, forKey: "VersionOfLastRun")
            UserDefaults.standard.synchronize()
        }
        
    }
}
