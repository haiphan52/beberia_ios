//
//  NotificationCommentViewController.swift
//  Beberia
//
//  Created by IMAC on 9/26/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import FFPopup

class NotificationCommentViewController: BaseViewController {
    
    //MARK: Outlet
    @IBOutlet weak var lblNameUserReply: UILabel!
    @IBOutlet weak var btnViewMoreReply: UIButton!
    @IBOutlet weak var leadingTbvCommnet: NSLayoutConstraint!
    @IBOutlet weak var heightViewReply: NSLayoutConstraint!
    @IBOutlet weak var viewReply: UIView!
    @IBOutlet weak var heightViewInput: NSLayoutConstraint!
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet weak var heightTbvCommentRoot: NSLayoutConstraint!
    @IBOutlet weak var tbvComment: UITableView!
    @IBOutlet weak var tbvCommentRoot: UITableView!
    @IBOutlet weak var heightViewMoreReply: NSLayoutConstraint!
    @IBOutlet weak var viewMoreReply: UIView!
    
    //MARK: Properties
    var isCheckFromNotification = false
    var feed = HomeFeed.init(json: "")
    var fromToView = FromToview.Home
    var idNotification = 0
    var updateDataWhenDeleteComment: (_ idNotification: Int)->() = {_ in}
    var userFrom: User?
    var commentNotification: Comment?
    var isCheckEdit = false
    var viewInputFromNib: ViewInput = ViewInput.instanceFromNib()
    var commentRoot: Comment?
    var listComments = [Comment]()
    var heightDictionary: [IndexPath : CGFloat] = [:]
    var typeNotification = NotificationList.ComFeed
    var isEditComment = false
    var idPost = 0
    var idCommentParent = 0
    var type: Int?
    var userIdTag:[Int]? = nil
    var nextPageReply: Int = 1
    var typeCommentRoot: Int = Key.TypeLikeComment
//    var isShowViewMoreReply = false
//    {
//        didSet{
//            if isShowViewMoreReply{
//                heightViewMoreReply.constant = 28
//                btnViewMoreReply.isHidden = false
//            }else{
//                heightViewMoreReply.constant = 0
//                btnViewMoreReply.isHidden = true
//            }
//        }
//    }
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: Setup UI
    func setupUI(){
        self.title = R.string.localizable.notificationTitle()
        customLeftBarButton()
        initViewInput()
        initTableView(tbvComment: tbvComment)
        initTableView(tbvComment: tbvCommentRoot)
        handleDataInput()
//        isShowViewMoreReply = false
        
        self.tbvCommentRoot.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    func initTableView(tbvComment : UITableView){
        tbvComment.register(R.nib.commentTableViewCell)
        tbvComment.delegate = self
        tbvComment.dataSource = self
        tbvComment.separatorStyle = .none
        tbvComment.allowsSelection = false
        tbvComment.rowHeight = UITableView.automaticDimension
    }
    
    func setupUIEditComment(cell: CommentTableViewCell){
        cell.viewNumberLike.isHidden = isEditComment ? true : false
        cell.viewNumberReply.isHidden = isEditComment ? true : false
        cell.lblContent.isHidden = isEditComment ? true : false
        cell.btnSavedEdit.isHidden = isEditComment ? false : true
        cell.btnCancelEdit.isHidden = isEditComment ? false : true
        cell.txtContent.isHidden = isEditComment ? false : true
        cell.heightTextViewContent.isActive = isEditComment ? true : false
    }
    
    func handleDataInput(){
        
        print("LikeRepCom")
        typeCommentRoot = Key.TypeLikeReplyComment
        type = Key.TypeReplyComment
        leadingTbvCommnet.constant = -50
        userIdTag = [userFrom?.id ?? 0]
        if commentRoot?.secretNumber == 0 || commentRoot?.secretNumber == nil{
            lblNameUserReply.text = commentNotification?.user?.displayName ?? ""
        }else{
            lblNameUserReply.text = "@Ẩn danh\(commentNotification?.secretNumber ?? 0)"
        }
        
        
        switch typeNotification {
            
        case .LikeFeed:
            print("LikeFeed")
//        case .LikeComment, .ComFeed:
//            print("LikeComment")
//            leadingTbvCommnet.constant = -50
//            self.view.layoutIfNeeded()
//            type = Key.TypeComment
//            typeCommentRoot = Key.TypeLikeComment
//            self.getCommentReplyHomeFeed(page: nextPageReply)
        case .LikeRepComInfo, .LikeRepComDiary, .RepcomInfo, .RepComDiary:
            self.getCommentReplyInfoDiary(page: nextPageReply)
            print("!23")
        case .LikeRepCom, .RepComt, .RepcomSecret, .LikeRepComSecret:
//            print("LikeRepCom")
//            typeCommentRoot = Key.TypeLikeReplyComment
//            type = Key.TypeReplyComment
//            leadingTbvCommnet.constant = -50
            self.getCommentReplyHomeFeed(page: nextPageReply)
//            userIdTag = [userFrom?.id ?? 0]
//            lblNameUserReply.text = "@\(userFrom?.displayName ?? "")"
//        case .ComFeed:
//            print("ComFeed")
//            leadingTbvCommnet.constant = -50
//            type = Key.TypeComment
//            self.getCommentReplyHomeFeed(page: nextPageReply, type: type!)
//        case .RepComt:
//            type = Key.TypeReplyComment
//            listComments.append(commentRoot!)
//            self.tbvComment.reloadData()
        default :
            print("Ádasd")
        }
    }
    
    func initViewInput(){
        viewInputFromNib.frame = viewInput.bounds
        viewInputFromNib.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewInput.addSubview(viewInputFromNib)
        
        viewInputFromNib.updatHeightViewInput = { height in
            self.heightViewInput.constant = height
            UIView.animate(withDuration: 0.5) {
                self.updateViewConstraints()
                self.view.layoutIfNeeded()
            }
        }
        
        viewInputFromNib.sendComment = { textComment in
            print(textComment)
            self.viewInputFromNib.initViewInput()
            self.heightViewInput.constant = self.viewInputFromNib.heightConstraintViewInput.constant
            self.view.endEditing(true)
            
            switch self.typeNotification {
            case .LikeRepCom, .RepComt:
                self.sendCommnetHomeFeed(content: textComment)
            case .LikeRepComInfo, .LikeRepComDiary, .RepcomInfo, .RepComDiary:
                self.sendCommentInfoDiary(content: textComment)
            default:
                print("1234")
            }
            
        }
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tbvCommentRoot.layer.removeAllAnimations()
        self.heightTbvCommentRoot.constant = tbvCommentRoot.contentSize.height

        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: Request API
    func getCommentReplyInfoDiary(page: Int){
        SVProgressHUD.show()
        APIManager.getCommentReply(commentRoot?.commentId ?? 0 , page: page, callbackSuccess: { [weak self] (comments, nextPage) in
            guard let self = self else { return }
            self.listComments.append(contentsOf: comments)
            self.tbvComment.reloadData()
            
            if self.nextPageReply == 1{
                let index = self.listComments.firstIndex(where: { (comment) -> Bool in
                    return comment.commentId == self.commentNotification?.commentId
                })
                
                if index != nil{
                    let indexCommentNotification = IndexPath.init(row: index!, section: 0 )
                    self.tbvComment.scrollToRow(at: indexCommentNotification, at: .bottom, animated: true)
                }else{
                    self.listComments.insert(self.commentNotification!, at: 0)
                    self.tbvComment.reloadData()
                    self.tbvComment.scroll(to: .bottom, animated: true)
                }
            }else{
             //   let pathToLastRow = IndexPath.init(row: (self.tbvComment.indexPathsForVisibleRows?.last?.row)! + comments.count, section: 0)
             //   self.tbvComment.scrollToRow(at: pathToLastRow, at: .bottom, animated: false)
            }
            
            self.nextPageReply = nextPage
            
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func getCommentReplyHomeFeed(page: Int){
        SVProgressHUD.show()
        APIManager.getComment(commentRoot?.commentId ?? 0, page: page, type: Key.TypeReplyComment, callbackSuccess: { [weak self] (comments, nextPage) in
            guard let self = self else { return }
            self.listComments.append(contentsOf: comments)
            self.tbvComment.reloadData()

            if self.nextPageReply == 1{
                let index = self.listComments.firstIndex(where: { (comment) -> Bool in
                    return comment.commentId == self.commentNotification?.commentId
                })
                
                if index != nil{
                    let indexCommentNotification = IndexPath.init(row: index!, section: 0 )
                    self.tbvComment.scrollToRow(at: indexCommentNotification, at: .middle, animated: true)
                }else{
                    self.listComments.insert(self.commentNotification!, at: 0)
                    self.tbvComment.reloadData()
                    self.tbvComment.scroll(to: .top, animated: true)
                }
            }else{
             //   let pathToLastRow = IndexPath.init(row: (self.tbvComment.indexPathsForVisibleRows?.last?.row)! + comments.count, section: 0)
             //   self.tbvComment.scrollToRow(at: pathToLastRow, at: .bottom, animated: false)
            }
            
            self.nextPageReply = nextPage
            
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func sendCommentInfoDiary(content: String){

        SVProgressHUD.show()
        APIManager.sendComment(feed.id ?? 0, parentId: idCommentParent, content: content, userTag: userIdTag?[0] ?? 0, callbackSuccess: { [weak self] (comment) in
            guard let self = self else { return }
         //   self.listComments.append(comment)
            self.listComments.insert(comment, at: 0)
            self.tbvComment.reloadData()
            self.tbvComment.scroll(to: .bottom, animated: true)
            self.initViewInput()
            
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func sendCommnetHomeFeed(content: String){
        SVProgressHUD.show()
        APIManager.sendCommentHomeFeed(idPost == 0 ? idCommentParent : idPost, type: type, content: content, userTag: userIdTag ?? [], callbackSuccess: { [weak self] (comment) in
            guard let self = self else { return }
         //   self.listComments.append(comment)
            self.listComments.insert(comment, at: 0)
            self.tbvComment.reloadData()
            self.tbvComment.scroll(to: .bottom, animated: true)
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func likeCommentInfoDiary(_ cell: CommentTableViewCell, idComment: Int, checkLike: Int, type : Int, tableView: UITableView,index: Int){

        APIManager.likePost(idPost: idComment, type: type, callbackSuccess: { [weak self] (numberLike, checkLike) in
            guard let self = self else { return }
            cell.isLike = checkLike
            cell.numberLike = numberLike
            cell.lblNumberLike.text = "\(cell.numberLike)"
            if tableView == self.tbvCommentRoot{
                self.commentRoot?.isLiked = checkLike
                self.commentRoot?.likeNumber = numberLike
            }else{
                self.listComments[index].isLiked = checkLike
                self.listComments[index].likeNumber = checkLike
            }

        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func likeCommentHomeFeed(_ cell: CommentTableViewCell, idComment: Int, checkLike: Int, type : Int, tableView: UITableView,index: Int){
        APIManager.likeHomeFeed(id: idComment, type: type, checkLike: checkLike, callbackSuccess: { [weak self] (numberLike, checkLike) in
            guard let self = self else { return }
            cell.isLike = checkLike
            cell.numberLike = numberLike
            cell.lblNumberLike.text = "\(cell.numberLike)"
            if tableView == self.tbvCommentRoot{
                self.commentRoot?.isLiked = checkLike
                self.commentRoot?.likeNumber = numberLike
            }else{
                self.listComments[index].isLiked = checkLike
                self.listComments[index].likeNumber = checkLike
            }

        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func deleteComment(index: Int, isCheckCommentRoot: Bool, type: Int?, indexParentComment: Int?, cell : CommentTableViewCell){
        SVProgressHUD.show()
        let idDelete = !isCheckCommentRoot ? self.listComments[index].commentId ?? 0 : commentRoot?.commentId ?? 0
        let typeDelete = fromToView == .Home ? (!isCheckCommentRoot ? Key.TypeReplyComment :  Key.TypeComment) : nil
        APIManager.deleteComment(idDelete,type: typeDelete, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else { return }
            if isSuccess {
                if !isCheckCommentRoot{
                    self.listComments.remove(at: index)
                    self.tbvComment.reloadData()
                }else{
                    self.updateDataWhenDeleteComment(self.idNotification)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func editComment(index: Int, isCheckCommentRoot: Bool, content: String, indexParentComment: Int? , type: Int?, cell : CommentTableViewCell, completion:@escaping ()->()){
        
        SVProgressHUD.show()
        let idEdit = !isCheckCommentRoot ? self.listComments[index].commentId ?? 0 : commentRoot?.commentId ?? 0
        let typeEdit  = fromToView == .Home ? (!isCheckCommentRoot ? Key.TypeReplyComment :  Key.TypeComment) : nil
        APIManager.editComment(idEdit ,content, type: typeEdit ,callbackSuccess: { [weak self] (comment) in
            guard let self = self else { return }
            if !isCheckCommentRoot{
                self.listComments[index].content = comment.content
                self.tbvComment.reloadData()
                self.isCheckEdit = false
                self.handelUpdateLayoutEditComment(cell: cell, tableView: self.tbvComment)
            }else{
                self.commentRoot?.content = comment.content
                self.tbvCommentRoot.reloadData()
                self.isCheckEdit = false
                self.handelUpdateLayoutEditComment(cell: cell, tableView: self.tbvCommentRoot)
            }
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    
    
    func showAtionDeleteEdit(index: Int,isCheckCommentRoot: Bool, cell: CommentTableViewCell, type :Int?, indexParentComment: Int?, tableView: UITableView){
        let bottomVC = BaseBottomViewController.init(nib: R.nib.baseBottomViewController)
        bottomVC.selectedOption = { [weak self] indexSelect in
            guard let self = self else { return }
            switch indexSelect {
            case 0:
                print(indexSelect)
                self.isCheckEdit = true
                self.handelUpdateLayoutEditComment(cell: cell, tableView: tableView)
            case 1:
                
                self.dismiss(animated: true, completion: {
                    Utils.showAlertOKWithAction(controller: self, title: R.string.localizable.commonNoti(), message: "Bạn có chắc chắn xoá bình luận ?", cancel: true) {
                        self.deleteComment(index: index, isCheckCommentRoot: isCheckCommentRoot, type: type, indexParentComment: indexParentComment, cell: cell)
                    }
                })
                
                
                
//                let viewPopupDelete =  ViewPopupDelete.instanceFromNib()
//                viewPopupDelete.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width - 40, height: 126)
//                let popup = FFPopup.init(contentView: viewPopupDelete)
//
//                viewPopupDelete.tapDelete = {
//                   self.deleteComment(index: index, isCheckCommentRoot: isCheckCommentRoot, type: type, indexParentComment: indexParentComment, cell: cell)
//                    popup.dismiss(animated: true)
//                }
//
//                viewPopupDelete.tapCancel = {
//                    popup.dismiss(animated: true)
//                }
//
//                popup.showType = .growIn
//                popup.dismissType = .shrinkOut
//                let layout = FFPopupLayout(horizontal: .center, vertical: .center)
//                popup.show(layout: layout)
//
                
            default:
                print("123")
            }
            self.dismiss(animated: true, completion: {
                
            })
        }
        bottomVC.datasource = [R.string.localizable.infomationEditComment(), R.string.localizable.homeDelete()]
        bottomVC.height = CGFloat((bottomVC.datasource.count + 1) * 44)
        bottomVC.topCornerRadius = 20
        bottomVC.presentDuration = 0.25
        bottomVC.dismissDuration = 0.25
        self.present(bottomVC, animated: true, completion: nil)
    }
    
    func handelUpdateLayoutEditComment(cell: CommentTableViewCell, tableView: UITableView){
        cell.viewNumberLike.isHidden = self.isCheckEdit ? true : false
        cell.viewNumberReply.isHidden = self.isCheckEdit ? true : false
        cell.lblContent.isHidden = self.isCheckEdit ? true : false
        cell.btnSavedEdit.isHidden = self.isCheckEdit ? false : true
        cell.btnCancelEdit.isHidden = self.isCheckEdit ? false : true
        cell.txtContent.isHidden = self.isCheckEdit ? false : true
        cell.heightTextViewContent.isActive = self.isCheckEdit ? true : false
        cell.heightTextViewContent.constant = self.isCheckEdit ? max(cell.lblContent.frame.height, 66) : 0
        cell.layoutIfNeeded()
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func pushProfileVC(user: User){
        
        let ProfileVC = R.storyboard.main.profileViewController()
        ProfileVC?.hidesBottomBarWhenPushed = true
        ProfileVC?.check = 1
        ProfileVC?.user = user
        Utils.getTopMostViewController()?.navigationController?.pushViewController(ProfileVC!, animated: true)
        
    }

    //MARK: Action
    @IBAction func didPressViewMoreReply(_ sender: Any) {
        self.getCommentReplyHomeFeed(page: self.nextPageReply)
    }
    
    @IBAction func didPressCommentRoot(_ sender: Any) {
        switch typeNotification {
        case .LikeRepCom, .RepComt :
            guard let selfStrong = Utils.getTopMostViewController() else {
                return
            }
            Router.share.pushToViewHomeDetail(tableView: UITableView.init(), array: [],indexPath: IndexPath.init(), viewSelf: selfStrong, idFeed: feed.id ?? 0, editFromView: .Home, completion: { homeFeeds in
            })
        case .RepcomSecret:
            guard let selfStrong = Utils.getTopMostViewController() else {
                return
            }
            Router.share.pushToViewHomeDetail(tableView: UITableView.init(), array: [],indexPath: IndexPath.init(), viewSelf: selfStrong, idFeed: feed.id ?? 0, editFromView: .Secret, completion: { homeFeeds in
            })
        case .LikeRepComInfo, .LikeRepComDiary, .RepcomInfo, .RepComDiary:
            guard let selfStrong = Utils.getTopMostViewController() else {
                return
            }
            Router.share.pushToViewDiaryDetail(tableView: UITableView.init(), array: [], indexPath: IndexPath.init(),row: IndexPath.init().row, viewSelf: selfStrong, idFeed: feed.id ?? 0, editFromView: .Home) { (diary) in
            }
        default:
            print("1234")
        }
        
    }
    
    deinit {
        self.tbvCommentRoot?.removeObserver(self, forKeyPath: "contentSize")
    }
    
}

//MARK: UITableview Delegate
extension NotificationCommentViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == tbvCommentRoot ? 1 : listComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.commentTableViewCell, for: indexPath)!
        
        cell.leadingConstrain.constant = 11
        if tableView == tbvComment{
            cell.btnLike.tag = indexPath.row
            cell.setupData(comment: listComments[indexPath.row], nextPage: 1, isReply: true)
            cell.contentView.backgroundColor  = UIColor.init(hexString: AppColor.colorReplyCommentBG, alpha: 0.14)
            cell.viewLine.isHidden = false
            cell.lblNumberReply.isHidden = true
            setupUIEditComment(cell: cell)
            cell.buttonLoadMore.isHidden = true
            cell.heightButtonLoadMore.constant = 0
            if self.nextPageReply > 0 {
                       if indexPath.row == self.listComments.count - 1 {
                           cell.buttonLoadMore.isHidden = false
                           cell.heightButtonLoadMore.constant = 29
                       }
                   }
            cell.tapOption = { [weak self] index, _, _ in
                guard let self = self else { return }
                print(index)
                self.showAtionDeleteEdit(index: index, isCheckCommentRoot: false, cell: cell, type: self.type!, indexParentComment: nil, tableView: self.tbvComment)
            }
            
            cell.tapCancelEdit = { [weak self] index, _, _ in
                guard let self = self else { return }
                self.isCheckEdit = false
                self.handelUpdateLayoutEditComment(cell: cell, tableView: self.tbvComment)
                print(index)
            }
            cell.tapShowMoreReplyComment = { [weak self] index,_ ,type in
                guard let self = self else { return }
                self.getCommentReplyHomeFeed(page: self.nextPageReply)
                
            }
            cell.tapSavedEdit = { [weak self] index, _, _ in
                guard let self = self else { return }
                self.editComment(index: index, isCheckCommentRoot: false, content: cell.txtContent.text, indexParentComment: nil, type: nil, cell: cell, completion: {
                    
                })
            }
            
            cell.tapLike = { [weak self] index, _, _ in
                guard let self = self else { return }
                switch self.typeNotification {
                case .LikeRepCom, .RepComt, .LikeRepComSecret, .RepcomSecret:
                    self.likeCommentHomeFeed(cell, idComment: self.listComments[index].commentId ?? 0, checkLike: self.listComments[index].isLiked ?? 0, type: Key.TypeLikeReplyComment, tableView: self.tbvCommentRoot, index: index)
                case .LikeRepComInfo, .LikeRepComDiary, .RepcomInfo, .RepComDiary:
                    self.likeCommentInfoDiary(cell, idComment: self.listComments[index].commentId ?? 0, checkLike: self.listComments[index].isLiked ?? 0, type: Key.TypeLikeReplyComment, tableView: self.tbvCommentRoot, index: index)
                default:
                    print("1234")
                }
            }
            
            cell.tapReply = { [weak self] index  in
                guard let self = self else { return }
                self.userIdTag = [self.listComments[index].user?.id ?? 0]
                
                // check Secret
                if self.listComments[index].secretNumber ?? 0 > 0{
                    self.lblNameUserReply.text = "@Ẩn danh \(self.listComments[index].secretNumber ?? 0)"
                }else{
                    self.lblNameUserReply.text = "@\(self.listComments[index].user?.displayName ?? "")"
                }
            }
            
            cell.tapUser = { [weak self] index, user, type in
                guard let self = self else { return }
                var user: User?
                if type == 5{
                    if self.listComments[index].userTag?.count ?? [User]().count > 0{
                        user = self.listComments[index].userTag![0]
                    }
                }else{
                    user = self.listComments[index].user
                }
                
                if let user = user {
                    print(user.id as Any)
                    self.pushProfileVC(user: user)
                }
                
            }
            
        }else{
            cell.btnLike.tag = indexPath.row
            cell.setupData(comment: commentRoot!, nextPage: 1, isReply: true)
            cell.viewLine.isHidden = true
            cell.constrainHeightViewReply.constant = 0
            setupUIEditComment(cell: cell)
            cell.buttonLoadMore.isHidden = true
            cell.heightButtonLoadMore.constant = 0
            cell.tapOption = { index, _, _ in
                print(index)
                self.showAtionDeleteEdit(index: index, isCheckCommentRoot: true, cell: cell, type: self.type!, indexParentComment: nil, tableView: self.tbvCommentRoot)
            }
            
            cell.tapCancelEdit = { [weak self] index, _, _ in
                guard let self = self else { return }
                self.isCheckEdit = false
                self.handelUpdateLayoutEditComment(cell: cell, tableView: self.tbvCommentRoot)
                print(index)
            }
            
            cell.tapSavedEdit = { [weak self] index, _, _ in
                guard let self = self else { return }
                self.editComment(index: index, isCheckCommentRoot: true, content: cell.txtContent.text, indexParentComment: nil, type: nil, cell: cell, completion: {
                    
                })
            }
            
            cell.tapLike = { [weak self] index, _, _ in
                guard let self = self else { return }
                switch self.typeNotification {
                case .LikeRepCom, .RepComt, .LikeRepComSecret, .RepcomSecret:
                    self.likeCommentHomeFeed(cell, idComment: self.commentRoot!.commentId ?? 0, checkLike: self.commentRoot!.isLiked ?? 0, type: Key.TypeLikeComment, tableView: self.tbvCommentRoot, index: index)
                case .LikeRepComInfo, .LikeRepComDiary, .RepcomInfo, .RepComDiary:
                    self.likeCommentInfoDiary(cell, idComment: self.listComments[index].commentId ?? 0, checkLike: self.listComments[index].isLiked ?? 0, type: Key.TypeLikeComment, tableView: self.tbvCommentRoot, index: index)
                default:
                    print("1234")
                }
            }
            
            cell.tapReply = { [weak self] index  in
                guard let self = self else { return }
                self.lblNameUserReply.text = "@\(self.commentRoot!.user?.displayName ?? "")"
                
                // check Secret
                if self.listComments[index].secretNumber ?? 0 > 0{
                    self.lblNameUserReply.text = "@Ẩn danh \(self.commentRoot?.secretNumber ?? 0)"
                }else{
                    self.lblNameUserReply.text = "@\(self.commentRoot?.user?.displayName ?? "")"
                }
                
                self.userIdTag = [self.commentRoot!.user?.id ?? 0]
            }
            
            cell.tapUser = { [weak self] index, user, type in
                guard let self = self else { return }
                let user = self.commentRoot?.user
                print(user?.id as Any)
                self.pushProfileVC(user: user!)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return height ?? UITableView.automaticDimension
    }
}
