//
//  ChildbirthViewController.swift
//  Beberia
//
//  Created by IMAC on 8/13/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import Kingfisher
import SVProgressHUD
import PullToRefresh

class ChildbirthViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var heighBtn: NSLayoutConstraint!
    @IBOutlet weak var heightClv: NSLayoutConstraint!
    @IBOutlet weak var heightTbv: NSLayoutConstraint!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var tbvNewHot: UITableView!
    @IBOutlet weak var clvChild: UICollectionView!
    @IBOutlet weak var btnMore: UIButton!
    
    // MARK: - Properties
    
    var id = 0
    {
        didSet {
            if listCategories.count == 0{
                getCategory(parentId: id)
            }
        }
    }
    var listCategoriesNewHot = [Diaries]()
    var listCategories = [Category]()
    let refresher = PullToRefresh()
    fileprivate var heightDictionary: [IndexPath : CGFloat] = [:]
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    //    clvChild.reloadData()
    //    id = 0
        self.getCategoryNewHot(category_id: "5", page: 1)
    }
    
    // MARK: - Request API
    
    func getCategory(parentId: Int){
        APIManager.getCategoryShow(parentId: id, callbackSuccess: { [weak self](categories) in
            guard let self = self else { return }
            self.listCategories = categories
            self.clvChild.reloadData()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func getCategoryNewHot(category_id: String,page:Int){
        
    //    SVProgressHUD.show()
        APIManager.getListDiaryMore(type: category_id, page: page, callbackSuccess: { [weak self] (detailMore,next_page) in
            guard let self = self else { return }
            self.listCategoriesNewHot = detailMore
            if detailMore.count > 3 {
                self.heighBtn.constant = 30
                self.btnMore.isHidden = false
            }
            self.tbvNewHot.reloadData()
            self.scrollview.endRefreshing(at: .top)
            self.scrollview.finishInfiniteScroll(completion: { (collection) in
            })
 //           SVProgressHUD.dismiss()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
 //           SVProgressHUD.dismiss()
        }
        
    }
    
    // MARK: - SetupUI
    
    override func viewDidLayoutSubviews() {
        if let flowLayout = self.clvChild.collectionViewLayout as? UICollectionViewFlowLayout {
            let widthCell = self.view.frame.width / 2
            flowLayout.itemSize = CGSize(width: widthCell, height: widthCell)

//            let widthCell = self.view.frame.width / 2.2
//            flowLayout.itemSize = CGSize(width: widthCell, height: widthCell + 30)
            flowLayout.minimumInteritemSpacing = 0
            flowLayout.minimumLineSpacing = 0
            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            switch self.listCategories.count {
            case 0,1,2:
                self.heightClv.constant = widthCell * 1
            case 3,4:
                self.heightClv.constant = widthCell * 2
            case 5,6:
                self.heightClv.constant = widthCell * 3
            default:
                self.heightClv.constant = widthCell * 3
            }
            
        }
    }
    
    func setupUI(){
        clvChild.register(R.nib.categoryCollectionViewCell)
        clvChild.isScrollEnabled = false
        tbvNewHot.register(R.nib.newHotTableViewCell)
        tbvNewHot.delegate = self
        tbvNewHot.dataSource = self
        tbvNewHot.separatorStyle = .none
        tbvNewHot.rowHeight = 140
        self.tbvNewHot.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        self.clvChild.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        btnMore.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 15)
        heighBtn.constant = 0
        btnMore.isHidden = true
        scrollview.addPullToRefresh(refresher) {
            self.getCategoryNewHot(category_id: "5", page: 1)
            self.scrollview.startRefreshing(at: .top)
        }
        
        
    }
    
    //MARK: - Helper methods
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    //    tbvNewHot.layer.removeAllAnimations()
        self.heightTbv.constant = tbvNewHot.contentSize.height
        
//        clvChild.layer.removeAllAnimations()
//        
//        
//        UIView.animate(withDuration: 0.5) {
//            self.updateViewConstraints()
//            self.view.layoutIfNeeded()
//        }
    }
    
    // MARK: - Action
    
    @IBAction func didPressMore(_ sender: Any) {
        let listNewHotVC = ListNewHotViewController.init(nib: R.nib.listNewHotViewController)
        listNewHotVC.hidesBottomBarWhenPushed = true
        Utils.getTopMostViewController()?.navigationController?.pushViewController(listNewHotVC, animated: true)
    }
    
    // MARK: - Deinit
    
    deinit {
        self.tbvNewHot?.removeObserver(self, forKeyPath: "contentSize")
        self.clvChild?.removeObserver(self, forKeyPath: "contentSize")
    }
}

// MARK: - UICollectionViewDelegate

extension ChildbirthViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.categoryCollectionViewCell, for: indexPath)!
        cell.lblNameCategory.text = self.listCategories[indexPath.row].categoryName
        cell.imgCategory.kf.setImage(with: URL(string: self.listCategories[indexPath.row].urlImage ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      //  if Utils.checkLogin(selfView: self){
            let type = self.listCategories[indexPath.row].type ?? 0
            switch type {
            case 1:
                print(type)
                let postVC = DetailInfoViewController.init(nib: R.nib.detailInfoViewcontroller)
                postVC.hidesBottomBarWhenPushed = true
                postVC.titleView = self.listCategories[indexPath.row].categoryName ?? ""
                postVC.gettype = self.listCategories[indexPath.row].type ?? 0
                postVC.getcategory_id = self.listCategories[indexPath.row].categoryId ?? 0
                Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
            case 2:
                print(type)
                let postVC = DetailParentViewcontroller.init(nib: R.nib.detailParentViewcontroller)
                postVC.hidesBottomBarWhenPushed = true
                postVC.gettile = self.listCategories[indexPath.row].categoryName ?? ""
                postVC.gettype2 = self.listCategories[indexPath.row].type ?? 0
                postVC.getcategory_id2 = self.listCategories[indexPath.row].categoryId ?? 0
                Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
            case 0:
                let postVC = DetailMoreController.init(nib: R.nib.detailInfoMoreController)
                postVC.hidesBottomBarWhenPushed = true
                postVC.title = self.listCategories[indexPath.row].categoryName ?? ""
                postVC.getcategory_idmore = self.listCategories[indexPath.row].categoryId ?? 0
                Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
                
            default:
                print(type)
            }
    //    }
    }
}

// MARK: - UITableViewDelegate

extension ChildbirthViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listCategoriesNewHot.count > 3 ? 3 : self.listCategoriesNewHot.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.newHotTableViewCell, for: indexPath)!
        let categoryHot = self.listCategoriesNewHot[indexPath.row]
        cell.setupDataNewsHot(category: categoryHot)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return height ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  if Utils.checkLogin(selfView: self){
            tableView.deselectRow(at: indexPath, animated: true)
            let idPost = self.listCategoriesNewHot[indexPath.row].id ?? 0
            self.pushPostDetailWithObjectDiary(idPost: idPost) { (numberLike, isLike, numberComment) in
                self.listCategoriesNewHot[indexPath.row].likeNumber = numberLike
                self.listCategoriesNewHot[indexPath.row].isLiked = isLike
                self.listCategoriesNewHot[indexPath.row].commentNumber = numberComment
                self.tbvNewHot.reloadData()
            }
      //  }
    }
    
}

