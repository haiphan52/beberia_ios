//
//  ListNewHotViewController.swift
//  Beberia
//
//  Created by IMAC on 9/26/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import PullToRefresh

class ListNewHotViewController: BaseViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var tbvNewHot: UITableView!
    
    // MARK: - Properties
    
    var isLoadingMore = false
    var listNewHot = [Diaries]()
    var page = 1
    var nextpage = 0
    let refresher = PullToRefresh()
    var keyTypeNewHot = "5"
        
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        getListNewHot(type: keyTypeNewHot, page: page)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        self.title = R.string.localizable.infomationTitleNewHot()
        tbvNewHot.register(R.nib.newHotTableViewCell)
        tbvNewHot.delegate = self
        tbvNewHot.dataSource = self
        tbvNewHot.separatorStyle = .none
        customLeftBarButton()
        tbvNewHot.infiniteScrollTriggerOffset = 700
        tbvNewHot.showsVerticalScrollIndicator = false
        tbvNewHot.addInfiniteScroll { (tableView) in
            tableView.performBatchUpdates({ () -> Void in
                // update collection view
                self.isLoadingMore = true
                if self.nextpage > 0 {
                    self.page = self.page + 1
                    self.getListNewHot(type: self.keyTypeNewHot, page: self.page)
                }else{
                    self.tbvNewHot.finishInfiniteScroll(completion: { (collection) in
                    })
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                tableView.finishInfiniteScroll()
            });
            
        }

        
        tbvNewHot.addPullToRefresh(refresher) {
            self.isLoadingMore = true
            self.page = 1
            self.getListNewHot(type: self.keyTypeNewHot, page: self.page)
            self.tbvNewHot.startRefreshing(at: .top)
        }
    }
    
    // MARK: - Request API
    
    func getListNewHot(type: String,page:Int){
        if !isLoadingMore{
            SVProgressHUD.show()
        }
        APIManager.getListDiaryMore(type: type, page: page, callbackSuccess: { [weak self] (detailMore,next_page) in
            guard let self = self else { return }
            self.nextpage = next_page
            if self.page == 1{
                self.listNewHot.removeAll()
            }
            if self.listNewHot.count == 0 {
                self.listNewHot = detailMore
            }else{
                self.listNewHot.append(contentsOf: detailMore)
            }
            self.tbvNewHot.reloadData()
            self.tbvNewHot.endRefreshing(at: .top)
//            self.tbvNewHot.finishInfiniteScroll(completion: { (collection) in
//            })
            SVProgressHUD.dismiss()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        }
    }
}

// MARK: - UITableViewDelegate

extension ListNewHotViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listNewHot.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.newHotTableViewCell, for: indexPath)!
        let categoryHot = self.listNewHot[indexPath.row]
        cell.setupDataNewsHot(category: categoryHot)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utils.checkLogin(selfView: self){
            tableView.deselectRow(at: indexPath, animated: true)
            let idPost = self.listNewHot[indexPath.row].id ?? 0
            self.pushPostDetailWithObjectDiary(idPost: idPost) { (numberLike, isLike, numberComment) in
                self.listNewHot[indexPath.row].likeNumber = numberLike
                self.listNewHot[indexPath.row].isLiked = isLike
                self.listNewHot[indexPath.row].commentNumber = numberComment
                self.tbvNewHot.reloadData()
            }
        }
    }
}

