//
//  PostDetailViewController.swift
//  Beberia
//
//  Created by IMAC on 8/14/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import WebKit
//import DTOverlayController
import SVProgressHUD

class PostDetailViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightWebView: NSLayoutConstraint!
    @IBOutlet weak var heightTbvComment: NSLayoutConstraint!
    @IBOutlet weak var tbvComment: CommentTableView!
    @IBOutlet weak var viewLikeComment: UIView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet weak var tvInputComment: UITextView!
    @IBOutlet weak var heightConstraintViewInput: NSLayoutConstraint!
    @IBOutlet weak var heightViewInput: NSLayoutConstraint!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblNameUserReply: UILabel!
    @IBOutlet weak var viewNotiReplyComment: UIView!
    @IBOutlet weak var imgAvaUser: UIImageView!
    @IBOutlet weak var lblNameUser: UILabel!
    @IBOutlet weak var lblNumberLike: UILabel!
    @IBOutlet weak var lblNumberCom: UILabel!
    
    // MARK: - Properties
    
    var idPost = 0
    var numberLike: Int = 0
    var isLike: Int = 0
    var viewLikeAndComment: ViewLikeComment = ViewLikeComment.instanceFromNib()
    var postDetailModel = PostDetailModel.init(json: "")
    let progressView: UIProgressView = {
        let view = UIProgressView(progressViewStyle: .default)
        view.progressTintColor = UIColor.init(hexString: AppColor.mainColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    var updateObject:(_ postDetal: PostDetailModel)->() = {_ in}
    var updateLikeNumber:(_ likeNumber: Int, _ isLike: Int, _ numberComment: Int)->() = {_,_,_  in}
    var titleVC = ""
    var isTyping = false
    var isFristTyping = false
    var commentNotification : Comment?
    
    var isHidenViewInput = true{
        didSet{
            UIView.animate(withDuration: 0.5) {
                self.viewInput.isHidden = self.isHidenViewInput
                self.heightConstraintViewInput.constant = self.isHidenViewInput ? 0 : 50
            }
        }
    }
    
    // MARK: - Deinit
    
    deinit {
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
        self.tbvComment?.removeObserver(self, forKeyPath: "contentSize")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.updateNumberComment, object: nil)
    }
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isHidenViewInput = true
        setupUI()
        
        webView.addObserver(self,
                            forKeyPath: #keyPath(WKWebView.estimatedProgress),
                            options: .new,
                            context: nil)
        getPostDetail()
        scrollView.delegate = self
        
        self.tbvComment.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        self.webView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    //MARK: - Helper methods
    
    @objc func tapOption(){
        showAtionTunrOnOff(turnOnOff: postDetailModel.turnOn ?? 0, id: idPost)
    }
    
    func showAtionTunrOnOff(turnOnOff:Int, id:Int){
        var onOff = R.string.localizable.homeTunrOnNoti()
        if turnOnOff == 1 {
            onOff = R.string.localizable.homeTunrOffNoti()
        }
        self.showActionBottom(datasource: [onOff], isNoTextColorRed: false) { (index) in
            switch index {
            case 0:
                self.settingNoti(turnOn: turnOnOff, id: id)
            default:
                print("123")
            }
        }
    }
    
    func initTextViewComment(){
        tvInputComment.cornerRadius = 15
        tvInputComment.delegate = self
        tvInputComment.text =  R.string.localizable.infomationWriteComment()
        tvInputComment.textColor = UIColor.lightGray
        btnSend.isEnabled = true
        if  tvInputComment.textColor == UIColor.lightGray{
            btnSend.isEnabled = false
        }
        tvInputComment.autocorrectionType = .no
        viewNotiReplyComment.isHidden = true
    }
    
    func initViewInput(){
        self.tvInputComment.text =  R.string.localizable.infomationWriteComment()
        self.tvInputComment.textColor = UIColor.lightGray
        self.heightViewInput.constant = 33
        self.heightConstraintViewInput.constant = 55
        Utils.getTopMostViewController()?.view.endEditing(true)
    }
    
    func showViewNotiReply(){
        scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 40, right: 0)
        UIView.animate(withDuration: 0.25) {
            self.viewNotiReplyComment.center.y -= self.viewNotiReplyComment.bounds.height
        }
    }
    
    @objc func updateNumberComment(notification: Notification){
        guard let object = notification.object as? [String:Any] else {
            return
        }
        let numberComment = object[Key.KeyNotification.numberComment] as? Int
        viewLikeAndComment.lblNumberComment.text = "\(numberComment ?? 0)"
        viewLikeAndComment.postDetailModel.commentNumber = numberComment
        viewLikeAndComment.numberComment = numberComment ?? 0
        self.lblNumberCom.text = "\(numberComment ?? 0) bình luận"
    }
    
    // MARK: - Show and Hide ProgressView
    
    func showProgressView() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.progressView.alpha = 1
        }, completion: nil)
    }
    
    func hideProgressView() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.progressView.alpha = 0
        }, completion: nil)
    }
    
    // MARK: - Update height Text View
    
    func updateHeightTextView(_ textView: UITextView){
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
        heightConstraintViewInput.constant = newFrame.height + 22
        heightViewInput.constant = newFrame.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Overrides
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView.progress = Float(webView.estimatedProgress)
        }
        
        if keyPath == "contentSize" {
            
            tbvComment.layer.removeAllAnimations()
            heightTbvComment.constant = tbvComment.contentSize.height
            
            UIView.animate(withDuration: 0.5) {
                self.updateViewConstraints()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - Request API
    
    func settingNoti(turnOn:Int, id:Int ){
        SVProgressHUD.show()
        APIManager.settingNotification(id: id, turnOn: turnOn, callbackSuccess: { [weak self] (isSucces) in
            guard let self = self else { return }
            self.postDetailModel.turnOn = isSucces
            SVProgressHUD.dismiss()
        }, failed: { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
        })
    }
    
    func getPostDetail(){
        APIManager.getPostDetail(idPost: idPost, callbackSuccess: { [weak self] (postModel) in
            guard let self = self else { return }
            self.postDetailModel = postModel
            self.loadData()
            self.lblNumberLike.text = "\(self.postDetailModel.likeNumber ?? 0) thích"
            self.lblNumberCom.text = "\(self.postDetailModel.commentNumber ?? 0) bình luận"
            self.lblNameUser.text = self.postDetailModel.createdUser?.name
            self.imgAvaUser.kf.setImage(with: URL.init(string: self.postDetailModel.createdUser?.avatar ?? "") ,placeholder: UIImage.init(named: Key.ImagePlaceholder))
            self.viewLikeAndComment.fromToView = .Info
            self.viewLikeAndComment.postDetailModel = postModel
            self.viewLikeAndComment.loadData()
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    // MARK: - Overrides
    
    override func backVC() {
        print(self.postDetailModel.likeNumber as Any )
        print(self.postDetailModel.isLiked as Any )
        updateObject(postDetailModel)
        updateLikeNumber(self.postDetailModel.likeNumber ?? 0, self.postDetailModel.isLiked ?? 0, self.postDetailModel.commentNumber ?? 0)
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - SetupData
    
    func loadData(){
        let htmlStart = "<HTML><HEAD><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, shrink-to-fit=no\"></HEAD><BODY><style=\"font-family: \(AppFont.HelveticaNeue); font-size: 16\">"
        let htmlEnd = "</style></BODY></HTML>"
        let title = "<h1 style='font-size:26px;font-family: \(AppFont.HelveticaNeueBold); text-align: '>\(self.postDetailModel.title ?? "")</h1>"
        var content = self.postDetailModel.content ?? ""
//        print(content)
//        if content.contains("Times New Roman"){
//            content = content.replacingOccurrences(of: "Times New Roman", with: "NotoSans")
//        }
        
        let htmlString = "\(htmlStart) \(title) \(content) \(htmlEnd)"
        
        webView.loadHTMLString( htmlString, baseURL: nil)
    }
    
    // MARK: - SetupUI
    
    func customrightBarButton(){
        if Utils.checkLogin(selfView: self, showAlert: false){
            let rightBarButtonItemOption = UIBarButtonItem.menuButton(self, action: #selector(HomeDetailViewController.tapOption), imageName: R.image.ic_option1() ?? UIImage.init(), height: 40, width: 40)
            self.navigationItem.rightBarButtonItem = rightBarButtonItemOption
        }
    }
    
    func setupUI(){
        viewInput.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 5)
        self.extendedLayoutIncludesOpaqueBars = true
        customLeftBarButton()
        // self.title = R.string.localizable.infomationTitlePost()
        self.title = titleVC
        //        lblNameUser.text = postDetailModel.
        
        viewLikeAndComment.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 0)
        viewLikeAndComment.frame = viewLikeComment.bounds
        viewLikeComment.addSubview(viewLikeAndComment)
        viewLikeAndComment.tapButtonComment = {
            self.scrollView.scrollToBottom(animated: true)
            self.isHidenViewInput = false
            self.tvInputComment.becomeFirstResponder()
            self.isFristTyping = true
        }
        webView.navigationDelegate = self
        webView.scrollView.isScrollEnabled = false
        [progressView].forEach { self.view.addSubview($0) }
        progressView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        progressView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        if #available(iOS 11.0, *) {
            progressView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            // Fallback on earlier versions
        }
        progressView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        customrightBarButton()
        NotificationCenter.default.addObserver(self, selector: #selector(updateNumberComment), name: NSNotification.Name.updateNumberComment, object: nil)
        initTextViewComment()
        
        tbvComment.tapReaply = { name in
            self.isHidenViewInput = false
            self.lblNameUserReply.text = ""
            self.viewNotiReplyComment.isHidden = true
            
            self.lblNameUserReply.text = name
            self.viewNotiReplyComment.isHidden = false
            self.showViewNotiReply()
        }
        
    }
    
    // MARK: - Action
    
    @IBAction func didPressSendComment(_ sender: Any) {
        guard let content = tvInputComment.text?.trimmingCharacters(in: .whitespacesAndNewlines), content.count > 0 else {
            return
        }
        tbvComment.didPressSendComment(content: content, completion: {
            self.initViewInput()
            self.lblNameUserReply.text = ""
            self.viewNotiReplyComment.isHidden = true
        })
    }
    
    @IBAction func didPressCloseViewNotiReply(_ sender: Any) {
        lblNameUserReply.text = ""
        viewNotiReplyComment.isHidden = true
        tbvComment.parentId = nil
        tbvComment.indexParent = 0
        tbvComment.userIdTag = []
        scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    @IBAction func didPressSave(_ sender: Any) {
        
    }
    
    @IBAction func didPessLike(_ sender: Any) {
        
    }
    
    @IBAction func didPressComment(_ sender: Any) {
        
    }
}

extension UIScrollView {
    func scrollToBottom(animated: Bool) {
        UIView.animate(withDuration: 0.5) {
            if self.contentSize.height < self.bounds.size.height { return }
            let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
            self.setContentOffset(bottomOffset, animated: animated)
        }
        
    }
}

// MARK: - WKNavigationDelegate

extension PostDetailViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideProgressView()
        
//        let changeFontFamilyScript = "document.getElementsByTagName(\'body\')[0].style.fontFamily = \"\(AppFont.NotoSansBold)\";"
//        webView.evaluateJavaScript(changeFontFamilyScript) { (response, error) in
//            debugPrint("Am here")
//            if response != nil {
                self.webView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    self.heightWebView.constant = height as! CGFloat
                    
                    if Utils.checkLogin(selfView: self, showAlert: false){
                        self.tbvComment.fromToView = .Info
                        self.tbvComment.idPost = self.idPost
                        self.tbvComment.commentNotification = self.commentNotification
                        self.tbvComment.numberComment = self.postDetailModel.commentNumber ?? 0
                    }
                    
                    UIView.animate(withDuration: 0.5) {
                        self.updateViewConstraints()
                        self.view.layoutIfNeeded()
                    }
                })
//            }
//        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.showProgressView()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.hideProgressView()
    }
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else{
            decisionHandler(.allow)
            return
        }
        
        let urlString = url.absoluteString.lowercased()
        if urlString.starts(with: "http://") || urlString.starts(with: "https://") {
            decisionHandler(.cancel)
            UIApplication.shared.open(url, options: [:])
        } else {
            decisionHandler(.allow)
        }
    }
    
}

// MARK: - UITextViewDelegate

extension PostDetailViewController: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        
        updateHeightTextView(textView)
        btnSend.isEnabled = true
        isTyping = true
        
        if (tvInputComment.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! || tvInputComment.text.count < 1{
            btnSend.isEnabled = false
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        updateHeightTextView(textView)
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        isFristTyping = false
        isTyping = false
        if textView.text.isEmpty {
            tvInputComment.text =  R.string.localizable.infomationWriteComment()
            textView.textColor = UIColor.lightGray
        }
    }
    
}
