//
//  InfoViewController.swift
//  Beberia
//
//  Created by IMAC on 8/13/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

class InfoViewController: BaseViewController, CustomSegmentedControlDelegate {

    // MARK: - CustomSegmentedControlDelegate
    
    func changeToIndex(index: Int) {
        print(index)
        pageMenu?.moveToPage(index)
    }
    
    // MARK: - Outlets
    @IBOutlet weak var constrainTop: NSLayoutConstraint!
    @IBOutlet weak var constrainBottom: NSLayoutConstraint!
    @IBOutlet weak var heightSegment: NSLayoutConstraint!
    @IBOutlet weak var customSegmetnView: CustomSegmentView!
    {
        didSet{
            customSegmetnView.setButtonTitles(buttonTitles: [R.string.localizable.infomationTitleMe(),R.string.localizable.infomationTitleBe()])
            customSegmetnView.selectorViewColor = customSegmetnView.selectorTextColor
            customSegmetnView.selectorTextColor = customSegmetnView.selectorTextColor
            customSegmetnView.delegate = self
        }
    }
    @IBOutlet weak var viewIndicationChild: UIView!
    @IBOutlet weak var viewIndicationMom: UIView!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Properties
    
    var pageMenu : CAPSPageMenu?
    var listCategories = [Category]()
    var childVC: ChildbirthViewController!
    var momVC: ChildbirthViewController!
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        LoaddingManager.initLoadding(view: self.view)
        getCategory(parentId: 5)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.delegate = self
    }
    
    //MARK: - Helper methods
    
    @objc func tapSreach(){
        if Utils.checkLogin(selfView: self) {
            let homeVC = HomeInfo.init(nib: R.nib.homeInfo)
            homeVC.indext = 2
            homeVC.hidesBottomBarWhenPushed = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return false
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        
        customSegmetnView.isHidden = true
        heightSegment.constant = 0
      //  constrainTop.constant = 0
        constrainBottom.constant = 0
        viewIndicationMom.isHidden = true
        viewIndicationChild.isHidden = true
        
        let label = UILabel()
        label.text = "Cẩm nang"//R.string.localizable.infomationInfomation()
        label.textColor = UIColor.init(hexString: "0299fe")
        label.font = UIFont(name:"\(AppFont.NotoSansBold)", size: 24)
     //   self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: label)
        
        let leftBarButtonItem = UIBarButtonItem.menuButton(self, action: #selector(BaseViewController.backVC), imageName: R.image.ic_Back() ?? UIImage.init(), height: 30, width: 40, view: self.view)
        self.navigationItem.leftBarButtonItems = [leftBarButtonItem, UIBarButtonItem.init(customView: label)]
        self.navigationController?.navigationBar.isTranslucent = false
        
        
        self.navigationController?.navigationBar.isTranslucent = false
        let rightBarButtonSetting = UIBarButtonItem.menuButton(self, action: #selector(InfoViewController.tapSreach), imageName: R.image.ic_SearchLength() ?? UIImage.init(), height: 35, width: 150)
        self.navigationItem.rightBarButtonItems = [rightBarButtonSetting]
        self.extendedLayoutIncludesOpaqueBars = true

    }
    
    func setupPageMenu(){
        var controllerArray : [UIViewController] = []
        childVC = ChildbirthViewController.init(nib: R.nib.childbirthViewController)
        childVC.title = R.string.localizable.infomationTitleMe()
        childVC.listCategories = listCategories
        controllerArray.append(childVC)
        momVC = ChildbirthViewController.init(nib: R.nib.childbirthViewController)
        momVC.title = R.string.localizable.infomationTitleBe()
        controllerArray.append(momVC)
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .menuItemSeparatorColor(UIColor.red),
            .scrollMenuBackgroundColor(UIColor.init(hexString: "ffffff", alpha: 1.0)!),
            .selectionIndicatorColor(UIColor.init(hexString: "fd799d", alpha: 1.0)!),
            .menuItemFont(UIFont.init(name: AppFont.HelveticaNeueBold, size: 23)!),
            .unselectedMenuItemLabelColor(UIColor.init(hexString: AppColor.colorBlack, alpha: 0.2)!),
            .selectedMenuItemLabelColor(UIColor.init(hexString: "fd799d", alpha: 0.87)!),
            .menuHeight(50),
            .menuItemSeparatorWidth(0),
            .selectionIndicatorHeight(3),
            .menuItemWidth(80),
            .enableHorizontalBounce(false),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .centerMenuItems(true)
        ]
      
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: containerView.frame.width, height: containerView.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        containerView.addSubview(pageMenu!.view)
    }
    
    // MARK: - Request API
    
    func getCategory(parentId: Int){
        LoaddingManager.startLoadding()
        APIManager.getCategoryShow(parentId: parentId, callbackSuccess: { [weak self] (categories) in
            guard let self = self else { return }
            self.listCategories = categories
            self.setupPageMenu()
            self.viewIndicationMom.isHidden = true
            self.viewIndicationChild.isHidden = false
            LoaddingManager.stopLoadding()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            LoaddingManager.stopLoadding()
        }
    }
}

// MARK: - CAPSPageMenuDelegate

extension InfoViewController: CAPSPageMenuDelegate{
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
       // customSegmetnView.setIndex(index: index)
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        if index == 1{
            momVC.id = 4
        }
        self.viewIndicationMom.isHidden = index == 0
        self.viewIndicationChild.isHidden = index == 1
    }
}

