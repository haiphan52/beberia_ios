//
//  InfoViewController.swift
//  Beberia
//
//  Created by IMAC on 8/13/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

class DetailParentViewcontroller: BaseViewController, CustomSegmentedControlDelegate {
    
    // MARK: - CustomSegmentedControlDelegate
            
            func changeToIndex(index: Int) {
                print(index)
                pageMenu?.moveToPage(index)
            }

    // MARK: - Outlets
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var customSegmetnView: CustomSegmentView!
         {
             didSet{
                 customSegmetnView.setButtonTitles(buttonTitles: [R.string.localizable.infomationTitleHN(),R.string.localizable.infomationTitleHCM()])
                 customSegmetnView.selectorViewColor = customSegmetnView.selectorTextColor
                 customSegmetnView.selectorTextColor = customSegmetnView.selectorTextColor
                 customSegmetnView.delegate = self
             }
         }
    
    // MARK: - Properties
    
    var pageMenu : CAPSPageMenu?
    var listCategories = [Category]()
    var childVC: DetailInfoViewController!
    var momVC: DetailInfoViewController!
    var gettype2 = 0
    var getcategory_id2 = 0
    var gettile = ""
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupPageMenu()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
      //  setupPageMenu()
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        self.title = gettile
        customLeftBarButton()
    }
    
    func setupPageMenu(){
        var controllerArray : [UIViewController] = []
        childVC = DetailInfoViewController.init(nib: R.nib.detailInfoViewcontroller)
        childVC.title = R.string.localizable.infomationTitleHN()
        childVC.getcategory_id = getcategory_id2
        childVC.gettype = gettype2
        childVC.getlocation = "ha-noi"
        controllerArray.append(childVC)
        momVC = DetailInfoViewController.init(nib: R.nib.detailInfoViewcontroller)
        momVC.title = R.string.localizable.infomationTitleHCM()
        momVC.getcategory_id = getcategory_id2
        momVC.gettype = gettype2
        momVC.getlocation = "ho-chi-minh"
        controllerArray.append(momVC)
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor.init(hexString: AppColor.colorBGMenu, alpha: 1.0)!),
            .selectionIndicatorColor(UIColor.init(hexString: AppColor.colorYellowDark, alpha: 1.0)!),
            .menuItemFont(UIFont.init(name: AppFont.HelveticaNeueBold, size: 16)!),
            .unselectedMenuItemLabelColor(UIColor.init(hexString: AppColor.colorBlack, alpha: 0.5)!),
            .selectedMenuItemLabelColor(UIColor.black),
            .menuHeight(0),
            .menuItemSeparatorWidth(0),
            .selectionIndicatorHeight(3),
            .menuItemWidth(80),
            .enableHorizontalBounce(false),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .centerMenuItems(true)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: containerView.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        containerView.addSubview(pageMenu!.view)
    }
 
}

// MARK: - CAPSPageMenuDelegate

extension DetailParentViewcontroller: CAPSPageMenuDelegate{
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        customSegmetnView.setIndex(index: index)
    }
}

