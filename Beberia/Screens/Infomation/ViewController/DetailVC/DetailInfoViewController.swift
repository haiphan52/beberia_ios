//
//  DetailInfoViewController.swift
//  Beberia
//
//  Created by iMAC on 8/16/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
class DetailInfoViewController: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var tbvDetail: UITableView!
    
    // MARK: - Properties
    
    var getcategory_id = 0
    var gettype = 0
    var getlocation = ""
    var listCategories = [Category]()
    var titleView = ""
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoaddingManager.initLoadding(view: self.view)
        setupUI()
        getCategory(category_id: getcategory_id, type: gettype, location: getlocation )
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tbvDetail.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 104, right: 0)
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        if gettype == 2 {
            
        }else {
            self.title = titleView
        }
        tbvDetail.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 64, right: 0)
        tbvDetail.registerHeaderFooterView(R.nib.headerSectionTableViewCell)
        tbvDetail.register(R.nib.detailInfoTableViewCell)
        tbvDetail.delegate = self
        tbvDetail.dataSource = self
        tbvDetail.separatorStyle = .none
        tbvDetail.rowHeight = 114
        customLeftBarButton()
    }
    
    // MARK: - Request API
    
    func getCategory(category_id: Int,type:Int,location:String ){
        LoaddingManager.startLoadding()
        APIManager.getCategory(category_id: category_id, type: type, location: location, callbackSuccess: { [weak self] (categories) in
            guard let self = self else { return }
            self.listCategories = categories
            LoaddingManager.stopLoadding()
            self.tbvDetail.reloadData()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            LoaddingManager.stopLoadding()
        }
    }
    
    //MARK: - Helper methods
    
    @objc func loadmore(sender: UIButton){
        let touchPoint = sender.convert(CGPoint.zero, to: self.tbvDetail)
        var clickedButtonIndexPath = tbvDetail.indexPathForRow(at: touchPoint)
        let postVC = DetailMoreController.init(nib: R.nib.detailInfoMoreController)
        postVC.hidesBottomBarWhenPushed = true
        postVC.title = listCategories[clickedButtonIndexPath?.section ?? 0].categoryName ?? ""
        postVC.getcategory_idmore = listCategories[clickedButtonIndexPath?.section ?? 0].categoryId ?? 0
        Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
    }
}

// MARK: - UITableViewDelegate

extension DetailInfoViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCategories[section].posts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.detailInfoTableViewCell, for: indexPath)!
        let totalRows = tableView.numberOfRows(inSection: indexPath.section)
        cell.setupData(listCategories: self.listCategories[indexPath.section], row: indexPath.row,totalRows: totalRows )
        cell.leaddingIconLike.constant = 0
        cell.lblDate.text = ""
        cell.btnLoadmore.addTarget(self, action: #selector(loadmore(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let totalRows = tableView.numberOfRows(inSection: indexPath.section)
        if listCategories[indexPath.section].total_post ?? 0 > 3 {
            if indexPath.row == totalRows - 1 {
                return DetailInfoTableViewCell.heightRowWithButton
            }
        }
        return DetailInfoTableViewCell.heightRow
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
          return listCategories.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if listCategories[section].posts?.count ?? 0 > 0 {
            return 40
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: R.reuseIdentifier.headerSectionTableviewCell)!
        headerView.lblTitle.text = listCategories[section].categoryName
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        let postVC = PostDetailViewController.init(nibName: "PostDetailViewController", bundle: nil)
//        postVC.updateObject = { postDetail in
//            self.listCategories[indexPath.section].posts?[indexPath.row] = postDetail
//            self.tbvDetail.reloadData()
//        }
//        postVC.hidesBottomBarWhenPushed = true
//        postVC.idPost = listCategories[indexPath.section].posts?[indexPath.row].id ?? 0
//        Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
        
        let idPost = listCategories[indexPath.section].posts?[indexPath.row].id ?? 0
        self.pushPostDetail(array: listCategories[indexPath.section].posts!, idPost: idPost, indexPath.row) { (posts) in
            self.listCategories[indexPath.section].posts = posts
            self.tbvDetail.reloadData()
        }
    }

}

