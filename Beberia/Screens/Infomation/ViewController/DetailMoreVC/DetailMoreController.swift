//
//  DetailMoreController.swift
//  Beberia
//
//  Created by iMAC on 8/22/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import PullToRefresh
class DetailMoreController: BaseViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var tbvDetailMore: UITableView!
    
    // MARK: - Properties
    
    var listDetailMore = [PostDetailModel]()
    var getcategory_idmore = 0
    var page = 1
    var nextpage = 0
    let refresher = PullToRefresh()
    var cellHeightsDictionary: [IndexPath: CGFloat] = [:]
    
    // MARK: - View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoaddingManager.initLoadding(view: Utils.getTopMostViewController()!.view)
        setupUI()
        getDetailMore(category_id: getcategory_idmore, page: page)
    }
    
    // MARK: - SetupUI
    
    func setupUI(){
        tbvDetailMore.registerHeaderFooterView(R.nib.headerSectionTableViewCell)
        
        tbvDetailMore.register(R.nib.detailInfoTableViewCell)
        tbvDetailMore.delegate = self
        tbvDetailMore.dataSource = self
        tbvDetailMore.separatorStyle = .none
        customLeftBarButton()
        tbvDetailMore.infiniteScrollTriggerOffset = 700
        tbvDetailMore.addInfiniteScroll { (tableView) in
            if self.nextpage > 0 {
                self.page = self.page + 1
                self.getDetailMore(category_id: self.getcategory_idmore, page: self.page)
            }
        }
        
        tbvDetailMore.showsVerticalScrollIndicator = false
        tbvDetailMore.addInfiniteScroll { (tableView) in
            tableView.performBatchUpdates({ () -> Void in
                // update collection view
                if self.nextpage > 0 {
                    self.page = self.page + 1
                    self.getDetailMore(category_id: self.getcategory_idmore, page: self.page)
                }else{
                    self.tbvDetailMore.finishInfiniteScroll(completion: { (collection) in
                    })
                }
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                tableView.finishInfiniteScroll()
            });
        }
        
        tbvDetailMore.addPullToRefresh(refresher) {
            self.page = 1
            self.getDetailMore(category_id: self.getcategory_idmore, page: self.page)
            self.tbvDetailMore.startRefreshing(at: .top)
        }
    }
    
    // MARK: - Request API
    
    func getDetailMore(category_id: Int,page:Int){
        LoaddingManager.startLoadding()
        APIManager.getDetailLoadMore(category_id: category_id, page: page, callbackSuccess: {[weak self]  (detailMore,next_page) in
            guard let self = self else { return }
            self.nextpage = next_page
            if self.page == 1{
                self.listDetailMore.removeAll()
            }
            if self.listDetailMore.count == 0 {
                self.listDetailMore = detailMore
            }else{
                self.listDetailMore.append(contentsOf: detailMore)
            }
            self.tbvDetailMore.reloadData()
            self.tbvDetailMore.endRefreshing(at: .top)
            self.tbvDetailMore.finishInfiniteScroll(completion: { (collection) in
            })
            LoaddingManager.stopLoadding()
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            LoaddingManager.stopLoadding()
        }
    }
}

// MARK: - UITableViewDelegate

extension DetailMoreController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDetailMore.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.detailInfoTableViewCell, for: indexPath)!
        cell.setupDataMore(listDetailMore: self.listDetailMore[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DetailInfoTableViewCell.heightRow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let idPost = self.listDetailMore[indexPath.row].id ?? 0
        self.pushPostDetail(array: listDetailMore, idPost: idPost, indexPath.row) { (posts) in
            self.listDetailMore = posts
            self.tbvDetailMore.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
          self.cellHeightsDictionary[indexPath] = cell.frame.size.height
      }
      
      func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
          if let height =  self.cellHeightsDictionary[indexPath] {
              return height
          }
          return UITableView.automaticDimension
      }
}
