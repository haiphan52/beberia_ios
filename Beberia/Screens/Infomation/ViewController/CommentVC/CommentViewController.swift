//
//  CommentViewController.swift
//  Beberia
//
//  Created by IMAC on 8/15/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

class CommentViewController: BaseViewController{
    
    //MARK: - Outlets
    @IBOutlet weak var btnViewPost: UIButton!
    @IBOutlet weak var heightViewTotalComment: NSLayoutConstraint!
    @IBOutlet weak var viewTotalComment: UIView!
    @IBOutlet weak var lblNameUserReply: UILabel!
    @IBOutlet weak var viewNotiReplyComment: UIView!
    @IBOutlet weak var btnClose: UIImageView!
    @IBOutlet weak var constrainHeightViewHeaderComment: NSLayoutConstraint!
    @IBOutlet weak var btnSeeMoreComment: UIButton!
    @IBOutlet weak var tbvComment: UITableView!
    @IBOutlet weak var lblNumberComment: UILabel!
    @IBOutlet weak var heightTbv: NSLayoutConstraint!
    @IBOutlet weak var tvInputComment: UITextView!
    @IBOutlet weak var heightConstraintViewInput: NSLayoutConstraint!
    @IBOutlet weak var heightViewInput: NSLayoutConstraint!
    @IBOutlet weak var btnSend: UIButton!
    
    //MARK: - Properties
    var feed = HomeFeed.init(json: "")
    var commentNotification: Comment?
    var isCheckFromNotification = false
    var comments = [Comment]()
    var numberComment : Int = 0
    var pageReply = 1
    var parentId:Int? = nil
    var indexParent:Int = 0
    var nextPage: Int = 1
    var nextPageReply: Int = 1
    fileprivate var heightDictionary: [IndexPath : CGFloat] = [:]
    var isCheckEdit = false
    var indexEdit: Int = 0
    var isCheckCommentOrReply = false
    var idPost = 0
    var type: Int?
    var userIdTag:[Int]? = nil
    var fromToView = FromToview.Home
    {
        didSet{
            switch fromToView {
            case .Home:
                type = 4
            default:
                type = nil
            }
        }
    }
    let cell : CommentTableViewCell? = nil
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getComment(page: nextPage)
    }
    
    //MARK: - SetupUI
    func setupUI(){
        tbvComment.register(R.nib.commentTableViewCell)
        tbvComment.delegate = self
        tbvComment.dataSource = self
        tbvComment.separatorStyle = .none
        tbvComment.allowsSelection = false
        tbvComment.rowHeight = UITableView.automaticDimension
        lblNumberComment.text = "\(numberComment) \(R.string.localizable.infomationComment())"
        if numberComment > 15 {
            constrainHeightViewHeaderComment.constant = 46
            btnSeeMoreComment.isHidden = false
        }else{
            constrainHeightViewHeaderComment.constant = 0
            btnSeeMoreComment.isHidden = true
        }
        
        tvInputComment.cornerRadius = 15
        tvInputComment.delegate = self
        tvInputComment.text =  R.string.localizable.infomationWriteComment()
        tvInputComment.textColor = UIColor.lightGray
        btnSend.isEnabled = true
        if  tvInputComment.textColor == UIColor.lightGray{
            btnSend.isEnabled = false
        }
        tvInputComment.autocorrectionType = .no
        viewNotiReplyComment.isHidden = true
        
        if isCheckFromNotification{
            //  heightViewTotalComment.constant = 0
            btnClose.isHidden = true
            lblNumberComment.isHidden = true
            btnViewPost.isHidden = false
            customLeftBarButton()
            self.title = "Bình luận"
        }else{
            btnClose.isHidden = false
            lblNumberComment.isHidden = false
            btnViewPost.isHidden = true
        }
    }
    
    func initViewInput(){
        self.tvInputComment.text =  R.string.localizable.infomationWriteComment()
        self.tvInputComment.textColor = UIColor.lightGray
        self.heightViewInput.constant = 33
        self.heightConstraintViewInput.constant = 55
        self.view.endEditing(true)
    }
    
    //MARK: - Action
    @IBAction func didPressCloseViewNotiReply(_ sender: Any) {
        lblNameUserReply.text = ""
        viewNotiReplyComment.isHidden = true
        self.parentId = nil
        self.indexParent = 0
        userIdTag = []
    }
    
    @IBAction func didPressSendComment(_ sender: Any) {
        if self.fromToView == FromToview.Home {
            sendCommnetHomeFeed(cell: cell)
        }else{
            sendComment()
        }
    }
    
    @IBAction func didPressLoadMoreComment(_ sender: Any) {
        getComment(page: nextPage)
    }
    
    @IBAction func didPressClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Request API
    func sendComment(){
        
        guard let content = tvInputComment.text, content.count > 0 else {
            return
        }
        SVProgressHUD.show()
        APIManager.sendComment(idPost, parentId: parentId, content: content, userTag: userIdTag?[0] ?? 0, callbackSuccess: { [weak self] (comment) in
            guard let self = self else { return }
            if self.parentId != nil{
                self.comments[self.indexParent].reply!.append(comment)
                self.comments[self.indexParent].replyCommentNumber! += 1
                self.comments[self.indexParent].isTapExpand = true
            }else{
                self.comments.append(comment)
                self.numberComment += 1
                self.lblNumberComment.text = "\(self.numberComment) \(R.string.localizable.infomationComment())"
                self.tbvComment.scroll(to: .bottom, animated: true)
            }
            NotificationCenter.default.post(name: NSNotification.Name.updateNumberComment, object: [Key.KeyNotification.numberComment : self.numberComment], userInfo: nil)
            self.tbvComment.reloadData()
            self.initViewInput()
            self.parentId = nil
            self.indexParent = 0
            self.lblNameUserReply.text = ""
            self.viewNotiReplyComment.isHidden = true
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func sendCommnetHomeFeed(cell: CommentTableViewCell?){
        guard let content = tvInputComment.text, content.count > 0 else {
            return
        }
        SVProgressHUD.show()
        
        let type = (parentId == nil) ? Key.TypeComment : Key.TypeReplyComment
        
        APIManager.sendCommentHomeFeed(parentId ?? idPost, type: type, content: content, userTag: userIdTag ?? [], callbackSuccess: { [weak self] (comment) in
            guard let self = self else { return }
            if self.parentId != nil{
                self.comments[self.indexParent].reply!.append(comment)
                self.comments[self.indexParent].replyCommentNumber! += 1
                self.comments[self.indexParent].isTapExpand = true
            }else{
                self.comments.append(comment)
                self.numberComment += 1
                self.lblNumberComment.text = "\(self.numberComment) \(R.string.localizable.infomationComment())"
                self.tbvComment.scroll(to: .bottom, animated: true)
            }
            
            NotificationCenter.default.post(name: NSNotification.Name.updateNumberComment, object: [Key.KeyNotification.numberComment : self.numberComment], userInfo: nil)
            
            self.tbvComment.reloadData()
            self.initViewInput()
            self.parentId = nil
            self.indexParent = 0
            self.lblNameUserReply.text = ""
            self.viewNotiReplyComment.isHidden = true
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func getComment(page: Int){
        SVProgressHUD.show()
        APIManager.getComment(idPost, page: page, type: type, callbackSuccess: { [weak self] (comments, nextPage) in
            guard let self = self else { return }
            var comments = comments
            
            comments.reverse()
            if comments.count > 0{
                for comment in comments{
                    comment.reply?.reverse()
                }
            }
            self.comments.insert(contentsOf: comments, at: 0)
            self.tbvComment.reloadData()
            
            if !self.isCheckFromNotification{
                // check scroll tableview
                if (self.nextPage == 1 && self.comments.count <= 15) || self.nextPage == 0{
                    self.tbvComment.scroll(to: .bottom, animated: true)
                }else{
                    //                    let pathToLastRow = IndexPath.init(row: (self.tbvComment.indexPathsForVisibleRows?.last?.row)! + comments.count, section: 0)
                    //                    self.tbvComment.scrollToRow(at: pathToLastRow, at: .bottom, animated: true)
                    //                    self.tbvComment.reloadData()
                }
            }else{
                if self.nextPage == 1{
                    let index = self.comments.firstIndex(where: { (comment) -> Bool in
                        return comment.commentId == self.commentNotification?.commentId
                    })
                    
                    if index != nil{
                        let indexCommentNotification = IndexPath.init(row: index!, section: 0 )
                        self.tbvComment.scrollToRow(at: indexCommentNotification, at: .middle, animated: true)
                    }else{
                        self.comments.append(self.commentNotification!)
                        self.tbvComment.reloadData()
                        self.tbvComment.scroll(to: .bottom, animated: true)
                    }
                }else{
                    let pathToLastRow = IndexPath.init(row: (self.tbvComment.indexPathsForVisibleRows?.last?.row)! + comments.count, section: 0)
                    self.tbvComment.scrollToRow(at: pathToLastRow, at: .bottom, animated: false)
                }
                
            }
            
            self.nextPage = nextPage
            
            if nextPage == 0{
                self.constrainHeightViewHeaderComment.constant = 0
                self.btnSeeMoreComment.isHidden = true
            }else{
                self.constrainHeightViewHeaderComment.constant = 46
                self.btnSeeMoreComment.isHidden = false
            }
            
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func getCommentReplyHomeFeed(index: Int,page: Int, type: Int, completion:@escaping ()->()){
        SVProgressHUD.show()
        APIManager.getComment(self.comments[index].commentId ?? 0, page: page, type: type, callbackSuccess: { [weak self] (comments, nextPage) in
            guard let self = self else { return }
            var comments = comments
            
            self.nextPageReply = nextPage
            
            if self.comments[index].reply?.count ?? [Comment]().count < 4 {
                self.comments[index].reply?.removeAll()
            }
            
            comments.reverse()
            self.comments[index].reply?.insert(contentsOf: comments, at: 0)
            self.comments[index].nextPage = nextPage
            //   self.tbvComment.reloadData()
            completion()
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func getCommentReply(index: Int, page: Int, completion:@escaping ()->()){
        SVProgressHUD.show()
        APIManager.getCommentReply(self.comments[index].commentId ?? 0, page: page, callbackSuccess: { [weak self] (comments, nextPage) in
            guard let self = self else { return }
            var comments = comments
            self.nextPageReply = nextPage
            
            if self.comments[index].reply?.count ?? [Comment]().count < 4 {
                self.comments[index].reply?.removeAll()
            }
            
            comments.reverse()
            self.comments[index].reply?.insert(contentsOf: comments, at: 0)
            self.comments[index].nextPage = nextPage
            completion()
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func deleteComment(index: Int, isCheckCommentOrReply: Bool, type: Int?, indexParentComment: Int?, cell : CommentTableViewCell){
        SVProgressHUD.show()
        let idDelete = isCheckCommentOrReply ? self.comments[index].commentId ?? 0 : self.comments[indexParentComment!].reply![index].commentId ?? 0
        APIManager.deleteComment(idDelete,type: type, callbackSuccess: { [weak self] (isSuccess) in
            guard let self = self else { return }
            if isSuccess {
                if !isCheckCommentOrReply{
                    var numberRep = self.comments[indexParentComment!].replyCommentNumber ?? 0
                    numberRep -= 1
                    self.comments[indexParentComment!].reply?.remove(at: index)
                    self.comments[indexParentComment!].replyCommentNumber = numberRep
                    
                }else{
                    self.comments.remove(at: index)
                    self.numberComment -= 1
                    self.lblNumberComment.text = "\(self.numberComment) \(R.string.localizable.infomationComment())"
                }
                self.tbvComment.reloadData()
            }
            
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func editComment(index: Int, isCheckCommentOrReply: Bool, content: String, indexParentComment: Int? , type: Int?, cell : CommentTableViewCell, completion:@escaping ()->()){
        SVProgressHUD.show()
        let idEdit = isCheckCommentOrReply ? self.comments[index].commentId ?? 0 : self.comments[indexParentComment!].reply![index].commentId ?? 0
        APIManager.editComment(idEdit ,content, type: type ,callbackSuccess: { [weak self] (comment) in
            guard let self = self else { return }
            // add lại data mới khi edit thành công
            if !isCheckCommentOrReply{
                self.comments[indexParentComment!].reply![index].content = comment.content
            }else{
                self.comments[index].content = comment.content
            }
            // update UI
            self.isCheckEdit = false
            
            if type == Key.TypeReplyComment || !isCheckCommentOrReply {
                guard let cell1 = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                    return
                }
                cell1.tbvComment.reloadData()
                cell1.layoutIfNeeded()
                cell1.tbvComment.beginUpdates()
                cell1.tbvComment.endUpdates()
                completion()
                //self.tbvComment.reloadData()
            }else{
                self.tbvComment.reloadData()
            }
            
            self.initViewInput()
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
        }
    }
    
    func likeCommentInfomation(_ index :Int,_ type: Int, _ cell: CommentTableViewCell, idReply: Int, indexParent: Int){
         cell.btnLike.isEnabled = false
         let id = (idReply == Key.IndexException) ? self.comments[index].commentId ?? 0 : idReply
         APIManager.likePost(idPost: id, type: type, callbackSuccess: { [weak self] (numberLike, checkLike) in
            guard let self = self else { return }
             if idReply != Key.IndexException{
                 guard let cell = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                     return
                 }
                 cell.isLike = checkLike
                 cell.numberLike = numberLike
                 cell.btnLike.isEnabled = true
                 cell.lblNumberLike.text = "\(cell.numberLike)"
                 self.comments[indexParent].reply?[index].isLiked = checkLike
                 self.comments[indexParent].reply?[index].likeNumber = numberLike
             }else{
                 cell.isLike = checkLike
                 cell.numberLike = numberLike
                 cell.btnLike.isEnabled = true
                 cell.lblNumberLike.text = "\(cell.numberLike)"
                 self.comments[index].isLiked = checkLike
                 self.comments[index].likeNumber = numberLike
             }
         }) { (error) in
             Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
         }
     }
     
     func likeCommentHomeFeed(_ index :Int,_ cell: CommentTableViewCell, type :Int, idReply: Int, indexParent: Int){
         cell.btnLike.isEnabled = false
         let id = (idReply == Key.IndexException) ? self.comments[index].commentId ?? 0 : idReply
         let checkLike = (idReply == Key.IndexException) ? self.comments[index].isLiked ?? 0 : self.comments[indexParent].reply?[index].isLiked
         APIManager.likeHomeFeed(id: id, type: type, checkLike: checkLike ?? 0, callbackSuccess: { [weak self] (numberLike, checkLike) in
            guard let self = self else { return }
             if type == Key.TypeLikeReplyComment{
                 guard let cell = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                     return
                 }
                 cell.isLike = checkLike
                 cell.numberLike = numberLike
                 cell.btnLike.isEnabled = true
                 cell.lblNumberLike.text = "\(cell.numberLike)"
                 self.comments[indexParent].reply?[index].isLiked = checkLike
                 self.comments[indexParent].reply?[index].likeNumber = numberLike
             }else{
                 cell.isLike = checkLike
                 cell.numberLike = numberLike
                 cell.btnLike.isEnabled = true
                 cell.lblNumberLike.text = "\(cell.numberLike)"
                 self.comments[index].isLiked = checkLike
                 self.comments[index].likeNumber = numberLike
             }
             
         }) { (error) in
             Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
         }
     }
    
    func pushProfileVC(user: User){
        
        if isCheckFromNotification{
            let ProfileVC = R.storyboard.main.profileViewController()
            ProfileVC?.hidesBottomBarWhenPushed = true
            ProfileVC?.check = 1
            ProfileVC?.user = user
            Utils.getTopMostViewController()?.navigationController?.pushViewController(ProfileVC!, animated: true)
        }else{
            self.dismiss(animated: true) {
                let ProfileVC = R.storyboard.main.profileViewController()
                ProfileVC?.hidesBottomBarWhenPushed = true
                ProfileVC?.check = 1
                ProfileVC?.user = user
                Utils.getTopMostViewController()?.navigationController?.pushViewController(ProfileVC!, animated: true)
            }
        }
    }
    //MARK: - Handle Update layout
    func handelUpdateLayoutEditComment(cell: CommentTableViewCell){
        cell.viewNumberLike.isHidden = self.isCheckEdit ? true : false
        cell.viewNumberReply.isHidden = self.isCheckEdit ? true : false
        cell.lblContent.isHidden = self.isCheckEdit ? true : false
        cell.btnSavedEdit.isHidden = self.isCheckEdit ? false : true
        cell.btnCancelEdit.isHidden = self.isCheckEdit ? false : true
        cell.txtContent.isHidden = self.isCheckEdit ? false : true
        cell.heightTextViewContent.isActive = self.isCheckEdit ? true : false
        cell.heightTextViewContent.constant = self.isCheckEdit ? max(cell.lblContent.frame.height, 66) : 0
        cell.layoutIfNeeded()
        self.tbvComment.beginUpdates()
        self.tbvComment.endUpdates()
    }
    
    //MARK: - Show Action Delete
    func showAtionDeleteEdit(index: Int,isCheckCommentOrReply: Bool, cell: CommentTableViewCell, type :Int?, indexParentComment: Int?){
        let bottomVC = BaseBottomViewController.init(nib: R.nib.baseBottomViewController)
        bottomVC.selectedOption = { indexSelect in
            
            switch indexSelect {
            case 0:
                self.isCheckEdit = true
                self.indexEdit = index
                self.indexParent = indexParentComment ?? 0
                
                if type == Key.TypeReplyComment || !isCheckCommentOrReply{
                    
                    guard let cell1 = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                        return
                    }
                    self.handelUpdateLayoutEditComment(cell: cell1)
                    cell.layoutIfNeeded()
                    cell.tbvComment.beginUpdates()
                    cell.tbvComment.endUpdates()
                    self.tbvComment.beginUpdates()
                    self.tbvComment.endUpdates()
                }else {
                    self.handelUpdateLayoutEditComment(cell: cell)
                }
                
            case 1:
                self.deleteComment(index: index, isCheckCommentOrReply: isCheckCommentOrReply, type: type, indexParentComment: indexParentComment, cell: cell)
            default:
                print("123")
            }
            self.dismiss(animated: true, completion: {
                self.parentId = nil
                self.indexParent = 0
                switch self.fromToView {
                case .Home:
                    self.type = 4
                default:
                    self.type = nil
                }
            })
        }
        bottomVC.datasource = [R.string.localizable.infomationEditComment(), R.string.localizable.homeDelete()]
        bottomVC.height = CGFloat((bottomVC.datasource.count + 1) * 44)
        bottomVC.topCornerRadius = 20
        bottomVC.presentDuration = 0.25
        bottomVC.dismissDuration = 0.25
        self.present(bottomVC, animated: true, completion: nil)
    }
    
 
    
    func showViewNotiReply(){
        UIView.animate(withDuration: 0.25) {
            self.viewNotiReplyComment.center.y -= self.viewNotiReplyComment.bounds.height
        }
    }
    
    //MARK: - Find Index Reply Comment
    func findIndexReplyComment(_ id :Int)-> Int{
        if id != Key.IndexException {
            for (index ,comment) in self.comments.enumerated(){
                if comment.reply?.count ?? [Comment]().count > 0 {
                    for replyComment in comment.reply!{
                        if replyComment.commentId! == id{
                            return index
                        }
                    }
                }
            }
        }
        return Key.IndexException
    }
    
    @IBAction func didPressViewPost(_ sender: Any) {
        guard let selfStrong = Utils.getTopMostViewController() else {
            return
        }
        if self.fromToView == FromToview.Home {
            Router.share.pushToViewHomeDetail(tableView: UITableView.init(), array: [],indexPath: IndexPath.init(), viewSelf: selfStrong, idFeed: feed.id ?? 0, editFromView: .Home, completion: { homeFeeds in
            })
        }else{
            Router.share.pushToViewDiaryDetail(tableView: UITableView.init(), array: [Diaries](), indexPath: IndexPath.init(),row: IndexPath.init().row, viewSelf: selfStrong, idFeed: feed.id ?? 0, editFromView: .Home) { (diary) in
            }
        }
    }
    
    //MARK: - Reload Table View
    func reloadTableView(cell: CommentTableViewCell, index: Int){
        self.tbvComment.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        cell.layoutIfNeeded()
        self.tbvComment.layoutIfNeeded()
        self.tbvComment.beginUpdates()
        self.tbvComment.endUpdates()
    }
}
//MARK: - UITableViewDelegate
extension CommentViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.commentTableViewCell, for: indexPath)!
        cell.viewNumberLike.isHidden = false
        cell.viewNumberReply.isHidden = false
        cell.lblContent.isHidden = false
        cell.viewLine.isHidden = true
        cell.btnSavedEdit.isHidden = true
        cell.btnCancelEdit.isHidden = true
        cell.txtContent.isHidden = true
        cell.heightTextViewContent.isActive = false
        cell.contentView.backgroundColor = .white
        cell.viewBGReplyComment.backgroundColor  = UIColor.init(hexString: AppColor.colorReplyCommentBG, alpha: 0.14)
        cell.btnShowReplyComment.tag = indexPath.row
        cell.setupData(comment: self.comments[indexPath.row], nextPage: self.comments[indexPath.row].nextPage ?? 100, isReply: false)
        cell.comments = self.comments
        cell.btnLike.tag = indexPath.row
        
        cell.tapOption = { [weak self] index, idReply, type  in
            guard let self = self else { return }
            if self.fromToView == FromToview.Home {
                if type == Key.TypeComment{
                    self.showAtionDeleteEdit(index: index,isCheckCommentOrReply: true, cell: cell, type: Key.TypeComment, indexParentComment: nil)
                } else {
                    self.showAtionDeleteEdit(index: index,isCheckCommentOrReply: false, cell: cell, type: Key.TypeReplyComment, indexParentComment: self.findIndexReplyComment(idReply ?? Key.IndexException))
                }
            }else{
                // dựa vào có indexReplyComment ko để check nó là comment hay replr comment
                let indexReplyCM = self.findIndexReplyComment(idReply ?? Key.IndexException)
                self.showAtionDeleteEdit(index: index,isCheckCommentOrReply: indexReplyCM == Key.IndexException ? true : false, cell: cell, type: nil, indexParentComment: indexReplyCM)
            }
        }
        
        cell.tapShowReply = { [weak self] index in
            guard let self = self else { return }
            self.comments[index].isTapExpand = true
            self.reloadTableView(cell: cell, index: index)
        }
        
        cell.tapReply = { [weak self] index in
            guard let self = self else { return }
            self.didPressCloseViewNotiReply(UIButton.self)
            self.parentId = self.comments[index].commentId
            self.indexParent = index
            self.lblNameUserReply.text = "@\(self.comments[index].user?.displayName ?? "")"
            self.userIdTag = [self.comments[index].user?.id ?? 0]
            self.viewNotiReplyComment.isHidden = false
            self.showViewNotiReply()
        }
        
        cell.tapReplyComment = { [weak self] userID, nameUser, idComment, numberSecret  in
            guard let self = self else { return }
            self.didPressCloseViewNotiReply(UIButton.self)
            
            var commentTemp: Comment? = nil
            for (index ,comment) in self.comments.enumerated(){
                if comment.reply?.count ?? [Comment]().count > 0 {
                    for replyComment in comment.reply!{
                        if replyComment.commentId! == idComment{
                            commentTemp = comment
                            self.indexParent = index
                        }
                    }
                }
            }
            if commentTemp != nil{
                self.parentId = commentTemp!.commentId
            }
            self.userIdTag = [userID]
            self.lblNameUserReply.text = "@\(nameUser)"
            self.viewNotiReplyComment.isHidden = false
            self.showViewNotiReply()
        }
        
        cell.tapLike = { [weak self] index, idReply, type in
            guard let self = self else { return }
            if self.fromToView == FromToview.Home {
                
                self.likeCommentHomeFeed(index, cell, type: type, idReply: idReply ?? Key.IndexException, indexParent: self.findIndexReplyComment(idReply ?? Key.IndexException))
            }else{
                self.likeCommentInfomation(index, type, cell, idReply: idReply ?? Key.IndexException, indexParent: self.findIndexReplyComment(idReply ?? Key.IndexException))
            }
        }
        
        cell.tapShowMoreReplyComment = { [weak self] index ,_ ,type in
            guard let self = self else { return }
            if self.fromToView == FromToview.Home {
                self.getCommentReplyHomeFeed(index: index, page: self.nextPageReply, type: type, completion: {
                    self.reloadTableView(cell: cell, index: index)
                })
            }else{
                self.getCommentReply(index: index, page: self.nextPageReply, completion: {
                    self.reloadTableView(cell: cell, index: index)
                })
            }
        }
        
        cell.tapCancelEdit = { [weak self] index, idReply, type in
            guard let self = self else { return }
            self.isCheckEdit = false
            if type == Key.TypeReplyComment{
                guard let cell1 = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                    return
                }
                self.handelUpdateLayoutEditComment(cell: cell1)
            }else{
                cell.txtContent.text = cell.lblContent.text
                self.handelUpdateLayoutEditComment(cell: cell)
            }
        }
        
        cell.tapSavedEdit = { [weak self] index, idReply, type in
            guard let self = self else { return }
            if self.fromToView == FromToview.Home {
                if type == Key.TypeComment{
                    self.editComment(index: index, isCheckCommentOrReply: true, content: cell.txtContent.text, indexParentComment: self.findIndexReplyComment(idReply ?? Key.IndexException), type: type, cell: cell, completion: {
                        
                    })
                } else {
                    guard let cell1 = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                        return
                    }
                    self.editComment(index: index, isCheckCommentOrReply: false, content: cell1.txtContent.text, indexParentComment: self.findIndexReplyComment(idReply ?? Key.IndexException), type: type, cell: cell, completion: {
                        self.reloadTableView(cell: cell, index: self.findIndexReplyComment(idReply ?? Key.IndexException))
                    })
                }
            }else{
                
                let indexReplyCM = self.findIndexReplyComment(idReply ?? Key.IndexException)
                if indexReplyCM == Key.IndexException {
                    self.editComment(index: index, isCheckCommentOrReply: true, content: cell.txtContent.text, indexParentComment: indexReplyCM, type: nil, cell: cell, completion: {
                        
                    })
                }else{
                    guard let cell1 = cell.tbvComment.cellForRow(at: IndexPath.init(row: index, section: 0)) as? CommentTableViewCell else {
                        return
                    }
                    self.editComment(index: index, isCheckCommentOrReply: false, content: cell1.txtContent.text, indexParentComment: indexReplyCM, type: nil, cell: cell, completion: {
                        self.reloadTableView(cell: cell, index: self.findIndexReplyComment(idReply ?? Key.IndexException))
                    })
                }
            }
        }
        
        cell.tapUser = { [weak self] index, user, type in
            guard let self = self else { return }
            var user = user
            if type == Key.TypeComment{
                user = self.comments[index].user
            }
            print(user?.id as Any)
            self.pushProfileVC(user: user!)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return height ?? UITableView.automaticDimension
    }
}

//MARK: - UITextViewDelegate
extension CommentViewController: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
        heightConstraintViewInput.constant = newFrame.height + 22
        heightViewInput.constant = newFrame.height
        btnSend.isEnabled = true
        if (tvInputComment.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! || tvInputComment.text.count < 1{
            btnSend.isEnabled = false
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            tvInputComment.text =  R.string.localizable.infomationWriteComment()
            textView.textColor = UIColor.lightGray
        }
    }
    
}

