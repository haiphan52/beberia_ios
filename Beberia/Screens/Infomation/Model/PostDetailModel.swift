//
//  PostDetailModel.swift
//
//  Created by IMAC on 8/14/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class PostDetailModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let content = "content"
    static let updated = "updated"
    static let slug = "slug"
    static let id = "id"
    static let turnOn = "turnOn"
    static let createdUser = "user_create"
    static let created = "created"
    static let updatedUser = "user_update"
    static let title = "title"
    static let author = "author"
    static let category = "category"
    static let likeNumber = "like_number"
    static let commentNumber = "comment_number"
    static let categoryId = "category_id"
    static let isFavourite = "check_save"
    static let isLiked = "is_liked"
     static let urlImage = "url_image"
    static let quote = "quote"
  }
    

  // MARK: Properties
  public var content: String?
  public var updated: Int?
  public var slug: String?
  public var id: Int?
  public var turnOn: Int?
  public var createdUser: UserCreate?
  public var created: Int?
  public var updatedUser: UserCreate?
  public var title: String?
  public var author: String?
  public var category: String?
    public var likeNumber: Int?
    public var commentNumber: Int?
    public var categoryId: Int?
    public var isFavourite: Int?
    public var isLiked: Int?
    public var urlImage: String?
    public var quote: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.

  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    content = json[SerializationKeys.content].string
    urlImage = json[SerializationKeys.urlImage].string
    updated = json[SerializationKeys.updated].int
    slug = json[SerializationKeys.slug].string
    id = json[SerializationKeys.id].int
    turnOn = json[SerializationKeys.turnOn].int
    createdUser = UserCreate(json: json[SerializationKeys.createdUser])
    created = json[SerializationKeys.created].int
    updatedUser = UserCreate(json: json[SerializationKeys.updatedUser])
    title = json[SerializationKeys.title].string
    author = json[SerializationKeys.author].string
    category = json[SerializationKeys.category].string
    likeNumber = json[SerializationKeys.likeNumber].int
    commentNumber = json[SerializationKeys.commentNumber].int
    categoryId = json[SerializationKeys.categoryId].int
    isFavourite = json[SerializationKeys.isFavourite].int
    isLiked = json[SerializationKeys.isLiked].int
    quote = json[SerializationKeys.quote].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = content { dictionary[SerializationKeys.content] = value }
    if let value = urlImage { dictionary[SerializationKeys.urlImage] = value }
    if let value = updated { dictionary[SerializationKeys.updated] = value }
    if let value = slug { dictionary[SerializationKeys.slug] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = turnOn { dictionary[SerializationKeys.turnOn] = value }
    if let value = createdUser { dictionary[SerializationKeys.createdUser] = value.dictionaryRepresentation() }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = updatedUser { dictionary[SerializationKeys.updatedUser] = value.dictionaryRepresentation() }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = author { dictionary[SerializationKeys.author] = value }
    if let value = category { dictionary[SerializationKeys.category] = value }
    if let value = likeNumber { dictionary[SerializationKeys.likeNumber] = value }
    if let value = commentNumber { dictionary[SerializationKeys.commentNumber] = value }
    if let value = categoryId { dictionary[SerializationKeys.categoryId] = value }
    if let value = isFavourite { dictionary[SerializationKeys.isFavourite] = value }
    if let value = isLiked { dictionary[SerializationKeys.isLiked] = value }
    if let value = quote { dictionary[SerializationKeys.quote] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.content = aDecoder.decodeObject(forKey: SerializationKeys.content) as? String
    self.urlImage = aDecoder.decodeObject(forKey: SerializationKeys.urlImage) as? String
    self.updated = aDecoder.decodeObject(forKey: SerializationKeys.updated) as? Int
    self.slug = aDecoder.decodeObject(forKey: SerializationKeys.slug) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.turnOn = aDecoder.decodeObject(forKey: SerializationKeys.turnOn) as? Int
    self.createdUser = aDecoder.decodeObject(forKey: SerializationKeys.createdUser) as? UserCreate
    self.created = aDecoder.decodeObject(forKey: SerializationKeys.created) as? Int
    self.updatedUser = aDecoder.decodeObject(forKey: SerializationKeys.updatedUser) as? UserCreate
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.author = aDecoder.decodeObject(forKey: SerializationKeys.author) as? String
    self.category = aDecoder.decodeObject(forKey: SerializationKeys.category) as? String
    self.likeNumber = aDecoder.decodeObject(forKey: SerializationKeys.likeNumber) as? Int
    self.commentNumber = aDecoder.decodeObject(forKey: SerializationKeys.commentNumber) as? Int
    self.categoryId = aDecoder.decodeObject(forKey: SerializationKeys.categoryId) as? Int
    self.isFavourite = aDecoder.decodeObject(forKey: SerializationKeys.isFavourite) as? Int
    self.isLiked = aDecoder.decodeObject(forKey: SerializationKeys.isLiked) as? Int
    self.quote = aDecoder.decodeObject(forKey: SerializationKeys.quote) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(content, forKey: SerializationKeys.content)
    aCoder.encode(urlImage, forKey: SerializationKeys.urlImage)
    aCoder.encode(updated, forKey: SerializationKeys.updated)
    aCoder.encode(slug, forKey: SerializationKeys.slug)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(turnOn, forKey: SerializationKeys.turnOn)
    aCoder.encode(createdUser, forKey: SerializationKeys.createdUser)
    aCoder.encode(created, forKey: SerializationKeys.created)
    aCoder.encode(updatedUser, forKey: SerializationKeys.updatedUser)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(author, forKey: SerializationKeys.author)
    aCoder.encode(category, forKey: SerializationKeys.category)
    aCoder.encode(likeNumber, forKey: SerializationKeys.likeNumber)
    aCoder.encode(commentNumber, forKey: SerializationKeys.commentNumber)
    aCoder.encode(categoryId, forKey: SerializationKeys.categoryId)
    aCoder.encode(isFavourite, forKey: SerializationKeys.isFavourite)
    aCoder.encode(isLiked, forKey: SerializationKeys.isLiked)
    aCoder.encode(quote, forKey: SerializationKeys.quote)
  }

}
