//
//  Category.swift
//
//  Created by IMAC on 8/13/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Category: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updated = "updated"
    static let parentId = "parent_id"
    static let slug = "slug"
    static let groupId = "group_id"
    static let created = "created"
    static let updatedUser = "updated_user"
    static let categoryId = "category_id"
    static let categoryName = "category_name"
    static let urlImage = "url_image"
    static let createdUser = "created_user"
    static let children = "children"
    static let posts = "posts"
    static let type = "type"
    static let total_post = "total_post"
  }

  // MARK: Properties
  public var updated: Int?
  public var type: Int?
  public var parentId: Int?
  public var total_post: Int?
  public var slug: String?
  public var groupId: Int?
  public var created: Int?
  public var updatedUser: UserCreate?
  public var categoryId: Int?
  public var categoryName: String?
  public var urlImage: String?
  public var createdUser: UserCreate?
  public var children: [Category]?
  public var posts: [PostDetailModel]?
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    type = json[SerializationKeys.type].int
    updated = json[SerializationKeys.updated].int
    parentId = json[SerializationKeys.parentId].int
    total_post = json[SerializationKeys.total_post].int
    slug = json[SerializationKeys.slug].string
    groupId = json[SerializationKeys.groupId].int
    created = json[SerializationKeys.created].int
    updatedUser = UserCreate(json: json[SerializationKeys.updatedUser])
    createdUser = UserCreate(json: json[SerializationKeys.createdUser])
    categoryId = json[SerializationKeys.categoryId].int
    categoryName = json[SerializationKeys.categoryName].string
    urlImage = json[SerializationKeys.urlImage].string
    if let items = json[SerializationKeys.children].array { children = items.map { Category(json: $0)} }
    if let items1 = json[SerializationKeys.posts].array { posts = items1.map { PostDetailModel(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = updated { dictionary[SerializationKeys.updated] = value }
    if let value = parentId { dictionary[SerializationKeys.parentId] = value }
    if let value = total_post { dictionary[SerializationKeys.total_post] = value }
    if let value = slug { dictionary[SerializationKeys.slug] = value }
    if let value = groupId { dictionary[SerializationKeys.groupId] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = updatedUser { dictionary[SerializationKeys.updatedUser] = value.dictionaryRepresentation() }
    if let value = categoryId { dictionary[SerializationKeys.categoryId] = value }
    if let value = categoryName { dictionary[SerializationKeys.categoryName] = value }
    if let value = children { dictionary[SerializationKeys.children] = value }
    if let value = posts { dictionary[SerializationKeys.posts] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? Int
    self.updated = aDecoder.decodeObject(forKey: SerializationKeys.updated) as? Int
    self.parentId = aDecoder.decodeObject(forKey: SerializationKeys.parentId) as? Int
    self.total_post = aDecoder.decodeObject(forKey: SerializationKeys.total_post) as? Int
    self.slug = aDecoder.decodeObject(forKey: SerializationKeys.slug) as? String
    self.groupId = aDecoder.decodeObject(forKey: SerializationKeys.groupId) as? Int
    self.created = aDecoder.decodeObject(forKey: SerializationKeys.created) as? Int
    self.updatedUser = aDecoder.decodeObject(forKey: SerializationKeys.updatedUser) as? UserCreate
    self.createdUser = aDecoder.decodeObject(forKey: SerializationKeys.createdUser) as? UserCreate
    self.categoryId = aDecoder.decodeObject(forKey: SerializationKeys.categoryId) as? Int
    self.categoryName = aDecoder.decodeObject(forKey: SerializationKeys.categoryName) as? String
    self.urlImage = aDecoder.decodeObject(forKey: SerializationKeys.urlImage) as? String
    self.children = aDecoder.decodeObject(forKey: SerializationKeys.children) as? [Category]
    self.posts = aDecoder.decodeObject(forKey: SerializationKeys.posts) as? [PostDetailModel]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(type, forKey: SerializationKeys.type)
    aCoder.encode(updated, forKey: SerializationKeys.updated)
    aCoder.encode(parentId, forKey: SerializationKeys.parentId)
    aCoder.encode(total_post, forKey: SerializationKeys.total_post)
    aCoder.encode(slug, forKey: SerializationKeys.slug)
    aCoder.encode(groupId, forKey: SerializationKeys.groupId)
    aCoder.encode(created, forKey: SerializationKeys.created)
    aCoder.encode(updatedUser, forKey: SerializationKeys.updatedUser)
    aCoder.encode(categoryId, forKey: SerializationKeys.categoryId)
    aCoder.encode(categoryName, forKey: SerializationKeys.categoryName)
    aCoder.encode(createdUser, forKey: SerializationKeys.createdUser)
    aCoder.encode(urlImage, forKey: SerializationKeys.urlImage)
    aCoder.encode(children, forKey: SerializationKeys.children)
    aCoder.encode(posts, forKey: SerializationKeys.posts)
  }

}
