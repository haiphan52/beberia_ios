//
//  User.swift
//
//  Created by iMAC on 8/24/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class User: NSCoding {
    
  //  static let shareUser = User()

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let fcmToken = "fcm_token"
    static let verifed = "verifed"
    static let displayName = "display_name"
    static let numberFollowing = "number_following"
    static let typeMom = "type_mom"
    static let cityName = "city_name"
    static let numberFollower = "number_follower"
    static let babyInfo = "baby_info"
    static let checkFollow = "check_follow"
    static let id = "id"
    static let phone = "phone"
    static let avatar = "avatar"
    static let numberFeed = "number_feed"
    static let unWatch = "unWatch"
    static let unWatch_secret = "unWatch_secret"
    static let district = "district"
    static let password_secrect = "password_secrect"
    static let isFollow = "is_follow"
    static let isRequest = "is_request"
    static let timeOnline = "time_online"
    static let newDevice = "new_device"
  }

  // MARK: Properties
  public var fcmToken: String?
  public var timeOnline: Int?
  public var verifed: Int?
  public var displayName: String?
  public var numberFollowing: Int?
  public var typeMom: Int?
  public var cityName: CityName?
  public var district: DistrictModel?
  public var numberFollower: Int?
  public var babyInfo: [BabyInfo]?
  public var checkFollow: Int?
  public var id: Int?
  public var phone: String?
  public var avatar: String?
  public var numberFeed: Int?
  public var unWatch: Int?
  public var unWatch_secret: Int?
  public var password_secrect: String?
    public var isFollow: Int?
    public var isRequest: Int?
    public var isSelected: Bool = false
    public var newDevice: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    timeOnline = json[SerializationKeys.timeOnline].int
    isFollow = json[SerializationKeys.isFollow].int
    isRequest = json[SerializationKeys.isRequest].int
    password_secrect = json[SerializationKeys.password_secrect].string
    fcmToken = json[SerializationKeys.fcmToken].string
    verifed = json[SerializationKeys.verifed].int
    displayName = json[SerializationKeys.displayName].string
    numberFollowing = json[SerializationKeys.numberFollowing].int
    typeMom = json[SerializationKeys.typeMom].int
    cityName = CityName(json: json[SerializationKeys.cityName])
//    district = District(json: json[SerializationKeys.district])
    numberFollower = json[SerializationKeys.numberFollower].int
    if let items = json[SerializationKeys.babyInfo].array { babyInfo = items.map { BabyInfo(json: $0) } }
    checkFollow = json[SerializationKeys.checkFollow].int
    id = json[SerializationKeys.id].int
    phone = json[SerializationKeys.phone].string
    avatar = json[SerializationKeys.avatar].string
    numberFeed = json[SerializationKeys.numberFeed].int
    unWatch = json[SerializationKeys.unWatch].int
    unWatch_secret = json[SerializationKeys.unWatch_secret].int
    newDevice = json[SerializationKeys.newDevice].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fcmToken { dictionary[SerializationKeys.fcmToken] = value }
    if let value = verifed { dictionary[SerializationKeys.verifed] = value }
    if let value = displayName { dictionary[SerializationKeys.displayName] = value }
    if let value = numberFollowing { dictionary[SerializationKeys.numberFollowing] = value }
    if let value = typeMom { dictionary[SerializationKeys.typeMom] = value }
    if let value = cityName { dictionary[SerializationKeys.cityName] = value.dictionaryRepresentation() }
//    if let value = district { dictionary[SerializationKeys.district] = value.dictionaryRepresentation() }
    if let value = numberFollower { dictionary[SerializationKeys.numberFollower] = value }
    if let value = babyInfo { dictionary[SerializationKeys.babyInfo] = value.map { $0.dictionaryRepresentation() } }
    if let value = checkFollow { dictionary[SerializationKeys.checkFollow] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = avatar { dictionary[SerializationKeys.avatar] = value }
    if let value = numberFeed { dictionary[SerializationKeys.numberFeed] = value }
    if let value = unWatch { dictionary[SerializationKeys.unWatch] = value }
    if let value = unWatch_secret { dictionary[SerializationKeys.unWatch_secret] = value }
    if let value = newDevice { dictionary[SerializationKeys.newDevice] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.fcmToken = aDecoder.decodeObject(forKey: SerializationKeys.fcmToken) as? String
    self.verifed = aDecoder.decodeObject(forKey: SerializationKeys.verifed) as? Int
    self.displayName = aDecoder.decodeObject(forKey: SerializationKeys.displayName) as? String
    self.numberFollowing = aDecoder.decodeObject(forKey: SerializationKeys.numberFollowing) as? Int
    self.typeMom = aDecoder.decodeObject(forKey: SerializationKeys.typeMom) as? Int
    self.cityName = aDecoder.decodeObject(forKey: SerializationKeys.cityName) as? CityName
//    self.district = aDecoder.decodeObject(forKey: SerializationKeys.district) as? District
    self.numberFollower = aDecoder.decodeObject(forKey: SerializationKeys.numberFollower) as? Int
    self.babyInfo = aDecoder.decodeObject(forKey: SerializationKeys.babyInfo) as? [BabyInfo]
    self.checkFollow = aDecoder.decodeObject(forKey: SerializationKeys.checkFollow) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
    self.avatar = aDecoder.decodeObject(forKey: SerializationKeys.avatar) as? String
    self.numberFeed = aDecoder.decodeObject(forKey: SerializationKeys.numberFeed) as? Int
    self.unWatch = aDecoder.decodeObject(forKey: SerializationKeys.unWatch) as? Int
    self.unWatch_secret = aDecoder.decodeObject(forKey: SerializationKeys.unWatch_secret) as? Int
    self.newDevice = aDecoder.decodeObject(forKey: SerializationKeys.newDevice) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(fcmToken, forKey: SerializationKeys.fcmToken)
    aCoder.encode(verifed, forKey: SerializationKeys.verifed)
    aCoder.encode(displayName, forKey: SerializationKeys.displayName)
    aCoder.encode(numberFollowing, forKey: SerializationKeys.numberFollowing)
    aCoder.encode(typeMom, forKey: SerializationKeys.typeMom)
    aCoder.encode(cityName, forKey: SerializationKeys.cityName)
//    aCoder.encode(district, forKey: SerializationKeys.district)
    aCoder.encode(numberFollower, forKey: SerializationKeys.numberFollower)
    aCoder.encode(babyInfo, forKey: SerializationKeys.babyInfo)
    aCoder.encode(checkFollow, forKey: SerializationKeys.checkFollow)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(phone, forKey: SerializationKeys.phone)
    aCoder.encode(avatar, forKey: SerializationKeys.avatar)
    aCoder.encode(numberFeed, forKey: SerializationKeys.numberFeed)
    aCoder.encode(unWatch, forKey: SerializationKeys.unWatch)
    aCoder.encode(unWatch_secret, forKey: SerializationKeys.unWatch_secret)
    aCoder.encode(unWatch_secret, forKey: SerializationKeys.newDevice)
  }

}
