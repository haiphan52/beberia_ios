//
//  Posts.swift
//
//  Created by IMAC on 8/20/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Posts: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let urlImage = "url_image"
    static let likeNumber = "like_number"
    static let updated = "updated"
    static let commentNumber = "comment_number"
    static let slug = "slug"
    static let id = "id"
    static let created = "created"
    static let title = "title"
    static let categoryId = "category_id"
    static let author = "author"
  }

  // MARK: Properties
  public var urlImage: String?
  public var likeNumber: Int?
  public var updated: Int?
  public var commentNumber: Int?
  public var slug: String?
  public var id: Int?
  public var created: Int?
  public var title: String?
  public var categoryId: Int?
  public var author: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    urlImage = json[SerializationKeys.urlImage].string
    likeNumber = json[SerializationKeys.likeNumber].int
    updated = json[SerializationKeys.updated].int
    commentNumber = json[SerializationKeys.commentNumber].int
    slug = json[SerializationKeys.slug].string
    id = json[SerializationKeys.id].int
    created = json[SerializationKeys.created].int
    title = json[SerializationKeys.title].string
    categoryId = json[SerializationKeys.categoryId].int
    author = json[SerializationKeys.author].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = urlImage { dictionary[SerializationKeys.urlImage] = value }
    if let value = likeNumber { dictionary[SerializationKeys.likeNumber] = value }
    if let value = updated { dictionary[SerializationKeys.updated] = value }
    if let value = commentNumber { dictionary[SerializationKeys.commentNumber] = value }
    if let value = slug { dictionary[SerializationKeys.slug] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = categoryId { dictionary[SerializationKeys.categoryId] = value }
    if let value = author { dictionary[SerializationKeys.author] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.urlImage = aDecoder.decodeObject(forKey: SerializationKeys.urlImage) as? String
    self.likeNumber = aDecoder.decodeObject(forKey: SerializationKeys.likeNumber) as? Int
    self.updated = aDecoder.decodeObject(forKey: SerializationKeys.updated) as? Int
    self.commentNumber = aDecoder.decodeObject(forKey: SerializationKeys.commentNumber) as? Int
    self.slug = aDecoder.decodeObject(forKey: SerializationKeys.slug) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.created = aDecoder.decodeObject(forKey: SerializationKeys.created) as? Int
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.categoryId = aDecoder.decodeObject(forKey: SerializationKeys.categoryId) as? Int
    self.author = aDecoder.decodeObject(forKey: SerializationKeys.author) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(urlImage, forKey: SerializationKeys.urlImage)
    aCoder.encode(likeNumber, forKey: SerializationKeys.likeNumber)
    aCoder.encode(updated, forKey: SerializationKeys.updated)
    aCoder.encode(commentNumber, forKey: SerializationKeys.commentNumber)
    aCoder.encode(slug, forKey: SerializationKeys.slug)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(created, forKey: SerializationKeys.created)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(categoryId, forKey: SerializationKeys.categoryId)
    aCoder.encode(author, forKey: SerializationKeys.author)
  }

}
