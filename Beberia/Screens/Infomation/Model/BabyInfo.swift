//
//  BabyInfo.swift
//
//  Created by iMAC on 9/6/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

enum GenderBaby: Int, CaseIterable {
    case male, female, unknown
    
    var keyAPI: String {
        switch self {
        case .male:
            return "MALE"
        case .female:
            return "FEMALE"
        case .unknown:
            return ""
        }
    }
}

public final class BabyInfo: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let nickname = "nickname"
    static let gender = "gender"
    static let year = "year"
    static let month = "month"
    static let id = "id"
    static let birthday = "birthday"
    static let ageRange = "age_range"
    static let avatar = "avatar"
    
  }

  // MARK: Properties
  public var nickname: String?
  public var gender: Int?
  public var year: Int?
  public var month: Int?
  public var id: Int?
  public var birthday: Int?
  public var ageRange: Int?
    public var avatar: String?
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    nickname = json[SerializationKeys.nickname].string
    avatar = json[SerializationKeys.avatar].string
    gender = json[SerializationKeys.gender].int
    year = json[SerializationKeys.year].int
    month = json[SerializationKeys.month].int
    id = json[SerializationKeys.id].int
    birthday = json[SerializationKeys.birthday].int
    ageRange = json[SerializationKeys.ageRange].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = nickname { dictionary[SerializationKeys.nickname] = value }
    if let value = gender { dictionary[SerializationKeys.gender] = value }
    if let value = year { dictionary[SerializationKeys.year] = value }
    if let value = month { dictionary[SerializationKeys.month] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = birthday { dictionary[SerializationKeys.birthday] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.nickname = aDecoder.decodeObject(forKey: SerializationKeys.nickname) as? String
    self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? Int
    self.year = aDecoder.decodeObject(forKey: SerializationKeys.year) as? Int
    self.month = aDecoder.decodeObject(forKey: SerializationKeys.month) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.birthday = aDecoder.decodeObject(forKey: SerializationKeys.birthday) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(nickname, forKey: SerializationKeys.nickname)
    aCoder.encode(gender, forKey: SerializationKeys.gender)
    aCoder.encode(year, forKey: SerializationKeys.year)
    aCoder.encode(month, forKey: SerializationKeys.month)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(birthday, forKey: SerializationKeys.birthday)
  }

}
