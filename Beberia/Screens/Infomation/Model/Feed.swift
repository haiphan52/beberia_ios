//
//  Feed.swift
//
//  Created by iMAC on 9/6/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Feed: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let content = "content"
    static let media = "media"
    static let id = "id"
    static let userCreate = "user_create"
    static let title = "title"
  }

  // MARK: Properties
  public var content: String?
  public var media: [Media]?
  public var id: Int?
  public var userCreate: UserCreate?
  public var title: String?
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    content = json[SerializationKeys.content].string
    title = json[SerializationKeys.title].string
    if let items = json[SerializationKeys.media].array { media = items.map { Media(json: $0) } }
    id = json[SerializationKeys.id].int
    userCreate = UserCreate(json: json[SerializationKeys.userCreate])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = content { dictionary[SerializationKeys.content] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = media { dictionary[SerializationKeys.media] = value.map { $0.dictionaryRepresentation() } }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = userCreate { dictionary[SerializationKeys.userCreate] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.content = aDecoder.decodeObject(forKey: SerializationKeys.content) as? String
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.media = aDecoder.decodeObject(forKey: SerializationKeys.media) as? [Media]
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.userCreate = aDecoder.decodeObject(forKey: SerializationKeys.userCreate) as? UserCreate
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(content, forKey: SerializationKeys.content)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(media, forKey: SerializationKeys.media)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(userCreate, forKey: SerializationKeys.userCreate)
  }

}
