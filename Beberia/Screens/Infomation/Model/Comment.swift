//
//  Comment.swift
//
//  Created by iMAC on 9/6/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Comment: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let content = "content"
    static let likeNumber = "like_number"
    static let user = "user"
    static let reply = "reply"
    static let commentId = "comment_id"
    static let createdAt = "created"
    static let remainReply = "remain_reply"
    static let replyCommentNumber = "reply_comment_number"
    static let isLiked = "is_liked"
    static let userTag = "user_tag"
    static let secretNumber = "secret_number"
    
    
  }

  // MARK: Properties
  public var content: String?
  public var likeNumber: Int?
  public var user: User?
  public var reply: [Comment]?
  public var commentId: Int?
  public var createdAt: Int?
  public var remainReply: Int?
  public var replyCommentNumber: Int?
  public var isLiked: Int?
  public var isTapExpand: Bool = false
  public var nextPage: Int?
  public var secretNumber: Int?
  public var userTag: [User]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    content = json[SerializationKeys.content].string
    likeNumber = json[SerializationKeys.likeNumber].int
    secretNumber = json[SerializationKeys.secretNumber].int
    user = User(json: json[SerializationKeys.user])
    if let items = json[SerializationKeys.userTag].array { userTag = items.map { User(json: $0) } }
    if let items = json[SerializationKeys.reply].array { reply = items.map { Comment(json: $0) } }
    commentId = json[SerializationKeys.commentId].int
    createdAt = json[SerializationKeys.createdAt].int
    remainReply = json[SerializationKeys.remainReply].int
    replyCommentNumber = json[SerializationKeys.replyCommentNumber].int
    isLiked = json[SerializationKeys.isLiked].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = content { dictionary[SerializationKeys.content] = value }
    if let value = likeNumber { dictionary[SerializationKeys.likeNumber] = value }
    if let value = user { dictionary[SerializationKeys.user] = value.dictionaryRepresentation() }
    if let value = reply { dictionary[SerializationKeys.reply] = value.map { $0.dictionaryRepresentation() } }
    if let value = userTag { dictionary[SerializationKeys.userTag] = value.map { $0.dictionaryRepresentation() } }
    if let value = commentId { dictionary[SerializationKeys.commentId] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = remainReply { dictionary[SerializationKeys.remainReply] = value }
    if let value = replyCommentNumber { dictionary[SerializationKeys.replyCommentNumber] = value }
    if let value = isLiked { dictionary[SerializationKeys.isLiked] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.content = aDecoder.decodeObject(forKey: SerializationKeys.content) as? String
    self.likeNumber = aDecoder.decodeObject(forKey: SerializationKeys.likeNumber) as? Int
    self.user = aDecoder.decodeObject(forKey: SerializationKeys.user) as? User
    self.reply = aDecoder.decodeObject(forKey: SerializationKeys.reply) as? [Comment]
    self.userTag = aDecoder.decodeObject(forKey: SerializationKeys.reply) as? [User]
    self.commentId = aDecoder.decodeObject(forKey: SerializationKeys.commentId) as? Int
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? Int
    self.remainReply = aDecoder.decodeObject(forKey: SerializationKeys.remainReply) as? Int
    self.replyCommentNumber = aDecoder.decodeObject(forKey: SerializationKeys.replyCommentNumber) as? Int
    self.isLiked = aDecoder.decodeObject(forKey: SerializationKeys.isLiked) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(content, forKey: SerializationKeys.content)
    aCoder.encode(likeNumber, forKey: SerializationKeys.likeNumber)
    aCoder.encode(user, forKey: SerializationKeys.user)
    aCoder.encode(reply, forKey: SerializationKeys.reply)
    aCoder.encode(userTag, forKey: SerializationKeys.userTag)
    aCoder.encode(commentId, forKey: SerializationKeys.commentId)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(remainReply, forKey: SerializationKeys.remainReply)
    aCoder.encode(replyCommentNumber, forKey: SerializationKeys.replyCommentNumber)
    aCoder.encode(isLiked, forKey: SerializationKeys.isLiked)
  }

}
