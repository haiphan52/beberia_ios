//
//  Datas.swift
//
//  Created by iMAC on 9/6/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Noti: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let replyComment = "reply_comment"
        static let userFrom = "user_from"
        static let id = "id"
        static let feed = "feed"
        static let createdAt = "created_at"
        static let totalUser = "total_user"
        static let comment = "comment"
        static let type = "type"
        static let read = "read"
        static let secret_number = "secret_number"
        static let message = "message"
        static let group = "group"
        static let typeRedirect = "type_redirect"
    }
    
    // MARK: Properties
    public var replyComment: Comment?
    public var userFrom: User?
    public var id: Int?
    public var secret_number: Int?
    public var feed: HomeFeed?
    public var createdAt: Int?
    public var totalUser: Int?
    public var comment: Comment?
    public var type: Int?
    public var read: Int?
    public var message: MessageNotifyModel?
    public var group: GroupNotifyModel?
    public var typeRedirect: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        replyComment = Comment(json: json[SerializationKeys.replyComment])
        userFrom = User(json: json[SerializationKeys.userFrom])
        id = json[SerializationKeys.id].int
        secret_number = json[SerializationKeys.secret_number].int
        feed = HomeFeed(json: json[SerializationKeys.feed])
        createdAt = json[SerializationKeys.createdAt].int
        totalUser = json[SerializationKeys.totalUser].int
        comment = Comment(json: json[SerializationKeys.comment])
        type = json[SerializationKeys.type].int
        read = json[SerializationKeys.read].int
        message = MessageNotifyModel(json: json[SerializationKeys.message])
        group = GroupNotifyModel(json: json[SerializationKeys.group])
        typeRedirect = json[SerializationKeys.typeRedirect].int
        
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = replyComment { dictionary[SerializationKeys.replyComment] = value.dictionaryRepresentation() }
        if let value = userFrom { dictionary[SerializationKeys.userFrom] = value.dictionaryRepresentation() }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = secret_number { dictionary[SerializationKeys.secret_number] = value }
        if let value = feed { dictionary[SerializationKeys.feed] = value.dictionaryRepresentation() }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = totalUser { dictionary[SerializationKeys.totalUser] = value }
        if let value = comment { dictionary[SerializationKeys.comment] = value.dictionaryRepresentation() }
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = read { dictionary[SerializationKeys.read] = value }
        if let value = message { dictionary[SerializationKeys.message] = value.dictionaryRepresentation() }
        if let value = group { dictionary[SerializationKeys.group] = value.dictionaryRepresentation() }
        if let value = typeRedirect { dictionary[SerializationKeys.typeRedirect] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.replyComment = aDecoder.decodeObject(forKey: SerializationKeys.replyComment) as? Comment
        self.userFrom = aDecoder.decodeObject(forKey: SerializationKeys.userFrom) as? User
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.feed = aDecoder.decodeObject(forKey: SerializationKeys.feed) as? HomeFeed
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? Int
        self.totalUser = aDecoder.decodeObject(forKey: SerializationKeys.totalUser) as? Int
        self.comment = aDecoder.decodeObject(forKey: SerializationKeys.comment) as? Comment
        self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? Int
        self.read = aDecoder.decodeObject(forKey: SerializationKeys.read) as? Int
        self.secret_number = aDecoder.decodeObject(forKey: SerializationKeys.secret_number) as? Int
        self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? MessageNotifyModel
        self.group = aDecoder.decodeObject(forKey: SerializationKeys.group) as? GroupNotifyModel
        self.typeRedirect = aDecoder.decodeObject(forKey: SerializationKeys.typeRedirect) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(replyComment, forKey: SerializationKeys.replyComment)
        aCoder.encode(userFrom, forKey: SerializationKeys.userFrom)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(feed, forKey: SerializationKeys.feed)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(totalUser, forKey: SerializationKeys.totalUser)
        aCoder.encode(comment, forKey: SerializationKeys.comment)
        aCoder.encode(type, forKey: SerializationKeys.type)
        aCoder.encode(read, forKey: SerializationKeys.read)
        aCoder.encode(secret_number, forKey: SerializationKeys.secret_number)
        aCoder.encode(message, forKey: SerializationKeys.message)
        aCoder.encode(group, forKey: SerializationKeys.group)
        aCoder.encode(typeRedirect, forKey: SerializationKeys.typeRedirect)
    }
    
}
