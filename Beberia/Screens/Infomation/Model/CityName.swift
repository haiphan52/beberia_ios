//
//  CityName.swift
//
//  Created by iMAC on 9/6/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CityName: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let locationNameVi = "location_name_vi"
    static let userActive = "user_active"
    static let id = "id"
    static let locationNameEn = "location_name_en"
    static let locationCode = "location_code"
  }

  // MARK: Properties
  public var locationNameVi: String?
  public var userActive: Int?
  public var id: Int?
  public var locationNameEn: String?
  public var locationCode: Int?
    
    public init() {
        
    }

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    locationNameVi = json[SerializationKeys.locationNameVi].string
    userActive = json[SerializationKeys.userActive].int
    id = json[SerializationKeys.id].int
    locationNameEn = json[SerializationKeys.locationNameEn].string
    locationCode = json[SerializationKeys.locationCode].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = locationNameVi { dictionary[SerializationKeys.locationNameVi] = value }
    if let value = userActive { dictionary[SerializationKeys.userActive] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = locationNameEn { dictionary[SerializationKeys.locationNameEn] = value }
    if let value = locationCode { dictionary[SerializationKeys.locationCode] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.locationNameVi = aDecoder.decodeObject(forKey: SerializationKeys.locationNameVi) as? String
    self.userActive = aDecoder.decodeObject(forKey: SerializationKeys.userActive) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.locationNameEn = aDecoder.decodeObject(forKey: SerializationKeys.locationNameEn) as? String
    self.locationCode = aDecoder.decodeObject(forKey: SerializationKeys.locationCode) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(locationNameVi, forKey: SerializationKeys.locationNameVi)
    aCoder.encode(userActive, forKey: SerializationKeys.userActive)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(locationNameEn, forKey: SerializationKeys.locationNameEn)
    aCoder.encode(locationCode, forKey: SerializationKeys.locationCode)
  }

}
