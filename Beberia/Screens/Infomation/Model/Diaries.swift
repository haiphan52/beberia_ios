//
//  Diaries.swift
//
//  Created by iMAC on 9/5/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Diaries: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let description = "description"
    static let urlImage = "url_image"
    static let likeNumber = "like_number"
    static let commentNumber = "comment_number"
    static let id = "id"
    static let user_create = "user_create"
    static let title = "title"
    static let isLiked = "is_liked"
    static let media = "media"
    static let isPrivate = "is_private"
    static let created = "created"
    static let quote = "quote"
    
  }

  // MARK: Properties
  public var description: String?
  public var urlImage: String?
  public var likeNumber: Int?
  public var commentNumber: Int?
  public var id: Int?
  public var user_create: User?
  public var media: [Media]?
  public var title: String?
  public var isLiked: Int?
    public var isPrivate: Int?
    public var created: Int?
    public var quote: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    description = json[SerializationKeys.description].string
    urlImage = json[SerializationKeys.urlImage].string
    likeNumber = json[SerializationKeys.likeNumber].int
    commentNumber = json[SerializationKeys.commentNumber].int
    id = json[SerializationKeys.id].int
    user_create = User(json: json[SerializationKeys.user_create])
    if let items = json[SerializationKeys.media].array { media = items.map { Media(json: $0) } }
    title = json[SerializationKeys.title].string
    isLiked = json[SerializationKeys.isLiked].int
    isPrivate = json[SerializationKeys.isPrivate].int
    created = json[SerializationKeys.created].int
    quote = json[SerializationKeys.quote].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = description { dictionary[SerializationKeys.description] = value }
    if let value = urlImage { dictionary[SerializationKeys.urlImage] = value }
    if let value = likeNumber { dictionary[SerializationKeys.likeNumber] = value }
    if let value = commentNumber { dictionary[SerializationKeys.commentNumber] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = user_create { dictionary[SerializationKeys.user_create] = value.dictionaryRepresentation() }
    if let value = media { dictionary[SerializationKeys.media] = value.map { $0.dictionaryRepresentation() } }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = isLiked { dictionary[SerializationKeys.isLiked] = value }
    if let value = isPrivate { dictionary[SerializationKeys.isPrivate] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.description = aDecoder.decodeObject(forKey: SerializationKeys.description) as? String
    self.urlImage = aDecoder.decodeObject(forKey: SerializationKeys.urlImage) as? String
    self.likeNumber = aDecoder.decodeObject(forKey: SerializationKeys.likeNumber) as? Int
    self.commentNumber = aDecoder.decodeObject(forKey: SerializationKeys.commentNumber) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.user_create = aDecoder.decodeObject(forKey: SerializationKeys.user_create) as? User
    self.media = aDecoder.decodeObject(forKey: SerializationKeys.media) as? [Media]
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.isLiked = aDecoder.decodeObject(forKey: SerializationKeys.isLiked) as? Int
    self.isPrivate = aDecoder.decodeObject(forKey: SerializationKeys.isPrivate) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(description, forKey: SerializationKeys.description)
    aCoder.encode(urlImage, forKey: SerializationKeys.urlImage)
    aCoder.encode(likeNumber, forKey: SerializationKeys.likeNumber)
    aCoder.encode(commentNumber, forKey: SerializationKeys.commentNumber)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(user_create, forKey: SerializationKeys.user_create)
    aCoder.encode(media, forKey: SerializationKeys.media)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(isLiked, forKey: SerializationKeys.isLiked)
    aCoder.encode(isPrivate, forKey: SerializationKeys.isPrivate)
  }

}
