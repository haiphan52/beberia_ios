//
//  Data.swift
//
//  Created by iMAC on 9/5/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class DiaryHome: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let type = "type"
    static let title = "title"
    static let diaries = "diaries"
    static let is_load_more = "is_load_more"
  }

  // MARK: Properties
  public var type: String?
  public var title: String?
  public var is_load_more: Int?
  public var diaries: [Diaries]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    type = json[SerializationKeys.type].string
    title = json[SerializationKeys.title].string
    is_load_more = json[SerializationKeys.is_load_more].int
    if let items = json[SerializationKeys.diaries].array { diaries = items.map { Diaries(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = is_load_more { dictionary[SerializationKeys.is_load_more] = value }
    if let value = diaries { dictionary[SerializationKeys.diaries] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? String
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.is_load_more = aDecoder.decodeObject(forKey: SerializationKeys.is_load_more) as? Int
    self.diaries = aDecoder.decodeObject(forKey: SerializationKeys.diaries) as? [Diaries]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(type, forKey: SerializationKeys.type)
    aCoder.encode(title, forKey: SerializationKeys.title)
     aCoder.encode(is_load_more, forKey: SerializationKeys.is_load_more)
    aCoder.encode(diaries, forKey: SerializationKeys.diaries)
  }

}
