//
//  DetailInfoTableViewCell.swift
//  Beberia
//
//  Created by iMAC on 8/19/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class DetailInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var leaddingIconLike: NSLayoutConstraint!
    @IBOutlet weak var imgAnh: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var heightViewBtnLoadContraint: NSLayoutConstraint!
    @IBOutlet weak var heigtViewContraint: NSLayoutConstraint!
    @IBOutlet weak var btnLoadmore: UIButton!
    @IBOutlet weak var viewbtnLoadmoreConstraint: UIView!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var imgDelete: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var widthBtnOption: NSLayoutConstraint!
    
    var tapDeleteAlert: (_ index :Int) -> () = {_ in}
    
    static let heightRow:CGFloat = 95
    static let heightRowWithButton:CGFloat = 140
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnLoadmore.layer.cornerRadius = 10
        btnLoadmore.layer.borderWidth = 1
        btnLoadmore.layer.borderColor = UIColor.clear.cgColor
    }

    func setupData(listCategories: Category,row:Int,totalRows:Int){
        guard let category = listCategories.posts?[row] else {
            return
        }
       
        widthBtnOption.constant = 0
        imgDelete.isHidden = true
        btnDelete.isHidden = true
        btnLoadmore.isHidden = true
        viewbtnLoadmoreConstraint.isHidden = true
        imgAnh.setImage(imageString: category.urlImage ?? "") //kf.setImage(with: URL(string: category.urlImage ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        lblDescription.text = category.title
        let date = Date(timeIntervalSince1970: TimeInterval(category.created ?? 0))
        let dateFormatter = DateFormatter()
        imgLike.image = R.image.ic_like()
        if category.isLiked == 1 {
            imgLike.image = R.image.icLikeActive()
        }
        dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyy
        lblDate.text = dateFormatter.string(from: date)
        lblLike.text = "\(category.likeNumber ?? 0)"
        lblComment.text  = "\(category.commentNumber ?? 0)"
        
        if listCategories.total_post ?? 0 > 3 {
            if row == totalRows - 1 {
                viewbtnLoadmoreConstraint.isHidden = false
                btnLoadmore.isHidden = false
            }
        }
       

    }
    func setupDataMore(listDetailMore: PostDetailModel){
        leaddingIconLike.constant = 0
        widthBtnOption.constant = 0
        imgDelete.isHidden = true
        btnDelete.isHidden = true
        imgAnh.kf.setImage(with: URL(string: listDetailMore.urlImage ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        lblDescription.text = listDetailMore.title
//        let date = Date(timeIntervalSince1970: TimeInterval(listDetailMore.created ?? 0))
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = Key.DateFormat.DateFormatddMMyy
//        lblDate.text = dateFormatter.string(from: date)
        lblDate.text = ""
        imgLike.image = R.image.ic_like()
        if listDetailMore.isLiked == 1 {
            imgLike.image = R.image.icLikeActive()
        }
        lblLike.text = "\(listDetailMore.likeNumber ?? 0)"
        lblComment.text  = "\(listDetailMore.commentNumber ?? 0)"
        btnLoadmore.isHidden = true
        viewbtnLoadmoreConstraint.isHidden = true
    }
    
    @IBAction func didPressDelete(_ sender: Any) {
        tapDeleteAlert(btnDelete.tag)
    }
    
}

