//
//  HeaderSectionTableviewCell.swift
//  Beberia
//
//  Created by iMAC on 8/16/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class HeaderSectionTableviewCell: UITableViewHeaderFooterView {

    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    var tapViewLogout: (_ index :Int) -> () = {_ in}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func didTapLogout(_ sender: Any) {
        tapViewLogout(btnAction.tag)
    }
}
