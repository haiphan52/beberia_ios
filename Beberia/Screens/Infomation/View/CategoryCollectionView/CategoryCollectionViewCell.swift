//
//  CategoryCollectionViewCell.swift
//  Beberia
//
//  Created by IMAC on 8/13/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblNameCategory: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 5)
      //  imgCategory.roundCorners(corners: [.topLeft, .topRight], radius: 5.0)
    }

}
