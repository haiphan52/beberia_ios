//
//  CommentTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 8/15/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var imgReply: UIImageView!
    @IBOutlet weak var imgOption: UIImageView!
    @IBOutlet weak var leadingConstrain: NSLayoutConstraint!
    @IBOutlet var heightTextViewContent: NSLayoutConstraint!
    @IBOutlet weak var viewNumberLike: UIView!
    @IBOutlet weak var viewNumberReply: UIView!
    @IBOutlet weak var btnCancelEdit: UIButton!
    @IBOutlet weak var btnSavedEdit: UIButton!
    @IBOutlet weak var txtContent: UITextView!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var heightViewBGReply: NSLayoutConstraint!
    @IBOutlet weak var viewBGReplyComment: UIView!
    @IBOutlet weak var btnOption: UIButton!
    @IBOutlet weak var constrainHeightViewReply: NSLayoutConstraint!
    @IBOutlet weak var constrainHeightTbvCommentReply: NSLayoutConstraint!
    @IBOutlet weak var tbvCommenReply: UITableView!
    @IBOutlet weak var lblNumberReply1: UILabel!
    @IBOutlet weak var lblNumberReply: UILabel!
    @IBOutlet weak var lblNumberLike: UILabel!
    @IBOutlet weak var lblContent: ActiveLabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tbvComment: UITableView!
    @IBOutlet weak var btnShowReplyComment: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var lblNumberReplyAndViewSeeMore: UILabel!
    @IBOutlet weak var buttonLoadMore: UIButton!
    @IBOutlet weak var heightButtonLoadMore: NSLayoutConstraint!
    var indexbtn: Int = 0
    var nextPageReply: Int = 1
    var comments = [Comment]()
    var replyComments = [Comment]()
    var tapShowReply         : (_ index: Int) ->() = {_ in}
    var tapLike          : (_ index: Int, _ idComment : Int?, _ type: Int) ->() = {_,_,_ in}
    var tapOption        : (_ index: Int, _ idComment : Int?, _ type: Int) ->() = {_,_,_ in}
    var tapReply         : (_ index: Int) ->() = {_ in}
    var tapReplyComment  : (_ userId: Int,_ nameUser: String, _ idComment: Int,_ numberSecret: Int ) ->() = {_,_,_,_ in}
    var tapShowMoreReplyComment  : (_ index: Int, _ idComment : Int?, _ type: Int) ->() = {_,_,_  in}
    var tapCancelEdit            : (_ index: Int, _ idComment : Int?, _ type: Int) ->() = {_,_,_ in}
    var tapSavedEdit             : (_ index: Int, _ idComment : Int?, _ type: Int) ->() = {_,_,_ in}
    var tapUser             : (_ index: Int, _ user : User?, _ type: Int) ->() = {_,_,_ in}
    var numberLike: Int = 0
    var numberLikeReply: Int = 0
    var isTapReplyComment = false
    var indexComment = 0
    fileprivate var heightDictionary: [IndexPath : CGFloat] = [:]
    
    var isLike = 0
    {
        didSet{
            imgLike.image = isLike == 1 ? R.image.icLikeActive() : R.image.ic_like()
        }
    }
    
    var isLikeReply = 0
    {
        didSet{
            imgLike.image = isLike == 1 ? R.image.icLikeActive() : R.image.ic_like()
        }
    }
    
    var isCheckSecret = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgAvatar.cornerRadius = imgAvatar.frame.height / 2
        
        tbvComment.register(UINib(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "CommentTableViewCell")
        tbvComment.delegate = self
        tbvComment.dataSource = self
        tbvComment.separatorStyle = .none
        tbvComment.allowsSelection = false
        tbvComment.rowHeight = UITableView.automaticDimension
//        tbvComment.estimatedRowHeight = 112
        
        viewLine.cornerRadius = 2
        setUpActiveLable()
        
        self.tbvComment.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
       
        tbvComment.layer.removeAllAnimations()
        self.constrainHeightTbvCommentReply.constant = tbvComment.contentSize.height
        
        UIView.animate(withDuration: 0.5) {
            self.updateConstraints()
            self.layoutIfNeeded()
        }
    }
    
    func setUpActiveLable(){
        lblContent.enabledTypes = [.mention]
        lblContent.textColor = .black
        lblContent.mentionColor = UIColor.init(hexString: AppColor.textMentionColor)!
        lblContent.handleMentionTap { hashtag in
            print("Success. You just tapped the \(hashtag) hashtag")
            print(self.btnLike.tag)
            self.tapUser(self.btnLike.tag, nil , Key.TypeReplyComment)
        }
    }

    func setupData(comment: Comment, nextPage: Int, isReply: Bool){

        let date = Date(timeIntervalSince1970: TimeInterval(comment.createdAt ?? 0))
       // let date = Date(timeIntervalSince1970: TimeInterval(comment.createdAt ?? 0)).timeAgoDisplayHomeFeed()
        
        lblTime.text = date.timeAgoDisplayHomeFeed()
        if isReply{
            if (comment.userTag ?? [User]()).count > 0{
                if comment.secretNumber ?? 0 > 0{
                    lblContent.text = "\(comment.content ?? "")"
                }else{
                    lblContent.text = "@\(comment.userTag?[0].displayName ?? "") \(comment.content ?? "")"
                }
            }else{
                lblContent.text = "\(comment.content ?? "")"
            }
        }else{
            lblContent.text = comment.content ?? ""
        }
        txtContent.text = comment.content ?? ""
        
        
        imgAvatar.image = R.image.avaDefautSecret()
        if comment.secretNumber ?? 0 > 0{
            imgAvatar.image = R.image.avaDefautSecret()
        }else{
            imgAvatar.kf.setImage(with: URL.init(string: comment.user?.avatar ?? ""), placeholder: UIImage.init(named: Key.ImagePlaceholder))
        }
        
        lblName.text = ""
        if comment.secretNumber ?? 0 > 0 {
            isCheckSecret = true
            lblName.text = R.string.localizable.secretTile_Name() + " \(comment.secretNumber!)"
        }else{
            lblName.text = comment.user?.displayName ?? ""
        }
        
     
        lblNumberReply.text = "\(comment.replyCommentNumber ?? 0)"
        lblNumberLike.text = "\(comment.likeNumber ?? 0)"
        constrainHeightTbvCommentReply.constant = 0
        isLike = comment.isLiked ?? 0
        numberLike = comment.likeNumber ?? 0
        isTapReplyComment = comment.isTapExpand
        isTapReplyComment ? (lblNumberReplyAndViewSeeMore.text = "\(R.string.localizable.homeViewMoreReplies()) ...") : (lblNumberReplyAndViewSeeMore.text = comment.replyCommentNumber == 1 ? " ---- Xem \(comment.replyCommentNumber ?? 0) câu trả lời ----" : " ---- Xem \(comment.replyCommentNumber ?? 0) câu trả lời ----")
      
        
        constrainHeightViewReply.constant = 29
        lblNumberReplyAndViewSeeMore.isHidden = false
        if (isTapReplyComment && comment.replyCommentNumber ?? 0 < 4) || (nextPage == 0) || (comment.replyCommentNumber == 0) ||  (comment.replyCommentNumber == nil) || lblNumberReplyAndViewSeeMore.text == "\(R.string.localizable.homeViewMoreReplies()) ..."{
            constrainHeightViewReply.constant = 0
            lblNumberReplyAndViewSeeMore.isHidden = true
        }
        
        imgReply.image = R.image.ic_replyComment()
        if comment.replyCommentNumber ?? 0 > 0 {
            imgReply.image = R.image.icReplyActive()
        }
        
        imgOption.isHidden = true
        btnOption.isHidden = true
        if Utils.checkLogin(selfView: Utils.getTopMostViewController()!, showAlert: false){
            if UserInfo.shareUserInfo.id == comment.user?.id ?? 0{
                btnOption.isHidden = false
                imgOption.isHidden = false
            } 
        }

        self.replyComments = comment.reply ?? [Comment]()
        tbvComment.reloadData()
        self.layoutIfNeeded()
        self.tbvComment.beginUpdates()
        self.tbvComment.endUpdates()
        
    }
  
    @IBAction func didPressSaveEdit(_ sender: Any) {
        tapSavedEdit(btnLike.tag, nil, Key.TypeComment)
    }
    
    @IBAction func didPressCancelEdit(_ sender: Any) {
        tapCancelEdit(btnLike.tag, nil, Key.TypeComment)
    }
    
    @IBAction func didPressOption(_ sender: Any) {
        tapOption(btnLike.tag, nil, Key.TypeComment)
    }
    
    @IBAction func didPressLike(_ sender: Any) {
        if Utils.checkLogin(selfView: Utils.getTopMostViewController()!){
            tapLike(btnLike.tag, nil, Key.TypeLikeComment)
        }
    }
    
    @IBAction func didPressReply(_ sender: UIButton) {
        
//        if lblNumberReplyAndViewSeeMore.text == "\(R.string.localizable.homeViewMoreReplies()) ..."{
//            tapShowMoreReplyComment(btnLike.tag, nil, Key.TypeReplyComment)
//        }else{
//            tapShowReply(sender.tag)
//        }
        
        tapShowReply(sender.tag)
        tapShowMoreReplyComment(btnLike.tag, nil, Key.TypeReplyComment)
        
    }
    
    @IBAction func didPressReplyComment(_ sender: Any) {
        if Utils.checkLogin(selfView: Utils.getTopMostViewController()!){
            tapReply(btnLike.tag)
        }
    }
    
    @IBAction func didPressImageUser(_ sender: Any) {
        // check Secret
        if !isCheckSecret{
            tapUser(btnLike.tag, nil,Key.TypeComment)
        }
    }
    
    @IBAction func didPressLoadMore(_ sender: UIButton) {
         tapShowMoreReplyComment(btnLike.tag, nil, Key.TypeComment)
    }

}

extension CommentTableViewCell: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isTapReplyComment ? self.replyComments.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        cell.setupData(comment: self.replyComments[indexPath.row], nextPage: 0, isReply: true)
        cell.btnLike.tag = indexPath.row
        cell.viewNumberLike.isHidden = false
        cell.viewNumberReply.isHidden = false
        cell.lblContent.isHidden = false
        cell.viewLine.isHidden = false
        cell.btnSavedEdit.isHidden = true
        cell.btnCancelEdit.isHidden = true
        cell.txtContent.isHidden = true
        cell.heightTextViewContent.isActive = false
        cell.lblNumberReply.isHidden = true
        cell.heightButtonLoadMore.constant = 0
        cell.buttonLoadMore.isHidden = true
        let numberLike = Int(lblNumberReply.text ?? "0") ?? 0
        if indexPath.row == replyComments.count - 1 && replyComments.count - 1 > 1 && numberLike > replyComments.count{
            cell.buttonLoadMore.isHidden = false
            cell.heightButtonLoadMore.constant = 29
        }
        cell.contentView.backgroundColor  = UIColor.init(hexString: AppColor.colorReplyCommentBG, alpha: 0.14)
        numberLikeReply = self.replyComments[indexPath.row].likeNumber ?? 0
        cell.tapLike = { index ,_,_  in
            self.tapLike(index, self.replyComments[index].commentId ?? 0, Key.TypeLikeReplyComment)
        }

        cell.tapReply = { index in
            self.tapReplyComment(self.replyComments[index].user?.id ?? 0, self.replyComments[index].user?.displayName ?? "", self.replyComments[index].commentId ?? 0, self.replyComments[index].secretNumber ?? 0)
        }
        
        cell.tapOption = { index ,_,_  in
            self.tapOption(index, self.replyComments[index].commentId ?? 0, Key.TypeReplyComment)
        }
        
        cell.tapSavedEdit = { index ,_,_  in
            self.tapSavedEdit(index, self.replyComments[index].commentId ?? 0, Key.TypeReplyComment)
        }
        
        cell.tapShowMoreReplyComment = { index ,_,_ in
            self.tapShowMoreReplyComment(index, self.replyComments[index].commentId ?? 0, Key.TypeReplyComment)
        }

        cell.tapCancelEdit = { index ,_,_  in
            self.tapCancelEdit(index, self.replyComments[index].commentId ?? 0, Key.TypeReplyComment)
            cell.txtContent.text = self.replyComments[index].content ?? ""
            cell.layoutIfNeeded()
            self.tbvComment.layoutIfNeeded()
            self.tbvComment.beginUpdates()
            self.tbvComment.endUpdates()
        }
        
        cell.tapUser = { index,_,type  in
            if type == Key.TypeReplyComment{
                if (self.replyComments[index].userTag ?? [User]()).count > 0 {
                    self.tapUser(index, self.replyComments[index].userTag?[0] ?? User.init(json: ""), Key.TypeReplyComment)
                }
            }else{
                self.tapUser(index, self.replyComments[index].user ?? User.init(json: ""), Key.TypeReplyComment)
            }
        }
        
        cell.constrainHeightTbvCommentReply.constant = 0
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        constrainHeightTbvCommentReply.constant = tbvComment.contentSize.height
        heightDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath]
        return height ?? UITableView.automaticDimension
      //  return UITableView.automaticDimension
    }
}

