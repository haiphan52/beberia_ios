//
//  NewHotTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 9/26/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit

class NewHotTableViewCell: UITableViewCell {
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDecs: UILabel!
   // @IBOutlet weak var lblDateCreate: UILabel!
    @IBOutlet weak var lblNumberComment: UILabel!
    @IBOutlet weak var lblNumberLike: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        viewContainer.dropShadow(offsetX: -0.5, offsetY: 0.5, color: UIColor.gray, opacity: 0.3, radius: 5)
    }

    func setupData(category: PostDetailModel){
        lblNumberComment.text = "\(category.commentNumber ?? 0)"
        lblNumberLike.text = "\(category.likeNumber ?? 0)"
        imgPost.image = UIImage.init(named: Key.ImagePlaceholder)
        imgPost.kf.setImage(with: URL.init(string: category.urlImage ?? "") ,placeholder: UIImage.init(named: Key.ImagePlaceholder))
        lblTitle.text = category.title ?? ""
       // lblDateCreate.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(category.created ?? 0)), format: "dd/MM/yy")
        
      //  lblDecs.text = category.quote ?? ""
        lblDecs.text = "adgf skahdsjh jksh kjsdfuiwa âhjkshd kjashdjkahdjkayiuqw djk hkadjdhku yqwr akfhkjdsfuyue ỳkuhf"
    }
    
    func setupDataNewsHot(category: Diaries){
        lblNumberComment.text = "\(category.commentNumber ?? 0)"
        lblNumberLike.text = "\(category.likeNumber ?? 0)"
//        imgPost.image = UIImage.init(named: Key.ImagePlaceholder)
        imgPost.kf.setImage(with: URL.init(string: category.media?[0].link ?? "") ,placeholder: UIImage.init(named: Key.ImagePlaceholder))
        imgLike.image = R.image.ic_like()
        if category.isLiked == 1 {
            imgLike.image = R.image.icLikeActive()
        }
        lblTitle.text = category.title ?? ""
    //    lblDateCreate.text = Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(category.created ?? 0)), format: "dd/MM/yy")
        lblDecs.text = category.quote ?? ""
       // lblDecs.text = "adgf skahdsjh jksh kjsdfuiwa âhjkshd kjashdjkahdjkayiuqw djk hkadjdhku yqwr akfhkjdsfuyue ỳkuhf"
    }
    
}
