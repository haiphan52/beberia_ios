
//
//  
//  CheckStoryboardVC.swift
//  Beberia
//
//  Created by haiphan on 08/09/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//
//
import UIKit
import RxCocoa
import RxSwift

class CheckStoryboardVC: UIViewController {
    
    // Add here outlets
    
    // Add here your view model
    private var viewModel: CheckStoryboardVM = CheckStoryboardVM()
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
}
extension CheckStoryboardVC {
    
    private func setupUI() {
        // Add here the setup for the UI
    }
    
    private func setupRX() {
        // Add here the setup for the RX
    }
}
