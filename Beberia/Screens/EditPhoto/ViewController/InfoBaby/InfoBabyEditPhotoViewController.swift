//
//  InfoBabyEditPhotoViewController.swift
//  Beberia
//
//  Created by IMAC on 10/8/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class InfoBabyEditPhotoViewController: BaseViewController {
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var weightBabyTf: UITextField!
    @IBOutlet weak var heightBabyTf: UITextField!
    @IBOutlet weak var ageBabyTf: UITextField!
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var btnSelectDate: UIButton!
    @IBOutlet weak var nameBabyTf: UITextField!
    @IBOutlet weak var nameBabyLable: UILabel!
    @IBOutlet weak var btnSelectBaby: UIButton!
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var viewDate: UIView!
    
    var infoBabyEditPhoto:(_ name: String, _ weight: String, _ height: String, _ age: String, _ date: String)->() = {_,_,_,_,_ in}
    let babys =  UserInfo.shareUserInfo.baby_info.filter({ $0.ageRange != 0 &&  $0.ageRange != 6})
    var infoBabe: (String, String , String, String) = ("","","","")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        customLeftBarButton()
        title = "Thông tin bé"
        
        viewDate.roundCorners([.topLeft, .bottomLeft], radius: 10)
        if babys.count > 0 {
            imgBaby.kf.setImage(with: URL.init(string: babys[0].avatar ?? ""))
            nameBabyTf.text = babys[0].nickname ?? ""
            nameBabyLable.text = babys[0].nickname ?? ""
        }
        dateLable.text = Utils.dateToString(date: Date(), format: "dd/MM/yyyy")
        
        self.heightBabyTf.text = infoBabe.0
        self.weightBabyTf.text = infoBabe.1
        self.ageBabyTf.text = infoBabe.2
        self.nameBabyTf.text = infoBabe.3
        
        initRx()
    }
    
    func initRx(){
        btnSelectDate.rx.tap.subscribe { (_) in
            let viewCalendar = ViewCalendar.instanceFromNib()
            viewCalendar.titlePopup.text = "Chọn ngày tải lên"
            viewCalendar.fsCalendar.appearance.selectionColor = UIColor.init(hexString: "ff99bb")
            viewCalendar.btnOK.backgroundColor = UIColor.init(hexString: "ff99bb")
            viewCalendar.fsCalendar.appearance.headerTitleColor = UIColor.init(hexString: "ff99bb")
            viewCalendar.titlePopup.backgroundColor = UIColor.init(hexString: "ff99bb")
            let popup = self.showPopup(view: viewCalendar, height: 360)
            viewCalendar.tapOK = { date in
                print(date)
                self.dateLable.text = date
                popup.dismiss(animated: true)
            }
        }.disposed(by: disposeBag)
        
        btnSelectBaby.rx.tap.subscribe { (_) in
            let view =  ViewSelectedBaby.instanceFromNib(isCheckBaby: true)
            view.viewTitle.backgroundColor = UIColor.init(hexString: "ff99bb")
            view.selectBabyLable.textColor = UIColor.init(hexString: "ffffff")
            view.babys.removeLast()
            view.babies.accept(view.babys)
            let popup = self.showPopup(view: view, height: 300)
            
            view.didSelectedBaby = { baby in
                self.nameBabyLable.text = baby.nickname ?? ""
                self.nameBabyTf.text = baby.nickname ?? ""
                self.imgBaby.kf.setImage(with: URL.init(string: baby.avatar ?? ""))
                popup.dismiss(animated: true)
            }
        }.disposed(by: disposeBag)
        
        btnUpdate.rx.tap.subscribe { (_) in
            guard let name = self.nameBabyTf.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), name.count > 0 else{
                Utils.showAlertOKWithoutAction(controller: self, title: "Thông báo", message: "Vui lòng nhập tên bé")
                return
            }
            guard let ageBaby = self.ageBabyTf.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), ageBaby.count > 0 else{
                Utils.showAlertOKWithoutAction(controller: self, title: "Thông báo", message: "Vui lòng nhập tuổi bé")
                return
            }
            guard let heightBaby = self.heightBabyTf.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), heightBaby.count > 0 else{
                Utils.showAlertOKWithoutAction(controller: self, title: "Thông báo", message: "Vui lòng nhập chiều cao bé")
                return
            }
            guard let weightBaby = self.weightBabyTf.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), weightBaby.count > 0 else{
                Utils.showAlertOKWithoutAction(controller: self, title: "Thông báo", message: "Vui lòng nhập cân nặng bé")
                return
            }
            
            self.infoBabyEditPhoto(name, weightBaby, heightBaby, ageBaby, self.dateLable.text ?? "")
            self.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
    }


}
