//
//  FrameCollectionViewCell.swift
//  Beberia
//
//  Created by IMAC on 9/30/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class FrameCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameFrameLable: UILabel!
    @IBOutlet weak var imgFrame: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
