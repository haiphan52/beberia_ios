//
//  EditPhotoHomeViewController.swift
//  Beberia
//
//  Created by IMAC on 9/30/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

enum TypeEditPhoto {
    case Filter
    case Frame
    case Sticker
}
struct ItemTopicPhoto {
    var image: UIImage
    var title: String
    var isSelect: Bool = false
    var imageActive: UIImage? = nil
}

struct FramePhoto {
    var title: String
    var isSelect: Bool = false
}

struct FilterImage {
    var image: UIImage
    var name: String
    var isSelect: Bool = false
}

import UIKit
import RxCocoa
import RxSwift
import SwiftEntryKit

class EditPhotoHomeViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var indicatorLoadding: UIActivityIndicatorView!
    @IBOutlet weak var clvTopicStickers: UICollectionView!
    @IBOutlet weak var viewStyleText: ViewTextStyle!
    @IBOutlet weak var bottomConstrainsViewTextStyle: NSLayoutConstraint!
    @IBOutlet weak var ratioActiveVew: NSLayoutConstraint!
    @IBOutlet weak var btnEditDone: UIButton!
    @IBOutlet weak var clvImagesFrame: UICollectionView!
    @IBOutlet weak var imgFame: UIImageView!
    @IBOutlet weak var imgSelect: UIImageView!
    @IBOutlet weak var activeView: UIView!
    @IBOutlet weak var clvTopicEditPhoto: UICollectionView!
    
    var typeEditPhoto = TypeEditPhoto.Filter
    {
        didSet{
            
        }
    }
    var infoBabe: (String, String , String, String) = ("","","","")
    var textViewTemp = UITextView()
    var disposeBag = DisposeBag()
    private var gesture = ""
    private var _selectedStickerView:StickerView?
    var selectedStickerView:StickerView? {
        get {
            return _selectedStickerView
        }
        set {
            // if other sticker choosed then resign the handler
            if _selectedStickerView != newValue {
                if let selectedStickerView = _selectedStickerView {
                    selectedStickerView.showEditingHandlers = false
                }
                _selectedStickerView = newValue
            }
            // assign handler to new sticker added
            if let selectedStickerView = _selectedStickerView {
                selectedStickerView.showEditingHandlers = true
                selectedStickerView.superview?.bringSubviewToFront(selectedStickerView)
            }
        }
    }
    
    private var imageFrames = BehaviorRelay.init(value: [String]())
    private var items = BehaviorRelay.init(value: [ItemTopicPhoto]())
    private var thumbnailFilterImages = BehaviorRelay.init(value: [FilterImage]())
    private var imageLoadFromLocal = Utils.loadFrameImagesFromAlbum(hasPrefix: "Frame")
    
    private var arrayThumbnail = [FilterImage]()
    private var viewInfoBaby = ViewInfoBaby()
    private var stickerViewInfoBaby: StickerView? = nil

    var isCheckRoomImage = false
    var isCheckFrame = false
    var isCheckFilter = false
    var isDiary = false
    var photoSelect = UIImage()
    var thumbnailImage = CIImage()
    var editPhotoDone:((UIImage, Bool))->() = {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customLeftBarButton()
        title = "Chỉnh sửa ảnh"
        imgSelect.image = photoSelect
        imgSelect.transform = CGAffineTransform.identity
        imgSelect.frame = imgFame.bounds
        
        dummyData()
        initRx()
        setGestureImageSelect()
        
        imageLoadFromLocal.sort()
        imageLoadFromLocal.insert("none", at: 0)
        
    //    self.btnEditDone.setImage(R.image.editPhotoDone()!, for: .normal)
        viewStyleText.isHidden = true
        clvImagesFrame.isHidden = true
        clvTopicStickers.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        self.indicatorLoadding.isHidden = true
        DispatchQueue.global(qos: .background).async {
            self.arrayThumbnail = FilterImageManager.share.getThumbnailImages(thumbnailImage: self.thumbnailImage)
            self.arrayThumbnail[0].isSelect = true
            
            DispatchQueue.main.async {
                self.thumbnailFilterImages.accept(self.arrayThumbnail)
                self.indicatorLoadding.isHidden = true
                self.indicatorLoadding.stopAnimating()
            }
            
        }
        
        viewStyleText.actionSelectTextStyle = { action in
            switch action {
            case .aligment(let aligment):
                self.textViewTemp.textAlignment = aligment
                
            case .font(let fontText):
                self.textViewTemp.font = UIFont(name: fontText.font, size: 30)
                
            case .color(let color):
                self.textViewTemp.textColor = UIColor.init(hexString: color)
                
            case .background(let isBackground):
                self.textViewTemp.backgroundColor = isBackground ? UIColor.init(hexString: "ffffff", alpha: 0.5) : UIColor.clear
            }
        }

        customRightBarButtonWithText(title: "Xong")
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.init(hexString: "ff99bb")
        
    }
    
    @objc func keyboardWasShown(notification: NSNotification) {

        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue

        UIView.animate(withDuration: 0.25) {
            self.viewStyleText.isHidden = false
            self.bottomConstrainsViewTextStyle.constant =  keyboardRect!.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {

        UIView.animate(withDuration: 0.25) {
            self.viewStyleText.isHidden = true
            self.bottomConstrainsViewTextStyle.constant = 0
        }
    }
    
    func customRightBarButtonWithText(title: String){
           let leftBarButtonItem = UIBarButtonItem.init(title: title, style: .done, target: self, action: #selector(EditPhotoHomeViewController.actionRightBar))
           self.navigationItem.rightBarButtonItem?.tintColor = .red
           self.navigationItem.rightBarButtonItem = leftBarButtonItem
    }
    
    @objc func actionRightBar(){
        self.saveImage()
    }
    
    func isEqualImages(image1: UIImage, image2: UIImage) -> Bool {
        let data1: Data? = image1.pngData()
        let data2: Data? = image2.pngData()
        return data1 == data2
    }
    
    func customLeftBarButton(){
        let leftBarButtonItem = UIBarButtonItem.menuButton(self, action: #selector(EditPhotoHomeViewController.backVC), imageName: R.image.ic_Back() ?? UIImage.init(), height: 30, width: 40, view: self.view)
        
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc func backVC(){
        if isDiary {
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    private func dummyData(){
        var items = [ItemTopicPhoto]()
        
        items.append(contentsOf: [ItemTopicPhoto.init(image: R.image.editPhotoFilter()!, title: "Bộ lọc", imageActive: R.image.editPhotoFillterActive()), ItemTopicPhoto.init(image: R.image.editphotoFrame()!, title: "Khung ảnh", imageActive: R.image.editPhotoFrameActive()), ItemTopicPhoto.init(image: R.image.editphotoSticker()!, title: "Sticker"), ItemTopicPhoto.init(image: R.image.editphotoText()!, title: "Văn bản"), ItemTopicPhoto.init(image: R.image.editphotoInfoBaby()!, title: "Thông tin bé")])
        
        self.items.accept(items)
    }
    
    private final func initRx(){
        // MARK:  CollectionView Topic Edit Photo
        let flowLayoutTocpicEditPhoto = UICollectionViewFlowLayout()
        let widthCell = ((UIScreen.main.bounds.width - 16 - 28) / 5)
        flowLayoutTocpicEditPhoto.itemSize = CGSize(width: widthCell , height: 100)
        flowLayoutTocpicEditPhoto.scrollDirection = .horizontal
        clvTopicEditPhoto.setCollectionViewLayout(flowLayoutTocpicEditPhoto, animated: true)
        clvTopicEditPhoto.showsVerticalScrollIndicator = false
        clvTopicEditPhoto.register(R.nib.topicEditCollectionViewCell)
        clvTopicEditPhoto.clipsToBounds = false
        clvTopicEditPhoto.layer.masksToBounds = false
        
        
        self.items.asObservable().bind(to: self.clvTopicEditPhoto.rx.items(cellIdentifier: R.reuseIdentifier.topicEditCollectionViewCell.identifier, cellType: TopicEditCollectionViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
            cell.titleTopic.text = data.title
            cell.imgTopic.image = data.image
            if data.isSelect {
                cell.imgTopic.image = data.imageActive
            }
        }.disposed(by: disposeBag)
        
        
        clvTopicEditPhoto.rx.modelSelected(ItemTopicPhoto.self).observeOn(MainScheduler.asyncInstance).subscribe { [self] (item) in
            switch item.element!.title {
            case "Bộ lọc":
                self.typeEditPhoto = .Filter
                self.thumbnailFilterImages.accept(self.arrayThumbnail)
                self.clvTopicStickers.isHidden = false
                self.clvImagesFrame.isHidden = true
              //  self.clvTopicEditPhoto.isHidden = true
              //  self.btnEditDone.setImage(R.image.editPhotoDone()!, for: .normal)
                if self.thumbnailFilterImages.value.count > 0{
                    self.indicatorLoadding.isHidden = true
                    self.indicatorLoadding.stopAnimating()
                }else{
                    self.indicatorLoadding.isHidden = false
                    self.indicatorLoadding.startAnimating()
                }
                
                
                var itemsTopic = items.value
                itemsTopic[0].isSelect = false
                itemsTopic[1].isSelect = false
                itemsTopic[0].isSelect = true
                self.items.accept(itemsTopic)
                
            case "Khung ảnh":
                self.indicatorLoadding.isHidden = true
                self.typeEditPhoto = .Frame
                self.clvImagesFrame.isHidden = false
             //   self.clvTopicEditPhoto.isHidden = true
                self.clvTopicStickers.isHidden = true
             //   self.btnEditDone.setImage(R.image.editPhotoDone()!, for: .normal)
                self.imageFrames.accept(self.imageLoadFromLocal)
                
                
                var itemsTopic = items.value
                itemsTopic[0].isSelect = false
                itemsTopic[1].isSelect = false
                itemsTopic[1].isSelect = true
                self.items.accept(itemsTopic)
                
            case "Sticker":
                self.typeEditPhoto = .Sticker
                let viewSticker = StickerViewController.init(nib: R.nib.stickerViewController)
                viewSticker.selectTicker = { nameImage in
                    self.dismiss(animated: true) {
                        self.addSticker(image: nameImage)
                    }
                }
                self.present(viewSticker, animated: true, completion: nil)
                
            case "Văn bản":
                self.addTextView()
                
            case "Thông tin bé":
                let infoBabyVC = InfoBabyEditPhotoViewController.init(nib: R.nib.infoBabyEditPhotoViewController)
                infoBabyVC.infoBabyEditPhoto = { name, weightBaby, heightBaby, ageBaby, date in
                    self.addInfoBaby(name, weightBaby, heightBaby, ageBaby, date)
                }

                infoBabyVC.infoBabe = self.infoBabe

                self.navigationController?.pushViewController(infoBabyVC, animated: true)
                
            default:
                print("!21121221")
            }
        }.disposed(by: disposeBag)
        
        
        
        // MARK:  CollectionView Image Frame
        let flowLayoutImageFrame = UICollectionViewFlowLayout()
        let widthCell1 = ((UIScreen.main.bounds.width - 0) / 4)
        flowLayoutImageFrame.itemSize = CGSize(width: widthCell1 , height: 120)
        flowLayoutImageFrame.scrollDirection = .horizontal
        flowLayoutImageFrame.minimumInteritemSpacing = 0
        flowLayoutImageFrame.minimumLineSpacing = 0
        flowLayoutImageFrame.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvImagesFrame.setCollectionViewLayout(flowLayoutImageFrame, animated: true)
        clvImagesFrame.showsVerticalScrollIndicator = false
        clvImagesFrame.register(R.nib.frameCollectionViewCell)
        clvImagesFrame.clipsToBounds = false
        clvImagesFrame.layer.masksToBounds = false
        
        
        self.imageFrames.asObservable().bind(to: self.clvImagesFrame.rx.items(cellIdentifier: R.reuseIdentifier.frameCollectionViewCell.identifier, cellType: FrameCollectionViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
         
            cell.nameFrameLable.textColor = UIColor.init(hexString: "cecece")
            cell.imgFrame.image = UIImage.init(named: data)
            var nameImage = data.replace(string: "Frame_", replacement: "")
            nameImage = nameImage.replace(string: ".png", replacement: "")
            cell.nameFrameLable.text = nameImage.replace(string: "H_", replacement: "")
        }.disposed(by: disposeBag)
        
        
        
        Observable
            .zip(clvImagesFrame.rx.itemSelected, clvImagesFrame.rx.modelSelected(String.self))
            .bind { [weak self] indexPath, item in
                guard let self = self else { return }
                
                switch self.typeEditPhoto{
                case .Filter:
                  //  self.imgSelect.image = self.photoSelect.addFilter(filterType: FilterType.allCases[indexPath.row])
                    
                    print("121212")
                case .Frame:
                     self.imageFrames.accept(self.imageLoadFromLocal)
                    
                    self.isCheckFrame = indexPath.row == 0 ? false : true
                    
                    self.clvImagesFrame.layoutIfNeeded()

                    if let cell = self.clvImagesFrame.cellForItem(at: indexPath) as? FrameCollectionViewCell{
                        cell.nameFrameLable.textColor = .black
                    }
                    
                    self.imgFame.image = UIImage.init(named: item)
                     
                     if item.contains("Frame_H_"){
                        self.setConstrainsRatio(ratio: 2)
                     }else{
                        self.setConstrainsRatio(ratio: 1)
                     }
                    
                     if indexPath.row == 0 { self.imgFame.image = UIImage.init() }
                    
                case .Sticker:
                    print("121212")
                }
                
                
                
                
        }.disposed(by: disposeBag)
        
        // MARK:  CollectionView Image Filter
        let flowLayoutImageFilter = UICollectionViewFlowLayout()
        let widthCellFilter = ((UIScreen.main.bounds.width - 0) / 4)
        flowLayoutImageFilter.itemSize = CGSize(width: widthCellFilter , height: 120)
        flowLayoutImageFilter.scrollDirection = .horizontal
        flowLayoutImageFilter.minimumInteritemSpacing = 0
        flowLayoutImageFilter.minimumLineSpacing = 0
        flowLayoutImageFilter.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvTopicStickers.setCollectionViewLayout(flowLayoutImageFilter, animated: true)
        clvTopicStickers.showsVerticalScrollIndicator = false
        clvTopicStickers.register(R.nib.frameCollectionViewCell)
        clvTopicStickers.clipsToBounds = false
        clvTopicStickers.layer.masksToBounds = false
        
        
        self.thumbnailFilterImages.asObservable().bind(to: self.clvTopicStickers.rx.items(cellIdentifier: R.reuseIdentifier.frameCollectionViewCell.identifier, cellType: FrameCollectionViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
         
            cell.nameFrameLable.textColor = UIColor.init(hexString: "cecece")
            cell.imgFrame.image = data.image
            cell.nameFrameLable.text = data.name
            if data.isSelect { cell.nameFrameLable.textColor = .black }
        }.disposed(by: disposeBag)
        
        
        
        Observable
            .zip(clvTopicStickers.rx.itemSelected, clvTopicStickers.rx.modelSelected(FilterImage.self))
            .bind { [weak self] indexPath, item in
                guard let self = self else { return }
                if indexPath.row != 0 {
                    self.imgSelect.image = FilterImageManager.share.applyFilter(
                    at: indexPath.row, image: self.photoSelect)
                }else{
                    self.imgSelect.image = self.photoSelect
                }
                
                self.isCheckFilter = indexPath.row == 0 ? false : true
                
                for (index, _) in self.arrayThumbnail.enumerated() {
                    self.arrayThumbnail[index].isSelect = false
                }

                self.arrayThumbnail[indexPath.row].isSelect = true
                self.thumbnailFilterImages.accept(self.arrayThumbnail)
                
                
        }.disposed(by: disposeBag)
        
        
        
//        btnEditDone.rx.tap.subscribe { (_) in
//            if (self.btnEditDone.currentImage == R.image.editPhotoDone()!) {
//                self.clvImagesFrame.isHidden = true
//                self.clvTopicStickers.isHidden = true
//                self.clvTopicEditPhoto.isHidden = false
//                self.btnEditDone.setImage(R.image.editPhotoDownload()!, for: .normal)
//                self.indicatorLoadding.isHidden = true
//                self.indicatorLoadding.stopAnimating()
//            }else{
//                print("Download image")
//
//                self.saveImage()
//            }
//        }.disposed(by: disposeBag)
        
    }
    
    func saveImage(){
        self.selectedStickerView?.showEditingHandlers = false
        if let image = self.mergeImages(imageView: self.imgFame){
            
            if self.activeView.subviews.count == 2 && !self.isCheckFilter && !self.isCheckFrame && !isCheckRoomImage{
                if isDiary{
                    self.editPhotoDone((image, false))
                    self.dismiss(animated: true, completion: nil)
                }
            }else{
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
            }

        }else{
            print("Image not found !!")
        }
    }
    
    func mergeImages(imageView: UIImageView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(imageView.frame.size, false, 0.0)
        imageView.superview!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
      
    func setConstrainsRatio(ratio: CGFloat){
        let newConstraint = ratioActiveVew.constraintWithMultiplier(ratio)
        view.removeConstraint(ratioActiveVew)
        view.addConstraint(newConstraint)
        view.layoutIfNeeded()
        ratioActiveVew = newConstraint
    }
    
    @IBAction func didSetColor(_ sender: Any) {
        textViewTemp.textColor = .red
    }
    
    @IBAction func didPressTextAli(_ sender: Any) {
        textViewTemp.textAlignment = .left
    }
    
    //MARK: - Save Image callback
       
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            print(error.localizedDescription)
        } else {
            if isDiary{
                self.editPhotoDone((image, true))
                self.dismiss(animated: true, completion: nil)
            }else{
                print("save image success")
                let view = ViewToastPopup.instanceFromNib()
                view.titleLable.text = "Lưu ảnh thành công!"
                
                var attributes = EKAttributes.topFloat
                attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 12, offset: .zero))
                attributes.displayDuration = 3
                attributes.position = .top
                SwiftEntryKit.display(entry: view, using: attributes)
            }
        }
    }
    
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

// MARK: Set Farme
extension EditPhotoHomeViewController{
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
     func setGestureImageSelect(){
        let panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(handlePanGesture(_:)))
        panGesture.delegate = self
        
        let pinchGesture = UIPinchGestureRecognizer.init(target: self, action: #selector(handlePinchGesture(_:)))
        pinchGesture.delegate = self
        
        let  rotateGesture = UIRotationGestureRecognizer.init(target: self, action: #selector(handleRotateGesture(_:)))
        rotateGesture.delegate = self
        
        imgSelect.addGestureRecognizer(panGesture)
        imgSelect.addGestureRecognizer(pinchGesture)
        imgSelect.addGestureRecognizer(rotateGesture)
    }
    
    @objc func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
        
        let translation = recognizer.translation(in: recognizer.view)
        recognizer.view?.transform = (recognizer.view?.transform.translatedBy(x: translation.x, y: translation.y))!
        recognizer.setTranslation(.zero, in: recognizer.view)
        isCheckRoomImage = true
    }
    
    @objc func handlePinchGesture(_ recognizer: UIPinchGestureRecognizer) {
        recognizer.view?.transform = (recognizer.view?.transform.scaledBy(x: recognizer.scale, y: recognizer.scale))!
        recognizer.scale = 1
        isCheckRoomImage = true
    }
    
    @objc func handleRotateGesture(_ recognizer: UIRotationGestureRecognizer) {
        
        recognizer.view?.transform = (recognizer.view?.transform.rotated(by: recognizer.rotation))!
        recognizer.rotation = 0
        isCheckRoomImage = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      //  print("Adadasd")
        self.view.endEditing(true)
        
    }
    
    func updateTextFont(textView: UITextView) {
         if (textView.text.isEmpty || textView.bounds.size.equalTo(CGSize.zero)) {
            return
         }
         let textViewSize = textView.frame.size
         let fixedWidth = textViewSize.width
         let expectSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT)))
         var expectFont = textView.font;
         if (expectSize.height > textViewSize.height) {
            while (textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT))).height > textViewSize.height) {
                expectFont = textView.font!.withSize(textView.font!.pointSize - 1)
                textView.font = expectFont
            }
         }
         else {
            while (textView.sizeThatFits(CGSize(width: fixedWidth,height: CGFloat(MAXFLOAT))).height < textViewSize.height) {
                expectFont = textView.font
                textView.font = textView.font!.withSize(textView.font!.pointSize + 1)
            }
            textView.font = expectFont
         }
     }
}

// MARK: Set Sticker
extension EditPhotoHomeViewController : StickerViewDelegate {
    
    func addSticker(image: UIImage){
        gesture = "Stickers"
        let testImage = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
        testImage.image = image
        testImage.contentMode = .scaleAspectFit
        let stickerView3 = StickerView.init(contentView: testImage)
        stickerView3.center = CGPoint.init(x: 150, y: 150)
        stickerView3.delegate = self
        stickerView3.outlineBorderColor = .white
        stickerView3.setImage(R.image.editStickerClose()!, forHandler: StickerViewHandler.close)
        stickerView3.setImage(R.image.editStickerRoom()!, forHandler: StickerViewHandler.rotate)
        stickerView3.setImage(R.image.editStickerRotate()!, forHandler: StickerViewHandler.flip)
        stickerView3.showEditingHandlers = false
        stickerView3.tag = 999
        self.activeView.addSubview(stickerView3)
        self.selectedStickerView = stickerView3
    }
    
    func addTextView(){
        gesture = "TextView"
        let textView = UITextView(frame: CGRect(x: 0, y: activeView.center.y,
                                                width: 120, height: 40))
      //  textView.text.a
        textView.textColor = .white
        textView.textAlignment = .left
        textView.font = UIFont(name: FontText.SVNAbrilFatface.font, size: 30)
        textView.backgroundColor = .clear
        textView.becomeFirstResponder()
        //  textView.textColor = textColor
        let stickerView3 = StickerView.init(contentView: textView)
        stickerView3.center = CGPoint.init(x: 150, y: 150)
        stickerView3.delegate = self
        stickerView3.outlineBorderColor = .white
        stickerView3.setImage(R.image.editStickerClose()!, forHandler: StickerViewHandler.close)
        stickerView3.setImage(R.image.editStickerRoom()!, forHandler: StickerViewHandler.rotate)
        stickerView3.setImage(R.image.editStickerRotate()!, forHandler: StickerViewHandler.flip)
        stickerView3.showEditingHandlers = false
        stickerView3.tag = 998
        self.activeView.addSubview(stickerView3)
        self.selectedStickerView = stickerView3
        
        textViewTemp = stickerView3.contentView as! UITextView
        viewStyleText.setStatusView(textView: textView)

    }
    
    func getWidthText(texts: [String])->CGFloat{
        var width:CGFloat = 0
        for text in texts {
            width += text.widthOfString(usingFont:  UIFont.init(name: R.font.iCielCadena.fontName, size: 17)!)
        }
        return width
    }
    
    func addInfoBaby(_ name: String, _ weight: String, _ height: String, _ age: String, _ date: String){
          gesture = "InfoBaby"
        viewInfoBaby = ViewInfoBaby.instanceFromNib()
        viewInfoBaby.ageBaby.text = age
        viewInfoBaby.nameBaby.text = "\(name)"
        viewInfoBaby.heightBaby.text = "\(height) cm"
        viewInfoBaby.weightLable.text = "\(weight) kg"
        viewInfoBaby.dateLable.text = "\(date)"
        
        infoBabe = (height,weight,age,name)
        
        let width = getWidthText(texts: [viewInfoBaby.ageBaby.text ?? "", viewInfoBaby.heightBaby.text ?? "", viewInfoBaby.weightLable.text ?? ""]) + 70

        if stickerViewInfoBaby == nil {
            
            stickerViewInfoBaby = StickerView.init(contentView: viewInfoBaby)
            stickerViewInfoBaby?.frame = CGRect.init(x: 0, y: 0, width: width, height: 120)
            stickerViewInfoBaby!.center = CGPoint.init(x: 150, y: 150)
            stickerViewInfoBaby!.delegate = self
            stickerViewInfoBaby?.outlineBorderColor = .clear
            stickerViewInfoBaby!.setImage(R.image.editStickerClose()!, forHandler: StickerViewHandler.close)
            stickerViewInfoBaby!.setImage(R.image.editStickerRoom()!, forHandler: StickerViewHandler.rotate)
            stickerViewInfoBaby!.showEditingHandlers = false
            stickerViewInfoBaby!.tag = 997
            self.activeView.addSubview(stickerViewInfoBaby!)
            self.selectedStickerView = stickerViewInfoBaby
            viewInfoBaby.roundCorners([.topLeft, .bottomLeft, .topRight], radius: 35)
            viewInfoBaby.weightLable.fitFontForSize()
            viewInfoBaby.heightBaby.fitFontForSize()
            viewInfoBaby.ageBaby.fitFontForSize()
        }else{
            stickerViewInfoBaby!.removeFromSuperview()
            stickerViewInfoBaby = StickerView.init(contentView: viewInfoBaby)
            stickerViewInfoBaby?.frame = CGRect.init(x: 0, y: 0, width: width, height: 120)
            stickerViewInfoBaby!.center = CGPoint.init(x: 150, y: 150)
            stickerViewInfoBaby!.delegate = self
            stickerViewInfoBaby?.outlineBorderColor = .clear
            stickerViewInfoBaby!.setImage(R.image.editStickerClose()!, forHandler: StickerViewHandler.close)
            stickerViewInfoBaby!.setImage(R.image.editStickerRoom()!, forHandler: StickerViewHandler.rotate)
            stickerViewInfoBaby!.showEditingHandlers = false
            stickerViewInfoBaby!.tag = 997
            self.activeView.addSubview(stickerViewInfoBaby!)
            self.selectedStickerView = stickerViewInfoBaby
            viewInfoBaby.roundCorners([.topLeft, .bottomLeft, .topRight], radius: 35)
            viewInfoBaby.weightLable.fitFontForSize()
            viewInfoBaby.heightBaby.fitFontForSize()
            viewInfoBaby.ageBaby.fitFontForSize()
        }
       

      }
    
    func stickerViewDidTap(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
        gesture = "Stickers"

        if stickerView.contentView.isKind(of: UITextView.self) {
            textViewTemp = stickerView.contentView as! UITextView
            textViewTemp.becomeFirstResponder()
            viewStyleText.setStatusView(textView: textViewTemp)

        }else{
            self.view.endEditing(true)
        }
    }
    
    func stickerViewDidBeginMoving(_ stickerView: StickerView) {
        self.selectedStickerView = stickerView
        gesture = "Stickers"
    }
    
    func stickerViewDidChangeMoving(_ stickerView: StickerView) {
        print("hnjknmádasdasd,")
    }
    
    func stickerViewDidEndMoving(_ stickerView: StickerView) {
        gesture = ""
        self.selectedStickerView?.showEditingHandlers = false
    }
    
    func stickerViewDidBeginRotating(_ stickerView: StickerView) {
        
    }
    
    func stickerViewDidChangeRotating(_ stickerView: StickerView) {
     //   print("hnjknm,")
        if stickerView.contentView.isKind(of: UITextView.self) {
            textViewTemp = stickerView.contentView as! UITextView
            self.updateTextFont(textView: textViewTemp)
        }
        
        if stickerView.contentView.isKind(of: ViewInfoBaby.self) {
            let view = stickerView.contentView as! ViewInfoBaby
            view.roundCorners([.topLeft, .bottomLeft, .topRight], radius: (stickerView.contentView.frame.height / 2) - 1 )
            view.backgroundColor = .white

            view.nameBaby.fitFontForSize()
            view.weightLable.fitFontForSize()
            view.heightBaby.fitFontForSize()
            view.ageBaby.fitFontForSize()
            view.dateLable.fitFontForSize()
        }
    }
    
    func stickerViewDidEndRotating(_ stickerView: StickerView) {
        gesture = ""
        self.selectedStickerView?.showEditingHandlers = false
    }
    
    func stickerViewDidClose(_ stickerView: StickerView) {
        if stickerView.contentView.isKind(of: ViewInfoBaby.self) {
            infoBabe  = ("","","","")
        }
    }
}
