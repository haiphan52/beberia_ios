//
//  ViewTextStyle.swift
//  Beberia
//
//  Created by IMAC on 10/3/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

enum FontText:String, CaseIterable {
    
    case Pony
    case BreathePro
    case DTHMdeSade
    case Hipsteria
    case iCielCadena
    case SVNAbrilFatface
    case SVNCookie
    case SVNCookies
    case SVNLobster
    case SVNPoky_s
    
    var font: String{
        switch self {
        case .Hipsteria:
            return R.font.iCielHipsteria.fontName
        case .Pony:
            return R.font.iCielPony.fontName
        case .BreathePro:
            return R.font.breathePro.fontName
        case .DTHMdeSade:
            return R.font.dthMdeSade.fontName
        case .iCielCadena:
            return R.font.iCielCadena.fontName
        case .SVNAbrilFatface:
            return R.font.svnAbrilFatface.fontName
        case .SVNCookie:
            return R.font.svnCookie.fontName
        case .SVNCookies:
            return R.font.svnCookies.fontName
        case .SVNLobster:
            return R.font.svnLobster.fontName
        case .SVNPoky_s:
            return R.font.svnPokyS.fontName
       }
    }
}

enum ActionText {
    case font(FontText)
    case aligment(NSTextAlignment)
    case color(String)
    case background(Bool)
}

class ViewTextStyle: UIView {
    
    let kCONTENT_XIB_NAME = "ViewTextStyle"
    var arrayTextStyle = BehaviorRelay.init(value: [UIImage]())
    var arrayColor = BehaviorRelay.init(value: [String]())
    
    @IBOutlet weak var heightClvColor: NSLayoutConstraint!
    @IBOutlet weak var widthClvTextStyle: NSLayoutConstraint!
    @IBOutlet weak var widthClvColor: NSLayoutConstraint!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var clvColor: UICollectionView!
    @IBOutlet weak var clvTextStyle: UICollectionView!
    
    var arrayAligments = [NSTextAlignment.left, NSTextAlignment.center, NSTextAlignment.right]
    var actionSelectTextStyle: (ActionText)->() = {_ in}
    var indexFont = FontText.allCases.startIndex
    var indexAligment = 0
    {
        didSet{
            arrayStyle = [R.image.editPhotoTextFont()!,R.image.editPhotoColorWheel()!,isBackground ? R.image.editPhotoAAActive()! : R.image.editPhotoAA()!,arrayAlignment[indexAligment]!]
            arrayTextStyle.accept(arrayStyle)
        }
    }
    var isBackground = false
    {
        didSet{
            arrayStyle = [R.image.editPhotoTextFont()!,R.image.editPhotoColorWheel()!,isBackground ? R.image.editPhotoAAActive()! : R.image.editPhotoAA()!,arrayAlignment[indexAligment]!]
            arrayTextStyle.accept(arrayStyle)
        }
    }
    
    var disposeBag = DisposeBag()
    var arrayStyle = [R.image.editPhotoTextFont()!,R.image.editPhotoColorWheel()!,R.image.editPhotoAA()!,R.image.editPhotoLeftAlign()!]
    let colors = ["ff9f00","66bb00","00cc71","0095ff","6b77e8","b760ea","ff193d","ffffff","000000"]
    var colorString = "000000"
    var arrayAlignment = [R.image.editPhotoLeftAlign()!,R.image.editPhotoCenterAlign()!,R.image.editPhotoRightAlign()]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
        setShowClvColor(isShow: false)
  
    }
    
    func setStatusView(textView: UITextView){
        isBackground = textView.backgroundColor == UIColor.init(hexString: "ffffff", alpha: 0.5) ? true : false
        
        switch textView.textAlignment {
        case .left:
            indexAligment = 0
            
        case .center:
            indexAligment = 1
            
        case .right:
            indexAligment = 2
            
        default:
            indexAligment = 0
        }
    }
    
    func nextFont() -> FontText{
        indexFont = indexFont.advanced(by: 1)
        indexFont = indexFont >= FontText.allCases.endIndex ? FontText.allCases.startIndex : indexFont

        return FontText.allCases[indexFont]
    }
    
    func nextAligment() -> NSTextAlignment{
        let indexTemp = indexAligment.advanced(by: 1)
        indexAligment = indexTemp >= arrayAligments.endIndex ? arrayAligments.startIndex : indexTemp
        
        return arrayAligments[indexAligment]
    }
    
    func setShowClvColor(isShow: Bool){
        UIView.animate(withDuration: 0.25) {
            self.heightClvColor.constant = isShow ? 35 : 0
            self.layoutIfNeeded()
        }
    }
    
    override func awakeFromNib() {
        
        arrayTextStyle.accept(arrayStyle)
        
        let height = 35
        widthClvTextStyle.constant = CGFloat((arrayStyle.count) * height) + CGFloat(15 * (arrayStyle.count - 1))
        // MARK:  CollectionView Image Frame
        let flowLayoutImageFrame = UICollectionViewFlowLayout()

        flowLayoutImageFrame.itemSize = CGSize(width: height , height: height + 15)
        flowLayoutImageFrame.scrollDirection = .horizontal
        flowLayoutImageFrame.minimumInteritemSpacing = 0
        flowLayoutImageFrame.minimumLineSpacing = 15
        flowLayoutImageFrame.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 15)
        clvTextStyle.setCollectionViewLayout(flowLayoutImageFrame, animated: true)
        clvTextStyle.showsVerticalScrollIndicator = false
        clvTextStyle.register(R.nib.textStyleCollectionViewCell)

        self.arrayTextStyle.asObservable().bind(to: self.clvTextStyle.rx.items(cellIdentifier: R.reuseIdentifier.textStyleCollectionViewCell.identifier, cellType: TextStyleCollectionViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
            // cell.nameFrameLable.text = data.title
            cell.imgTextStyle.image = data
       
        }.disposed(by: disposeBag)
        
        
        
        Observable
            .zip(clvTextStyle.rx.itemSelected, clvTextStyle.rx.modelSelected(UIImage.self))
            .bind { [weak self] indexPath, item in
                guard let self = self else { return }
                
                switch indexPath.row{
                case 0 :
                    self.actionSelectTextStyle(ActionText.font(self.nextFont()))
                case 1 :
                    self.setShowClvColor(isShow: true)
                case 2 :
                    self.isBackground = !self.isBackground
                    self.actionSelectTextStyle(ActionText.background(self.isBackground))
                case 3 :
                    self.actionSelectTextStyle(ActionText.aligment(self.nextAligment()))
                default:
                    print("Text Font")
                }
        }.disposed(by: disposeBag)
        
        
        // MARK:  CollectionView Image Frame
        
        arrayColor.accept(colors)
         
         let heightCell = 20
         widthClvColor.constant = CGFloat((colors.count) * heightCell) + CGFloat(8 * (colors.count - 1))
         
         let flowLayout = UICollectionViewFlowLayout()

         flowLayout.itemSize = CGSize(width: heightCell , height: heightCell)
         flowLayout.scrollDirection = .horizontal
         flowLayout.minimumInteritemSpacing = 0
         flowLayout.minimumLineSpacing = 8
         flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
         clvColor.setCollectionViewLayout(flowLayout, animated: true)
         clvColor.showsVerticalScrollIndicator = false
         clvColor.register(R.nib.colorCollectionViewCell)

         self.arrayColor.asObservable().bind(to: self.clvColor.rx.items(cellIdentifier: R.reuseIdentifier.colorCollectionViewCell.identifier, cellType: ColorCollectionViewCell.self)) { [weak self]  row, data, cell in
             guard let _ = self else { return }
             
            cell.viewColor.backgroundColor = UIColor.init(hexString: data)
            cell.layer.cornerRadius =  cell.frame.width / 2
         }.disposed(by: disposeBag)
         
         
         
         Observable
             .zip(clvColor.rx.itemSelected, clvColor.rx.modelSelected(String.self))
             .bind { [weak self] indexPath, item in
                 guard let self = self else { return }
                self.colorString = self.colors[indexPath.row]
                self.actionSelectTextStyle(ActionText.color(self.colorString))
         }.disposed(by: disposeBag)
    }
    
    
}
