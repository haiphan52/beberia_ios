//
//  SelectImageEditViewController.swift
//  Beberia
//
//  Created by IMAC on 9/30/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import Photos
import RxSwift
import RxCocoa
import PhotosUI


private extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map { $0.indexPath }
    }
}

class AlbumModel {
    var name:String
    var count:Int
    var collection:PHFetchResult<PHAsset>
    init(name:String, count:Int, collection:PHFetchResult<PHAsset>) {
      self.name = name
      self.count = count
      self.collection = collection
    }
  }

class SelectImageEditViewController: BaseViewController {
    
    @IBOutlet weak var tbvAlbums: UITableView!
    @IBOutlet weak var imgSelect: UIImageView!
    @IBOutlet weak var clvSelectImage: UICollectionView!
    
    private final var allPhotos : PHFetchResult<PHAsset>? = nil
    private final var allPhotoImages = BehaviorRelay.init(value: [UIImage]())
    private final var albums = BehaviorRelay.init(value: [AlbumModel]())
    private final var album = AlbumModel.init(name: "", count: 0, collection: PHFetchResult<PHAsset>())
    
    public var assetGroupTypes: [PHAssetCollectionSubtype] = [
        .smartAlbumUserLibrary,
        .smartAlbumFavorites,
        .albumRegular
    ]
    
    var fetchResult: PHFetchResult<PHAsset>!
    fileprivate let imageManager = PHCachingImageManager()
    fileprivate var thumbnailSize: CGSize!
    fileprivate var previousPreheatRect = CGRect.zero
    
    // Size of preview image
    var targetSize: CGSize!
    
    // currently previewed photo
    var previewedPhotoIndexPath: IndexPath!
    var previewedThumbnailData: CIImage!
    var previewedImageData: CIImage!
    var showViewSelectLibrary = true
    {
        didSet{
            self.hideShowViewSelectAlbum(isShow: showViewSelectLibrary)
            if showViewSelectLibrary {
                customRightBarButton(image: UIImage.init())
            }else{
                customRightBarButtonWithText(title: "Tiếp")
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customLeftBarButton()
        clvSelectImage.dataSource = self
        clvSelectImage.delegate = self
        clvSelectImage.register(R.nib.selectImageEditCollectionViewCell)
        getALlsPhoto()
        initRx()
        
        showViewSelectLibrary = true
        let viewtitle = ViewTitleLibrary.instanceFromNib()
        viewtitle.didPressLibrary = {
            self.showViewSelectLibrary = true
        }
        customTitleView(view: viewtitle)
    }
    
    func hideShowViewSelectAlbum(isShow: Bool){
        UIView.animate(withDuration: 0.3) {
            self.tbvAlbums.isHidden = !isShow
        }
    }
    
    override func actionRightBar() {
        guard let imageSelect = imgSelect.image else {
            return
        }
        guard let data = self.previewedThumbnailData else {
            return
        }
        let editPhotoHome = EditPhotoHomeViewController.init(nib: R.nib.editPhotoHomeViewController)
        editPhotoHome.photoSelect = imageSelect
        editPhotoHome.thumbnailImage =  data
        self.navigationController?.pushViewController(editPhotoHome, animated: true)
    }
    
   override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Determine the size of the thumbnails to request from the PHCachingImageManager
        let scale = UIScreen.main.scale
        
        let cellSize = (clvSelectImage.collectionViewLayout as! UICollectionViewFlowLayout).itemSize
        thumbnailSize = CGSize(width: cellSize.width * scale + 50, height: cellSize.height * scale + 50)

        targetSize = CGSize(width: imgSelect.bounds.width * scale, height: imgSelect.bounds.height * scale)
     
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           updateCachedAssets()
       }
       
       override func didReceiveMemoryWarning() {
           super.didReceiveMemoryWarning()
           // Dispose of any resources that can be recreated.
       }

    private final func initRx(){
        
        tbvAlbums.register(R.nib.albumsTableViewCell)
        tbvAlbums.tableFooterView = UIView()
        tbvAlbums.rowHeight = 145
       // tbvAlbums.rx.setDelegate(self).disposed(by: disposeBag)
        self.tbvAlbums.delegate = nil
        self.tbvAlbums.dataSource = nil
        
        self.albums.asObservable().bind(to: self.tbvAlbums.rx.items(cellIdentifier: R.reuseIdentifier.albumsTableViewCell.identifier, cellType: AlbumsTableViewCell.self)) { [weak self]  row, data, cell in
            guard let self = self else { return }
            cell.imgAlbums.image = R.image.placeholder()
            if data.collection.count > 0 {
                // Request an image for the asset from the PHCachingImageManager.
                cell.representedAssetIdentifier = data.name
                self.imageManager.requestImage(
                    for: data.collection[0],
                    targetSize: self.thumbnailSize,
                    contentMode: .aspectFill,
                    options: nil,
                    resultHandler: { image, _ in
                      if image != nil {
                        cell.imgAlbums.image = image
                      }
                })
            }
            
            cell.nameAlbums.text  = data.name
            cell.numberImgLable.text  = "\(data.count)"
        }.disposed(by: disposeBag)
        
        
        tbvAlbums.rx.modelSelected(AlbumModel.self).observeOn(MainScheduler.asyncInstance).subscribe { [self] (item) in
            self.showViewSelectLibrary = false
            self.album = item.element!
//                                let allPhotosOptions = PHFetchOptions()
//                                allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
//            self.album.collection = PHAsset.fetchAssets(with: allPhotosOptions)
            self.updatePreviewImage(at: 0)
            DispatchQueue.main.async {
                self.clvSelectImage.reloadData()
            }
        }.disposed(by: disposeBag)
    }
    
    private final func getALlsPhoto(){
        /// Load Photos
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
         //   case .limited:
              //  PHPhotoLibrary.shared().register(self)
            case .authorized:
                print("Good to proceed")

                
                self.setupPhotos()
                // image
                self.resetCachedAssets()
                  PHPhotoLibrary.shared().register(self)
                  // If we get here without a segue, it's because we're visible at app launch,
                  // so match the behavior of segue from the default "All Photos" view.
//                if self.fetchResult == nil {
//                    let allPhotosOptions = PHFetchOptions()
//                    allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
//                    self.fetchResult = PHAsset.fetchAssets(with: allPhotosOptions)
//                    DispatchQueue.main.async {
//                        self.clvSelectImage.reloadData()
//                    }
//                }
                
          //      self.updatePreviewImage(at: 0)
                
            case .denied, .restricted:
                print("Not allowed")
            case .notDetermined:
                print("Not determined yet")
            @unknown default:
                fatalError("getALlsPhoto error")
            }
        }
    }
    
    open func collectionType(for subtype: PHAssetCollectionSubtype) -> PHAssetCollectionType {
        return subtype.rawValue < PHAssetCollectionSubtype.smartAlbumGeneric.rawValue ? .album : .smartAlbum
    }
    
    private final func setupPhotos() {

        var album:[AlbumModel] = [AlbumModel]()
        for (_, groupType) in assetGroupTypes.enumerated() {
            let fetchResult = PHAssetCollection.fetchAssetCollections(with: self.collectionType(for: groupType),
                                                                      subtype: groupType,
                                                                      options: nil)
            fetchResult.enumerateObjects({ (collection, index, stop) in
                let allPhotosOptions = PHFetchOptions()
                allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                let result = PHAsset.fetchAssets(in: collection, options: allPhotosOptions)
                
                
              //  result = PHAsset.fetchAssets(with: allPhotosOptions)
                
                let newAlbum = AlbumModel(name: collection.localizedTitle!, count: result.count, collection:result)
                album.append(newAlbum)
                print(newAlbum.name)
                print(newAlbum.count)
                self.albums.accept(album)
            })
        }
    }
    
    deinit {
           PHPhotoLibrary.shared().unregisterChangeObserver(self)
       }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
          updateCachedAssets()
      }
      
      // MARK: Asset Caching
      fileprivate func resetCachedAssets() {
          imageManager.stopCachingImagesForAllAssets()
          previousPreheatRect = .zero
      }
      
      fileprivate func updateCachedAssets() {
          // Update only if the view is visible.
          guard isViewLoaded && view.window != nil else { return }
          
          // The preheat window is twice the height of the visible rect.
          let visibleRect = CGRect(origin: clvSelectImage!.contentOffset, size: clvSelectImage!.bounds.size)
          let preheatRect = visibleRect.insetBy(dx: 0, dy: -0.5 * visibleRect.height)
          
          // Update only if the visible area is significantly different from the last preheated area.
          let delta = abs(preheatRect.midY - previousPreheatRect.midY)
          guard delta > view.bounds.height / 3 else { return }
          
          // Compute the assets to start caching and to stop caching.
          let (addedRects, removedRects) = differencesBetweenRects(previousPreheatRect, preheatRect)
          let addedAssets = addedRects
              .flatMap { rect in clvSelectImage!.indexPathsForElements(in: rect) }
            .map { indexPath in self.album.collection.object(at: indexPath.item) }
          let removedAssets = removedRects
              .flatMap { rect in clvSelectImage!.indexPathsForElements(in: rect) }
              .map { indexPath in self.album.collection.object(at: indexPath.item) }
          
          // Update the assets the PHCachingImageManager is caching.
          imageManager.startCachingImages(for: addedAssets,
                                          targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
          imageManager.stopCachingImages(for: removedAssets,
                                         targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
          
          // Store the preheat rect to compare against in the future.
          previousPreheatRect = preheatRect
      }
      
      fileprivate func differencesBetweenRects(
          _ old: CGRect,
          _ new: CGRect) -> (added: [CGRect], removed: [CGRect]) {
          if old.intersects(new) {
              var added = [CGRect]()
              if new.maxY > old.maxY {
                  added += [CGRect(x: new.origin.x, y: old.maxY,
                                   width: new.width, height: new.maxY - old.maxY)]
              }
              if old.minY > new.minY {
                  added += [CGRect(x: new.origin.x, y: new.minY,
                                   width: new.width, height: old.minY - new.minY)]
              }
              var removed = [CGRect]()
              if new.maxY < old.maxY {
                  removed += [CGRect(x: new.origin.x, y: new.maxY,
                                     width: new.width, height: old.maxY - new.maxY)]
              }
              if old.minY < new.minY {
                  removed += [CGRect(x: new.origin.x, y: old.minY,
                                     width: new.width, height: new.minY - old.minY)]
              }
              return (added, removed)
          } else {
              return ([new], [old])
          }
      }
}


// MARK: PHPhotoLibraryChangeObserver
extension SelectImageEditViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        guard let changes = changeInstance.changeDetails(for: self.album.collection)
            else { return }
        
        // Change notifications may be made on a background queue. Re-dispatch to the
        // main queue before acting on the change as we'll be updating the UI.
        DispatchQueue.main.sync {
            // Hang on to the new fetch result.
            var albumTemp = self.albums.value
            self.album.collection = changes.fetchResultAfterChanges
            self.album.count = changes.fetchResultAfterChanges.count
            self.album.name = self.album.name
            let index = self.albums.value.firstIndex(where: { $0.name == self.album.name})
            
            if index != nil {
                albumTemp[index!] = self.album
                self.albums.accept(albumTemp)
            }
            
            if changes.hasIncrementalChanges {
                // If we have incremental diffs, animate them in the collection view.
                guard let photoAlbum  = self.clvSelectImage else { fatalError() }
                photoAlbum.performBatchUpdates({
                    // For indexes to make sense, updates must be in this order:
                    // delete, insert, reload, move
                    if let removed = changes.removedIndexes, removed.count > 0 {
                        photoAlbum.deleteItems(at: removed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let inserted = changes.insertedIndexes, inserted.count > 0 {
                        photoAlbum.insertItems(at: inserted.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let changed = changes.changedIndexes, changed.count > 0 {
                        photoAlbum.reloadItems(at: changed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    changes.enumerateMoves { fromIndex, toIndex in
                        photoAlbum.moveItem(at: IndexPath(item: fromIndex, section: 0),
                                                to: IndexPath(item: toIndex, section: 0))
                    }
                })
            } else {
                // Reload the collection view if incremental diffs are not available.
                 clvSelectImage!.reloadData()
            }
            resetCachedAssets()
        }
    }
}

extension SelectImageEditViewController:  UICollectionViewDataSource,
   UICollectionViewDelegate,
   UICollectionViewDelegateFlowLayout{
    
    // MARK: UICollectionView
      // return cell size UICollectionViewDelegateFlowLayout
      func collectionView(_ collectionView: UICollectionView,
                          layout collectionViewLayout: UICollectionViewLayout,
                          sizeForItemAt indexPath: IndexPath) -> CGSize {
          let numRow: Int = 3
        let cellSize:CGFloat = self.view.bounds.width / CGFloat(numRow) - (8.0 + 8.0)
          return CGSize(width: cellSize, height: cellSize)
      }
      
      func collectionView(
          _ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        guard let fetchResult = fetchResult else {
//            return 0
//        }
//          return fetchResult.count
        
        return self.album.count
      }
      
      func numberOfSections(in collectionView: UICollectionView) -> Int {
          return 1
      }
      
      func collectionView(
          _ collectionView: UICollectionView,
          cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let asset = self.album.collection.object(at: indexPath.item)
          
          // Dequeue a GridViewCell.
          guard let cell: SelectImageEditCollectionViewCell = collectionView.dequeueReusableCell(
              withReuseIdentifier: String(describing: SelectImageEditCollectionViewCell.self), for: indexPath) as? SelectImageEditCollectionViewCell
              else { fatalError("unexpected cell in collection view") }
          
          // Request an image for the asset from the PHCachingImageManager.
          cell.representedAssetIdentifier = asset.localIdentifier
          imageManager.requestImage(
              for: asset,
              targetSize: thumbnailSize,
              contentMode: .aspectFill,
              options: nil,
              resultHandler: { image, _ in
                if image != nil {
                    // The cell may have been recycled by the time this handler gets called;
                    // set the cell's thumbnail image only if it's still showing the same asset.
                    if cell.representedAssetIdentifier == asset.localIdentifier {
                        cell.thumbnailImage = image
                      if indexPath.row == 0{
                          guard let ciImage = cell.thumbnailImage.cgImage else {
                              return
                          }
                          self.previewedThumbnailData = CIImage(cgImage: ciImage)
                      }
                    }
                }
          })
          
          return cell
      }
      
      func updatePreviewImage(at: Int) {
        if self.album.count > 0 {
            let asset = self.album.collection.object(at: at)
              // Prepare the options to pass when fetching the live photo.
              let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            options.isNetworkAccessAllowed = true
              // Request the live photo for the asset from the default PHImageManager.
              imageManager.requestImage(
                  for: asset,
                  targetSize: targetSize,
                  contentMode: .aspectFit,
                  options: options,
                  resultHandler: { image, _ in
                    DispatchQueue.main.async {
                        self.imgSelect.image = image
                    }
              })
        }else{
            self.imgSelect.image = R.image.placeholder()
        }
        
      }
      
      // cell is selected
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SelectImageEditCollectionViewCell
        if cell.thumbnailImage != nil {
            if imgSelect != nil &&  cell.thumbnailImage.cgImage != nil && self.imgSelect != R.image.placeholder(){
                previewedPhotoIndexPath = indexPath
                self.previewedThumbnailData = CIImage(cgImage: cell.thumbnailImage.cgImage!)
                self.updatePreviewImage(at: indexPath.item)
            }else{
                self.imgSelect.image = R.image.placeholder()
            }
        }else{
            self.imgSelect.image = R.image.placeholder()
        }
      }
}

