//
//  SelectImageEditCollectionViewCell.swift
//  Beberia
//
//  Created by IMAC on 9/30/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class SelectImageEditCollectionViewCell: UICollectionViewCell {
     
     @IBOutlet weak var imgPhoto: UIImageView!
    
    var representedAssetIdentifier: String!
    
    var thumbnailImage: UIImage! {
        didSet {
            imgPhoto.image = thumbnailImage
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgPhoto.image = nil
    }

}
