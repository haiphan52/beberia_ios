//
//  AlbumsTableViewCell.swift
//  Beberia
//
//  Created by IMAC on 10/21/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class AlbumsTableViewCell: UITableViewCell {

    @IBOutlet weak var numberImgLable: UILabel!
    @IBOutlet weak var nameAlbums: UILabel!
    @IBOutlet weak var imgAlbums: UIImageView!
    
    var representedAssetIdentifier: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
