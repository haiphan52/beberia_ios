//
//  TopicTickerCollectionViewCell.swift
//  Beberia
//
//  Created by IMAC on 10/2/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit

class TopicTickerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewSelect: UIView!
    @IBOutlet weak var nameSticker: UILabel!
    @IBOutlet weak var imgTopicSticker: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
