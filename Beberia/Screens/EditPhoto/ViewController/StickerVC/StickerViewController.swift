//
//  StickerViewController.swift
//  Beberia
//
//  Created by IMAC on 10/2/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

struct TopicSticker {
    var image: UIImage
    var name: String
    var isSelect: Bool = false
}

class StickerViewController: BottomPopupViewController {
    
    @IBOutlet weak var btnCloseView: UIButton!
    @IBOutlet weak var clvStickers: UICollectionView!
    @IBOutlet weak var clvTopicSticker: UICollectionView!
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var disposeBag = DisposeBag()
    private var imageStickers = BehaviorRelay.init(value: [UIImage]())
    private var topicStickers = BehaviorRelay.init(value: [TopicSticker]())
    
    var selectTicker: (UIImage)->() = {_ in}
    
    var arrayEveryday = [UIImage]()
    var arrayBoy = [UIImage]()
    var arrayGirl = [UIImage]()
    var arrayBirthday = [UIImage]()
    var arrayAnimal = [UIImage]()
    var arrayToy = [UIImage]()
    var arrayPhuKien = [UIImage]()
    var arrayDate = [UIImage]()
    var arrayTrangTri = [UIImage]()
    var arrayHappy = [UIImage]()
    var arrayStars = [UIImage]()
    var arrayLovely = [UIImage]()
    var arrayFamily = [UIImage]()
    var arrayStickers = [TopicSticker]()
    var retureVAlue:([UIImage])->() = {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.view.frame.height)
        
        
        
        self.arrayEveryday.append(contentsOf: Utils.getImagesFromBundle(path: "Everyday"))
        self.imageStickers.accept(self.arrayEveryday)

        self.arrayBoy.append(contentsOf: Utils.getImagesFromBundle(path: "Boy"))
        self.arrayGirl.append(contentsOf: Utils.getImagesFromBundle(path: "girl"))
        self.arrayBirthday.append(contentsOf: Utils.getImagesFromBundle(path: "Birthday"))
        
        
        DispatchQueue.global(qos: .background).async {
            self.arrayAnimal.append(contentsOf: Utils.getImagesFromBundle(path: "Animal"))
            self.arrayToy.append(contentsOf: Utils.getImagesFromBundle(path: "girlcopy"))
            self.arrayPhuKien.append(contentsOf: Utils.getImagesFromBundle(path: "Deco"))
            self.arrayDate.append(contentsOf: Utils.getImagesFromBundle(path: "Date"))
            self.arrayTrangTri.append(contentsOf: Utils.getImagesFromBundle(path: "Accessories"))
            self.arrayHappy.append(contentsOf: Utils.getImagesFromBundle(path: "vuive"))
            self.arrayStars.append(contentsOf: Utils.getImagesFromBundle(path: "ngoisao"))
            self.arrayLovely.append(contentsOf: Utils.getImagesFromBundle(path: "dangyeu"))
            self.arrayFamily.append(contentsOf: Utils.getImagesFromBundle(path: "giadinh"))
          
        }

        initRx()
        
        dumyData()
    }

    func dumyData(){

        arrayStickers.append(contentsOf: [TopicSticker.init(image: R.image.sun()!, name: "Hàng ngày"),TopicSticker.init(image: R.image.boy()!, name: "Bé trai"),TopicSticker.init(image: R.image.girl()!, name: "Bé gái"),TopicSticker.init(image: R.image.birthday()!, name: "Sinh nhật"),TopicSticker.init(image: R.image.hamster()!, name: "Động vật"),TopicSticker.init(image: R.image.teddyBear()!, name: "Đồ chơi"),TopicSticker.init(image: R.image.hat()!, name: "Phụ kiện"),TopicSticker.init(image: R.image.edit_calendar()!, name: "Lịch"),TopicSticker.init(image: R.image.balloons()!, name: "Trang trí"),TopicSticker.init(image: R.image.family()!, name: "Gia đình"),TopicSticker.init(image: R.image.lovely()!, name: "Đáng yêu"),TopicSticker.init(image: R.image.star()!, name: "Ngôi sao")])
        
        var arrayStickerTemp = arrayStickers
        arrayStickerTemp[0].isSelect = true
        
        topicStickers.accept(arrayStickerTemp)
    }
    
    func initRx(){
        // MARK:  CollectionView Image Sticker
        let flowLayoutImageFrame = UICollectionViewFlowLayout()
        let widthCell1 = ((UIScreen.main.bounds.width - 16) / 3)
        flowLayoutImageFrame.itemSize = CGSize(width: widthCell1 , height: widthCell1 - 10)
        flowLayoutImageFrame.scrollDirection = .horizontal
        flowLayoutImageFrame.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvStickers.setCollectionViewLayout(flowLayoutImageFrame, animated: true)
        clvStickers.showsVerticalScrollIndicator = false
        clvStickers.register(R.nib.tickerCollectionViewCell)
        
        
        
        self.imageStickers.asObservable().bind(to: self.clvStickers.rx.items(cellIdentifier: R.reuseIdentifier.tickerCollectionViewCell.identifier, cellType: TickerCollectionViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
            cell.imgTicker.image = data
        }.disposed(by: disposeBag)
        
        
        
        Observable
            .zip(clvStickers.rx.itemSelected, clvStickers.rx.modelSelected(UIImage.self))
            .bind { [weak self] indexPath, item in
                guard let self = self else { return }
                
                self.selectTicker(item)
                
        }.disposed(by: disposeBag)
        
        // MARK:  CollectionView Sticker Topic
        let flowLayout = UICollectionViewFlowLayout()
        let heihgt = self.clvTopicSticker.frame.height
        flowLayout.itemSize = CGSize(width: heihgt - 40 , height: heihgt - 10)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvTopicSticker.setCollectionViewLayout(flowLayout, animated: true)
        clvTopicSticker.showsVerticalScrollIndicator = false
        clvTopicSticker.register(R.nib.topicTickerCollectionViewCell)
        
        
        
        self.topicStickers.asObservable().bind(to: self.clvTopicSticker.rx.items(cellIdentifier: R.reuseIdentifier.topicTickerCollectionViewCell.identifier, cellType: TopicTickerCollectionViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
            cell.imgTopicSticker.image = data.image
            cell.nameSticker.text = data.name
            cell.nameSticker.textColor = UIColor.init(hexString: "cecece")
            cell.viewSelect.backgroundColor = UIColor.init(hexString: "f6f6f6")
            if data.isSelect {
                cell.nameSticker.textColor = UIColor.init(hexString: "000000")
                cell.viewSelect.backgroundColor = UIColor.init(hexString: "fff197")
            }
        }.disposed(by: disposeBag)
        
        
        
        Observable
            .zip(clvTopicSticker.rx.itemSelected, clvTopicSticker.rx.modelSelected(TopicSticker.self))
            .bind { [weak self] indexPath, item in
                guard let self = self else { return }
                
                var topicStickerTemp = [TopicSticker]()
                topicStickerTemp = self.arrayStickers
                topicStickerTemp[indexPath.row].isSelect = true
                self.topicStickers.accept(topicStickerTemp)
                
                
                switch item.name {
                case "Hàng ngày" :
                    self.imageStickers.accept(self.arrayEveryday)
                    
                case "Bé trai" :
                    self.imageStickers.accept(self.arrayBoy)
                    
                case "Bé gái" :
                    self.imageStickers.accept(self.arrayGirl)
                    
                case "Sinh nhật" :
                    self.imageStickers.accept(self.arrayBirthday)
                    
                case "Động vật" :
                    self.imageStickers.accept(self.arrayAnimal)
                    
                case "Đồ chơi" :
                    self.imageStickers.accept(self.arrayToy)
                    
                case "Phụ kiện" :
                    self.imageStickers.accept(self.arrayPhuKien)
                    
                case "Lịch" :
                    self.imageStickers.accept(self.arrayDate)
                    
                case "Trang trí" :
                    self.imageStickers.accept(self.arrayTrangTri)
                    
                case "Gia đình" :
                    self.imageStickers.accept(self.arrayFamily)
                    
                case "Đáng yêu" :
                    self.imageStickers.accept(self.arrayLovely)
                    
                case "Ngôi sao" :
                    self.imageStickers.accept(self.arrayStars)
                    
                default:
                    print("!2121")
                }
                
        }.disposed(by: disposeBag)
        
        btnCloseView.rx.tap.subscribe { (_) in
            self.dismiss(animated: true, completion: nil)
        }.disposed(by: disposeBag)
    }
    
    
    override func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(UIScreen.main.bounds.height)
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(0)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 0.3
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 0.3
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
}
