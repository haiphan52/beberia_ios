//
//  OrdersVC.swift
//  Beberia
//
//  Created by haiphan on 06/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD
import PullToRefresh

class OrdersVC: BaseNavigationColor {
    
    enum StatusStackView: Int, CaseIterable {
        case all, notYetPay, preparing, completed, sent, cancel
        
        var valueStatus: Int {
            switch self {
            case .all: return StatusOrderGlobal.all.rawValue
            case .notYetPay: return StatusOrderGlobal.notYetPay.rawValue
            case .preparing: return StatusOrderGlobal.preparing.rawValue
            case .completed: return StatusOrderGlobal.completed.rawValue
            case .sent: return StatusOrderGlobal.sent.rawValue
            case .cancel: return StatusOrderGlobal.cancel.rawValue
            }
        }
    }
    
    enum StatusNew: Int, CaseIterable {
        case all,  analysis, result
        var valueStatus: Int {
            switch self {
            case .all: return StatusOrderGlobal.all.rawValue
            case .analysis: return StatusOrderGlobal.preparing.rawValue
            case .result: return StatusOrderGlobal.all.rawValue
            }
        }
    }

    @IBOutlet var views: [UIView]!
    @IBOutlet var lbs: [UILabel]!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet var lbsNew: [UILabel]!
    @IBOutlet var btsNew: [UIButton]!
    @IBOutlet weak var lineNewView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet var viewsNew: [UIView]!
    private var page = 1
    private var nextPage = 1
    private var total = 0
    @VariableReplay private var list: [ManageOrderModel] = []
    private var statusOrder: StatusStackView = .all
    @VariableReplay private var statusNew: StatusNew = .all
    private let refresher = PullToRefresh()
    private let destinationURL: PublishSubject<URL> = PublishSubject.init()
    private let mssgErr: PublishSubject<String> = PublishSubject.init()
    private let processing: PublishSubject<Double> = PublishSubject.init()
    
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SVProgressHUD.dismiss()
    }

}
extension OrdersVC {
    
    private func setupUI() {
        title = "Quản lý đơn hàng"
        
        self.tableView.delegate = self
        self.tableView.register(OrderCell.nib, forCellReuseIdentifier: OrderCell.identifier)
        self.tableView.register(ResultCell.nib, forCellReuseIdentifier: ResultCell.identifier)
        self.getListOrders()
        
        self.tableView.addInfiniteScroll { [weak self] (tableView) in
            guard let wSelf = self else { return }
            if wSelf.nextPage != 0 {
                wSelf.getListOrders()
            }else{
                wSelf.tableView.endRefreshing(at: .top)
                wSelf.tableView.finishInfiniteScroll(completion: { (collection) in
                })
            }
        }
        self.tableView.addPullToRefresh(refresher) {
            self.page = 1
            self.nextPage = 1
            self.total = 0
            self.list = []
            self.getListOrders()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let rect = self.viewsNew[StatusNew.all.rawValue].frame
            self.animateLineNewView(rect: rect)
        }
    }
    
    private func setupRX() {
        self.$list.asObservable().bind { [weak self] list in
            guard let wSelf = self else { return }
            if list.count <= 0 {
                wSelf.tableView.isHidden = true
                wSelf.emptyView.isHidden = false
            } else {
                wSelf.tableView.isHidden = false
                wSelf.emptyView.isHidden = true
            }
        }.disposed(by: self.disposeBag)
        
        StatusStackView.allCases.forEach { [weak self] type in
            guard let wSelf = self else { return }
            let bt = wSelf.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                let rect = wSelf.views[type.rawValue].frame
                wSelf.animateLineView(rect: rect)
                //Update value default when change selection
                wSelf.page = 1
                wSelf.nextPage = 1
                wSelf.total = 0
                wSelf.statusOrder = type
                wSelf.updateLabelOld(statusNew: type)
                wSelf.$list.accept([])
                wSelf.getListOrders()
            }.disposed(by: wSelf.disposeBag)
        }
        
        StatusNew.allCases.forEach { [weak self] type in
            guard let wSelf = self else { return }
            let bt = wSelf.btsNew[type.rawValue]
            bt.rx.tap.bind { [weak self] in
                guard let wSelf = self else { return }
                let rect = wSelf.viewsNew[type.rawValue].frame
                wSelf.animateLineNewView(rect: rect)
                wSelf.page = 1
                wSelf.nextPage = 1
                wSelf.total = 0
                wSelf.statusNew = type
                wSelf.$list.accept([])
                wSelf.getListOrdersNew()
            }.disposed(by: wSelf.disposeBag)
        }
        
        self.$statusNew.asObservable().bind { [weak self] type in
            guard let wSelf = self else { return }
            wSelf.updateLabel(statusNew: type)
        }.disposed(by: self.disposeBag)
        
        self.$list.asObservable().bind(to: tableView.rx.items) { [weak self] (tv, row, item) -> UITableViewCell in
            guard let wSelf = self else { fatalError() }
            switch wSelf.statusOrder {
            case .completed:
                guard let cell = tv.dequeueReusableCell(withIdentifier: ResultCell.identifier, for: IndexPath.init(row: row, section: 0)) as? ResultCell else  {
                    fatalError()
                }
                cell.setupValue(order: item)
                if let last = item.result?.last, let url = last.image?.storageURI {
                    cell.uploadImage(url: url)
                } else {
                    cell.imageDefault()
                }
                cell.updateHeight = { [weak self] in
                    guard let self = self else { return }
                    self.tableView.beginUpdates()
                    self.tableView.endUpdates()
                }
                cell.action = { [weak self] type in
                    guard let self = self else { return }
                    switch type {
                    case .rate:
                        ManageApp.shared.openLink(link: ConstantApp.shared.linkRate)
                    case .feedback:
                        ManageApp.shared.openLink(link: ConstantApp.shared.linkFeedback)
                    case .dowload:
                        self.dowloadURL(item: item)
                    }
                }
                return cell
            default:
                guard let cell = tv.dequeueReusableCell(withIdentifier: OrderCell.identifier, for: IndexPath.init(row: row, section: 0)) as? OrderCell else  {
                    fatalError()
                }
                cell.setupValue(order: item)
                cell.tap = { [weak self] in
                    guard let wSelf = self else { return }
                    wSelf.moveToTabbar(order: wSelf.list[row])
                }
                cell.tapDetail = { [weak self] in
                    guard let wSelf = self else { return }
                    wSelf.moveToDetail(idx: row)
                }
                
                return cell
            }
        }.disposed(by: disposeBag)
        
        self.processing.asObservable().bind { pro in
            SVProgressHUD.showProgress(Float(pro))
        }.disposed(by: self.disposeBag)
        
        self.destinationURL.asObservable().bind { [weak self] url in
            guard let self = self else { return }
            do {
                let data: Data = try Data(contentsOf: url)
                if let image: UIImage = UIImage(data: data) {
                    ManageApp.shared.saveImageToPhoto(inputImage: image)
                }
            } catch let err {
                self.showAlert(title: nil, message: err.localizedDescription)
            }
            SVProgressHUD.dismiss()
        }.disposed(by: self.disposeBag)
        
        self.mssgErr.asObservable().bind { [weak self] text in
            guard let self = self else { return }
            self.showAlert(title: nil, message: text)
        }.disposed(by: self.disposeBag)
        
        self.tableView.rx.itemSelected.bind { [weak self] idx in
            guard let wSelf = self else { return }
            wSelf.moveToDetail(idx: idx.row)
        }.disposed(by: disposeBag)
    }
    
    private func dowloadURL(item: ManageOrderModel) {
        if let last = item.result?.last, let url = last.image?.storageURI {
            AlamofireManage.shared.dowloadImageURL(url: url,
                                                folderName: ConstantApp.FolderName.folderDowloadImage.rawValue,
                                                id: Date().timeIntervalSince1970,
                                                destinationURL: self.destinationURL,
                                                msgError: self.mssgErr) { [weak self] process in
                guard let self = self else { return }
                self.processing.onNext(process)
            }
        }
    }
    
    private func updateLabelOld(statusNew: StatusStackView) {
        self.lbs.enumerated().forEach { element in
            let index = element.offset
            let lb = element.element
            if statusNew.rawValue == index {
                lb.textColor = R.color.fd799dColor()
            } else {
                lb.textColor = R.color.d5B5B()
            }
        }
    }
    
    private func updateLabel(statusNew: StatusNew) {
        switch statusNew {
        case .all:
            self.lbsNew[StatusNew.all.rawValue].textColor = R.color.fd799dColor()
            self.lbsNew[StatusNew.analysis.rawValue].textColor = R.color.d5B5B()
            self.lbsNew[StatusNew.result.rawValue].textColor = R.color.d5B5B()
        case .analysis:
            self.lbsNew[StatusNew.all.rawValue].textColor = R.color.d5B5B()
            self.lbsNew[StatusNew.analysis.rawValue].textColor = R.color.fd799dColor()
            self.lbsNew[StatusNew.result.rawValue].textColor = R.color.d5B5B()
        case .result:
            self.lbsNew[StatusNew.all.rawValue].textColor = R.color.d5B5B()
            self.lbsNew[StatusNew.analysis.rawValue].textColor = R.color.d5B5B()
            self.lbsNew[StatusNew.result.rawValue].textColor = R.color.fd799dColor()
        }
    }
    
    private func moveToDetail(idx: Int) {
        let vc = OrderDetail.createVC()
        vc.detailOrder = self.list[idx]
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func moveToTabbar(order: ManageOrderModel) {
        if let tabBarVC = R.storyboard.main.tabBarVC() {
            tabBarVC.selectedIndex = 3
            tabBarVC.passValueBaby(order: order)
            self.navigationController?.pushViewController(tabBarVC, animated: true)
        }
    }
    
    private func getListOrdersNew(){
        SVProgressHUD.show()
        APIManager.listOrders(page: self.nextPage, status: self.statusNew.valueStatus) { [weak self] list, nextPage, total in
            guard let wSelf = self else { return }
            var myListTemp = wSelf.list
            myListTemp.append(contentsOf: list)
            wSelf.list = myListTemp
            wSelf.nextPage = nextPage
            wSelf.tableView.endRefreshing(at: .top)
            wSelf.tableView.finishInfiniteScroll(completion: { (collection) in
            })
            SVProgressHUD.dismiss()
        } failed: { error in
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
            SVProgressHUD.dismiss()
        }
    }
    
    private func getListOrders(){
        SVProgressHUD.show()
        APIManager.listOrders(page: self.nextPage, status: self.statusOrder.valueStatus) { [weak self] list, nextPage, total in
            guard let wSelf = self else { return }
            var myListTemp = wSelf.list
            myListTemp.append(contentsOf: list)
            wSelf.list = myListTemp
            wSelf.nextPage = nextPage
            wSelf.tableView.endRefreshing(at: .top)
            wSelf.tableView.finishInfiniteScroll(completion: { (collection) in
            })
            SVProgressHUD.dismiss()
        } failed: { error in
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
            SVProgressHUD.dismiss()
        }
    }
    
    private func animateLineNewView(rect: CGRect) {
        var f = self.lineNewView.frame
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            f.origin.x = rect.origin.x
            f.size.width = rect.size.width
            self.lineNewView.frame = f
        }, completion: nil)
    }
    
    private func animateLineView(rect: CGRect) {
        var f = self.lineView.frame
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            f.origin.x = rect.origin.x
            f.size.width = rect.size.width
            self.lineView.frame = f
        }, completion: nil)
    }
    
}
extension OrdersVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension OrdersVC: OrderDetailDelegate {
    func updateOrder(order: ManageOrderModel) {
        guard let index = self.list.firstIndex(where: { $0.id == order.id }) else {
            return
        }
        self.list.remove(at: index)
        self.list.insert(order, at: index)
        guard let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? OrderCell else {
            return
        }
        cell.setupValue(order: order)
    }
}
