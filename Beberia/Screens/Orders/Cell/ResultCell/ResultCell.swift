//
//  ResultCell.swift
//  Beberia
//
//  Created by haiphan on 03/07/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher

class ResultCell: UITableViewCell {
    
    enum Action: Int, CaseIterable {
        case feedback, dowload, rate
    }
    
    var action: ((Action) -> Void)?
    var updateHeight: (() -> Void)?

    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var imgFrame: UIImageView!
    @IBOutlet weak var lbID: UILabel!
    @IBOutlet weak var lbStatus: UILabel!
    
    private let disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imageDefault()
        self.setupRX()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension ResultCell {
    private func setupRX() {
        Action.allCases.forEach { [weak self] type in
            guard let wSelf = self else { return }
            let bt = wSelf.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] in
                guard let wSelf = self else { return }
                wSelf.action?(type)
            }.disposed(by: wSelf.disposeBag)
        }
    }
    
    func setupValue(order: ManageOrderModel) {
        self.lbID.text = "#\(order.id ?? 21)"
        let type = StatusOrderGlobal.init(rawValue: order.status ?? 0)
        self.lbStatus.text = type?.text
    }
    
    func imageDefault() {
        self.imgFrame.image = R.image.placeholder()?.resizeImageSpace(width: UIScreen.main.bounds.width - 32)
    }
    
    func uploadImage(url: String?) {
        if let textUrl = url, let url = URL(string: textUrl) {
            KingfisherManager.shared.retrieveImage(with: url) { result in
                do {
                    let image = try result.get().image
                    self.imgFrame.image = image.resizeImageSpace(width: UIScreen.main.bounds.width - 32)
                    self.updateHeight?()
                } catch {
                    print(error)
                }
            }
        }
    }
}
