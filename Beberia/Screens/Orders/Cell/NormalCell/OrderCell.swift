//
//  OrderCell.swift
//  Beberia
//
//  Created by haiphan on 06/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit
import RxSwift

class OrderCell: UITableViewCell {
    
    var tap: ( () -> Void )?

    @IBOutlet weak var lbID: UILabel!
    @IBOutlet weak var lbStatus: UILabel!
    @IBOutlet weak var imageBaby: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbCount: UILabel!
    @IBOutlet weak var lbPriceElement: UILabel!
    @IBOutlet weak var lbCountProduct: UILabel!
    @IBOutlet weak var lbPriceCountProduct: UILabel!
    @IBOutlet weak var btReOrder: UIButton!
    @IBOutlet weak var btDetail: UIButton!
    
    var tapDetail: (() -> Void)?
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.setupRX()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupRX() {
        self.btReOrder.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.tap?()
        }.disposed(by: self.disposeBag)
        
        self.btDetail.rx.tap.bind { [weak self] in
            guard let wSelf = self else { return }
            wSelf.tapDetail?()
        }.disposed(by: self.disposeBag)
    }
    
    func setupValue(order: ManageOrderModel) {
        self.lbID.text = "#\(order.id ?? 21)"
        self.lbTitle.text = "\(order.item?.name ?? "In cơ bản")"
        
        self.lbPriceElement.text = order.item?.showPricce()
        self.lbPriceCountProduct.text = order.payAmount?.currency
        if let textUrl = order.image, let url = URL(string: textUrl) {
            self.imageBaby.kf.setImage(with: url)
        }
        
        let type = StatusOrderGlobal.init(rawValue: order.status ?? 0)
        self.lbStatus.text = type?.text
        switch type {
        case .all, .completed, .sent, .cancel:
            self.btReOrder.setTitle("Đặt ảnh lại", for: .normal)
            self.btReOrder.backgroundColor = R.color.fd799D()
            self.btReOrder.isEnabled = true
        case .notYetPay, .preparing:
            self.btReOrder.setTitle("Thanh toán lại", for: .normal)
            self.btReOrder.backgroundColor = R.color.e0E0E0()
            self.btReOrder.isEnabled = false
        case .none: break
        }
        
    }
    
}
