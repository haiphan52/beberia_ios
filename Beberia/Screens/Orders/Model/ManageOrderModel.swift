//
//  OrderModel.swift
//  Beberia
//
//  Created by haiphan on 07/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

struct ManageOrderModel: Codable {
    let id: Int?
    let orderCode: String?
    let payAmount: Double?
    let item: PricePrintModel?
    let image: String?
    let contactName, contactEmail, phone: String?
    let created, payDate, sendDate: Double?
    var cancelDate: Double?
    let cancelReason: String?
    var status: Int?
    let images: [String]?
    let message: String?
    let frame: String?
    let result: [ResultOrder]?

    enum CodingKeys: String, CodingKey {
        case id, status
        case orderCode = "order_code"
        case payAmount = "pay_amount"
        case item, image, contactName, contactEmail, phone, created
        case payDate = "pay_date"
        case cancelDate = "cancel_date"
        case sendDate = "send_date"
        case cancelReason = "cancel_reason"
        case images, message, frame, result
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        orderCode = try values.decodeIfPresent(String.self, forKey: .orderCode)
        payAmount = try values.decodeIfPresent(Double.self, forKey: .payAmount)
        item = try values.decodeIfPresent(PricePrintModel.self, forKey: .item)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        contactName = try values.decodeIfPresent(String.self, forKey: .contactName)
        contactEmail = try values.decodeIfPresent(String.self, forKey: .contactEmail)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        created = try values.decodeIfPresent(Double.self, forKey: .created)
        payDate = try values.decodeIfPresent(Double.self, forKey: .payDate)
        cancelDate = try values.decodeIfPresent(Double.self, forKey: .cancelDate)
        sendDate = try values.decodeIfPresent(Double.self, forKey: .sendDate)
        cancelReason = try values.decodeIfPresent(String.self, forKey: .cancelReason)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        images = try values.decodeIfPresent([String].self, forKey: .images)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        frame = try values.decodeIfPresent(String.self, forKey: .frame)
        result = try values.decodeIfPresent([ResultOrder].self, forKey: .result)
    }
}
// MARK: - Result
struct ResultOrder: Codable {
    let image: ImageOrder?
    enum CodingKeys: String, CodingKey {
        case  image
    }
}

// MARK: - Image
struct ImageOrder: Codable {
    let id: Double?
//    let uploadName, updateDateTime, createDateTime: String
    let storageURI: String?

    enum CodingKeys: String, CodingKey {
        case id
//        case uploadName, updateDateTime, createDateTime
        case storageURI = "storageUri"
    }
}
