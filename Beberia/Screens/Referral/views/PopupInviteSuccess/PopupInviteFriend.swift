//
//  PopupInviteFriend.swift
//  Beberia
//
//  Created by Lap on 11/28/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import UIKit

class PopupInviteFriend: UIView {
    @IBOutlet weak var lblInfo: PopupInviteFriend!
    @IBOutlet weak var btnGoHome: UIButton!
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupInviteFriend.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> PopupInviteFriend {
        return R.nib.popupInviteFriend.instantiate(withOwner: nil)[0] as! PopupInviteFriend
    }
}
