//
//  PopupInviteGetGift.swift
//  Beberia
//
//  Created by Lap on 11/28/21.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
import UIKit

class PopupInviteGetGift: UIView {
    
    @IBOutlet weak var btnCloes: UIButton!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var btnComfirm: UIButton!
    @IBOutlet weak var tfInputReferral: UITextField!
    @IBOutlet weak var lbPointInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.setupRX()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    override func removeFromSuperview() {
        superview?.removeFromSuperview()
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupInviteGetGift.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> PopupInviteGetGift {
        return R.nib.popupInviteGetGift.instantiate(withOwner: nil)[0] as! PopupInviteGetGift
    }
}
extension PopupInviteGetGift {
    private func setupUI() {
//        self.btnComfirm.clipsToBounds = true
//        self.btnComfirm.layer.cornerRadius = 8
//        self.btnComfirm.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
//        self.tfInputReferral.clipsToBounds = true
//        self.tfInputReferral.layer.cornerRadius = 4
//        self.tfInputReferral.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
//        self.setupLabel()
    }
    
    private func setupRX() {
        
    }
    
    private func setupLabel() {
        let attrs1 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Bold", size: 14),
                      NSAttributedString.Key.foregroundColor : UIColor.green]

        let attrs2 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Bold", size: 14),
                      NSAttributedString.Key.foregroundColor : UIColor.white]

        let attributedString1 = NSMutableAttributedString(string:"Drive", attributes:attrs1 as [NSAttributedString.Key : Any])

        let attributedString2 = NSMutableAttributedString(string:"safe", attributes:attrs2 as [NSAttributedString.Key : Any])

        attributedString1.append(attributedString2)
        self.lbPointInfo.attributedText = attributedString1
    }
}
