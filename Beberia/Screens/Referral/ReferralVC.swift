//
//  ReferralVC.swift
//  Beberia
//
//  Created by haiphan on 14/12/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

protocol ReferralDelegate {
    func dismiss(user: User?)
}

class ReferralVC: UIViewController {
    
    enum Action: Int, CaseIterable {
        case confirm, dismiss, invite, moveToForum
    }
    
    @IBOutlet weak var tfInputRef: UITextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lbPointInfo: UILabel!
    @IBOutlet weak var refStackView: UIStackView!
    @IBOutlet weak var successView: UIView!
    @IBOutlet weak var successStackView: UIStackView!
    @IBOutlet var bts: [UIButton]!
    
    var delegate: ReferralDelegate?
    var user: User?
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
}
extension ReferralVC {
    
    private func setupUI() {
        self.bts[Action.confirm.rawValue].clipsToBounds = true
        self.bts[Action.confirm.rawValue].layer.cornerRadius = 8
        self.bts[Action.confirm.rawValue].layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        self.tfInputRef.clipsToBounds = true
        self.tfInputRef.layer.cornerRadius = 4
        self.tfInputRef.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 16
        self.contentView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        self.successView.clipsToBounds = true
        self.successView.layer.cornerRadius = 16
        self.successView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        self.setupLabel()
    }
    
    private func setupRX() {
        Action.allCases.forEach { [weak self] type in
            guard let wSelf = self else { return }
            let bt = wSelf.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                switch type {
                case .invite:
                    ManageApp.shared.shareCode(viewcontroller: wSelf)
                case .moveToForum:
                    wSelf.dismiss(animated: true) {
                        wSelf.delegate?.dismiss(user: wSelf.user)
                    }
                case .dismiss:
                    wSelf.dismiss(animated: true) {
                        wSelf.delegate?.dismiss(user: wSelf.user)
                        wSelf.handleJoinGame(code: "", type: type)
                    }
                case .confirm:
                    guard let code = wSelf.tfInputRef.text, code.count > 0 else {
                        return
                    }
                    wSelf.tfInputRef.resignFirstResponder()
                    wSelf.handleJoinGame(code: code, type: type)
                }
            }.disposed(by: wSelf.disposeBag)
        }
    }
    
    private func handleJoinGame(code: String, type: Action){
        APIManager.inviteCode(code: code) { [weak self] codeStatus in
            guard let wSelf = self else { return }
            switch type {
            case .dismiss: break
            case .confirm:
                if codeStatus == 200 {
                    wSelf.refStackView.isHidden = true
                    wSelf.successStackView.isHidden = false
                } else if codeStatus == 400 {
                    Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: "\(codeStatus)")
                } else {
                    Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: "\(codeStatus)")
                }
            case .invite, .moveToForum: break
            }
            
        } failed: { error in
            Utils.showAlertOKWithoutAction(controller: Utils.getTopMostViewController()!, title: R.string.localizable.commonNoti(), message: error)
        }
    }
    
    private func setupLabel() {
        let attrs1 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 14),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "5D5B5B")]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Bold", size: 14),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "0299FE")]
        
        let attrs3 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 14),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "5D5B5B")]
        
        let attributedString1 = NSMutableAttributedString(string:"Nếu không có mã giới thiệu từ bạn bè, bạn có thể mời bạn bè tải ứng dụng để nhận", attributes:attrs1 as [NSAttributedString.Key : Any])
        
        let attributedString2 = NSMutableAttributedString(string:"\n500 điểm", attributes:attrs2 as [NSAttributedString.Key : Any])
        let attributedString3 = NSMutableAttributedString(string:" nhé", attributes:attrs3 as [NSAttributedString.Key : Any])
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        self.lbPointInfo.attributedText = attributedString1
    }
    
}
