//
//  FrameBabyface.swift
//  Beberia
//
//  Created by Lap on 08/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation

// MARK: - PricePrintModel
struct FrameBabyface: Codable {
    let name: String
    let link_frame: String
    let link_sample: String
    var isSelected: Bool = false

    enum CodingKeys: String, CodingKey {
        case name
        case link_frame
        case link_sample
    }
}
