//
//  OrderModel.swift
//  Beberia
//
//  Created by Lap on 10/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation

// MARK: - PricePrintModel
struct OrderModel: Codable {
    let status: Int
    let price: Int
    let address: String
    let payment_url : String
    let user_id: Int
    let created_at: Int
    let id: Int

    enum CodingKeys: String, CodingKey {
        case status
        case price
        case address
        case user_id
        case created_at
        case id
        case payment_url
    }
}
