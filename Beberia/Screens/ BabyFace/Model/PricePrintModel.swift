//
//  PricePrintModel.swift
//  Beberia
//
//  Created by Lap on 08/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation

// MARK: - PricePrintModel
struct PricePrintModel: Codable {
    let itemID: Int
    let name, pricePrintModelDescription: String
    let price: Int
    var isSelected: Bool = false
    let sale: Double
    let salePrice: String
    let priceFormat: String

    enum CodingKeys: String, CodingKey {
        case itemID = "item_id"
        case name
        case pricePrintModelDescription = "description"
        case price, sale
        case salePrice = "sale_price"
        case priceFormat = "price_format"
    }
    
    func showPricce() -> String {
        if sale > 0 {
            return salePrice
        } else {
            return priceFormat
        }
    }
}

