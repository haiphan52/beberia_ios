//
//  PopupOrderResult.swift
//  Beberia
//
//  Created by Lap on 14/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import SwiftEntryKit
import FFPopup

class PopupOrderResult: UIView {
    
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblStatusDetail: UILabel!
    
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.popupOrderResult.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> PopupOrderResult {
      
        return R.nib.popupOrderResult.instantiate(withOwner: nil)[0] as! PopupOrderResult
    }
    
    func showData(textString: String){
        if textString == "Giao dịch thành công"{
            lblStatus.text = "Đặt hàng thành công!"
            lblStatusDetail.text = "Cám ơn bạn đã sử dụng dịch vụ"
            imgStatus.image = R.image.group152111()
        } else {
            lblStatus.text = "Đặt hàng không thành công"
            lblStatusDetail.text = textString
            imgStatus.image = R.image.group15214()
        }
    }
    
    @IBAction func didPressBackToHome(_ sender: Any) {
      //  SwiftEntryKit.dismiss()
        FFPopup.dismissAllPopups()
        let deadlineTime = DispatchTime.now() + .milliseconds(300)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            guard let topVC = Utils.getTopMostViewController() as? BabyFaceViewController else {
                return
            }
            
            topVC.pageMenu?.moveToPage(0)
        }
    }
  

}
