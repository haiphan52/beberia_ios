//
//  ViewTutorialUpdateImageBaby.swift
//  Beberia
//
//  Created by Lap on 10/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit

enum PopupType {
    case baby
    case dadmom
    case devli
}



class ViewTutorialUpdateImageBaby: UIView {

    var actionDismiss: (() -> Void)?
    
    @IBOutlet weak var ratioImage: NSLayoutConstraint!
    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var viewHeader: UIStackView!
    @IBOutlet weak var viewImageBaby1: UIView!
    @IBOutlet weak var viewImageBaby2: UIView!
    
    
    var textBaby = "Vui lòng tải lên ảnh siêu âm lập thể được chụp cách đây hơn 25 tuần (ảnh đen trắng, có màu) \n\n Tải lên ảnh siêu âm 3D của em bé bạn muốn phân tích (có thể tải lên tối đa 5 ảnh , chỉ có 1 ảnh để phân tích. Vui lòng chọn cẩn thận.)"
    
    var textDadMom = "Vui lòng tải lên một bức ảnh của mẹ và bố \n\n - Nó được sử dụng làm tài liệu tham khảo \n\n - Ảnh đã chỉnh sửa không thể được sử dung làm tài liệu tham khảo (Vui lòng không sử dụng ứng dụng Photoshop)"
    
    var textDevli = "*Vui lòng lựa chọn cẩn thận, bức ảnh không thể sửa đổi sau khi phân tích AI và sau khi in ảnh."
    
    func showData(type: PopupType){
        if type == .baby {
            lblText.text = ""
            imgImage.isHidden = true
            ratioImage.constant = 2
            viewHeader.isHidden = false
            viewImageBaby1.isHidden = false
            viewImageBaby2.isHidden = false
            
        } else if type == .dadmom {
            lblText.text = textDadMom
            imgImage.image = R.image.group15280()
            ratioImage.constant = 2
            viewHeader.isHidden = true
            viewImageBaby1.isHidden = true
            viewImageBaby2.isHidden = true
        } else {
            lblText.text = textDevli
            imgImage.image = R.image.group15283()
            ratioImage.constant = 1
            viewHeader.isHidden = true
            viewImageBaby1.isHidden = true
            viewImageBaby2.isHidden = true
        }   
    }

    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        guard subviews.isEmpty else { return self }
        return R.nib.viewTutorialUpdateImageBaby.firstView(owner: nil)
    }
    
    class func instanceFromNib() -> ViewTutorialUpdateImageBaby {
      
        return R.nib.viewTutorialUpdateImageBaby.instantiate(withOwner: nil)[0] as! ViewTutorialUpdateImageBaby
    }

    @IBAction func actionDismiss(_ sender: Any) {
        self.actionDismiss?()
    }
}
