//
//  BabyFaceViewController.swift
//  Beberia
//
//  Created by Lap on 06/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
 
protocol BabyFaceDelegate {
    func pauseVideoBabyface()
}

class BabyFaceViewController: BaseViewController {
    
    enum OpenFrom {
        case other, preganant
    }
    
    struct Constant {
        static let bottomView: CGFloat = 70
    }
    
    enum ViewBottomStatus: Int, CaseIterable {
        case select, uploadImage, payment
    }

    var pageMenu : CAPSPageMenu?
    var openFrom: OpenFrom = .other
    
    var selectPrintVC = SelectPrintViewController()
    var uploadImageVC = UploadImageViewController()
    var paymentVC = PaymentViewController()
    var delegate: BabyFaceDelegate?
    
    @IBOutlet weak var heightViewBar: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var processingView: UIView!
    @IBOutlet var bottomViews: [UIView]!
    @IBOutlet var bts: [UIButton]!
    @IBOutlet weak var lbPrice: UILabel!
    @IBOutlet weak var btBackUpload: UIButton!
    @IBOutlet weak var btBackPayment: UIButton!
    @IBOutlet var btsHeader: [UIButton]!
    @IBOutlet weak var lbRawPrice: UILabel!
    @IBOutlet weak var lbSaleDiscount: UILabel!
    @IBOutlet weak var saleDiscountView: UIView!
    @IBOutlet weak var lbSale: UILabel!
    @IBOutlet weak var saleView: UIView!
    @IBOutlet weak var leftContainerView: NSLayoutConstraint!
    @IBOutlet weak var rightContainerView: NSLayoutConstraint!
    @VariableReplay var statusView: ViewBottomStatus = .select
    private let sideTrigger: BehaviorRelay<Bool> = BehaviorRelay.init(value: true)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
        setupPageMenu()
//        addNavBarImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = (self.openFrom == .other) ? true : false
        if self.openFrom == .preganant {
            self.customLeftBarButtonVer2(imgArrow: R.image.ic_back_white()!)
            self.setupNavigationVer2()
            self.navigationBarCustom(font: UIFont.notoSansFont(weight: .bold, size: 18),
                                     bgColor: R.color.fd799D()!,
                                     textColor: .white,
                                     isTranslucent: true)
            self.removeBorder(font: UIFont.notoSansFont(weight: .bold, size: 18),
                              bgColor: R.color.fd799D()!,
                              textColor: .white)

        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.selectPrintVC.playVideo.pause()
    }

    @IBAction func didSelecStep(_ sender: UIButton) {
        let indexView = self.pageMenu?.currentPageIndex ?? 0
        
        if indexView > (sender.tag - 1){
            self.pageMenu?.moveToPage(sender.tag - 1)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

extension BabyFaceViewController {
    
    private func setupUI() {
        self.processingView.clipsToBounds = true
        self.processingView.cornerRadius = 16
        self.processingView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        self.heightViewBar.constant = ConstantApp.shared.getStatusBarHeight()
        if self.openFrom == .preganant {
            let height = self.navigationController?.navigationBar.frame.height ?? 0
            self.heightViewBar.constant += height
        }
        self.bottomViews.forEach { v in
            v.clipsToBounds = true
            v.layer.cornerRadius = 16
            v.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }

    }
    
    private func setupRX() {
        ViewBottomStatus.allCases.forEach { [weak self] type in
            guard let wSelf = self else { return }
            let bt = wSelf.bts[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                switch type {
                case .select:
                    wSelf.pageMenu?.moveToPage(1)
                    wSelf.statusView = .uploadImage
                    wSelf.selectPrintVC.playVideo.pause()
                case .uploadImage:
                    if wSelf.uploadImageVC.imageBabysTemp.count == 0{
                        Utils.showAlertOKWithoutAction(controller: wSelf,
                                                       title: R.string.localizable.commonNoti(),
                                                       message: "Vui lòng chọn ảnh siêu âm")
                        return
                    }
                    wSelf.pageMenu?.moveToPage(2)
                    wSelf.statusView = .payment
                case .payment:
                    wSelf.paymentVC.createOrderEvent.onNext(())
                }
            }.disposed(by: wSelf.disposeBag)
        }
        
        let btBackUpload = self.btBackUpload.rx.tap.map{ ViewBottomStatus.uploadImage }
        let btBackPayment = self.btBackPayment.rx.tap.map{ ViewBottomStatus.payment }
        Observable.merge(btBackUpload, btBackPayment).bind { [weak self] stt in
            guard let wSelf = self else { return }
            switch stt {
            case .uploadImage:
                wSelf.pageMenu?.moveToPage(0)
                wSelf.statusView = .select
            case .payment:
                wSelf.pageMenu?.moveToPage(1)
                wSelf.statusView = .uploadImage
            case .select: break
            }
        }.disposed(by: self.disposeBag)
        
        self.$statusView.asObservable().bind { [weak self] stt in
            guard let wSelf = self else { return }
            switch stt {
            case .select:
                wSelf.bottomViews[ViewBottomStatus.select.rawValue].isHidden = false
                wSelf.bottomViews[ViewBottomStatus.uploadImage.rawValue].isHidden = true
                wSelf.bottomViews[ViewBottomStatus.payment.rawValue].isHidden = true
                
                wSelf.btsHeader[ViewBottomStatus.uploadImage.rawValue].backgroundColor = UIColor.init(named: "fd799dColor") ?? .white
                wSelf.btsHeader[ViewBottomStatus.uploadImage.rawValue].setTitleColor(.white, for: .normal)
                wSelf.btsHeader[ViewBottomStatus.payment.rawValue].backgroundColor = UIColor.init(named: "fd799dColor") ?? .white
                wSelf.btsHeader[ViewBottomStatus.payment.rawValue].setTitleColor(.white, for: .normal)
            case .uploadImage:
                wSelf.bottomViews[ViewBottomStatus.uploadImage.rawValue].isHidden = false
                wSelf.bottomViews[ViewBottomStatus.select.rawValue].isHidden = true
                wSelf.bottomViews[ViewBottomStatus.payment.rawValue].isHidden = true
                
                wSelf.btsHeader[ViewBottomStatus.uploadImage.rawValue].backgroundColor = .white
                wSelf.btsHeader[ViewBottomStatus.uploadImage.rawValue].setTitleColor(UIColor.init(named: "fd799dColor"), for: .normal)
                wSelf.btsHeader[ViewBottomStatus.payment.rawValue].backgroundColor = UIColor.init(named: "fd799dColor") ?? .white
                wSelf.btsHeader[ViewBottomStatus.payment.rawValue].setTitleColor(.white, for: .normal)
            case .payment:
                wSelf.bottomViews[ViewBottomStatus.payment.rawValue].isHidden = false
                wSelf.bottomViews[ViewBottomStatus.select.rawValue].isHidden = true
                wSelf.bottomViews[ViewBottomStatus.uploadImage.rawValue].isHidden = true
                
                wSelf.btsHeader[ViewBottomStatus.uploadImage.rawValue].backgroundColor = .white
                wSelf.btsHeader[ViewBottomStatus.uploadImage.rawValue].setTitleColor(UIColor.init(named: "fd799dColor"), for: .normal)
                wSelf.btsHeader[ViewBottomStatus.payment.rawValue].backgroundColor = .white
                wSelf.btsHeader[ViewBottomStatus.payment.rawValue].setTitleColor(UIColor.init(named: "fd799dColor"), for: .normal)
            }
        }.disposed(by: self.disposeBag)
        
        self.sideTrigger
            .witElementhUnretained(self)
            .bind { isSide in
                self.leftContainerView.constant = (!isSide) ? 16 : 0
                self.rightContainerView.constant = (!isSide) ? 16 : 0
        }.disposed(by: disposeBag)
    }
    
    func passValueOrderToRepay(order: ManageOrderModel) {
        self.paymentVC.reorder = order
        self.uploadImageVC.reorder = order
        self.uploadImageVC.nameFrame = order.frame
        self.statusView = .uploadImage
        self.uploadImageVC.getImages(order: order, hideViewSelect: true)
        self.uploadImageVC.getFrame()
    }
    
    func passValueOrder(order: ManageOrderModel) {
        self.paymentVC.order = order
        self.statusView = .uploadImage
        self.uploadImageVC.nameFrame = order.frame
        self.uploadImageVC.getImages(order: order, hideViewSelect: false)
        self.uploadImageVC.getFrame()
    }

    func addNavBarImage() {

            let navController = navigationController!

            let image = R.image.logoBabyface() //Your logo url here
            let imageView = UIImageView(image: image)

            let bannerWidth = navController.navigationBar.frame.size.width
            let bannerHeight = navController.navigationBar.frame.size.height

            let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
            let bannerY = bannerHeight / 2 - (image?.size.height)! / 2

            imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth - 20, height: bannerHeight - 20)
            imageView.contentMode = .scaleAspectFit

            navigationItem.titleView = imageView
        }
    

    func setupPageMenu(){
        var controllerArray : [UIViewController] = []
        
        selectPrintVC = SelectPrintViewController.init(nib: R.nib.selectPrintViewController)
        selectPrintVC.delegate = self
        controllerArray.append(selectPrintVC)
        
        uploadImageVC = UploadImageViewController.init(nib: R.nib.uploadImageViewController)
        uploadImageVC.delegate = self
        controllerArray.append(uploadImageVC)
        
        paymentVC = PaymentViewController.init(nib: R.nib.paymentViewController)
        self.paymentVC.delegate = self
        controllerArray.append(paymentVC)
        
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor.init(hexString: AppColor.colorBGMenu, alpha: 1.0)!),
            .selectionIndicatorColor(UIColor.init(hexString: AppColor.colorYellowDark, alpha: 1.0)!),
            .menuItemFont(UIFont.init(name: AppFont.HelveticaNeueBold, size: 16)!),
            .unselectedMenuItemLabelColor(UIColor.init(hexString: AppColor.colorBlack, alpha: 0.5)!),
            .selectedMenuItemLabelColor(UIColor.black),
            .menuHeight(0),
            .menuItemSeparatorWidth(0),
            .selectionIndicatorHeight(3),
            .menuItemWidth(80),
            .enableHorizontalBounce(false),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .centerMenuItems(true)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray,
                                frame: CGRect(x: 0.0, y: 0.0, width: self.containerView.frame.width,
                                              height: self.containerView.frame.height),
                                pageMenuOptions: parameters)
        pageMenu?.delegate = self
        
        pageMenu?.controllerScrollView.isScrollEnabled = false
        pageMenu!.view.subviews.forEach { v in
            v.backgroundColor = .clear
        }
        containerView.addSubview(pageMenu!.view)
        if let page = self.pageMenu {
            page.view.backgroundColor = R.color.f1F1F1()
        }
    }
    
    
}
extension BabyFaceViewController: SelectPrintDelegate {
    func moveToUpload() {
        self.pageMenu?.moveToPage(ViewBottomStatus.uploadImage.rawValue)
        self.uploadImageVC.tutorialBabyImage()
        self.statusView = .uploadImage
        self.selectPrintVC.selectImageView.pauseVideoSelect()
    }
    
    func selecPrintWillAppear() {
    }
    
    func updateSelect(price: PricePrintModel) {
        if price.sale > 0 {
            self.saleDiscountView.isHidden = false
            self.saleView.isHidden = false
            self.lbRawPrice.isHidden = true
            self.lbPrice.text = "\(price.showPricce())"
            let percent = price.sale * 100
            self.lbSale.text = "-\(Int(percent))%"
            self.lbSaleDiscount.text = "\(price.priceFormat)"
            self.paymentVC.priceName = price.name
        } else {
            self.lbPrice.text = "\(price.showPricce())"
            self.paymentVC.priceName = price.name
            self.saleDiscountView.isHidden = true
            self.saleView.isHidden = true
            self.lbRawPrice.isHidden = false
        }
        
    }
}

//MARK: CAPSPageMenu Delegate
extension BabyFaceViewController: CAPSPageMenuDelegate{

    func willMoveToPage(_ controller: UIViewController, index: Int) {
        switch ViewBottomStatus.init(rawValue: index) {
        case .select:
            self.sideTrigger.accept(true)
        case .uploadImage, .payment:
            self.sideTrigger.accept(false)
            self.selectPrintVC.pauseVideo()
            self.delegate?.pauseVideoBabyface()
        case .none:  break
        }
    }
}
extension BabyFaceViewController: PaymentVỉeDelegate {
    func paymentWillAppera() {
    }
    
    func disableButtonPayment(disable: Bool) {
        self.bts[ViewBottomStatus.payment.rawValue].isEnabled = !disable
    }
}
extension BabyFaceViewController: UploadImageViewDelegate {
    func updateImageWillAppear() {
    }
}
