//
//  WebViewViewController.swift
//  Beberia
//
//  Created by Lap on 07/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import WebKit


extension WKWebView {
    class func clean() {
        guard #available(iOS 9.0, *) else {return}

        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)

        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                #if DEBUG
                    print("WKWebsiteDataStore record deleted:", record)
                #endif
            }
        }
    }
}

class WebViewViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    var linkPayment = ""
    var updateResultVNPay: (String, String)->() = {_,_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: linkPayment)
        let request = URLRequest(url: url!)
        
        WKWebView.clean()
        webView.navigationDelegate = self
        webView.load(request)
    }
    
    @IBAction func didPressBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getResultVNPay(code: Int)-> String{
        switch  code{
        case 00:
           return "Giao dịch thành công"
        case 01:
           return "Giao dịch đã tồn tại"
        case 02:
           return "Merchant không hợp lệ (kiểm tra lại vnp_TmnCode)"
        case 03:
           return "Dữ liệu gửi sang không đúng định dạng"
        case 04:
           return "Khởi tạo GD không thành công do Website đang bị tạm khóa"
        case 05:
           return "Giao dịch không thành công do: Quý khách nhập sai mật khẩu quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch"
        case 13:
           return "Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch."
        case 07:
           return "Giao dịch bị nghi ngờ là giao dịch gian lận"
        case 09:
           return "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng."
        case 10:
           return "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần"
        case 11:
           return "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch."
        case 12:
           return "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa."
        case 51:
           return "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch."
        case 65:
           return "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày."
        case 08:
           return "Giao dịch không thành công do: Hệ thống Ngân hàng đang bảo trì. Xin quý khách tạm thời không thực hiện giao dịch bằng thẻ/tài khoản của Ngân hàng này."
        case 99:
           return "Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê)"
            
        default:
            return ""
        }
    }
}

extension WebViewViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        guard let url = webView.url?.absoluteString else {
            return
        }
        if url.contains("https://babyface.beberia.me/success") {
            
            
            let arrayString = url.components(separatedBy: "&")
             for item in arrayString {
                 if item.contains("vnp_ResponseCode"){
                     let array = item.components(separatedBy: "=")
                    
                    Utils.getTopMostViewController()?.dismiss(animated: true, completion: {
                        self.updateResultVNPay(self.getResultVNPay(code: Int(array[1])!), webView.url?.absoluteString ?? "")
                    })

                 }
             }
        }
        
      
        // https://api.beberia.me/api/v1/babyface/paymentResults?vnp_Amount=50000000&vnp_BankCode=NCB&vnp_BankTranNo=20210511121527&vnp_CardType=ATM&vnp_OrderInfo=Thanh+to%C3%A1n+baby+face&vnp_PayDate=20210511121501&vnp_ResponseCode=00&vnp_TmnCode=PGLVGCNR&vnp_TransactionNo=13502632&vnp_TxnRef=10&vnp_SecureHashType=SHA256&vnp_SecureHash=8f2b987e027781df7890403bbdcc59088d5424152f3c95f610a47562f8fa77ed
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
       
        print("Finished loading")
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
}
