//
//  PaymentViewController.swift
//  Beberia
//
//  Created by Lap on 06/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import IQKeyboardManagerSwift
import RxSwift

protocol PaymentVỉeDelegate {
    func disableButtonPayment(disable: Bool)
    func paymentWillAppera()
}

class PaymentViewController: BaseViewController {
    
    enum LabelHeaderStatus: Int, CaseIterable {
        case name, numberPhone, email
    }
    
    var order: ManageOrderModel?
    var reorder: ManageOrderModel?
    var priceName: String?
    var delegate: PaymentVỉeDelegate?

    @IBOutlet weak var btnMomo: UIButton!
    @IBOutlet weak var btnAirPay: UIButton!
    @IBOutlet weak var btnPayVNPay: UIButton!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var tvMessage: UITextView!
    @IBOutlet weak var tfTinh: UITextField!
    @IBOutlet weak var tfDictris: UITextField!
    @IBOutlet weak var tfAdress: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var btnBoy: UIButton!
    @IBOutlet weak var btnGirl: UIButton!
    @IBOutlet weak var lblErrorPhone: UILabel!
    @IBOutlet weak var lblErrorEmail: UILabel!
    @IBOutlet weak var lblErrorName: UILabel!
    @IBOutlet weak var lblErrorAdress: UILabel!
    @IBOutlet var headerLbs: [UILabel]!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var item_id = 0
    var filesChild = [UIImage]()
    var filesParent = [UIImage]()
    var locationFeed = CityName.init(json: "")
    var districtModel = DistrictModel.init(json: "")
    var noticeResult = ""
    var pay_type = 1
    let createOrderEvent: PublishSubject<Void> = PublishSubject.init()
    private var orderId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.delegate?.paymentWillAppera()
    }
    
    @IBAction func didPressTinh(_ sender: Any) {
        self.view.endEditing(true)
        let locationVC = LocationViewController.init(nib: R.nib.locationViewController)
        locationVC.didSelectedLocation = { city in
            self.locationFeed = city
            self.tfTinh.text = city.locationNameVi ?? ""
            self.districtModel = DistrictModel.init(json: "")
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    @IBAction func didPressDistric(_ sender: Any) {
        self.view.endEditing(true)
        let districVC = DistrictViewController.init(nib: R.nib.districtViewController)
        districVC.locationID = locationFeed.locationCode ?? 0
        districVC.isHaveDistric = false
        districVC.hidesBottomBarWhenPushed = true
        districVC.didSelectedLocation = { city in
            self.districtModel = city
            self.tfDictris.text = city.districtName ?? ""
            self.tfAdress.becomeFirstResponder()
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(districVC, animated: true)
    }
    @IBAction func didPressDevli(_ sender: Any) {
        let view = ViewTutorialUpdateImageBaby.instanceFromNib()
        view.showData(type: .devli)
        let _ = self.showPopup(view: view, height: 400)
    }
    
    func showButtonSelecGender(index: Int){
            btnBoy.setImage(index == 5 ? R.image.group15221() : R.image.group15264(), for: .normal)
            btnGirl.setImage(index == 6 ? R.image.group15221() : R.image.group15264(), for: .normal)
    }
    
    func showButtonPayment(index: Int){
       // btnMomo.setImage(R.image.icPayMomo(), for: .normal)
      //  btnAirPay.setImage(R.image.icPayAirPay(), for: .normal)
        btnPayVNPay.setImage(R.image.img_vnpay(), for: .normal)
        
        if index == 1 {
            btnPayVNPay.setImage(R.image.img_vnpay(), for: .normal)
        } else if index == 2 {
      //      btnAirPay.setImage(R.image.icPayAirPayActive(), for: .normal)
        } else {
      //      btnMomo.setImage(R.image.icPayMomoActive(), for: .normal)
        }
        
        pay_type = index
    }
    
    @IBAction func didPressPay(_ sender: UIButton) {
        showButtonPayment(index: sender.tag)
    }
    
    @IBAction func didPressSelectGender(_ sender: UIButton) {
        showButtonSelecGender(index: sender.tag)
    }
    
    func isValidPhone(testStr:String) -> Bool {
        let phoneRegEx = "^[0]\\d{9}$"
        var phoneNumber = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneNumber.evaluate(with: testStr)
    }
    
}
extension PaymentViewController {
    
    private func setupUI() {
        showButtonSelecGender(index: 5)
        showButtonPayment(index: pay_type)
        
        guard let topVC = Utils.getTopMostViewController() as? BabyFaceViewController,let price = topVC.selectPrintVC.itemPrice else {
            return
        }
        
        lblPrice.text = "\(price.showPricce())"
        lblTotalPrice.text = "\(price.showPricce())"
        
        tfAdress.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.resgiterPhone))
        tfPhoneNumber.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(self.resgiterEmail))
        
        self.setupLabelTextProcess()
        self.setupLabelTextNumberPhone()
        self.setupLabelTextEmail()
        
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: BabyFaceViewController.Constant.bottomView, right: 0)
        
        if let order = self.order {
            self.tfName.text = order.contactName
            self.tfPhoneNumber.text = order.phone
            self.tfEmail.text = order.contactEmail
            self.tvMessage.text = order.message
        }
        
        if let order = self.reorder {
            self.tfName.text = order.contactName
            self.tfPhoneNumber.text = order.phone
            self.tfEmail.text = order.contactEmail
            self.tvMessage.text = order.message
        }
        
    }
    
    private func setupRX() {
        self.createOrderEvent.asObservable().bind { [weak self] _ in
            guard let wSelf = self else { return }
            if let order = wSelf.reorder {
                wSelf.createReOrder(orderId: order.id ?? 0)
            } else if let orderId = wSelf.orderId {
                wSelf.createReOrder(orderId: orderId)
            } else {
                wSelf.createOrder()
            }
            
        }.disposed(by: self.disposeBag)
        
        tfPhoneNumber.rx.text.skip(1).map({ text in
            return (self.isValidPhone(testStr: text!), text!.count == 0 ? 0 : 1)
        }).subscribe { isCheckValid, isEmpty in
            //  guard let textPhone = isCheckValid else { return }
            if isEmpty == 0 {
                self.lblErrorPhone.text = "Vui lòng nhập số điện thoại"
            } else {
                self.lblErrorPhone.text = isCheckValid ? "" : "Vui lòng nhập đúng định dạng số điện thoại"
            }
            
        }.disposed(by: disposeBag)
        
        tfEmail.rx.text.skip(1).map({ text in
            return (Utils.isValid(text!), text!.count == 0 ? 0 : 1)
        }).subscribe { isCheckValid, isEmpty in
            //  guard let textPhone = isCheckValid else { return }
            if isEmpty == 0 {
                self.lblErrorEmail.text = "Vui lòng nhập email"
            } else {
                self.lblErrorEmail.text = isCheckValid ? "" : "Vui lòng nhập đúng định dạng email"
            }
            
        }.disposed(by: disposeBag)
        
        tfAdress.rx.text.skip(1).map({ text in
            return text!.count == 0
        }).subscribe {isEmpty in
            guard let isEmpty = isEmpty.element else { return }
            self.lblErrorAdress.text = !isEmpty ? "" : "Vui lòng nhập địa chỉ"
            
        }.disposed(by: disposeBag)
        
        tfName.rx.text.skip(1).map({ text in
            return text!.count == 0
        }).subscribe {isEmpty in
            guard let isEmpty = isEmpty.element else { return }
            self.lblErrorName.text = !isEmpty ? "" : "Vui lòng nhập tên"
            
        }.disposed(by: disposeBag)
    }
    
    
    @objc func resgiterPhone(){
        tfPhoneNumber.becomeFirstResponder()
    }
    
    @objc func resgiterEmail(){
//        tfEmail.becomeFirstResponder()
    }
    
    private func setupLabelTextProcess() {
    
        let attrs1 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 12),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "fd799dColor")]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 12),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "FF0046")]
        
        let attributedString1 = NSMutableAttributedString(string:"Họ & tên ", attributes:attrs1 as [NSAttributedString.Key : Any])
        
        let attributedString2 = NSMutableAttributedString(string:"*", attributes:attrs2 as [NSAttributedString.Key : Any])
        
        attributedString1.append(attributedString2)
        self.headerLbs[LabelHeaderStatus.name.rawValue].attributedText = attributedString1
    }
    
    private func setupLabelTextNumberPhone() {
    
        let attrs1 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 12),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "fd799dColor")]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 12),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "FF0046")]
        
        let attributedString1 = NSMutableAttributedString(string:"Số điện thoại ", attributes:attrs1 as [NSAttributedString.Key : Any])
        
        let attributedString2 = NSMutableAttributedString(string:"*", attributes:attrs2 as [NSAttributedString.Key : Any])
        
        attributedString1.append(attributedString2)
        self.headerLbs[LabelHeaderStatus.numberPhone.rawValue].attributedText = attributedString1
    }
    
    private func setupLabelTextEmail() {
    
        let attrs1 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 12),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "fd799dColor")]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 12),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "FF0046")]
        
        let attributedString1 = NSMutableAttributedString(string:"Email *", attributes:attrs1 as [NSAttributedString.Key : Any])
        
        let attributedString2 = NSMutableAttributedString(string:"*", attributes:attrs2 as [NSAttributedString.Key : Any])
        
        attributedString1.append(attributedString2)
        self.headerLbs[LabelHeaderStatus.email.rawValue].attributedText = attributedString1
    }
    
}

extension PaymentViewController {
    
    func createReOrder(orderId: Int){
        guard let topVC = Utils.getTopMostViewController() as? BabyFaceViewController,
              let nameFrame = topVC.uploadImageVC.framePrice?.name
        else {
            return
        }
        
        guard let name = tfName.text , name.count > 0 else {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập tên của bạn")
            return
        }
        
        guard let phoneNumber = tfPhoneNumber.text , phoneNumber.count > 0 else {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập số điện thoại")
            return
        }
        
//        guard let email = tfEmail.text , email.count > 0 else {
//            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập Email")
//            return
//        }
        
        if !self.isValidPhone(testStr: phoneNumber) {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập đúng định dạng số điện thoại")
            return
        }
        
//        if !Utils.isValid(email) {
//            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập đúng định dạng Email")
//            return
//        }
        
        guard let mess = self.tvMessage.text, mess.count <= ConstantApp.shared.maxMess else {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập lời nhắn nhỏ hơn 20 ký tự")
            return
        }
        
        SVProgressHUD.show()
        self.delegate?.disableButtonPayment(disable: true)
        APIManager.createReOrder(orderId: orderId ,
                                 contactEmail: "",
                                 contactName: name,
                                 phone: phoneNumber,
                                 message: mess,
                                 filesChild: topVC.uploadImageVC.imageBabys,
                                 imagelink: [],
                                 pay_type: pay_type,
                                 frame: nameFrame,
                                 gender: topVC.uploadImageVC.gender.keyAPI) { orderModel in
            print(orderModel.payment_url)
            
            SVProgressHUD.dismiss()
            self.delegate?.disableButtonPayment(disable: false)
            
            let webView = WebViewViewController.init(nib: R.nib.webViewViewController)
            webView.linkPayment = orderModel.payment_url
            webView.modalPresentationStyle = .overFullScreen
            webView.updateResultVNPay = { [weak self] string, urlString in
                guard let wSelf = self else { return }
                wSelf.noticeResult = string
                print(string)
                if string == "Giao dịch thành công"{
                    let orderStatus = OrderStatusViewController.init(nib: R.nib.orderStatusViewController)
                    let rootView = UINavigationController.init(rootViewController: orderStatus)
                    orderStatus.linkUrl = urlString
                    orderStatus.priceName = wSelf.priceName
                    wSelf.order = nil
                    wSelf.tfName.text = nil
                    wSelf.tfPhoneNumber.text = nil
                    wSelf.tfEmail.text = nil
                    wSelf.tvMessage.text = nil
                    
                    rootView.modalPresentationStyle = .fullScreen
                    Utils.getTopMostViewController()?.present(rootView, animated: true, completion: nil)
                } else {
                    let vc = PopupCancelationOrder.createVC()
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overFullScreen
                    vc.deleegate = self
                    vc.statusView = .paymentFail
                    vc.subPayment = string
                    wSelf.present(vc, animated: true, completion: nil)
                }
                
            }
            
            Utils.getTopMostViewController()?.present(webView, animated: true, completion: nil)
            
        } failed: { error in
            print(error)
            SVProgressHUD.dismiss()
            self.delegate?.disableButtonPayment(disable: false)
        }
        
    }
    
    func createOrder(){
        
//        let webView = WebViewViewController.init(nib: R.nib.webViewViewController)
//   //     webView.linkPayment = orderModel.payment_url
//        webView.modalPresentationStyle = .overFullScreen
//        webView.updateResultVNPay = { string, urlString in
//            self.noticeResult = string
//            print(string)
//            if string == "Giao dịch thành công"{
//                let orderStatus = OrderStatusViewController.init(nib: R.nib.orderStatusViewController)
//                let rootView = UINavigationController.init(rootViewController: orderStatus)
//                orderStatus.linkUrl = urlString
//       //         orderStatus.linkFrame = nameFrame
//                rootView.modalPresentationStyle = .overFullScreen
//                Utils.getTopMostViewController()?.present(rootView, animated: true, completion: nil)
//            } else {
//                let view = PopupOrderResult.instanceFromNib()
//                view.showData(textString: string)
//                let _ = self.showPopup(view: view, height: 300)
//            }
//
//
//        }
//
//        Utils.getTopMostViewController()?.present(webView, animated: true, completion: nil)

//        return
        
        guard let topVC = Utils.getTopMostViewController() as? BabyFaceViewController,
              let nameFrame = topVC.uploadImageVC.framePrice?.name,
              let linkFrame = topVC.uploadImageVC.framePrice?.link_sample
              else {
            return
        }
        
        

        guard let name = tfName.text , name.count > 0 else {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập tên của bạn")
            return
        }
        
//        guard let codeCity = self.locationFeed.locationCode else {
//            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng chọn tỉnh")
//            return
//        }
        
//        guard let codeDistrict = self.districtModel.districtCode else {
//            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng chọn phường xã")
//            return
//        }

//        guard let adress = tfAdress.text , adress.count > 0 else {
//            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập địa chỉ")
//            return
//        }
        
        guard let phoneNumber = tfPhoneNumber.text , phoneNumber.count > 0 else {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập số điện thoại")
            return
        }
        
//        guard let email = tfEmail.text , email.count > 0 else {
//            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập Email")
//            return
//        }
        
        if !self.isValidPhone(testStr: phoneNumber) {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập đúng định dạng số điện thoại")
            return
        }
        
//        if !Utils.isValid(email) {
//            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập đúng định dạng Email")
//            return
//        }
        
        guard let mess = self.tvMessage.text, mess.count <= ConstantApp.shared.maxMess else {
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: "Vui lòng nhập lời nhắn nhỏ hơn 20 ký tự")
            return
        }
      //  LoaddingManager.startLoadding()
        
        SVProgressHUD.show()
        self.delegate?.disableButtonPayment(disable: true)
        APIManager.createOrder(item_id: topVC.selectPrintVC.itemPrice?.itemID ?? 0,
                               contactEmail: "",
                               contactName: name,
                               phone: phoneNumber,
                               address: nil,
                               message: mess,
                               filesChild: topVC.uploadImageVC.imageBabys,
                               filesParent: topVC.uploadImageVC.imageDadMoms,
                               frame: nameFrame,
                               city_code: nil,
                               district_code: nil,
                               pay_type: pay_type,
                               gender: topVC.uploadImageVC.gender.keyAPI) { [weak self] orderModel in
            guard let wSelf = self else { return }
            print(orderModel.payment_url)
            wSelf.orderId = orderModel.id
            SVProgressHUD.dismiss()
            wSelf.delegate?.disableButtonPayment(disable: false)

            let webView = WebViewViewController.init(nib: R.nib.webViewViewController)
            webView.linkPayment = orderModel.payment_url
            webView.modalPresentationStyle = .overFullScreen
            webView.updateResultVNPay = { [weak self] string, urlString in
                guard let wSelf = self else { return }
                wSelf.noticeResult = string
                print(string)
                if string == "Giao dịch thành công"{
                    let orderStatus = OrderStatusViewController.init(nib: R.nib.orderStatusViewController)
                    let rootView = UINavigationController.init(rootViewController: orderStatus)
                    orderStatus.linkUrl = urlString
                    orderStatus.linkFrame = linkFrame
                    orderStatus.priceName = wSelf.priceName
                    wSelf.order = nil
                    wSelf.reorder = nil
                    wSelf.tfName.text = nil
                    wSelf.tfPhoneNumber.text = nil
                    wSelf.tfEmail.text = nil
                    wSelf.orderId = nil
                    rootView.modalPresentationStyle = .fullScreen
                    Utils.getTopMostViewController()?.present(rootView, animated: true, completion: nil)
                } else {
                    let vc = PopupCancelationOrder.createVC()
                    vc.modalTransitionStyle = .crossDissolve
                    vc.modalPresentationStyle = .overFullScreen
                    vc.deleegate = self
                    vc.statusView = .paymentFail
                    vc.subPayment = string
                    wSelf.present(vc, animated: true, completion: nil)
                }

            }

            Utils.getTopMostViewController()?.present(webView, animated: true, completion: nil)

        } failed: { error in
            print(error)
            SVProgressHUD.dismiss()
            Utils.showAlertOKWithoutAction(controller: self, title: R.string.localizable.commonNoti(), message: error)
        self.delegate?.disableButtonPayment(disable: false)
        }

    }
}
extension PaymentViewController: PopupCancelationOrderDelegate {
    func actionPopup(action: PopupCancelationOrder.Action) {
        switch action {
        case .homeBaby:
            guard let topVC = Utils.getTopMostViewController() as? BabyFaceViewController else {
                return
            }
            self.view.endEditing(true)
            topVC.statusView = .select
            topVC.pageMenu?.moveToPage(0)
            topVC.selectPrintVC.scrollView.setContentOffset(.zero, animated: true)
        case .orders:
            guard let topVC = Utils.getTopMostViewController() as? BabyFaceViewController else {
                return
            }
            self.view.endEditing(true)
            let vc = OrdersVC.createVC()
            vc.hidesBottomBarWhenPushed = true
            topVC.navigationController?.pushViewController(vc, animated: true)
        default: break
        }
    }
}
