//
//  GuidePointModel.swift
//  Beberia
//
//  Created by haiphan on 17/12/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import Foundation
// MARK: - WelcomeData
struct ListRevew: Codable {
    let datas: [Renew]?
    enum CodingKeys: String, CodingKey {
        case datas = "datas"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        datas = try values.decodeIfPresent([Renew].self, forKey: .datas)
    }
}

// MARK: - DataElement
struct Renew: Codable {
    let image: String?
    let content, user: String?
    enum CodingKeys: String, CodingKey {
        case image = "image"
        case content = "content"
        case user = "user"
    }
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        content = try values.decodeIfPresent(String.self, forKey: .content)
        user = try values.decodeIfPresent(String.self, forKey: .user)
    }
    
}
