//
//  SelectPrintViewController.swift
//  Beberia
//
//  Created by Lap on 06/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import youtube_ios_player_helper
import Photos

protocol SelectPrintDelegate {
    func updateSelect(price: PricePrintModel)
    func selecPrintWillAppear()
    func moveToUpload()
}

class SelectPrintViewController: BaseViewController {
    
    enum ActionRateView {
        case left, right
    }
    
    struct Constant {
        static let offSetToStop: CGFloat = 0
        static let textMonth: String = "Tháng"
    }
    
    var delegate: SelectPrintDelegate?
    
    //    @IBOutlet weak var playView: YTPlayerView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var heightTbv: NSLayoutConstraint!
    @IBOutlet weak var tbvPrince: UITableView!
    @IBOutlet weak var lblDateDeliv: UILabel!
    @IBOutlet weak var lblDateFinish: UILabel!
    //  @IBOutlet weak var heightClv: NSLayoutConstraint!
    //    @IBOutlet weak var clvImage: UICollectionView!
    //    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var reviewCollection: UICollectionView!
    @IBOutlet weak var lbTextProcess: UILabel!
    @IBOutlet weak var contentStackView: UIView!
    @IBOutlet weak var contentVideoView: UIView!
    @IBOutlet weak var contentQuestionView: UIView!
    @IBOutlet weak var contentPrivacyView: UIView!
    @IBOutlet weak var contentDesBabyFace: UIView!
    @IBOutlet weak var contentIntroduceView: UIView!
    @IBOutlet weak var contentSelectImage: UIView!
    @IBOutlet weak var contentRateView: UIView!
    @IBOutlet weak var contentExperienceView: UIView!
    @IBOutlet weak var btLeftRateView: UIButton!
    @IBOutlet weak var brRightRateView: UIButton!
    private var listReviewEvent: BehaviorRelay<[Renew]> = BehaviorRelay.init(value: [])
    private let questionView: QuestionsView = .loadXib()
    private let descriptionView: DesBabyFaceView = .loadXib()
    private let privacyView: PrivacyView = .loadXib()
    private let introduceView: IntroduceBabeFace = .loadXib()
    private let experienceView: ExperienceView = .loadXib()
    let selectImageView: SelectImageView = .loadXib()
    var playVideo = AVPlayer()
    private var offSet: CGPoint?
    private let tapVideo: UITapGestureRecognizer = UITapGestureRecognizer()
    
    var listPrices = BehaviorRelay.init(value: [PricePrintModel]())
    var itemPrice: PricePrintModel?
    var dataImage = BehaviorRelay.init(value: [R.image.baby1(),R.image.baby2(),R.image.baby3(),R.image.baby4(),R.image.baby5(),R.image.baby6(),R.image.baby7(),R.image.baby8(),R.image.baby9(),R.image.baby10()])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        initRx()
        getListPrice()
        
        let date = Calendar.current.date(byAdding: .day, value: 3, to: Date())!
        let nextDate = Calendar.current.date(byAdding: .day, value: 8, to: Date())!
        lblDateFinish.text = String(format: date.toString(format: .EEddThangMM) , Constant.textMonth)
        lblDateDeliv.text = String(format: nextDate.toString(format: .EEddThangMM) , Constant.textMonth)
        
        self.videoView.addGestureRecognizer(self.tapVideo)
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.delegate?.selecPrintWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.pauseVideo()
    }
    
}

extension SelectPrintViewController {
    
    private func setupUI() {
        self.logoView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.scrollView.delegate = self
        self.offSet = self.scrollView.contentOffset
        self.reviewCollection.register(R.nib.reviewSelectCell)
        self.reviewCollection.delegate = self
        
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: BabyFaceViewController.Constant.bottomView, right: 0)
        self.setupLabelTextProcess()
        
        self.getListRenew()
        
        guard let path = Bundle.main.path(forResource: "video_select_print", ofType:"mp4")else {
            debugPrint("video.m4v not found")
            return
        }
        let url = URL(fileURLWithPath: path)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.playVideo(url: url)
        }
        self.contentQuestionView.addSubview(self.questionView)
        self.questionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        self.contentPrivacyView.addSubview(self.privacyView)
        self.privacyView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        self.contentDesBabyFace.addSubview(self.descriptionView)
        self.descriptionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.descriptionView.delegate = self
        
        self.contentIntroduceView.addSubview(self.introduceView)
        self.introduceView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.introduceView.delegate = self
        
        self.contentSelectImage.addSubview(self.selectImageView)
        self.selectImageView.snp.makeConstraints { make in
            make.left.top.right.equalToSuperview()
            make.bottom.equalToSuperview().inset(5)
        }
        
        self.contentExperienceView.addSubview(self.experienceView)
        self.experienceView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        
        self.view.backgroundColor = .clear
        
    }
    
    //    func playVideo(){
    //      //  videoPlayer.delegate = self
    ////        let playvarsDic = ["controls": 1, "playsinline": 1, "autohide": 1, "showinfo": 0, "autoplay": 0, "modestbranding": 1, "startSeconds": 12.6]
    ////        //video play example: M7lc1UVf-VE
    ////        playView.load(withVideoId: "Q27x3Q_Pfps", playerVars: playvarsDic)
    //    }
    
    
    func initRx(){
        
        // MARK:  CollectionView Topic Edit Photo
        //        let flowLayoutTocpicEditPhoto = UICollectionViewFlowLayout()
        //        let widthCell = self.clvImage.frame.height
        //        flowLayoutTocpicEditPhoto.minimumInteritemSpacing = 5
        //        flowLayoutTocpicEditPhoto.minimumLineSpacing = 5
        //        flowLayoutTocpicEditPhoto.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //        flowLayoutTocpicEditPhoto.itemSize = CGSize(width: widthCell , height: self.clvImage.frame.height)
        //        flowLayoutTocpicEditPhoto.scrollDirection = .horizontal
        //        clvImage.setCollectionViewLayout(flowLayoutTocpicEditPhoto, animated: true)
        //        clvImage.showsVerticalScrollIndicator = false
        //        clvImage.register(R.nib.selectImageEditCollectionViewCell)
        //        clvImage.clipsToBounds = false
        //        clvImage.layer.masksToBounds = false
        //
        //        imgBaby.image = self.dataImage.value[0]
        
        self.listReviewEvent.asObservable()
            .bind(to: self.reviewCollection.rx.items(cellIdentifier: R.reuseIdentifier.reviewSelectCell.identifier,
                                                     cellType: ReviewSelectCell.self)) { row, data, cell in
                cell.setupValue(value: data)
            }.disposed(by: disposeBag)
        
        //        self.dataImage.asObservable().bind(to: self.clvImage.rx.items(cellIdentifier: R.reuseIdentifier.selectImageEditCollectionViewCell.identifier, cellType: SelectImageEditCollectionViewCell.self)) { [weak self]  row, data, cell in
        //            guard let _ = self else { return }
        //
        //            cell.thumbnailImage = data
        //
        //
        //        }.disposed(by: disposeBag)
        //
        //        Observable.zip(clvImage.rx.itemSelected, clvImage.rx.modelSelected(UIImage.self))
        //        .bind { [weak self] indexPath, item in
        //            guard let self = self else { return }
        //
        //            self.imgBaby.image = item
        //        }
        //        .disposed(by: disposeBag)
        
        // Bind To TableView
        tbvPrince.register(R.nib.priceTableViewCell)
        tbvPrince.rowHeight = 50
        
        listPrices.asObservable().bind(to: self.tbvPrince.rx.items(cellIdentifier: R.reuseIdentifier.priceTableViewCell.identifier, cellType: PriceTableViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
            cell.setupData(priceModel: data)
        }.disposed(by: disposeBag)
        
        tbvPrince.rx.observe(CGSize.self, "contentSize").asObservable().observeOn(MainScheduler.asyncInstance).subscribe { [weak self] (size) in
            guard let self = self else { return }
            self.heightTbv.constant = size.element!!.height
        }.disposed(by: disposeBag)
        
        Observable.zip(tbvPrince.rx.itemSelected, tbvPrince.rx.modelSelected(PricePrintModel.self))
            .bind { [weak self] indexPath, item in
                //  tbvPrince.des
                guard let self = self else { return }
                
                var arrayTEmp = self.listPrices.value
                for var item in arrayTEmp {
                    item.isSelected = false
                }
                arrayTEmp[indexPath.row].isSelected = true
                self.listPrices.accept(arrayTEmp)
                self.itemPrice = item
            }.disposed(by: disposeBag)
        
        let _ = NotificationCenter.default.rx
            .notification(NSNotification.Name.AVPlayerItemDidPlayToEndTime)
            .bind { [weak self] notify in
                guard  let wSelf = self else { return }
                wSelf.playVideo.seek(to: CMTime.zero)
                wSelf.playVideo.play()
            }.disposed(by: disposeBag)
        
        self.tapVideo.rx.event.asObservable().bind { [weak self] _ in
            guard let wSelf = self else { return }
            if wSelf.playVideo.rate != 0 {
                wSelf.playVideo.pause()
            } else {
                wSelf.playVideo.play()
            }
        }.disposed(by: self.disposeBag)
        
        let _ = NotificationCenter.default.rx.notification(UIApplication.didEnterBackgroundNotification).bind { [weak self] _ in
            guard let wSelf = self else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                wSelf.playVideo.pause()
            }
        }.disposed(by: self.disposeBag)
        
        let left = self.btLeftRateView.rx.tap.map { ActionRateView.left }
        let right = self.brRightRateView.rx.tap.map { ActionRateView.right }
        Observable.merge(left, right)
            .withUnretained(self)
            .bind { owner, type in
                let list = owner.reviewCollection.indexPathsForVisibleItems.map { $0.item }
                guard let min = list.min(),
                      let max = list.max() else {
                    return
                }
                owner.reviewCollection.scrollToIndex(index: (type == .left) ? min : max , direction: .hozontical)
            }.disposed(by: disposeBag)
        
    }
    
    private func playVideo(url: URL) {
        self.playVideo = AVPlayer(url: url)
        let playerLayer = AVPlayerLayer(player: self.playVideo)
        playerLayer.frame = videoView.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resize
        self.videoView.layoutIfNeeded()
        playerLayer.layoutIfNeeded()
        self.videoView.layer.addSublayer(playerLayer)
    }
    
    func pauseVideo() {
        self.playVideo.pause()
    }
    
    func getListPrice(){
        APIManager.getPrice(page: 1) { [weak self] prices in
            guard let wSelf = self else { return }
            if prices.count > 0 {
                var priceTemp = prices
                priceTemp[0].isSelected = true
                wSelf.itemPrice = priceTemp[0]
                wSelf.listPrices.accept(priceTemp)
                wSelf.delegate?.updateSelect(price: priceTemp[0])
            }
            
        } failed: { error in
            print(error)
        }
        
    }
    
    private func getListRenew() {
        APIManager.review { [weak self] list in
            guard let wSelf = self else { return }
            wSelf.listReviewEvent.accept(list)
        } failed: { error in
            print(error)
        }
    }
    
    private func setupLabelTextProcess() {
        
        let attrs1 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 16),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "5D5B5B")]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Bold", size: 16),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "4D4D4D")]
        
        let attrs3 = [NSAttributedString.Key.font : UIFont.init(name: "NotoSans-Regular", size: 16),
                      NSAttributedString.Key.foregroundColor : UIColor.init(named: "5D5B5B")]
        
        let attributedString1 = NSMutableAttributedString(string:"Từ ảnh siêu âm từ tuần thai 25 trở đi, ", attributes:attrs1 as [NSAttributedString.Key : Any])
        
        let attributedString2 = NSMutableAttributedString(string:"Babyface", attributes:attrs2 as [NSAttributedString.Key : Any])
        let attributedString3 = NSMutableAttributedString(string:" kết hợp công nghệ AI được ứng dụng để nhận diện, phân tích các đặc điểm trên ảnh siêu âm để tạo ra gương mặt em bé.", attributes:attrs3 as [NSAttributedString.Key : Any])
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        self.lbTextProcess.attributedText = attributedString1
    }
}
extension SelectPrintViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 300, height: self.reviewCollection.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}
extension SelectPrintViewController: UIScrollViewDelegate {
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < Constant.offSetToStop {
            self.killScroll(scrollView: scrollView)
        }
        
        //        let viewFrame = scrollView.convert(self.videoView.bounds, from: self.contentVideoView)
        //                if viewFrame.intersects(scrollView.bounds) {
        //                    // targetView is visible
        //                    print("targetView is visible")
        //                }
        //                else {
        //                    // targetView is not visible
        //                    print("targetView is not visible")
        //                }
        let positionY = self.contentSelectImage.frame.origin.y + self.contentSelectImage.frame.origin.y
        let start = self.scrollView.frame.height + scrollView.contentOffset.y - BabyFaceViewController.Constant.bottomView
        print("======= self.contentStackView.frame.origin.y \(self.contentStackView.frame.origin.y)")
        print("======= self.contentSelectImage.frame.origin.y \(self.contentSelectImage.frame.origin.y)")
        print("======= positionY \(positionY)")
        if start > positionY && scrollView.contentOffset.y < positionY + self.videoView.frame.height {
            //            self.playVideo.play()
            self.selectImageView.playVideoSelect()
        } else {
            self.selectImageView.pauseVideoSelect()
            //            self.playVideo.pause()
        }
    }
    
    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        return false
    }
    private func killScroll(scrollView: UIScrollView) {
        if let off = self.offSet {
            scrollView.setContentOffset(off, animated: false)
        }
    }
}

extension Int {
    func formatString(separator: String = ".")->String?{
        let formater = NumberFormatter()
        formater.groupingSeparator = separator
        formater.numberStyle = .decimal
        return formater.string(from: NSNumber(value: self))
    }
}
public extension UIScrollView {
    
    /// Returns `adjustedContentInset` on iOS >= 11 and `contentInset` on iOS < 11.
    var fullContentInsets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return adjustedContentInset
        } else {
            return contentInset
        }
    }
    
    /// Visible content frame. Equal to bounds without insets.
    var visibleContentFrame: CGRect {
        bounds.inset(by: fullContentInsets)
    }
}
extension SelectPrintViewController: BabyFaceDelegate {
    func pauseVideoBabyface() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.pauseVideo()
        }
    }
}
extension SelectPrintViewController: DesBabyDelegate {
    func moveToUploadImage() {
        self.delegate?.moveToUpload()
    }
}
extension SelectPrintViewController: IntroduceBabeFaceDelegate {
    func selectAction(action: IntroduceBabeFace.Action) {
        UIView.animate(withDuration: 0.6) {
            switch action {
            case .introduce:
                self.scrollView.contentOffset.y = self.contentIntroduceView.frame.origin.y
            case .question:
                self.scrollView.contentOffset.y = self.contentPrivacyView.frame.origin.y
            case .rate:
                self.scrollView.contentOffset.y = self.contentRateView.frame.origin.y
                
            }
            self.view.layoutIfNeeded()
        }
    }
}
