//
//  ReviewSelectCell.swift
//  Beberia
//
//  Created by haiphan on 01/01/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import UIKit

class ReviewSelectCell: UICollectionViewCell {
    
    enum LikeStatus: Int, CaseIterable {
        case one = 1, two, three, four, five
    }
    
    struct Constant {
        static let xShadow: CGFloat = 0
        static let yShadow: CGFloat = 1
        static let spreadShadow: CGFloat = 0
        static let blur: CGFloat = 12
    }
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet var imgs: [UIImageView]!
    @IBOutlet weak var lbContent: UILabel!
    @IBOutlet weak var imgRevew: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.clipsToBounds = true
        self.layer.cornerRadius = 8
        self.dropShadow(offsetX: 0, offsetY: 1, color: UIColor.init(named: "141F3C08") ?? .white, opacity: 6, radius: 1)
        
        self.imgRevew.clipsToBounds = true
        self.imgRevew.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

}
extension ReviewSelectCell {
    
    func setupRate(value: Int) {
        imgs.enumerated().forEach { item in
            let offSet = item.offset + 1 
            let element = item.element
            element.isHidden = (offSet <= value) ? false : true
        }
    }
    
    func setupValue(value: Renew) {
        self.lbName.text = value.user
        self.lbContent.text = value.content
        if let textURL = value.image, let url = URL(string: textURL) {
            self.imgRevew.kf.setImage(with: url)
        }
    }
}
