//
//  PriceTableViewCell.swift
//  Beberia
//
//  Created by Lap on 08/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit

class PriceTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!
    @IBOutlet weak var widthPrice: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    func setupData(priceModel :PricePrintModel){
        
        lblName.text = priceModel.name
        let image = priceModel.isSelected ? R.image.ic_select_price_cell() : R.image.activedCard()
        imgSelect.image = image
        lblPrice.text = "\(priceModel.showPricce())"
        self.widthPrice.constant = self.lblPrice.sizeThatFits(lblPrice.bounds.size).width
    }
}
