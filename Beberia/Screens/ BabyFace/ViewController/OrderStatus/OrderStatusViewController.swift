//
//  OrderStatusViewController.swift
//  Beberia
//
//  Created by Lap on 19/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }

    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
}

import UIKit
import RxSwift

class OrderStatusViewController: UIViewController {

    @IBOutlet weak var lblFrame: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCodeOrder: UILabel!
    @IBOutlet weak var lblDateOrder: UILabel!
    @IBOutlet weak var lblDateDeliv: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lbPriceName: UILabel!
    @IBOutlet weak var btOrder: UIButton!
    @IBOutlet weak var tvNote: UITextView!
    
    private let disposeBag = DisposeBag()
    var linkUrl = ""
    var linkFrame = ""
    var priceName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func didPressBack(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            guard let topVC = Utils.getTopMostViewController() as? BabyFaceViewController else {
                return
            }
            topVC.statusView = .select
            topVC.pageMenu?.moveToPage(0)
            topVC.uploadImageVC.resetValue()
            topVC.selectPrintVC.scrollView.setContentOffset(.zero, animated: true)
        })
    }
    
    // https://babyface.beberia.me/success?vnp_Amount=50000000&vnp_BankCode=NCB&vnp_BankTranNo=20210519103937&vnp_CardType=ATM&vnp_OrderInfo=Thanh%20to%C3%A1n%20baby%20face&vnp_PayDate=20210519103919&vnp_ResponseCode=00&vnp_TmnCode=BEBERIA2&vnp_TransactionNo=13506908&vnp_TxnRef=1&vnp_SecureHashType=SHA256&vnp_SecureHash=65f534b59119a62ebb493ba5a85f70d3a978c091f82d6d5715ad9e549a004ab2
}
extension OrderStatusViewController {
    
    private func setupUI() {
        self.addNavBarImage()
        lblFrame.setImage(imageString: linkFrame)
        
        lblDateDeliv.text = Utils.dateToString(date: Calendar.current.date(byAdding: .day, value: 4, to: Date())!, format: "dd/MM/yyyy")
        
        let arrayString = linkUrl.components(separatedBy: "&")
        for item in arrayString {
            if item.contains("vnp_Amount"){
                let array = item.components(separatedBy: "=")
                lblPrice.text = "\(((Double(array[1]) ?? 0)  / 100).currency)"
            }
            
            if item.contains("vnp_PayDate"){
                let array = item.components(separatedBy: "=")
                lblDateOrder.text = "\(array[1].substring(with: 6..<8))/\(array[1].substring(with: 4..<6))/\(array[1].substring(to: 4)) \(array[1].substring(with: 8..<10)):\(array[1].substring(with: 10..<12))"
            }
            
            if item.contains("vnp_TxnRef"){
                let array = item.components(separatedBy: "=")
                lblCodeOrder.text = "BABY\(array[1])BEBERIA"
            }
        }
        self.lbPriceName.text = self.priceName ?? "In cơ bản"
        self.contentView.clipsToBounds = true
        self.contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        self.setupTextViewNote()
    }
    
    private func setupRX() {
        self.btOrder.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.dismiss(animated: true, completion: {
                guard let topVC = Utils.getTopMostViewController() as? BabyFaceViewController else {
                    return
                }
                let orders = OrdersVC.init(nib: R.nib.ordersVC)
                orders.hidesBottomBarWhenPushed = true
                topVC.navigationController?.pushViewController(orders, animated: true)
            })
        }.disposed(by: self.disposeBag)
    }
    
    func addNavBarImage() {
        
        let navController = navigationController!
        
        let image = R.image.logoBabyface() //Your logo url here
        let imageView = UIImageView(image: image)
        
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth - 20, height: bannerHeight - 20)
        imageView.contentMode = .scaleAspectFit
        
        navigationItem.titleView = imageView
    }
    
    private func setupTextViewNote() {
        let attrs1 = [NSAttributedString.Key.font : R.font.notoSansSemiBold(size: 13),
                      NSAttributedString.Key.foregroundColor : R.color.d5B5B()]
        
        let attrs2 = [NSAttributedString.Key.font : R.font.notoSansRegular(size: 13),
                      NSAttributedString.Key.foregroundColor : R.color.d5B5B()]
        
        let attrs3 = [NSAttributedString.Key.font : R.font.notoSansBold(size: 13),
                      NSAttributedString.Key.foregroundColor : R.color.d5B5B()]
        
        let attributedString1 = NSMutableAttributedString(string:"Lưu ý\n", attributes:attrs1 as [NSAttributedString.Key : Any])
        
        let attributedString2 = NSMutableAttributedString(string:"1. Sau khi thanh toán, các mẹ hãy ấn vào ", attributes:attrs2 as [NSAttributedString.Key : Any])
        let attributedString3 = NSMutableAttributedString(string:"“Trạng thái đơn hàng” ", attributes:attrs3 as [NSAttributedString.Key : Any])
        let attributedString4 = NSMutableAttributedString(string:"hoặc vào mục ", attributes:attrs2 as [NSAttributedString.Key : Any])
        let attributedString5 = NSMutableAttributedString(string:"“Tiện ích” ", attributes:attrs3 as [NSAttributedString.Key : Any])
        let attributedString6 = NSMutableAttributedString(string:"> ", attributes:attrs2 as [NSAttributedString.Key : Any])
        let attributedString7 = NSMutableAttributedString(string:"“Quản lý đơn hàng”", attributes:attrs3 as [NSAttributedString.Key : Any])
        let attributedString8 = NSMutableAttributedString(string:"để theo dõi đơn của mình.\n\n", attributes:attrs2 as [NSAttributedString.Key : Any])
        let t1: String = "“Đã gửi”"
        let t: String = "2. Sau khi đơn hàng chuyển sang trạng thái \(t1) (trong vòng 5 ngày) thì mẹ hãy kiểm tra email, kết quả có thể bị trôi vào phần thư rác/ quảng cáo. Nếu vẫn chưa thấy kết quả thì mẹ hãy liên hệ qua inbox fanpage Beberia để được hỗ trợ kịp thời.\n\n3. Sau 2 ngày đăng ký, nếu muốn huỷ đơn mẹ hãy liên hệ qua inbox fanpage Beberia để được hỗ trợ huỷ đơn, hoàn tiền\n\n4. Thời gian hoàn tiền :\n- Thẻ nội địa / ví điện tử từ 3-8 ngày làm việc\n- Thẻ quốc tế tối đa 90 ngày làm việc  thời gian cụ thể do mỗi ngân hàng và tổ chức phát hành thẻ quy định"
        let attributedString9 = NSMutableAttributedString(string:"\(t)", attributes:attrs2 as [NSAttributedString.Key : Any])
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        attributedString1.append(attributedString4)
        attributedString1.append(attributedString5)
        attributedString1.append(attributedString6)
        attributedString1.append(attributedString7)
        attributedString1.append(attributedString8)
        attributedString1.append(attributedString9)
        self.tvNote.attributedText = attributedString1
    }
}
