//
//  UploadImageViewController.swift
//  Beberia
//
//  Created by Lap on 06/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import DKImagePickerController
import Photos
import MobileCoreServices
import SVProgressHUD

protocol UploadImageViewDelegate: AnyObject {
    func updateImageWillAppear()
}

class UploadImageViewController: BaseViewController {
    
    struct Constant {
        static let sizeCellFrame: CGSize = CGSize(width: 56, height: 84)
    }
    
    var reorder: ManageOrderModel?
    var nameFrame: String?
    var delegate: UploadImageViewDelegate?

   // @IBOutlet weak var heightViewimageDadMom: NSLayoutConstraint!
    @IBOutlet weak var heihgtViewImagebaby: NSLayoutConstraint!
  //  @IBOutlet weak var viewSelectedDadMom: UIView!
    @IBOutlet weak var viewSelectedImageBaby: UIView!
    @IBOutlet weak var viewImage: ViewImage!
    @IBOutlet weak var imgFrame: UIImageView!
    @IBOutlet weak var clvFrame: UICollectionView!
  //  @IBOutlet weak var viewImageDadMom: ViewImage!
    @IBOutlet weak var btSelectImage: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var btsBaby: [UIButton]!
    @IBOutlet weak var btSeeMore: UIButton!
    
    var dataFrames = BehaviorRelay.init(value: [FrameBabyface]())
    var framePrice: FrameBabyface?
    var isSelected = false
    
    
    var dkAsset = [DKAsset]()
    
    var imageArrayDefault:[UIImage] = [R.image.img_placeHolder_upload()!,
                                       R.image.img_placeHolder_upload()!,
                                       R.image.img_placeHolder_upload()!,
                                       R.image.img_placeHolder_upload()!,
                                       R.image.img_placeHolder_upload()!]
    var gender: GenderBaby = .unknown
    var imageBabys = [UIImage]()
    {
        didSet {
            viewSelectedImageBaby.isHidden = imageBabys.count > 0
            viewImage.isHidden = imageBabys.count == 0

        }
    }
    var imageDadMoms = [UIImage]()
    {
        didSet {
//            viewSelectedDadMom.isHidden = imageDadMoms.count > 0
//            viewImageDadMom.isHidden = imageDadMoms.count == 0
        }
    }
    
    var imageBabysTemp = [UIImage]()
    var imageMomDadsTemp = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewImage.isFromViewBaby = true
        viewImage.tag = 4
     //   viewImageDadMom.isFromViewBaby = true
   //     viewImageDadMom.tag = 5
    //    heightViewimageDadMom.constant = UIScreen.main.bounds.width - 32
        DispatchQueue.main.async {
            self.heihgtViewImagebaby.constant = self.calculateHeightViewBaby()
        }
        
//        imageBabys = imageArrayDefault
    //    imageDadMoms = imageArrayDefault
        self.viewImage.images = imageBabys
    //    self.viewImageDadMom.images = imageDadMoms
        self.setupUI()
        initRx()
        getFrame()
    }
    
    @IBAction func didPressTutorialBabys(_ sender: Any) {
        self.tutorialBabyImage()
    }
    
    func tutorialBabyImage() {
        let view = ViewTutorialUpdateImageBaby.instanceFromNib()
        view.showData(type: .baby)
        let viewPopup = self.showPopup(view: view, height: 400)
        view.actionDismiss = {
            viewPopup.removeFromSuperview()
        }
    }
    
//    @IBAction func didPressTutorialDadMom(_ sender: Any) {
//        let view = ViewTutorialUpdateImageBaby.instanceFromNib()
//        view.showData(type: .dadmom)
//        let _ = self.showPopup(view: view, height: 400)
//        self.didPRessSeleectImage(tagView: 0)
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.delegate?.updateImageWillAppear()
        if #available(iOS 15.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            // Fallback on earlier versions
        }
    }
}


extension UploadImageViewController {
    
    private func setupUI() {
        let flowLayoutTocpicEditPhoto = UICollectionViewFlowLayout()
        flowLayoutTocpicEditPhoto.itemSize = Constant.sizeCellFrame
        flowLayoutTocpicEditPhoto.scrollDirection = .horizontal
        flowLayoutTocpicEditPhoto.minimumInteritemSpacing = 8
        clvFrame.setCollectionViewLayout(flowLayoutTocpicEditPhoto, animated: true)
        clvFrame.showsVerticalScrollIndicator = false
        clvFrame.register(R.nib.frameBabyfaceCollectionViewCell)
        clvFrame.clipsToBounds = false
        clvFrame.layer.masksToBounds = false
        
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: BabyFaceViewController.Constant.bottomView, right: 0)
        
        self.stateButtonBaby(gender: self.gender)
        
        self.btSeeMore.contentHorizontalAlignment = .right
        
    }
    
    
    func initRx(){
        // MARK:  CollectionView Topic Edit Photo
        self.btSelectImage.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.didPRessSeleectImage(tagView: 4)
        }.disposed(by: self.disposeBag)
        
        self.dataFrames.asObservable().bind(to: self.clvFrame.rx.items(cellIdentifier: R.reuseIdentifier.frameBabyfaceCollectionViewCell.identifier, cellType: FrameBabyfaceCollectionViewCell.self)) { [weak self]  row, data, cell in
            guard let _ = self else { return }
            
            cell.setupData(data: data)
            
        }.disposed(by: disposeBag)
        
        Observable.zip(clvFrame.rx.itemSelected, clvFrame.rx.modelSelected(FrameBabyface.self))
            .bind { [weak self] indexPath, item in
           
                guard let self = self else { return }
                
              
                var arrayTEmp = self.dataFrames.value
                
                for (index, _) in arrayTEmp.enumerated() {
                    arrayTEmp[index].isSelected = false
                }
                arrayTEmp[indexPath.row].isSelected = true
                
                self.dataFrames.accept(arrayTEmp)
                
                self.framePrice = item
                self.imgFrame.setImage(url: URL.init(string: item.link_sample)!)
                
        }.disposed(by: disposeBag)
        
        
        viewImage.actionDelete = { index in
            self.imageBabys.remove(at: index)
            
            self.imageBabys.append(R.image.img_placeHolder_upload()!)
            self.imageBabysTemp.remove(at: index)
            
            self.viewImage.images = self.imageBabys
 
        }
        
//        viewImageDadMom.actionDelete = { index in
//            self.imageDadMoms.remove(at: index)
//            self.imageDadMoms.append(R.image.group_15334()!)
//            self.imageMomDadsTemp.remove(at: index)
//
//            self.viewImageDadMom.images = self.imageDadMoms
//
//        }
        
        viewImage.tapImageBaby = { viewTag in
            self.didPRessSeleectImage(tagView: viewTag)
        }
        
//        viewImageDadMom.tapImageBaby = { viewTag in
//            self.didPRessSeleectImage(tagView: viewTag)
//        }
        
        GenderBaby.allCases.forEach { type in
            let bt = self.btsBaby[type.rawValue]
            bt.rx.tap.bind { [weak self] _ in
                guard let wSelf = self else { return }
                wSelf.gender = type
                wSelf.stateButtonBaby(gender: type)
            }.disposed(by: self.disposeBag)
        }
        
    }
    
    private func stateButtonBaby(gender: GenderBaby) {
        switch gender {
        case .male:
            self.btsBaby[GenderBaby.male.rawValue].setImage(R.image.img_select_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.female.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.unknown.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
        case .female:
            self.btsBaby[GenderBaby.male.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.female.rawValue].setImage(R.image.img_select_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.unknown.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
        case .unknown:
            self.btsBaby[GenderBaby.male.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.female.rawValue].setImage(R.image.img_unselect_button_pregnant(), for: .normal)
            self.btsBaby[GenderBaby.unknown.rawValue].setImage(R.image.img_select_button_pregnant(), for: .normal)
        }
    }
    
    private func calculateHeightViewBaby() -> CGFloat {
        let height = UIScreen.main.bounds.width - CanvaseeLayout.Constant.distanceSides
        let widthCellHeader = (height - (CanvaseeLayout.Constant.cellPadding)) / CanvaseeLayout.Constant.totalCelTop
        let widthCellSub = (height - (CanvaseeLayout.Constant.cellPadding * 2)) / CanvaseeLayout.Constant.totalCellBot
        return widthCellHeader + widthCellSub + CanvaseeLayout.Constant.cellPadding
    }
    
    func handelSelectImage(){
        
    }
    
    func didPRessSeleectImage(tagView: Int) {
        
        if tagView == 4 {
            
            DKImagePickerControllerResource.customLocalizationBlock = { title in
                if title == "picker.select.maxLimitReached.error.message" {
                    return "Bạn có thể chọn tối đa %@ ảnh"
                } else {
                    return nil
                }
            }
            
            let babyPickerController = DKImagePickerController()
            babyPickerController.assetType = .allPhotos
            babyPickerController.maxSelectableCount = 5 - self.imageBabysTemp.count
            babyPickerController.didSelectAssets = { [weak self] (assets: [DKAsset])  in
                guard let selfVC = self  else {
                    return
                }
                
                selfVC.dkAsset = assets
                
                print("didSelectAssets")
                if assets.count > 0 {
                    
                    //  print(assets)
                    let requestOptions = PHImageRequestOptions()
                    requestOptions.deliveryMode = .highQualityFormat
                    requestOptions.isSynchronous = false
                    requestOptions.isNetworkAccessAllowed = true
                    
                    var imageTemp = [UIImage]()
                    
                    for (_,asset) in assets.enumerated() {
                        SVProgressHUD.show()
                        asset.fetchOriginalImage(options: requestOptions, completeBlock: { [weak self] image, info in
                            guard let image = image?.resizedTo5MB() else {
                                SVProgressHUD.dismiss()
                                return
                            }
                            selfVC.imageBabysTemp.append(image)
                            
                            imageTemp.append(image)
                            
                            if imageTemp.count == assets.count {
                                self?.imageBabys = selfVC.imageBabysTemp
                                
                                if (self?.imageBabysTemp.count)! < 6 {
                                    for _ in (self?.imageBabysTemp.count)!...5 {
                                        self?.imageBabys.append(R.image.img_placeHolder_upload()!)
                                    }
                                }
                                
                                selfVC.viewImage.images = selfVC.imageBabys
                            }
                            SVProgressHUD.dismiss()
                        })
                    }
                }
                
            }
            Utils.getTopMostViewController()?.present(babyPickerController, animated: true) {}
            
        } else {
            
            //            let dadMomPickerController = DKImagePickerController()
            //            dadMomPickerController.assetType = .allPhotos
            //            dadMomPickerController.maxSelectableCount = 5 - self.imageMomDadsTemp.count
            //            dadMomPickerController.didSelectAssets = { [weak self] (assets: [DKAsset])  in
            //                guard let selfVC = self  else {
            //                    return
            //                }
            //                print("didSelectAssets")
            //                if assets.count > 0{
            //                  //  print(assets)
            //                    let requestOptions = PHImageRequestOptions()
            //                    requestOptions.deliveryMode = .highQualityFormat
            //                    requestOptions.isSynchronous = true
            //                    requestOptions.isNetworkAccessAllowed = true
            //                    for asset in assets {
            //                        asset.fetchOriginalImage(options: requestOptions, completeBlock: { [weak self] image, info in
            //                            guard let image = image?.resizedTo5MB() else { return }
            //                            selfVC.imageMomDadsTemp.append(image)
            //
            //                        })
            //                    }
            //
            //                    self?.imageDadMoms = selfVC.imageMomDadsTemp
            //
            //                    if (self?.imageMomDadsTemp.count)! < 6 {
            //                        for _ in (self?.imageMomDadsTemp.count)!...5 {
            //                            self?.imageDadMoms.append(R.image.group_15334()!)
            //                        }
            //                    }
            //
            //                       selfVC.viewImageDadMom.images = selfVC.imageDadMoms
            //                }
            //            }
            //            Utils.getTopMostViewController()?.present(dadMomPickerController, animated: true) {}
        }
        
    }
    
    func getImages(order: ManageOrderModel?, hideViewSelect: Bool) {
        SVProgressHUD.show()
        var imageTemp = [UIImage]()
        if let order = order {
            self.viewSelectedImageBaby.isHidden = hideViewSelect
            self.viewImage.isHidden = !hideViewSelect
            if hideViewSelect {
                self.imageBabys = self.imageBabysTemp
                
                if (self.imageBabysTemp.count) < 6 {
                    for _ in (self.imageBabysTemp.count)...5 {
                        self.imageBabys.append(R.image.img_placeHolder_upload()!)
                    }
                }
                
                self.viewImage.images = self.imageBabys
                SVProgressHUD.dismiss()
                order.images?.forEach({ textURL in
                    SVProgressHUD.show()
                    DispatchQueue.global().sync {
                        if let url = URL(string: textURL) {
                            do {
                                let data = try Data(contentsOf: url)
                                if let img = UIImage(data: data) {
                                    DispatchQueue.main.async {
                                        self.imageBabysTemp.append(img)
                                        imageTemp.append(img)
                                        self.imageBabys = self.imageBabysTemp
                                        
                                        if (self.imageBabysTemp.count) < 6 {
                                            for _ in (self.imageBabysTemp.count)...5 {
                                                self.imageBabys.append(R.image.img_placeHolder_upload()!)
                                            }
                                        }
                                        
                                        self.viewImage.images = self.imageBabys
                                        SVProgressHUD.dismiss()
                                    }
                                }
                            } catch {
                                SVProgressHUD.dismiss()
                                print("\(error.localizedDescription)")
                            }
                        }

                    }
                })
            }
        } else {
            self.viewSelectedImageBaby.isHidden = false
            self.viewImage.isHidden = true
            SVProgressHUD.dismiss()
        }
        
        

        

//        if imageTemp.count == assets.count {
//            self?.imageBabys = selfVC.imageBabysTemp
//
//            if (self?.imageBabysTemp.count)! < 6 {
//                for _ in (self?.imageBabysTemp.count)!...5 {
//                    self?.imageBabys.append(R.image.img_placeHolder_upload()!)
//                }
//            }
//
//            selfVC.viewImage.images = selfVC.imageBabys
//        }
    }
    
    func resetValue() {
        self.reorder = nil
        self.viewSelectedImageBaby.isHidden = false
        self.viewImage.isHidden = true
        self.imageBabys = []
        self.imageBabysTemp = []
        self.viewImage.images = []
        self.nameFrame = nil
        self.getFrame()
    }
    
    func getFrame(){
        SVProgressHUD.show()
        APIManager.getListFrame { [weak self] frames in
            guard let wSelf = self else { return }
            if let name = wSelf.nameFrame, let index = frames.firstIndex(where: { $0.name == name }) {
                var framesTemp = frames
                framesTemp[index].isSelected = true
                wSelf.framePrice = framesTemp[index]
                wSelf.dataFrames.accept(framesTemp)
                wSelf.imgFrame.setImage(url: URL.init(string: framesTemp[index].link_sample)!)
            } else if frames.count > 0 {
                var framesTemp = frames
                framesTemp[0].isSelected = true
                wSelf.framePrice = framesTemp[0]
                wSelf.dataFrames.accept(framesTemp)
                wSelf.imgFrame.setImage(url: URL.init(string: framesTemp[0].link_sample)!)
            }
            SVProgressHUD.dismiss()
        } failed: { error in
            print(error)
            SVProgressHUD.dismiss()
        }

    }
}
