//
//  FrameBabyfaceCollectionViewCell.swift
//  Beberia
//
//  Created by Lap on 08/05/2021.
//  Copyright © 2021 IMAC. All rights reserved.
//

import UIKit

class FrameBabyfaceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var btnSelected: UIButton!
    @IBOutlet weak var imgFrame: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.clipsToBounds = true
        self.imgFrame.clipsToBounds = true
        self.imgFrame.layer.cornerRadius = 4
    }
    
    func setupData(data: FrameBabyface){
        imgFrame.setImage(imageString: data.link_frame)
        btnSelected.setImage(R.image.group15264(), for: .normal)
        imgFrame.layer.borderWidth = 0
        imgFrame.layer.borderColor = UIColor.clear.cgColor
        
        if data.isSelected {
            btnSelected.setImage(R.image.ic_select_price_cell(), for: .normal)
            imgFrame.layer.borderWidth = 1
            imgFrame.layer.borderColor = UIColor.init(named: "fd799dColor")?.cgColor
        }
    }

}
