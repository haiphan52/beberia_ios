//
//  BaseViewController.swift
//  Beberia
//
//  Created by IMAC on 8/14/19.
//  Copyright © 2019 IMAC. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftEntryKit
import TTGTagCollectionView
import RxSwift
import RxCocoa
import FFPopup

class BaseViewController: UIViewController, UITabBarControllerDelegate, UIGestureRecognizerDelegate {
    
    var actionRightBarButton: ()->() = {}
    var disposeBag = DisposeBag()
    var qc = 0
    var active = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.delegate = self
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            if let navBar = self.navigationController {
                let bar = navBar.navigationBar
                bar.standardAppearance = appearance
                bar.scrollEdgeAppearance = appearance
            }

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationVer2()
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            if let navBar = self.navigationController {
                let bar = navBar.navigationBar
                bar.standardAppearance = appearance
                bar.scrollEdgeAppearance = appearance
            }

        }
    }
    
    func removeBorder(font: UIFont, bgColor: UIColor, textColor: UIColor) {
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            let atts = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]
            appearance.configureWithOpaqueBackground()
            appearance.configureWithTransparentBackground()
            appearance.titleTextAttributes = atts
            appearance.backgroundColor = bgColor
            if let navBar = self.navigationController {
                let bar = navBar.navigationBar
                bar.standardAppearance = appearance
                bar.scrollEdgeAppearance = appearance
            }
        } else {
            self.navigationController?.hideHairline()
        }
    }
    
    func navigationBarCustom(font: UIFont,
                             bgColor: UIColor,
                             textColor: UIColor,
                             isTranslucent: Bool = true) {
        self.navigationController?.navigationBar.isTranslucent = isTranslucent
        self.navigationController?.navigationBar.barTintColor = bgColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: textColor,
                                                                        NSAttributedString.Key.font: font]
        self.setupNavigationBariOS15(font: font,
                                     bgColor: bgColor,
                                     textColor: textColor,
                                     isTranslucent: isTranslucent)
    }
    
    func setupNavigationBariOS15(font: UIFont, bgColor: UIColor, textColor: UIColor, isTranslucent: Bool = true) {
        if #available(iOS 15.0, *) {
            let atts = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.titleTextAttributes = atts
            appearance.backgroundColor = bgColor
            
            if let navBar = self.navigationController {
                let bar = navBar.navigationBar
                bar.standardAppearance = appearance
                bar.scrollEdgeAppearance = appearance
//                bar.compactScrollEdgeAppearance = appearance
            }
            
        }
    }
    
    func setupNavigationVer2() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                                                                        NSAttributedString.Key.font: UIFont.notoSansFont(weight: .bold, size: 18)]
    }
    
    func customLeftBarButtonVer2(imgArrow: UIImage){
        let buttonLeft = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 44, height: 44)))
        buttonLeft.setImage(imgArrow, for: .normal)
        buttonLeft.contentEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        let leftBarButton = UIBarButtonItem(customView: buttonLeft)
        navigationItem.leftBarButtonItem = leftBarButton
        buttonLeft.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.navigationController?.popViewController(animated: true, {
            })
        }.disposed(by: disposeBag)
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func customLeftBarButton(){
        let leftBarButtonItem = UIBarButtonItem.menuButton(self, action: #selector(BaseViewController.backVC), imageName: R.image.ic_Back() ?? UIImage.init(), height: 30, width: 40, view: self.view)
        
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    func customRightBarButton(image: UIImage){
           let leftBarButtonItem = UIBarButtonItem.menuButton(self, action: #selector(BaseViewController.actionRightBar), imageName: image, height: 30, width: 40, view: self.view)
           
           self.navigationItem.rightBarButtonItem = leftBarButtonItem
       }
    
    func customRightBarButtonWithText(title: String){
        let leftBarButtonItem = UIBarButtonItem.init(title: title, style: .done, target: self, action: #selector(BaseViewController.actionRightBar))
        self.navigationItem.rightBarButtonItem?.tintColor = .red
        self.navigationItem.rightBarButtonItem = leftBarButtonItem
    }
    
    func customRighBarView(view : UIView){
    
        (view as! ViewSelectedBabyRightBar).actionRightBar = {
            self.actionRightBarButton()
        }
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let barButtonItem = UIBarButtonItem.init(customView: view)
        barButtonItem.customView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BaseViewController.onBarButtonItemClicked)))
        self.navigationItem.rightBarButtonItem = barButtonItem
    }
    
    func customLeftBarView(view : UIView){
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let barButtonItem = UIBarButtonItem.init(customView: view)
        barButtonItem.customView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BaseViewController.onBarButtonItemClicked)))
        self.navigationItem.leftBarButtonItem = barButtonItem
    }
    
    
    
    @objc func onBarButtonItemClicked(){
        actionRightBarButton()
    }
    
    func customHideLeftBarButton(){
        let leftBarButtonItem = UIBarButtonItem.menuButton(self, action: #selector(BaseViewController.hideVC), imageName:  UIImage.init(), height: 30, width: 40, view: self.view)
        
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    func customTitleView(view: UIView){
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        let viewTitle = view
        viewTitle.frame = titleView.bounds
        titleView.addSubview(viewTitle)
        
        viewTitle.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        navigationItem.titleView = viewTitle
    }
    
    @objc func actionRightBar(){
        actionRightBarButton()
    }
    
    @objc func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func hideVC(){
        
    }
    
    func showPopup(view: UIView, height: CGFloat) -> FFPopup{
        view.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width - 52, height: height)
        let popup = FFPopup.init(contentView: view)
        popup.showType = .growIn
        popup.dismissType = .shrinkOut
        let layout = FFPopupLayout(horizontal: .center, vertical: .center)
        popup.show(layout: layout)
        return popup
    }
    
    func showToastPopup(view: UIView) {
        
        var attributes = EKAttributes.topFloat
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 12, offset: .zero))
        attributes.displayDuration = 3
        attributes.position = .top
        SwiftEntryKit.display(entry: view, using: attributes)
        
    }
    
    func showNotiCreatePost(isEdit: Bool, isFeed: Bool){
        let viewPopupDelete =  ViewNotificationCreatePost.instanceFromNib()
        if isFeed{
            viewPopupDelete.lblNoti.text = isEdit ? R.string.localizable.homeSaveHome() : R.string.localizable.homeCreateHome()
        }else{
            viewPopupDelete.lblNoti.text = isEdit ? R.string.localizable.diarySaveDiary() : R.string.localizable.diaryCreateDiary()
        }
        
        var attributes = EKAttributes()
        attributes.displayDuration = 5
        attributes.position = .top
        SwiftEntryKit.display(entry: viewPopupDelete, using: attributes)
    }
    
    func dismissPopup(){
        SwiftEntryKit.dismiss()
    }
    
    func setupTagView(data: [String], tagView: TTGTextTagCollectionView){
        //        tagView.addTags([locations![0].locationNameVi, locations![1].locationNameVi])
        tagView.addTags(data)
        let config = tagView.defaultConfig!
        config.textColor = UIColor.init(hexString: AppColor.colorSelect,alpha: 0.3)
        config.selectedTextColor = UIColor.init(hexString: AppColor.colorYellow)
        config.backgroundColor = UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.4)
        config.selectedBackgroundColor = UIColor.init(hexString: AppColor.colorBGMenu)
        config.borderColor = UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.5)
        config.selectedBorderColor = UIColor.init(hexString: AppColor.colorYellow)
        config.borderWidth = 1
        config.selectedBorderWidth = 1
        config.cornerRadius = 15
        config.selectedCornerRadius = 15
        config.shadowOffset = CGSize.zero
        config.shadowRadius = 0
        config.shadowOpacity = 0
        config.exactHeight = 30
        tagView.enableTagSelection = true
        tagView.defaultConfig = config
        //        tagView.delegate = self
        tagView.reload()
    }
    
    func getCheckCompleteDiary(id: Int, isEdit: Bool = true ,completion: @escaping (_ homeFeeds: Diaries)->()){
        self.showNotiCreatePost(isEdit: isEdit, isFeed: false)
        let _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { (timer) in
            APIManager.checkCompletedDiary(id: id, callbackSuccess: { [weak self] (diary, status) in
                guard let self = self else { return }
                if status == 1{
                    completion(diary)
                    timer.invalidate()
                    self.dismissPopup()
                }
            }) { (error) in
                timer.invalidate()
                self.dismissPopup()
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            }
        }
    }
    
    func getCheckCompleteFeed(id: Int, isEdit: Bool = true, completion: @escaping (_ homeFeeds: HomeFeed)->()){
        self.showNotiCreatePost(isEdit: isEdit, isFeed: true)
        let _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { (timer) in
            APIManager.checkCompletedFeed(id: id, callbackSuccess: { [weak self] (feed, status) in
                guard let self = self else { return }
                if status == 1{
                    completion(feed)
                    timer.invalidate()
                    self.dismissPopup()
                }
            }) { (error) in
                timer.invalidate()
                self.dismissPopup()
                Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            }
        }
    }
    
    func getDetailDiary(array: [Diaries] ,idDiary: Int,_ indexRow: Int, editFromView: EditFromView,commentCommentNotification: Comment? = nil,tableView: UITableView, completion: @escaping (_ homeFeeds: [Diaries])->()){
        var arrayTemp = array
        tableView.isUserInteractionEnabled = false
        defer {
             tableView.isUserInteractionEnabled = true
        }
        SVProgressHUD.show()
        APIManager.getDetailDiary(id: idDiary, callbackSuccess: { (homeFeed) in
            let homeDetail = HomeDetailViewController.init(nib: R.nib.homeDetailViewController)
            homeDetail.hidesBottomBarWhenPushed = true
            homeDetail.homeFeed = homeFeed
            homeDetail.isHomeVC = false
            homeDetail.commentNotification = commentCommentNotification
            homeDetail.titleVC = R.string.localizable.diaryDiary()
            homeDetail.editFromView = editFromView
            homeDetail.fromToView = .Diary
            homeDetail.updateObject = { homefeedUpdate in
                if arrayTemp.count > 0{
                    arrayTemp[indexRow].commentNumber = homefeedUpdate.numberComment
                    arrayTemp[indexRow].likeNumber = homefeedUpdate.numberLike
                    arrayTemp[indexRow].isLiked = homefeedUpdate.checkLike
                    completion(arrayTemp)
                }
            }
            
            homeDetail.deleteObject = { id in
                arrayTemp.remove(at: indexRow)
                completion(arrayTemp)
            }
            
            Utils.getTopMostViewController()?.navigationController?.pushViewController(homeDetail, animated: true)
            SVProgressHUD.dismiss()
            tableView.isUserInteractionEnabled = true
        }) { (error) in
            SVProgressHUD.dismiss()
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            tableView.isUserInteractionEnabled = true
        }
    }
    
    // , completion: @escaping (_ homeFeed: HomeFeed)->()
    func getDetailFeed(array: [HomeFeed] ,idFeed: Int,_ indexRow: Int, editFromView: EditFromView,commentCommentNotification: Comment? = nil,tableView: UITableView = UITableView.init(), completion: @escaping (_ homeFeeds: [HomeFeed])->()){
        var arrayTemp = array
        tableView.isUserInteractionEnabled = true
        SVProgressHUD.show()
        APIManager.getDetailFeed(id: idFeed, callbackSuccess: { (homeFeed) in
            // if self.qc == 0 {
            let homeDetail = HomeDetailViewController.init(nib: R.nib.homeDetailViewController)
            homeDetail.hidesBottomBarWhenPushed = true
            homeDetail.idFeed = idFeed
            homeDetail.homeFeed = homeFeed
            homeDetail.isHomeVC = true
            homeDetail.editFromView = editFromView
            homeDetail.commentNotification = commentCommentNotification
            homeDetail.titleVC = R.string.localizable.homeTitleDetail()
            homeDetail.updateObject = { homefeedUpdate in
                guard arrayTemp.count > 0 else { return }
                let homeFeedTemp = arrayTemp[indexRow]

                arrayTemp[indexRow] = homefeedUpdate
                arrayTemp[indexRow].content = homeFeedTemp.content
                if homeFeedTemp.ads == 1{
                    arrayTemp[indexRow].media = homefeedUpdate.cover
                }
                completion(arrayTemp)

            }
            
            homeDetail.deleteObject = { id in
                arrayTemp.remove(at: indexRow)
                completion(arrayTemp)
            }
            tableView.isUserInteractionEnabled = true
            Utils.getTopMostViewController()?.navigationController?.pushViewController(homeDetail, animated: true)
            SVProgressHUD.dismiss()
            
        }) { (error) in
            Utils.showAlertView(controller: self, title: R.string.localizable.commonError(), message: error)
            SVProgressHUD.dismiss()
            tableView.isUserInteractionEnabled = true
        }
    }
    
    func pushPostDetail(array: [PostDetailModel] ,idPost: Int,_ indexRow: Int,commentCommentNotification: Comment? = nil, completion: @escaping (_ homeFeeds: [PostDetailModel])->()){
        var arrayTemp = array
        let postDetail = PostDetailViewController.init(nib: R.nib.postDetailViewController)
        postDetail.idPost = idPost
        postDetail.titleVC = R.string.localizable.infomationInfomation()
        postDetail.commentNotification = commentCommentNotification
        postDetail.hidesBottomBarWhenPushed = true
        postDetail.updateObject = { post in
            guard arrayTemp.count > 0 else { return }
            arrayTemp[indexRow] = post
            completion(arrayTemp)
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(postDetail, animated: true)
    }
    
    func pushPostDetailWithObjectDiary(idPost: Int, completion: @escaping (_ numberLike: Int, _ isLike: Int,_ numberComment: Int)->()){
        let postDetail = PostDetailViewController.init(nib: R.nib.postDetailViewController)
        postDetail.idPost = idPost
        postDetail.hidesBottomBarWhenPushed = true
        postDetail.titleVC = R.string.localizable.infomationTitleNewHot()
        postDetail.updateLikeNumber = { numberLike, isLike, numberComment in
            completion(numberLike,isLike,numberComment)
        }
        Utils.getTopMostViewController()?.navigationController?.pushViewController(postDetail, animated: true)
    }
    
    func showActionBottom(datasource:[String], isNoTextColorRed: Bool, completion: @escaping (_ index: Int)->()){
        let bottomVC = BaseBottomViewController.init(nib: R.nib.baseBottomViewController)
        //  print(bottomVC.w)
        bottomVC.selectedOption = { indexSelect in
            completion(indexSelect)
            Utils.getTopMostViewController()?.dismiss(animated: true, completion: {})
        }
        bottomVC.isNoTextColorRed = isNoTextColorRed
        bottomVC.datasource = datasource
        bottomVC.height = CGFloat((bottomVC.datasource.count + 1) * 44)
        bottomVC.topCornerRadius = 20
        bottomVC.presentDuration = 0.25
        bottomVC.dismissDuration = 0.25
        Utils.getTopMostViewController()?.navigationController?.present(bottomVC, animated: true)
    }
    
    func handelClickNotification(noti: NotificationList?, notification: Noti, viewVC : BaseViewController){
        switch noti {
        // done
        case .LikeFeed?:
            let id = notification.feed?.id ?? 0
            viewVC.getDetailFeed(array: [HomeFeed](), idFeed: id, 0, editFromView: .Notification) { (homefeed) in
            }
            
        case .LikeComment?, .ComFeed?, .LikeComtDiary?, .ComDiary?, .LikeComInfo?:
            // done
            //            let commentVC = CommentViewController.init(nib: R.nib.commentViewController)
            //            commentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            //            commentVC.isCheckFromNotification = true
            //            if noti == .LikeComment || noti == .ComFeed{
            //                commentVC.fromToView = .Home
            //            }else{
            //                commentVC.fromToView = .Diary
            //            }
            //
            //            commentVC.commentNotification = notification.comment
            //            commentVC.idPost = notification.feed?.id ?? 0
            //            viewVC.navigationController?.pushViewController(commentVC, animated: true)
            
            if noti == .LikeComment || noti == .ComFeed{
                viewVC.getDetailFeed(array: [HomeFeed](), idFeed: notification.feed?.id ?? 0, 0, editFromView: .Notification, commentCommentNotification: notification.comment) { (_) in
                    print("123")
                }
            }
            else if noti == .LikeComInfo || noti == .ComInfo{
                viewVC.pushPostDetail(array: [PostDetailModel](), idPost: notification.feed?.id ?? 0, 0, commentCommentNotification: notification.comment,completion: { (_) in
                    print("123")
                })
            }
            else{
                viewVC.getDetailDiary(array: [Diaries](), idDiary: notification.feed?.id ?? 0, 0, editFromView: .Notification, commentCommentNotification: notification.comment, tableView: UITableView.init()) { (_) in
                    print("123")
                }
            }
            
        case .LikeRepCom?, .LikeRepComDiary?, .LikeRepComInfo?:
            // done
            let notiCommentVC = NotificationCommentViewController.init(nib: R.nib.notificationCommentViewController)
            //     if (notification.comment?.reply ?? [Comment]()).count > 0{
            let comment = notification.replyComment
            notiCommentVC.commentNotification = comment
            //    }
            notiCommentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            notiCommentVC.fromToView = noti == .LikeRepCom ? .Home : .Diary
            notiCommentVC.userFrom = notification.userFrom ?? User.init(json: "")
            notiCommentVC.commentRoot = notification.comment
            notiCommentVC.typeNotification = noti!
            notiCommentVC.idCommentParent = notification.comment?.commentId ?? 0
            viewVC.navigationController?.pushViewController(notiCommentVC, animated: true)
            
        case .RepComt?, .RepComDiary?, .RepcomInfo?:
            
            let notiCommentVC = NotificationCommentViewController.init(nib: R.nib.notificationCommentViewController)
            //     if (notification.comment?.reply ?? [Comment]()).count > 0{
            let comment = notification.replyComment
            notiCommentVC.commentNotification = comment
            //     }
            
            notiCommentVC.feed = notification.feed ?? HomeFeed.init(json: "")
            notiCommentVC.fromToView = noti == .RepComt ? .Home : .Diary
            notiCommentVC.idNotification = notification.id ?? 0
            notiCommentVC.userFrom = notification.userFrom ?? User.init(json: "")
            notiCommentVC.commentRoot = notification.comment
            notiCommentVC.idCommentParent = notification.comment?.commentId ?? 0
            notiCommentVC.typeNotification = noti!
            viewVC.navigationController?.pushViewController(notiCommentVC, animated: true)
            
        case .LikeDiary?:
            let id = notification.feed?.id ?? 0
            viewVC.getDetailDiary(array: [Diaries](), idDiary: id, 0, editFromView: .Notification, tableView: UITableView.init()) { (_) in
                print("123")
            }
            
        case .RemindWrite:
            print("Noti")
            
            if let tabBarController = viewVC.tabBarController{
                tabBarController.selectedIndex = 1
                
                // print(Utils.getTopMostViewController())
                if let view = Utils.getTopMostViewController() as? DiaryViewController{
                    let writeHome = WriteHomeViewController.init(nib: R.nib.writeHomeViewController)
                    writeHome.hidesBottomBarWhenPushed = true
                    writeHome.fromView = .Diary
                    writeHome.checkComplete = { id in
                        view.getCheckCompleteDiary(id: id ,isEdit: false, completion: { diary in
//                            guard let diaryNews = view.getObjectDiaryNews() else { return }
//                            diaryNews.diaries?.insert(diary, at: 0)
//                            view.tbvDiary.reloadData()
                            
                            var listDiary = view.listDiaryNew.value
                            listDiary.insert(diary, at: 0)
                            view.listDiaryNew.accept(listDiary)
                            
                            NotificationCenter.default.post(name: NSNotification.Name.updateDiaryNew, object: ["DiaryNew": diary], userInfo: nil)
                            
                            view.dayWrite += 1
                            view.setupProgressBar(day: view.dayWrite)
                        })
                    }
                    view.navigationController?.pushViewController(writeHome, animated: true)
                }
            }
            
        case .LikeComSecret, .LikeSecret, .LikeRepComSecret, .ComSecret, .RepcomSecret:
            
            let setPassVC = SetPasswordSecretViewController.init(nib: R.nib.setPasswordSecretViewController)
            setPassVC.typeSetPass = .Permiss
            let naviSetPass = UINavigationController.init(rootViewController: setPassVC)
            naviSetPass.modalPresentationStyle = .overCurrentContext
            viewVC.present(naviSetPass, animated: true, completion: nil)
            
            setPassVC.actionConfirm =  { (isResult) in
                if isResult{
                    viewVC.tabBarController?.selectedIndex = 2
                }
            }
            
        case .SENDREQUESTFRIEND:
             let ProfileVC = R.storyboard.main.profileViewController()
             ProfileVC?.user = notification.userFrom ?? User.init(json: "")
             let naviProfile = UINavigationController.init(rootViewController: ProfileVC!)
             naviProfile.modalPresentationStyle = .fullScreen
            viewVC.present(naviProfile, animated: true, completion: nil)
            
        default:
            print("Open Notification Default")
        }
        
        if noti != .NotificationNomar && noti != .NOTIFICATIONGAME{
            APIManager.readNotification(id: notification.id ?? 0, callbackSuccess: { [weak self] (isSuccess) in
                guard let _ = self else { return }
                if isSuccess{
                    print("Read Notication Success")
                }else{
                    print("Read Notication Fail")
                }
            }) { (error) in
                print(error)
            }
        }
    }
    
}

extension BaseViewController{
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {

        if tabBarController.viewControllers?.firstIndex(of: viewController) == 0 || tabBarController.viewControllers?.firstIndex(of: viewController) == 3 || tabBarController.viewControllers?.firstIndex(of: viewController) == 4
        {
            if UserInfo.shareUserInfo.token == ""{
                Utils.presentLoginVC()
                return false
            }
        }
        
        // double tap icon home scrollView to top
        

        if tabBarController.viewControllers?.firstIndex(of: viewController) == 1{
            if tabBarController.tabBar.items?[1].badgeValue == "" {
//                FirebaseDataBaseRealtime.share.setValue(userId: UserInfo.shareUserInfo.id ?? 0, value: 0)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
                    if let view = Utils.getTopMostViewController() as? HomeViewController{
                        view.scrollView.setContentOffset(CGPoint.zero, animated: true)
                        view.getHomeFeed(tokenPage: "", about: 0, category: 0, ageRange: 7, locationID: 1, isLoadFeedNews: true)
                        view.setUpButtonLocation()
                    }
                }
            }else{
                if Utils.getTopMostViewController()!.isKind(of: HomeViewController.self){
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                        if let view = Utils.getTopMostViewController() as? HomeViewController{
                            view.scrollView.setContentOffset(CGPoint.zero, animated: true)
                        }
                    }
                }
                
            }
        }
        
        return true
    }
    
    
    
}
