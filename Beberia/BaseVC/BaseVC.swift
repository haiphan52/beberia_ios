//
//  BaseVC.swift
//  Beberia
//
//  Created by haiphan on 21/05/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class BaseVC: UIViewController {
    
    let buttonRight: UIButton = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 44, height: 44)))
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            if let navBar = self.navigationController {
                let bar = navBar.navigationBar
                bar.standardAppearance = appearance
                bar.scrollEdgeAppearance = appearance
            }

        }
    }
    
    func removeBorder(font: UIFont, bgColor: UIColor, textColor: UIColor) {
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            let atts = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]
            appearance.configureWithOpaqueBackground()
            appearance.configureWithTransparentBackground()
            appearance.titleTextAttributes = atts
            appearance.backgroundColor = bgColor
            if let navBar = self.navigationController {
                let bar = navBar.navigationBar
                bar.standardAppearance = appearance
                bar.scrollEdgeAppearance = appearance
            }
        } else {
            self.navigationController?.hideHairline()
        }
    }
    
    func setupNavigationVer2() {
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                                                                        NSAttributedString.Key.font: UIFont.notoSansFont(weight: .bold, size: 18)]
    }
    
    func navigationBarCustom(font: UIFont,
                             bgColor: UIColor,
                             textColor: UIColor,
                             isTranslucent: Bool = true) {
        self.navigationController?.navigationBar.isTranslucent = isTranslucent
        self.navigationController?.navigationBar.barTintColor = bgColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: textColor,
                                                                        NSAttributedString.Key.font: font]
        self.setupNavigationBariOS15(font: font,
                                     bgColor: bgColor,
                                     textColor: textColor,
                                     isTranslucent: isTranslucent)
    }
    
    func setupNavigationBariOS15(font: UIFont, bgColor: UIColor, textColor: UIColor, isTranslucent: Bool = true) {
        if #available(iOS 15.0, *) {
            let atts = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.titleTextAttributes = atts
            appearance.backgroundColor = bgColor
            
            if let navBar = self.navigationController {
                let bar = navBar.navigationBar
                bar.standardAppearance = appearance
                bar.scrollEdgeAppearance = appearance
//                bar.compactScrollEdgeAppearance = appearance
            }
            
        }
    }
    
    func customRightBarButtonTitle(title: String) {
        self.buttonRight.setTitle(title, for: .normal)
        self.buttonRight.contentEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        let leftBarButton = UIBarButtonItem(customView: self.buttonRight)
        navigationItem.rightBarButtonItem = leftBarButton
    }
    
    func customRightBarButtonVer2(imgArrow: UIImage){
        self.buttonRight.setImage(imgArrow, for: .normal)
        self.buttonRight.contentEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        let leftBarButton = UIBarButtonItem(customView: self.buttonRight)
        navigationItem.rightBarButtonItem = leftBarButton
    }
    
    func customLeftBarButtonVer2(imgArrow: UIImage){
        let buttonLeft = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 44, height: 44)))
        buttonLeft.setImage(imgArrow, for: .normal)
        buttonLeft.contentEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        let leftBarButton = UIBarButtonItem(customView: buttonLeft)
        navigationItem.leftBarButtonItem = leftBarButton
        buttonLeft.rx.tap.bind { [weak self] _ in
            guard let wSelf = self else { return }
            wSelf.navigationController?.popViewController(animated: true, {
            })
        }.disposed(by: disposeBag)
    }
}
extension UINavigationController {
    func hideHairline() {
        if let hairline = findHairlineImageViewUnder(navigationBar) {
            hairline.isHidden = true
        }
    }
    func restoreHairline() {
        if let hairline = findHairlineImageViewUnder(navigationBar) {
            hairline.isHidden = false
        }
    }
    func findHairlineImageViewUnder(_ view: UIView) -> UIImageView? {
        if view is UIImageView && view.bounds.size.height <= 1.0 {
            return view as? UIImageView
        }
        for subview in view.subviews {
            if let imageView = self.findHairlineImageViewUnder(subview) {
                return imageView
            }
        }
        return nil
    }
}
