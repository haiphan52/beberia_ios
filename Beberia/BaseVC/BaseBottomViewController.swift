//
//  BaseBottomViewController.swift
//  Canvasee
//
//  Created by VNPM1 on 6/26/19.
//  Copyright © 2019 VNPM1. All rights reserved.
//

import UIKit

class BaseBottomViewController: BottomPopupViewController {
    @IBOutlet var tbvBottom: UITableView!
    var datasource = [String]()
    var dataImg = [String]()
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    var selectedOption: (_ index: Int)->() = {_ in}
    var isNoTextColorRed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbvBottom.tableFooterView = UIView()
        tbvBottom.register(BottomTableViewCell.self, forCellReuseIdentifier: "BottomTableViewCell")
        self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.view.frame.height)
    }
    
    override func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(300)
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 1.0
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 1.0
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }

}

extension BaseBottomViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BottomTableViewCell", for: indexPath) as! BottomTableViewCell
        cell.lblTitle.text = datasource[indexPath.row]
        
        if !isNoTextColorRed{
//            if indexPath.row == 2{
//                cell.lblTitle.textColor = .red
//            }
            if cell.lblTitle.text == R.string.localizable.homeDelete(){
                cell.lblTitle.textColor = .red
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedOption(indexPath.row)
    }
}

class BottomTableViewCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "BottomTableViewCell")
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let lblTitle: UILabel = {
        let label = UILabel()
      //  label.textAlignment = NSTextAlignment.center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setup() {
        contentView.addSubview(lblTitle)
        lblTitle.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        lblTitle.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        lblTitle.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
}

