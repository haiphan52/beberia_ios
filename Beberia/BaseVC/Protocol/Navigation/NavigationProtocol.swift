//
//  NavigationProtocol.swift
//  Beberia
//
//  Created by haiphan on 23/08/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation

protocol NavigationProtocol {}
extension NavigationProtocol {
    
    func moveToInvited() {
        guard let topVC = ManageApp.shared.topViewController() else {
            return
        }
        let vc = InvitedSuccessVC.createVC()
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        topVC.present(vc, animated: true, completion: nil)
    }
    
}
