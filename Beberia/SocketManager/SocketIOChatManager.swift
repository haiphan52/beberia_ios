//
//  SocketIOChatManager.swift
//  Beberia
//
//  Created by haiphan on 22/02/2022.
//  Copyright © 2022 IMAC. All rights reserved.
//

import Foundation
import UIKit
import SocketIO
import RxSwift
import RxRelay
import SwiftyJSON

class SocketIOChatManager {
    
    enum ChatSocketEventType {
        case onGroupCreate(NewGroupModel), onMessageReceived(NewMessageModel), onMessageUpdate, onChannelChanged, leaveGroup(Int), onMessageDelete(DeleteMessageModel)
    }
    static var shared = SocketIOChatManager()
    
    var manager: SocketManager!
    var socket: SocketIOClient!
    let chatSocketEvent: PublishSubject<ChatSocketEventType> = PublishSubject.init()
    private init() {
    }
    
    func configureSocketClient(userID: Int?) {
        guard let url = URL(string: ConstantApp.shared.linkSocketChat()), let userID = userID else {
            return
        }
        self.manager = SocketManager(socketURL: url, config: [.log(true), .compress, .connectParams(["id": userID])])
        self.manager.reconnects = true
        self.manager.reconnectWait = 3
        self.manager.reconnectWaitMax = 30
        socket = self.manager.defaultSocket
        self.connectSocket()
    }
    
    private func connectSocket(){
        guard let socket = self.socket else {
            return
        }
        
        socket.on(clientEvent: .connect) { data, _ in
            print("==============data connect==============", data)
        }
        
        socket.on(clientEvent: .error) { data, _ in
            print("==============data error==============", data)
        }
        
        socket.connect()
        self.handlersEvent()
    }
    
    func disconnectSocket(){
        guard let socket = self.socket else{
            return
        }
        
        socket.disconnect()
    }
    
    func handlersEvent() {
        guard let socket = self.socket else{
            return
        }
        
        socket.on(ConstantApp.shared.onGroupCreateEvent) { [weak self] ( newgroup, ack) -> Void in
            guard let wSelf = self else { return }
            do {
                let json = JSON(newgroup)
                let decoder = JSONDecoder()
                let model = try decoder.decode([NewGroupModel].self , from:json.rawData())
                if let f = model.first {
                    wSelf.chatSocketEvent.onNext(.onGroupCreate(f))
                }
            } catch {
                print(error.localizedDescription)
            }
            
        }
        
        socket.on(ConstantApp.shared.onMessageReceivedEvent) { [weak self] ( newMessage, ack) -> Void in
            guard let wSelf = self else { return }
            do {
                let json = JSON(newMessage)
                let decoder = JSONDecoder()
                let model = try decoder.decode([NewMessageModel].self , from:json.rawData())
                if let f = model.first {
                    wSelf.chatSocketEvent.onNext(.onMessageReceived(f))
                }
            } catch {
                print(error.localizedDescription)
            }
            
        }
        
        socket.on(ConstantApp.shared.onMessageUpdateEvent) { [weak self] ( dataArray, ack) -> Void in
            guard let wSelf = self else { return }
            wSelf.chatSocketEvent.onNext(.onMessageUpdate)
        }
        
        socket.on(ConstantApp.shared.onChannelChangedEvent) { [weak self] ( dataArray, ack) -> Void in
            guard let wSelf = self else { return }
            wSelf.chatSocketEvent.onNext(.onChannelChanged)
        }
        
        socket.on(ConstantApp.shared.onMessageDeleteEvent) { [weak self] ( deleteMessage, ack) -> Void in
            guard let wSelf = self else { return }
            do {
                let json = JSON(deleteMessage)
                let decoder = JSONDecoder()
                let model = try decoder.decode([DeleteMessageModel].self , from:json.rawData())
                if let f = model.first {
                    wSelf.chatSocketEvent.onNext(.onMessageDelete(f))
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        
        socket.on(ConstantApp.shared.onMessageLikeEvent) { [weak self] ( dataArray, ack) -> Void in
            guard let wSelf = self else { return }
            wSelf.chatSocketEvent.onNext(.onChannelChanged)
        }
        
    }
    
}
