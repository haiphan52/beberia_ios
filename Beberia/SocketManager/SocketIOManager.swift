//
//  SocketIOManager.swift
//  Beberia
//
//  Created by IMAC on 7/13/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit
import SocketIO

class SocketIOManager: NSObject {
    
    static let manager = SocketManager(socketURL: URL(string: "http://112.213.86.102:8004")!, config: [.log(true), .compress])
    static let socket = manager.defaultSocket
    
    
    class func connectSocket(userID: Int){
        self.manager.config = SocketIOClientConfiguration.init(arrayLiteral: .connectParams(["id": userID]))
        socket.connect()
    }
    
    class func disconnectSocket(){
        socket.disconnect()
    }
    
}
