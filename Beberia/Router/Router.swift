//
//  Router.swift
//  Beberia
//
//  Created by IMAC on 11/10/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import Foundation
import UIKit

class Router {
    static let share = Router.init()
    
    func pushToViewHomeDetail(tableView: UITableView, array: [HomeFeed], indexPath: IndexPath, viewSelf: UIViewController, idFeed: Int, editFromView: EditFromView, commentCommentNotification: Comment? = nil, completion: @escaping (_ homeFeeds: [HomeFeed])->()){
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.isUserInteractionEnabled = false
        defer {
             tableView.isUserInteractionEnabled = true
        }
        var arrayTemp = array
        let detail = HomeDetailViewController.init(nib: R.nib.homeDetailViewController)
        detail.idFeed = idFeed
      //  detail.homeFeed = arrayTemp[indexPath.row]
        detail.editFromView = editFromView
        detail.hidesBottomBarWhenPushed = true
        detail.isHomeVC = true
        detail.commentNotification = commentCommentNotification
        detail.titleVC = R.string.localizable.homeTitleDetail()
        detail.updateObject = { homefeedUpdate in
            guard arrayTemp.count > 0 else { return }
            let homeFeedTemp = arrayTemp[indexPath.row]
            if homeFeedTemp != homefeedUpdate {
                arrayTemp[indexPath.row] = homefeedUpdate
                arrayTemp[indexPath.row].content = homeFeedTemp.content
                completion(arrayTemp)
            }
        }
        
        detail.deleteObject = { id in
            if indexPath.row <= arrayTemp.count {
                arrayTemp.remove(at: indexPath.row)
                completion(arrayTemp)
            }
        }
        viewSelf.navigationController?.pushViewController(detail, animated: true)
    }
    
    func pushToViewDiaryDetail(tableView: UITableView, array: [Diaries], indexPath: IndexPath,row:Int, viewSelf: UIViewController, idFeed: Int, editFromView: EditFromView, commentCommentNotification: Comment? = nil,completion: @escaping (_ homeFeeds: [Diaries])->()){
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.isUserInteractionEnabled = false
        defer {
             tableView.isUserInteractionEnabled = true
        }
        var arrayTemp = array
        let detail = HomeDetailViewController.init(nib: R.nib.homeDetailViewController)
        detail.idFeed = idFeed
        detail.editFromView = editFromView
        detail.hidesBottomBarWhenPushed = true
        detail.isHomeVC = false
        detail.fromToView = .Diary
        detail.commentNotification = commentCommentNotification
        detail.titleVC = R.string.localizable.diaryDiary()
        detail.updateObject = { homefeedUpdate in
            if arrayTemp.count > 0 && row <= arrayTemp.count - 1 {
                arrayTemp[row].commentNumber = homefeedUpdate.numberComment
                arrayTemp[row].likeNumber = homefeedUpdate.numberLike
                arrayTemp[row].isLiked = homefeedUpdate.checkLike
                completion(arrayTemp)
            }
        }
        
        detail.deleteObject = { id in
            arrayTemp.remove(at: row)
            completion(arrayTemp)
        }
        viewSelf.navigationController?.pushViewController(detail, animated: true)
    }
}
