//
//  KeyChainManager.swift
//  Beberia
//
//  Created by IMAC on 2/28/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import Foundation
import SwiftKeychainWrapper

class KeyChainManager {
    static let shareIn = KeyChainManager()
    
    init() {}
    
    func saveIDGoogle(idGoogle: String){
        KeychainWrapper.standard.set(idGoogle, forKey: KeychainKeys.idGoogle.rawValue)
    }
    
    func saveIDFB(idFB: String){
        KeychainWrapper.standard.set(idFB, forKey: KeychainKeys.idFacebook.rawValue)
    }
    
    func saveIdApple(idApple: String){
        KeychainWrapper.standard.set(idApple, forKey: KeychainKeys.idApple.rawValue)
    }
    
    func removeIDGoogle(idGoogle: String){
        KeychainWrapper.standard.removeObject(forKey: KeychainKeys.idGoogle.rawValue)
    }
    
    func removeIDFB(idFB: String){
        KeychainWrapper.standard.removeObject(forKey: KeychainKeys.idFacebook.rawValue)
    }
    
    func removeIdApple(idApple: String){
        KeychainWrapper.standard.removeObject(forKey: KeychainKeys.idApple.rawValue)
    }
}
