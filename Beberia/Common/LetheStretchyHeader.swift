//
//  LetheStretchyHeader.swift
//  LetheStretchyHeader
//
//  Created by Osman YILDIRIM on 3.06.2019.
//  Copyright © 2019 Osman YILDIRIM. All rights reserved.
//

import UIKit

public class LetheStretchyHeader: UIView {
    fileprivate var customHeader: UIView?
    fileprivate var height: CGFloat = 0
    public var imageView: UIImageView!
    public var btnLeft: UIButton!
    public var btnRight : UIButton!
    fileprivate var inset: CGFloat = 0
    fileprivate var type: HeaderType = .alwaysHideNavigationBar
    fileprivate var viewController: UIViewController!
    fileprivate var viewSize = CGSize.zero
    public var tapBack: (_ index :Int) -> () = {_ in}
    public var tapOption: (_ index :Int) -> () = {_ in}
    private func initialImageView(_ width: CGFloat, _ height: CGFloat) {
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        self.addSubview(imageView)
    }
    
    private func initialButtonLeft(){

        btnLeft = UIButton(frame: CGRect(x: 18, y: UIApplication.shared.statusBarFrame.size.height > 20 ? 50 : 27, width: 40, height: 30))
        btnLeft.setImage(UIImage(named: "ic_BackBG"), for: .normal)
        btnLeft.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        btnLeft.tag = 1
        self.addSubview(btnLeft)
    }

    private func initialButtonRight(){

        btnRight = UIButton(frame: CGRect(x:(viewController.navigationController?.navigationBar.frame.size.width ?? 0) - 55, y: UIApplication.shared.statusBarFrame.size.height > 20 ? 50 : 27, width: 40, height: 30))
        btnRight.setImage(UIImage(named: "ic_MoreBG"), for: .normal)
        btnRight.addTarget(self, action: #selector(tapMore), for: .touchUpInside)
        btnRight.tag = 2
        self.addSubview(btnRight)
    }

    @objc func buttonAction(sender: UIButton!) {
       tapBack(btnLeft.tag)
    }
    
    @objc func tapMore(sender: UIButton!) {
        tapOption(btnRight.tag)
    }
    

    
    public func initial(viewController: UIViewController, parentView: UIScrollView, customHeader: UIView?, image: UIImage?, height: CGFloat, type: HeaderType,completionMore:@escaping (_ index :Int) -> () = {_ in}, completion:@escaping (_ index :Int) -> () = {_ in})  {
        self.customHeader = customHeader
        self.height = height
        self.type = type
        self.viewController = viewController
        self.tapBack = completion
        self.tapOption = completionMore
        switch type {
        case .alwaysHideNavigationBar, .afterShowNavigationBar:
            if let customHeader = self.customHeader {
                customHeader.alpha = 0
            } else {
                viewController.navigationController!.navigationBar.alpha = 0
            }
            
        case .alwaysShowNavigationBar:
            if let customHeader = self.customHeader {
                inset = customHeader.frame.height
            } else {
                if let translucent = viewController.navigationController?.navigationBar.isTranslucent {
                    
                    if translucent {
                        inset = UIApplication.shared.statusBarFrame.height + (viewController.navigationController?.navigationBar.frame.size.height ?? 44)
                    }
                }
            }
        }
        
        initialImageView(viewController.view.frame.width, height)
        initialButtonLeft()
        initialButtonRight()
        self.isUserInteractionEnabled = true
        
        
        self.frame = CGRect(x: 0, y: inset, width: viewController.view.frame.width, height: height)
        viewSize = self.frame.size
        
        if let customHeader = self.customHeader {
            viewController.view.insertSubview(self, belowSubview: customHeader)
        } else if let navigationBar = viewController.navigationController?.navigationBar {
            viewController.view.insertSubview(self, belowSubview: navigationBar)
        } else {
            viewController.view.addSubview(self)
        }
        parentView.contentInset = UIEdgeInsets(top: height, left: 0, bottom: 0, right: 0)
        (parentView as UIScrollView).delegate = self
        
        guard let image = image else { return }
        imageView.image = image
      //  self.isUserInteractionEnabled = false
        parentView._letheStretchyHeader = self
    }

}

extension LetheStretchyHeader: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if imageView == nil { return }
        if scrollView.contentOffset.y != 0 {
            let scrollOffset = scrollView.contentOffset.y + height
            if scrollOffset < 0 {
                imageView.frame = CGRect(x: scrollOffset, y: scrollOffset, width: scrollView.bounds.size.width - (scrollOffset * 2), height: viewSize.height - scrollOffset * 2)
            } else {
                imageView.frame = CGRect(x: 0, y: -scrollOffset, width: viewSize.width, height: viewSize.height)
            }
            navigationBarAlpha(scrollOffset)
        }
    }
    
    private func navigationBarAlpha(_ scrollOffset: CGFloat) {
        if type == .afterShowNavigationBar {
            let navBarHeight = (customHeader?.frame.height ?? viewController.navigationController?.navigationBar.frame.maxY) ?? 0
            if (viewSize.height - scrollOffset) > navBarHeight {
                if let customHeader = self.customHeader {
                    customHeader.alpha = scrollOffset / (viewSize.height - navBarHeight)
                } else {
                    Utils.getTopMostViewController()?.navigationController?.navigationBar.alpha = scrollOffset / (viewSize.height - navBarHeight)
                    btnLeft.alpha = 1 - (scrollOffset / (viewSize.height - navBarHeight))
                    btnRight.alpha = 1 - (scrollOffset / (viewSize.height - navBarHeight))
                }
            } else {
                if let customHeader = self.customHeader {
                    customHeader.alpha = 1
                } else {
                    Utils.getTopMostViewController()?.navigationController?.navigationBar.alpha = 1
                }
            }
        }
//        else {
//             let navBarHeight = (customHeader?.frame.height ?? viewController.navigationController?.navigationBar.frame.maxY) ?? 0
//            if (viewSize.height - scrollOffset) > navBarHeight {
//                if let customHeader = self.customHeader {
//                    customHeader.alpha = scrollOffset / (viewSize.height - navBarHeight)
//                } else {
//                    viewController.navigationController?.navigationBar.isTranslucent = true
//                    viewController.navigationController?.view.backgroundColor = .clear
//                    //                    btnLeft.alpha = 1 - (scrollOffset / (viewSize.height - navBarHeight))
//                }
//            } else {
//                if let customHeader = self.customHeader {
//                    customHeader.alpha = 1
//                } else {
//                    print(scrollOffset)
//
//                    viewController.navigationController?.navigationBar.isTranslucent = false
////                    viewController.navigationController?.view.backgroundColor = .white
//                    Utils.getTopMostViewController()?.navigationController?.navigationBar.alpha = scrollOffset / (viewSize.height - navBarHeight)
//                }
//            }
//        }
    }
}

var prop_1: String? = nil
extension UIScrollView {
    public var _letheStretchyHeader: LetheStretchyHeader! {
        get { return objc_getAssociatedObject(self, &prop_1) as? LetheStretchyHeader }
        set { objc_setAssociatedObject(self, &prop_1, newValue as LetheStretchyHeader?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

public enum HeaderType {
    case afterShowNavigationBar
    case alwaysHideNavigationBar
    case alwaysShowNavigationBar
}
