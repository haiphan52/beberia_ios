//
//  CityName.swift
//
//  Created by iMAC on 9/6/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class GameModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {

    static let id = "id"
    static let name = "name"
    static let image = "image"
    static let description = "description"
  }

  // MARK: Properties

  public var id: Int?
  public var name: String?
  public var image: String?
    public var description: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    name = json[SerializationKeys.name].string
    image = json[SerializationKeys.image].string
    description = json[SerializationKeys.description].string
    id = json[SerializationKeys.id].int

  }
    
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
  //    if let value = locationNameVi { dictionary[SerializationKeys.locationNameVi] = value }
  //    if let value = userActive { dictionary[SerializationKeys.userActive] = value }
  //    if let value = id { dictionary[SerializationKeys.id] = value }
  //    if let value = locationNameEn { dictionary[SerializationKeys.locationNameEn] = value }
  //    if let value = locationCode { dictionary[SerializationKeys.locationCode] = value }
      return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
  //    self.locationNameVi = aDecoder.decodeObject(forKey: SerializationKeys.locationNameVi) as? String
  //    self.userActive = aDecoder.decodeObject(forKey: SerializationKeys.userActive) as? Int
  //    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
  //    self.locationNameEn = aDecoder.decodeObject(forKey: SerializationKeys.locationNameEn) as? String
  //    self.locationCode = aDecoder.decodeObject(forKey: SerializationKeys.locationCode) as? Int
    }

    public func encode(with aCoder: NSCoder) {
  //    aCoder.encode(locationNameVi, forKey: SerializationKeys.locationNameVi)
  //    aCoder.encode(userActive, forKey: SerializationKeys.userActive)
  //    aCoder.encode(id, forKey: SerializationKeys.id)
  //    aCoder.encode(locationNameEn, forKey: SerializationKeys.locationNameEn)
  //    aCoder.encode(locationCode, forKey: SerializationKeys.locationCode)
    }
}

public final class DistrictModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {

    static let id = "id"
    static let districtName = "district_name"
    static let districtCode = "district_code"
  }

  // MARK: Properties

  public var id: Int?
  public var districtName: String?
  public var districtCode: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    districtName = json[SerializationKeys.districtName].string
    districtCode = json[SerializationKeys.districtCode].string
    id = json[SerializationKeys.id].int

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
//    if let value = locationNameVi { dictionary[SerializationKeys.locationNameVi] = value }
//    if let value = userActive { dictionary[SerializationKeys.userActive] = value }
//    if let value = id { dictionary[SerializationKeys.id] = value }
//    if let value = locationNameEn { dictionary[SerializationKeys.locationNameEn] = value }
//    if let value = locationCode { dictionary[SerializationKeys.locationCode] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
//    self.locationNameVi = aDecoder.decodeObject(forKey: SerializationKeys.locationNameVi) as? String
//    self.userActive = aDecoder.decodeObject(forKey: SerializationKeys.userActive) as? Int
//    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
//    self.locationNameEn = aDecoder.decodeObject(forKey: SerializationKeys.locationNameEn) as? String
//    self.locationCode = aDecoder.decodeObject(forKey: SerializationKeys.locationCode) as? Int
  }

  public func encode(with aCoder: NSCoder) {
//    aCoder.encode(locationNameVi, forKey: SerializationKeys.locationNameVi)
//    aCoder.encode(userActive, forKey: SerializationKeys.userActive)
//    aCoder.encode(id, forKey: SerializationKeys.id)
//    aCoder.encode(locationNameEn, forKey: SerializationKeys.locationNameEn)
//    aCoder.encode(locationCode, forKey: SerializationKeys.locationCode)
  }

}
