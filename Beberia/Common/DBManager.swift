//
//  DBManager.swift
//  Canvasee
//
//  Created by VNPM1 on 6/19/19.
//  Copyright © 2019 VNPM1. All rights reserved.
//

import UIKit
import RealmSwift

class DBManager {
    private var   database:Realm
    static let   sharedInstance = DBManager()
    
    private init() {
        database = try! Realm()
    }
    
    func getDataFromDB() -> Results<ItemSearch> {
        let results: Results<ItemSearch> = database.objects(ItemSearch.self).sorted(byKeyPath: "timeStamp", ascending: false)
        return results
    }
    
    func addData(object: ItemSearch){
        try! database.write {
            database.add(object, update: true)
            print("Added new object")
        }
    }
    
//    func insertObject(object: ItemSearch){
//        try! database.write {
//            print(database.objects(ItemSearch.self).first)
//
//            database.objects(ItemSearch.self).
//        }
//    }
    
    func deleteAllFromDatabase(){
        try! database.write {
            database.deleteAll()
        }
    }
    
    func deleteFromDb(object: ItemSearch){
        try! database.write {
            database.delete(object)
        }
    }
    
    func objectExist (id: Int) -> Bool {
        return database.object(ofType: ItemSearch.self, forPrimaryKey: id) != nil
    }

}

class ItemSearch: Object {
    let itemSearch = List<ItemSearch>()
  //  @objc dynamic var id = 0
    @objc dynamic var textSearch = ""
    @objc dynamic var timeStamp: Double = 0
    override static func primaryKey() -> String? {
        return "textSearch"
    }
}
