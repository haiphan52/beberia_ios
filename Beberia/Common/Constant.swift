//
//  Constant.swift
//  
//
//  Created by Mac on 4/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit

enum UserDefaultKeys {
    case warehouseBookMark
}
enum NotificationList: Int {
    case LikeFeed = 1
    case LikeComment = 2
    case LikeRepCom  = 3
    case ComFeed = 4
    case RepComt = 5
    case LikeDiary = 6
    case LikeComtDiary = 7
    case LikeRepComDiary = 8
    case ComDiary = 9
    case RepComDiary = 10
    case LikeInfo = 11
    case LikeComInfo = 12
    case LikeRepComInfo = 13
    case ComInfo = 14
    case RepcomInfo = 15
    
    case RemindWrite = 16
    
    case LikeSecret = 17
    case LikeComSecret = 18
    case LikeRepComSecret = 19
    case ComSecret = 20
    case RepcomSecret = 21
    
    case SENDREQUESTFRIEND = 22
    
    case NewFeed = 23
    
    case NotificationNomar = 24
    
    case NOTIFICATIONGAME = 25
    case NewMessage = 27
    
    var title: String{
        switch self {
            
        case .LikeFeed,.LikeSecret:
            return "đã thích bài viết của bạn"
        case .LikeComment,.LikeComSecret:
            return "đã thích bình luận của bạn trong bài viết"
        case .LikeComInfo:
            return "đã thích bình luận của bạn trong thông tin"
        case .LikeComtDiary:
            return "đã thích bình luận của bạn trong nhật ký"
//        case .LikeComInfo:
//             return "đã thích bình luận của bạn trong thông tin"
        case .LikeRepCom, .LikeRepComSecret :
            return "đã thích phản hồi bình luận của bạn trong một bài viết"
        case .LikeRepComInfo:
            return "đã thích phản hồi bình luận của bạn trong một thông tin"
        case .LikeRepComDiary:
            return "đã thích phản hồi bình luận của bạn trong một nhật ký"
        case .ComFeed, .ComSecret:
            return  "đã bình luận trong bài viết của"
        case .RepComt,.RepcomSecret:
             return "đã phản hồi bình luận của"
        case .RepComDiary:
            return "đã phản hồi bình luận của"
        case .RepcomInfo:
            return "đã phản hồi bình luận của"
        case .LikeDiary:
            return "đã thích nhật ký của bạn"
        case .LikeInfo:
            return "đã thích bài thông tin"
        case .ComDiary:
            return "đã bình luận trong nhật ký của "
        case .ComInfo:
            return "đã bình luận trong thông tin "
            
        case .SENDREQUESTFRIEND:
            return " đã gửi lời mời kết bạn"
        default:
            return ""
        }
    }
    
    var image: UIImage{
        switch self {
        case .LikeFeed , .LikeComment, .LikeRepCom, .ComFeed, .RepComt:
            return R.image.ic_NotiHome()!
        case .LikeDiary, .LikeComtDiary ,.LikeRepComDiary,.ComDiary, .RepComDiary:
            return R.image.ic_NotiDiary()!
        case .LikeInfo, .LikeComInfo ,.LikeRepComInfo,.ComInfo, .RepcomInfo:
            return R.image.ic_NotiInfo()!
        default:
            return UIImage.init()
        }
    }
    
}

enum CodeEASY: Int {
    case e = 1
    case a = 2
    case s = 3
    case y = 4
    case o = 5
    case code12 = 12
    case code34 = 34
    
    var title: String{
        switch self {
        case .e:
            return "E"
        case .a:
            return "A"
        case .s:
            return "S"
        case .y:
            return "Y"
        case .o:
            return ""
        case .code12:
            return "E.A"
        case .code34:
            return "S.Y"
        }
    }
        
    var colorEASY: String {
        switch self {
        case .e:
            return "ff99bb"
        case .a:
            return "48c7ff"
        case .s:
            return "ffc440"
        case .y:
            return "f4703c"
        case .o:
            return "000000"
        case .code12:
            return "ff99bb"
        case .code34:
            return "48c7ff"
        }
    }
}

enum KeychainKeys : String {
    case isLoggedIn
    case userName
    case passWord
    case passWordSecret
    case idApple
    case idGoogle
    case idFacebook
}

enum SUBTITLE_SECRET: Int,CaseIterable {
    case All = 0
    case GiaDinh = 1
    case CuocSong  = 2
    case ConCai = 3
    case BocPhot = 4
    case Khac = 5
    
    var title: String{
         switch self {
         case .All:
             return "Tất cả"
         case .GiaDinh:
             return "Gia đình"
         case .CuocSong:
             return "Cuộc Sống"
         case .ConCai:
            return "Con cái"
         case .BocPhot:
            return "Bóc Phốt"
         case .Khac:
            return "Khác"
        }
     }
    
    static var arrayList: [String] {
          return SUBTITLE_SECRET.allCases.map { $0.title }
      }
    
}

enum AboutFeed: Int, CaseIterable {
    case be = 1
    case me = 2
    case others = 3
    
    var title: String{
        switch self {
        case .be:
            return "Bé"
        case .me:
            return "Mẹ"
        case .others:
            return "Khác"
        }
    }
    
    static var arrayList: [String] {
        return AboutFeed.allCases.map { $0.title }
    }
}

enum AgeRange:Int,CaseIterable {
    case mt = 0
    case tuoi1 = 1
    case tuoi2 = 2
    case tuoi3 = 3
    case tuoi4 = 4
    case tuoi5 = 5
    case con = 6
    
    var title: String {
        switch self {
        case .mt:
        return "Đang mang thai"
        case .tuoi1:
        return "~1 tuổi"
        case .tuoi2:
        return "~1-2 tuổi"
        case .tuoi3:
        return "~2-3 tuổi"
        case .tuoi4:
        return "~3-5 tuổi"
        case .tuoi5:
        return "trên 5 tuổi"
        case .con:
            return "Đã có con"

        }
    }
    static var arrayList: [String] {
        return AgeRange.allCases.map { $0.title }
    }
}


enum CategoryFeed: Int, CaseIterable {
  
    case QnA = 1
    case Info = 2
    case Share = 3
    
    var title: String{
        switch self {
        case .QnA:
            return "Hỏi đáp"
        case .Info:
            return "Chia sẻ"
        case .Share:
            return "Đổi đồ"
        }
    }
    
    var colorLine: String{
        switch self {
        case .QnA:
            return AppColor.ColorLineQnA
        case .Info:
            return AppColor.ColorLineInfo
        case .Share:
            return AppColor.ColorLineShare
        }
    }
    
    var color: String{
        switch self {
        case .QnA:
            return AppColor.colorQnA
        case .Info:
            return AppColor.Info
        case .Share:
            return AppColor.Share
        }
    }
    
    static var arrayList: [String] {
        return CategoryFeed.allCases.map { $0.title }
    }
}

// MARK: - App constant config

enum UIUserInterfaceIdiom : Int {
    case Unspecified
    case phone
    case pad
}

// MARK: - Enum - Class

enum EditFromView {
    case Secret
    case Home
    case Sreach
    case Profile
    case Save
    case Notification
}

enum FromView {
    case Home
    case Diary
    case EditHome
    case EditDiary
    case Secret
}

class ItemPost {
    var id: Int?
    var image: UIImage? = nil
    var urlImage: String? = nil
    var content : NSMutableAttributedString? = nil
    var contentHtml : String?
    var isImageGIF: Bool? = nil
    var dataImageGIF: Data? = nil
}

struct ScreenSize {
    static let screenWidth = UIScreen.main.bounds.size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    static let screenMaxLength = max(ScreenSize.screenWidth, ScreenSize.screenHeight)
    static let screenMinLength = min(ScreenSize.screenWidth, ScreenSize.screenHeight)
}

struct DeviceType {
    static let iPhone4OrLess  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength < 568.0
    static let iPhoneSE = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 568.0
    static let iPhone8 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 667.0
    static let iPhone8Plus = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 736.0
    static let iPhoneXr = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 896.0
    static let iPhoneXs = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 812.0
    static let iPhoneXsMax = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 896.0
    static let iPad = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength == 1024.0
}

struct AppColor {
    static let mainColor = "#ffc440"
    static let textMentionColor = "#eebf3b"
    static let colorReplyCommentBG = "#d2d0c3"
    static let colorText = "#eebf3b"
    static let colorBG = "#fefbe8"
    static let colorBGMenu = "#fdf8d9"
    static let colorYellow = "#f2c955"
    static let colorYellowLight = "#fff197"
    static let colorYellowDark = "#fed87c"
    static let colorGreyLight = "#C0C0C0"
    static let colorBlack = "#000000"
    static let colorSelect = "#4e576c"
    static let colorLineComment = "#ffab2d"
    static let colorQnA = "#0091cb"
    static let Info = "#f4703c"
    static let Share = "#e9ac00"
    static let BDColor = "#DFE0E4"
    
    static let ColorLineInfo = "#ff9b74"
    static let ColorLineQnA = "#aae1e8"
    static let ColorLineShare = "#ffd254"
}

struct AppFont {
    static let HelveticaNeue = "HelveticaNeue"
    static let HelveticaNeueMedium = "HelveticaNeue-Medium"
    static let HelveticaNeueBold = "HelveticaNeue-Bold"
    static let HelveticaNeueItalic = "HelveticaNeue-Italic"
    static let NotoSansBold = "NotoSans-Bold"
    static let NotoSans = "NotoSans-Regular"
    static let NotoSansMedium = "NotoSans-Medium"
    static let NotoSansSemiBold = "NotoSans-SemiBold"
}

struct LibKey {

    static let APIKeysZalo = "1841411986306258520"
    static let APIKeysYahoo = "D8NVV4WFCHRYXVF66CQD"
    static let APPIDSendbird = "1B4F83D8-EC2B-45D1-8F99-1C819E8F4E76"
    static let APITokenSB = "64c7ed5c295cda8161c0e3a35aa9b4853afe2eb7"
    
    static let ClientIDGG = "58226617176-087bbir74265mk7i2pln7hong6q9gknc.apps.googleusercontent.com"
    
    static let linkURLAppStore = "https://apps.apple.com/vn/app/beberia-m%E1%BA%B9-v%C3%A0-b%C3%A9/id1487920280"
    static let linkUpdateApp = "https://beberiavn.page.link/UkMX"
}

struct UserDefault {
    static let providerId = "providerId"
    static let googlePlace = ""
    static let notification = "Notification"
    static let userID = "userID"
    static let badgeCount = "badge"
    
    static let KeyNotification = "Notiiiii"
    static let KeyNotificationSendBird = "NotiiiiiSendBird"
    static let KeyNotificationEnd = "End"
    static let KeyNotificationEndSendBird = "EndSendBird"
    static let KeyFristInstallApp = "FristInstallApp"
    static let KeyEntitlement = "group.com.gngbc.Beberia"
    
    static let KeyUserIDSocial = "userIDSocial"
    
    static var userIDSocial : String {
        get {
            return UserDefaults.standard.string(forKey: UserDefault.KeyUserIDSocial) ?? ""
        }
        set (newValue){
            UserDefaults.standard.set(newValue, forKey: UserDefault.KeyUserIDSocial)
        }
    }
}

struct SpecialString {
    static let connectNameAndPhone = " • "
}

struct Screen {
    static let height = UIScreen.main.bounds.size.height
    static let width = UIScreen.main.bounds.size.width
}

// MARK: - Constants API

struct Key {
    // Type Comment
    static let TypeLikeComment = 2
    static let TypeLikeReplyComment = 3
    static let TypeComment = 4
    static let TypeReplyComment = 5
    static let IndexException = -1
    
    // Placeholder Image
    static let ImagePlaceholder = "placeholder"
    
    // Key Notification
    struct KeyNotification {
        static let idSecret = "idSecret"
        static let idFeed = "idFeed"
        static let CreateFeed = "CreateFeed"
        static let EditFeed = "EditFeed"
        static let idDiary = "idDiary"
        static let numberComment = "numberComment"
        static let numberLike = "numberLike"
    }
    
    struct DateFormat {
        static let DateFormatddMMyy = "dd/MM/yy"
        static let DateFormatddMMyyyy = "dd/MM/yyyy"
    }
    
    struct FontSizeAttribuite {
        static let FontSize = " style='font-size: 14px; font-family: \(AppFont.NotoSans)'>"
    }
    
    struct HeaderJsonImage {
        static let HeaderJsonImage = "data:image/jpeg;base64,"
    }
    
    struct AccountTest {
        static let UserName = "tkt123456"
        static let Pass = "123456"
    }
    
    static let Content = "content"
    static let MessageID = "messageID"
    static let Name = "name"
    static let Image = "image"
    static let ArrayUserIDLike = "arrayUserIDLike"
    
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}

extension Notification.Name {
    static let loginSuccess = Notification.Name("loginSuccess")
    static let shareNewsFeed = Notification.Name("shareNewsFeed")
    static let editProfileChild = Notification.Name("editProfileChild")
    static let editProfileInfo = Notification.Name("editProfileInfo")
    static let editProfileParent = Notification.Name("editProfileParent")
    static let checkEditDiary = Notification.Name("checkEditDiary")
    static let checkEditHome = Notification.Name("checkEditHome")
    static let checkEditDiarySreach = Notification.Name("checkEditDiarySreach")
    static let checkEditHomeSreach = Notification.Name("checkEditHomeSreach")
    static let checkEditDiaryProfile = Notification.Name("checkEditDiaryProfile")
    static let checkEditHomeProfile = Notification.Name("checkEditHomeProfile")
    static let checkEditDiarySave = Notification.Name("checkEditDiarySave")
//    static let deleteDiary = Notification.Name("deleteDiary")
    static let checkEditHomeSave = Notification.Name("checkEditHomeSave")
    static let updateBagde = Notification.Name("updateBagde")
    static let updateNumberComment = Notification.Name("updateNumberComment")
    static let updateNumberLike = Notification.Name("updateNumberLike")
    static let updateFeedNew = Notification.Name("updateFeedNew")
    static let updateDiaryNew = Notification.Name("updateDiaryNew")
    static let updateNumberTicket = Notification.Name("updateNumberTicket")
    static let updateDeleteChanel = Notification.Name("updateDeleteChanel")
    
    static let createSecret = Notification.Name("createSecret")
    
}



