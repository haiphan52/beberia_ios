//
//  LocationManager.swift
//  Beberia
//
//  Created by IMAC on 8/29/19.
//  Copyright © 2019 IMAC. All rights reserved.
//



import RealmSwift
import Foundation

class LocationManager {
    var realm: Realm?
    static let sharedInstance = LocationManager()
    
    private init() {
        realm = try! Realm()
    }
    
    public func getDataFromDB() -> Results<LocationRealm>? {
        guard let realm = realm else {
            return nil
        }
        let results: Results<LocationRealm> = realm.objects(LocationRealm.self)
        return results
    }
    
    public func deleteAll()
    {
        if (realm != nil) {
            try! realm!.write {
                realm!.deleteAll()
            }
        } else {
            print("save location error")
        }
    }
    
    public func saveLocation(_ comic: LocationRealm) throws
    {
        if (realm != nil) {
            try! realm!.write {
                realm!.add(comic)
            }
        } else {
            print("save location error")
        }
    }
    
    public func findLocationById(_ locationNameVi: String) throws -> Results<LocationRealm>?
    {
        if (realm != nil) {
            let predicate = NSPredicate(format: "locationNameVi = %@", locationNameVi)
            return realm!.objects(LocationRealm.self).filter(predicate)
        } else {
            print("find location error")
            return nil
            
        }
    }
    
}

class LocationRealm: Object {
    let locations = List<LocationRealm>()
    @objc dynamic var id = 0
    @objc dynamic var locationCode = 0
    @objc dynamic var locationNameVi = ""
    @objc dynamic var locationNameEn = ""
    @objc dynamic var userActive = 0
    override static func primaryKey() -> String? {
        return "id"
    }
}
