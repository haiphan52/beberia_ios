//
//  User.swift
//  Canvasee
//
//  Created by VNPM1 on 5/23/19.
//  Copyright © 2019 VNPM1. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserInfo: NSObject {
    
    static var shareUserInfo = UserInfo()
    
    var token: String = ""
    var id: Int? = 0
    var email: String = ""
    var name: String = ""
    var birthday: Int = 0
    var gender: Int = 0
    var avartar: String = ""
    var cover: String = ""
    var status: Int = 0
    var fcm_token: String = ""
    var point: Int = 0
    var request_follow: Int = 0
    var phone: String = ""
    var number_follower: Int = 0
    var number_feed: Int = 0
    var check_follow: Int = 0
    var number_following: Int = 0
    var choose_hashtag: Int = 0
    var baby_info: [BabyInfo] = []
    var city_name = CityName.init(json: "")
    var unWatch: Int = 0
    var district = DistrictModel.init(json: "")
    var game = GameModel.init(json: "")
    var passwordSecrect: String = ""
    var unWatch_secret:Int = 0
    var is_otp:Int = 0
    var is_easy:Int = 0
    var is_cycle:Int = 0
    var verified_card:Int = 0
    var is_social:Int = 0
    var total_invite:Int = 0
    var new_device:Int = 0
    var address: String = ""
    
    func initDataUser(json: JSON){
        token = json["token"].stringValue
        id = json["user"]["id"].intValue
        is_social = json["user"]["is_social"].intValue
        email = json["user"]["email"].stringValue
        passwordSecrect = json["user"]["password_secret"].stringValue
        name = json["user"]["display_name"].stringValue
        birthday = json["user"]["birthday"].intValue
        unWatch = json["user"]["unWatch"].intValue
        unWatch_secret = json["user"]["unWatch_secret"].intValue
        gender = json["user"]["gender"].intValue
        avartar = json["user"]["avartar"].stringValue
        cover = json["user"]["cover"].stringValue
        status = json["user"]["status"].intValue
        avartar = json["user"]["avatar"].stringValue
        fcm_token = json["user"]["fcm_token"].stringValue
        point = json["user"]["point"].intValue
        request_follow = json["user"]["request_follow"].intValue
        number_follower = json["user"]["number_follower"].intValue
        number_feed = json["user"]["number_feed"].intValue
        check_follow = json["user"]["check_follow"].intValue
        number_following = json["user"]["number_following"].intValue
        choose_hashtag = json["user"]["choose_hashtag"].intValue
        phone = json["user"]["phone"].stringValue
        is_otp = json["user"]["is_otp"].intValue
        is_easy = json["user"]["is_easy"].intValue
        is_cycle = json["user"]["is_cycle"].intValue
        verified_card = json["user"]["verified_card"].intValue
        
        for json in json["user"]["baby_info"].arrayValue{
            baby_info.append(BabyInfo.init(json: json))
        }
        
//        baby_info = baby_info.filter { (baby) -> Bool in
//            return baby.ageRange != 0
//        }
        
        city_name = CityName.init(json: json["user"]["city_name"])
        district =  DistrictModel.init(json: json["user"]["district"])
        game =  GameModel.init(json: json["user"]["game"])
       
        address = json["user"]["address"].stringValue
        total_invite = json["user"]["total_invite"].intValue
        new_device = json["user"]["new_device"].intValue
        
    }

    
    func initDataUserWithoutToken(json: JSON){
        id = json["id"].intValue
        email = json["email"].stringValue
        name = json["display_name"].stringValue
        birthday = json["birthday"].intValue
        unWatch = json["unWatch"].intValue
        unWatch_secret = json["unWatch_secret"].intValue
        gender = json["gender"].intValue
        avartar = json["avartar"].stringValue
        cover = json["cover"].stringValue
        status = json["status"].intValue
        avartar = json["avatar"].stringValue
        fcm_token = json["fcm_token"].stringValue
        point = json["point"].intValue
        request_follow = json["request_follow"].intValue
        number_follower = json["number_follower"].intValue
        number_feed = json["number_feed"].intValue
        check_follow = json["check_follow"].intValue
        number_following = json["number_following"].intValue
        choose_hashtag = json["choose_hashtag"].intValue
        phone = json["phone"].stringValue
        is_otp = json["user"]["is_otp"].intValue
        is_easy = json["is_easy"].intValue
        is_cycle = json["is_cycle"].intValue
        verified_card = json["verified_card"].intValue
        is_social = json["is_social"].intValue
        
        baby_info = [BabyInfo]()
        for json in json["baby_info"].arrayValue{
            baby_info.append(BabyInfo.init(json: json))
        }
        
//        baby_info = baby_info.filter { (baby) -> Bool in
//            return baby.ageRange != 0
//        }
        
        city_name = CityName.init(json: json["city_name"])
        district = DistrictModel.init(json: json["district"])
        game =  GameModel.init(json: json["user"]["game"])
        
        address = json["address"].stringValue
        total_invite = json["total_invite"].intValue
        new_device = json["new_device"].intValue
    }
    
}
