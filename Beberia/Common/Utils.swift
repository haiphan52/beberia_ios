//
//  Utils.swift
//  
//
//  Created by Mac on 4/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import FFPopup
import SwiftEntryKit
import Alamofire
import ImageIO
import MobileCoreServices
import RxSwift
import RxCocoa

class Utils: NSObject {
    
    class func openAppStore() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1487920280"),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:]) { (opened) in
                if(opened){
                    print("App Store Opened")
                }
            }
        } else {
            print("Can't Open URL on Simulator")
        }
    }
    
    class func dismissPopAllViewViewControllers() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
        }
    }
    
    static func getAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
    
    class func dateToString(date: Date, format: String?) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format ?? "dd/MM/yyyy HH:mm"
        return dateFormatter.string(from: date)
    }
    
    class func timeStampToString(timeStamp: Int64, format:String = Key.DateFormat.DateFormatddMMyyyy)->String{
        return Utils.dateToString(date: Date(timeIntervalSince1970: TimeInterval(String(timeStamp).count > 10 ? timeStamp/1000 : timeStamp)), format: format)
    }
    
    class func toTimeInterval(date: Date) -> String {
        let timeStamp = Int64(date.timeIntervalSince1970)
        return "\(timeStamp)"
    }
    
    class func loadXib(viewName: String, containerView: UIView) -> UIView{
        let viewLoad = Bundle.main.loadNibNamed(viewName, owner: nil, options: nil)![0] as! UIView
        viewLoad.frame = containerView.bounds
        viewLoad.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(viewLoad)
        return viewLoad
    }
    
    class func showAlertView(controller: UIViewController, title : String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
       alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (UIAlertAction) in
                     // self.getTopMostViewController()?.navigationController?.popViewController(animated: true)
                  }))
        controller.present(alert, animated: true, completion: nil)
    }
    
   
    class func presentLoginVC(){
        let login = R.storyboard.main.loginSocialViewController()
        login?.hidesBottomBarWhenPushed = true
        login?.isCheckLogin = true
        let rootViewLoginVC = UINavigationController.init(rootViewController: login!)
         rootViewLoginVC.modalPresentationStyle = .fullScreen
        self.getTopMostViewController()?.navigationController?.present(rootViewLoginVC, animated: true, completion: nil)
    }
    
    class func showAlertViewLogin(controller: UIViewController, title : String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (UIAlertAction) in
            self.presentLoginVC()
        }))
        alert.addAction(UIAlertAction.init(title: "Huỷ", style: .cancel, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    class func showAlertOKWithAction(controller: UIViewController, title : String, message: String, cancel: Bool = false, completion:@escaping ()->()){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (UIAlertAction) in
            completion()
        }))
        if cancel == true{
            alert.addAction(UIAlertAction.init(title: "Huỷ", style: .cancel, handler: nil))
        }
        controller.present(alert, animated: true, completion: nil)
    }

    class func showAlertOK(controller: UIViewController, title : String, message: String, cancel: Bool = false){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (UIAlertAction) in
            self.getTopMostViewController()?.navigationController?.popViewController(animated: true)
        }))
        if cancel == true{
            alert.addAction(UIAlertAction.init(title: "Huỷ", style: .cancel, handler: nil))
        }
        controller.present(alert, animated: true, completion: nil)
    }
    
    class func showAlertOKWithoutAction(controller: UIViewController, title : String, message: String){
           let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
           alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (UIAlertAction) in
              // self.getTopMostViewController()?.navigationController?.popViewController(animated: true)
           }))
           controller.present(alert, animated: true, completion: nil)
       }
    
    class func isValid(_ email: String) -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    class func cutEmail(email: String)->String{
        if email.contains("@"){
            let arrayString = email.components(separatedBy: "@")
            var emailTemp : String = ""
            if arrayString[0].count >= 3{
                var mySubstring = arrayString[0].prefix((arrayString[0].count - 3))
                mySubstring = mySubstring + "***@"
                emailTemp = mySubstring + arrayString[1]
            }else{
                emailTemp = "***@" + arrayString[1]
            }
            return emailTemp
        }
        return ""
    }
    
    class func saveRememberPass(isSave: Bool){
        UserDefaults.standard.set(isSave, forKey: "RememberPass")
    }
    
    class func getRememberPass() -> Bool{
        return UserDefaults.standard.bool(forKey: "RememberPass")
    }
    
//    class func setCheckLogout(logout: String){
//        UserDefaults.standard.set(logout, forKey: "1231231212312312")
//        UserDefaults.standard.synchronize()
//    }
//    
//    class func getCheckLogout() -> String?{
//        return UserDefaults.standard.string(forKey: "1231231212312312")
//    }
    
    class func saveStringToUserDefault(name: String, value: String){
        UserDefaults.standard.set(value, forKey: name)
    }
    
    class func getStringFromUserDefaul(name: String) -> String?{
        return UserDefaults.standard.string(forKey: name)
    }
    
    class func addTrailingMoreToLable(_ label: ActiveLabel) {
        DispatchQueue.main.async {
            let readMoreText = "...More"
            let stringColor: UIColor = UIColor.red
            let attributes = [NSAttributedString.Key.foregroundColor: stringColor, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18)]
            let numberOfLines = label.numberOfVisibleLines

            if numberOfLines > 3 {
                
                let lengthForVisibleString: Int = self.fit( label.text, into: label)
                let mutableString = label.text ?? ""
                let trimmedString = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: (label.text?.count ?? 0) - lengthForVisibleString), with: "")
                let readMoreLength: Int = readMoreText.count
                var trimmedForReadMore: String = ""
                if trimmedString.count >= 7{
                    trimmedForReadMore = (trimmedString as NSString).replacingCharacters(in: NSRange(location: trimmedString.count - readMoreLength, length: readMoreLength), with: "")
                }else{
                    trimmedForReadMore = trimmedString
                }
                
                let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: UIFont.init(name: AppFont.HelveticaNeue, size: 13.0)])
                let readMoreAttributed = NSMutableAttributedString(string: readMoreText, attributes: attributes)
                answerAttributed.append(readMoreAttributed)
                
                label.attributedText = answerAttributed
            }
        }
    }
    
    class func isAnimatedImage(_ data: Data) -> Bool {
     //   if let data = try? Data(contentsOf: imageUrl),
              if let source = CGImageSourceCreateWithData(data as CFData, nil) {

            let count = CGImageSourceGetCount(source)
            return count > 1
        }
        return false
    }
    
    class func fit(_ string: String?, into label: UILabel?) -> Int {
        guard let stringObjc = string as NSString? else {
            return 0
        }
        let font: UIFont = label?.font ?? UIFont.init(name: AppFont.HelveticaNeue, size: 13.0)!
        let mode: NSLineBreakMode? = label?.lineBreakMode
        let labelWidth: CGFloat? = label?.frame.size.width
        let labelHeight: CGFloat? = label?.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth ?? 0.0, height: CGFloat.greatestFiniteMagnitude)
        let attributes = [NSAttributedString.Key.font: font]
        
        let device = UIDevice.current
        let iosVersion = Double(device.systemVersion) ?? 0
        
        if iosVersion > 7 {
            let attributedText = NSAttributedString(string: string ?? "", attributes: attributes)
            let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
            do {
                if boundingRect.size.height > (labelHeight ?? 0.0) {
                    var index: Int = 0
                    var prev: Int
                    let characterSet = CharacterSet.whitespacesAndNewlines
                    repeat {
                        prev = index
                        if mode == .byCharWrapping {
                            index += 1
                        } else {
                            index = Int((string as NSString?)?.rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: (string?.count ?? 0) - index - 1)).location ?? 0)
                        }
                    } while index != NSNotFound && index < (string?.count ?? 0)
                        && (stringObjc.substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes, context: nil).size.height) <= labelHeight!
                    return prev;
                }
            }
        } else {
            if stringObjc.size(withAttributes: attributes).height > labelHeight! {
                var index: Int = 0
                var prev: Int
                let characterSet = CharacterSet.whitespacesAndNewlines
                repeat {
                    prev = index
                    if mode == .byCharWrapping {
                        index += 1
                    } else {
                        index = stringObjc.rangeOfCharacter(from: characterSet, options: NSString.CompareOptions.caseInsensitive, range: NSRange(location: index + 1, length: stringObjc.length - index - 1)).location
                    }
                    
                } while index != NSNotFound && index < (string?.count)! && (stringObjc.substring(to: index) as NSString).size(withAttributes: attributes).height <= labelHeight!
                return prev
                
            }
        }
        return (string?.count)!
    }
    
//    class func getHeightImage(url: String)-> CGFloat{
//        let cfurl = CFURLCreateWithString(nil, url as CFString, nil)
//        if let imageSource = CGImageSourceCreateWithURL(cfurl as! CFURL, nil) {
//            if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
//                let pixelWidth = imageProperties[kCGImagePropertyPixelWidth] as! CGFloat
//                let pixelHeight = imageProperties[kCGImagePropertyPixelHeight] as! CGFloat
//                print("the image width is: \(pixelWidth)")
//                print("the image height is: \(pixelHeight)")
//                print("the image ratio is: \(pixelWidth / pixelHeight)")
//                return pixelWidth / pixelHeight
//            }
//            return 0
//        }
//        return 0
//    }
    
//    class func stringFromTimeInterval(interval: TimeInterval) -> String {
//        let formatter = DateComponentsFormatter()
//        formatter.allowedUnits = [.minute, .second]
//        return formatter.string(from: interval)!
//    }
    
    class func stringFromTimeInterval(interval: TimeInterval?) -> String {
        let endingDate = Date()
        if let timeInterval = interval {
            let startingDate = endingDate.addingTimeInterval(-timeInterval)
            let calendar = Calendar.current
            
            var componentsNow = calendar.dateComponents([.minute, .second], from: startingDate, to: endingDate)
            if let minute = componentsNow.minute, let seconds = componentsNow.second {
                return "\(minute):\(seconds)"
            } else {
                return "00:00"
            }
            
        } else {
            return "00:00"
        }
    }
    
    class func presentTabbar(){
        
        guard let tabBarVC = R.storyboard.main.tabBarVC() else {
            return
        }
        tabBarVC.modalPresentationStyle = .fullScreen
        // self.present(tabBarVC, animated: true, completion: nil)
        
        Utils.getTopMostViewController()?.present(tabBarVC, animated: true, completion: {
            
           // DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
                if !Utils.checkLogin(selfView: Utils.getTopMostViewController()!, showAlert: false) {
                    tabBarVC.selectedIndex = 1
                }
          //  }
        })
    }
    
    class func getVersionUpdateApp(fromToView: UIViewController){
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String

        if appVersion != "2.0.2" {
            Utils.showAlertOKWithAction(controller: fromToView, title: "Thông báo", message: "Cập nhật App") {
                if let url = URL(string: "itms-apps://apple.com/app/id1487920280") {
                    UIApplication.shared.open(url)
                }
            }
        }
        
    }
    
    class func dateStringToTimeStamp(date: String, formatString: String = "dd/MM/yyyy") -> Int {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat = formatString
        let date = dfmatter.date(from: date)
        let dateStamp:TimeInterval = (date ?? Date()).timeIntervalSince1970
        let dateSt:Int = Int(dateStamp)
        print(dateSt)
        return dateSt
    }
    
    class func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    class func updateColorButton(btnSelect: UIButton, btnUnSelect: UIButton){
        btnSelect.setTitleColor(UIColor.init(hexString: AppColor.colorYellow), for: .normal)
        btnSelect.backgroundColor = UIColor.init(hexString: AppColor.colorBGMenu)
        btnSelect.borderColor = UIColor.init(hexString: AppColor.colorYellow)
        btnSelect.borderWidth = 1
        btnSelect.cornerRadius = 15
        
        btnUnSelect.setTitleColor(UIColor.init(hexString: AppColor.colorSelect,alpha: 0.3), for: .normal)
        btnUnSelect.backgroundColor = UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.4)
        btnUnSelect.borderColor = UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.5)
        btnUnSelect.borderWidth = 1
        btnUnSelect.cornerRadius = 15
    }
    
    
    class func updateColorLocation(btnSelect: UIButton, btnUnSelect: UIButton, viewSelect: UIView, view:UIView){
        btnSelect.setTitleColor(UIColor.init(hexString: AppColor.colorYellow), for: .normal)
        btnUnSelect.setTitleColor(UIColor.init(hexString: AppColor.colorSelect,alpha: 0.3), for: .normal)
        view.backgroundColor =  UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.4)
        view.borderColor = UIColor.init(hexString: AppColor.colorGreyLight,alpha: 0.5)
        view.borderWidth = 1
        view.cornerRadius = 15
        
        viewSelect.backgroundColor =  UIColor.init(hexString: AppColor.colorBGMenu,alpha: 0.4)
        viewSelect.borderColor = UIColor.init(hexString: AppColor.colorYellow,alpha: 0.5)
        viewSelect.borderWidth = 1
        viewSelect.cornerRadius = 15
    }
    
    class func districtColorDefault(btn:UIButton, img:UIImageView, view:UIView){
        btn.setTitleColor(UIColor.init(hexString: "#979797",alpha: 0.87), for: .normal)
        img.image = R.image.ic_ArrowDown()
        view.backgroundColor =  UIColor.init(hexString: "#f6f6f7")
        view.borderColor = UIColor.init(hexString: "#979797",alpha: 0.2)
    }
    
    static func readJSONFromFile(fileName: String) -> Array<Any>?
    {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let data = jsonResult["datas"] as? Array<Any> {
                    return data
                }
            } catch {
                return []
            }
        }
        return []
    }
    
    
    static func getImagesFromBundle(path: String)-> [UIImage]{
        var icons = [UIImage]()
        
        let fileManager = FileManager.default
        let bundleURL = Bundle.main.bundleURL
        let assetURL = bundleURL.appendingPathComponent("\(path).bundle") // Bundle URL
        do {
            let contents = try fileManager.contentsOfDirectory(at: assetURL,
                                                               includingPropertiesForKeys: [URLResourceKey.nameKey, URLResourceKey.isDirectoryKey],
                                                               options: .skipsHiddenFiles)
            let sortedURLs = contents.sorted { a, b in
                return a.lastPathComponent
                    .localizedStandardCompare(b.lastPathComponent)
                        == ComparisonResult.orderedAscending
            }
            
         //   let queue = DispatchQueue(label: "Serial queue")
         //   let group = DispatchGroup()
            for item in sortedURLs { // item is the URL of everything in MyBundle imgs or otherwise.
             //   group.enter()
             //   queue.sync {
                    let image = UIImage(contentsOfFile: item.path) // Initializing an image
             //       DispatchQueue.main.async {
                        icons.append(image!) // Adding the image to the icons array
             //           group.leave()
                        
            //        }
                    
             //   }
                
             //   group.wait()
            }
            
//            group.notify(queue: queue) {
//                print("All tasks done")
//                print(icons.count)
//
//            }
            return icons
            
        }
        catch let error as NSError {
            print(error)
            return []
        }
    }
    
    
    static func checkLogin(selfView: UIViewController, showAlert: Bool = true)->Bool{
      // if UserInfo.shareUserInfo.name == Key.AccountTest.UserName || UserInfo.shareUserInfo.name == ""{
        if UserInfo.shareUserInfo.token == ""{
            if showAlert{
                Utils.showAlertViewLogin(controller: selfView, title: R.string.localizable.commonNoti(), message: "Vui lòng đăng nhập để sử dụng chức năng!")
            }
            return false
        }
        return true
    }
    
    static func checkShowPopupVerifyAccount(){
           guard let viewVC = Utils.getTopMostViewController() else { return }
           if Utils.checkLogin(selfView: viewVC){
            if UserInfo.shareUserInfo.is_otp == 0 && UserInfo.shareUserInfo.is_social == 0{
                   let vc = PopupHomeVC.init(nib: R.nib.popupHomeVC)
                   vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                   vc.writePopupHome = { index in
                       let forgetPassVC = ForgetPassVC.init(nib: R.nib.forgetPassVC)
                       //  let naviAccountVerifyVC = UINavigationController.init(rootViewController: accountVerifyVC)
                        forgetPassVC.hidesBottomBarWhenPushed = true
                        forgetPassVC.typeView = .register
                       viewVC.navigationController?.pushViewController(forgetPassVC, animated: true)
                        
                   }
                   viewVC.present(vc, animated: true)
               }
           }
       }
    
    class func getDateFromTimeStamp(timeStamp : Double,dateFormat: String = "hh:mm a") -> String {

           let date = NSDate(timeIntervalSince1970: timeStamp / 1000)

           let dayTimePeriodFormatter = DateFormatter()
        //   dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"
        // UnComment below to get only time
         dayTimePeriodFormatter.dateFormat = dateFormat

           let dateString = dayTimePeriodFormatter.string(from: date as Date)
           return dateString
       }
    
    class func checkForUpdate(completion:@escaping(Bool)->()){
        
        guard let bundleInfo = Bundle.main.infoDictionary,
            let currentVersion = bundleInfo["CFBundleShortVersionString"] as? String,
            //let identifier = bundleInfo["CFBundleIdentifier"] as? String,
            let url = URL(string: "https://itunes.apple.com/lookup?bundleId=com.gngbc.Beberia")
            else{
                print("some thing wrong")
                completion(false)
                return
        }
        
        let task = URLSession.shared.dataTask(with: url) {
            (data, resopnse, error) in
            if error != nil{
                completion(false)
                print("something went wrong")
            }else{
                do{
                    guard let reponseJson = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any],
                        let result = (reponseJson["results"] as? [Any])?.first as? [String: Any],
                        let version = result["version"] as? String
                        else{
                            completion(false)
                            return
                    }
                    print("Current Ver:\(currentVersion)")
                    print("Prev version:\(version)")
                    if currentVersion != version{
                        completion(true)
                    }else{
                        completion(false)
                    }
                }
                catch{
                    completion(false)
                    print("Something went wrong")
                }
            }
        }
        task.resume()
    }
    
    class func checkShowViewEven(baner : Baner){
          if !UserDefaults.standard.bool(forKey: "\(String(describing: baner.id))"){
              if let link = baner.link,
                 let url = URL(string: link),
                 !Utils.getAppDelegate().isCheckGetData,
                 let cfURL = url as? CFURL {
                var height = 0
                if let imageSource = CGImageSourceCreateWithURL(cfURL, nil) {
                    if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                        let pixelWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                        let pixelHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                        height = (Int((UIScreen.main.bounds.width - 40)) * pixelHeight) / pixelWidth
                    }
                }
                
                let viewShowEven =  ViewShowEven.instanceFromNib()
                viewShowEven.imageEven.kf.setImage(with: URL.init(string: baner.link ?? ""))
                viewShowEven.frame = CGRect.init(x: 0, y: 0, width: Int(UIScreen.main.bounds.width - 40), height: height)
                let popup = FFPopup.init(contentView: viewShowEven)
                viewShowEven.tapNotRemind = {
                    UserDefaults.standard.set(true, forKey: "\(String(describing: baner.id))")
                    popup.dismiss(animated: true)
                }
                
                viewShowEven.tapClose = {
                    popup.dismiss(animated: true)
                }
                
                viewShowEven.tapEven = {
                    popup.dismiss(animated: true)
                    
                    guard let bannerType = BannerView.BannerTypeRedirect(rawValue: baner.typeRedirect ?? 0 ) else {
                        return
                    }
                    switch bannerType {
                    case .none:
                        if let url = URL.init(string:baner.linkAds ?? "")  {
                            if (baner.linkAds ?? "") == LibKey.linkUpdateApp{
                                self.checkForUpdate { (isUpdate) in
                                    if isUpdate {
                                        if let url = URL(string: LibKey.linkURLAppStore) {
                                            DispatchQueue.main.async {
                                                UIApplication.shared.open(url)
                                        }
                                        }
                                    }else{
                                        DispatchQueue.main.async {
                                            self.showAlertOK(controller: self.getTopMostViewController()!, title: "Thông báo", message: "Bạn đã cài version mới nhất!")
                                        }
                                        UserDefaults.standard.set(true, forKey: "\(String(describing: baner.id))")
                                       // popup.dismiss(animated: true)
                                    }
                                   
                                }
                            }else{
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(url , options: [:], completionHandler: nil)
                                } else {
                                    UIApplication.shared.openURL(url)
                                }
                            }

                        }
                    case .rankPoint:
                        let rankPointVC = RankPointVC.init(nib: R.nib.rankPointVC)
                        rankPointVC.hidesBottomBarWhenPushed = true
                        Utils.getTopMostViewController()?.navigationController?.pushViewController(rankPointVC, animated: true)
                    case .giftGame:
                        let gameVC = GameHomeViewController.init(nib: R.nib.gameHomeViewController)
                        gameVC.modalPresentationStyle = .overFullScreen
                        Utils.getTopMostViewController()?.present(gameVC, animated: true, completion: nil)
                    case .info:
                        guard let lichKTVC = R.storyboard.main.infoViewController() else{ return }
                        lichKTVC.hidesBottomBarWhenPushed = true
                        Utils.getTopMostViewController()?.navigationController?.pushViewController(lichKTVC, animated: true)
                    case .momTok:
                        guard let momtokVC = R.storyboard.main.secretViewController() else {
                            return
                        }
                        momtokVC.hidesBottomBarWhenPushed = true
                        Utils.getTopMostViewController()?.navigationController?.pushViewController(momtokVC, animated: true)
                    case .news:
                        let listNewHotVC = ListNewHotViewController.init(nib: R.nib.listNewHotViewController)
                        listNewHotVC.hidesBottomBarWhenPushed = true
                        Utils.getTopMostViewController()?.navigationController?.pushViewController(listNewHotVC, animated: true)
                    case .listDiary:
                        let postVC = ListDiaryViewController.init(nib: R.nib.listDiaryViewController)
                        postVC.hidesBottomBarWhenPushed = true
                        postVC.gettype = "3"
                        postVC.title = "Mẹo cho bạn"
                        Utils.getTopMostViewController()?.navigationController?.pushViewController(postVC, animated: true)
                    case .home:
                        guard let tabbar  = Utils.getTopMostViewController()?.navigationController?.tabBarController else { return }
                        tabbar.selectedIndex = 1
                    case .diaary:
                        guard let tabbar  = Utils.getTopMostViewController()?.navigationController?.tabBarController else { return }
                        tabbar.selectedIndex = 2
                    }
                }
                
                popup.showType = .growIn
                popup.dismissType = .shrinkOut
                let layout = FFPopupLayout(horizontal: .center, vertical: .center)
                popup.show(layout: layout)
                
                Utils.getAppDelegate().isCheckGetData = true
            }
            
        }
      }
    class func appStoreVersion(callback: @escaping (Bool,String)->Void) {
        let bundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        Alamofire.request("https://itunes.apple.com/vn/lookup?bundleId=\(bundleId)").responseJSON { response in
            
            print(response)
            
            print(response.result.value as? NSDictionary)
            
            if let json = response.result.value as? NSDictionary,
               let results = json["results"] as? NSArray,
               let entry = results.firstObject as? NSDictionary,
               let appStoreVersion = entry["version"] as? String{
                let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
                let versionUpdate = ConstantApp.shared.textVersion + (currentVersion ?? "")
                if versionUpdate == appStoreVersion || currentVersion == appStoreVersion {
                    callback(true,appStoreVersion)
                }else{
                    callback(false, appStoreVersion)
                }
                
            }else{
                callback(false, "-")
            }
        }
    }
    
    class func checkShowViewUpdate(baner : Baner){
        
        var height = 0
        if let imageSource = CGImageSourceCreateWithURL(URL.init(string: baner.link ?? "") as! CFURL, nil) {
            if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                let pixelWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                let pixelHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                height = (Int((UIScreen.main.bounds.width - 40)) * pixelHeight) / pixelWidth
            }
        }
        
        let viewShowEven =  ViewShowEven.instanceFromNib()
        viewShowEven.imageEven.kf.setImage(with: URL.init(string: baner.link ?? ""))
        viewShowEven.frame = CGRect.init(x: 0, y: 0, width: Int(UIScreen.main.bounds.width - 40), height: height)
        viewShowEven.btnClose.isHidden = true
        viewShowEven.btnNoRemind.isHidden = true
        
        var attributes = EKAttributes.default
        attributes.screenBackground =  .color(color: UIColor.init(hexString: "000000", alpha: 0.4)!)
        attributes.position = .center
        // Animate in and out using default translation
        attributes.entryInteraction = .absorbTouches
        attributes.screenInteraction = .absorbTouches
        attributes.displayDuration = .infinity
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 10, offset: .zero))
        attributes.roundCorners = .all(radius: 15)
        // attributes.positionConstraints.size = .init(width: .offset(value: 20), height: .intrinsic)
        SwiftEntryKit.display(entry: viewShowEven, using: attributes)
        
        viewShowEven.tapEven = {
            
            
            guard let url = URL.init(string: baner.linkAds ?? "") else {
                return
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url , options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
        
    }
    
    static func loadFrameImagesFromAlbum(hasPrefix:String) -> [String]{
        var minions = [String]()
        let fm = FileManager.default
        if let path = Bundle.main.resourcePath {
            let items = try! fm.contentsOfDirectory(atPath: path )
            minions = items.filter{$0.hasSuffix("png")}
            return minions.filter{$0.hasPrefix(hasPrefix)}.sorted { $0 < $1 }
            
        }
        return []
    }
    
    static func animatedGif(from images: [UIImage]) -> URL?{
            let fileProperties: CFDictionary = [kCGImagePropertyGIFDictionary as String: [kCGImagePropertyGIFLoopCount as String: 0]]  as CFDictionary
            let frameProperties: CFDictionary = [kCGImagePropertyGIFDictionary as String: [(kCGImagePropertyGIFDelayTime as String): 1.0]] as CFDictionary
            
            let documentsDirectoryURL: URL? = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileURL: URL? = documentsDirectoryURL?.appendingPathComponent("animated.gif")
            
            if let url = fileURL as CFURL? {
                if let destination = CGImageDestinationCreateWithURL(url, kUTTypeGIF, images.count, nil) {
                    CGImageDestinationSetProperties(destination, fileProperties)
                    for image in images {
                        
                        if let cgImage = image.cgImage {
                            CGImageDestinationAddImage(destination, cgImage, frameProperties)
                        }
                    }
                    if !CGImageDestinationFinalize(destination) {
                        print("Failed to finalize the image destination")
                    }
                    print("Url = \(fileURL)")
                    return fileURL
                }
                return nil
            }
        return nil
    }
    
    static func createPreviewGIF(with images: [UIImage], loopCount: Int = 0, frameDelay: Double, fileName: String) -> Observable<URL?> {
        
        return Observable.create({ subscriber in

            let fileProperties = [kCGImagePropertyGIFDictionary as String: [kCGImagePropertyGIFLoopCount as String: loopCount]] as CFDictionary
            let frameProperties = [kCGImagePropertyGIFDictionary as String: [kCGImagePropertyGIFDelayTime as String: frameDelay]] as CFDictionary
            
            let documentsDirectory = NSTemporaryDirectory()
            let url = NSURL(fileURLWithPath: documentsDirectory).appendingPathComponent("\(fileName).gif")
            
            if let url = url, let destination = CGImageDestinationCreateWithURL(url as CFURL, kUTTypeGIF, images.count, nil) {
                
                CGImageDestinationSetProperties(destination, fileProperties)
                
                for i in 0..<images.count {
                    
                    if let cgImage = images[i].cgImage {
                        CGImageDestinationAddImage(destination, cgImage, frameProperties)
                    }
                    
                }
                
                if CGImageDestinationFinalize(destination) {
                    
                    subscriber.onNext(url)
                    
                } else {
                    
                    subscriber.onNext(nil)

                }
                
            } else  {
             
                subscriber.onNext(nil)

            }
            
            subscriber.onCompleted()
            
            return Disposables.create()

        })
        
    }
}
