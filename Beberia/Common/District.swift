//
//  District.swift
//
//  Created by OS on 12/9/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class District: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let districtCode = "district_code"
    static let districtName = "district_name"
  }

  // MARK: Properties
  public var id: Int?
  public var districtCode: String?
  public var districtName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    id = json[SerializationKeys.id].int
    districtCode = json[SerializationKeys.districtCode].string
    districtName = json[SerializationKeys.districtName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = districtCode { dictionary[SerializationKeys.districtCode] = value }
    if let value = districtName { dictionary[SerializationKeys.districtName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.districtCode = aDecoder.decodeObject(forKey: SerializationKeys.districtCode) as? String
    self.districtName = aDecoder.decodeObject(forKey: SerializationKeys.districtName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(districtCode, forKey: SerializationKeys.districtCode)
    aCoder.encode(districtName, forKey: SerializationKeys.districtName)
  }

}
